<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'api'], function() {
    //rutas para servicios restful
    Route::post('api/cargar_respuestas_app', 'AppIntegrationController@cargarRespuestasApp');
});


///rutas accessibles si e usuario no esta logeado
Route::group(['middleware' => 'guest'], function () {
Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', ['as' =>'login', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('register', 'Auth\AuthController@getRegister');
Route::post('register', ['as' => 'auth/register', 'uses' => 'Auth\AuthController@postRegister']);
});
//rutas accessibles solo si el usuario esta autenticado y ha ingresado al sistema
Route::group(['middleware' => 'auth'], function () {
Route::get('/', 'HomeController@index');
Route::get('home', 'HomeController@index');
Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@getLogout']);

Route::get('form_editar_usuario/{id}', 'UsuariosController@form_editar_usuario');
Route::post('editar_usuario', 'UsuariosController@editar_usuario');
Route::post('subir_imagen_usuario', 'UsuariosController@subir_imagen_usuario');
Route::post('cambiar_password', 'UsuariosController@cambiar_password');
Route::get('form_educacion_usuario/{id}', 'EducacionController@form_educacion_usuario');
Route::post('agregar_educacion_usuario', 'EducacionController@agregar_educacion');
Route::get('borrar_educacion/{id}', 'EducacionController@borrar_educacion');

Route::get('listado_monitoreo/{page?}', 'MonitoreoController@index');
Route::post('actualizarmonitoreo/{id}', 'MonitoreoController@update');
Route::get('form_monitoreo_aplicadores/{id}', 'MonitoreoController@form_monitoreo_aplicadores');
Route::get('form_monitoreo_sustentantes/{id}/{amie}/{fecha}/{sesion}/{id_usuario}', 'MonitoreoController@form_monitoreo_sustentantes');
Route::get('form_monitoreo_corresponsable/{id}', 'MonitoreoController@form_monitoreo_corresponsable');
Route::post('editar_monitoreo/{id}', 'MonitoreoController@update');
Route::post('editar_sustentante/{id}', 'MonitoreoController@update_sustentante');
Route::get('buscar_monitoreo/{fecha}/{dato?}', 'MonitoreoController@buscar_monitoreo');
Route::get('form_monitoreo_informe/{id}', 'InformeController@form_monitoreo_informe');
Route::get('form_monitoreo_instalacion/{id}', 'MonitoreoController@form_monitoreo_instalacion');
Route::post('editar_aplicador', 'MonitoreoController@editar_aplicador');
Route::post('editar_descarga', 'MonitoreoController@editar_descarga');
Route::post('editar_instalacion', 'MonitoreoController@editar_instalacion');
Route::post('editar_institucion', 'MonitoreoController@editar_institucion');
Route::post('editar_aplicadores/{id}/{id_sede}', 'MonitoreoController@editar_aplicadores');
Route::get('form_monitoreo_institucion/{id}/{id_sede}/{id_usuario}', 'MonitoreoController@form_monitoreo_institucion');
Route::post('editar_rector_monitoreo', 'MonitoreoController@editar_rector_monitoreo');
Route::get('buscar_sustentantes/{aux}/{dato_sustentantes?}', 'MonitoreoController@buscar_sustentantes');
Route::post('editar_listados', 'MonitoreoController@editar_listados');

Route::post('agregar_publicacion_usuario', 'InformeController@agregar_publicacion');
Route::get('borrar_publicacion/{id}', 'InformeController@borrar_publicacion');

Route::get('listado_validacion/{page?}', 'ValidacionController@index');
Route::get('form_validacion_laboratorio/{id}', 'ValidacionController@form_validacion_laboratorio');
Route::get('form_institucion/{id}', 'ValidacionController@form_institucion');
Route::post('editar_rector', 'ValidacionController@update_labo');
Route::post('editar_laboratorio/{id}', 'ValidacionController@editar_laboratorio');
Route::post('agregar_laboratorio', 'ValidacionController@agregar_laboratorio');
Route::post('editar_validador', 'ValidacionController@editar_validador');
Route::get('form_validacion_aplicadores/{id}', 'ValidacionController@form_validacion_aplicadores');

////////////////////////////////////////////////////////// Validación Laboratorios //////////////////////////////////////////////////////////
Route::resource('reporte_validar','Reportes\ReportevalidarController@reporte_validar');
Route::get('fecha_programada/{id}','Reportes\ReportevalidarController@fecha_programada');
Route::get('coordSelect_periodo/{id}','Reportes\ReportevalidarController@coordPeriodo');
Route::get('coordSelect_periodo_rect/{id}','Reportes\ReportevalidarController@coordPeriodo_rect');
Route::get('sesion/{fecha}','Reportes\ReportevalidarController@sesion');
Route::resource('reporte','Reportes\ReportevalidarController@aplicacion');
Route::resource('reporte_general/{tp_reporte}/{id_periodo}/{coordinador}/{fch?}/{sesion?}/','Reportes\ReportevalidarController@reporte_general');
Route::resource('provincia/{tp_reporte}/{id_periodo}/{fch?}/{sesion?}/','Reportes\ReportevalidarController@provincia');
Route::resource('distrito/{tp_reporte}/{id_periodo}/{fch?}/{sesion?}/','Reportes\ReportevalidarController@distrito');
Route::resource('monitor/{tp_reporte}/{id_periodo}/{coordinador}/{fch?}/{sesion?}/','Reportes\ReportevalidarController@monitor');
Route::resource('laboratorio/{tp_reporte}/{id_periodo}/{coordinador}/{fch?}/{sesion?}/','Reportes\ReportevalidarController@laboratorio');
////////////////////////////////////////////////////////// Aplicacion  //////////////////////////////////////////////////////////////////////
Route::resource('reporte_zonal','Reportes\ReporteaplicacionController@index');
Route::resource('reporte_aplicacion/{tp_reporte}/{id_periodo}/{coordinador}/{fch?}/{sesion?}/','Reportes\ReporteaplicacionController@reporte_aplicacion');
Route::resource('provincia_aplicacion/{tp_reporte}/{id_periodo}/{fch?}/{sesion?}/','Reportes\ReporteaplicacionController@provincia_aplicacion');
Route::resource('distrito_aplicacion/{tp_reporte}/{id_periodo}/{fch?}/{sesion?}/','Reportes\ReporteaplicacionController@distrito_aplicacion');
Route::resource('monitor_aplicacion/{tp_reporte}/{id_periodo}/{fch?}/{sesion?}/','Reportes\ReporteaplicacionController@monitor_aplicacion');
Route::resource('laboratorio_aplicacion/{tp_reporte}/{id_periodo}/{fch?}/{sesion?}/','Reportes\ReporteaplicacionController@laboratorio_aplicacion');
Route::resource('sustentante_aplicacion/{tp_reporte}/{id_periodo}/{fch?}/{sesion?}/','Reportes\ReporteaplicacionController@sustentante_aplicacion');
////////////////////////////////////////////////////////// Exportar Excel  //////////////////////////////////////////////////////////////////////
Route::get('exportaruno/{id}/excel','ExportarexcelController@exportar_exceluno');
Route::get('exportarLlamadas/{id}/excel','ExportarexcelController@exportar_llamadas');
Route::get('exportarLlamadas_apli/{id}/excel','ExportarexcelController@exportar_llamadas_apli');
Route::get('exportarLlamadas_apliValidacion/{id}/excel','ExportarexcelController@exportar_llamadas_apliValidacion');
Route::get('exportar_info_aplicacion/{id}/excel','ExportarexcelController@exportar_info_aplicacion');
Route::get('exportar_info_sustentantes_off/{id}/excel','ExportarexcelController@exportar_info_sustentantes_off'); // sustentantes_off
Route::get('boton','BotonupdateController@index');
Route::get('boton_update/{id_periodo}','BotonupdateController@boton_update');
Route::get('observacion_descarga/{id}/{fch?}/{sesion?}/excel','ExportarobservacionesaplicacionController@observacion_descarga');
Route::get('observacion_instalo/{id}/{fch?}/{sesion?}/excel','ExportarobservacionesaplicacionController@observacion_instalo');
Route::get('observacion_inicio/{id}/{fch?}/{sesion?}/excel','ExportarobservacionesaplicacionController@observacion_inicio');

//////////////// MAPA //////////////////////////////////////////////////////////////////////////
Route::resource('mapa','mapaController@mapa'); // Nuevo
Route::resource('mapa_zona/{tp_reporte}/{id_periodo}/{fch?}/{sesion?}/','mapaController@mapa_zona'); // Nuevo
Route::resource('mapa_zona_n/{zona}/{tp_reporte}/{id_periodo}/{fch?}/{sesion?}/','mapaController@mapa_zona_n'); // Nuevo

//////////////// BORRAR //////////////////////////////////////////////////////////////////////////
Route::resource('reporte_zonal_borrar','Reportes\ReporteaplicacionController@borrar'); // Nuevo 
Route::resource('reporte_aplicacion_borrar/{tp_reporte}/{id_periodo}/{coordinador}/{fch?}/{sesion?}/','Reportes\ReporteaplicacionController@reporte_aplicacion_borrar'); // Nuevo
Route::get('exportar_info_aplicacion_borrar/{id}/excel','ExportarborrarController@exportar_info_aplicacion_borrar');

});
//rutas accessibles solo para el usuario administrador
Route::group(['middleware' => 'usuarioAdmin'], function () {
Route::get('listado_usuarios/{page?}', 'UsuariosController@listado_usuarios')->name('listado_usuarios.list');
Route::get('form_nuevo_usuario', 'UsuariosController@form_nuevo_usuario');
Route::post('agregar_nuevo_usuario', 'UsuariosController@agregar_nuevo_usuario');      
Route::get('form_cargar_datos_usuarios', 'UsuariosController@form_cargar_datos_usuarios');
Route::post('cargar_datos_usuarios', 'UsuariosController@cargar_datos_usuarios');
Route::get('form_iniciar_proceso', 'ProcesoController@index');

});

// Rutas para aplicación onLine
Route::group(['middleware' => 'auth'], function () {
Route::get('listado_monitoreo_online/{page?}', 'OnLine\MonitoreoController@index');
Route::get('buscar_monitoreo_online/{fecha}/{dato?}', 'OnLine\MonitoreoController@buscar_monitoreo');
Route::post('editar_monitoreo_online/{id}', 'OnLine\MonitoreoController@update');
Route::get('form_monitoreo_institucion_online/{id}/{id_sede}/{id_usuario}', 'OnLine\MonitoreoController@form_monitoreo_institucion');
Route::post('editar_rector_monitoreo_online', 'OnLine\MonitoreoController@editar_rector_monitoreo');
Route::get('form_monitoreo_sustentantes_online/{id}/{amie}/{fecha}/{sesion}/{id_usuario}', 'OnLine\MonitoreoController@form_monitoreo_sustentantes');
Route::get('buscar_sustentantes_online/{aux}/{dato_sustentantes?}', 'OnLine\MonitoreoController@buscar_sustentantes');
Route::post('editar_sustentante_online/{id}', 'OnLine\MonitoreoController@update_sustentante');
Route::get('form_monitoreo_informe_online/{id}', 'OnLine\InformeController@form_monitoreo_informe');
Route::post('agregar_publicacion_usuario_online', 'OnLine\InformeController@agregar_publicacion');
Route::get('borrar_publicacion_online/{id}', 'OnLine\InformeController@borrar_publicacion');
Route::post('editar_listados_online', 'OnLine\MonitoreoController@editar_listados');
Route::get('form_monitoreo_instalacion_online/{id}', 'OnLine\MonitoreoController@form_monitoreo_instalacion');
Route::post('editar_descarga_online', 'OnLine\MonitoreoController@editar_descarga');
Route::post('editar_instalacion_online', 'OnLine\MonitoreoController@editar_instalacion');
//Reporte
Route::resource('reporte_online','OnLine\Reportes\ReporteaplicacionController@index');
Route::resource('reporte_aplicacion_online/{tp_reporte}/{id_periodo}/{coordinador}/{fch?}/{sesion?}/','OnLine\Reportes\ReporteaplicacionController@reporte_aplicacion');
Route::get('exportar_info_aplicacion_online/{id}/excel','ExportarexcelController@exportar_info_aplicacion_online');
});