/*
 |-------------------------------------------------
 |Datatables
 |-------------------------------------------------
 */
var $form = $('#edit_form');
var $form_load = $('#edit_form_load');
$(function () {
    if (!$('#dataTableBuilder').length) return;

    $('#dataTableBuilder').addClass('table-hover');

    var table = window.LaravelDataTables["dataTableBuilder"];

    $('#dataTableBuilder').on('click', '.edit-modal', function () {
        url = (typeof editUrl === 'undefined') ? url : editUrl;
        $('#edit_modal .modal-title').html('<i class="fa fa-edit mr10"></i>Editar');
        var row = $(this).closest('tr')[0];
        var rowData = table.row(row).data();
        $('#edit_form_body').html('<input type="hidden" name="id" value="' + rowData.id + '">');
        createEditModal(rowData);
    });

    $('#dataTableBuilder').on('click', '.delete', function () {
        var id = $(this).data('id');
        url = (typeof deleteUrl === 'undefined') ? url : deleteUrl;
        bootbox.confirm("¿Seguro que desea eliminar la fila seleccionada?", function (result) {
            if (!result) return;
            $.post(url, {id: id}, function (response) {
                if (response.status == 'ok') {
                    new PNotify({
                        title: 'Correcto!',
                        text: response.message,
                        type: 'success',
                        width: '400px'
                    });
                }
                else {
                    new PNotify({
                        title: 'Error!',
                        text: response.message,
                        type: 'error',
                        width: '400px'
                    });
                }
            }).error(function () {
                new PNotify({
                    title: 'Error!',
                    text: 'Lo sentimos, ha ocurrido un error!',
                    type: 'error',
                    width: '400px'
                });
            }).always(function () {
                table.draw(false);
            });
        });
    });

    $form.submit(function (e) {
        e.preventDefault();
        $.post(url, $form.serialize(), function (response) {
            if (response.status == 'ok') {
                new PNotify({
                    title: 'Correcto!',
                    text: response.message,
                    type: 'success',
                    width: '400px'
                });
            }
            else {
                new PNotify({
                    title: 'Error!',
                    text: response.message,
                    type: 'error',
                    width: '400px'
                });
            }
            $('#edit_modal').modal('hide');
        }).error(function (response) {
            if (response.status === 422) {
                $form.find('.form-group').removeClass('has-error');
                $form.find('.help-block').remove();
                $.each(response.responseJSON, function (field, error) {
                    $form.find('input[name=' + field + ']').parent('.form-group').addClass('has-error');
                    $.each(error, function (k, v) {
                        $form.find('[name=' + field + ']').parent('.form-group').append('<p class="help-block">' + v + '</p>');
                    });
                });
            }
            else {
                new PNotify({
                    title: 'Error!',
                    text: 'Lo sentimos, ha ocurrido un error!',
                    type: 'error',
                    width: '400px'
                });
            }
        }).always(function (e) {
            table.draw(false);
        });
    });
});

/**
 * Crea un modal con los campos de la comlumna datatable seleccionada
 * @param rowData
 * @param title
 */
function createEditModal(rowData) {
    var table = window.LaravelDataTables["dataTableBuilder"];
    var columns = table.settings()[0].aoColumns;
    $.each(columns, function (k, column) {
        if (column.editable) {
            var val = '';
            if (rowData != null && column.type != 'password') val = rowData[column.data];
            var required = column.required ? 'required = "required"' : '';
            switch (column.type) {
                case 'text':
                case 'number':
                case 'password':
                case 'email':
                    var input = '<div class="col-lg-12 form-group">' +
                        '<label for="' + column.data + '" class="col-lg-3 control-label"><b>' + column.title + '</b></label>' +
                        '<div class="col-lg-9"><input type="' + column.type + '" name="' + column.data + '" ' + required + ' value="' + val + '" class="form-control" placeholder="' + column.title + '"></div>' +
                        '</div>';
                    break;
                case 'checkbox':
                    var checked = (val.indexOf('success') > -1) ? 'checked' : '';
                    var input = '<div class="col-lg-12 form-group">' +
                        '<label for="' + column.data + '" class="col-lg-3 control-label"><b>' + column.title + '</b></label>' +
                        '<div class="col-lg-9"><input type="hidden" value="0" name="' + column.data + '">' +
                        '<input type="checkbox" value="1" name="' + column.data + '" ' + checked + ' class="mt10">' +
                        '</div></div>';
                    break;
                case 'select':
                    var multiple = (typeof column.multiple === 'undefined') ? '' : 'multiple="multiple"';
                    var input = '<div class="col-lg-12 form-group">' +
                        '<label for="' + column.data + '" class="col-lg-3 control-label"><b>' + column.title + '</b></label>' +
                        '<div class="col-lg-9"><select name="' + column.name + '" ' + required + ' class="form-control" placeholder="' + column.title + '">';
                    $.each(column.source, function (k, v) {
                        var selected = '';
                        if (k === val) selected = 'selected';
                        input += '<option value="' + k + '" ' + selected + '>' + v + '</option>';
                    });
                    input += '</select></div></div>';
                    break;
                case 'select-multiple':
                    var selectedValues = val.split(', ');
                    var input = '<div class="col-lg-12 form-group">' +
                        '<label for="' + column.data + '" class="col-lg-3 control-label"><b>' + column.title + '</b></label>' +
                        '<div class="col-lg-9"><select name="' + column.name + '" ' + required + ' class="select2" multiple="multiple">';
                    $.each(column.source, function (k, v) {
                        var selected = '';
                        if ($.inArray(v, selectedValues) !== -1) selected = 'selected';
                        input += '<option value="' + k + '" ' + selected + '>' + v + '</option>';
                    });
                    input += '</select></div></div>';
                    break;
                case 'textarea':
                    var input = '<div class="col-lg-12 form-group">' +
                        '<label for="' + column.data + '" class="col-lg-3 control-label"><b>' + column.title + '</b></label>' +
                        '<div class="col-lg-9"><textarea name="' + column.data + '" ' + required + ' class="form-control" placeholder="' + column.title + '">' + val + '</textarea></div>' +
                        '</div>';
                    break;

            }
            $('#edit_form_body').append(input);
        }
    });
    $('#edit_form_body select.select2').select2({width: '100%', placeholder: 'Seleccione'});
    $('#edit_modal').modal('show');
    setTimeout(function () {
        $form.find('input:first').focus()
    }, 600);
}



/**
 * Fin Datatables edit form
 */