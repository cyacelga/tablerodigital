(function ($, DataTable) {
    "use strict";

    var _buildUrl = function (dt, action) {
        var url = dt.ajax.url() || '';
        var params = dt.ajax.params();
        params.action = action;

        return url + '?' + $.param(params);
    };

    DataTable.ext.buttons.excel = {
        className: 'buttons-excel btn btn-sm btn-default',

        text: function (dt) {
            return '<i class="fa fa-file-excel-o"></i> Excel' + dt.i18n('buttons.excel', '');
        },

        action: function (e, dt, button, config) {
            var url = _buildUrl(dt, 'excel');
            window.location = url;
        }
    };

    DataTable.ext.buttons.csv = {
        className: 'buttons-csv btn btn-sm btn-default',

        text: function (dt) {
            return '<i class="fa fa-file-excel-o"></i> csv' + dt.i18n('buttons.csv', '');
        },

        action: function (e, dt, button, config) {
            var url = _buildUrl(dt, 'csv');
            window.location = url;
        }
    };

    DataTable.ext.buttons.pdf = {
        className: 'buttons-pdf btn btn-sm btn-default',

        text: function (dt) {
            return '<i class="fa fa-file-pdf-o"></i> PDF' + dt.i18n('buttons.pdf', '');
        },

        action: function (e, dt, button, config) {
            var url = _buildUrl(dt, 'pdf');
            window.location = url;
        }
    };

    DataTable.ext.buttons.print = {
        className: 'buttons-print btn btn-sm btn-default',

        text: function (dt) {
            return '<i class="fa fa-print"></i> Imprimir' + dt.i18n('buttons.print', '');
        },

        action: function (e, dt, button, config) {
            var url = _buildUrl(dt, 'print');
            window.location = url;
        }
    };

    DataTable.ext.buttons.reset = {
        className: 'buttons-reset btn btn-sm btn-default',

        text: function (dt) {
            return '<i class="fa fa-undo"></i> Resetear' + dt.i18n('buttons.reset', '');
        },

        action: function (e, dt, button, config) {
            dt.search('').draw();
        }
    };

    DataTable.ext.buttons.reload = {
        className: 'buttons-reload btn btn-sm btn-default',

        text: function (dt) {
            return '<i class="fa fa-refresh"></i> Actualizar' + dt.i18n('buttons.reload', '');
        },

        action: function (e, dt, button, config) {
            dt.draw(false);
        }
    };

    DataTable.ext.buttons.create = {
        className: 'buttons-create btn btn-sm btn-default',

        text: function (dt) {
            return '<i class="fa fa-plus"></i> Nuevo' + dt.i18n('buttons.create', '');
        },

        action: function (e, dt, button, config) {
            window.location = window.location.href.replace(/\/+$/, "") + '/add';
        }
    };

    DataTable.ext.buttons.createModal = {
        className: 'buttons-create btn btn-sm btn-default',

        text: function (dt) {
            return '<i class="fa fa-plus-circle"></i> Nuevo' + dt.i18n('buttons.create', '');
        },

        action: function (e, dt, button, config) {
            //requiere incluir partials/edit-datatable-script
            $('#edit_form_body').html('');
            url = (typeof createUrl === 'undefined') ? url : createUrl;
            $('#edit_modal .modal-title').html('<i class="fa fa-plus mr10"></i>Nuevo');
            createEditModal(null);
        }
    };
})(jQuery, jQuery.fn.dataTable);