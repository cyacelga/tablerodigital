function cargarformulario(arg){
//funcion que carga todos los formularios del sistema
 
		if(arg==1){ var url = "form_nuevo_usuario"; }
    if(arg==2){ var url = "form_cargar_datos_usuarios";  }
    if(arg==3){ var url = "form_iniciar_proceso";  }
    if(arg==4){ var url = "reporte";  }
    if(arg==5){ var url = "reporte_validar";  }
    if(arg==6){ var url = "reporte_zonal";  }
    if(arg==7){ var url = "mapa";  }
    if(arg==8){ var url = "reporte_online";  }
		$("#contenido_principal").html($("#cargador_empresa").html());   
		$.get(url,function(resul){
      $("#contenido_principal").html(resul);
    })
        

}


function cargarlistado(listado){

    //funcion para cargar los diferentes  en general
      if(listado==1){ var url = "listado_usuarios"; }
      if(listado==2){ var url = "listado_monitoreo"; }
      if(listado==3){ var url = "listado_validacion"; }
      if(listado==4){ var url = "listado_monitoreo_online"; }
      $("#contenido_principal").html($("#cargador_empresa").html());
      $.get(url,function(resul){
        $("#contenido_principal").html(resul); 
      })

}


 $(document).on("submit",".form_entrada",function(e){

//funcion para atrapar los formularios y enviar los datos

       e.preventDefault();
        
        $('html, body').animate({scrollTop: '0px'}, 200);
        
        var formu=$(this);
        var quien=$(this).attr("id");
        var rs=false; //leccion 10
        var rst=false;
        var seccion_sel=  $("#seccion_seleccionada").val();
        var seccion_sel2=  $("#seccion_seleccionada2").val();
        var seccion_usuario2=  $("#usuario_seleccionado2").val();
        ///var id2 = "12345";  ////$(this).attr('href')
        
        if(quien=="f_nuevo_usuario"){ var varurl="agregar_nuevo_usuario"; var divresul="notificacion_resul_fanu";  rs=true;}
        if(quien=="f_editar_usuario"){ var varurl="editar_usuario"; var divresul="notificacion_resul_feu"; }
        if(quien=="f_cambiar_password"){ var varurl="cambiar_password"; var divresul="notificacion_resul_fcp";  }
        if(quien=="f_agregar_educacion"){ var varurl="agregar_educacion_usuario"; var divresul="notificacion_resul_faedu";  rs=true; }  //leccion 10
        if(quien=="f_agregar_laboratorio"){ var varurl="agregar_laboratorio";  var divresul="notificacion_resul_feu"; rst=true} 
        if(quien=="f_editar_validador"){ var varurl="editar_validador";  var divresul="notificacion_resul_feu"; rst2=true}
        if(quien=="f_editar_aplicador"){ var varurl="editar_aplicador"; rst3=true} 
        if(quien=="f_editar_descarga"){ var varurl="editar_descarga"; rst3=true}
        if(quien=="f_editar_instalacion"){ var varurl="editar_instalacion"; rst3=true} 
        if(quien=="f_editar_institucion"){ var varurl="editar_institucion"; rst3=true} 
        if(quien=="f_editar_rector"){ var varurl="editar_rector"; rst3=true} 
        if(quien=="f_editar_rector_monitoreo"){ var varurl="editar_rector_monitoreo"; rst3=true} 
        if(quien=="f_editar_listados"){ var varurl="editar_listados"; rst3=true}
        //Para aplicaión en línea
        if(quien=="f_editar_rector_monitoreo_online"){ var varurl="editar_rector_monitoreo_online"; rst3=true} 
        if(quien=="f_editar_listados_online"){ var varurl="editar_listados_online"; rst3=true}
        if(quien=="f_editar_descarga_online"){ var varurl="editar_descarga_online"; rst3=true}
        if(quien=="f_editar_instalacion_online"){ var varurl="editar_instalacion_online"; rst3=true}
   
   
        $("#"+divresul+"").html($("#cargador_empresa").html());

              $.ajax({

                    type: "POST",
                    url : varurl,
                    datatype:'json',
                    data : formu.serialize(),
                    success : function(resul){

                      notif({
                            msg: resul.message,
                            type: resul.status,
                            opacity: 1,

                            });
                      if(rst)
                      {mostrarseccionvalidacion(1);}
                      

                        $("#"+divresul+"").html(resul);
                       
                        if(rs ){
                         $('#'+quien+'').trigger("reset");
                         if(seccion_sel)
                         {
                         mostrarseccion(seccion_sel);}
                  
                        }

                    },
          error: function(xhr, textStatus, thrownError){
            notif({
            msg: "<b>¡Registro no actualizado!</b>",
            type: "error",
            opacity: 1,

            });         // $('#SaveAlert').alert('Se produjo un Error.');
          }

                }); //alert(formu)


})


$(document).on("click",".pagination li a",function(e){
//para que la pagina se cargen los elementos
 e.preventDefault();
 var url =$( this).attr("href");
 $("#contenido_principal").html($("#cargador_empresa").html());
 $.get(url,function(resul){
    $("#contenido_principal").html(resul); 
 })

})


  //leccion 7 
function mostrarficha(id_usuario) {
  //funcion para mostrar y etditar la informacion del usuario
  $("#usuario_seleccionado").val(id_usuario); // leccion10
  $("#capa_modal").show();
  $("#capa_para_edicion").show();
  var url = "form_editar_usuario/"+id_usuario+""; 
  $("#contenido_capa_edicion").html($("#cargador_empresa").html());  //leccion 10
  $.get(url,function(resul){
  $("#contenido_capa_edicion").html(resul);  //leccion 10
  })

}

$(document).on("click",".div_modal",function(e){
 //funcion para ocultar las capas modales
 $("#capa_modal").hide();
 $("#capa_para_edicion").hide();
 $("#contenido_capa_edicion").html("");  //leccion 10

})  
//js para mostrar las pestaña de lleva monitoreo
function mostrarmonitoreo(id_usuario, lab, id_sede, amie, institucion, fecha, sesion) {
  //funcion para mostrar y etditar la informacion del usuario
  $("#usuario_seleccionado1").val(id_usuario); // leccion10
  $("#usuario_lab").val(lab); // leccion10
  $("#id_sede").val(id_sede); // leccion10
  $("#codigoamei").val(amie);
  $("#fecha_aplicacion").val(fecha);
  $("#sesion_aplicacion").val(sesion);
  $("#tipo_aplicacion").val('');
  $("#descarga").show();
  $("#capa_modal1").show();
  $("#capa_para_edicion1").show();
  $("#institucionsede2").val("INSTITUCIÓN EDUCATIVA: "+institucion+" - AMIE: "+amie);
  //var url = "form_monitoreo_corresponsable/"+id_usuario+"";
  var url = "form_monitoreo_institucion/"+amie+"/"+id_sede+"/"+id_usuario+"";
  $("#contenido_capa_edicion1").html($("#cargador_empresa").html());  //leccion 10
  $.get(url,function(resul){
  $("#contenido_capa_edicion1").html(resul);  //leccion 10
  })

}

function mostrarvalidacion(id_usuario, id, amie, institucion) {
  //funcion para mostrar y etditar la informacion del usuario
  $("#usuario_seleccionado2").val(id_usuario); // leccion10
  $("#id_institucion").val(id); // leccion10
  $("#institucionsede").val("INSTITUCIÓN EDUCATIVA: "+institucion+" - AMIE: "+amie); 
  $("#capa_modal2").show();
  $("#capa_para_edicion2").show();
  ///var url = "form_validacion_aplicadores/"+id_usuario+"";
  var url = "form_institucion/"+id+"";  
  $("#contenido_capa_edicion2").html($("#cargador_empresa").html());  //leccion 10
  $.get(url,function(resul){
  $("#contenido_capa_edicion2").html(resul);  //leccion 10
  })

}


$(document).on("click",".div_modal",function(e){
 //funcion para ocultar las capas modales
 $("#capa_modal2").hide();
 $("#capa_para_edicion2").hide();
 $("#contenido_capa_edicion2").html("");  //leccion 10

})  

$(document).on("click",".div_modal",function(e){
 //funcion para ocultar las capas modales
 $("#capa_modal1").hide();
 $("#capa_para_edicion1").hide();
 $("#contenido_capa_edicion1").html("");  //leccion 10

})  


  //leccion 8 y 9
$(document).on("submit",".formarchivo",function(e){

     
        e.preventDefault();
        var formu=$(this);
        var nombreform=$(this).attr("id");
        var seccion_sel=  $("#seccion_seleccionada1").val();
        var tp_aplicacion= $("#tipo_aplicacion").val();
        var rs=false;
        if(nombreform=="f_subir_imagen" ){ var miurl="subir_imagen_usuario";  var divresul="notificacion_resul_fci";}
        if(nombreform=="f_cargar_datos_usuarios" ){ var miurl="cargar_datos_usuarios";  var divresul="notificacion_resul_fcdu"; }
        if(nombreform=="f_agregar_publicacion"+tp_aplicacion ){ var miurl="agregar_publicacion_usuario"+tp_aplicacion;   rs=true;}

        //información del formulario
        var formData = new FormData($("#"+nombreform+"")[0]);
        //hacemos la petición ajax   
        $("#"+divresul+"").html($("#cargador_empresa").html());
        $.ajax({
            url: miurl,  
            type: 'POST',
     
            // Form data
            //datos del formulario
            data: formData,
            //necesario para subir archivos via ajax
            cache: false,
            contentType: false,
            processData: false,
            //mientras enviamos el archivo
            
            //una vez finalizado correctamente
            success: function(data){
               notif({
                            msg: data.message,
                            type: data.status,
                            opacity: 1,

                            });
               
              $("#"+divresul+"").html(data);
              ///$.notify('Successfull delete customer');
              $("#fotografia_usuario").attr('src', $("#fotografia_usuario").attr('src') + '?' + Math.random() );   
              if(rs ){
                         $('#'+nombreform+'').trigger("reset");
                         mostrarseccionmonitoreo(3);
                        }           
            },
            //si ha ocurrido un error
            error: function(data){
              notif({
                width: 500,
             msg: data.message,
                            type: data.status,
                            opacity: 1,

            });
                
            }
        });
});

//leccion  10
 
function mostrarseccion(arg){
  var id_usuario=$("#usuario_seleccionado").val(); 
  $("#seccion_seleccionada").val(arg);
  if(arg==1){ var url = "form_editar_usuario/"+id_usuario+""; }
  if(arg==2){ var url = "form_educacion_usuario/"+id_usuario+""; } 
  $("#contenido_capa_edicion").html($("#cargador_empresa").html());
  $.get(url,function(resul){
  $("#contenido_capa_edicion").html(resul);
  })

}

///para cambiar la secciones del tablero de monitoreo 

function mostrarseccionmonitoreo(arg){
  var id_usuario=$("#usuario_seleccionado1").val(); 
  var id_lab=$("#usuario_lab").val(); ///// solo si los lab son unicos
  var id_sede=$("#id_sede").val();
  var amie=$("#codigoamei").val();
  var fecha=$("#fecha_aplicacion").val();
  var sesion=$("#sesion_aplicacion").val();
  var tp_aplicacion=$("#tipo_aplicacion").val();

  $("#seccion_seleccionada1").val(arg);
  if(arg==5){ var url = "form_monitoreo_aplicadores/"+amie+""; }
  if(arg==1){ var url = "form_monitoreo_corresponsable/"+id_usuario+""; }
  if(arg==2){ var url = "form_monitoreo_sustentantes"+tp_aplicacion+"/"+id_lab+"/"+amie+"/"+fecha+"/"+sesion+"/"+id_usuario+""; }
  if(arg==3){ var url = "form_monitoreo_informe"+tp_aplicacion+"/"+id_usuario+""; } 
  if(arg==4){ var url = "form_monitoreo_instalacion"+tp_aplicacion+"/"+id_usuario+""; } 
  if(arg==6){ var url = "form_monitoreo_institucion"+tp_aplicacion+"/"+amie+"/"+id_sede+"/"+id_usuario+""; } 
  $("#contenido_capa_edicion1").html($("#cargador_empresa").html());
  $.get(url,function(resul){
  $("#contenido_capa_edicion1").html(resul);
  })

}


function mostrarseccionvalidacion(arg){
  var id_usuario=$("#usuario_seleccionado2").val(); 
  var id_inst=$("#id_institucion").val(); 
  $("#seccion_seleccionada2").val(arg);
  if(arg==1){ var url = "form_validacion_laboratorio/"+id_usuario+""; }
  if(arg==2){ var url = "form_institucion/"+id_inst+""; }
  if(arg==3){ var url = "form_validacion_aplicadores/"+id_usuario+""; }
  $("#contenido_capa_edicion2").html($("#cargador_empresa").html());
  $.get(url,function(resul){
  $("#contenido_capa_edicion2").html(resul);
  })

}


function borrareducacion(arg){
var url="borrar_educacion/"+arg+"" ;
var divresul="notificacion_resul_edu";
$("#"+divresul+"").html($("#cargador_empresa").html());

$.get(url,function(resul){
  $("#"+divresul+"").html(resul);
  mostrarseccion(2);
})

}

function mostrardiv_publicaciones(arg){
  $("#info_libro").hide();
  $("#info_revista").hide();
  if(arg==5){ $("#info_libro").show(); $("#info_revista").hide();  } 
  if(arg==4){ $("#info_libro").hide(); $("#info_revista").show();  } 

}

function borrarpublicacion(arg){
var tp_aplicacion=$("#tipo_aplicacion").val();
var url="borrar_publicacion"+tp_aplicacion+"/"+arg+"" ;
var divresul="notificacion_resul_fapu";
$("#"+divresul+"").html($("#cargador_empresa").html());

$.get(url,function(resul){
  $("#"+divresul+"").html(resul);
  mostrarseccionmonitoreo(3);
  ///cargarlistado(2,1);
})
}
/////////////////////para buscar monitoreo
function buscarmonitoreo(){

  var fecha=$("#select_filtro_fecha").val();
  var dato=$("#dato_buscado").val();
      if(dato == "")
    {
    
      var url="buscar_monitoreo/"+fecha+"";
    }
    else
    {
      var url="buscar_monitoreo/"+fecha+"/"+dato+"";
    }
  
  $("#contenido_principal").html($("#cargador_empresa").html());
 $.get(url,function(resul){
    $("#contenido_principal").html(resul);  
  })

}


function buscarsustentantes(){

  var aux=$("#aux").val();
  var id_lab=$("#usuario_lab").val();
  var dato=$("#dato_sustentantes").val();
      if(dato == "")
    {
    
      var url="buscar_sustentantes/"+id_lab+"";
    }
    else
    {
      var url="buscar_sustentantes/"+id_lab+"/"+dato+"";
    }
  
  $("#contenido_capa_edicion1").html($("#cargador_empresa").html());
  $.get(url,function(resul){
  $("#contenido_capa_edicion1").html(resul);
  })

}
//// Mayreth Para el proceso SEST 12/2018
function reemplazo(id){
var valorObs=$(".reemplazoInput"+id).attr("href");

 if(valorObs === '0'){
  $(".reemplazoInput"+id).css("display","block");  
  $(".reemplazoSelect"+id).css("display","none"); 
  $(".reemplazoSelect"+id).val("");
  $(".reemplazoInput"+id).val("");
  $(".reemplazoInput"+id).attr("href", 1)
 }else{
  $(".reemplazoSelect"+id).css("display","block");   
  $(".reemplazoInput"+id).css("display","none");         
  $(".reemplazoInput"+id).val("");
  $(".reemplazoInput"+id).attr("href", 0)
 } 
}


/* PARA LA VISTA DE form_monitoreo_sustentantes_SEST19.blade.php
 
function reemplazoObservacion(id){

if($(".reemplazoSelect"+id).val() !== '' || $(".reemplazoSelect"+id).val() === null ){
     $(".reemplazoInput"+id).val($(".reemplazoSelect"+id).val());
 }else{
     $(".reemplazoInput"+id).val($(".reemplazoSelect"+id).val());
 }

}

*/

/////////////////////para buscar monitoreo en aplicación en línea.
function buscarmonitoreo_online(){

  var fecha=$("#select_filtro_fecha").val();
  var dato=$("#dato_buscado").val();
      if(dato == "")
    {
    
      var url="buscar_monitoreo_online/"+fecha+"";
    }
    else
    {
      var url="buscar_monitoreo_online/"+fecha+"/"+dato+"";
    }
  
  $("#contenido_principal").html($("#cargador_empresa").html());
 $.get(url,function(resul){
    $("#contenido_principal").html(resul);  
  })

}

//js para mostrar las pestaña de Coordinador de IE, Sustentantes y Carga de Informes para aplicación en línea
function mostrarmonitoreo_online(id_usuario, lab, id_sede, amie, institucion, fecha, sesion, tp_aplicacion) {
  $("#usuario_seleccionado1").val(id_usuario);
  $("#usuario_lab").val(lab); 
  $("#id_sede").val(id_sede); 
  $("#codigoamei").val(amie);
  $("#fecha_aplicacion").val(fecha);
  $("#sesion_aplicacion").val(sesion);
  $("#tipo_aplicacion").val(tp_aplicacion);
  $("#descarga").show();
  $("#capa_modal1").show();
  $("#capa_para_edicion1").show();
  $("#institucionsede2").val("INSTITUCIÓN EDUCATIVA: "+institucion+" - AMIE: "+amie);
  var url = "form_monitoreo_institucion_online/"+amie+"/"+id_sede+"/"+id_usuario+"";
  $("#contenido_capa_edicion1").html($("#cargador_empresa").html());  
  $.get(url,function(resul){
  $("#contenido_capa_edicion1").html(resul); 
  })

}

function buscarsustentantes_online(){
  var aux=$("#aux").val();
  var id_lab=$("#usuario_lab").val();
  var dato=$("#dato_sustentantes").val();
      if(dato == ""){
        var url="buscar_sustentantes_online/"+id_lab+"";
      }else{
        var url="buscar_sustentantes_online/"+id_lab+"/"+dato+"";
      }
  
  $("#contenido_capa_edicion1").html($("#cargador_empresa").html());
  $.get(url,function(resul){
  $("#contenido_capa_edicion1").html(resul);
  })
}