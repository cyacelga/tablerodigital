 function grafica_areazona(){
    var id_periodo=$("#periodo").val();
    var fecha=$("#fecha_programada").val();
    var sesion=$("#sesion").val();
    var options ={
        chart: {
            renderTo:'area_zona',
            type: 'areaspline'
        },
        title: {
            text: 'Sustentantes'
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 250,
            y: 200,
            floating: true,
            borderWidth: 1,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        xAxis: {
            categories: []
            //plotBands: [{ // visualize the weekend
              //  from: 4.5,
                // to: 6.5,
               // color: 'rgba(68, 170, 213, .2)'
            //}]
        },
        yAxis: {
            title: {
                text: 'Zona'
            }
        },
        tooltip: {
            shared: true,
            //valueSuffix: ' Sustentantes'
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            areaspline: {
                fillOpacity: 0.5
            }
        },
        series: [{
            name: 'Programados',
            data: []
        }, {
            name: 'Asistencia',
            data: []
        }]
    }
    
if(id_periodo !=="" && fecha ==="" && sesion ===""){
    var url = "grafica_lineal/"+id_periodo+"";
}else{
    if(id_periodo !=="" && fecha !=="" && sesion ===""){
    var url = "grafica_lineal/"+id_periodo+"/"+fecha+"";
    }else{
    var url = "grafica_lineal/"+id_periodo+"/"+fecha+"/"+sesion+"";    
    }
}

$("#area_zona").html( $("#cargador_empresa").html());

$.get(url,function(resul){
var datos= jQuery.parseJSON(resul);
var nzonas=datos.nzonas;
var zona=datos.zona;
var progzonas=datos.progzonas;
var asiszonas=datos.asiszonas;
var i=0;
    for(i=1; i<=nzonas; i++){
    
    options.series[0].data.push( progzonas[i] );
    options.series[1].data.push( asiszonas[i] );
    options.xAxis.categories.push(zona[i]);
    };
 //options.title.text="aqui e podria cambiar el titulo dinamicamente";
 chart = new Highcharts.Chart(options);
 
})  
    
}

function grafica_areaprov(){
    var id_periodo=$("#periodo").val();
    var fecha=$("#fecha_programada").val();
    var sesion=$("#sesion").val();
    
    var options ={
        chart: {
            renderTo:'area_prov',
            type: 'areaspline'
        },
        title: {
           text: 'Sustentantes'
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 250,
            y: 200,
            floating: true,
            borderWidth: 1,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        xAxis: {
            categories: []
            //plotBands: [{ // visualize the weekend
              //  from: 4.5,
                // to: 6.5,
               // color: 'rgba(68, 170, 213, .2)'
            //}]
        },
        yAxis: {
            title: {
                text: 'Provincia'
            }
        },
        tooltip: {
            shared: true,
            //valueSuffix: ' Sustentantes'
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            areaspline: {
                fillOpacity: 0.5
            }
        },
        series: [{
            name: 'Programados',
            data: []
        }, {
            name: 'Asistencia',
            data: []
        }]
    }
 
 if(id_periodo !=="" && fecha ==="" && sesion ===""){
    var url = "area_provincia/"+id_periodo+"";
}else{
    if(id_periodo !=="" && fecha !=="" && sesion ===""){
    var url = "area_provincia/"+id_periodo+"/"+fecha+"";
    }else{
    var url = "area_provincia/"+id_periodo+"/"+fecha+"/"+sesion+"";    
    }
}
    
$("#area_prov").html( $("#cargador_empresa").html() );
$.get(url,function(resul){
var datos= jQuery.parseJSON(resul);
var nprov=datos.nprov;
var provincia=datos.provincia;
var progprov=datos.progprov;
var asisprov=datos.asisprov;
var i=0;
    for(i=1; i<=nprov; i++){
    
    options.series[0].data.push( progprov[i] );
    options.series[1].data.push( asisprov[i] );
    options.xAxis.categories.push(provincia[i]);
    };
 //options.title.text="aqui e podria cambiar el titulo dinamicamente";
 chart = new Highcharts.Chart(options);
 
})  
    
} 
///////////// Index, Reporte && Reporte_provincia /////////////////////////////////
$(function grafico_area() {
    Highcharts.chart('grafico', {
        chart: {
            type: 'areaspline'
        },
        title: {
            text: 'Sustentantes'
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 250,
            y: 200,
            floating: true,
            borderWidth: 1,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        xAxis: {
            categories: ['1','2','3','4','5','6','7','8','9']
            //plotBands: [{ // visualize the weekend
              //  from: 4.5,
                // to: 6.5,
               // color: 'rgba(68, 170, 213, .2)'
            //}]
        },
        yAxis: {
            title: {
                text: 'Programados'
            }
        },
        tooltip: {
            shared: true,
            //valueSuffix: ' Sustentantes'
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            areaspline: {
                fillOpacity: 0.5
            }
        },
        series: [{
            name: 'Programados',
            data: [0, 0, 0, 0, 0, 0, 0, 0, 0]
        }, {
            name: 'Asistencia',
            data: [0, 0, 0, 0, 0, 0, 0, 0, 0]
        }]
    });
});

 google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(grafico_pastel);
      function grafico_pastel() {
        var data = google.visualization.arrayToDataTable([
          ['Zona', 'Programados'],
          ['Zona 1',  0],
          ['Zona 2',  0],
          ['Zona 3',  0],
          ['Zona 4',  0],
          ['Zona 5',  0],
          ['Zona 6',  0],
          ['Zona 7',  0],
          ['Zona 8',  0],
          ['Zona 9',  0]
        ]);

        var options = {
          title: 'Programados por Zona',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('grafico1'));
        chart.draw(data, options);
      }


