<script>
$(function(){
$("#enviar_labmonitor").click(function(){    
    $(".monitord").css("display","none");
    $(".labmonitores").css("display","block");
    document.getElementById("loading").style.display="block";
    var id_periodo = $("#periodoLab").val();
    var fecha = $("#fecha_programada").val();
    var sesion = $("#sesion").val();
    var tp_reporte = $("#tp_reporte").val();
    
    if (id_periodo === "" && fecha === "" && sesion === "") {
        alert('Debe Seleccionar un Periodo');
    }else{
        if (id_periodo !== "" && fecha === "" && sesion === "") {
                    var ur = "laboratorio/" + tp_reporte + "/" + id_periodo + "";
                } else {
                    if (id_periodo !== "" && fecha !== "" && sesion === "") {
                        var ur = "laboratorio/" + tp_reporte + "/" + id_periodo + "/" + fecha + "";
                    } else {
                        var ur = "laboratorio/" + tp_reporte + "/" + id_periodo + "/" + fecha + "/" + sesion + "";
                    }
                }
                 var url = ur;
                $.ajax({
                type: "POST",
                url: url,
                data: $("#formulariol").serialize(),
                success: function(data)
                {
                $("#laboratorio").html(data);
                document.getElementById("loading").style.display="none";
                }
                }); 
         }
   return false;
   //alert(data);
});

});

</script>
   
<div class="col-sm-12 col-md-10" style="width: auto;">
<div class="box table-responsive no-padding " style="width: auto;">
                                <div class="box-body">
                                    {!! Form::open(['method' => 'POST', 'id' => 'formulariol' ]) !!}
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 90%;">
                                        <tr style="background-color: #0489B1; color: white;">  
                                        <h4> <th style="text-align: center; vertical-align: middle;" colspan="7"><b>REPORTE LLAMADAS DE APLICADORES</b></th></h4>
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;">  
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">ZONA</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">COORDINADOR</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">MONITOR</th>
                                            <th style="text-align:center;  vertical-align: middle; padding: 3px;">PROGRAMADOS</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;" colspan="3">LLAMADAS A APLICADORES</th>
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;">
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">  </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> CONTACTADO </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> NO CONTACTADO </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> EN PROCESO </td>
                                                                                       
                                        </tr>                                        
                                        
                                            @foreach( $reporteAplicadormont as $aplicador )
                                            
                                            <?php 
                                            if( $aplicador->amie == 0){                                            
                                            $porcecontactado = number_format((($aplicador->contactado / 1) * 100), '2',',',''); 
                                            $porcenocontactado = number_format((($aplicador->nocontactado / 1) * 100), '2',',',''); 
                                            $porceenproceso = number_format((($aplicador->enproceso / 1) * 100), '2',',',''); 
                                            $porcenovedad = number_format((($aplicador->novedadllamada / 1) * 100), '2',',',''); 
                                            }else{                                           
                                            $porcecontactado = number_format((($aplicador->contactado / $aplicador->amie) * 100), '2',',','');
                                            $porcenocontactado = number_format((($aplicador->nocontactado / $aplicador->amie) * 100), '2',',','');
                                            $porceenproceso = number_format((($aplicador->enproceso / $aplicador->amie) * 100), '2',',','');
                                            $porcenovedad = number_format((($aplicador->novedadllamada / $aplicador->amie) * 100), '2',',','');
                                            
                                            }
                                            ?>
                                        <tr>             
                                             <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                                 <label>    
                                                    <input type="checkbox" id="monitor{{ $aplicador->monitor }}" value="{{ $aplicador->monitor }}" name="monitor[]">
                                                    {{ $aplicador->zona }}
                                                    </label>
                                             </td>
                                             <td style="text-align: right; vertical-align: middle; padding: 3px;">{{ $aplicador->coordinador }}</td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">
                                                    {{ $aplicador->monitor }}
                                            </td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">{{ $aplicador->amie }}</td>                                            
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php if($aplicador->contactado ==0){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">     
                                                    <?php }elseif($aplicador->contactado != $aplicador->amie) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($aplicador->contactado == $aplicador->amie) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>( {{ $aplicador->contactado }} ) {{ $porcecontactado }}%</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php if($aplicador->nocontactado ==0){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                    <?php }elseif($aplicador->nocontactado != $aplicador->amie) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($aplicador->nocontactado == $aplicador->amie) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>( {{ $aplicador->nocontactado }} ) {{ $porcenocontactado }}%</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php if($aplicador->enproceso ==0){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                    <?php }elseif($aplicador->enproceso != $aplicador->amie) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($aplicador->enproceso == $aplicador->amie) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>( {{ $aplicador->enproceso }} ) {{ $porceenproceso }}%</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>
                                                                                
                                        </tr>
                                            @endforeach
                                            <tr><input type="hidden" value="{{ $coordinador }}" name="coordinador">
                                            <td style="background-color: #0489B1; color: white;" colspan="3"><b>TOTALES</b></td>
                                             <?php
                                             if($TotalProgAmie == 0){                                            
                                            $porcecontac = ($TotalContactado / 1) ; 
                                            $porcenocontac = ($TotalNoContactado / 1) ; 
                                            $porceenproc = ($TotalEnProceso / 1) ; 
                                            $porcenoved = ($TotalNovedad / 1) ; 
                                            }else{
                                            $porcecontac = number_format((($TotalContactado / $TotalProgAmie) * 100), '2',',',''); 
                                            $porcenocontac = number_format((($TotalNoContactado / $TotalProgAmie) * 100), '2',',',''); 
                                            $porceenproc = number_format((($TotalEnProceso / $TotalProgAmie) * 100), '2',',',''); 
                                            $porcenoved = number_format((($TotalNovedad / $TotalProgAmie) * 100), '2',',',''); 
                                            }
                                             ?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalProgAmie }}</b></td>
                                            <?php if( $TotalContactado ==0){?>
                                            <td align="right" style="background-color: #FA5858; color: white;"><b>( {{ $TotalContactado }} ) {{ $porcecontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalContactado != $TotalProgAmie){?>
                                            <td align="right" style="background-color: #D7DF01; color: white;"><b>( {{ $TotalContactado }} ) {{ $porcecontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalContactado == $TotalProgAmie){?>
                                            <td align="right" style="background-color: #5FB404; color: white;"><b>( {{ $TotalContactado }} ) {{ $porcecontac }}% &nbsp;&nbsp;        
                                            <?php } ?>
                                            <?php if( $TotalNoContactado ==0){?>
                                            <td align="right" style="background-color: #5FB404; color: white;"><b>( {{ $TotalNoContactado }} ) {{ $porcenocontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalNoContactado != $TotalProgAmie){?>
                                            <td align="right" style="background-color: #D7DF01; color: white;"><b>( {{ $TotalNoContactado }} ) {{ $porcenocontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalNoContactado == $TotalProgAmie){?>
                                            <td align="right" style="background-color: #FA5858; color: white;"><b>( {{ $TotalNoContactado }} ) {{ $porcenocontac }}% &nbsp;&nbsp;        
                                            <?php } ?>        
                                            <?php if( $TotalEnProceso ==0){?>
                                            <td align="right" style="background-color: #5FB404; color: white;"><b>( {{ $TotalEnProceso }} ) {{ $porceenproc }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalEnProceso != $TotalProgAmie){?>
                                            <td align="right" style="background-color: #D7DF01; color: white;"><b>( {{ $TotalEnProceso }} ) {{ $porceenproc }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalEnProceso == $TotalProgAmie){?>
                                            <td align="right" style="background-color: #FA5858; color: white;"><b>( {{ $TotalEnProceso }} ) {{ $porceenproc }}% &nbsp;&nbsp;        
                                            <?php } ?>        
                                           
                                        </tr>
                                        <tr>
                                           <td colspan="7"> 
                                              <!-- {!! Form::submit('Para consulta de Distrito por monitor hacer Click aqui', ['class' => 'btn btn-block btn-primary', 'onclick' => 'alert(hola);']) !!} -->
                                              <button class="btn btn-block btn-info" type="submit" id="enviar_labmonitor"><b>Para consulta de Laboratorio por Monitor hacer Click aqui</b></button>
                                           </td>
                                       </tr>
                                    </table>
                                   {!! Form::close() !!}
                                </div>
                            </div>
                            </div>  