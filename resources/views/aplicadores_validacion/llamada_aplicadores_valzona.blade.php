<script>
$("#checkZona").change(function(){
    @foreach( $reporteAplicador as $llamadaapli )
   $("#zona{{ $llamadaapli->zona }}").prop('checked', $(this).prop("checked"));   
    @endforeach;
});

$(function(){
$("#enviar_zona").click(function(){
    $(".zona").css("display","none");
    $(".monitord").css("display","block");
    document.getElementById("loading").style.display="block";
    var id_periodo = $("#periodoLab").val();
    var fecha = $("#fecha_programada").val();
    var sesion = $("#sesion").val();    
    var tp_reporte = $("#tp_reporte").val();
    var coordinador = $("#coordinador").val();
            
    if (id_periodo === "" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "0") {
         alert('Debe Seleccionar un Periodo');
    }else{
        if (id_periodo !== "" && fecha === "" && sesion === "") {
                    var ur = "monitor/" + tp_reporte + "/" + id_periodo + "/" + coordinador +  "" ;
                } else {
                    if (id_periodo !== "" && fecha !== "" && sesion === "") {
                        var ur = "monitor/" + tp_reporte + "/" + id_periodo + "/" + coordinador +  "/"  + fecha + "";
                    } else {
                        var ur = "monitor/" + tp_reporte + "/" + id_periodo + "/" + coordinador +  "/"  + fecha + "/" + sesion + "";
                    }
                }
                var url = ur;
                $.ajax({
                type: "POST",
                url: url,
                data: $("#formulariom").serialize(),
                success: function(data)
                {
                $("#monitor").html(data);
                document.getElementById("loading").style.display="none";
                }
                }); 
         }
   return false;
   //alert(data);
});

});

$(function(){
   $(".azona").click(function(){
      $(".zona").css("display","block");
      $(".monitord").css("display","none");          
      $(".labmonitores").css("display","none");          
   }); 
});


$(function(){
   $(".amonitor").click(function(){
      $(".monitord").css("display","block");
      $(".labmonitores").css("display","none");          
      $(".zona").css("display","none");          
   }); 
});
</script>
<div class="zona">
    <ol class="breadcrumb">
        <li><a href="exportarLlamadas_apliValidacion/{{ $idperiodo }}/excel">
                <img src="images/excel.png" title="DESCARGAR EXCEL" style=" height: 23px; margin-right: -13px;">&nbsp; 
            </a>&nbsp; </li>
        <li class="active">Zona</li>
    </ol>
</div>
<div class="row zona">
<div class="col-lg-4 col-sm-12" style="width: auto;">
                            <div class="box table-responsive no-padding">                                
                                <div class="box-body">
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 100%;">
                                       <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >ESTADO DE LLAMADAS DE APLICADORES</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >ESTADO</th>
                                            <th style="text-align: center;">CANTIDAD</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                        </tr>
                                         @foreach( $estadollamada as $estado)
                                        <?php 
                                        if( $totalLlamada == 0){
                                            $porcenestado = number_format((($estado->totalestado / 1) * 100), '2',',','') ;
                                        }else{
                                            $porcenestado = number_format((($estado->totalestado / $totalLlamada) * 100), '2',',',''); 
                                        }
                                        ?>
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;"><b>{{ $estado->estadollamada }}</b></td>                                           
                                            <td style="text-align: right;"><b> {{ $estado->totalestado }} </b></td>                                            
                                            <?php if($porcenestado <= 45){ ?>                                            
                                            <td style="text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcenestado }} % </b></td>
                                            <?php }elseif($porcenestado > 45 && $porcenestado <= 99){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: white;"><b> {{ $porcenestado }} % </b></td>
                                            <?php }elseif($porcenestado >= 100){ ?>
                                            <td style="text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcenestado }} % </b></td>
                                            <?php } ?>
                                            
                                        </tr>                                        
                                        @endforeach
                                        <tr>
                                            <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ $totalLlamada }}</b></td>
                                            <td style=" text-align: right; background-color: #5FB404; color: white;"><b> 100,00 % </b></td>
                                       </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div>
     
</div>

<div class="row zona" style="display: block;">
   
<div class="col-sm-12 col-md-6" style="width: auto;">
<div class="box table-responsive no-padding " style="width: auto;">
                                <div class="box-body">
                                    {!! Form::open(['method' => 'POST', 'id' => 'formulariom' ]) !!}
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 90%;">
                                        <tr style="background-color: #0489B1; color: white;">  
                                        <h4> <th style="text-align: center; vertical-align: middle;" colspan="6"><b>REPORTE LLAMADAS DE APLICADORES</b></th></h4>
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;">  
                                            <th style="width: 20px; text-align:center;" >
                                            <label>
                                                <input type="checkbox" id="checkZona"> <b>ZONA</b>                                                
                                            </label>
                                            </th>
                                            <th style="text-align:center;  vertical-align: middle; padding: 3px;">PROGRAMADOS</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;" colspan="4">LLAMADAS A APLICADORES</th>
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;">
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">  </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> CONTACTADO </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> NO CONTACTADO </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> EN PROCESO </td>
                                                                                        
                                        </tr>                                        
                                        
                                            @foreach( $reporteAplicador as $aplicador )
                                            
                                            <?php 
                                            if( $aplicador->amie == 0){                                            
                                            $porcecontactado = number_format((($aplicador->contactado / 1) * 100), '2',',',''); 
                                            $porcenocontactado = number_format((($aplicador->nocontactado / 1) * 100), '2',',',''); 
                                            $porceenproceso = number_format((($aplicador->enproceso / 1) * 100), '2',',',''); 
                                            $porcenovedad = number_format((($aplicador->novedadllamada / 1) * 100), '2',',',''); 
                                            }else{                                           
                                            $porcecontactado = number_format((($aplicador->contactado / $aplicador->amie) * 100), '2',',','');
                                            $porcenocontactado = number_format((($aplicador->nocontactado / $aplicador->amie) * 100), '2',',','');
                                            $porceenproceso = number_format((($aplicador->enproceso / $aplicador->amie) * 100), '2',',','');
                                            $porcenovedad = number_format((($aplicador->novedadllamada / $aplicador->amie) * 100), '2',',','');
                                            
                                            }
                                            ?>
                                        <tr>                                            
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">
                                                    <label>    
                                                    <input type="checkbox" id="zona{{ $aplicador->zona }}" value="{{ $aplicador->zona }}" name="zona[]">
                                                    {{ $aplicador->zona }}
                                                    </label>
                                            </td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">{{ $aplicador->amie }}</td>                                            
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php if($aplicador->contactado ==0){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">     
                                                    <?php }elseif($aplicador->contactado != $aplicador->amie) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($aplicador->contactado == $aplicador->amie) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>( {{ $aplicador->contactado }} ) {{ $porcecontactado }}%</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php if($aplicador->nocontactado ==0){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                    <?php }elseif($aplicador->nocontactado != $aplicador->amie) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($aplicador->nocontactado == $aplicador->amie) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>( {{ $aplicador->nocontactado }} ) {{ $porcenocontactado }}%</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php if($aplicador->enproceso ==0){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                    <?php }elseif($aplicador->enproceso != $aplicador->amie) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($aplicador->enproceso == $aplicador->amie) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>( {{ $aplicador->enproceso }} ) {{ $porceenproceso }}%</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td
                                                                                
                                        </tr>
                                            @endforeach
                                            <tr><input type="hidden" value="{{ $coordinador }}" name="coordinador">
                                             <td style="background-color: #0489B1; color: white;"><b>TOTALES</b></td>
                                             <?php
                                             if($TotalProgAmie == 0){                                            
                                            $porcecontac = ($TotalContactado / 1) ; 
                                            $porcenocontac = ($TotalNoContactado / 1) ; 
                                            $porceenproc = ($TotalEnProceso / 1) ; 
                                            $porcenoved = ($TotalNovedad / 1) ; 
                                            }else{
                                            $porcecontac = number_format((($TotalContactado / $TotalProgAmie) * 100), '2',',',''); 
                                            $porcenocontac = number_format((($TotalNoContactado / $TotalProgAmie) * 100), '2',',',''); 
                                            $porceenproc = number_format((($TotalEnProceso / $TotalProgAmie) * 100), '2',',',''); 
                                            $porcenoved = number_format((($TotalNovedad / $TotalProgAmie) * 100), '2',',',''); 
                                            }
                                             ?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalProgAmie }}</b></td>
                                            <?php if( $TotalContactado ==0){?>
                                            <td align="right" style="background-color: #FA5858; color: white;"><b>( {{ $TotalContactado }} ) {{ $porcecontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalContactado != $TotalProgAmie){?>
                                            <td align="right" style="background-color: #D7DF01; color: white;"><b>( {{ $TotalContactado }} ) {{ $porcecontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalContactado == $TotalProgAmie){?>
                                            <td align="right" style="background-color: #5FB404; color: white;"><b>( {{ $TotalContactado }} ) {{ $porcecontac }}% &nbsp;&nbsp;        
                                            <?php } ?>
                                            <?php if( $TotalNoContactado ==0){?>
                                            <td align="right" style="background-color: #5FB404; color: white;"><b>( {{ $TotalNoContactado }} ) {{ $porcenocontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalNoContactado != $TotalProgAmie){?>
                                            <td align="right" style="background-color: #D7DF01; color: white;"><b>( {{ $TotalNoContactado }} ) {{ $porcenocontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalNoContactado == $TotalProgAmie){?>
                                            <td align="right" style="background-color: #FA5858; color: white;"><b>( {{ $TotalNoContactado }} ) {{ $porcenocontac }}% &nbsp;&nbsp;        
                                            <?php } ?>        
                                            <?php if( $TotalEnProceso ==0){?>
                                            <td align="right" style="background-color: #5FB404; color: white;"><b>( {{ $TotalEnProceso }} ) {{ $porceenproc }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalEnProceso != $TotalProgAmie){?>
                                            <td align="right" style="background-color: #D7DF01; color: white;"><b>( {{ $TotalEnProceso }} ) {{ $porceenproc }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalEnProceso == $TotalProgAmie){?>
                                            <td align="right" style="background-color: #FA5858; color: white;"><b>( {{ $TotalEnProceso }} ) {{ $porceenproc }}% &nbsp;&nbsp;        
                                            <?php } ?> 
                                            
                                        </tr>
                                        <tr>
                                           <td colspan="6"> 
                                              <!-- {!! Form::submit('Para consulta de Distrito por Zona hacer Click aqui', ['class' => 'btn btn-block btn-primary', 'onclick' => 'alert(hola);']) !!} -->
                                              <button class="btn btn-block btn-info" type="submit" id="enviar_zona"><b>Para consulta de Zona por Monitor hacer Click aqui</b></button>
                                           </td>
                                       </tr>
                                    </table>
                                   {!! Form::close() !!}
                                </div>
                            </div>
                            </div> 
                            <div class="col-lg-6 col-md-12" >
                            <div class="box">
                            <div class="box-header with-border" style="padding: 0px;">
                                <h5>&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-bar-chart"></i>&nbsp; <b> Llamada de Aplicadores por Zona</b></h5>                             
                            </div>
                            <div id="area_zona">
                
                            </div>
                            </div>    
                            </div>
                            </div>
                        <div class="monitord" style="display: none;">
                        <ol class="breadcrumb">
                        <li><a href="#">Reporte</a></li>
                        <li><a href="#" class="azona">Zona</a></li>
                        <li class="active">Monitores</li>
                        </ol>
                        </div>                        
                        <div class="row monitord" style="display: none;">                        
                        <!-- Reporte Distrito-->
                        <div id="monitor" class="col-lg-7 col-sm-12" style=" width: auto;">
                        </div>
                        </div>
                        
                        <div class="labmonitores" style="display: none;">
                        <ol class="breadcrumb">
                        <li><a href="#">Reporte</a></li>
                        <li><a href="#" class="azona">Zona</a></li>
                        <li><a href="#" class="amonitor">Monitores</a></li>
                        <li class="active">Laboratorios</li>
                        </ol>
                        </div>                        
                        <div class="row labmonitores" style="display: none;">                        
                        <!-- Reporte Distrito-->
                        <div id="laboratorio" class="col-lg-7 col-sm-12" style=" width: auto;">
                        </div>
                        </div>
                                                
 <script type="text/javascript"> 
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ['ZONA', 'CONTACTADO', 'NO CONTACTADO ', 'EN PROCESO'],
        @foreach( $reporteAplicador as $llamaapli )
        ['Zona {{ $llamaapli->zona }}', {{ $llamaapli->contactado }}, {{ $llamaapli->nocontactado }}, {{ $llamaapli->enproceso }}],
        @endforeach
      ]);

      var options = {
          
        isStacked: 'percent',
        height: 350,
        legend: { position: 'top', maxLines: 3 },
        
        vAxis: {
            minValue: 0,
            ticks: [0, .3, .6, .8, 1]
        }
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("area_zona"));
      chart.draw(data, options);
  }

</script>