<div class="row">
   
<div class="col-sm-12 col-md-10" style="width: auto;">
<div class="box table-responsive no-padding " style="width: auto;">
                                <div class="box-body">
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 90%;">
                                        <tr style="background-color: #0489B1; color: white;">  
                                        <h4> <th style="text-align: center; vertical-align: middle;" colspan="9"><b>REPORTE LABORATORIO POR MONITORE</b></th></h4>
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;">  
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">Nº</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">COORDINADOR</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">MONITOR</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">ZONA</th>
                                            <th style="text-align:center;  vertical-align: middle; padding: 3px;">AMIE</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">INSTITUCION</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">RECTOR</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">ESTADO DE LLAMADA</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">NOVEDAD DE LLAMADA</th>
                                        </tr>                                                                 
                                           <?php $i = 0;?>
                                            @foreach( $reportelabapli as $labaplicador )
                                        <?php $i = $i + 1;?>                                                
                                            <tr>                                            
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"><b> {{ $i }} </b></td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">{{ $labaplicador->coordinador }}</td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">{{ $labaplicador->monitor }}</td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">{{ $labaplicador->zona }}</td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">{{ $labaplicador->amie }}</td>
                                            <td style="text-align: left; vertical-align: middle; padding: 3px;">{{ $labaplicador->institucion }}</td>
                                            <td style="text-align: left; vertical-align: middle; padding: 3px;">{{ $labaplicador->rector }}</td>
                                            
                                            <?php if( $labaplicador->estadollamada=='CONTACTADO' ){?>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px; color: white; background-color: #5FB404;">
                                            <?php }elseif( $labaplicador->estadollamada=='NO CONTACTADO' ){?>    
                                            <td style="text-align: center; vertical-align: middle; padding: 3px; color: white; background-color: #FA5858;">
                                            <?php }elseif( $labaplicador->estadollamada=='EN PROCESO' ){?>    
                                            <td style="text-align: center; vertical-align: middle; padding: 3px; color: white; background-color: #D7DF01;">
                                            <?php } ?>
                                                {{ $labaplicador->estadollamada }}</td>
                                            <?php if( $labaplicador->novedadllamada=='' ){?>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">
                                            <?php }elseif( $labaplicador->novedadllamada!='VOLVER A LLAMAR' && $labaplicador->novedadllamada!='' ){?>    
                                            <td style="text-align: center; vertical-align: middle; padding: 3px; color: white; background-color: #FA5858;">
                                            <?php }elseif( $labaplicador->novedadllamada=='VOLVER A LLAMAR' ){?>    
                                            <td style="text-align: center; vertical-align: middle; padding: 3px; color: white; background-color: #D7DF01;">
                                            <?php } ?>
                                                {{ $labaplicador->novedadllamada }}</td>
                                        </tr>
                                        
                                         @endforeach
                                    </table>
                                </div>
                            </div>
                            </div>                                               
                                                 
                            </div>