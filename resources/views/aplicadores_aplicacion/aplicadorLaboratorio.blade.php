<script>
$(function(){
    $(".azona").click(function(){
    $(".zona").css("display", "block");
    $(".provincia").css("display", "none");
    $(".distritod").css("display", "none");
    $(".monitord").css("display", "none");
    $(".labmonitores").css("display","none"); 
    });
    });
    
$(function(){
    $(".aprovincia").click(function(){
    $(".provincia").css("display", "block");
    $(".zona").css("display", "none");
    $(".distritod").css("display", "none");
    $(".monitord").css("display", "none");          
    $(".labmonitores").css("display","none"); 
    });
});

$(function(){
    $(".adistrito").click(function(){
    $(".provincia").css("display", "none");
    $(".zona").css("display", "none");
    $(".distritod").css("display", "block");
    $(".monitord").css("display", "none");          
    $(".labmonitores").css("display","none"); 
    });
});

$(function(){
    $(".amonitor").click(function(){
    $(".provincia").css("display", "block");
    $(".zona").css("display", "none");
    $(".distritod").css("display", "none");
    $(".monitord").css("display", "none");          
    $(".labmonitores").css("display","none"); 
    });
});
</script>
<div class="row">
   
<div class="col-sm-12 col-md-10" style="width: auto;">
    <ol class="breadcrumb">
        <li><a href="exportarLlamadas_apli/{{ $idperiodo }}/excel">
                <img src="images/excel.png" title="DESCARGAR EXCEL" style=" height: 23px; margin-right: -13px;">
            </a> &nbsp; &nbsp; &nbsp;</li>
        <li><a href="#" class="azona">Zonas</a></li>
        <!-- <li><a href="#" class="aprovincia">Provincias</a></li>
        <li><a href="#" class="adistrito">Distritos</a></li> -->
        <li><a href="#" class="amonitor">Monitores</a></li>
        <li class="active">Laboratorios</li>
    </ol>
    
<div class="box table-responsive no-padding " style="width: auto;">
                                <div class="box-body">
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 90%;">
                                        <tr style="background-color: #0489B1; color: white;">  
                                        <h4> <th style="text-align: center; vertical-align: middle;" colspan="12"><b>REPORTE LLAMADAS DE APLICADORES POR LABORATORIO Y MONITOR</b></th></h4>
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;">  
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">Nº</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">MONITOR</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">DISTRITO</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">PROVINCIA</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">COORDINADOR</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">ZONA</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">INSTITUCION</th>
                                            <th style="text-align:center;  vertical-align: middle; padding: 3px;">AMIE</th>
                                            <th style="text-align:center;  vertical-align: middle; padding: 3px;">SESION</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">ESTADO DE LLAMADA</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">PROBLEMA DE LLAMADA</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">OBSERVACION</th>
                                        </tr>                                                                 
                                           <?php $i = 0;?>
                                            @foreach( $reportelabapli as $labaplicador )
                                        <?php $i = $i + 1;?>                                                
                                            <tr>                                            
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"><b> {{ $i }} </b></td>
                                            <td style="text-align: left; vertical-align: middle; padding: 3px;">{{ $labaplicador->name_monitor }}</td>
                                            <td style="text-align: left; vertical-align: middle; padding: 3px;">{{ $labaplicador->distrito_id }}</td>
                                            <td style="text-align: left; vertical-align: middle; padding: 3px;">{{ $labaplicador->provincia }}</td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"><b>{{ $labaplicador->coordinador }}</b></td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">{{ $labaplicador->zona }}</td>
                                            <td style="text-align: left; vertical-align: middle; padding: 3px;">{{ $labaplicador->institucion }}</td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">{{ $labaplicador->instituciones }}</td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">{{ $labaplicador->sesion }}</td>
                                            
                                            <?php if( $labaplicador->estadollamada=='CONTACTADO' ){?>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px; color: white; background-color: #5FB404;">
                                            <?php }elseif( $labaplicador->estadollamada=='NO CONTACTADO' ){?>    
                                            <td style="text-align: center; vertical-align: middle; padding: 3px; color: white; background-color: #FA5858;">
                                            <?php }elseif( $labaplicador->estadollamada=='EN PROCESO' ){?>    
                                            <td style="text-align: center; vertical-align: middle; padding: 3px; color: white; background-color: #D7DF01;">
                                            <?php } ?>
                                                {{ $labaplicador->estadollamada }}</td>
                                            <?php if( $labaplicador->novedadllamada=='' ){?>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">
                                            <?php }elseif( $labaplicador->novedadllamada!='VOLVER A LLAMAR' && $labaplicador->novedadllamada!='' ){?>    
                                            <td style="text-align: center; vertical-align: middle; padding: 3px; color: white; background-color: #FA5858;">
                                            <?php }elseif( $labaplicador->novedadllamada=='VOLVER A LLAMAR' ){?>    
                                            <td style="text-align: center; vertical-align: middle; padding: 3px; color: white; background-color: #D7DF01;">
                                            <?php } ?>
                                                {{ $labaplicador->novedadllamada }}</td>
                                            <td style="text-align: left; vertical-align: middle; padding: 3px;">{{ $labaplicador->observaciones }}</td>
                                        </tr>
                                        
                                         @endforeach
                                    </table>
                                </div>
                            </div>
                            </div>                                               
                                                 
                            </div>