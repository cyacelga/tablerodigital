<script>
$("#checkZona").change(function(){
    @foreach( $reporteAplicador as $llamadaapli )
   $("#zona{{ $llamadaapli->zona }}").prop('checked', $(this).prop("checked"));   
    @endforeach;
});

$(function(){
$("#enviar_zona").click(function(){    
    $(".zona").css("display","none");
    $(".provincia").css("display","block");
    document.getElementById("loading").style.display="block";
    var id_periodo = $("#periodo").val();
    var fecha = $("#fecha_programada").val();
    var sesion = $("#sesion").val();
    var tp_reporte = $("#tp_reporte").val();
    
    if (id_periodo === "" && fecha === "" && sesion === "") {
        alert('Debe Seleccionar un Periodo');
    }else{
        if (id_periodo !== "" && fecha === "" && sesion === "") {
                    var ur = "monitor_aplicacion/" + tp_reporte + "/" + id_periodo + "";
                } else {
                    if (id_periodo !== "" && fecha !== "" && sesion === "") {
                        var ur = "monitor_aplicacion/" + tp_reporte + "/" + id_periodo + "/" + fecha + "";
                    } else {
                        var ur = "monitor_aplicacion/" + tp_reporte + "/" + id_periodo + "/" + fecha + "/" + sesion + "";
                    }
                }
                 var url = ur;
                $.ajax({
                type: "POST",
                url: url,
                data: $("#formularioo").serialize(),
                success: function(data)
                {
                $("#provincia").html(data);
                document.getElementById("loading").style.display="none";
                }
                }); 
         }
   return false;
   //alert(data);
});

});


</script>
<div class="zona">
        <ol class="breadcrumb">
                    <li><a href="exportarLlamadas_apli/{{ $idperiodo }}/excel">
                    <img src="images/excel.png" title="DESCARGAR EXCEL" style=" height: 23px; margin-right: -13px;">
                    </a> &nbsp; &nbsp; &nbsp;</li> 
                    <li><a href="#">Zonal</a></li> 
                    </ol>
</div>
<div class="row zona">
<div class="col-lg-4 col-sm-12" style="width: auto;">
                            <div class="box table-responsive no-padding">
                                <div class="box-body">
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 100%;">
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >{{ $name_periodo }}</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >ESTADO DE LLAMADAS A APLICADORES</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >ESTADO</th>
                                            <th style="text-align: center;">CANTIDAD</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                        </tr>
                                         @foreach( $estadollamada as $estado)
                                        <?php 
                                        if( $totalLlamada == 0){
                                            $porcenestado = number_format((($estado->totalestado / 1) * 100), '2',',','') ;
                                        }else{
                                            $porcenestado = number_format((($estado->totalestado / $totalLlamada) * 100), '2',',',''); 
                                        }
                                        ?>
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;"><b>{{ $estado->estadollamada }}</b></td>                                           
                                            <td style="text-align: right;"><b> {{ $estado->totalestado }} </b></td>                                            
                                            <?php if($porcenestado <= 45){ ?>                                            
                                            <td style="text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcenestado }} % </b></td>
                                            <?php }elseif($porcenestado > 45 && $porcenestado <= 99){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: white;"><b> {{ $porcenestado }} % </b></td>
                                            <?php }elseif($porcenestado >= 100){ ?>
                                            <td style="text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcenestado }} % </b></td>
                                            <?php } ?>
                                            
                                        </tr>                                        
                                        @endforeach
                                        <tr>
                                            <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ $totalLlamada }}</b></td>
                                            <td style=" text-align: right; background-color: #5FB404; color: white;"><b> 100,00 % </b></td>
                                       </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div>
    <div class="col-lg-4 col-sm-12" style="width: auto;">
                            <div class="box table-responsive no-padding">
                                <div class="box-body">
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 100%;">
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >{{ $name_periodo }}</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >PROBLEMA DE LLAMADAS A APLICADORES</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >NOVEDAD</th>
                                            <th style="text-align: center;">CANTIDAD</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                        </tr>
                                         @foreach( $novedadllamada as $novedad)
                                        <?php 
                                        if( $TotalNovLlamada == 0){
                                            $porcennovedad = number_format((($novedad->totalnovedad / 1) * 100), '2',',','') ;
                                        }else{
                                            $porcennovedad = number_format((($novedad->totalnovedad / $TotalNovLlamada) * 100), '2',',',''); 
                                        }
                                        ?>
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;"><b>{{ $novedad->novedadllamada }}</b></td>                                           
                                            <td style="text-align: right;"><b> {{ $novedad->totalnovedad }} </b></td>                                            
                                            <?php if($porcennovedad <= 45){ ?>                                            
                                            <td style="text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcennovedad }} % </b></td>
                                            <?php }elseif($porcennovedad > 45 && $porcennovedad <= 99){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: white;"><b> {{ $porcennovedad }} % </b></td>
                                            <?php }elseif($porcennovedad >= 100){ ?>
                                            <td style="text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcennovedad }} % </b></td>
                                            <?php } ?>
                                            
                                        </tr>                                        
                                        @endforeach
                                        <tr>
                                            <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ $TotalNovLlamada }}</b></td>
                                            <td style=" text-align: right; background-color: #5FB404; color: white;"><b> 100,00 % </b></td>
                                       </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div>
</div>


<div class="row zona" style="display: block;">
   
<div class="col-sm-12 col-md-10" style="width: auto;">
<div class="box table-responsive no-padding " style="width: auto;">
                                <div class="box-body">
                                    {!! Form::open(['method' => 'POST', 'id' => 'formularioo' ]) !!}
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 90%;">
                                        <tr style="background-color: #0489B1; color: white;">  
                                        <h4> <th style="text-align: center; vertical-align: middle;" colspan="12"><b>REPORTE DE LLAMADAS DE APLICADORES POR ZONA</b></th></h4>
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;">  
                                            <th style="width: 20px; text-align:center;" >
                                            <label>
                                                <input type="checkbox" id="checkZona"> <b>ZONA</b>                                                
                                            </label>
                                            </th>
                                            <th style="text-align:center;  vertical-align: middle; padding: 3px;">PROGRAMADOS</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;" colspan="4">LLAMADAS A APLICADORES</th>
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;">
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">  </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> CONTACTADO </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> NO CONTACTADO </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> EN PROCESO </td>
                                            <!-- <td style="text-align: center; vertical-align: middle; padding: 3px;"> PROBLEMA DE LLAMADA </td> -->
                                            
                                        </tr>                                        
                                        
                                            @foreach( $reporteAplicador as $aplicador )
                                            
                                            <?php 
                                            if( $aplicador->instituciones == 0){                                            
                                            $porcecontactado = number_format((($aplicador->contactado / 1) * 100), '2',',',''); 
                                            $porcenocontactado = number_format((($aplicador->nocontactado / 1) * 100), '2',',',''); 
                                            $porceenproceso = number_format((($aplicador->enproceso / 1) * 100), '2',',',''); 
                                            $porcenovedad = number_format((($aplicador->novedadllamada / 1) * 100), '2',',',''); 
                                            }else{                                           
                                            $porcecontactado = number_format((($aplicador->contactado / $aplicador->instituciones) * 100), '2',',','');
                                            $porcenocontactado = number_format((($aplicador->nocontactado / $aplicador->instituciones) * 100), '2',',','');
                                            $porceenproceso = number_format((($aplicador->enproceso / $aplicador->instituciones) * 100), '2',',','');
                                            $porcenovedad = number_format((($aplicador->novedadllamada / $aplicador->instituciones) * 100), '2',',','');
                                            
                                            }
                                            ?>
                                        <tr>                                            
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">
                                                    <label>    
                                                    <input type="checkbox" id="zona{{ $aplicador->zona }}" value="{{ $aplicador->zona }}" name="zona_d[]">
                                                    {{ $aplicador->zona }}
                                                    </label>
                                            </td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">{{ $aplicador->instituciones }}</td>                                            
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php if($aplicador->contactado ==0){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">     
                                                    <?php }elseif($aplicador->contactado != $aplicador->instituciones) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($aplicador->contactado == $aplicador->instituciones) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>( {{ $aplicador->contactado }} ) {{ $porcecontactado }}%</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php if($aplicador->nocontactado ==0){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                    <?php }elseif($aplicador->nocontactado != $aplicador->instituciones) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($aplicador->nocontactado == $aplicador->instituciones) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>( {{ $aplicador->nocontactado }} ) {{ $porcenocontactado }}%</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php if($aplicador->enproceso ==0){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                    <?php }elseif($aplicador->enproceso != $aplicador->instituciones) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($aplicador->enproceso == $aplicador->instituciones) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>( {{ $aplicador->enproceso }} ) {{ $porceenproceso }}%</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>
                                           <!-- <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php /* if($aplicador->novedadllamada == $aplicador->nocontactado){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                    <?php }elseif($aplicador->novedadllamada != $aplicador->nocontactado) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">    
                                                    <?php } */ ?>        
                                                        <span class="skill"><i class="val"><b>( {{ $aplicador->novedadllamada }} ) {{ $porcenovedad }}%</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td> -->                                          
                                        </tr>
                                            @endforeach
                                            <tr><input type="hidden" value="{{ $coordinador }}" name="coordinador">
                                             <td style="background-color: #0489B1; color: white;"><b>TOTALES</b></td>
                                             <?php
                                             if($TotalInst == 0){                                            
                                            $porcecontac = ($TotalContactado / 1) ; 
                                            $porcenocontac = ($TotalNoContactado / 1) ; 
                                            $porceenproc = ($TotalEnProceso / 1) ; 
                                            $porcenoved = ($TotalNovedad / 1) ; 
                                            }else{
                                            $porcecontac = number_format((($TotalContactado / $TotalInst) * 100), '2',',',''); 
                                            $porcenocontac = number_format((($TotalNoContactado / $TotalInst) * 100), '2',',',''); 
                                            $porceenproc = number_format((($TotalEnProceso / $TotalInst) * 100), '2',',',''); 
                                            $porcenoved = number_format((($TotalNovedad / $TotalInst) * 100), '2',',',''); 
                                            }
                                             ?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalInst }}</b></td>
                                            <?php if( $TotalContactado ==0){?>
                                            <td align="right" style="background-color: #FA5858; color: white;"><b>( {{ $TotalContactado }} ) {{ $porcecontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalContactado != $TotalLab){?>
                                            <td align="right" style="background-color: #D7DF01; color: white;"><b>( {{ $TotalContactado }} ) {{ $porcecontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalContactado == $TotalLab){?>
                                            <td align="right" style="background-color: #5FB404; color: white;"><b>( {{ $TotalContactado }} ) {{ $porcecontac }}% &nbsp;&nbsp;        
                                            <?php } ?>
                                            <?php if( $TotalNoContactado ==0){?>
                                            <td align="right" style="background-color: #5FB404; color: white;"><b>( {{ $TotalNoContactado }} ) {{ $porcenocontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalNoContactado != $TotalLab){?>
                                            <td align="right" style="background-color: #D7DF01; color: white;"><b>( {{ $TotalNoContactado }} ) {{ $porcenocontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalNoContactado == $TotalLab){?>
                                            <td align="right" style="background-color: #FA5858; color: white;"><b>( {{ $TotalNoContactado }} ) {{ $porcenocontac }}% &nbsp;&nbsp;        
                                            <?php } ?>        
                                            <?php if( $TotalEnProceso ==0){?>
                                            <td align="right" style="background-color: #5FB404; color: white;"><b>( {{ $TotalEnProceso }} ) {{ $porceenproc }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalEnProceso != $TotalLab){?>
                                            <td align="right" style="background-color: #D7DF01; color: white;"><b>( {{ $TotalEnProceso }} ) {{ $porceenproc }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalEnProceso == $TotalLab){?>
                                            <td align="right" style="background-color: #FA5858; color: white;"><b>( {{ $TotalEnProceso }} ) {{ $porceenproc }}% &nbsp;&nbsp;        
                                            <?php } ?>        
                                            <?php /* if( $TotalNovedad == $TotalNoContactado){?>
                                           <!-- <td align="right" style="background-color: #5FB404; color: white;"><b>( {{ $TotalNovedad }} ) {{ $porcenoved }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalNovedad != $TotalNoContactado){?>
                                            <td align="right" style="background-color: #D7DF01; color: white;"><b>( {{ $TotalNovedad }} ) {{ $porcenoved }}% &nbsp;&nbsp;    -->  
                                            <?php } */ ?>        
                                            
                                        </tr>
                                        <tr>
                                           <td colspan="12"> 
                                              <!-- {!! Form::submit('Para consulta de Distrito por Zona hacer Click aqui', ['class' => 'btn btn-block btn-primary', 'onclick' => 'alert(hola);']) !!} -->
                                              <button class="btn btn-block btn-info" type="submit" id="enviar_zona"><b>Para consulta de Zona por Monitor hacer Click aqui</b></button>
                                           </td>
                                       </tr>
                                    </table>
                                   {!! Form::close() !!}
                                </div>
                            </div>
                            </div>                                               
                                                 
                            </div>
                         
                                              
                         <!-- Reporte Provincia-->
                                                 <div class="row provincia" style="display: none;">                       
                                                     <div id="provincia" class="col-lg-7 col-sm-12" style=" width: auto;">                            
                                                     </div>
                                                 </div>                     
                                                 <!-- Reporte Distrito-->                    
                                                 <div class="row distritod" style="display: none;">                        
                                                     <div id="distrito" class="col-lg-12 col-sm-12">

                                                     </div>
                                                 </div> 
                                                 <!-- Reporte Monitor--> 
                                                 <div class="row monitord" style="display: none;">
                                                     <div id="monitor" class="col-sm-12">
                                                     </div>
                                                 </div>
                                                 <!-- Reporte Laboratorios-->
                                                 <div class="row labmonitores" style="display: none;"> 
                                                     <div id="laboratorio" class="col-lg-7 col-sm-12" style=" width: auto;">
                                                     </div>
                                                 </div>