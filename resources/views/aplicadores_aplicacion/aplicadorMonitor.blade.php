<script>
$("#checkMonitor").change(function(){
    var i =0;
    @foreach( $reporteAplicadormont as $monit )
    i = i + 1;
   $("#monitor"+i).prop('checked', $(this).prop("checked"));
    $("#distritom"+i).prop('checked', $(this).prop("checked"));
    $("#provinciam"+i).prop('checked', $(this).prop("checked"));
    $("#coordinadorm"+i).prop('checked', $(this).prop("checked"));
    $("#zonasm"+i).prop('checked', $(this).prop("checked"));
    @endforeach;
});

function selectDistrito(i){
    
    $("#monitor"+i).change(function(){
    $("#distritom"+i).prop('checked', $(this).prop("checked"));
    $("#provinciam"+i).prop('checked', $(this).prop("checked"));
    $("#coordinadorm"+i).prop('checked', $(this).prop("checked"));
    $("#zonasm"+i).prop('checked', $(this).prop("checked"));
    });
};

$(function(){
$("#enviar_labmonitor").click(function(){    
     $(".provincia").css("display","none");
    $(".labmonitores").css("display","block");
    document.getElementById("loading").style.display="block";
    var id_periodo = $("#periodo").val();
    var fecha = $("#fecha_programada").val();
    var sesion = $("#sesion").val();
    var tp_reporte = $("#tp_reporte").val();
    
    if (id_periodo === "" && fecha === "" && sesion === "") {
        alert('Debe Seleccionar un Periodo');
    }else{
        if (id_periodo !== "" && fecha === "" && sesion === "") {
                    var ur = "laboratorio_aplicacion/" + tp_reporte + "/" + id_periodo + "";
                } else {
                    if (id_periodo !== "" && fecha !== "" && sesion === "") {
                        var ur = "laboratorio_aplicacion/" + tp_reporte + "/" + id_periodo + "/" + fecha + "";
                    } else {
                        var ur = "laboratorio_aplicacion/" + tp_reporte + "/" + id_periodo + "/" + fecha + "/" + sesion + "";
                    }
                }
                 var url = ur;
                $.ajax({
                type: "POST",
                url: url,
                data: $("#formulariom").serialize(),
                success: function(data)
                {
                $("#laboratorio").html(data);
                document.getElementById("loading").style.display="none";
                }
                }); 
         }
   return false;
   //alert(data);
});

});

$(function(){
    $(".azona").click(function(){
    $(".zona").css("display", "block");
    $(".provincia").css("display", "none");
    $(".distritod").css("display", "none");
    $(".monitord").css("display", "none");          
    $(".labmonitores").css("display","none"); 
    });
    });
    
$(function(){
    $(".aprovincia").click(function(){
    $(".provincia").css("display", "block");
    $(".zona").css("display", "none");
    $(".distritod").css("display", "none");
    $(".monitord").css("display", "none");          
    $(".labmonitores").css("display","none"); 
    });
});

$(function(){
    $(".adistrito").click(function(){
    $(".provincia").css("display", "none");
    $(".zona").css("display", "none");
    $(".distritod").css("display", "block");
    $(".monitord").css("display", "none");          
    $(".labmonitores").css("display","none"); 
    });
});
</script>
                        
    <ol class="breadcrumb">
        <li><a href="exportarLlamadas_apli/{{ $idperiodo }}/excel">
                <img src="images/excel.png" title="DESCARGAR EXCEL" style=" height: 23px; margin-right: -13px;">
            </a> &nbsp; &nbsp; &nbsp;</li>
        <li><a href="#" class="azona">Zonas</a></li>
        <!-- <li><a href="#" class="aprovincia">Provincias</a></li>
        <li><a href="#" class="adistrito">Distritos</a></li> -->
        <li class="active">Monitores</li>
    </ol>
    
<div class="box table-responsive no-padding " style="width: auto;">
                                <div class="box-body">
                                    {!! Form::open(['method' => 'POST', 'id' => 'formulariom' ]) !!}
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 90%;">
                                        <tr style="background-color: #0489B1; color: white;">  
                                        <h4> <th style="text-align: center; vertical-align: middle;" colspan="10"><b>REPORTE LLAMADAS DE APLICADORES</b></th></h4>
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;">  
                                             <th style="text-align: left;  vertical-align: middle; padding: 3px;">
                                                &nbsp;&nbsp; <input type="checkbox" id="checkMonitor"> MONITOR
                                            </th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">DISTRITO</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">PROVINCIA</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">COORDINADOR</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">ZONA</th>
                                            <th style="text-align:center;  vertical-align: middle; padding: 3px;">PROGRAMADOS</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;" colspan="4">LLAMADAS A APLICADORES</th>
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;">
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">  </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">  </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">  </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> CONTACTADO </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> NO CONTACTADO </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> EN PROCESO </td>
                                           <!-- <td style="text-align: center; vertical-align: middle; padding: 3px;"> PROBLEMA DE LLAMADA </td> -->
                                            
                                        </tr>                                        
                                        <?php $i = 0;?>
                                            @foreach( $reporteAplicadormont as $aplicador )
                                            
                                            <?php 
                                            $i++;
                                            
                                            if( $aplicador->instituciones == 0){                                            
                                            $porcecontactado = number_format((($aplicador->contactado / 1) * 100), '2',',',''); 
                                            $porcenocontactado = number_format((($aplicador->nocontactado / 1) * 100), '2',',',''); 
                                            $porceenproceso = number_format((($aplicador->enproceso / 1) * 100), '2',',',''); 
                                            $porcenovedad = number_format((($aplicador->novedadllamada / 1) * 100), '2',',',''); 
                                            }else{                                           
                                            $porcecontactado = number_format((($aplicador->contactado / $aplicador->instituciones) * 100), '2',',','');
                                            $porcenocontactado = number_format((($aplicador->nocontactado / $aplicador->instituciones) * 100), '2',',','');
                                            $porceenproceso = number_format((($aplicador->enproceso / $aplicador->instituciones) * 100), '2',',','');
                                            $porcenovedad = number_format((($aplicador->novedadllamada / $aplicador->instituciones) * 100), '2',',','');
                                            
                                            }
                                            ?>
                                        <tr>                                            
                                            <td style="text-align: left; vertical-align: middle; padding: 3px;">
                                                    <label>
                                                        <input type="checkbox" id="monitor{{ $i }}" value="{{ $aplicador->monitor }}" name="monitor[]" onclick="selectDistrito({{ $i }});">
                                                        {{ $aplicador->name_monitor }}
                                                    </label>
                                            </td> 
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">
                                                    <label>    
                                                        <input type="checkbox" id="distritom{{ $i }}" value="{{ $aplicador->distrito_id }}" name="distrito_m[]" hidden>
                                                    {{ $aplicador->distrito_id }}
                                                    </label>
                                            </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">
                                                    <label>    
                                                        <input type="checkbox" id="provinciam{{ $i }}" value="{{ $aplicador->provincia }}" name="provincia_m[]" hidden>
                                                    {{ $aplicador->provincia }}
                                                    </label>
                                            </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">
                                                    <label>    
                                                        <input type="checkbox" id="coordinadorm{{ $i }}" value="{{ $aplicador->coordinador }}" name="coordinador_m[]" hidden>
                                                    {{ $aplicador->coordinador }}
                                                    </label>
                                            </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">
                                                    <label>    
                                                        <input type="checkbox" id="zonasm{{ $i }}" value="{{ $aplicador->zona }}" name="zona_m[]" hidden>
                                                    {{ $aplicador->zona }}
                                                    </label>
                                            </td>                                           
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">{{ $aplicador->instituciones }}</td>                                            
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php if($aplicador->contactado ==0){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">     
                                                    <?php }elseif($aplicador->contactado != $aplicador->laboratorio) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($aplicador->contactado == $aplicador->laboratorio) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>( {{ $aplicador->contactado }} ) {{ $porcecontactado }}%</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php if($aplicador->nocontactado ==0){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                    <?php }elseif($aplicador->nocontactado != $aplicador->laboratorio) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($aplicador->nocontactado == $aplicador->laboratorio) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>( {{ $aplicador->nocontactado }} ) {{ $porcenocontactado }}%</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php if($aplicador->enproceso ==0){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                    <?php }elseif($aplicador->enproceso != $aplicador->laboratorio) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($aplicador->enproceso == $aplicador->laboratorio) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>( {{ $aplicador->enproceso }} ) {{ $porceenproceso }}%</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>
                                            <!-- <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php /* if($aplicador->novedadllamada == $aplicador->nocontactado){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                    <?php }elseif($aplicador->novedadllamada != $aplicador->nocontactado) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">    
                                                    <?php } */ ?>        
                                                        <span class="skill"><i class="val"><b>( {{ $aplicador->novedadllamada }} ) {{ $porcenovedad }}%</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td> -->                                           
                                        </tr>
                                            @endforeach
                                            <tr>
                                            <td style="background-color: #0489B1; color: white;" colspan="5"><b>TOTALES</b></td>
                                             <?php
                                             if($TotalInst == 0){                                            
                                            $porcecontac = ($TotalContactado / 1) ; 
                                            $porcenocontac = ($TotalNoContactado / 1) ; 
                                            $porceenproc = ($TotalEnProceso / 1) ; 
                                            $porcenoved = ($TotalNovedad / 1) ; 
                                            }else{
                                            $porcecontac = number_format((($TotalContactado / $TotalInst) * 100), '2',',',''); 
                                            $porcenocontac = number_format((($TotalNoContactado / $TotalInst) * 100), '2',',',''); 
                                            $porceenproc = number_format((($TotalEnProceso / $TotalInst) * 100), '2',',',''); 
                                            $porcenoved = number_format((($TotalNovedad / $TotalInst) * 100), '2',',',''); 
                                            }
                                             ?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalInst }}</b></td>
                                            <?php if( $TotalContactado ==0){?>
                                            <td align="right" style="background-color: #FA5858; color: white;"><b>( {{ $TotalContactado }} ) {{ $porcecontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalContactado != $TotalLab){?>
                                            <td align="right" style="background-color: #D7DF01; color: white;"><b>( {{ $TotalContactado }} ) {{ $porcecontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalContactado == $TotalLab){?>
                                            <td align="right" style="background-color: #5FB404; color: white;"><b>( {{ $TotalContactado }} ) {{ $porcecontac }}% &nbsp;&nbsp;        
                                            <?php } ?>
                                            <?php if( $TotalNoContactado ==0){?>
                                            <td align="right" style="background-color: #5FB404; color: white;"><b>( {{ $TotalNoContactado }} ) {{ $porcenocontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalNoContactado != $TotalLab){?>
                                            <td align="right" style="background-color: #D7DF01; color: white;"><b>( {{ $TotalNoContactado }} ) {{ $porcenocontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalNoContactado == $TotalLab){?>
                                            <td align="right" style="background-color: #FA5858; color: white;"><b>( {{ $TotalNoContactado }} ) {{ $porcenocontac }}% &nbsp;&nbsp;        
                                            <?php } ?>        
                                            <?php if( $TotalEnProceso ==0){?>
                                            <td align="right" style="background-color: #5FB404; color: white;"><b>( {{ $TotalEnProceso }} ) {{ $porceenproc }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalEnProceso != $TotalLab){?>
                                            <td align="right" style="background-color: #D7DF01; color: white;"><b>( {{ $TotalEnProceso }} ) {{ $porceenproc }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalEnProceso == $TotalLab){?>
                                            <td align="right" style="background-color: #FA5858; color: white;"><b>( {{ $TotalEnProceso }} ) {{ $porceenproc }}% &nbsp;&nbsp;        
                                            <?php } ?>        
                                            <?php /* if( $TotalNovedad == $TotalNoContactado){?>
                                            <!-- <td align="right" style="background-color: #5FB404; color: white;"><b>( {{ $TotalNovedad }} ) {{ $porcenoved }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalNovedad != $TotalNoContactado){?>
                                            <td align="right" style="background-color: #D7DF01; color: white;"><b>( {{ $TotalNovedad }} ) {{ $porcenoved }}% &nbsp;&nbsp;  -->    
                                            <?php } */ ?>        
                                            
                                        </tr>
                                        <tr>
                                           <td colspan="10"> 
                                              <!-- {!! Form::submit('Para consulta de Distrito por monitor hacer Click aqui', ['class' => 'btn btn-block btn-primary', 'onclick' => 'alert(hola);']) !!} -->
                                              <button class="btn btn-block btn-info" type="submit" id="enviar_labmonitor"><b>Para consulta de Monitor por Laboratorio hacer Click aqui</b></button>
                                           </td>
                                       </tr>
                                    </table>
                                   {!! Form::close() !!}
                                </div>
                            </div>