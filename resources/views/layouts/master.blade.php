<html>
    <head>
        <title>@yield('title')</title>
    </head>
    <body>
        @section('sidebar')
            <nav class="navbar navbar-default navbar-fixed-up">
				<div class="container">
					<img src="/images/ineval_logo_mini.png" >
				</div>
			</nav>
			
		    <nav class="navbar navbar-default navbar-fixed-bottom">
				<div class="container">
					<a class="navbar-brand" href="#">Todos los derechos reservados Ineval 2016</a>
				</div>
			</nav>
        @show

        <div class="container">
            @yield('content')
        </div>
    </body>
</html>