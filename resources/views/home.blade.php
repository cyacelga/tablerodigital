<script type="text/javascript">
$(document).ready(function(){
  if(2== <?php echo Auth::user()->institucion_periodo_id; ?>)
  {
$("#1").hide();
$("#3").hide();
  }
    
    
  }
  
});
</script>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sistema | Panel Control</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
     <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="css/pe-icon-7-stroke.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

     <link rel="stylesheet" href="css/sistemalaravel.css">

     <link rel="stylesheet" type="text/css" href="css/notifIt.css">

<link href="toggle/bootstrap-toggle.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
   <style type='text/css'>
    #loading{
    width:100%;
    height:100%;
                background-color:#CCC;
    position:fixed;
    top:0;
    left:0;
    z-index:9999;
    opacity: 0.8;
    filter: alpha(opacity=80);
    }
    </style>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>Dact</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Tablero</b>Monitoreo</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
            
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?=  $usuario->image;  ?>"  alt="User Image"  style="width:20px;height:20px;">
                  <span class="hidden-xs"><?=  $usuario->first_name;  ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                      <?php if($usuario->image==""){ $usuario->image="imagenes/avatar.jpg"; }  ?>
                      <img src="<?=  $usuario->image;  ?>"  alt="User Image"  style="width:50px;height:50px;">
                    <p>
                     <?=  $usuario->first_name;  ?>
                      <small>Member since Nov. 2012</small>
                    </p>
                  </li>
                  <!-- Menu Body
                  <li class="user-body">
                    <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>
                  </li>
                  Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="#" onclick="mostrarficha(<?= $usuario->id; ?>);" class="btn btn-default btn-flat">Perfil</a>
                    </div> 
                    <div class="pull-right">
                      <a href="logout" class="btn btn-default btn-flat">Salir</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>
            </ul>
          </div>
        </nav>


      </header>
      <!-- Left side column. contains the logo and sidebar -->
      
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
                <?php if($usuario->image==""){ $usuario->image="imagenes/avatar.jpg"; }  ?>
              <img src="<?=  $usuario->image;  ?>"  alt="User Image"  style="width:50px;height:50px;">
            </div>
            <div class="pull-left info">
              <p>Usuario: <?=  $usuario->first_name;  ?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MENÚ</li>
          <?php if (Auth::user()->user_group_id==2 or Auth::user()->user_group_id==0 or Auth::user()->user_group_id==3){ ?>
            <li id=3 class="actsive treeview" style="display: none">
           <?php }else{ ?>
           <li id=3 class="actsive treeview">
           <?php } ?> 
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Panel de control</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="javascript:void(0);" onclick="cargarformulario(1);" ><i class="fa fa-fw fa-user-plus"></i>Agregar usuario </a></li>
                <li class="active"><a href="javascript:void(0);" onclick="cargarlistado(1,1);" ><i class="fa fa-circle-o"></i>Listado Usuarios</a></li>
                <li class="active"><a href="javascript:void(0);" onclick="cargarformulario(2);" ><i class="fa fa-circle-o"></i>Cargar Datos Us.</a></li>
                <li class="active"><a href="javascript:void(0);" onclick="cargarformulario(3);" ><i class="fa fa-circle-o"></i>Iniciar proceso.</a></li>
              </ul>
            </li>  
            <?php if (Auth::user()->user_group_id==2 or Auth::user()->user_group_id==3){ ?>
            <li id=1 class="actsive treeview" style="display: none">
           <?php }else{ ?>
           <li id=1 class="actsive treeview">
           <?php } ?> 
              <a href="#">
                <i class="fa fa-fw fa-tablet"></i> <span>Monitoreo</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
              <li class="active"><a href="javascript:void(0);" onclick="cargarlistado(3);" ><i class="fa fa-circle-o"></i>Tablero Validación. </a></li>
              <li class="active"><a href="javascript:void(0);" onclick="cargarlistado(2);" ><i class="fa fa-circle-o"></i>Tablero Aplicación offLine. </a></li> 
              <li class="active"><a href="javascript:void(0);" onclick="cargarlistado(4);" ><i class="fa fa-circle-o"></i>Tablero Aplicación onLine. </a></li>                
              </ul>
            </li> 
           
            <li id=2 class="actsive treeview" <?php if (Auth::user()->user_group_id==0 or Auth::user()->user_group_id==3){ echo "style='display: none'";}?> >
              <a href="#">
                <i class="fa fa-fw fa-tablet"></i> <span>Reporteria</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="javascript:void(0);" onclick="cargarformulario(5);" ><i class="fa fa-circle-o"></i>Reportes Validacion. </a></li>
             
              <!--<ul class="treeview-menu">
                <li class="active"><a href="javascript:void(0);" onclick="cargarformulario(4);" ><i class="fa fa-circle-o"></i>Reportes Aplicación. </a></li>
              </ul>-->
          
                <li class="active"><a href="javascript:void(0);" onclick="cargarformulario(6);" ><i class="fa fa-circle-o"></i>Reportes Aplicación offLine. </a></li>
                  <li class="active"><a href="javascript:void(0);" onclick="cargarformulario(8);" ><i class="fa fa-circle-o"></i>Reportes Aplicación onLine. </a></li>
                 <li class="active"><a href="javascript:void(0);" onclick="cargarformulario(7);" ><i class="fa fa-circle-o"></i>Reportes Georeferencia. </a></li>
              </ul>
            </li>   


          
          </ul>




        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper" style="min-height:2000px !important;">
        <!-- Content Header (Page header) -->
        <section class="content-header"> 
          <h1>
            Escritorio
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

         <!-- contenido capas modales -->

            <section> 
                     <div id="capa_modal" class="div_modal" ></div>
                     <div id="capa_para_edicion" class="div_contenido" > 
                      <input type="hidden" id="usuario_seleccionado" value="0"  />
                      <input type="hidden" id="seccion_seleccionada" value="0"  />

                      <div class="margin"  id="botones_control" >
                                  <button type="button" class="btn btn-primary" onclick="mostrarseccion(1);" >Información</button>
                                  <button type="button" class="btn btn-primary" onclick="mostrarseccion(2);" >Educación</button>
                                  
                                 
                      </div>
                      <div  id="contenido_capa_edicion" ></div>
            </section>

            <!-- contenido capas modales monitoreo -->

            <section> 
                     <div id="capa_modal1" class="div_modal" ></div>
                     <div id="capa_para_edicion1" class="div_contenido" > 
                      <input type="hidden" id="usuario_seleccionado1" value="0"  />
                      <input type="hidden" id="seccion_seleccionada1" value="0"  />
                      <input type="hidden" id="usuario_lab" value="0"  />
                      <input type="hidden" id="codigoamei" value="0"  />
                      <input type="hidden" id="tipo_aplicacion" value=""  />
                      
                      <div class="box-header">
                          <h3 class="box-title">
                            <input type="text" disabled id="institucionsede2" style="width:1000px;" /></h3>
                        </div>
                      <div class="margin"  id="botones_control1" >
                      <button type="button" class="btn btn-primary" onclick="mostrarseccionmonitoreo(6);">Coordinador de IE</button>
                      <!--<button type="button" class="btn btn-primary" onclick="mostrarseccionmonitoreo(5);" >Aplicadores</button>
                      <button type="button" class="btn btn-primary" onclick="mostrarseccionmonitoreo(1);" >Aplicador - Rector</button>-->
                      <button type="button" class="btn btn-primary" id="descarga" onclick="mostrarseccionmonitoreo(4);">Descarga e Instalación de Laboratorios</button>
                      <!--<button type="button" class="btn btn-primary" onclick="mostrarseccionmonitoreo(4);">Recepción de listados de asistencia</button>  -->                    
                      <button type="button" class="btn btn-primary" onclick="mostrarseccionmonitoreo(2);" >Sustentantes</button>
                      <button type="button" class="btn btn-primary" onclick="mostrarseccionmonitoreo(3);" >Carga de informes</button>
                      </div>
                      <div  id="contenido_capa_edicion1" ></div>
            </section>

            <!-- contenido capas modales monitoreo -->

            <section> 

                     <div id="capa_modal2" class="div_modal" ></div>
                     <div id="capa_para_edicion2" class="div_contenido" > 
                      <input type="hidden" id="usuario_seleccionado2" value="0"  />
                      <input type="hidden" id="seccion_seleccionada2" value="0"  />
                      <input type="hidden" id="usuario_lab" value="0"  />
                      <input type="hidden" id="id_sede" value="0"  />
                       <input type="hidden" id="id_institucion" value="0"  />
                       <input type="hidden" id="fecha_aplicacion" value="0"  />
                       <input type="hidden" id="sesion_aplicacion" value="0"  />
                        <div class="box-header">
                          <h3 class="box-title">
                            <input type="text" id="institucionsede" style="width:1000px;" /></h3>
                        </div><!-- /.box-header -->
                      <div class="margin"  id="botones_control2" >
                      <button type="button" class="btn btn-primary" onclick="mostrarseccionvalidacion(2);" >Coordinador de la IE</button>
                     <button type="button" class="btn btn-primary" onclick="mostrarseccionvalidacion(1);" >Laboratorio</button>
                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>          
                                 
                      </div>
                      <div  id="contenido_capa_edicion2" ></div>
            </section>


   
       

        <!-- contenido principal -->
        <section class="content"  id="contenido_principal">        
        </section>
    
      <!-- cargador empresa -->
        <div style="display: none;" id="cargador_empresa" align="center">
            <br>
         

         <label style="color:#FFF; background-color:#ABB6BA; text-align:center">&nbsp;&nbsp;&nbsp;Espere... &nbsp;&nbsp;&nbsp;</label>

         <img src="imagenes/cargando.gif" align="middle" alt="cargador"> &nbsp;<label style="color:#ABB6BA">Realizando tarea solicitada ...</label>

          <br>
         <hr style="color:#003" width="50%">
         <br>
       </div>

  



      </div><!-- /.content-wrapper -->
     


     
    </div><!-- ./wrapper -->



     <script src="js/jquery.min.js"></script>
       <script type="text/javascript" src="js/loader.js"></script>
    <script src="js/exporting.js"></script>
    <!-- <script type="text/javascript" src="js/loader.js"></script> -->
    <script src="js/highcharts.js"></script>
    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

    <script>  $("#content-wrapper").css("min-height","2000px"); </script>
    
    <!--dagi
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>-->
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="plugins/morris/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="plugins/knob/jquery.knob.js"></script>
    <script src="js/jquery.js"></script><script src="toggle/bootstrap-toggle.min.js"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>



   <script type="text/javascript" src="js/notifIt.js"></script>
 
 <!-- javascript del sistema laravel -->
   <script src="js/sistemalaravel.js"></script>


<?php if (Auth::user()->user_group_id==2 or Auth::user()->user_group_id==3){ ?>
            <script>cargarformulario(6);</script>
           <?php }else{ ?>
           <script>cargarlistado(2);</script>
           <?php } ?> 
   


  </body>
</html>
<script>
window.tiempoRestante = 120 //2 minutos
 
function contador(){
     window.tiempoRestante--;//saco 1 segundo
     if(tiempoRestante==0){
          window.location.href="otrapagina.html"; //redirecciono
     }else{
          setTimeOut("contador();",1000);//recursion en 1 segundo
     }
}
 
function hayActividad(){
     window.tiempoRestante=120;//reinicio a 2 minutos
}
</script>
