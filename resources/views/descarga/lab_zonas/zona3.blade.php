<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
        <title>index</title>     
        <?php include '../resources/views/descarga/Graficas/ColorDistrito.php'; ?>

        <script type="text/javascript">

            $(function () {
                //http://www.switchonthecode.com/tutorials/xml-parsing-with-jquery
                $.ajax({
                    type: 'POST',
                    url: 'images/dagi/xmlMapas/zonal/zona3.xml', // .svg renamed .xml for IE support
                    dataType: 'xml',
                    success: function (xml) {
                        var r = Raphael('canvas_d', 560, 580);
                        var map = {};
                        var map_set = r.set();
                        var active_fill = 'gold';
                        var active_stroke = 'white';
                        var normal_fill = $('body').css('color');
                        var normal_stroke = '';
                        var active = null;

                        var colorC = ["#004899"];
                        var colorB = ["#FFFFFF"];
                        var colorD = ["#FFFFFF"];

                        $(xml).find('path').each(function () {
                            var id = (String)($(this).attr('id'));
                            var path = (String)($(this).attr('d'));
                            var tasa = (String)($(this).attr('tasa'));
                            var colors = {
                                zona: '#C6C6C5',
                            <?php echo coloresDistrito(3,$id_periodo,$fecha,$sesion); ?>                                
                            };
                            map[id] = r.path(path)
                                    .attr({fill: colors[id], stroke: normal_stroke})
                                    .drag(
                                            // hacer el drag como con los circulos no funciona muy bien para los paths
                                                    // ya que los dx son usados cada vez
                                                            // aqui calculo el diferencial continuamente

                                                                    function (dx, dy) {// move
                                                                        this.translate(dx - this.dx, dy - this.dy);
                                                                        this.dx = dx;
                                                                        this.dy = dy;
                                                                        //$('#test').html(dx+'--'+dy);
                                                                    },
                                                                    function (ox, oy) {// start
                                                                        //this.ox = ox;
                                                                        //this.oy = oy;
                                                                        this.dx = 0;
                                                                        this.dy = 0;
                                                                        this.toFront();
                                                                        this.attr({opacity: .5});
                                                                        //$('#test').html(ox+'-'+oy);
                                                                    },
                                                                    function () {// up
                                                                        // regresa a la posición original
                                                                        this.translate(-this.dx, -this.dy);
                                                                        this.attr({opacity: 1});

                                                                        // Este bloque se hacia en click() pero mejor aqui para que tambien funcione en IE
                                                                        // restablecer activo previo
                                                                        if (active) {
                                                                            active.animate({fill: colorD}, 500, '>');
                                                                        }
                                                                        colorD = colors[id];
                                                                        // activar actual
                                                                        active = this;
                                                                        active.animate({fill: colors[id], opacity: 1}, 500, '>'); //celeste
                                                                        if (active) {
                                                                            active.animate({fill: colorC, stroke: normal_stroke}, 500, '>');
                                                                        }
                                                                        // ocultar otras info
                                                                        $('.info').hide();
                                                                        // mostrar info actual
                                                                        $('#' + id).show().css('background-color', colorB); //celeste

                                                                    }
                                                            )
                                                                    .hover(function () {
                                                                        this.color = colorC; //azul
                                                                        if (this != active) {
                                                                            this.animate({fill: "#878786", stroke: active_stroke}); //turquesa+                                                                        

                                                                        }
                                                                    }, function () {
                                                                        if (this != active) {
                                                                            this.animate({fill: colors[id], stroke: normal_stroke});
                                                                        }
                                                                    })

                                                            map_set.push(map[id]);
                                                        });// end each
                                            } // end success
                                });

            });
            
             $(function(){
   $(".azona").click(function(){
      $(".zona").css("display","block");
      $(".provincia").css("display", "none");
      $(".distritod").css("display", "none");
      $(".monitord").css("display", "none");          
      $(".labmonitores").css("display","none");          
   }); 
});
            
        </script>
    </head>
    <body>
        <div id="wrapper">
            <div class="container">
              <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                      <div class="box box-primary">
                          <div class="box-header">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title">Mapa Ecuador</h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -6px;">
                          <div class="box-body" style="height: 484px;">
                              <div id="canvas_d" style="margin-top: -70px;"></div>  
                          </div>
                      </div>
                  </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" id="infobox">
                    <div class="info" style="display:block;">
                      <div class="box box-primary" style="height: 540px; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 3</h2>
                              <h3>Distrito: D06D04</h3>                                                
                              </h3>
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -20px;">
                          <div class="box-body">
                        <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=06D04&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                                        
                    </div>
                    <div id="D06D04" class="info">
           <div class="box box-primary" style="height: 540px; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 3</h2>
                              <h3>Distrito: 06D04</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=06D04&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                 
                    </div>
                    <div id="D18D04" class="info">
          <div class="box box-primary" style="height: 540px; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 3</h2>
                              <h3>Distrito: 18D04</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=18D04&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                    
                    </div>
                    <div id="D05D05" class="info">
          <div class="box box-primary" style="height: 540px; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 3</h2>
                              <h3>Distrito: 05D05</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=05D05&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                     
                    </div>
                    <div id="D18D02" class="info">
          <div class="box box-primary" style="height: 540px; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 3</h2>
                              <h3>Distrito: 18D02</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=18D02&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                     
                    </div>
                    <div id="D05D01" class="info">
          <div class="box box-primary" style="height: 540px; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 3</h2>
                              <h3>Distrito: 05D01</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=05D01&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                     
                    </div>
                    <div id="D18D06" class="info">
          <div class="box box-primary" style="height: 540px; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 3</h2>
                              <h3>Distrito: 18D06</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=18D06&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                    
                    </div>
                    <div id="D06D01" class="info">
          <div class="box box-primary" style="height: 540px; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 3</h2>
                              <h3>Distrito: 06D01</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=06D01&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>
                    </div>
                    <div id="D18D01" class="info">
          <div class="box box-primary" style="height: 540px; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 3</h2>
                              <h3>Distrito: 18D01</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=18D01&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                   
                    </div>
                    <div id="D06D02" class="info">
          <div class="box box-primary" style="height: 540px; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 3</h2>
                              <h3>Distrito: 06D02</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=06D02&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                  
                    </div>
                    <div id="D18D03" class="info">
          <div class="box box-primary" style="height: 540px; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 3</h2>
                              <h3>Distrito: 18D03</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=18D03&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                   
                    </div>
                    <div id="D05D03" class="info">
          <div class="box box-primary" style="height: 540px; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 3</h2>
                              <h3>Distrito: 05D03</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=05D03&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                    
                    </div>
                    <div id="D16D02" class="info">
          <div class="box box-primary" style="height: 540px; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 3</h2>
                              <h3>Distrito: 16D02</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=16D02&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                   
                    </div>
                    <div id="D05D04" class="info">
          <div class="box box-primary" style="height: 540px; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 3</h2>
                              <h3>Distrito: 05D04</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=05D04&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                   
                    </div>
                    <div id="D18D05" class="info">
          <div class="box box-primary" style="height: 540px; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 3</h2>
                              <h3>Distrito: 18D05</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=18D05&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                    
                    </div>
                    <div id="D06D05" class="info">
          <div class="box box-primary" style="height: 540px; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 3</h2>
                              <h3>Distrito: 06D05</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=06D05&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                     
                    </div>
                    <div id="D06D03" class="info">
          <div class="box box-primary" style="height: 540px; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 3</h2>
                              <h3>Distrito: 06D03</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=06D03&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                     
                    </div>
                    <div id="D05D02" class="info">
          <div class="box box-primary" style="height: 540px; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                             <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 3</h2>
                              <h3>Distrito: 05D02</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=05D02&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                   
                    </div>
                    <div id="D05D06" class="info">
          <div class="box box-primary" style="height: 540px; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 3</h2>
                              <h3>Distrito: 05D06</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=05D06&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                    
                    </div>
                    <div id="D16D01" class="info">
          <div class="box box-primary" style="height: 540px; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 3</h2>
                              <h3>Distrito: 16D01</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=16D01&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                    
                    </div>
                </div>
              </div>
            </div>
        </div>
    </body>
</html>
