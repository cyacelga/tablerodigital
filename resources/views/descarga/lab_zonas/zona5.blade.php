<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
        <title>index</title>    
        <?php include '../resources/views/descarga/Graficas/ColorDistrito.php'; ?>

        <script type="text/javascript">

            $(function () {
                //http://www.switchonthecode.com/tutorials/xml-parsing-with-jquery
                $.ajax({
                    type: 'POST',
                    url: 'images/dagi/xmlMapas/zonal/zona5.xml', // .svg renamed .xml for IE support
                    dataType: 'xml',
                    success: function (xml) {
                        var r = Raphael('canvas_d', 560, 580);
                        var map = {};
                        var map_set = r.set();
                        var active_fill = 'gold';
                        var active_stroke = 'white';
                        var normal_fill = $('body').css('color');
                        var normal_stroke = '';
                        var active = null;

                        var colorC = ["#004899"];
                        var colorB = ["#FFFFFF"];
                        var colorD = ["#FFFFFF"];

                        $(xml).find('path').each(function () {
                            var id = (String)($(this).attr('id'));
                            var path = (String)($(this).attr('d'));
                            var tasa = (String)($(this).attr('tasa'));
                            var colors = {
                                zona: '#C6C6C5',
                            <?php echo coloresDistrito(5,$id_periodo,$fecha,$sesion); ?>                                
                            };
                            map[id] = r.path(path)
                                    .attr({fill: colors[id], stroke: normal_stroke})
                                    .drag(
                                            // hacer el drag como con los circulos no funciona muy bien para los paths
                                                    // ya que los dx son usados cada vez
                                                            // aqui calculo el diferencial continuamente

                                                                    function (dx, dy) {// move
                                                                        this.translate(dx - this.dx, dy - this.dy);
                                                                        this.dx = dx;
                                                                        this.dy = dy;
                                                                        //$('#test').html(dx+'--'+dy);
                                                                    },
                                                                    function (ox, oy) {// start
                                                                        //this.ox = ox;
                                                                        //this.oy = oy;
                                                                        this.dx = 0;
                                                                        this.dy = 0;
                                                                        this.toFront();
                                                                        this.attr({opacity: .5});
                                                                        //$('#test').html(ox+'-'+oy);
                                                                    },
                                                                    function () {// up
                                                                        // regresa a la posición original
                                                                        this.translate(-this.dx, -this.dy);
                                                                        this.attr({opacity: 1});

                                                                        // Este bloque se hacia en click() pero mejor aqui para que tambien funcione en IE
                                                                        // restablecer activo previo
                                                                        if (active) {
                                                                            active.animate({fill: colorD}, 500, '>');
                                                                        }
                                                                        colorD = colors[id];
                                                                        // activar actual
                                                                        active = this;
                                                                        active.animate({fill: colors[id], opacity: 1}, 500, '>'); //celeste
                                                                        if (active) {
                                                                            active.animate({fill: colorC, stroke: normal_stroke}, 500, '>');
                                                                        }
                                                                        // ocultar otras info
                                                                        $('.info').hide();
                                                                        // mostrar info actual
                                                                        $('#' + id).show().css('background-color', colorB); //celeste

                                                                    }
                                                            )
                                                                    .hover(function () {
                                                                        this.color = colorC; //azul
                                                                        if (this != active) {
                                                                            this.animate({fill: "#878786", stroke: active_stroke}); //turquesa+                                                                        

                                                                        }
                                                                    }, function () {
                                                                        if (this != active) {
                                                                            this.animate({fill: colors[id], stroke: normal_stroke});
                                                                        }
                                                                    })

                                                            map_set.push(map[id]);
                                                        });// end each
                                            } // end success
                                });

            });
            
            $(function(){
   $(".azona").click(function(){
      $(".zona").css("display","block");
      $(".provincia").css("display", "none");
      $(".distritod").css("display", "none");
      $(".monitord").css("display", "none");          
      $(".labmonitores").css("display","none");          
   }); 
});
        </script>
    </head>
    <body>
        <div id="wrapper">
            <div class="container">
              <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                      <div class="box box-primary">
                          <div class="box-header">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title">Mapa Ecuador</h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -6px;">
                          <div class="box-body" style="height: 484px;">
                              <div id="canvas_d" style="margin-top: -70px;"></div>  
                          </div>
                      </div>
                  </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" id="infobox">
                    <div class="info" style="display:block;"> 
                      <div class="box box-primary" style="height: 540px; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a></h3>
                              <h2 style="margin-top: -16px;">Zona: 5</h2>
                              <h3>Distrito: D02D02</h3>                                                
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -20px;">
                          <div class="box-body">
                        <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=02D02&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                                        
                    </div>
                    <div id="D02D02" class="info">
          <div class="box box-primary" style="height: 540; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 5</h2>
                              <h3>Distrito: 02D02</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=02D02&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                 
                    </div>
                    <div id="D09D14" class="info">
          <div class="box box-primary" style="height: 540; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 5</h2>
                              <h3>Distrito: 09D14</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=09D14&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                   
                    </div>
                    <div id="D12D04" class="info">
          <div class="box box-primary" style="height: 540; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 5</h2>
                              <h3>Distrito: 12D04</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=12D04&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                      
                    </div>
                    <div id="D09D22" class="info">
          <div class="box box-primary" style="height: 540; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 5</h2>
                              <h3>Distrito: 09D22</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=09D22&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                    
                    </div>
                    <div id="D09D20" class="info">
          <div class="box box-primary" style="height: 540; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 5</h2>
                              <h3>Distrito: 09D20</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=09D20&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                    
                    </div>
                    <div id="D09D18" class="info">
          <div class="box box-primary" style="height: 540; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 5</h2>
                              <h3>Distrito: 09D18</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=09D18&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                      
                    </div>
                    <div id="D20D01" class="info">
          <div class="box box-primary" style="height: 540; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 5</h2>
                              <h3>Distrito: 20D01</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=20D01&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                   
                    </div>
                    <div id="D24D01" class="info">
          <div class="box box-primary" style="height: 540; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 5</h2>
                              <h3>Distrito: 24D01</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=24D01&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                      
                    </div>
                    <div id="D09D13" class="info">
          <div class="box box-primary" style="height: 540; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 5</h2>
                              <h3>Distrito: 09D13</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=09D13&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                      
                    </div>
                    <div id="D09D11" class="info">
          <div class="box box-primary" style="height: 540; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 5</h2>
                              <h3>Distrito: 09D11</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=09D11&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                     
                    </div>
                    <div id="D02D01" class="info">
          <div class="box box-primary" style="height: 540; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 5</h2>
                              <h3>Distrito: 02D01</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=02D01&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                      
                    </div>
                    <div id="D09D21" class="info">
          <div class="box box-primary" style="height: 540; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 5</h2>
                              <h3>Distrito: 09D21</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=09D21&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                      
                    </div>
                    <div id="D09D17" class="info">
          <div class="box box-primary" style="height: 540; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 5</h2>
                              <h3>Distrito: 09D17</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=09D17&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                      
                    </div>
                    <div id="D09D19" class="info">
          <div class="box box-primary" style="height: 540; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 5</h2>
                              <h3>Distrito: 09D19</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=09D19&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                      
                    </div>
                    <div id="D12D06" class="info">
          <div class="box box-primary" style="height: 540; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 5</h2>
                              <h3>Distrito: 12D06</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=12D06&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                     
                    </div>
                    <div id="D09D15" class="info">
          <div class="box box-primary" style="height: 540; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 5</h2>
                              <h3>Distrito: 09D15</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=09D15&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                     
                    </div>
                    <div id="D24D02" class="info">
          <div class="box box-primary" style="height: 540; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 5</h2>
                              <h3>Distrito: 24D02</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=24D02&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                     
                    </div>
                    <div id="D12D03" class="info">
          <div class="box box-primary" style="height: 540; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 5</h2>
                              <h3>Distrito: 12D03</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=12D03&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                     
                    </div>
                    <div id="D09D12" class="info">
          <div class="box box-primary" style="height: 540; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 5</h2>
                              <h3>Distrito: 09D12</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=09D12&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                     
                    </div>
                    <div id="D12D02" class="info">
          <div class="box box-primary" style="height: 540; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 5</h2>
                              <h3>Distrito: 12D02</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=12D02&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                     
                    </div>
                    <div id="D12D05" class="info">
          <div class="box box-primary" style="height: 540; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 5</h2>
                              <h3>Distrito: 12D05</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=12D05&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                      
                    </div>
                    <div id="D02D04" class="info">
          <div class="box box-primary" style="height: 540; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 5</h2>
                              <h3>Distrito: 02D04</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=02D04&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                     
                    </div>
                    <div id="D02D03" class="info">
          <div class="box box-primary" style="height: 540; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 5</h2>
                              <h3>Distrito: 02D03</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=02D03&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                      
                    </div>
                    <div id="D09D16" class="info">
          <div class="box box-primary" style="height: 540; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 5</h2>
                              <h3>Distrito: 09D16</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=09D16&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                     
                    </div>
                    <div id="D12D01" class="info">
          <div class="box box-primary" style="height: 540; width: 625px;">
                          <div class="box-header" style="margin-top: -36px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title"><a class="boton azona" title="Regresar" href="#">Regresar</a><br>
                              <h2 style="margin-top: -16px;">Zona: 5</h2>
                              <h3>Distrito: 12D01</h3>
                              </h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/descarga/Graficas/LabDistrito.php?provincia=12D01&periodo={{$id_periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>                     
                          </div>
                      </div>                      
                    </div>
                </div>
              </div>
            </div>
        </div>
    </body>
</html>
