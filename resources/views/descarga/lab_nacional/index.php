<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
        <title>Ecuador</title>

        <script src="../../js/jquery-1.4.2.min.js" type="text/javascript"></script>        
        <script src="../../js/raphael.js" type="text/javascript"></script>
        <script src="../../js/g.raphael.js" type="text/javascript"></script>        
        <script src="../../js/jquery.js" type="text/javascript"></script> 
        <script src="../../js/bootstrap.min.js" type="text/javascript"></script>                
        <link href="../../css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/estilo.css" rel="stylesheet" type="text/css"/>      


        <script type="text/javascript">

            $(function () {
                //http://www.switchonthecode.com/tutorials/xml-parsing-with-jquery
                $.ajax({
                    type: 'POST',
                    url: '../../images/xmlMapas/Provincial/ecuador.xml', // .svg renamed .xml for IE support
                    dataType: 'xml',
                    success: function (xml) {
                        var r = Raphael('canvas', 560, 580);
                        var map = {};
                        var map_set = r.set();
                        var active_fill = 'gold';
                        var active_stroke = 'white';
                        var normal_fill = $('body').css('color');
                        var normal_stroke = '';
                        var active = null;

                        var colorC = ["#004899"];
                        var colorB = ["#FFFFFF"];
                        var colorD = ["#FFFFFF"];

                        $(xml).find('path').each(function () {
                            var id = (String)($(this).attr('id'));
                            var path = (String)($(this).attr('d'));
                            var tasa = (String)($(this).attr('tasa'));
                            var colors = {
                                <?php include '../Graficas/ColorProvincias.php'; ?>                              
                                
                                
                            };

                            map[id] = r.path(path)
                                    .attr({fill: colors[id], stroke: normal_stroke})
                                    .drag(
                                            // hacer el drag como con los circulos no funciona muy bien para los paths
                                                    // ya que los dx son usados cada vez
                                                            // aqui calculo el diferencial continuamente

                                                                    function (dx, dy) {// move
                                                                        this.translate(dx - this.dx, dy - this.dy);
                                                                        this.dx = dx;
                                                                        this.dy = dy;
                                                                        //$('#test').html(dx+'--'+dy);
                                                                    },
                                                                    function (ox, oy) {// start
                                                                        //this.ox = ox;
                                                                        //this.oy = oy;
                                                                        this.dx = 0;
                                                                        this.dy = 0;
                                                                        this.toFront();
                                                                        this.attr({opacity: .5});
                                                                        //$('#test').html(ox+'-'+oy);
                                                                    },
                                                                    function () {// up
                                                                        // regresa a la posición original
                                                                        this.translate(-this.dx, -this.dy);
                                                                        this.attr({opacity: 1});

                                                                        // Este bloque se hacia en click() pero mejor aqui para que tambien funcione en IE
                                                                        // restablecer activo previo
                                                                        if (active) {
                                                                            active.animate({fill: colorD}, 500, '>');
                                                                        }
                                                                        colorD = colors[id];
                                                                        // activar actual
                                                                        active = this;
                                                                        active.animate({fill: colors[id], opacity: 1}, 500, '>'); //celeste
                                                                        if (active) {
                                                                            active.animate({fill: colorC}, 500, '>');
                                                                        }
                                                                        // ocultar otras info
                                                                        $('.info').hide();
                                                                        // mostrar info actual
                                                                        $('#' + id).show().css('background-color', colorB); //celeste

                                                                    }
                                                            )
                                                                    .hover(function () {
                                                                        this.color = colorC; //azul
                                                                        if (this != active) {
                                                                            this.animate({fill: "#878786", stroke: active_stroke}); //turquesa+                                                                        

                                                                        }
                                                                    }, function () {
                                                                        if (this != active) {
                                                                            this.animate({fill: colors[id]});
                                                                        }
                                                                    })

                                                            map_set.push(map[id]);
                                                        });// end each
                                            } // end success
                                });

            });
        </script>
    </head>
    <body>

        <div id="wrapper">
            <div class="container">
              <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" id="canvas"></div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" id="infobox">
                    <div class="info" style="display:block;">
                        <h3>Provincia: Manabí</h3>   
                        <iframe scrolling="no" src="../Graficas/LabProvincias.php?provincia=MANABI" frameborder="0" height="400" width="600" ></iframe>
                    </div>
                    <div id="morona_santiago" class="info">   
                        <h3>Provincia: Morona Santiago</h3>  
                        <iframe scrolling="no" src="../Graficas/LabProvincias.php?provincia=MORONA SANTIAGO" frameborder="0" height="400" width="600" ></iframe>
                    </div>
                    <div id="pastaza" class="info">                        
                        <h3>Provincia: Pastaza</h3>    
                        <iframe scrolling="no" src="../Graficas/LabProvincias.php?provincia=PASTAZA" frameborder="0" height="400" width="600" ></iframe>
                    </div>
                    <div id="orellana" class="info">                        
                        <h3>Provincia: Orellana</h3>  
                        <iframe scrolling="no" src="../Graficas/LabProvincias.php?provincia=ORELLANA" frameborder="0" height="400" width="600" ></iframe>
                    </div>
                    <div id="sucumbios" class="info">                        
                        <h3>Provincia: Sucumbíos</h3>
                        <iframe scrolling="no" src="../Graficas/LabProvincias.php?provincia=SUCUMBIOS" frameborder="0" height="400" width="600" ></iframe>
                    </div>
                    <div id="napo" class="info">                        
                        <h3>Provincia: Napo</h3>    
                        <iframe scrolling="no" src="../Graficas/LabProvincias.php?provincia=NAPO" frameborder="0" height="400" width="600" ></iframe>
                    </div>
                    <div id="cotopaxi" class="info">                        
                        <h3>Provincia: Cotopaxi</h3>     
                        <iframe scrolling="no" src="../Graficas/LabProvincias.php?provincia=COTOPAXI" frameborder="0" height="400" width="600" ></iframe>
                    </div>
                    <div id="tungurahua" class="info">                        
                        <h3>Provincia: Tungurahua</h3>    
                        <iframe scrolling="no" src="../Graficas/LabProvincias.php?provincia=TUNGURAHUA" frameborder="0" height="400" width="600" ></iframe>
                    </div>
                    <div id="chimborazo" class="info">                        
                        <h3>Provincia: Chimborazo</h3>   
                        <iframe scrolling="no" src="../Graficas/LabProvincias.php?provincia=CHIMBORAZO" frameborder="0" height="400" width="600" ></iframe>
                    </div>
                    <div id="bolivar" class="info">                        
                        <h3>Provincia: Bolívar</h3>  
                        <iframe scrolling="no" src="../Graficas/LabProvincias.php?provincia=BOLIVAR" frameborder="0" height="400" width="600" ></iframe>
                    </div>
                    <div id="los_rios" class="info">                        
                        <h3>Provincia: Los Ríos</h3> 
                        <iframe scrolling="no" src="../Graficas/LabProvincias.php?provincia=LOS RIOS" frameborder="0" height="400" width="600" ></iframe>
                    </div>
                    <div id="manabi" class="info">  
                        <h3>Provincia: Manabí</h3>                                                 
                        <iframe scrolling="no" src="../Graficas/LabProvincias.php?provincia=MANABI" frameborder="0" height="400" width="600" ></iframe>                       
                    </div>
                    <div id="santa_elena" class="info"> 
                        <h3>Provincia: Santa Elena</h3>    
                        <iframe scrolling="no" src="../Graficas/LabProvincias.php?provincia=SANTA ELENA" frameborder="0" height="400" width="600" ></iframe>
                    </div>
                    <div id="el_oro" class="info">                        
                        <h3>Provincia: El Oro</h3>     
                        <iframe scrolling="no" src="../Graficas/LabProvincias.php?provincia=EL ORO" frameborder="0" height="400" width="600" ></iframe>
                    </div>
                    <div id="loja" class="info">                        
                        <h3>Provincia: Loja</h3>       
                        <iframe scrolling="no" src="../Graficas/LabProvincias.php?provincia=LOJA" frameborder="0" height="400" width="600" ></iframe>
                    </div>
                    <div id="zamora_chinchipe" class="info">                        
                        <h3>Provincia: Zamora Chinchipe</h3>      
                        <iframe scrolling="no" src="../Graficas/LabProvincias.php?provincia=ZAMORA CHINCHIPE" frameborder="0" height="400" width="600" ></iframe>
                    </div>
                    <div id="cañar" class="info">                        
                        <h3>Provincia: Cañar</h3>             
                        <iframe scrolling="no" src="../Graficas/LabProvincias.php?provincia=CAÑAR" frameborder="0" height="400" width="600" ></iframe>
                    </div>
                    <div id="santo_domingo_de_los_tsachilas" class="info">
                        <h3>Provincia: Santo Domingo de los Tsáchilas</h3>
                        <iframe scrolling="no" src="../Graficas/LabProvincias.php?provincia=SANTO DOMINGO DE LOS TSACHILAS" frameborder="0" height="400" width="600" ></iframe>
                    </div>
                    <div id="esmeraldas" class="info">   
                        <h3>Provincia: Esmeraldas</h3>
                        <iframe scrolling="no" src="../Graficas/LabProvincias.php?provincia=ESMERALDAS" frameborder="0" height="400" width="600" ></iframe>
                    </div>
                    <div id="imbabura" class="info">                        
                        <h3>Provincia: Imbabura</h3> 
                        <iframe scrolling="no" src="../Graficas/LabProvincias.php?provincia=IMBABURA" frameborder="0" height="400" width="600" ></iframe>
                    </div>
                    <div id="carchi" class="info">                        
                        <h3>Provincia: Carchi</h3> 
                        <iframe scrolling="no" src="../Graficas/LabProvincias.php?provincia=CARCHI" frameborder="0" height="400" width="600" ></iframe>
                    </div>
                    <div id="azuay" class="info">                        
                        <h3>Provincia: Azuay</h3>     
                        <iframe scrolling="no" src="../Graficas/LabProvincias.php?provincia=AZUAY" frameborder="0" height="400" width="600" ></iframe>
                    </div>
                    <div id="guayas" class="info">
                        <h3>Provincia: Guayas</h3>                        
                        <iframe scrolling="no" src="../Graficas/LabProvincias.php?provincia=GUAYAS" frameborder="0" height="400" width="600" ></iframe>
                    </div>
                    <div id="pichincha" class="info">                        
                        <h3>Provincia: Pichincha</h3>
                        <iframe scrolling="no" src="../Graficas/LabProvincias.php?provincia=PICHINCHA" frameborder="0" height="400" width="600" ></iframe>
                    </div>
                    <div id="galapagos" class="info">                        
                        <h3>Provincia: Galápagos</h3>  
                        <iframe scrolling="no" src="../Graficas/LabProvincias.php?provincia=GALAPAGOS" frameborder="0" height="400" width="600" ></iframe>
                    </div>
                    <div id="zona_no_delimitada" class="info">
                        <h3>Provincia: Zona no Delimitada</h3>  
                        <iframe scrolling="no" src="../Graficas/LabProvincias.php?provincia=ZONA NO DELIMITADA" frameborder="0" height="400" width="600" ></iframe>
                    </div> 
                    <div id="extranjero" class="info">
                        <h3>Extranjeros</h3>     
                        <iframe scrolling="no" src="../Graficas/LabProvincias.php?provincia=EXTRANJERO" frameborder="0" height="400" width="600" ></iframe>
                    </div>
                    <div id="regimen_sierra" class="info">
                        <h3>Régimen Sierra</h3>  
                        <iframe scrolling="no" src="../Graficas/LabProvincias.php?provincia=REGIMEN SIERRA" frameborder="0" height="400" width="600" ></iframe>
                    </div>       
                </div>
              </div>
            </div>
        </div>



    </body>
</html>
