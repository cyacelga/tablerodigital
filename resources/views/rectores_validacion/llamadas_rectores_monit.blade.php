<script>  
$("#checkMonitores").change(function(){
    var i =0;
    @foreach( $reportellamadarec as $llamadarect )
    i = i + 1;
   $("#monitor"+i).prop('checked', $(this).prop("checked"));   
   $("#zonas"+i).prop('checked', $(this).prop("checked"));   
    @endforeach;
});
function selectZonas(i){    
    $("#monitor"+i).change(function(){
    $("#zonas"+i).prop('checked', $(this).prop("checked"));
    });
};

$(function(){
   $(".azona").click(function(){
      $(".zona").css("display","block");
      $(".provincia").css("display","none");             
   }); 
});
    
$(function(){
$("#enviar_monitorlab").click(function(){ 
    $(".provincia").css("display","none");
    $(".laboratoriod").css("display","block");
    document.getElementById("loading").style.display="block";
    var id_periodo = $("#periodoLab").val();
    var fecha = $("#fecha_programada").val();
    var sesion = $("#sesion").val();
    var tp_reporte = $("#tp_reporte").val();
    var coordinador = $("#coordinador").val();
            
    if (id_periodo === "" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "0") {
                alert('Debe Seleccionar un Periodo');
    }else{
        if (id_periodo !== "" && fecha === "" && sesion === "") {
                    var ur = "laboratorio/" + tp_reporte + "/" + id_periodo + "/" + coordinador + "";
                } else {
                    if (id_periodo !== "" && fecha !== "" && sesion === "") {
                        var ur = "laboratorio/" + tp_reporte + "/" + id_periodo + "/" + coordinador +  "/"  + fecha + "";
                    } else {
                        var ur = "laboratorio/" + tp_reporte + "/" + id_periodo + "/" + coordinador +  "/"  + fecha + "/" + sesion + "";
                    }
                }
                var url = ur;
                $.ajax({
                type: "POST",
                url: url,
                data: $("#formulariol").serialize(),
                success: function(data)
                {
                $("#laboratorio").html(data);
                document.getElementById("loading").style.display="none";
                }
                }); 
         }
   return false;
   //alert(data);
});

});

</script>
<ol class="breadcrumb">
    <li><a href="exportarLlamadas/{{ $idperiodo }}/excel">
            <img src="images/excel.png" title="DESCARGAR EXCEL" style=" height: 23px; margin-right: -13px;">
        </a> &nbsp; &nbsp; &nbsp;</li>
    <li><a href="#" class="azona">Zona</a></li>
    <li class="active">Monitor</li>
</ol>
<div class="col-sm-12 col-md-10" style="width: auto;">
<div class="box table-responsive no-padding " style="width: auto;">
                                <div class="box-body">
                                    {!! Form::open(['method' => 'POST', 'id' => 'formulariol' ]) !!}
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 90%;">
                                        <tr style="background-color: #0489B1; color: white;">  
                                        <h4> <th style="text-align: center; vertical-align: middle;" colspan="6"><b>REPORTE DE LLAMADAS A RESPONSABLE DE SEDE POR MONITOR</b></th></h4>
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;">                                             
                                            <th style="text-align: center; vertical-align: middle; padding: 3px;">
                                                 &nbsp;&nbsp; <input type="checkbox" id="checkMonitores"> MONITOR 
                                            </th>
                                            <th style="width: 20px; text-align:center;" ><b>ZONA</b></th>
                                            <th style="text-align: center; vertical-align: middle; padding: 3px;"> PROGRAMADOS </th>
                                            <th style="text-align: center; vertical-align: middle; padding: 3px;"> CONTACTADO </th>
                                            <th style="text-align: center; vertical-align: middle; padding: 3px;"> NO CONTACTADO </th>
                                            <th style="text-align: center; vertical-align: middle; padding: 3px;"> POR CONTACTAR </th>   
                                        </tr>                                        
                                        <?php $totalAusen=0; $totalLab=0; $i=0;?>
                                            @foreach( $reportellamadarec as $rector )                                            
                                            <?php 
                                            $i++;
                                            if( $rector->programados_n_amie == 0){                                            
                                            $porcecontactado = number_format((($rector->contactado / 1) * 100), '2',',',''); 
                                            $porcenocontactado = number_format((($rector->nocontactado / 1) * 100), '2',',',''); 
                                            $porceenproceso = number_format((($rector->enproceso / 1) * 100), '2',',',''); 
                                            $porcenovedad = number_format((($rector->novedadllamada / 1) * 100), '2',',',''); 
                                            }else{                                           
                                            $porcecontactado = number_format((($rector->contactado / $rector->programados_n_amie) * 100), '2',',','');
                                            $porcenocontactado = number_format((($rector->nocontactado / $rector->programados_n_amie) * 100), '2',',','');
                                            $porceenproceso = number_format((($rector->enproceso / $rector->programados_n_amie) * 100), '2',',','');
                                            $porcenovedad = number_format((($rector->novedadllamada / $rector->programados_n_amie) * 100), '2',',','');
                                            
                                            }
                                            ?>
                                        <tr>                                            
                                            <td style="text-align: left; vertical-align: middle; padding: 3px;">
                                                    <label>    
                                                    <input type="checkbox" id="monitor{{ $i }}" value="{{ $rector->monitor }}" name="monitor[]" onclick="selectZonas({{ $i }});">
                                                    {{ $rector->monitor }}
                                                    </label>
                                            </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">
                                                <label>    
                                                    <input type="checkbox" id="zonas{{ $i }}" value="{{ $rector->zona }}" name="zona[]" hidden>
                                                    <b>{{ $rector->zona }}</b>
                                                    </label>
                                            </td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;"><b>{{ $rector->programados_n_amie }} </b></td>
                                                                                        
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php if($porcecontactado <= 44){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">     
                                                    <?php }elseif($porcecontactado >= 45 && $porcecontactado <= 99) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($porcecontactado >= 100) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>( {{ $rector->contactado }} ) {{ $porcecontactado }}%</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>                                            
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php if($porcenocontactado == '0,00'){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                    <?php }elseif($porcenocontactado >= 0 && $porcenocontactado <= 19 ) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($porcenocontactado >= 20 ) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>( {{ $rector->nocontactado }} ) {{ $porcenocontactado }}%</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>                                            
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php if($porceenproceso <= '0,00'){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                    <?php }elseif($porceenproceso >= 0 && $porceenproceso <= 19) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($porceenproceso >= 20 ) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>( {{ $rector->enproceso }} ) {{ $porceenproceso }}%</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>                                            
                                        </tr>
                                            @endforeach
                                            <tr><input type="hidden" value="{{ $coordinador }}" name="coordinador">
                                                <td style="background-color: #0489B1; color: white;" colspan="2"><b>TOTALES</b></td>
                                             <?php
                                             if($totalLlamadarec == 0){                                            
                                            $porcecontac = ($TotalContactado / 1) ; 
                                            $porcenocontac = ($TotalNoContactado / 1) ; 
                                            $porceenproc = ($TotalEnProceso / 1) ; 
                                            //$porcenoved = ($TotalNovedad / 1) ; 
                                            }else{
                                            $porcecontac = number_format((($TotalContactado / $totalLlamadarec) * 100), '2',',',''); 
                                            $porcenocontac = number_format((($TotalNoContactado / $totalLlamadarec) * 100), '2',',',''); 
                                            $porceenproc = number_format((($TotalEnProceso / $totalLlamadarec) * 100), '2',',',''); 
                                            //$porcenoved = number_format((($TotalNovedad / $totalLlamadarec) * 100), '2',',',''); 
                                            }
                                             ?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b> {{ $totalLlamadarec }} &nbsp;&nbsp;
                                            <?php if( $porcecontac <= 30 ){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ $TotalContactado }} ) {{ $porcecontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $porcecontac >= 31 && $porcecontac <= 79){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ $TotalContactado }} ) {{ $porcecontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $porcecontac >= 80){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ $TotalContactado }} ) {{ $porcecontac }}% &nbsp;&nbsp;        
                                            <?php } ?>
                                            <?php if( $porcenocontac <= 30 ){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ $TotalNoContactado }} ) {{ $porcenocontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $porcenocontac >= 31 && $porcenocontac <= 79){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ $TotalNoContactado }} ) {{ $porcenocontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $porcenocontac >= 80){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ $TotalNoContactado }} ) {{ $porcenocontac }}% &nbsp;&nbsp;        
                                            <?php } ?> 
                                            <?php if( $porceenproc <= 30 ){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ $TotalEnProceso }} ) {{ $porceenproc }}% &nbsp;&nbsp;
                                            <?php }elseif( $porceenproc >= 31 && $porcenocontac <= 79){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ $TotalEnProceso }} ) {{ $porceenproc }}% &nbsp;&nbsp;
                                            <?php }elseif( $porceenproc >= 80){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ $TotalEnProceso }} ) {{ $porceenproc }}% &nbsp;&nbsp;        
                                            <?php } ?>                                                       
                                            
                                        </tr>
                                        <tr>
                                           <td colspan="6"> 
                                              <!-- {!! Form::submit('Para consulta de Distrito por Zona hacer Click aqui', ['class' => 'btn btn-block btn-primary', 'onclick' => 'alert(hola);']) !!} -->
                                              <button class="btn btn-block btn-info" type="submit" id="enviar_monitorlab"><b>Para consulta de Instituciones por Monitor hacer Click aqui</b></button>
                                           </td>
                                       </tr>
                                    </table>
                                   {!! Form::close() !!}
                                </div>
                            </div>
                            </div>   