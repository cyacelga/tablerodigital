<script>
$(function(){
   $(".azona").click(function(){
      $(".zona").css("display","block");
      $(".provincia").css("display","none");             
      $(".laboratoriod").css("display","none");             
   }); 
});
$(function(){
   $(".amonitor").click(function(){
      $(".zona").css("display","none");
      $(".provincia").css("display","block");             
      $(".laboratoriod").css("display","none");             
   }); 
});
</script>
<ol class="breadcrumb">
                           <li><a href="exportarLlamadas/{{ $idperiodo }}/excel">
            <img src="images/excel.png" title="DESCARGAR EXCEL" style=" height: 23px; margin-right: -13px;">
        </a> &nbsp; &nbsp; &nbsp;</li>
                           <li><a href="#" class="azona">Zona</a></li>
                           <li><a href="#" class="amonitor">Monitor</a></li>
                           <li class="active">Laboratorio</li>
</ol>   
<div class="box table-responsive no-padding " style="width: auto;">
                                <div class="box-body">
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 90%;">
                                        <tr style="background-color: #0489B1; color: white;">  
                                        <h4> <th style="text-align: center; vertical-align: middle;" colspan="8"><b>REPORTE DE LLAMADAS A RESPONSABLE DE SEDE POR INSTITUCIÓN</b></th></h4>
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;">  
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">Nº</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">MONITOR</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">ZONA</th>
                                            <th style="text-align:center;  vertical-align: middle; padding: 3px;">AMIE</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">INSTITUCIÓN</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">RESPONSABLE DE SEDE</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">ESTADO DE LLAMADA</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">PROBLEMA DE LLAMADA</th>
                                        </tr>                                                                 
                                           <?php $i = 0;?>
                                            @foreach( $rector_lab as $labarector )
                                        <?php $i = $i + 1;?>                                                
                                            <tr>                                            
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"><b> {{ $i }} </b></td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">{{ $labarector->monitor }}</td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">{{ $labarector->zona }}</td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">{{ $labarector->programados_n_amie }}</td>
                                            <td style="text-align: left; vertical-align: middle; padding: 3px;">{{ $labarector->institucion }}</td>
                                            <td style="text-align: left; vertical-align: middle; padding: 3px;">{{ $labarector->rector }}</td>
                                            
                                            <?php if( $labarector->estadollamada=='CONTACTADO' ){?>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px; color: white; background-color: #5FB404;">
                                            <?php }elseif( $labarector->estadollamada=='NO CONTACTADO' ){?>    
                                            <td style="text-align: center; vertical-align: middle; padding: 3px; color: white; background-color: #FA5858;">
                                            <?php }elseif( $labarector->estadollamada=='POR CONTACTAR' ){?>    
                                            <td style="text-align: center; vertical-align: middle; padding: 3px; color: white; background-color: #D7DF01;">
                                            <?php } ?>
                                                {{ $labarector->estadollamada }}</td>
                                            <?php if( $labarector->novedadllamada=='' ){?>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">
                                            <?php }elseif( $labarector->novedadllamada!='VOLVER A LLAMAR' && $labarector->novedadllamada!='' ){?>    
                                            <td style="text-align: center; vertical-align: middle; padding: 3px; color: white; background-color: #FA5858;">
                                            <?php }elseif( $labarector->novedadllamada=='VOLVER A LLAMAR' ){?>    
                                            <td style="text-align: center; vertical-align: middle; padding: 3px; color: white; background-color: #D7DF01;">
                                            <?php } ?>
                                                {{ $labarector->novedadllamada }}</td>
                                        </tr>
                                        
                                         @endforeach
                                    </table>
                                </div>
                            </div>