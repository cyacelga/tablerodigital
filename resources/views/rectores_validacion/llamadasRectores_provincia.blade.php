<script>  
$("#checkZona").change(function(){
    @foreach( $reportellamadarec as $llamadarect )
   $("#zona{{ $llamadarect->zona }}").prop('checked', $(this).prop("checked"));   
    @endforeach;
});

$(function(){
   $(".azona").click(function(){
      $(".zona").css("display","block");
      $(".distritod").css("display","none");      
      $(".monitord").css("display","none");      
      $(".laboratoriod").css("display","none");      
   }); 
});
    
$(function(){
$("#enviar_monitor").click(function(){ 
    $(".zona").css("display","none");
    $(".provincia").css("display","block");
    document.getElementById("loading").style.display="block";
    var id_periodo = $("#periodoLab").val();
    var fecha = $("#fecha_programada").val();
    var sesion = $("#sesion").val();
    var tp_reporte = $("#tp_reporte").val();
    var coordinador = $("#coordinador").val();
            
    if (id_periodo === "" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "0") {
                alert('Debe Seleccionar un Periodo');
    }else{
        if (id_periodo !== "" && fecha === "" && sesion === "") {
                    var ur = "monitor/" + tp_reporte + "/" + id_periodo + "/" + coordinador + "";
                } else {
                    if (id_periodo !== "" && fecha !== "" && sesion === "") {
                        var ur = "monitor/" + tp_reporte + "/" + id_periodo + "/" + coordinador +  "/"  + fecha + "";
                    } else {
                        var ur = "monitor/" + tp_reporte + "/" + id_periodo + "/" + coordinador +  "/"  + fecha + "/" + sesion + "";
                    }
                }
                var url = ur;
                $.ajax({
                type: "POST",
                url: url,
                data: $("#formulario").serialize(),
                success: function(data)
                {
                $("#provincia").html(data);
                document.getElementById("loading").style.display="none";
                }
                }); 
         }
   return false;
   //alert(data);
});

});
</script>
        
        <div class="zona">
            <ol class="breadcrumb">
                    <li><a href="exportarLlamadas/{{ $idperiodo }}/excel">
                        <img src="images/excel.png" title="DESCARGAR EXCEL" style=" height: 23px; margin-right: -13px;">
                        </a> &nbsp; &nbsp; &nbsp;</li>
                    <li><a href="#" class="azona">Zona</a></li>
    <li class="active">Provincia</li>
        </ol>
        </div>
        
<div class="row zona" style="display: block;">

    <div class="col-sm-12 col-md-6" style="width: auto;">
        <div class="box table-responsive no-padding " style="width: auto;">
            <div class="box-body">
                {!! Form::open(['method' => 'POST', 'id' => 'formulario' ]) !!}
                <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 90%;">
                    <tr style="background-color: #0489B1; color: white;">  
                    <h4> <th style="text-align: center; vertical-align: middle;" colspan="5"><b>REPORTE DE LLAMADAS A RESPONSABLE DE SEDE POR ZONA</b></th></h4>
                    </tr>
                    <tr style="background-color: #0489B1; color: white;">  
                        <th style="width: 20px; text-align:center;" >
                            <label>
                                <input type="checkbox" id="checkZona"> <b>ZONA</b>                                                
                            </label>
                        </th>
                        <th style="text-align: center; vertical-align: middle; padding: 3px;"> PROGRAMADOS </th>
                        <th style="text-align: center; vertical-align: middle; padding: 3px;"> CONTACTADO </th>
                        <th style="text-align: center; vertical-align: middle; padding: 3px;"> NO CONTACTADO </th>
                        <th style="text-align: center; vertical-align: middle; padding: 3px;"> POR CONTACTAR </th>

                    </tr>                                        

                    @foreach( $reportellamadarec as $rector )

                    <?php
                    if ($totalLlamadarec == 0) {
                        $porcecontactado = number_format((($rector->contactado / 1) * 100), '2', ',', '');
                        $porcenocontactado = number_format((($rector->nocontactado / 1) * 100), '2', ',', '');
                        $porceenproceso = number_format((($rector->enproceso / 1) * 100), '2', ',', '');
                    } else {
                        $porcecontactado = number_format((($rector->contactado / $totalLlamadarec) * 100), '2', ',', '');
                        $porcenocontactado = number_format((($rector->nocontactado / $totalLlamadarec) * 100), '2', ',', '');
                        $porceenproceso = number_format((($rector->enproceso / $totalLlamadarec) * 100), '2', ',', '');
                    }
                    ?>
                    <tr>                                            
                        <td style="text-align: center; vertical-align: middle; padding: 3px;">
                            <label>    
                                <input type="checkbox" id="zona{{ $rector->zona }}" value="{{ $rector->zona }}" name="zona[]">
                                {{ $rector->zona }}
                            </label>
                        </td>
                        <td style="text-align: right; vertical-align: middle; padding: 3px;"><b>{{ $rector->programados_n_amie }} </b></td>

                        <td style="text-align: right; vertical-align: middle; padding: 3px;">
                            <div class="progress">
                                <?php if ($porcecontactado <= 30) { ?>
                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">     
                                    <?php } elseif ($porcecontactado >=31 && $porcecontactado <=79 ) { ?>
                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                        <?php } elseif ($porcecontactado >= 80 ) { ?>
                                            <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">
                                            <?php } ?>        
                                            <span class="skill"><i class="val"><b>( {{ $rector->contactado }} ) {{ $porcecontactado }}%</b></i></span>                                                        
                                        </div> 
                                    </div>
                                    </td>                                            
                                    <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                        <div class="progress">
                                            <?php if ($porcenocontactado <= 30) { ?>
                                                <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                <?php } elseif ($porcenocontactado >= 31 && $porcenocontactado <= 79) { ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php } elseif ($porcenocontactado >= 80) { ?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">
                                                        <?php } ?>        
                                                        <span class="skill"><i class="val"><b>( {{ $rector->nocontactado }} ) {{ $porcenocontactado }}%</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                                </td>                                            
                                                <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                                    <div class="progress">
                                                        <?php if ($porceenproceso <= 30) { ?>
                                                            <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                            <?php } elseif ($porceenproceso >= 31 && $porceenproceso <= 79) { ?>
                                                                <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                                <?php } elseif ($porceenproceso >= 80) { ?>
                                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">
                                                                    <?php } ?>        
                                                                    <span class="skill"><i class="val"><b>( {{ $rector->enproceso }} ) {{ $porceenproceso }}%</b></i></span>                                                        
                                                                </div> 
                                                            </div>
                                                            </td>


                                                            </tr><input type="hidden" value="{{ $coordinador }}" name="coordinador">
                                                            @endforeach
                                                            <tr>
                                                                <td style="background-color: #0489B1; color: white;"><b>TOTALES</b></td>
                                                                <?php
                                                                if ($totalLlamadarec == 0) {
                                                                    $porcecontac = ($TotalContactado / 1);
                                                                    $porcenocontac = ($TotalNoContactado / 1);
                                                                    $porceenproc = ($TotalEnProceso / 1);
                                                                } else {
                                                                    $porcecontac = number_format((($TotalContactado / $totalLlamadarec) * 100), '2', ',', '');
                                                                    $porcenocontac = number_format((($TotalNoContactado / $totalLlamadarec) * 100), '2', ',', '');
                                                                    $porceenproc = number_format((($TotalEnProceso / $totalLlamadarec) * 100), '2', ',', '');
                                                                }
                                                                ?>
                                                                <td align="right" style="background-color: #0489B1; color: white;"><b> {{ $totalLlamadarec }} &nbsp;&nbsp;
                                                                        <?php if ($porcecontac <= 30) { ?>
                                                                            <td align="right" style="background-color: #FA5858; color: white;"><b>( {{ $TotalContactado }} ) {{ $porcecontac }}% &nbsp;&nbsp;
                                                                                <?php } elseif ($porcecontac >=31 && $porcecontac <= 79) { ?>
                                                                                    <td align="right" style="background-color: #D7DF01; color: white;"><b>( {{ $TotalContactado }} ) {{ $porcecontac }}% &nbsp;&nbsp;
                                                                                        <?php } elseif ($porcecontac >= 80) { ?>
                                                                                            <td align="right" style="background-color: #5FB404; color: white;"><b>( {{ $TotalContactado }} ) {{ $porcecontac }}% &nbsp;&nbsp;        
                                                                                                <?php } ?>
                                                                                                <?php if ($porcenocontac <= 30) { ?>
                                                                                                    <td align="right" style="background-color: #5FB404; color: white;"><b>( {{ $TotalNoContactado }} ) {{ $porcenocontac }}% &nbsp;&nbsp;
                                                                                                        <?php } elseif ($porcenocontac >=31 && $porcenocontac <= 79) { ?>
                                                                                                            <td align="right" style="background-color: #D7DF01; color: white;"><b>( {{ $TotalNoContactado }} ) {{ $porcenocontac }}% &nbsp;&nbsp;
                                                                                                                <?php } elseif ($porcenocontac >= 80) { ?>
                                                                                                                    <td align="right" style="background-color: #FA5858; color: white;"><b>( {{ $TotalNoContactado }} ) {{ $porcenocontac }}% &nbsp;&nbsp;        
                                                                                                                        <?php } ?> 
                                                                                                                        <?php if ($porceenproc <= 30) { ?>
                                                                                                                            <td align="right" style="background-color: #5FB404; color: white;"><b>( {{ $TotalEnProceso }} ) {{ $porceenproc }}% &nbsp;&nbsp;
                                                                                                                                <?php } elseif ($porceenproc >= 31 && $porceenproc <= 79) { ?>
                                                                                                                                    <td align="right" style="background-color: #D7DF01; color: white;"><b>( {{ $TotalEnProceso }} ) {{ $porceenproc }}% &nbsp;&nbsp;
                                                                                                                                        <?php } elseif ($porceenproc >=80 ) { ?>
                                                                                                                                            <td align="right" style="background-color: #FA5858; color: white;"><b>( {{ $TotalEnProceso }} ) {{ $porceenproc }}% &nbsp;&nbsp;        
                                                                                                                                                <?php } ?>      


                                                                                                                                                </tr>
                                                                                                                                                <tr>
                                                                                                                                                    <td colspan="5"> 
                                                                                                                                                        <!-- {!! Form::submit('Para consulta de Distrito por Zona hacer Click aqui', ['class' => 'btn btn-block btn-primary', 'onclick' => 'alert(hola);']) !!} -->
                                                                                                                                                        <button class="btn btn-block btn-info" type="submit" id="enviar_monitor"><b>Para consulta de Zona por Monitor hacer Click aqui</b></button>
                                                                                                                                                    </td>
                                                                                                                                                </tr>
                                                                                                                                                </table>
                                                                                                                                                {!! Form::close() !!}
                                                                                                                                                </div>
                                                                                                                                                </div>
                                                                                                                                                </div>                                               
                                                                                                                                                <div class="col-lg-6 col-md-12" >
                                                                                                                                                    <div class="box">
                                                                                                                                                        <div class="box-header with-border" style="padding: 0px;">
                                                                                                                                                            <h5>&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-bar-chart"></i>&nbsp; <b> Llamadas a Responsable de Sede por Zona</b></h5>                             
                                                                                                                                                        </div>
                                                                                                                                                        <div id="area_zona">

                                                                                                                                                        </div>
                                                                                                                                                    </div>    
                                                                                                                                                </div>               
                                                                                                                                                </div>