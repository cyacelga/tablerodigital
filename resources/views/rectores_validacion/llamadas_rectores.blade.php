<script>  
$("#checkZona").change(function(){
    var i =0;
    @foreach( $reportellamadarec as $llamadarect )
    i = i + 1;
   $("#zona"+i).prop('checked', $(this).prop("checked"));   
    @endforeach;
});

$(function(){
   $(".azona").click(function(){
      $(".zona").css("display","block");
      $(".distritod").css("display","none");      
      $(".monitord").css("display","none");      
      $(".laboratoriod").css("display","none");      
   }); 
});
    
$(function(){
$("#enviar_monitor").click(function(){ 
    $(".zona").css("display","none");
    $(".provincia").css("display","block");
    document.getElementById("loading").style.display="block";
    var id_periodo = $("#periodoLab").val();
    var fecha = $("#fecha_programada").val();
    var sesion = $("#sesion").val();
    var tp_reporte = $("#tp_reporte").val();
    var coordinador = $("#coordinador").val();
            
    if (id_periodo === "" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "0") {
                alert('Debe Seleccionar un Periodo');
    }else{
        if (id_periodo !== "" && fecha === "" && sesion === "") {
                    var ur = "monitor/" + tp_reporte + "/" + id_periodo + "/" + coordinador + "";
                } else {
                    if (id_periodo !== "" && fecha !== "" && sesion === "") {
                        var ur = "monitor/" + tp_reporte + "/" + id_periodo + "/" + coordinador +  "/"  + fecha + "";
                    } else {
                        var ur = "monitor/" + tp_reporte + "/" + id_periodo + "/" + coordinador +  "/"  + fecha + "/" + sesion + "";
                    }
                }
                var url = ur;
                $.ajax({
                type: "POST",
                url: url,
                data: $("#formulario").serialize(),
                success: function(data)
                {
                $("#provincia").html(data);
                document.getElementById("loading").style.display="none";
                }
                }); 
         }
   return false;
   //alert(data);
});

});
</script>
        
        <div class="zona">
            <ol class="breadcrumb">
                    <li><a href="exportarLlamadas/{{ $idperiodo }}/excel">
                        <img src="images/excel.png" title="DESCARGAR EXCEL" style=" height: 23px; margin-right: -13px;">
                        </a> &nbsp; &nbsp; &nbsp;</li>
                    <li class="active">Zona</li>
        </ol>
        </div>

<div class="row zona">    
    
<div class="col-lg-3 col-sm-12" style="width: auto;">
                            <div class="box table-responsive no-padding">
                                <!-- <div class="box-header with-border">
                                    <h5><i class="fa fa-paste"></i> <b> Reporte de Descarga del Aplicativo</b></h5>
                                </div> /.box-header -->
                                <div class="box-body" hidden>
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 85%;">                                        
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >REPORTE DE DESCARGA DE APLICATIVO</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >ESTADO</th>
                                            <th style="text-align: center;">CANTIDAD</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                        </tr>
                                        @foreach($reportdescgeneral as $generalDesc)
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;">
                                              <b> @if ( $generalDesc->descarga == 'SI') DESCARGÓ @else {{$generalDesc->descarga}} DESCARGÓ @endif </b></td>                                           
                                            <td style="text-align: right;"><b> {{ $generalDesc->cantidad }} </b></td>
                                            <?php 
                                            if($totaldesc == 0){
                                            $porcen = number_format((($generalDesc->cantidad / 1 ) * 100), '2',',','');      
                                            }elseif($totaldesc !=0){
                                            $porcen = number_format((($generalDesc->cantidad / $totaldesc ) * 100), '2',',','');  
                                            }
                                            ?>
                                            <?php if($porcen <= 44 && $generalDesc->descarga == 'SI' ){ ?>                                            
                                            <td style="text-align: right; background-color: #FA5858; color: white;">
                                            <?php }elseif($porcen >= 45 && $porcen <= 99 && $generalDesc->descarga == 'SI'){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: #000000;">
                                            <?php }elseif($porcen >= 100 && $generalDesc->descarga == 'SI'){ ?>
                                            <td style="text-align: right; background-color: #5FB404; color: white;">
                                            <?php }elseif($generalDesc->descarga != 'SI'){ ?>
                                            <td style="text-align: right; background-color: #0489B1; color: white;">
                                            <?php } ?>     
                                                <b> {{ $porcen }} % </b></td>
                                        </tr>   
                                        @endforeach
                                        <tr>
                                            <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ $totaldesc }}</b></td>
                                            <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                                       </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div>
    
    <div class="col-lg-3 col-sm-12" style="width: auto;" hidden>
                            <div class="box table-responsive no-padding">
                                <div class="box-body">
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 85%;">
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >REPORTE DE INSTALACIÓN DE APLICATIVO</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >ESTADO</th>
                                            <th style="text-align: center;">CANTIDAD</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                        </tr>
                                        @foreach( $reporteinstaloapli as $generalInst)
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;">
                                            <b> @if ( $generalInst->instalo == 'SI') INSTALÓ @else {{ $generalInst->instalo }} INSTALÓ @endif </b>
                                            </td>                                           
                                            <td style="text-align: right;"><b> {{ $generalInst->cantidad }} </b></td>
                                            
                                            <?php 
                                            if($TotalInst == 0){
                                            $porcen = number_format((($generalInst->cantidad / 1 ) * 100), '2',',','');
                                            }elseif($TotalInst !=0){
                                            $porcen = number_format((($generalInst->cantidad / $TotalInst ) * 100), '2',',','');  
                                            }
                                            ?>
                                            <?php if($porcen <= 44 && $generalInst->instalo == 'SI'){ ?>                                            
                                            <td style="text-align: right; background-color: #FA5858; color: white;">
                                            <?php }elseif($porcen >= 45 && $porcen <= 99 && $generalInst->instalo == 'SI'){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: #000000;">
                                            <?php }elseif($porcen >= 100 && $generalInst->instalo == 'SI'){ ?>
                                            <td style="text-align: right; background-color: #5FB404; color: white;">
                                            <?php }elseif($generalInst->instalo != 'SI'){ ?>
                                            <td style="text-align: right; background-color: #0489B1; color: white;">
                                            <?php } ?>  
                                                <b> {{ $porcen }} % </b></td>
                                        </tr>   
                                        @endforeach
                                        <tr>
                                            <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ $TotalInst }}</b></td>
                                            <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                                       </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div>   
                        
</div>

<!-- LLAMADAS -->
        <div class="row zona">
                        <!-- /.box -->
                        <div class="col-xs-4 col-sm-12" style="width: auto;">
                            <div class="box table-responsive no-padding">
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 100%;">
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >AVANCE DE LLAMADAS</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >ESTADO</th>
                                            <th style="text-align: center;">CANTIDAD</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                        </tr>
                                         @foreach( $estadollamada as $estado)
                                        <?php 
                                        if( $totalLlamada == 0){
                                            $porcenestado = number_format((($estado->totalestado / 1) * 100), '2',',','') ;
                                        }else{
                                            $porcenestado = number_format((($estado->totalestado / $totalLlamada) * 100), '2',',',''); 
                                        }
                                        ?>
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;"><b>{{ $estado->estadollamada }}</b></td>                                           
                                            <td style="text-align: right;"><b> {{ $estado->totalestado }} </b></td>                                            
                                            <?php if($porcenestado <= 44 && $estado->estadollamada == 'CONTACTADO'){ ?>                                            
                                            <td style="text-align: right; background-color: #FA5858; color: white;">
                                            <?php }elseif($porcenestado >= 45 && $porcenestado <= 99 && $estado->estadollamada == 'CONTACTADO'){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: white;">
                                            <?php }elseif($porcenestado >= 100 && $estado->estadollamada == 'CONTACTADO'){ ?>
                                            <td style="text-align: right; background-color: #5FB404; color: white;">
                                            <?php }elseif($porcenestado <= '0,00' && $estado->estadollamada != 'CONTACTADO'){ ?>
                                            <td style="text-align: right; background-color: #5FB404; color: white;">
                                            <?php }elseif($porcenestado >= 0 && $porcenestado <= 19 && $estado->estadollamada != 'CONTACTADO'){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: white;">
                                            <?php }elseif($porcenestado >= 20 && $estado->estadollamada != 'CONTACTADO'){ ?>                                            
                                            <td style="text-align: right; background-color: #FA5858; color: white;">
                                            <?php } ?>
                                            <b> {{ $porcenestado }} % </b></td>
                                            
                                        </tr>                                        
                                        @endforeach
                                        <tr>
                                            <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ $totalLlamada }}</b></td>
                                            <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                                       </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div>
                        
                        <div class="col-xs-3 col-sm-12" style="width: auto">
                            <div class="box table-responsive no-padding">
                                 <!-- <div class="box-header with-border" style="padding: 0px;">
                                   <h5>&nbsp;&nbsp;&nbsp;<i class="fa fa-paste"></i> <b> Reporte de Problema de Llamadas</b></h5>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                     <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 100%;">
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >NO CONTACTADOS</th> 
                                        </tr> 
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >PROBLEMA</th>
                                            <th style="text-align: center;">CANTIDAD</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                        </tr>
                                        @foreach( $novedad as $novedad)
                                        <?php  
                                        
                                        if($ttnovedad == 0){
                                        $porcenovedad = number_format((($novedad->totalnovedad / 1) * 100), '2',',','');
                                        }elseif($ttnovedad != 0 ){
                                        $porcenovedad = number_format((($novedad->totalnovedad / $ttnovedad) * 100), '2',',','') ;
                                        }        
                                        ?>
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;"><b>{{ $novedad->novedadllamada }}</b></td>                                           
                                            <td style="text-align: right;"><b> {{ $novedad->totalnovedad }} </b></td>
                                            
                                            <?php if($porcenovedad == '0.00' ){ ?>                                            
                                            <td style="text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcenovedad }} % </b></td>
                                            <?php }elseif($porcenovedad >= 0 && $porcenovedad <= 19 ){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: white;"><b> {{ $porcenovedad }} % </b></td>
                                            <?php }elseif($porcenovedad >= 20 ){ ?>
                                            <td style="text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcenovedad }} % </b></td>
                                            <?php } ?>
                                            
                                        </tr>                                        
                                        @endforeach
                                        <tr>
                                            <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ $ttnovedad }} </b></td>
                                            <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                                       </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div>   
                        <div class="col-lg-3 col-sm-12" style="width: auto;">
                            <div class="box table-responsive no-padding">
                                <div class="box-body">
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 85%;">
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >REPORTE DE RECEPCIÓN DE COMUNICADO</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >ESTADO</th>
                                            <th style="text-align: center;">CANTIDAD</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                        </tr>
                                        @foreach( $recepcioncorreo as $correo)
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;">
                                            <b> @if ( $correo->comunicado == 'SI') RECIBIÓ @elseif ($correo->comunicado == 'REVISARA') {{ $correo->comunicado }} @else {{ $correo->comunicado }} RECIBIÓ @endif </b>
                                            </td>                                           
                                            <td style="text-align: right;"><b> {{ $correo->cantidad }} </b></td>
                                            
                                            <?php 
                                            if($TotalRecepcion == 0){
                                            $porcenrc = number_format((($correo->cantidad / 1 ) * 100), '2',',','');    
                                            }elseif($TotalRecepcion !=0){
                                            $porcenrc = number_format((($correo->cantidad / $TotalRecepcion ) * 100), '2',',','');  
                                            }
                                            ?>
                                            <?php if($porcenrc <= 44 && $correo->comunicado == 'SI'){ ?>                                            
                                            <td style="text-align: right; background-color: #FA5858; color: white;">
                                            <?php }elseif($porcenrc >= 45 && $porcenrc <= 99 && $correo->comunicado == 'SI'){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: #000000;">
                                            <?php }elseif($porcenrc >= 100 && $correo->comunicado == 'SI'){ ?>
                                            <td style="text-align: right; background-color: #5FB404; color: white;">
                                            <?php }elseif($correo->comunicado != 'SI'){ ?>
                                            <td style="text-align: right; background-color: #0489B1; color: white;">
                                            <?php } ?>  
                                                <b> {{ $porcenrc }} % </b></td>
                                        </tr>   
                                        @endforeach
                                        <tr>
                                            <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ $TotalRecepcion }}</b></td>
                                            <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                                       </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div>
                        <div class="col-lg-4 col-sm-12" style="width: auto;" hidden>
                            <div class="box table-responsive no-padding">
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 100%;">
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >SEDES CAPACITACIÓN</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >ESTADO</th>
                                            <th style="text-align: center;">CANTIDAD</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                        </tr>
                                                                               
                                         @foreach( $capacitacion as $capacitacion)
                                        <?php 
                                        if( $TotalSedes == 0){
                                            $porcenCapacitada = number_format((($capacitacion->totalcapacitadas / 1) * 100), '2',',','') ;
                                        }else{
                                            $porcenCapacitada = number_format((($capacitacion->totalcapacitadas / $TotalSedes) * 100), '2',',',''); 
                                        }
                                        ?>
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;">
                                              <b>  @IF ($capacitacion->capacitacion == 'SI') CAPACITADAS @ELSE {{ $capacitacion->capacitacion }} CAPACITADAS @ENDIF </b></td>                                           
                                            <td style="text-align: right;"><b> {{ $capacitacion->totalcapacitadas }} </b></td>                                            
                                            <?php if($porcenCapacitada <= 44 && $capacitacion->capacitacion == 'SI'){ ?>                                            
                                            <td style="text-align: right; background-color: #FA5858; color: white;">
                                            <?php }elseif($porcenCapacitada >= 45 && $porcenCapacitada <= 99 && $capacitacion->capacitacion == 'SI'){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: white;">
                                            <?php }elseif($porcenCapacitada >= 100 && $capacitacion->capacitacion == 'SI'){ ?>
                                            <td style="text-align: right; background-color: #5FB404; color: white;">
                                            <?php }elseif($porcenCapacitada <= '0.00' && $capacitacion->capacitacion != 'SI'){ ?>
                                            <td style="text-align: right; background-color: #5FB404; color: white;">
                                            <?php }elseif($porcenCapacitada >= 0 && $porcenCapacitada <= 19 && $capacitacion->capacitacion != 'SI'){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: white;">
                                            <?php }elseif($porcenCapacitada >= 20 && $capacitacion->capacitacion != 'SI'){ ?>                                            
                                            <td style="text-align: right; background-color: #FA5858; color: white;">
                                            <?php } ?>
                                            <b> {{ $porcenCapacitada }} % </b></td>
                                            
                                        </tr>                                        
                                        @endforeach
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;"><b>SEDES PROGRAMADAS</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ $TotalSedes }}</b></td>
                                            <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                                       </tr>
                                       
                                       @foreach($n_Sustentantes as $n_Sustentantes)
                                       <?php 
                                        if( $Total_Sustentantes == 0){
                                            $porcenTtSust = number_format((($n_Sustentantes->n_sustentantes / 1) * 100), '2',',','') ;
                                        }else{
                                            $porcenTtSust = number_format((($n_Sustentantes->n_sustentantes / $Total_Sustentantes) * 100), '2',',',''); 
                                        }
                                        ?>
                                       <tr>
                                            <td style="background-color: #0489B1; color: white; width: 140px;"><b>SUSTENTANTES AFECTADOS</b></td>    
                                            <td style="text-align: right;"><b>@IF ($n_Sustentantes->n_sustentantes == '') 0 @ELSE {{ $n_Sustentantes->n_sustentantes }} @ENDIF </b></td>
                                            
                                            <?php if($porcenTtSust <= '0.00'){ ?>
                                            <td style="text-align: right; background-color: #5FB404; color: white;">
                                            <?php }elseif($porcenTtSust >= 0 && $porcenTtSust <= 19){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: white;">
                                            <?php }elseif($porcenTtSust >= 20){ ?>                                            
                                            <td style="text-align: right; background-color: #FA5858; color: white;">
                                            <?php } ?>   
                                            <b> {{ $porcenTtSust }} % </b></td>
                                       </tr>
                                       @endforeach                                                                             
                                       <tr>
                                            <td style="background-color: #0489B1; color: white; width: 140px;"><b>TOTAL SUSTENTANTES</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ $Total_Sustentantes }} </b></td>
                                            <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                                       </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div>
                </div>
        
<div class="row zona" style="display: block;">

    <div class="col-sm-12 col-md-6" style="width: auto;">
        <div class="box table-responsive no-padding " style="width: auto;">
            <div class="box-body">
                {!! Form::open(['method' => 'POST', 'id' => 'formulario' ]) !!}
                <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 90%;">
                    <tr style="background-color: #0489B1; color: white;">  
                    <h4> <th style="text-align: center; vertical-align: middle;" colspan="5"><b>REPORTE DE LLAMADAS A RESPONSABLE DE SEDE POR ZONA</b></th></h4>
                    </tr>
                    <tr style="background-color: #0489B1; color: white;">  
                        <th style="width: 20px; text-align:center;" >
                            <label>
                                <input type="checkbox" id="checkZona"> <b>ZONA</b>                                                
                            </label>
                        </th>
                        <th style="text-align: center; vertical-align: middle; padding: 3px;"> PROGRAMADOS </th>
                        <th style="text-align: center; vertical-align: middle; padding: 3px;"> CONTACTADO </th>
                        <th style="text-align: center; vertical-align: middle; padding: 3px;"> NO CONTACTADO </th>
                        <th style="text-align: center; vertical-align: middle; padding: 3px;"> POR CONTACTAR </th>

                    </tr>                                        
                    <?php $i=0; ?>
                    @foreach( $reportellamadarec as $rector )

                    <?php
                    $i++;
                    if ($rector->programados_n_amie == 0) {
                        $porcecontactado = number_format((($rector->contactado / 1) * 100), '2', ',', '');
                        $porcenocontactado = number_format((($rector->nocontactado / 1) * 100), '2', ',', '');
                        $porceenproceso = number_format((($rector->enproceso / 1) * 100), '2', ',', '');
                    } else {
                        $porcecontactado = number_format((($rector->contactado / $rector->programados_n_amie) * 100), '2', ',', '');
                        $porcenocontactado = number_format((($rector->nocontactado / $rector->programados_n_amie) * 100), '2', ',', '');
                        $porceenproceso = number_format((($rector->enproceso / $rector->programados_n_amie) * 100), '2', ',', '');
                    }
                    ?>
                    <tr>                                            
                        <td style="text-align: center; vertical-align: middle; padding: 3px;">
                            <label>    
                                <input type="checkbox" id="zona{{ $i }}" value="{{ $rector->zona }}" name="zona[]">
                                {{ $rector->zona }}
                            </label>
                        </td>
                        <td style="text-align: right; vertical-align: middle; padding: 3px;"><b>{{ $rector->programados_n_amie }} </b></td>

                        <td style="text-align: right; vertical-align: middle; padding: 3px;">
                            <div class="progress">
                                <?php if ($porcecontactado <= 44) { ?>
                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">     
                                    <?php } elseif ($porcecontactado >= 45 && $porcecontactado <= 99 ) { ?>
                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                        <?php } elseif ($porcecontactado >= 100 ) { ?>
                                            <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">
                                            <?php } ?>        
                                            <span class="skill"><i class="val"><b>( {{ $rector->contactado }} ) {{ $porcecontactado }}%</b></i></span>                                                        
                                        </div> 
                                    </div>
                                    </td>                                            
                                    <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                        <div class="progress">
                                            <?php if ($porcenocontactado <= '0,00' ) { ?>
                                                <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                <?php } elseif ($porcenocontactado >= 0 && $porcenocontactado <= 19 ) { ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php } elseif ($porcenocontactado >= 20) { ?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">
                                                        <?php } ?>        
                                                        <span class="skill"><i class="val"><b>( {{ $rector->nocontactado }} ) {{ $porcenocontactado }}%</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                                </td>                                            
                                                <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                                    <div class="progress">
                                                        <?php if ($porceenproceso <= '0,00') { ?>
                                                            <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                            <?php } elseif ($porceenproceso >= 0 && $porceenproceso <= 19) { ?>
                                                                <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                                <?php } elseif ($porceenproceso >= 20) { ?>
                                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">
                                                                    <?php } ?>        
                                                                    <span class="skill"><i class="val"><b>( {{ $rector->enproceso }} ) {{ $porceenproceso }}%</b></i></span>                                                        
                                                                </div> 
                                                            </div>
                                                            </td>


                                                            </tr><input type="hidden" value="{{ $coordinador }}" name="coordinador">
                                                            @endforeach
                                                            <tr>
                                                                <td style="background-color: #0489B1; color: white;"><b>TOTALES</b></td>
                                                                <?php
                                                                if ($totalLlamadarec == 0) {
                                                                    $porcecontac = ($TotalContactado / 1);
                                                                    $porcenocontac = ($TotalNoContactado / 1);
                                                                    $porceenproc = ($TotalEnProceso / 1);
                                                                } else {
                                                                    $porcecontac = number_format((($TotalContactado / $totalLlamadarec) * 100), '2', ',', '');
                                                                    $porcenocontac = number_format((($TotalNoContactado / $totalLlamadarec) * 100), '2', ',', '');
                                                                    $porceenproc = number_format((($TotalEnProceso / $totalLlamadarec) * 100), '2', ',', '');
                                                                }
                                                                ?>
                                                                <td align="right" style="background-color: #0489B1; color: white;"><b> {{ $totalLlamadarec }} &nbsp;&nbsp;
                                                                        <?php if ($porcecontac <= 30) { ?>
                                                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ $TotalContactado }} ) {{ $porcecontac }}% &nbsp;&nbsp;
                                                                                <?php } elseif ($porcecontac >=31 && $porcecontac <= 79) { ?>
                                                                                    <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ $TotalContactado }} ) {{ $porcecontac }}% &nbsp;&nbsp;
                                                                                        <?php } elseif ($porcecontac >= 80) { ?>
                                                                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ $TotalContactado }} ) {{ $porcecontac }}% &nbsp;&nbsp;        
                                                                                                <?php } ?>
                                                                                                <?php if ($porcenocontac <= 30) { ?>
                                                                                                    <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ $TotalNoContactado }} ) {{ $porcenocontac }}% &nbsp;&nbsp;
                                                                                                        <?php } elseif ($porcenocontac >=31 && $porcenocontac <= 79) { ?>
                                                                                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ $TotalNoContactado }} ) {{ $porcenocontac }}% &nbsp;&nbsp;
                                                                                                                <?php } elseif ($porcenocontac >= 80) { ?>
                                                                                                                    <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ $TotalNoContactado }} ) {{ $porcenocontac }}% &nbsp;&nbsp;        
                                                                                                                        <?php } ?> 
                                                                                                                        <?php if ($porceenproc <= 30) { ?>
                                                                                                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ $TotalEnProceso }} ) {{ $porceenproc }}% &nbsp;&nbsp;
                                                                                                                                <?php } elseif ($porceenproc >= 31 && $porceenproc <= 79) { ?>
                                                                                                                                    <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ $TotalEnProceso }} ) {{ $porceenproc }}% &nbsp;&nbsp;
                                                                                                                                        <?php } elseif ($porceenproc >=80 ) { ?>
                                                                                                                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ $TotalEnProceso }} ) {{ $porceenproc }}% &nbsp;&nbsp;        
                                                                                                                                                <?php } ?>      


                                                                                                                                                </tr>
                                                                                                                                                <tr>
                                                                                                                                                    <td colspan="5"> 
                                                                                                                                                        <!-- {!! Form::submit('Para consulta de Distrito por Zona hacer Click aqui', ['class' => 'btn btn-block btn-primary', 'onclick' => 'alert(hola);']) !!} -->
                                                                                                                                                        <button class="btn btn-block btn-info" type="submit" id="enviar_monitor"><b>Para consulta de Zona por Monitor hacer Click aqui</b></button>
                                                                                                                                                    </td>
                                                                                                                                                </tr>
                                                                                                                                                </table>
                                                                                                                                                {!! Form::close() !!}
                                                                                                                                                </div>
                                                                                                                                                </div>
                                                                                                                                                </div>                                               
                                                                                                                                                <div class="col-lg-6 col-md-12" >
                                                                                                                                                    <div class="box">
                                                                                                                                                        <div class="box-header with-border" style="padding: 0px;">
                                                                                                                                                            <h5>&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-bar-chart"></i>&nbsp; <b> Llamadas a Responsable de Sede por Zona</b></h5>                             
                                                                                                                                                        </div>
                                                                                                                                                        <div id="area_zona">

                                                                                                                                                        </div>
                                                                                                                                                    </div>    
                                                                                                                                                </div>               
                                                                                                                                                </div>
                    
                    <div class="row provincia" style="display: none;" id="provincia1">
                        <!-- Reporte Distrito-->
                        <div id="provincia" class="col-lg-12 col-sm-12">
                            
                        </div>
                        <div class="col-lg-12 col-md-12" id="area_provincia">
                
                        </div>
                    </div>
                    <br>
                    <div class="distritod" style="display: none;">
                           <ol class="breadcrumb">
                           <li><a href="#">Reporte</a></li>
                           <li><a href="#" class="azona">Zona</a></li>
                           <li><a href="#" class="aprovincia">Provincia</a></li>
                           <li class="active">Distrito</li>
                           </ol>
                    </div>
                    <div class="row distritod" style="display: none;" id="distrito1">
                        <!-- Reporte Distrito-->
                        <div id="distrito" class="col-lg-12 col-sm-12">
                            
                        </div>
                        <div class="col-lg-12 col-md-12" id="area_distrito">
                
                        </div>
                    </div>
                    <br>
                    <div class="monitord" style="display: none;">
                           <ol class="breadcrumb">
                           <li><a href="#">Reporte</a></li>
                           <li><a href="#" class="azona">Zona</a></li>
                           <li><a href="#" class="aprovincia">Provincia</a></li>
                           <li><a href="#" class="adistrito">Distrito</a></li>
                           <li class="active">Monitor</li>
                           </ol>
                    </div>
                    <div class="row monitord" id="monitor1">
                        <div class="col-lg-12" id="monitor">
                        
                        </div>
                        <div class="col-lg-12 col-md-12" id="area_monitor">
                
                      </div>
                    </div>
                     <div class="row laboratoriod" id="laboratorio1">
                        <div class="col-lg-12" id="laboratorio">
                        
                        </div>                         
                    </div>
 <script type="text/javascript"> 
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ['ZONA', 'CONTACTADO', 'NO CONTACTADO ', 'POR CONTACTAR'],
        @foreach( $reportellamadarec as $llamarect )
        ['Zona {{ $llamarect->zona }}', {{ $llamarect->contactado }}, {{ $llamarect->nocontactado }}, {{ $llamarect->enproceso }}],
        @endforeach
      ]);

      var options = {
        isStacked: 'percent',
        height: 350,
        legend: { position: 'top', maxLines: 3 },
        vAxis: {
            minValue: 0,
            ticks: [0, .3, .6, .8, 1]
        }
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("area_zona"));
      chart.draw(data, options);
  }

</script>