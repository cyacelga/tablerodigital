<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
        <title>Ecuador</title>
        <link href="css/dagi/estilo.css" rel="stylesheet" type="text/css"/>    


        <script type="text/javascript">

            $(function () {
                //http://www.switchonthecode.com/tutorials/xml-parsing-with-jquery
                $.ajax({
                    type: 'POST',
                    url: 'images/dagi/xmlMapas/zonal/ecuadorz.xml', // .svg renamed .xml for IE support
                    dataType: 'xml',
                    success: function (xml) {
                        var r = Raphael('canvas', 560, 580);
                        var map = {};
                        var map_set = r.set();
                        var active_fill = 'gold';
                        var active_stroke = 'white';
                        var normal_fill = $('body').css('color');
                        var normal_stroke = '';
                        var active = null;

                        var colorC = ["#004899"];
                        var colorB = ["#FFFFFF"];
                        var colorD = ["#FFFFFF"];

                        $(xml).find('path').each(function () {
                            var id = (String)($(this).attr('id'));
                            var path = (String)($(this).attr('d'));
                            var tasa = (String)($(this).attr('tasa'));
                            var colors = {
<?php
///  $provincia= $_REQUEST['provincia'];
////$canton= $_REQUEST['canton'];                
//                $provincia='MANABI';
//                $canton='EL CARMEN';                

$user = "dact_tablero1";
$password = "dact123456";
$dbname = "evaluacion";
$port = "5432";
$host = "192.168.250.119";
$cadenaConexion = "host=$host port=$port dbname=$dbname user=$user password=$password";
$conexion = pg_connect($cadenaConexion) or die("Error en la Conexión: " . pg_last_error());
$query = "with tb1 as(
select zona,laboratorio,sesionsi,
round (((sesionsi :: numeric *100)/laboratorio),0) as porcentaje
 from
(SELECT  zona, count(cgi_laboratorio_id) as laboratorio,
--count(case when descarga_aplicativo ='' then 'NO' when descarga_aplicativo is null then 'NO' when descarga_aplicativo ='NO' then 'NO' end) as descargano,
--count(case when descarga_aplicativo ='SI' then 'SI' end) as descargasi
--,count(case when instalo_aplicativo ='' then 'NO' when instalo_aplicativo is null then 'NO' when instalo_aplicativo ='NO' then 'NO' end) as instalono,
--count(case when instalo_aplicativo ='SI' then 'SI' end) as instalosi,
count(case when estado_sesion ='' then 'NO' when estado_sesion is null then 'NO' when estado_sesion ='NO' then 'NO' end) as sesionno,
count(case when estado_sesion ='SI' then 'SI' end) as sesionsi 
FROM umonitoreo
WHERE cgi_periodo_id=$periodo AND estado=1 $fecha $sesion group by zona order by zona, laboratorio) as a
)
select 
'zona'||zona as zona,
case when tb1.porcentaje is null then '#B3B3B3' 
when tb1.porcentaje=0 then '#B3B3B3'
when tb1.porcentaje>=1 and  tb1.porcentaje<=10 then '#FF0022'
when tb1.porcentaje>=0 and  tb1.porcentaje<=10 then '#FF0022' 
when tb1.porcentaje>10 and  tb1.porcentaje<=20 then '#FF0022' 
when tb1.porcentaje>20 and  tb1.porcentaje<=30 then '#FF0022' 
when tb1.porcentaje>30 and  tb1.porcentaje<=40 then '#FF0022' 
when tb1.porcentaje>40 and  tb1.porcentaje<=50 then '#FFF500' 
when tb1.porcentaje>50 and  tb1.porcentaje<=60 then '#FFF500' 
when tb1.porcentaje>60 and  tb1.porcentaje<=70 then '#FFF500' 
when tb1.porcentaje>70 and  tb1.porcentaje<=80 then '#FFF500' 
when tb1.porcentaje>80 and  tb1.porcentaje<=99 then '#FFF500' 
when tb1.porcentaje>90 and  tb1.porcentaje=100 then '#007900'  end as color from tb1 where zona is not null";

$resultado = pg_query($conexion, $query) or die("Error en la Consulta SQL");
$numReg = pg_num_rows($resultado);
if ($numReg > 0) {
    while ($fila = pg_fetch_array($resultado)) {
        echo $fila['zona'] . ":'";
        echo $fila['color'] . "',";
    }
} else {
    echo "No hay Registros";
}
pg_close($conexion);
?>                             
                            };
                            map[id] = r.path(path)
                                    .attr({fill: colors[id], stroke: normal_stroke})
                                    .drag(
                                            // hacer el drag como con los circulos no funciona muy bien para los paths
                                                    // ya que los dx son usados cada vez
                                                            // aqui calculo el diferencial continuamente

                                                                    function (dx, dy) {// move
                                                                        this.translate(dx - this.dx, dy - this.dy);
                                                                        this.dx = dx;
                                                                        this.dy = dy;
                                                                        //$('#test').html(dx+'--'+dy);
                                                                    },
                                                                    function (ox, oy) {// start
                                                                        //this.ox = ox;
                                                                        //this.oy = oy;
                                                                        this.dx = 0;
                                                                        this.dy = 0;
                                                                        this.toFront();
                                                                        this.attr({opacity: .5});
                                                                        //$('#test').html(ox+'-'+oy);
                                                                    },
                                                                    function () {// up
                                                                        // regresa a la posición original
                                                                        this.translate(-this.dx, -this.dy);
                                                                        this.attr({opacity: 1});

                                                                        // Este bloque se hacia en click() pero mejor aqui para que tambien funcione en IE
                                                                        // restablecer activo previo
                                                                        if (active) {
                                                                            active.animate({fill: colorD}, 500, '>');
                                                                        }
                                                                        colorD = colors[id];
                                                                        // activar actual
                                                                        active = this;
                                                                        active.animate({fill: colors[id], opacity: 1}, 500, '>'); //celeste
                                                                        if (active) {
                                                                            active.animate({fill: colorC}, 500, '>');
                                                                        }
                                                                        // ocultar otras info
                                                                        $('.info').hide();
                                                                        // mostrar info actual
                                                                        $('#' + id).show().css('background-color', colorB); //celeste

                                                                    }
                                                            )
                                                                    .hover(function () {
                                                                        this.color = colorC; //azul
                                                                        if (this != active) {
                                                                            this.animate({fill: "#878786", stroke: active_stroke}); //turquesa+                                                                        

                                                                        }
                                                                    }, function () {
                                                                        if (this != active) {
                                                                            this.animate({fill: colors[id]});
                                                                        }
                                                                    })

                                                            map_set.push(map[id]);
                                                        });// end each
                                            } // end success
                                });

            });
        </script>
    </head>
    <body>

        <div class="row zona" style="display: block;">
                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                      <div class="box box-primary">
                          <div class="box-header">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title">Mapa Ecuador</h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -6px;">
                          <div class="box-body" style="height: 450px;">
                              <div id="canvas" style="margin-top: -70px;"></div> 
                          </div>
                      </div>
                  </div>                
                
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" id="infobox">
                    <div class="info" style="display:block;">
                        <div class="box box-primary" style="height: 508px; width: 625px;">
                          <div class="box-header" style="margin-top: -26px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title">Zona: 1 <a class="btn btn-primary" title="VerZonas" href="#" onclick="buscar_distritos(1);">Ver Información por distritos</a></h3>
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/iniciosesion/Graficas/LabZona.php?provincia=1&periodo={{$periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>
                          </div>
                      </div>  
                    </div>
                    <div id="zona1" class="info"> 
                        <div class="box box-primary" style="height: 508px; width: 625px;">
                          <div class="box-header" style="margin-top: -26px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title">Zona: 1 <a class="btn btn-primary" title="VerZonas" href="#" onclick="buscar_distritos(1);">Ver Información por distritos</a></h3>              
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/iniciosesion/Graficas/LabZona.php?provincia=1&periodo={{$periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>
                          </div>
                      </div> 
                    </div>

                    <div id="zona2" class="info"> 
                        <div class="box box-primary" style="height: 508px; width: 625px;">
                          <div class="box-header" style="margin-top: -26px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title">Zona: 2 <a class="btn btn-primary" title="VerZonas" href="#" onclick="buscar_distritos(2);">Ver Información por distritos</a></h3>                 
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                              <iframe scrolling="no" src="../resources/views/iniciosesion/Graficas/LabZona.php?provincia=2&periodo={{$periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>
                          </div>
                      </div>
                    </div>

                    <div id="zona3" class="info"> 
                        <div class="box box-primary" style="height: 508px; width: 625px;">
                          <div class="box-header" style="margin-top: -26px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title">Zona: 3 <a class="btn btn-primary" title="VerZonas" href="#" onclick="buscar_distritos(3);">Ver Información por distritos</a></h3>                 
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                             <iframe scrolling="no" src="../resources/views/iniciosesion/Graficas/LabZona.php?provincia=3&periodo={{$periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>
                          </div>
                      </div>  
                    </div>

                    <div id="zona4" class="info"> 
                        <div class="box box-primary" style="height: 508px; width: 625px;">
                          <div class="box-header" style="margin-top: -26px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title">Zona: 4 <a class="btn btn-primary" title="VerZonas" href="#" onclick="buscar_distritos(4);">Ver Información por distritos</a></h3>            
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                             <iframe scrolling="no" src="../resources/views/iniciosesion/Graficas/LabZona.php?provincia=4&periodo={{$periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>
                          </div>
                      </div>                        
                    </div>

                    <div id="zona5" class="info"> 
                        <div class="box box-primary" style="height: 508px; width: 625px;">
                          <div class="box-header" style="margin-top: -26px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title">Zona: 5 <a class="btn btn-primary" title="VerZonas" href="#" onclick="buscar_distritos(5);">Ver Información por distritos</a></h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                             <iframe scrolling="no" src="../resources/views/iniciosesion/Graficas/LabZona.php?provincia=5&periodo={{$periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>
                          </div>
                      </div>
                    </div>

                    <div id="zona6" class="info"> 
                        <div class="box box-primary" style="height: 508px; width: 625px;">
                          <div class="box-header" style="margin-top: -26px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title">Zona: 6 <a class="btn btn-primary" title="VerZonas" href="#" onclick="buscar_distritos(6);">Ver Información por distritos</a></h3>        
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                             <iframe scrolling="no" src="../resources/views/iniciosesion/Graficas/LabZona.php?provincia=6&periodo={{$periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>
                          </div>
                      </div>
                    </div>

                    <div id="zona7" class="info"> 
                        <div class="box box-primary" style="height: 508px; width: 625px;">
                          <div class="box-header" style="margin-top: -26px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title">Zona: 7 <a class="btn btn-primary" title="VerZonas" href="#" onclick="buscar_distritos(7);">Ver Información por distritos</a></h3>             
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                            <iframe scrolling="no" src="../resources/views/iniciosesion/Graficas/LabZona.php?provincia=7&periodo={{$periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>
                          </div>
                      </div>
                    </div>

                    <div id="zona8" class="info">
                        <div class="box box-primary" style="height: 508px; width: 625px;">
                          <div class="box-header" style="margin-top: -26px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title">Zona: 8 <a class="btn btn-primary" title="VerZonas" href="#" onclick="buscar_distritos(8);">Ver Información por distritos</a></h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                            <iframe scrolling="no" src="../resources/views/iniciosesion/Graficas/LabZona.php?provincia=8&periodo={{$periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>
                          </div>
                      </div>
                    </div>

                    <div id="zona9" class="info"> 
                        <div class="box box-primary" style="height: 508px; width: 625px;">
                          <div class="box-header" style="margin-top: -26px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title">Zona: 9 <a class="btn btn-primary" title="VerZonas" href="#" onclick="buscar_distritos(9);">Ver Información por distritos</a></h3>                  
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                           <iframe scrolling="no" src="../resources/views/iniciosesion/Graficas/LabZona.php?provincia=9&periodo={{$periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>
                          </div>
                      </div>
                    </div>

                    <div id="zonaSZ" class="info"> 
                        <div class="box box-primary" style="height: 508px; width: 625px;">
                          <div class="box-header" style="margin-top: -26px;">
                              <i class="ion ion-clipboard"></i>
                              <h3 class="box-title">Zona no delimitada <a class="btn btn-primary" title="VerZonas" href="#" onclick="buscar_distritos(0);">Ver Información por distritos</a></h3>                 
                          </div><!-- /.box-header -->
                          <hr style="margin-top: -26px;">
                          <div class="box-body">
                            <iframe scrolling="no" src="../resources/views/iniciosesion/Graficas/LabZona.php?provincia=SZ&periodo={{$periodo}}&fecha={{$fecha}}&sesion={{$sesion}}" frameborder="0" height="400" width="600" ></iframe>
                          </div>
                      </div>
                    </div>
                </div>
              </div>
                
                 <!-- Reporte Provincia-->
                                                 <div class="row provincia" style="display: none;">                       
                                                     <div id="provincia" class="col-lg-7 col-sm-12" style=" width: auto;">                            
                                                     </div>
                                                 </div>                     
                                                 <!-- Reporte Distrito-->                    
                                                 <div class="row distritod" style="display: none;">                        
                                                     <div id="distrito" class="col-lg-12 col-sm-12">

                                                     </div>
                                                 </div> 
                                                 <!-- Reporte Monitor--> 
                                                 <div class="row monitord" style="display: none;">
                                                     <div id="monitor" class="col-sm-12">
                                                     </div>
                                                 </div>
                                                 <!-- Reporte Laboratorios-->
                                                 <div class="row labmonitores" style="display: none;"> 
                                                     <div id="laboratorio" class="col-lg-7 col-sm-12" style=" width: auto;">
                                                     </div>
                                                 </div> 


    </body>
</html>