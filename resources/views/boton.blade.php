    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>AdminLTE 2 | Dashboard</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
       
    </head>    

<style type='text/css'>
		#loading{
		width:100%;
		height:100%;
                background-color:#ffffff;
		position:fixed;
		top:0;
		left:0;
		z-index:9999;
		opacity: 0.8;
		filter: alpha(opacity=80);
		}
		</style>
    <script src="js/jquery.min.js"></script>
    <!-- <script type="text/javascript" src="js/loader.js"></script> -->
     
    <script type="text/javascript">
        function buscar_datos() {
            
            var id_periodo = $("#periodo").val();
            $("#primero").css("display","none");                          
            if (id_periodo === "" ) {
                alert("Debe seleccionar un periodo");
            } else {
                $("#loading").css("display","block");
                if (id_periodo !== "" ) {
                    var ur = "boton_update/" + id_periodo + "";
                }
                
                $("#contenido").html();
                $.get(ur, function(resul) {
                    $("#contenido").html(resul);
                    // dialogo.dialog("close");
                    $("#loading").css("display","none")
                });
            }
            setTimeout('buscar_datos()',10000);           
           }        
                           
    </script>

            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
               
                <div>
                <section class="content-header"> 
                    <br>
                    <div class="row" id="seleccion">
                         <div class="col-sm-12 col-md-3">
                            <select class="form-control" id="periodo" onchange="buscar_datos();">
                               @foreach($periodo as $periodo)
                                <option value="{{ $periodo->cgi_periodo_id }}">{{ $periodo->name_periodo }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                </section>                     
                </div><br><br>
                
                <section> 
                    <div class="row">
                        <div class="col-sm-12 col-md-10" style="width: auto;" hidden>
<div class="box table-responsive no-padding " style="width: auto;">
                                <div class="box-body">
                                    {!! Form::open(['method' => 'POST', 'id' => 'formulario' ]) !!}
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 90%;">
                                        <tr style="background-color: #0489B1; color: white;">  
                                        <h4> <th style="text-align: center; vertical-align: middle;"><b>Actualizar Asistencia - Cargas</b></th></h4>
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;">  
                                            <th style="width: 20px; text-align:center;" >
                                            <label>
                                                <b>PERIODOS</b>                                                
                                            </label>
                                            </th>
                                        </tr>
                                            @foreach( $periodos as $periodos )
                                        <tr>                                            
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">
                                                    <label>    
                                                        <input type="checkbox" id="periodo{{ $periodos->periodo_monitoreo }}" value="{{ $periodos->periodo_monitoreo }}" name="periodos[]" checked="true">
                                                    {{ $periodos->periodo_monitoreo }}
                                                    </label>
                                            </td>                                            
                                        </tr>
                                            @endforeach                                            
                                    </table>
                                   {!! Form::close() !!}
                                </div>
                            </div>
                            </div> 
                    </div>

                </section>
                
                <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
                <!-- Main content -->
                   
                <div class="row" id="load" style="display: none;">
                        <img src="images/loading1.gif" style='margin:0 auto; position: absolute; top: 50%; left: 95%; margin: -230px 0 0 -30px;'>
                    </div>
                 <div class="row" id="loading" style="display: none;">
                        <img src="images/loading0.gif" style='margin:0 auto; position: absolute; top: 50%; left: 50%; margin: -30px 0 0 -30px;'>
                    </div>

                <section id="contenido" class="content">
                 
                </section><!-- /.content -->



            </div>      

        </div>   

    </div><!-- ./wrapper -->
    <script type="text/javascript" >
    window.onload = buscar_datos();
    </script>
    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script type="text/javascript" src="http://code.jquery.com/ui/jquery-ui-git.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
                            $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="plugins/morris/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="plugins/knob/jquery.knob.js"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
   <!-- <script src="dist/js/pages/dashboard.js"></script> -->
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>