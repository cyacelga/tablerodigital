<div>
<div class="col-lg-3 col-sm-12" style="width: auto">
                            <div class="box table-responsive no-padding">
                                <!-- <div class="box-header with-border">
                                    <h5><i class="fa fa-paste"></i> <b> Reporte de Descarga del Aplicativo</b></h5>
                                </div> /.box-header -->
                                <div class="box-body">
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 100%;">
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >PERIODO '{{ $periodo }}'</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >CARGAS DE ARCHIVOS</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >ESTADO</th>
                                            <th style="text-align: center;">CANTIDAD</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                        </tr>
                                        <?php 
                                        $porcargar=  $totalpro - $totalcargas ;
                                        if($totalpro == 0){
                                            $porcecarg= number_format(((  $totalcargas /  1 )*100),'2',',',''); 
                                              $porcenocarg= number_format(((  $porcargar /  1 )*100),'2',',','');
                                        }else{
                                              $porcecarg= number_format(((  $totalcargas /  $totalpro )*100),'2',',',''); 
                                              $porcenocarg= number_format(((  $porcargar /  $totalpro )*100),'2',',',''); 
                                        }
                                         ?>
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;"><b>ARCHIVOS CARGADOS</b></td>                                           
                                            <td style="text-align: right;"><b> {{ $totalcargas }} </b></td> 
                                            
                                            <?php if($porcecarg <= 45){ ?>                                            
                                            <td style="text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcecarg }} % </b></td>
                                            <?php }elseif($porcecarg > 45 && $porcecarg <= 99){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: #000000;"><b> {{ $porcecarg }} % </b></td>
                                            <?php }elseif($porcecarg >= 100){ ?>
                                            <td style="text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcecarg }} % </b></td>
                                            <?php } ?>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;"><b>ARCHIVOS POR CARGAR</b></td>                                           
                                            <td style="text-align: right;"><b> {{ $porcargar }} </b></td>        
                                            
                                            <?php if($porcenocarg > 45){ ?>                                            
                                            <td style="text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcenocarg }} % </b></td>
                                            <?php }elseif($porcenocarg > 0 && $porcenocarg <= 45){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: #000000;"><b> {{ $porcenocarg }} % </b></td>
                                            <?php }elseif($porcenocarg == 0){ ?>
                                            <td style="text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcenocarg }} % </b></td>
                                            <?php } ?>                                            
                                        </tr>   
                                        <tr>
                                            <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ $totalpro }}</b></td>
                                            <td style=" text-align: right; background-color: #5FB404; color: white;"><b> 100,00 % </b></td>
                                       </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div> 
    
                        <div class="col-lg-3 col-sm-12" style="width: auto">
                            <div class="box table-responsive no-padding">
                                <!-- <div class="box-header with-border">
                                    <h5><i class="fa fa-paste"></i> <b> Reporte de Descarga del Aplicativo</b></h5>
                                </div> /.box-header -->
                                <div class="box-body">
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 100%;">
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >PERIODO '{{ $periodo }}'</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >SUSTENTANTES PROGRAMADOS</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >ESTADO</th>
                                            <th style="text-align: center;">CANTIDAD</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                        </tr>
                                         <?php 
                                            if($totalpro ==0){
                                            $porcenttAsist= number_format((($totalasis / 1)*100),'2',',','');
                                            $porcenttAusest= number_format((($TotalAusent / 1)*100),'2',',','');
                                            }else{
                                            $porcenttAsist= number_format((($totalasis / $totalpro)*100),'2',',','');
                                            $porcenttAusest= number_format((($TotalAusent / $totalpro)*100),'2',',','');
                                            }
                                            ?>                                       
                                            
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;"><b>ASISTENCIA</b></td>                                           
                                            <td align="right"><b>{{ $totalasis }}  </b></td>  
                                            <?php if( $porcenttAsist <=75){?>
                                            <td align="right" style="background-color: #FA5858; color: white;">
                                            <?php }elseif( $porcenttAsist >75 && $porcenttAsist <=99){?>
                                            <td align="right" style="background-color: #D7DF01; color: #000000;">
                                            <?php }elseif( $porcenttAsist >=100){?>
                                            <td align="right" style="background-color: #5FB404; color: white;">        
                                            <?php } ?><b>{{ $porcenttAsist }}% </b></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;"><b>AUSENTES</b></td>                                           
                                            <td align="right" ><b>{{ $TotalAusent }}  </b></td>
                                            <?php if( $porcenttAusest ==0){?>
                                            <td align="right" style="background-color: #5FB404; color: white;">
                                            <?php }elseif( $porcenttAusest >0 && $porcenttAusest <=25){?>
                                            <td align="right" style="background-color: #D7DF01; color: #000000;">
                                            <?php }elseif( $porcenttAusest >25){?>
                                            <td align="right" style="background-color: #FA5858; color: white;">        
                                            <?php } ?><b>{{ $porcenttAusest }}% </b></td>                                         
                                        </tr>   
                                        <tr>
                                            <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTAL</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ $totalpro }}</b></td>
                                            <td style=" text-align: right; background-color: #5FB404; color: white;"><b> 100,00 % </b></td>
                                       </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div>
   
</div>
