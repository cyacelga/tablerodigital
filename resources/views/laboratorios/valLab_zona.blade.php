<script>
$("#checkZona").change(function(){ 
    var i =0;
    @foreach( $valida_lab as $validarLab )
    i = i + 1;
   $("#zona"+i).prop('checked', $(this).prop("checked"));   
    @endforeach;    
});    

$(function(){
$("#enviar").click(function(){ 
    $(".zona").css("display","none");
    $(".provincia").css("display","block");
    document.getElementById("loading").style.display="block";
    var id_periodo = $("#periodoLab").val();
    var fecha = $("#fecha_programada").val();
    var sesion = $("#sesion").val();
    var tp_reporte = $("#tp_reporte").val();
    
    if (id_periodo === "" && fecha === "" && sesion === "") {
        alert('Debe Seleccionar un Periodo');
    }else{
        if (id_periodo !== "" && fecha === "" && sesion === "") {
                    var ur = "provincia/" + tp_reporte + "/" + id_periodo + "";
                } else {
                    if (id_periodo !== "" && fecha !== "" && sesion === "") {
                        var ur = "provincia/" + tp_reporte + "/" + id_periodo + "/" + fecha + "";
                    } else {
                        var ur = "provincia/" + tp_reporte + "/" + id_periodo + "/" + fecha + "/" + sesion + "";
                    }
                }
                var url = ur;
                $.ajax({
                type: "POST",
                url: url,
                data: $("#formulario").serialize(),
                success: function(data)
                {
                $("#provincia").html(data);
                document.getElementById("loading").style.display="none";
                }
                }); 
         }
   return false;
   //alert(data);
});

});
/*
function mostrarOpcion(){
            var tp_reporte = $("#tp_reporte").val();
            
            if(tp_reporte !==''){
               $("#contenido").html('');
               $('#seleccion').show(1000);
            }else{
               $('#seleccion').hide(1000);
            }
        }        
*/
</script>
        <!-- Reporte Inicio de Sesión por Zona-->
        <div class="zona">
            <ol class="breadcrumb">                
                   <li><a href="exportaruno/{{ $idperiodo }}/excel">
                     <img src="images/excel.png" title="DESCARGAR EXCEL" style=" height: 23px; margin-right: -13px;">
                    </a> &nbsp; &nbsp; &nbsp;</li>
                    <li class="active">Zonas</li>
        </ol>
        </div>
        <div class="row zona">
                        
                        <div class="col-lg-3 col-sm-12" style="width: auto;">
                            <div class="box table-responsive no-padding">
                                <div class="box-header with-border" style="padding: 0px;">
                                    <h5>&nbsp;&nbsp;&nbsp;<i class="fa fa-paste"></i>&nbsp;&nbsp;&nbsp;
                                        <b> Reporte Estado de Laboratorios</b></h5>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                     <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 100%;">
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >ESTADO DE LABORATORIOS</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >ESTADO</th>
                                            <th style="text-align: center;">CANTIDAD</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                        </tr>
                                         @foreach($estadoLaboratorio as $edoLab)
                                         <?php $porceEdoLab= number_format((($edoLab->cantidadedo / $totalEdoLab)*100), '2',',',''); ?>
                                        <tr>                                           
                                            <td style="text-align: left; background-color: #0489B1; color: white;"><b>{{ $edoLab->dictamen2 }}</b></td>                                           
                                            <td style="text-align: right;"><b> {{ $edoLab->cantidadedo }} </b></td> 
                                            <?php if($porceEdoLab <= 45){?>
                                            <td style="text-align: right; background-color: #FA5858; color: white;"><b> {{ $porceEdoLab }} % </b></td>   
                                            <?php }elseif ($porceEdoLab > 45 && $porceEdoLab <= 99) { ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: white;"><b> {{ $porceEdoLab }} % </b></td>       
                                            <?php }elseif ($porceEdoLab >= 100) { ?>
                                            <td style="text-align: right; background-color: #5FB404; color: white;"><b> {{ $porceEdoLab }} % </b></td>           
                                            <?php } ?>
                                        </tr>  
                                        @endforeach
                                        <tr>
                                            <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ $totalEdoLab }} </b></td>
                                            <td style=" text-align: right; background-color: #5FB404; color: white;"><b> 100 % </b></td>
                                       </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div>      
            
            <div class="col-lg-4 col-sm-12" style="width: auto;">
                            <div class="box table-responsive no-padding">
                                <div class="box-header with-border" style="padding: 0px;">
                                    <h5>&nbsp;&nbsp;&nbsp;<i class="fa fa-paste"></i>&nbsp;&nbsp;&nbsp;
                                        <b> Reporte Observación de Laboratorios</b></h5>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto;  font-size: 90%;" >
                                        <tr style="background-color: #0489B1; color: white; padding: 0px;"> 
                                            <th style="text-align:center; padding: opx;" colspan="3" >OBSERVACIÓN DE LABORATORIOS</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white; padding: 0px;"> 
                                            <th style="text-align:center; padding: opx;" >OBSERVACÓN</th>
                                            <th style="text-align: center; padding: opx;">CANTIDAD</th>
                                            <th style="text-align: center; padding: opx;">% AVANCE</th> 
                                        </tr>
                                        @foreach( $observacionLab as $obs )
                                        <?php 
                                        if( $totalObsLab == 0){
                                           $porcenObs = number_format((($obs->cantobservacion / 1) * 100), '2',',','') ;
                                        }else{
                                           $porcenObs = number_format((($obs->cantobservacion / $totalObsLab) * 100), '2',',',''); 
                                        }
                                        ?>
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white; padding: opx;"><b>{{ $obs->observacion }}</b></td>                                           
                                            <td style="text-align: right; padding: opx;"><b>{{ $obs->cantobservacion }}</b></td>                                           
                                            <?php if($porcenObs >= 0 && $porcenObs <=20){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: white; padding: opx;">
                                            <?php } ?>
                                            <?php if($porcenObs > 20){ ?>
                                            <td style="text-align: right; background-color: #FA5858; color: white; padding: opx;">
                                            <?php } ?>    
                                            <b> {{ $porcenObs }}% </b></td>
                                            
                                        </tr> 
                                        @endforeach
                                        <tr>
                                            <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ $totalObsLab }}</b></td>
                                            <td style=" text-align: right; background-color: #FA5858; color: white;"><b> 100 % </b></td>
                                       </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div>
        </div>
        <div class="row zona">
            <div class="col-lg-11 col-sm-12" style="width: auto;">
                            <div class="box table-responsive no-padding">
                                <div class="box-header with-border" style="padding: 0px;">
                                    <h5>&nbsp;&nbsp;&nbsp;<i class="fa fa-paste"></i> <b> Reporte Estado de Laboratorio por Zona</b></h5>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                     {!! Form::open(['method' => 'POST', 'id' => 'formulario' ]) !!}
                                     <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 90%;">
                                         <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="width: 80px; text-align:center; vertical-align: middle;" >
                                            <label>
                                                <input type="checkbox" id="checkZona"> <b>Zona</b>                                                
                                            </label>
                                            </th>
                                            <th style="text-align: center; vertical-align: middle;">N_LABORATORIO</th>
                                            <th style="text-align: center; vertical-align: middle;">ÓPTIMOS</th>
                                            <th style="text-align: center; vertical-align: middle;">% ÓPTIMO</th>
                                            <th style="text-align: center; vertical-align: middle;">POR GESTIONAR</th>
                                            <th style="text-align: center; vertical-align: middle;">% POR GESTIONAR</th>
                                            <th style="text-align: center; vertical-align: middle;">RECOMENDABLE</th>
                                            <th style="text-align: center; vertical-align: middle;">% RECOMENDABLE</th>
                                            <th style="text-align: center; vertical-align: middle;">POR CONTACTAR</th>
                                            <th style="text-align: center; vertical-align: middle;">% POR CONTACTAR</th>
                                        </tr>
                                        <?php $i=0; ?>
                                         @foreach( $valida_lab as $valLaboratorio )
                                        <?php  $i++; ?>
                                        <tr>
                                            <td style="text-align: center; vertical-align: middle; padding: 0px;">
                                                <div class="checkbox" style="padding: 0px;">
                                                    <label>
                                                        <input type="checkbox" id="zona{{ $i }}" value="{{ $valLaboratorio->zona }}" name="zona[]">
                                                {{ $valLaboratorio->zona }}
                                                    </label>
                                                </div></td>                                           
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;"> {{ $valLaboratorio->laboratorio }} </td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;"> {{ $valLaboratorio->optimo }} </td>
                                            <?php
                                                  if( $valLaboratorio->laboratorio == 0 ){
                                                  $porcenoptimol = (number_format((($valLaboratorio->optimo / 1 )* 100), 2, ',', ' '));
                                                  $porcerecomendablel = (number_format((($valLaboratorio->recomendable / 1 )* 100), 2, ',', ' '));
                                                  $porceporgestionar = (number_format((($valLaboratorio->porgestionar / 1 )* 100), 2, ',', ' '));
                                                  $porcenorecomendablel = (number_format((($valLaboratorio->norecomendable / 1 )* 100), 2, ',', ' '));
                                            }else{
                                                  $porcenoptimol = (number_format((($valLaboratorio->optimo / $valLaboratorio->laboratorio )* 100), 2, ',', ' '));
                                                  $porceporgestionar = (number_format((($valLaboratorio->porgestionar / $valLaboratorio->laboratorio )* 100), 2, ',', ' '));
                                                  $porcerecomendablel = (number_format((($valLaboratorio->recomendable / $valLaboratorio->laboratorio )* 100), 2, ',', ' '));
                                                  $porcenorecomendablel = (number_format((($valLaboratorio->norecomendable / $valLaboratorio->laboratorio )* 100), 2, ',', ' '));
                                            } ?>
                                            <td align="right" style="padding: 3px;">
                                            <div class="progress">
                                                    <?php if($porcenoptimol <= 45){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">     
                                                    <?php }elseif($porcenoptimol > 45 && $porcenoptimol <= 80) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif( $porcenoptimol > 80 && $porcenoptimol <= 99 ) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #A5DF00;">                                                        
                                                    <?php }elseif($porcenoptimol >= 100 ) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>{{ $porcenoptimol.'%' }}</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;"> {{ $valLaboratorio->porgestionar }} </td>
                                            <td align="right" style="padding: 0px;">
                                            <div class="progress">
                                                    <?php if($porceporgestionar == 0){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                    <?php }elseif($porceporgestionar > 0 && $porceporgestionar <= 20) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #A5DF00;">                                                        
                                                    <?php }elseif( $porceporgestionar > 20 && $porceporgestionar <= 50 ) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($porceporgestionar > 50 ) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>{{ $porceporgestionar.'%' }}</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;"> {{ $valLaboratorio->recomendable }} </td>
                                            <td align="right" style="padding: 0px;">
                                            <div class="progress">
                                                    <?php if($porcerecomendablel <= 45){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">     
                                                    <?php }elseif($porcerecomendablel > 45 && $porcerecomendablel <= 80) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif( $porcerecomendablel > 80 && $porcerecomendablel <= 99 ) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #A5DF00;">                                                        
                                                    <?php }elseif($porcerecomendablel >= 100 ) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>{{ $porcerecomendablel.'%' }}</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>                                            
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;"> {{ $valLaboratorio->norecomendable }} </td>
                                            <td align="right" style="padding: 0px;">
                                            <div class="progress">
                                                    <?php if($porcenorecomendablel == 0){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                    <?php }elseif($porcenorecomendablel > 0 && $porcenorecomendablel <= 20) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #A5DF00;">                                                        
                                                    <?php }elseif( $porcenorecomendablel > 20 && $porcenorecomendablel <= 50 ) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($porcenorecomendablel > 50 ) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>{{ $porcenorecomendablel.'%' }}</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>                                                
                                        </tr>
                                        @endforeach
                                        <?php 
                                        if( $totalLab == 0 ){
                                        $porceoptimol = (number_format((($totalOptimo / 1) * 100), 2, ',', ' '));    
                                        $porcegestionar = (number_format((($totalgestionar / 1) * 100), 2, ',', ' '));    
                                        $porcerecomel = (number_format((($totalRecomen / 1) * 100), 2, ',', ' '));
                                        $porcenorecomel = (number_format((($totalNoReco / 1) * 100), 2, ',', ' '));                                            
                                        }else{
                                        $porceoptimol = (number_format((($totalOptimo / $totalLab) * 100), 2, ',', ' '));    
                                        $porcegestionar = (number_format((($totalgestionar / $totalLab) * 100), 2, ',', ' '));
                                        $porcerecomel = (number_format((($totalRecomen / $totalLab) * 100), 2, ',', ' '));
                                        $porcenorecomel = (number_format((($totalNoReco / $totalLab) * 100), 2, ',', ' '));
                                        }?>
                                        <tr>
                                            <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ $totalLab }} </b></td>
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ $totalOptimo }} </b></td>
                                           <?php if( $porceoptimol <= 45 ){?>
                                            <td style=" text-align: right; background-color: #FA5858; color: white;"><b> {{ $porceoptimol }}% </b></td>
                                           <?php }elseif ($porceoptimol >45 && $porceoptimol <=80 ) { ?>
                                            <td style=" text-align: right; background-color: #D7DF01; color: white;"><b> {{ $porceoptimol }} % </b></td>                                                
                                            <?php }elseif( $porceoptimol > 80 && $porceoptimol <= 99 ) { ?>
                                            <td style=" text-align: right; background-color: #A5DF00; color: white;"><b> {{ $porceoptimol }} % </b></td>                                                
                                            <?php }elseif ( $porceoptimol >= 100 ) { ?>
                                            <td style=" text-align: right; background-color: #5FB404; color: white;"><b> {{ $porceoptimol }} % </b></td>    
                                            <?php } ?>
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ $totalgestionar }} </b></td>
                                            
                                            <?php if( $porcegestionar == 0 ){?>
                                            <td style=" text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcegestionar }}% </b></td>
                                            <?php }elseif ($porcegestionar >0 && $porcegestionar <=20 ) { ?>
                                            <td style=" text-align: right; background-color: #A5DF00; color: white;"><b> {{ $porcegestionar }} % </b></td>                                                
                                            <?php }elseif( $porcegestionar > 20 && $porcegestionar <= 50 ) { ?>
                                            <td style=" text-align: right; background-color: #D7DF01; color: white;"><b> {{ $porcegestionar }} % </b></td>                                                
                                            <?php }elseif ( $porcegestionar >= 50 ) { ?>
                                            <td style=" text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcegestionar }} % </b></td>    
                                            <?php } ?>
                                            
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ $totalRecomen }} </b></td>
                                            <?php if( $porcerecomel <= 45 ){?>
                                            <td style=" text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcerecomel }}% </b></td>
                                            <?php }elseif ($porcerecomel >45 && $porcerecomel <=80 ) { ?>
                                            <td style=" text-align: right; background-color: #D7DF01; color: white;"><b> {{ $porcerecomel }} % </b></td>                                                
                                            <?php }elseif( $porcerecomel > 80 && $porcerecomel <= 99 ) { ?>
                                            <td style=" text-align: right; background-color: #A5DF00; color: white;"><b> {{ $porcerecomel }} % </b></td>                                                
                                            <?php }elseif ( $porcerecomel >= 100 ) { ?>
                                            <td style=" text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcerecomel }} % </b></td>    
                                            <?php } ?>
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ $totalNoReco }} </b></td>
                                            <?php if( $porcenorecomel == 0 ){?>
                                            <td style=" text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcenorecomel }}% </b></td>
                                            <?php }elseif ($porcenorecomel >0 && $porcenorecomel <=20 ) { ?>
                                            <td style=" text-align: right; background-color: #A5DF00; color: white;"><b> {{ $porcenorecomel }} % </b></td>                                                
                                            <?php }elseif( $porcenorecomel > 20 && $porcenorecomel <= 50 ) { ?>
                                            <td style=" text-align: right; background-color: #D7DF01; color: white;"><b> {{ $porcenorecomel }} % </b></td>                                                
                                            <?php }elseif ( $porcenorecomel >= 50 ) { ?>
                                            <td style=" text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcenorecomel }} % </b></td>    
                                            <?php } ?>
                                       </tr>
                                       <tr>
                                           <td colspan="10"> 
                                              <!-- {!! Form::submit('Para consulta de Distrito por Zona hacer Click aqui', ['class' => 'btn btn-block btn-primary', 'onclick' => 'alert(hola);']) !!} -->
                                              <button class="btn btn-block btn-info" type="submit" id="enviar"><b>Para consulta de Provincia por Zona hacer Click aqui</b></button>
                                           </td>
                                       </tr>
                                    </table>
                                    {!! Form::close() !!}
                                </div>
                            </div>                                               
                        </div>                        
                        <div class="col-lg-12 col-sm-12 responsive" id="area_zona">
                
                        </div>        
                    </div>
                     <!-- Reporte Provincia-->
                    <div class="row provincia" style="display: none;" id="provincia1">                       
                        <div id="provincia" class="col-lg-12 col-sm-12">                            
                        </div>
                    </div>                     
                    <!-- Reporte Distrito-->                    
                    <div class="row distritod" style="display: none;" id="distrito1">                        
                        <div id="distrito" class="col-lg-12 col-sm-12">
                            
                        </div>
                    </div>
                     <!-- Reporte Monitor-->
                    <div class="row monitord" id="monitor1">
                        <div class="col-lg-12" id="monitor">                        
                        </div>
                    </div>        
                     <!-- Reporte Laboratorios-->
                     <div class="row laboratoriod" id="laboratorio1">
                        <div class="col-lg-12" id="laboratorio">                        
                        </div>                         
                    </div>
                     
<script type="text/javascript"> 
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ['Zona', 'Optimo', 'Recomendable', 'No Recomendable'],
        @foreach( $valida_lab as $barra )
        ['Zona {{ $barra->zona }}', {{ $barra->optimo }}, {{ $barra->recomendable }}, {{ $barra->norecomendable }}],
        @endforeach
      ]);

      var options = {
        isStacked: 'percent',
        height: 350,
        legend: { position: 'top', maxLines: 3 },
        vAxis: {
            minValue: 0,
            ticks: [0, .3, .6, .8, 1]
        }
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("area_zona"));
      chart.draw(data, options);
  }
</script>