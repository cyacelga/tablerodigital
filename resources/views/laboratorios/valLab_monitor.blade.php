<script>
    $("#checkMonitor_l").change(function () {
        var i = 0;
                @foreach($valida_lab as $monitores)
        i++;
        $("#monitor_l" + i).prop('checked', $(this).prop("checked"));
        $("#distrito_l" + i).prop('checked', $(this).prop("checked"));
                @endforeach;
    });

    $(function(){
    $(".azona").click(function(){
    $(".zona").css("display", "block");
    $(".provincia").css("display", "none");
    $(".distritod").css("display", "none");
    $(".monitord").css("display", "none");
    $(".laboratoriod").css("display", "none");
    });
    });
    
    $(function(){
    $(".aprovincia").click(function(){
    $(".provincia").css("display", "block");
    $(".zona").css("display", "none");
    $(".distritod").css("display", "none");
    $(".monitord").css("display", "none");
    $(".laboratoriod").css("display", "none");
    });
    });
    
    $(function(){
    $(".adistrito").click(function(){
    $(".provincia").css("display", "none");
    $(".zona").css("display", "none");
    $(".distritod").css("display", "block");
    $(".monitord").css("display", "none");
    $(".laboratoriod").css("display", "none");
    });
    });
    
    $(function () {
        $("#enviar_monitor").click(function () {
            $(".monitord").css("display", "none");
            $(".laboratoriod").css("display", "block");
            document.getElementById("loading").style.display = "block";
            var id_periodo = $("#periodoLab").val();
            var fecha = $("#fecha_programada").val();
            var sesion = $("#sesion").val();
            var tp_reporte = $("#tp_reporte").val();
            var coordinador = $("#coordinador").val();

            if (id_periodo === "" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "0") {
                alert('Debe Seleccionar un Periodo');
            } else {
                if (id_periodo !== "" && fecha === "" && sesion === "") {
                    var ur = "laboratorio/" + tp_reporte + "/" + id_periodo + "/" + coordinador + "";
                } else {
                    if (id_periodo !== "" && fecha !== "" && sesion === "") {
                        var ur = "laboratorio/" + tp_reporte + "/" + id_periodo + "/" + coordinador + "/" + fecha + "";
                    } else {
                        var ur = "laboratorio/" + tp_reporte + "/" + id_periodo + "/" + coordinador + "/" + fecha + "/" + sesion + "";
                    }
                }
                var url = ur;
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#formulariol").serialize(),
                    success: function (data)
                    {
                        $("#laboratorio").html(data);
                        document.getElementById("loading").style.display = "none";
                    }
                });
            }
            return false;
            //alert(data);
        });

    });

</script>

    <ol class="breadcrumb">
        <li><a href="exportaruno/{{ $idperiodo }}/excel">
            <img src="images/excel.png" title="DESCARGAR EXCEL" style=" height: 23px; margin-right: -13px;">
        </a> &nbsp; &nbsp; &nbsp;</li>
        <li><a href="#" class="azona">Zona</a></li>
        <li><a href="#" class="aprovincia">Provincia</a></li>
        <li><a href="#" class="adistrito">Distrito</a></li>
        <li class="active">Monitor</li>
    </ol>

    <div class="box table-responsive no-padding">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-paste"></i> <b> Reporte Estado de Distrito por Monitor</b></h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            {!! Form::open(['method' => 'POST', 'id' => 'formulariol' ]) !!}
            <table class="table table-bordered table-striped table-hover">
                <tr style="background-color: #0489B1; color: white;"> 
                    <th style="text-align:center;" >
                        <label><input type="checkbox" id="checkMonitor_l"> MONITOR </label>
                    </th>
                    <th style="text-align: center;">DISTRITO</th>
                    <th style="text-align: center;">ZONA</th>
                    <th style="text-align: center;">N_LABORATORIO</th>
                    <th style="text-align: center;">ÓPTIMOS</th>
                    <th style="text-align: center;">% ÓPTIMO</th>
                    <th style="text-align: center;">POR GESTIONAR</th>
                    <th style="text-align: center;">% POR GESTIONAR</th>
                    <th style="text-align: center;">RECOMENDABLE</th>
                    <th style="text-align: center;">% RECOMENDABLE</th>
                    <th style="text-align: center;">POR CONTACTAR</th>
                    <th style="text-align: center;">% POR CONTACTAR</th>
                </tr> 
                <?php $i = 0; ?>
                @foreach($valida_lab as $monitor)
                <?php $i++ ?>
                <tr>
                    <td style="text-align: center;">
                        <input type="checkbox" id="monitor_l{{ $i }}" value="{{ $monitor->monitor }}" name="monitor[]"> &nbsp; {{ $monitor->monitor }}
                    </td>
                    <td style="text-align: center;">
                        <input type="checkbox" id="distrito_l{{ $i }}" value="{{ $monitor->distrito }}" name="distrito[]" hidden > &nbsp; {{ $monitor->distrito }}
                    </td>
                    <td style="text-align: center;">{{ $monitor->zona }}</td>
                    <td style="text-align: center;">{{ $monitor->laboratorio }}</td>                                           
                    <td style="text-align: right;">{{ $monitor->optimo }}</td>
                    <?php
                    if ($monitor->laboratorio == 0) {
                        $porcenoptimol = (number_format((($monitor->optimo / 1 ) * 100), 2, ',', ' '));
                        $porceporgestionar = (number_format((($monitor->porgestionar / 1 ) * 100), 2, ',', ' '));
                        $porcerecomendablel = (number_format((($monitor->recomendable / 1 ) * 100), 2, ',', ' '));
                        $porcenorecomendablel = (number_format((($monitor->norecomendable / 1 ) * 100), 2, ',', ' '));
                    } else {
                        $porcenoptimol = (number_format((($monitor->optimo / $monitor->laboratorio ) * 100), 2, ',', ' '));
                        $porceporgestionar = (number_format((($monitor->porgestionar / $monitor->laboratorio ) * 100), 2, ',', ' '));
                        $porcerecomendablel = (number_format((($monitor->recomendable / $monitor->laboratorio ) * 100), 2, ',', ' '));
                        $porcenorecomendablel = (number_format((($monitor->norecomendable / $monitor->laboratorio ) * 100), 2, ',', ' '));
                    }
                    if ($porcenoptimol <= 45) {
                        ?>
                        <td style=" text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcenoptimol }} % </b></td>
                    <?php } elseif ($porcenoptimol > 45 && $porcenoptimol <= 80) { ?>
                        <td style=" text-align: right; background-color: #D7DF01; color: white;"><b> {{ $porcenoptimol }} % </b></td>                                                
                    <?php } elseif ($porcenoptimol > 80 && $porcenoptimol <= 99) { ?>
                        <td style=" text-align: right; background-color: #A5DF00; color: white;"><b> {{ $porcenoptimol }} % </b></td>                                                
                    <?php } elseif ($porcenoptimol >= 100) { ?>
                        <td style=" text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcenoptimol }} % </b></td>    
                    <?php } ?>

                    <td style="text-align: right;"> {{ $monitor->porgestionar }} </td>
                    <?php if ($porceporgestionar <= 45) { ?>
                        <td style=" text-align: right; background-color: #FA5858; color: white;"><b> {{ $porceporgestionar }} % </b></td>
                    <?php } elseif ($porceporgestionar > 45 && $porceporgestionar <= 80) { ?>
                        <td style=" text-align: right; background-color: #D7DF01; color: white;"><b> {{ $porceporgestionar }} % </b></td>                                                
                    <?php } elseif ($porceporgestionar > 80 && $porceporgestionar <= 99) { ?>
                        <td style=" text-align: right; background-color: #A5DF00; color: white;"><b> {{ $porceporgestionar }} % </b></td>                                                
                    <?php } elseif ($porceporgestionar >= 100) { ?>
                        <td style=" text-align: right; background-color: #5FB404; color: white;"><b> {{ $porceporgestionar }} % </b></td>    
                    <?php } ?>

                    <td style="text-align: right;">{{ $monitor->recomendable }}</td>
                    <?php if ($porcerecomendablel <= 45) { ?>
                        <td style=" text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcerecomendablel }} % </b></td>
                    <?php } elseif ($porcerecomendablel > 45 && $porcerecomendablel <= 80) { ?>
                        <td style=" text-align: right; background-color: #D7DF01; color: white;"><b> {{ $porcerecomendablel }} % </b></td>                                                
                    <?php } elseif ($porcerecomendablel > 80 && $porcerecomendablel <= 99) { ?>
                        <td style=" text-align: right; background-color: #A5DF00; color: white;"><b> {{ $porcerecomendablel }} % </b></td>                                                
                    <?php } elseif ($porcerecomendablel >= 100) { ?>
                        <td style=" text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcerecomendablel }} % </b></td>    
                    <?php } ?>
                    <td style="text-align: right;">{{ $monitor->norecomendable }}</td>
                    <?php if ($porcenorecomendablel <= 45) { ?>
                        <td style=" text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcenorecomendablel }} % </b></td>
                    <?php } elseif ($porcenorecomendablel > 45 && $porcenorecomendablel <= 80) { ?>
                        <td style=" text-align: right; background-color: #D7DF01; color: white;"><b> {{ $porcenorecomendablel }} % </b></td>                                                
                    <?php } elseif ($porcenorecomendablel > 80 && $porcenorecomendablel <= 99) { ?>
                        <td style=" text-align: right; background-color: #A5DF00; color: white;"><b> {{ $porcenorecomendablel }} % </b></td>                                                
                    <?php } elseif ($porcenorecomendablel >= 100) { ?>
                        <td style=" text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcenorecomendablel }} % </b></td>    
                <?php } ?>
                </tr>
                @endforeach
                <?php
                if ($totalLab == 0) {
                    $porceoptimol = (number_format((($totalOptimo / 1) * 100), 2, ',', ' '));
                    $porcegestionar = (number_format((($totalgestionar / 1) * 100), 2, ',', ' '));
                    $porcerecomel = (number_format((($totalRecomen / 1) * 100), 2, ',', ' '));
                    $porcenorecomel = (number_format((($totalNoReco / 1) * 100), 2, ',', ' '));
                } else {
                    $porceoptimol = (number_format((($totalOptimo / $totalLab) * 100), 2, ',', ' '));
                    $porcegestionar = (number_format((($totalgestionar / $totalLab) * 100), 2, ',', ' '));
                    $porcerecomel = (number_format((($totalRecomen / $totalLab) * 100), 2, ',', ' '));
                    $porcenorecomel = (number_format((($totalNoReco / $totalLab) * 100), 2, ',', ' '));
                }
                ?>
                <tr>
                    <td style="text-align: center; background-color: #0489B1; color: white;" colspan="3"><b>TOTALES</b></td>    
                    <td style="text-align: center; background-color: #0489B1; color: white;"><b> {{ $totalLab }} </b></td>
                    <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ $totalOptimo }} </b></td>
                    <?php if ($porceoptimol <= 45) { ?>
                        <td style=" text-align: right; background-color: #FA5858; color: white;"><b> {{ $porceoptimol }}% </b></td>
                    <?php } elseif ($porceoptimol > 45 && $porceoptimol <= 80) { ?>
                        <td style=" text-align: right; background-color: #D7DF01; color: white;"><b> {{ $porceoptimol }} % </b></td>                                                
                    <?php } elseif ($porceoptimol > 80 && $porceoptimol <= 99) { ?>
                        <td style=" text-align: right; background-color: #A5DF00; color: white;"><b> {{ $porceoptimol }} % </b></td>                                                
<?php } elseif ($porceoptimol >= 100) { ?>
                        <td style=" text-align: right; background-color: #5FB404; color: white;"><b> {{ $porceoptimol }} % </b></td>    
                    <?php } ?>

                    <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ $totalgestionar }} </b></td>                                            
                    <?php if ($porcegestionar <= 45) { ?>
                        <td style=" text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcegestionar }}% </b></td>
                    <?php } elseif ($porcegestionar > 45 && $porcegestionar <= 80) { ?>
                        <td style=" text-align: right; background-color: #D7DF01; color: white;"><b> {{ $porcegestionar }} % </b></td>                                                
                    <?php } elseif ($porcegestionar > 80 && $porcegestionar <= 99) { ?>
                        <td style=" text-align: right; background-color: #A5DF00; color: white;"><b> {{ $porcegestionar }} % </b></td>                                                
<?php } elseif ($porcegestionar >= 100) { ?>
                        <td style=" text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcegestionar }} % </b></td>    
                    <?php } ?>

                    <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ $totalRecomen }} </b></td>                                            
                    <?php if ($porcerecomel <= 45) { ?>
                        <td style=" text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcerecomel }}% </b></td>
                    <?php } elseif ($porcerecomel > 45 && $porcerecomel <= 80) { ?>
                        <td style=" text-align: right; background-color: #D7DF01; color: white;"><b> {{ $porcerecomel }} % </b></td>                                                
                    <?php } elseif ($porcerecomel > 80 && $porcerecomel <= 99) { ?>
                        <td style=" text-align: right; background-color: #A5DF00; color: white;"><b> {{ $porcerecomel }} % </b></td>                                                
                    <?php } elseif ($porcerecomel >= 100) { ?>
                        <td style=" text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcerecomel }} % </b></td>    
                    <?php } ?> 
                    <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ $totalNoReco }} </b></td>
                    <?php if ($porcenorecomel <= 45) { ?>
                        <td style=" text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcenorecomel }}% </b></td>
                    <?php } elseif ($porcenorecomel > 45 && $porcenorecomel <= 80) { ?>
                        <td style=" text-align: right; background-color: #D7DF01; color: white;"><b> {{ $porcenorecomel }} % </b></td>                                                
                    <?php } elseif ($porcenorecomel > 80 && $porcenorecomel <= 99) { ?>
                        <td style=" text-align: right; background-color: #A5DF00; color: white;"><b> {{ $porcenorecomel }} % </b></td>                                                
<?php } elseif ($porcenorecomel >= 100) { ?>
                        <td style=" text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcenorecomel }} % </b></td>    
<?php } ?>
                </tr>
                <tr>
                    <td colspan="12"> 
                        <!-- {!! Form::submit('Para consulta de Distrito por Zona hacer Click aqui', ['class' => 'btn btn-block btn-primary', 'onclick' => 'alert(hola);']) !!} -->
                        <button class="btn btn-block btn-info" type="submit" id="enviar_monitor"><b>Para consulta de Laboratorio por Monitor hacer Click aqui</b></button>
                    </td>
                </tr>
            </table>
            {!! Form::close() !!}
        </div>
    </div>                                               
