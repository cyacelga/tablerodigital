<!DOCTYPE html>
<html>
        
     
    <script type="text/javascript">
              
        function buscar_datos() {
            
           // var dialogo = $("#dialogo");
           // dialogo.dialog({
           //    open: function(ui){ $(".ui-dialog-titlebar-close", ui.dialogo).hide(); } 
           // });
            
            var id_periodo = $("#periodoLab").val();
            var fecha = $("#fecha_programada").val();
            var sesion = $("#sesion").val();
            var tp_reporte = $("#tp_reporte").val();
            var coordinador = $("#coordinador").val();
            
            if (id_periodo === "" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "0") {
                alert("Debe seleccionar un periodo");
            } else {
                $("#loading").css("display","block");
                if (id_periodo !== "" && fecha === "" && sesion === "") {
                    var url = "reporte_general/" + tp_reporte + "/" + id_periodo + "/" + coordinador + "";
                } else {
                    if (id_periodo !== "" && fecha !== "" && sesion === "") {
                        var url = "reporte_general/" + tp_reporte + "/" + id_periodo + "/" + coordinador +  "/" + fecha + "";
                    } else {
                        var url = "reporte_general/" + tp_reporte + "/" + id_periodo + "/" + coordinador +  "/" + fecha + "/" + sesion + "";
                    }
                }
                $("#contenido").html();
                $.get(url, function(resul) {
                    $("#contenido").html(resul);
                    // dialogo.dialog("close");
                    $("#loading").css("display","none")
                });
            }
        }
        
         function mostrarOpcion(){
            var tp_reporte = $("#tp_reporte").val();
            
            if(tp_reporte !==''){
               $("#contenido").html('');
               $('#seleccion').css('display','block');
            }else{
               $('#seleccion').css('display','none');
            }
        }       
           
                           
    </script>

    <body class="hold-transition skin-blue sidebar-mini">
       
               
                <div>
		<section class="content-header">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <select class="form-control" id="tp_reporte" onchange="mostrarOpcion();" >
                                <option value="">Tipo de Reporte</option>
                                <option value="validacion_lab">Validacion de Laboratorios</option> 
                                <option value="llamadas_rectores">Llamadas a Responsable de Sede</option>
				<!-- <option value="llamadas_aplicadores">Llamadas a Aplicadores</option> -->
                            </select>
                        </div>
                    </div><br>
                    <div class="row" id="seleccion" style="display: none;">
                        <div class="col-sm-12 col-md-8">
                             <select class="form-control" id="periodoLab">
                                <option value="0"> Seleccione Periodo </option>
                                @foreach($periodos as $periodo)
                                <option value="{{ $periodo->cgi_periodo_id }}">{{ $periodo->name_periodo }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-12 col-md-3" hidden>
                            <select class="form-control" id="fecha_programada" name="fecha_programada">
                                <option value="">Seleccione Fecha</option>
                            </select>                                                        
                        </div>
                        <div class="col-sm-12 col-md-3" hidden>
                            <select class="form-control" id="sesion">
                                <option value="">Seleccione Sesion</option>
                            </select>
                        </div>
                        <div class="col-sm-12 col-md-2">
                            <select class="form-control" id="coordinador">
                                <option value="">Coordinador</option>
                            </select>
                        </div>
                        <div class="col-sm-12 col-md-2">
                            <button class="btn btn-block btn-info" type="button" onclick="buscar_datos();"><b>Buscar</b></button>
                        </div>
                    </div>

                </section>                
                </div>
                <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
                <!-- Main content -->
                <div class="row" id="load" style="display: none;">
                        <img src="images/loading1.gif" style='margin:0 auto; position: absolute; top: 50%; left: 95%; margin: -230px 0 0 -30px;'>
                    </div>
                 <div class="row" id="loading" style="display: none;">
                        <img src="images/loading0.gif" style='margin:0 auto; position: absolute; top: 50%; left: 50%; margin: -30px 0 0 -30px;'>
                    </div>

                <section id="contenido" class="content">
                   
                </section><!-- /.content -->


        <!-- Control Sidebar -->
        <!-- /.control-sidebar -->
        <!-- Add the sidebar's background. This div must be placed
             immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
    <script src="js/selectCoord_rector.js"></script> <!-- consulta de los select -->
    
</body>
</html>
