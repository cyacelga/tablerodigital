<script>
    $("#checkprovincias").change(function(){
        var i=0;
    @foreach($valida_lab as $valprovincia)
        i = i + 1;
            $("#prov"+i).prop('checked', $(this).prop("checked"));
    @endforeach;    
    });
    
    $(function(){
    $(".azona").click(function(){
    $(".zona").css("display", "block");
    $(".provincia").css("display", "none");
    $(".distritod").css("display", "none");
    $(".monitord").css("display", "none");
    $(".laboratoriod").css("display", "none");
    });
    });
    
    $(function(){
    $("#enviar_provincia").click(function(){
    $(".provincia").css("display", "none");
    $(".distritod").css("display", "block");
    document.getElementById("loading").style.display = "block";
    var id_periodo = $("#periodoLab").val();
    var fecha = $("#fecha_programada").val();
    var sesion = $("#sesion").val();
    var tp_reporte = $("#tp_reporte").val();
    if (id_periodo === "" && fecha === "" && sesion === "") {
    alert('Debe Seleccionar un Periodo');
    } else{
    if (id_periodo !== "" && fecha === "" && sesion === "") {
    var ur = "distrito/" + tp_reporte + "/" + id_periodo + "";
    } else {
    if (id_periodo !== "" && fecha !== "" && sesion === "") {
    var ur = "distrito/" + tp_reporte + "/" + id_periodo + "/" + fecha + "";
    } else {
    var ur = "distrito/" + tp_reporte + "/" + id_periodo + "/" + fecha + "/" + sesion + "";
    }
    }
    var url = ur;
    $.ajax({
    type: "POST",
            url: url,
            data: $("#formulariod").serialize(),
            success: function(data)
            {
            $("#distrito").html(data);
            document.getElementById("loading").style.display = "none";
            }
    });
    }
    return false;
    //alert(data);
    });
    });

</script>

    <ol class="breadcrumb">
        <li><a href="exportaruno/{{ $idperiodo }}/excel">
                <img src="images/excel.png" title="DESCARGAR EXCEL" style=" height: 23px; margin-right: -13px;">
            </a> &nbsp; &nbsp; &nbsp;</li>
        <li><a href="#" class="azona">Zonas</a></li>
        <li class="active">Provincias</li>
    </ol>

    <div class="box table-responsive no-padding">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-paste"></i> <b> Reporte Estado de Provincia por Zona</b></h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            {!! Form::open(['method' => 'POST', 'id' => 'formulariod' ]) !!}
            <table class="table table-bordered table-striped table-hover" style="width: auto;">
                <tr style="background-color: #0489B1; color: white;"> 
                    <th style="text-align:center;" >
                        <label>
                            <input type="checkbox" id="checkprovincias"> <b>provincia</b>                                                
                        </label>
                    </th>
                    <th style="text-align: center;">ZONA</th>
                    <th style="text-align: center;">N_LABORATORIO</th>
                    <th style="text-align: center;">ÓPTIMOS</th>
                    <th style="text-align: center;">% ÓPTIMO</th>
                    <th style="text-align: center;">POR GESTIONAR</th>
                    <th style="text-align: center;">% POR GESTIONAR</th>
                    <th style="text-align: center;">RECOMENDABLE</th>
                    <th style="text-align: center;">% RECOMENDABLE</th>
                    <th style="text-align: center;">POR CONTACTAR</th>
                    <th style="text-align: center;">% POR CONTACTAR</th>
                </tr>
                 <?php $i = 0;?>
                @foreach($valida_lab as $provincia)  
                 <?php $i++; ?>
                <tr>
                    <td style="text-align: left;"> 
                        <label>
                            <input type="checkbox" id="prov{{ $i }}" value="{{ $provincia->provincia }}" name="provincia[]">
                            {{ $provincia->provincia }}
                        </label>
                    </td>
                    <td style="text-align: center;">{{ $provincia->zona }}</td>                                           
                    <td style="text-align: right;"> {{ $provincia->laboratorio }} </td>
                    <td style="text-align: right;">{{ $provincia->optimo }}</td>
                    <?php
                    if ($provincia->laboratorio == 0) {
                        $porcenoptimol = (number_format((($provincia->optimo / 1 ) * 100), 2, ',', ' '));
                        $porceporgestionar = (number_format((($provincia->porgestionar / 1 ) * 100), 2, ',', ' '));
                        $porcerecomendablel = (number_format((($provincia->recomendable / 1 ) * 100), 2, ',', ' '));
                        $porcenorecomendablel = (number_format((($provincia->norecomendable / 1 ) * 100), 2, ',', ' '));
                    } else {
                        $porcenoptimol = (number_format((($provincia->optimo / $provincia->laboratorio ) * 100), 2, ',', ' '));
                        $porceporgestionar = (number_format((($provincia->porgestionar / $provincia->laboratorio ) * 100), 2, ',', ' '));
                        $porcerecomendablel = (number_format((($provincia->recomendable / $provincia->laboratorio ) * 100), 2, ',', ' '));
                        $porcenorecomendablel = (number_format((($provincia->norecomendable / $provincia->laboratorio ) * 100), 2, ',', ' '));
                    }
                    if ($porcenoptimol <= 45) {
                        ?>
                        <td style=" text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcenoptimol }} % </b></td>
                    <?php } elseif ($porcenoptimol > 45 && $porcenoptimol <= 80) { ?>
                        <td style=" text-align: right; background-color: #D7DF01; color: white;"><b> {{ $porcenoptimol }} % </b></td>                                                
                    <?php } elseif ($porcenoptimol > 80 && $porcenoptimol <= 99) { ?>
                        <td style=" text-align: right; background-color: #A5DF00; color: white;"><b> {{ $porcenoptimol }} % </b></td>                                                
                    <?php } elseif ($porcenoptimol >= 100) { ?>
                        <td style=" text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcenoptimol }} % </b></td>    
                    <?php } ?>

                    <td style="text-align: right;"> {{ $provincia->porgestionar }} </td>
                    <?php if ($porceporgestionar <= 45) { ?>
                        <td style=" text-align: right; background-color: #FA5858; color: white;"><b> {{ $porceporgestionar }} % </b></td>
                    <?php } elseif ($porceporgestionar > 45 && $porceporgestionar <= 80) { ?>
                        <td style=" text-align: right; background-color: #D7DF01; color: white;"><b> {{ $porceporgestionar }} % </b></td>                                                
                    <?php } elseif ($porceporgestionar > 80 && $porceporgestionar <= 99) { ?>
                        <td style=" text-align: right; background-color: #A5DF00; color: white;"><b> {{ $porceporgestionar }} % </b></td>                                                
                    <?php } elseif ($porceporgestionar >= 100) { ?>
                        <td style=" text-align: right; background-color: #5FB404; color: white;"><b> {{ $porceporgestionar }} % </b></td>    
                    <?php } ?>

                    <td style="text-align: right;">{{ $provincia->recomendable }}</td>
                    <?php if ($porcerecomendablel <= 45) { ?>
                        <td style=" text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcerecomendablel }} % </b></td>
                    <?php } elseif ($porcerecomendablel > 45 && $porcerecomendablel <= 80) { ?>
                        <td style=" text-align: right; background-color: #D7DF01; color: white;"><b> {{ $porcerecomendablel }} % </b></td>                                                
                    <?php } elseif ($porcerecomendablel > 80 && $porcerecomendablel <= 99) { ?>
                        <td style=" text-align: right; background-color: #A5DF00; color: white;"><b> {{ $porcerecomendablel }} % </b></td>                                                
                    <?php } elseif ($porcerecomendablel >= 100) { ?>
                        <td style=" text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcerecomendablel }} % </b></td>    
                    <?php } ?>
                    <td style="text-align: right;">{{ $provincia->norecomendable }}</td>
                    <?php if ($porcenorecomendablel <= 45) { ?>
                        <td style=" text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcenorecomendablel }} % </b></td>
                    <?php } elseif ($porcenorecomendablel > 45 && $porcenorecomendablel <= 80) { ?>
                        <td style=" text-align: right; background-color: #D7DF01; color: white;"><b> {{ $porcenorecomendablel }} % </b></td>                                                
                    <?php } elseif ($porcenorecomendablel > 80 && $porcenorecomendablel <= 99) { ?>
                        <td style=" text-align: right; background-color: #A5DF00; color: white;"><b> {{ $porcenorecomendablel }} % </b></td>                                                
                    <?php } elseif ($porcenorecomendablel >= 100) { ?>
                        <td style=" text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcenorecomendablel }} % </b></td>    
                    <?php } ?>
                </tr>
                @endforeach
                <?php
                if ($totalLab == 0) {
                    $porceoptimol = (number_format((($totalOptimo / 1) * 100), 2, ',', ' '));
                    $porcegestionar = (number_format((($totalgestionar / 1) * 100), 2, ',', ' '));
                    $porcerecomel = (number_format((($totalRecomen / 1) * 100), 2, ',', ' '));
                    $porcenorecomel = (number_format((($totalNoReco / 1) * 100), 2, ',', ' '));
                } else {
                    $porceoptimol = (number_format((($totalOptimo / $totalLab) * 100), 2, ',', ' '));
                    $porcegestionar = (number_format((($totalgestionar / $totalLab) * 100), 2, ',', ' '));
                    $porcerecomel = (number_format((($totalRecomen / $totalLab) * 100), 2, ',', ' '));
                    $porcenorecomel = (number_format((($totalNoReco / $totalLab) * 100), 2, ',', ' '));
                }
                ?>
                <tr>
                    <td style="text-align: center; background-color: #0489B1; color: white;" colspan="2"><b>TOTALES</b></td>    
                    <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ $totalLab }} </b></td>
                    <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ $totalOptimo }} </b></td>
                    <?php if ($porceoptimol <= 45) { ?>
                        <td style=" text-align: right; background-color: #FA5858; color: white;"><b> {{ $porceoptimol }}% </b></td>
                    <?php } elseif ($porceoptimol > 45 && $porceoptimol <= 80) { ?>
                        <td style=" text-align: right; background-color: #D7DF01; color: white;"><b> {{ $porceoptimol }} % </b></td>                                                
                    <?php } elseif ($porceoptimol > 80 && $porceoptimol <= 99) { ?>
                        <td style=" text-align: right; background-color: #A5DF00; color: white;"><b> {{ $porceoptimol }} % </b></td>                                                
                    <?php } elseif ($porceoptimol >= 100) { ?>
                        <td style=" text-align: right; background-color: #5FB404; color: white;"><b> {{ $porceoptimol }} % </b></td>    
                    <?php } ?>

                    <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ $totalgestionar }} </b></td>                                            
                    <?php if ($porcegestionar <= 45) { ?>
                        <td style=" text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcegestionar }}% </b></td>
                    <?php } elseif ($porcegestionar > 45 && $porcegestionar <= 80) { ?>
                        <td style=" text-align: right; background-color: #D7DF01; color: white;"><b> {{ $porcegestionar }} % </b></td>                                                
                    <?php } elseif ($porcegestionar > 80 && $porcegestionar <= 99) { ?>
                        <td style=" text-align: right; background-color: #A5DF00; color: white;"><b> {{ $porcegestionar }} % </b></td>                                                
                    <?php } elseif ($porcegestionar >= 100) { ?>
                        <td style=" text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcegestionar }} % </b></td>    
                    <?php } ?>

                    <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ $totalRecomen }} </b></td>
                    <?php if ($porcerecomel <= 45) { ?>
                        <td style=" text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcerecomel }}% </b></td>
                    <?php } elseif ($porcerecomel > 45 && $porcerecomel <= 80) { ?>
                        <td style=" text-align: right; background-color: #D7DF01; color: white;"><b> {{ $porcerecomel }} % </b></td>                                                
                    <?php } elseif ($porcerecomel > 80 && $porcerecomel <= 99) { ?>
                        <td style=" text-align: right; background-color: #A5DF00; color: white;"><b> {{ $porcerecomel }} % </b></td>                                                
                    <?php } elseif ($porcerecomel >= 100) { ?>
                        <td style=" text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcerecomel }} % </b></td>    
                    <?php } ?>                                              
                    <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ $totalNoReco }} </b></td>
                    <?php if ($porcenorecomel <= 45) { ?>
                        <td style=" text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcenorecomel }}% </b></td>
                    <?php } elseif ($porcenorecomel > 45 && $porcenorecomel <= 80) { ?>
                        <td style=" text-align: right; background-color: #D7DF01; color: white;"><b> {{ $porcenorecomel }} % </b></td>                                                
                    <?php } elseif ($porcenorecomel > 80 && $porcenorecomel <= 99) { ?>
                        <td style=" text-align: right; background-color: #A5DF00; color: white;"><b> {{ $porcenorecomel }} % </b></td>                                                
                    <?php } elseif ($porcenorecomel >= 100) { ?>
                        <td style=" text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcenorecomel }} % </b></td>    
                    <?php } ?>
                </tr>
                <tr>
                    <td colspan="11"> 
                        <!-- {!! Form::submit('Para consulta de provincia por Zona hacer Click aqui', ['class' => 'btn btn-block btn-primary', 'onclick' => 'alert(hola);']) !!} -->
                        <button class="btn btn-block btn-info" type="submit" id="enviar_provincia"><b>Para consulta de Distrito por Provincia hacer Click aqui</b></button>
                    </td>
                </tr>
            </table>
            {!! Form::close() !!}
        </div>
    </div>                                               