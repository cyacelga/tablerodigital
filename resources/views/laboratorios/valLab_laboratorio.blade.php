<script type="text/javascript">
$(function(){
    $(".amonitor").click(function(){
    $(".zona").css("display","none");        
    $(".distritod").css("display","none");        
    $(".monitord").css("display","block");        
    $(".laboratoriod").css("display","none");        
    });
});
</script>

<ol class="breadcrumb">
    <li><a href="exportaruno/{{ $idperiodo }}/excel">
            <img src="images/excel.png" title="DESCARGAR EXCEL" style=" height: 23px; margin-right: -13px;">
        </a> &nbsp; &nbsp; &nbsp;</li>
    <li><a href="#" class="azona">Zona</a></li>
    <li><a href="#" class="aprovincia">Provincia</a></li>
    <li><a href="#" class="adistrito">Distrito</a></li>
    <li><a href="#" class="amonitor">Monitor</a></li>
    <li class="active">Laboratorio</li>
</ol>

<div class="col-sm-12">
                            <div class="box table-responsive no-padding">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><i class="fa fa-paste"></i> <b> Reporte Estado de Laboratorios</b></h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                     <table class="table table-bordered table-striped table-hover">
                                       <tr style="background-color: #0489B1; color: white;">                                             
                                            <th style="text-align: center;">N°</th>
                                            <th style="text-align: center;">LABORATORIO</th>
                                            <th style="text-align:center;" > MONITOR </th>
                                            <th style="text-align: center;">DISTRITO</th>
                                            <th style="text-align: center;">ZONA</th>
                                            <th style="text-align: center;">TIPO</th>
                                            <th style="text-align: center;">REQUERIDAS</th>
                                            <th style="text-align: center;">VALIDADAS</th>
                                            <th style="text-align: center;">EQUI. FALTANTES</th>
                                            <th style="text-align: center;">IMPRESORA</th>
                                            <th style="text-align: center;">INTERNET</th>
                                            <th style="text-align: center;">OBSERVACIÓN</th>
                                            <th style="text-align: center;">VALIDACIÓN</th>
                                        </tr> 
                                        <?php $i = 0;?>
                                        @foreach( $valida_labo as $vlab )
                                        <?php $i++;?>
                                        <tr>
                                            <td style="text-align: center;"><b> {{ $i }} </b></td>
                                            <td style="text-align: center;">{{ $vlab->laboratorio }}</td>
                                            <td style="text-align: center;">{{ $vlab->monitor }}</td>
                                            <td style="text-align: center;">{{ $vlab->distrito }}</td>
                                            <td style="text-align: center;">{{ $vlab->zona }}</td>
                                            <td style="text-align: center;">{{ $vlab->tipo }}</td>                                           
                                            <td style="text-align: right;">{{ $vlab->maxasignada }}</td>                                            
                                            <td style=" text-align: right;">{{ $vlab->computadorc }}</td>
                                            <td style="text-align: right;">{{ $vlab->maxasignada - $vlab->computadorc }}</td>
                                            <td style=" text-align: right;">{{ $vlab->impresorac }}</td>
                                            <td style="text-align: right;">{{ $vlab->internetc }}</td>
                                            <td style="text-align: right;">{{ $vlab->observacion }}</td>
                                            <?php if( $vlab->dictamen == 'POR CONTACTAR'){ ?>
                                            <td style=" text-align: center; color: white; background-color: #FA5858;">
                                            <?php }elseif( $vlab->dictamen == 'RECOMENDABLE' || $vlab->dictamen == 'POR GESTIONAR' ){ ?>    
                                            <td style=" text-align: center; color: white; background-color: #D7DF01;">    
                                            <?php }elseif( $vlab->dictamen == 'OPTIMO'){ ?>        
                                            <td style=" text-align: center; color: white; background-color: #5FB404;">        
                                            <?php } ?>
                                            {{ $vlab->dictamen }}</td>
                                        </tr>                                         
                                        @endforeach
                                    </table>
                                </div>
                            </div>                                               
                        </div>                                                    

                    