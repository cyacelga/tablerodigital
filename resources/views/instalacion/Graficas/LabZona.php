<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript">
            google.charts.load("current", {packages: ['corechart']});
            google.charts.setOnLoadCallback(drawChart);
            function drawChart() {
                var data = google.visualization.arrayToDataTable([
                    ['Zona', 'Si', 'No'],
                   <?php 
                $provincia= $_REQUEST['provincia'];  
                $periodo= $_REQUEST['periodo']; 
                $fecha= $_REQUEST['fecha'];
                $sesion= $_REQUEST['sesion'];
                $user = "dact_tablero1";
                $password = "dact123456";
                $dbname = "evaluacion";
                $port = "5432";
                $host = "192.168.250.119";
                $cadenaConexion = "host=$host port=$port dbname=$dbname user=$user password=$password";
                $conexion = pg_connect($cadenaConexion) or die("Error en la Conexión: " . pg_last_error());
                $query = "select  cast('Instalación' as text ) as tipo,instalosi :: numeric as  si,instalono ::numeric  as no
 from(
SELECT  zona, count(cgi_laboratorio_id) as laboratorio,
--count(case when descarga_aplicativo ='' then 'NO' when descarga_aplicativo is null then 'NO' when descarga_aplicativo ='NO' then 'NO' end) as descargano,
--count(case when descarga_aplicativo ='SI' then 'SI' end) as descargasi,
count(case when instalo_aplicativo ='' then 'NO' when instalo_aplicativo is null then 'NO' when instalo_aplicativo ='NO' then 'NO' end) as instalono,
count(case when instalo_aplicativo ='SI' then 'SI' end) as instalosi
--count(case when estado_sesion ='' then 'NO' when estado_sesion is null then 'NO' when estado_sesion ='NO' then 'NO' end) as sesionno,
--count(case when estado_sesion ='SI' then 'SI' end) as sesionsi 
FROM umonitoreo
WHERE cgi_periodo_id=$periodo AND estado=1 and zona='$provincia' $fecha $sesion group by zona order by zona, laboratorio
) as a ";
                
                $resultado = pg_query($conexion, $query) or die("Error en la Consulta SQL");
                $numReg = pg_num_rows($resultado);
                if ($numReg > 0) {
                    while ($fila = pg_fetch_array($resultado)) {
                        echo "['" . $fila['tipo'] . "',";
                        echo $fila['si'] . ",".$fila['no'] ."],";
                    }
                } else {
                    echo "No hay Registros";
                }
                pg_close($conexion);
                ?>
                ]);

                var view = new google.visualization.DataView(data);
                view.setColumns([0, 1,
                    {calc: "stringify",
                        sourceColumn: 1,
                        type: "string",
                        role: "annotation"},
                    2]);

                var options = {
                    title: " <?php
                            $provincia = $_REQUEST['provincia'];
                            //$provincia='MANABI';
                            $periodo= $_REQUEST['periodo']; 
                            $fecha= $_REQUEST['fecha'];
                            $sesion= $_REQUEST['sesion'];
                            $user = "dact_tablero1";
                            $password = "dact123456";
                            $dbname = "evaluacion";
                            $port = "5432";
                            $host = "192.168.250.119";
                            $cadenaConexion = "host=$host port=$port dbname=$dbname user=$user password=$password";
                            $conexion = pg_connect($cadenaConexion) or die("Error en la Conexión: " . pg_last_error());
                            $query = "select laboratorio
 from(
SELECT  zona, count(cgi_laboratorio_id) as laboratorio,
--count(case when descarga_aplicativo ='' then 'NO' when descarga_aplicativo is null then 'NO' when descarga_aplicativo ='NO' then 'NO' end) as descargano,
--count(case when descarga_aplicativo ='SI' then 'SI' end) as descargasi,
count(case when instalo_aplicativo ='' then 'NO' when instalo_aplicativo is null then 'NO' when instalo_aplicativo ='NO' then 'NO' end) as instalono,
count(case when instalo_aplicativo ='SI' then 'SI' end) as instalosi
--count(case when estado_sesion ='' then 'NO' when estado_sesion is null then 'NO' when estado_sesion ='NO' then 'NO' end) as sesionno,
--count(case when estado_sesion ='SI' then 'SI' end) as sesionsi 
FROM umonitoreo
WHERE cgi_periodo_id=$periodo AND estado=1 and zona='$provincia' $fecha $sesion group by zona order by zona, laboratorio
) as a 
";

                            $resultado = pg_query($conexion, $query) or die("Error en la Consulta SQL");
                            $numReg = pg_num_rows($resultado);
                            if ($numReg > 0) {
                                while ($fila = pg_fetch_array($resultado)) {
                                    echo 'Total laboratorios: ' . $fila['laboratorio'];
                                }
                            } else {
                                echo "No hay Registros";
                            }
                            pg_close($conexion);
                            ?>",
                    isStacked: 'percent',
                    colors: ['#007900', '#FF0022'],   
                   // height: 450,
                    width: 630,
                    height: 400,
                    legend: {position: 'top', maxLines: 3},
                    bar:{groupWidth: "20%"},
                    vAxis: {
                        minValue: 0,
                        ticks: [0, .3, .6, .8, 1]
                    }
                };





                var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
                chart.draw(view, options);
            }
        </script>
        <div id="columnchart_values" style="width: 900px; height: 300px;"></div>
    </body>
</html>


