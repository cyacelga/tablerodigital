<?php

function carga($provincia, $id_periodo, $fecha, $sesion) {
    $user = "dact_tablero1";
    $password = "dact123456";
    $dbname = "evaluacion";
    $port = "5432";
    $host = "192.168.250.119";
    $cadenaConexion = "host=$host port=$port dbname=$dbname user=$user password=$password";
    $conexion = pg_connect($cadenaConexion) or die("Error en la Conexión: " . pg_last_error());
    $query = "with tb1 as(
select distrito_id as distrito,laboratorio,instalosi,
round (((instalosi :: numeric *100)/laboratorio),0) as porcentaje
 from
(SELECT distrito_id, count(cgi_laboratorio_id) as laboratorio,
--count(case when descarga_aplicativo ='' then 'NO' when descarga_aplicativo is null then 'NO' when descarga_aplicativo ='NO' then 'NO' end) as descargano,
--count(case when descarga_aplicativo ='SI' then 'SI' end) as descargasi,
count(case when instalo_aplicativo ='' then 'NO' when instalo_aplicativo is null then 'NO' when instalo_aplicativo ='NO' then 'NO' end) as instalono,
count(case when instalo_aplicativo ='SI' then 'SI' end) as instalosi
--count(case when estado_sesion ='' then 'NO' when estado_sesion is null then 'NO' when estado_sesion ='NO' then 'NO' end) as sesionno,
--count(case when estado_sesion ='SI' then 'SI' end) as sesionsi 
FROM umonitoreo
WHERE cgi_periodo_id=$id_periodo AND estado=1 and zona='$provincia' $fecha $sesion group by distrito_id order by distrito_id, laboratorio) as a
)
select 
'D'|| distrito as distrito,
case when tb1.porcentaje is null then '#B3B3B3' 
when tb1.porcentaje=0 then '#B3B3B3' 
when tb1.porcentaje>=1 and  tb1.porcentaje<=10 then '#FF0022' 
when tb1.porcentaje>10 and  tb1.porcentaje<=20 then '#FF0022' 
when tb1.porcentaje>20 and  tb1.porcentaje<=30 then '#FF0022' 
when tb1.porcentaje>30 and  tb1.porcentaje<=40 then '#FF0022' 
when tb1.porcentaje>40 and  tb1.porcentaje<=50 then '#FFF500' 
when tb1.porcentaje>50 and  tb1.porcentaje<=60 then '#FFF500' 
when tb1.porcentaje>60 and  tb1.porcentaje<=70 then '#FFF500' 
when tb1.porcentaje>70 and  tb1.porcentaje<=80 then '#FFF500' 
when tb1.porcentaje>80 and  tb1.porcentaje<=99 then '#FFF500' 
when tb1.porcentaje>90 and  tb1.porcentaje=100 then '#007900'  end as color from tb1 where distrito is not null";

    $resultado = pg_query($conexion, $query) or die("Error en la Consulta SQL");
    $numReg = pg_num_rows($resultado);
    if ($numReg > 0) {
        while ($fila = pg_fetch_array($resultado)) {
            echo $fila['distrito'] . ":'";
            echo $fila['color'] . "',";
        }
    } else {
        echo "No hay Registros";
    }
    pg_close($conexion);
}

function coloresDistrito($provincia, $id_periodo, $fecha, $sesion) {
    return carga($provincia, $id_periodo, $fecha, $sesion);
}
?>