<div class="box box-primary">

<div class="box-header">
                  <h3 class="box-title">Usuarios Encontrados</h3>
                </div>

<div class="box-body">              
<?php 

if( count($usuarios) >0){
?>

<table id="tabla_pacientes" tableStyle="width:auto" class="table table-bordered table-striped dataTable" cellspacing="0" >
       
        <thead>
            <tr>
             <th style="width:10px">Id</th>
                <th>usuario</th>
                <th>Nombres</th>
                <th>email</th>
             
              <th>Acción</th>
            </tr>
        </thead>
<tbody>
<?php 

   foreach($usuarios as $usuario){  
?>

 <tr role="row" class="odd">
    <td class="sorting_1"><?= $usuario->id; ?></td>
    <td class="sorting_1"><?= $usuario->username; ?></td>
    <td class="mailbox-messages mailbox-name" ><a href="javascript:void(0);" onclick="mostrarficha(<?= $usuario->id; ?>);"  style="display:block"><i class="fa fa-user"></i>&nbsp;&nbsp;<?= $usuario->first_name." ".$usuario->last_name;  ?></a></td>
    <td><?= $usuario->email;  ?></td>
    <td><button class="btn  btn-skin-green btn-xs" onclick="mostrarficha(<?= $usuario->id; ?>);" ><i class="fa fa-fw fa-eye"></i>Ver</button></td>
</tr>

<?php        
}
?>


  

    </table>



    <?php


echo str_replace('/?', '?', $usuarios->render() )  ;

}
else
{

?>


<br/><div class='rechazado'><label style='color:#FA206A'>...No se ha encontrado ningun usuario...</label>  </div> 

<?php
}

?>
</div>



