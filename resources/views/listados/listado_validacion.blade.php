<div class="box box-primary">
 <div class="box-header">
 <!--<h4 class="box-title">Buscar</h4>
        <div class="input-group input-group-sm">
                            <input type="text" class="form-control" id="dato_buscado">
                            <span class="input-group-btn">
                              <button class="btn btn-info btn-flat" type="button" onclick="buscarmonitoreo();" >Buscar!</button>
                            </span>
        </div> -->           
</div>
<div class="box box-primary table-responsive">

                <div class="box-header">
                  <h3 class="box-title">Módulo de validación</h3>
                </div>

<div class="box-body">              
<?php 

if( count($laboratorios) >0){
?>
    



<table id="tabla_pacientes" Style="width:auto" class="table table-bordered table-striped dataTable" cellspacing="0">
    <thead>
            <tr>
                <th>Zona</th>
                <th>Provincia</th>
                <!--<th>Fecha de evaluación</th>-->
                <th>Amie</th>
                <th>Sede</th>
                <th>Comunicado</th>
<!--                <th>Descarga</th>
                <th>Instalación</th>-->
                <th>Estado llamada</th>
                <th>Problema de llamada</th>
            </tr>
        </thead>
 
    <?php $i=1; ?>
@foreach ($laboratorios as $monitor)
    <tr id ="<?php echo $monitor->id; ?>">
    {{ Form::open( array('url' => 'editar_validacion/' . $monitor->id , 'name' => 'editar_monitoreoo' . $monitor->id , 'id' => 'editar_monitoreoo' . $monitor->id))}} 
   
    <td align="right" class="sorting_1">{{$monitor->zona}}</td>
    <td align="right" class="sorting_1">{{$monitor->provincia}}</td>
    <!--<td align="right" class="sorting_1">{{$monitor->fecha_evaluacion}}</td>-->
    <td class="mailbox-messages mailbox-name"><a href="javascript:void(0);" onclick="mostrarvalidacion('{{$monitor->amie}}', {{$monitor->id}}, '{{$monitor->amie}}', '{{$monitor->institucion}}');"  style="display:block"><i class="fa fa-folder"></i>&nbsp;&nbsp;<?= $monitor->amie;  ?></a></td>
    <!--<td align="right" class="sorting_1">{{$monitor->distrito}}</td>-->
    <td align="left" class="sorting_1">{{$monitor->institucion}}</td>
    <td>
    <span class="<?php if($monitor->recibiocomunicado=="SI"){echo "label label-success";} else {echo "label label-danger";}?>">{{$monitor->recibiocomunicado}}</span>
    </td>
<!--    <td>
    <span class="< ?php if($monitor->descarga=="SI"){echo "label label-success";} else {echo "label label-danger";}?>">{{$monitor->descarga}}</span>
    </td>
    <td>
    <span class="< ?php if($monitor->instalacion=="SI"){echo "label label-success";} else {echo "label label-danger";}?>">{{$monitor->instalacion}}</span>
    </td>-->
    <td>
    <span class="<?php if($monitor->estadollamada=="CONTACTADO"){echo "label label-success";} else {echo "label label-danger";}?>">{{$monitor->estadollamada}}</span>
    </td>
    <td>
    <span class="<?php if($monitor->novedadllamada==""){echo "label label-success";} else {echo "label label-danger";}?>">{{$monitor->novedadllamada}}</span>
    </td>
    {{ Form::close() }}
    </tr>
@endforeach
</table>


    <?php


echo str_replace('/?', '?', $laboratorios->render() )  ;

}
else
{

?>


<br/><div class='rechazado'><label style='color:#FA206A'>...No se ha encontrado ningun registro...</label>  </div> 

<?php
}

?>
</div>
<script>
jQuery( document ).ready( function( $ ) {   
    
    $(function() {
        $('.toggleden').bootstrapToggle({
            on: 'SI',
            off: 'NO'
            
        });
    });

        $('.toggleden').on('change', function(){
            
            $( this ).each(function(){
                
                if($(this).val() === "0" || $(this).val() === "1" || $(this).val() ===""){
                
                    var valor = parseInt($(this).val());
                    ///alert(valor);
                    var inputID = $(this).attr('href');
                        console.log( "'" + inputID + "'  |  " + $(this).val());
                    
                    switch(valor) {
                    case 1:
                        $(this).attr('value', parseInt(0));
                        $(this).attr('checked', false);
                        $( 'input[id=' + inputID + ']' ).val(0);
                        break;
                    case 0:
                        $(this).attr('value', parseInt(1));
                        $(this).attr('checked', true);
                        $( 'input[id=' + inputID + ']' ).val(1);
                        break;
                    case "":
                        $(this).attr('value', parseInt(1));
                        $(this).attr('checked', true);
                        $( 'input[id=' + inputID + ']' ).val(1);
                        break;
                    default:
                        $(this).attr('value', null);
                    }
                }
                
                
               
            });
      });   
  });  
</script>


<script>
  jQuery( document ).ready( function( $ ) {     


        $('.submitUpdate2').on('click', function(e){
            e.preventDefault();
            var monId = $(this).attr('href');
            ///sereal = $('#editar_monitoreoo' + monId).serialize();
            sereal =  $('#'+monId+' :input').serialize()+'&id='+monId;

             var form_data = $(this).serialize();
            var myUrl = 'editar_validacion/' + monId;;
            var sesion = $("#myoption").val();
            
                $.ajax({
                    url: myUrl,
                    type: 'POST',
                    ///cache: false,
                    data: sereal,
                    success: function(data){
                        notif({
                        msg: data.message,
                        type: data.status,
                        opacity: 1,

                        });
                        // $("#SaveAlert").alert("Registro Actualizado");
                        // $('#search_results_div').html(data); 
                    },
                    error: function(xhr, textStatus, thrownError){
                        notif({
                        msg: "<b>¡Registro no guardado!</b>",
                        type: "error"

                        });                 // $('#SaveAlert').alert('Se produjo un Error.');
                    }
                });
            ///alert(sereal);
        });
    });
</script>


<!-- Carga de checkButtons -->

