<?php 
if( count($monitoreo) >0){
?>
<div class="box box-primary">
 <div class="col-md-4">
              <div class="box box-solid">
                <div class="box-header">
                  <h3 class="box-title text-danger">Estado de avance</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-default btn-sm"><i class="fa fa-refresh2"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body text-center">
                  <div class="sparkline" data-type="pie" data-offset="90" data-width="100px" data-height="100px">
                    <div class="progress-group">
                        <span class="progress-text">Avance de asistencia</span>
                        <span class="progress-number">{{$avanceasistencia->asistencia}}/<b>{{$avanceasistencia->programadosa}}</b></span>
                        <div class="progress lg"> {{$avanceasistencia->porcentajeasistencia}} %
                          <div class="progress-bar progress-bar-yellow" style="width: {{$avanceasistencia->porcentajeasistencia}}"></div>
                        </div>
                      </div><!-- /.progress-group -->
                      <div class="progress-group">
                        <span class="progress-text">Avance de carga de archivos</span>
                        <span class="progress-number">{{$avanceasistencia->cargas}}/<b>{{$avanceasistencia->programadosa}}</b></span>
                        <div class="progress lg"> {{$avanceasistencia->porcentajecargas}} %
                    <div class="progress-bar progress-bar-green" style="width: {{$avanceasistencia->porcentajecargas}}"></div>
                        </div>
                      </div><!-- /.progress-group --></div>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
            <!-- /.seleccion de fecha -->
            <div class="col-md-3" hidden>
              <div class="box box-solid">
                <div class="box-header">
                  <h3 class="box-title text-danger">Fecha y sesión seleccionada</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-default btn-sm"><i class="fa fa-refresh2"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body text-center">
                  <div class="sparkline" data-type="pie" data-offset="90" data-width="100px" data-height="100px"> <p class="text-center">
                      </p>
                      <center ><h4>{{$name_periodo->name_periodo}}</h4></center>
                      <center ><h1><?php echo $fecha; ?></h1></center></div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
            <?php if (count($avancesesion) >0) { ?>
              @foreach( $avancesesion as $sesion)
              <div class="col-lg-2 col-xs-6">
              <!-- small box -->
              <?php if($sesion->porcentaje==100){ ?>
                  <div class="small-box bg-green">
              <?php } else { ?>
              <div class="small-box bg-yellow">
                 <?php } ?>
                <div class="inner">
                  <h3><?php echo $sesion->porcentaje; ?>%</h3>
                  <p><strong><?php echo $sesion->cantidad; ?></strong> Sesiones iniciadas</p>
                  <p><strong><?php echo $sesion->sesion - $sesion->cantidad; ?></strong> Sesiones NO iniciadas</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">Más información <i class="fa fa-arrow-circle-right"></i></a>
                
              </div>
            </div>
            @endforeach
            <?php } else {?>
             @foreach( $avanceiniciar as $iniciar)
              <div class="col-lg-2 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3>0.00%</h3>
                  <p><strong>0</strong> Sesiones iniciadas</p>
                  <p><strong><?php echo $iniciar->cantidad; ?></strong> Sesiones NO iniciadas</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">Más información <i class="fa fa-arrow-circle-right"></i></a>
                
              </div>
            </div>
             @endforeach
            <?php }?>
            
              </div>
</div>
            
<div class="box box-primary table-responsive">
<div class="box-header">
        <div class="input-group input-group-lg">
                            <input type="text" class="form-control" id="dato_buscado">
                            <span class="input-group-btn">
                              <button class="btn btn-warning dropdown-toggle" type="button" onclick="buscarmonitoreo();" >Buscar!</button>
                            </span>
        </div>            
</div>

<div class="box-body">              
<table id="tabla_pacientes" Style="width:auto" class="table table-bordered table-striped dataTable" cellspacing="0">
    <thead>
            <tr>
             <th>Zona</th>
             <th>Distrito</th>
              <th>Sede</th>
                <th>Amie</th>
                <th>Estado llamada</th>
                <th>Problema llamada</th>
                <th>Descarga</th>
                <th>Instalación</th>
                <th>Listado</th>
                <th>Población</th>
               <th>Fecha-sesión:</br><select  id="select_filtro_fecha"  onchange="buscarmonitoreo();" >
        <?php  if($fecha!=0){ 
             $listadopais=$fecha; 
        }else{  
            $listadopais="General";
         } ?>
        <option value="0">General </option>
        <?php foreach($fechaProramadaInicio as $fechaevaluacion){   ?>
        <option value="<?= $fechaevaluacion->sesion; ?>" <?php if($fechaevaluacion->sesion==$fecha){echo "selected";}?> > <?= $fechaevaluacion->sesion; ?></option>
        <?php }  ?>
        </select> </th>
                <!--<th>Aplicador</th>-->
                <th>Inicio Sesión</th>
                <th hidden>Inicio Sesión Contexto</th>
                <th>Problema Sesión</th>
<!--                <th>Presentes</th>-->
                <th>Convocados</th>
                <th>Asistencias</th>
                <th>Ausentes</th>
                <th>Reprogramados</th>
                <th>Cargas</th>
                <th>Opciones</th>
            </tr>
        </thead>
 
    <?php $i=1; ?>
@foreach ($monitoreo as $monitor)
    <tr id ="<?php echo $monitor->id2; ?>">
    {{ Form::open( array('url' => 'editar_monitoreo/' . $monitor->id2 , 'name' => 'editar_monitoreoo' . $monitor->id2, 'id' => 'editar_monitoreoo' . $monitor->id2))}}
   
    <td align="right" class="sorting_1">{{$monitor->zona}}</td>
    <td align="right" class="sorting_1">{{$monitor->distrito_id}}</td>
    <td>{{$monitor->institucion}}</td> 
     <?php if(1==1) { ?>   
    <td class="mailbox-messages mailbox-name" ><a href="javascript:void(0);" onclick="mostrarmonitoreo('{{$monitor->id2}}','{{$monitor->cgi_laboratorio_id}}', '{{$monitor->id_sede}}', '{{$monitor->codigoamei}}', '{{$monitor->institucion}}', '{{$monitor->fecha_programada_inicio}}', '{{$monitor->sesion}}');"  style="display:block"><i class="fa fa-folder"></i>&nbsp;&nbsp;<?= $monitor->id_sede;  ?></a></td>
     <?php } else {?>
      <td class="mailbox-messages mailbox-name" ><a href="javascript:void(0);" style="display:block"><i class="fa fa-ban"></i>&nbsp;&nbsp;<?= $monitor->id_sede;  ?></a></td>
      <?php } ?>
     <td>
    <span class="<?php if($monitor->estadollamada_apli=="CONTACTADO"){echo "label label-success";} else {echo "label label-danger";}?>">{{$monitor->estadollamada_apli}}</span>
    </td>
	<td>
    <span class="<?php if($monitor->novedadllamada_apli=="VOLVER A LLAMAR"){echo "label label-warning";} else {echo "label label-danger";}?>">{{$monitor->novedadllamada_apli}}</span>
    </td>
   <td >
    <span class="<?php if($monitor->descarga_aplicativo=="SI"){echo "label label-success";} else {echo "label label-danger";}?>">{{$monitor->descarga_aplicativo}}</span>
    </td>
    <td>
    <span class="<?php if($monitor->instalo_aplicativo=="SI"){echo "label label-success";} else {echo "label label-danger";}?>">{{$monitor->instalo_aplicativo}}</span>
    </td>
    <td>
        <span class="<?php if(\App\Http\Controllers\InformeController::tiene_listado_cargado($monitor->id2)=="SI"){echo "label label-success";} else {echo "label label-danger";}?>">{{ \App\Http\Controllers\InformeController::tiene_listado_cargado($monitor->id2) }}</span>
    </td>
        <td>{{$monitor->poblacion}}</td>
    <td>{{$monitor->sesion}}</td>
    <!--<td>
        <div class="btn-group" data-toggle="buttons">
               <label class="btn btn-primary < ?php if($monitor->sesion_aplicador=="SI"){echo "active";}?>">
                  <input type="radio" name="aplicador" id="aplicador" value="SI" < ?php if($monitor->sesion_aplicador=="SI"){echo "checked";}?>>SI
               </label>
               <label class="btn btn-primary < ?php if($monitor->sesion_aplicador=="NO"){echo "active";}?>">
                     <input type="radio" name="aplicador" id="aplicador" value="NO" < ?php if($monitor->sesion_aplicador=="NO"){echo "checked";}?>>NO
               </label>
          </div>
    </td>-->
    <td>
           <div class="btn-group" data-toggle="buttons">
               <label class="btn btn-primary <?php if($monitor->estado_sesion=="SI"){echo "active";}?>">
                  <input type="radio" name="sesion" id="sesion" value="SI" <?php if($monitor->estado_sesion=="SI"){echo "checked";}?>>Sí
               </label>
               <label class="btn btn-primary <?php if($monitor->estado_sesion=="NO"){echo "active";}?>">
                     <input type="radio" name="sesion" id="sesion" value="NO" <?php if($monitor->estado_sesion=="NO"){echo "checked";}?>>NO
               </label>
               <!--<label class="btn btn-primary <?php if($monitor->estado_sesion=="REP"){echo "active";}?>">
                     <input type="radio" name="sesion" id="sesion" value="REP" <?php if($monitor->estado_sesion=="REP"){echo "checked";}?>>REP
               </label>-->
          </div>
    </td>
    <td hidden>
           <div class="btn-group" data-toggle="buttons">
               <label class="btn btn-primary <?php if($monitor->estado_sesion2=="SI"){echo "active";}?>">
                  <input type="radio" name="sesion2" id="sesion2" value="SI" <?php if($monitor->estado_sesion2=="SI"){echo "checked";}?>>Sí
               </label>
               <label class="btn btn-primary <?php if($monitor->estado_sesion2=="NO"){echo "active";}?>">
                     <input type="radio" name="sesion2" id="sesion2" value="NO" <?php if($monitor->estado_sesion2=="NO"){echo "checked";}?>>NO
               </label>
               <!--<label class="btn btn-primary <?php if($monitor->estado_sesion2=="REP"){echo "active";}?>">
                     <input type="radio" name="sesion2" id="sesion2" value="REP" <?php if($monitor->estado_sesion2=="REP"){echo "checked";}?>>REP
               </label>-->
          </div>
    </td>
    <td>
    <select id="id_subcategoria_etiqueta" value = "<?= $monitor->sustentante_observacion; ?>" name="id_subcategoria_etiqueta" class="form-control">
                               <option value="SIN PROBLEMA" > Seleccione </option>
                                <?php foreach($subCategoriaetiqueta as $tipo){  ?>
                                   
                                   <option value="<?= $tipo->descripcion_cat; ?>" <?php if($monitor->observacion_sesion==$tipo->descripcion_cat){echo "selected";}?>> <?= $tipo->descripcion_cat; ?> </option>
                                
                                <?php } ?>
                                
                               </select>
    </td>
<!--    <td align="right"><input align="right" style="width:40px;" name="presentes" id="presentes" type="text" value="{{$monitor->presentes}}"></td>-->
    <td align="right">{{$monitor->programados}}</td>
    <td align="right">{{$monitor->asistencia}}</td>
    <td align="right">{{$monitor->programados - $monitor->asistencia}}</td>
        <td align="right">
            <input align="right" style="<?php if(isset($monitor->reprogramacion) and $monitor->reprogramacion > 0)
            {echo "color:White;background:#efe004;width:40px;";}
            else {echo "color:White;background:#5FB404;width:40px;";}?>"
                   disabled="true" name="cargas" type="text" value="{{isset($monitor->reprogramacion) ? $monitor->reprogramacion : 0}}">
        </td>
    <td align="right">
    <input align="right" style="<?php if($monitor->asistencia==$monitor->cargas){echo "color:White;background:#5FB404;width:40px;";} else {echo "color:White;background:#FA5858;width:40px;";}?>" disabled="true" name="cargas" type="text" value="{{$monitor->cargas}}">
    </td>
    <td>

     <?php if(1==1) { ?>

    {{ Form::submit('Actualizar', array('id' => 'submitUpdate' . $monitor->id2, 'href' => $monitor->id2, 'class' => 'btn btn-block btn-primary submitUpdate'))}}
     <?php } else {} ?>
     
    </td>
    
    
    {{ Form::close() }}
    </tr>
@endforeach
</table>
<?php
echo str_replace('/?', '?', $monitoreo->render() )  ;
} else { 
?>
<br/><div class='rechazado'><label style='color:#FA206A'>...No se ha encontrado ningun registro...</label>  </div> 
<?php } ?>
</div>

<script>
  jQuery( document ).ready( function( $ ) {     


        $('.submitUpdate').on('click', function(e){
            //console.log(this.closest('tr').cells[10].children[0].children[1].children[0].attributes['checked']);
            e.preventDefault();
            let isesion, mproblema;
            var monId = $(this).attr('href');
            ///sereal = $('#editar_monitoreoo' + monId).serialize();
            sereal =  $('#'+monId+' :input').serialize()+'&id='+monId;
            serealArray = $('#'+monId+' :input').serializeArray();
            $(serealArray).each(function(i, field){
                if (field.name === 'sesion')
                    isesion = field.value;
                if (field.name === 'id_subcategoria_etiqueta')
                    mproblema = field.value;
            });
            if (!isesion)
                if (this.closest('tr').cells[10].children[0].children[0].children[0].attributes['checked'])
                    isesion = 'SI';
                else
                    isesion = 'NO';

            if (isesion === 'NO' && mproblema === 'SIN PROBLEMA') {
                alert('Si no inició sesión se debe especificar el problema.')
                return false;
            }
            if (isesion === 'SI' && mproblema !== 'SIN PROBLEMA') {
                alert('Si la sesión se inició, por favor no seleccione un problema.')
                return false;
            }

            var form_data = $(this).serialize();
            var myUrl = 'editar_monitoreo/' + monId;;
            var sesion = $("#myoption").val();

                $.ajax({
                    url: myUrl,
                    type: 'POST',
                    ///cache: false,
                    data: sereal,
                    success: function(data){
                        notif({
                        msg: data.message,
                        type: "success",
                        opacity: 1,

                        });
                        // $("#SaveAlert").alert("Registro Actualizado");
                        // $('#search_results_div').html(data); 
                    },
                    error: function(xhr, textStatus, thrownError){
                        notif({
                        msg: "<b>¡Registro no guardado!</b>",
                        type: "error"

                        });                 // $('#SaveAlert').alert('Se produjo un Error.');
                    }
                });
            ///alert(sereal);
        });
    });
</script>


<!-- Carga de checkButtons -->

