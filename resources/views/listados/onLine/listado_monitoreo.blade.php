<?php 
if( count($monitoreo) >0){
?>
<div class="box box-primary">
 <div class="col-md-4">
              <div class="box box-solid">
                <div class="box-header">
                  <h3 class="box-title text-danger">Estado de avance</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-default btn-sm"><i class="fa fa-refresh2"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body text-center">
                  <div class="sparkline" data-type="pie" data-offset="90" data-width="100px" data-height="100px">
                    <div class="progress-group">
                        <span class="progress-text">Avance de asistencia</span>
                        <span class="progress-number">{{$avanceasistencia->asistencia}}/<b>{{$avanceasistencia->programadosa}}</b></span>
                        <div class="progress lg"> {{$avanceasistencia->porcentajeasistencia}} %
                          <div class="progress-bar progress-bar-yellow" style="width: {{$avanceasistencia->porcentajeasistencia}}"></div>
                        </div>
                      </div><!-- /.progress-group -->
                      <div class="progress-group">
                        <span class="progress-text">Avance de carga de archivos</span>
                        <span class="progress-number">{{$avanceasistencia->cargas}}/<b>{{$avanceasistencia->programadosa}}</b></span>
                        <div class="progress lg"> {{$avanceasistencia->porcentajecargas}} %
                    <div class="progress-bar progress-bar-green" style="width: {{$avanceasistencia->porcentajecargas}}"></div>
                        </div>
                      </div><!-- /.progress-group --></div>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
            <!-- /.seleccion de fecha -->
            <div class="col-md-3" hidden>
              <div class="box box-solid">
                <div class="box-header">
                  <h3 class="box-title text-danger">Fecha y sesión seleccionada</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-default btn-sm"><i class="fa fa-refresh2"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body text-center">
                  <div class="sparkline" data-type="pie" data-offset="90" data-width="100px" data-height="100px"> <p class="text-center">
                      </p>
                      <center ><h4>{{$name_periodo->name_periodo}}</h4></center>
                      <center ><h1><?php echo $fecha; ?></h1></center></div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
            <?php if (count($avancesesion) >0) { ?>
              @foreach( $avancesesion as $sesion)
              <div class="col-lg-2 col-xs-6">
              <!-- small box -->
              <?php if($sesion->porcentaje==100){ ?>
                  <div class="small-box bg-green">
              <?php } else { ?>
              <div class="small-box bg-yellow">
                 <?php } ?>
                <div class="inner">
                  <h3><?php echo $sesion->porcentaje; ?>%</h3>
                  <p><strong><?php echo $sesion->cantidad; ?></strong> Sesiones iniciadas</p>
                  <p><strong><?php echo $sesion->sesion - $sesion->cantidad; ?></strong> Sesiones NO iniciadas</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">Más información <i class="fa fa-arrow-circle-right"></i></a>
                
              </div>
            </div>
            @endforeach
            <?php } else {?>
             @foreach( $avanceiniciar as $iniciar)
              <div class="col-lg-2 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3>0.00%</h3>
                  <p><strong>0</strong> Sesiones iniciadas</p>
                  <p><strong><?php echo $iniciar->cantidad; ?></strong> Sesiones NO iniciadas</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">Más información <i class="fa fa-arrow-circle-right"></i></a>
                
              </div>
            </div>
             @endforeach
            <?php }?>
            
              </div>
</div>
            
<div class="box box-primary table-responsive">
<div class="box-header">
        <div class="input-group input-group-lg">
                            <input type="text" class="form-control" id="dato_buscado">
                            <span class="input-group-btn">
                              <button class="btn btn-warning dropdown-toggle" type="button" onclick="buscarmonitoreo_online();" >Buscar!</button>
                            </span>
        </div>            
</div>

<div class="box-body">              
<table id="tabla_pacientes" Style="width:auto" class="table table-bordered table-striped dataTable" cellspacing="0">
    <thead>
            <tr>
             <th>Zona</th>
              <th>Sede</th>
                <th>Amie</th>
                <th>Estado Reunión</th>
                <th>Novedad Reunión</th>
               <th>Fecha-sesión:</br><select  id="select_filtro_fecha"  onchange="buscarmonitoreo_online();" >
        <?php  if($fecha!=0){ 
             $listadopais=$fecha; 
        }else{  
            $listadopais="General";
         } ?>
        <option value="0">General </option>
        <?php foreach($fechaProramadaInicio as $fechaevaluacion){   ?>
        <option value="<?= $fechaevaluacion->sesion; ?>" <?php if($fechaevaluacion->sesion==$fecha){echo "selected";}?> > <?= $fechaevaluacion->sesion; ?></option>
        <?php }  ?>
        </select> </th>
                <!--<th>Aplicador</th>-->
                <th>Inicio Sesión</th>
                <th hidden>Inicio Sesión Contexto</th>
                <th>Problema Sesión</th>
<!--                <th>Presentes</th>-->
                <th>Convocados</th>
                <th>Asistencias</th>
                <th>Ausentes</th>
                <th>Reprogramados</th>
                <th>Cargas</th>
                <th>Opciones</th>
            </tr>
        </thead>
 
    <?php $i=1; ?>
@foreach ($monitoreo as $monitor)
    <tr id ="<?php echo $monitor->id2; ?>">
    {{ Form::open( array('url' => 'editar_monitoreo_online/' . $monitor->id2 , 'name' => 'editar_monitoreoo_online' . $monitor->id2, 'id' => 'editar_monitoreoo_online' . $monitor->id2))}}
   
    <td align="right" class="sorting_1">{{$monitor->zona}}</td>
    <td>{{$monitor->institucion}}</td> 
     <?php if(1==1) { ?>   
    <td class="mailbox-messages mailbox-name" ><a href="javascript:void(0);" onclick="mostrarmonitoreo_online('{{$monitor->id2}}','{{$monitor->cgi_laboratorio_id}}', '{{$monitor->id_sede}}', '{{$monitor->codigoamei}}', '{{$monitor->institucion}}', '{{$monitor->fecha_programada_inicio}}', '{{$monitor->sesion}}', '_{{$monitor->estado_sesion2}}');"  style="display:block"><i class="fa fa-folder"></i>&nbsp;&nbsp;<?= $monitor->id_sede;  ?></a></td>
     <?php } else {?>
      <td class="mailbox-messages mailbox-name" ><a href="javascript:void(0);" style="display:block"><i class="fa fa-ban"></i>&nbsp;&nbsp;<?= $monitor->id_sede;  ?></a></td>
      <?php } ?>
     <td>
    <span class="<?php if($monitor->estadollamada_apli=="INICIADA"){echo "label label-success";} else {echo "label label-danger";}?>">{{$monitor->estadollamada_apli}}</span>
    </td>
	<td>
    <span class="<?php {echo "label label-warning";}?>">{{$monitor->novedadllamada_apli}}</span>
    </td>   
    <td>{{$monitor->sesion}}</td>
    <td>
           <div class="btn-group" data-toggle="buttons">
               <label class="btn {{$monitor->estado_sesion=='SI' ? 'btn-success': 'btn-warning' }}"> 
                {{$monitor->estado_sesion=='SI' ? 'Sí': 'No' }} </label>
          </div>
    </td>
    <td>
    <select id="id_subcategoria_etiqueta" value = "<?= $monitor->sustentante_observacion; ?>" name="id_subcategoria_etiqueta" class="form-control">
                               <option value="SIN PROBLEMA" > Seleccione </option>
                                <?php foreach($subCategoriaetiqueta as $tipo){  ?>
                                   
                                   <option value="<?= $tipo->descripcion_cat; ?>" <?php if($monitor->observacion_sesion==$tipo->descripcion_cat){echo "selected";}?>> <?= $tipo->descripcion_cat; ?> </option>
                                
                                <?php } ?>
                                
                               </select>
    </td>
    <td align="right">{{$monitor->programados}}</td>
    <td align="right">{{$monitor->asistencia}}</td>
    <td align="right">{{$monitor->programados - $monitor->asistencia}}</td>
        <td align="right">
            <input align="right" style="<?php if(isset($monitor->reprogramacion) and $monitor->reprogramacion > 0)
            {echo "color:White;background:#efe004;width:40px;";}
            else {echo "color:White;background:#5FB404;width:40px;";}?>"
                   disabled="true" name="cargas" type="text" value="{{isset($monitor->reprogramacion) ? $monitor->reprogramacion : 0}}">
        </td>
    <td align="right">
    <input align="right" style="<?php if($monitor->asistencia==$monitor->cargas){echo "color:White;background:#5FB404;width:40px;";} else {echo "color:White;background:#FA5858;width:40px;";}?>" disabled="true" name="cargas" type="text" value="{{$monitor->cargas}}">
    </td>
    <td>

     <?php if(1==1) { ?>

    {{ Form::submit('Actualizar', array('id' => 'submitUpdateOnLine' . $monitor->id2, 'href' => $monitor->id2, 'class' => 'btn btn-block btn-primary submitUpdateOnLine'))}}
     <?php } else {} ?>
     
    </td>
    
    
    {{ Form::close() }}
    </tr>
@endforeach
</table>
<?php
echo str_replace('/?', '?', $monitoreo->render() )  ;
} else { 
?>
<br/><div class='rechazado'><label style='color:#FA206A'>...No se ha encontrado ningun registro...</label>  </div> 
<?php } ?>
</div>

<script>
  jQuery( document ).ready( function( $ ) {     


        $('.submitUpdateOnLine').on('click', function(e){
            //console.log(this.closest('tr').cells[10].children[0].children[1].children[0].attributes['checked']);
            e.preventDefault();
            let isesion, mproblema;
            var monId = $(this).attr('href');
            ///sereal = $('#editar_monitoreoo' + monId).serialize();
            sereal =  $('#'+monId+' :input').serialize()+'&id='+monId;
            serealArray = $('#'+monId+' :input').serializeArray();
            $(serealArray).each(function(i, field){
                if (field.name === 'id_subcategoria_etiqueta')
                    mproblema = field.value;
            });

            var form_data = $(this).serialize();
            var myUrl = 'editar_monitoreo_online/' + monId;
            var sesion = $("#myoption").val();

                $.ajax({
                    url: myUrl,
                    type: 'POST',
                    ///cache: false,
                    data: sereal,
                    success: function(data){
                        notif({
                        msg: data.message,
                        type: "success",
                        opacity: 1,

                        }); 
                    },
                    error: function(xhr, textStatus, thrownError){
                        notif({
                        msg: "<b>¡Registro no guardado!</b>",
                        type: "error"

                        });
                    }
                });
        });
    });
</script>


<!-- Carga de checkButtons -->

