<script>
$("#checkProvincias").change(function(){
    var i =0;
    @foreach( $reporteprovincia as $prov )
    i = i + 1;
   $("#provincias"+i).prop('checked', $(this).prop("checked"));
   $("#coordinador"+i).prop('checked', $(this).prop("checked"));
   $("#zonas"+i).prop('checked', $(this).prop("checked")); 
   
   if($("#provincias"+i).prop('checked')=== true ){
       $("#enviar_provincia").css("display","block");
   }else{
       $("#enviar_provincia").css("display","none");
   }
    @endforeach;
}); 

function select_provincia(){
    var p = 0;
    var i = 0;
    @foreach( $reporteprovincia as $selectp )
    p = p + 1;
    var selectProvincia = $("#provincias"+p).prop('checked');     
    if (selectProvincia){
         i = i + 1;
       $('#enviar_provincia').css("display", "block");
    }else if (i == 0 ){
       $('#enviar_provincia').css("display", "none"); 
    }  
    @endforeach;
};
function selectCoordinador(i){
    
    $("#provincias"+i).change(function(){
    $("#coordinador"+i).prop('checked', $(this).prop("checked"));
    $("#zonas"+i).prop('checked', $(this).prop("checked"));
    });
};

$(function(){
$("#enviar_provincia").click(function(){        
    var id_periodo = $("#periodo").val();
    var fecha = $("#fecha_programada").val();
    var sesion = $("#sesion").val();
    var tp_reporte = $("#tp_reporte").val();
    
    if (id_periodo === "" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "0") {
                //alert("Debe seleccionar un periodo");
                            notif({
                            msg: 'Debe Seleccionar un Periodo !',
                            type: 'warning',
                            opacity: 1,
                            });
            } else {
                $(".provincia").css("display", "none");
                    $(".distritod").css("display", "block");
                    document.getElementById("loading").style.display = "block";
        if (id_periodo !== "" && fecha === "" && sesion === "") {
                    var ur = "distrito_aplicacion/" + tp_reporte + "/" + id_periodo + "";
                } else {
                    if (id_periodo !== "" && fecha !== "" && sesion === "") {
                        var ur = "distrito_aplicacion/" + tp_reporte + "/" + id_periodo + "/" + fecha + "";
                    } else {
                        var ur = "distrito_aplicacion/" + tp_reporte + "/" + id_periodo + "/" + fecha + "/" + sesion + "";
                    }
                }
                 var url = ur;
                $.ajax({
                type: "POST",
                url: url,
                data: $("#formulariop").serialize(),
                success: function(data)
                {
                $("#distrito").html(data);
                document.getElementById("loading").style.display="none";
                }
                }); 
         }
   return false;
   //alert(data);
});

});
    
$(function(){
   $(".azona").click(function(){
      $(".zona").css("display","block");
      $(".provincia").css("display", "none");
      $(".distritod").css("display", "none");
      $(".monitord").css("display", "none");          
      $(".labmonitores").css("display","none");          
   }); 
});

</script>

<ol class="breadcrumb">
    <li><a href="exportar_info_aplicacion/{{ $idperiodo }}/excel">
            <img src="images/excel.png" title="DESCARGAR EXCEL" style=" height: 23px; margin-right: -13px;">
        </a> &nbsp; &nbsp; &nbsp;
    </li>
    <li><a href="#" class="azona">Zonas</a></li>
        <li class="active">Provincias</li>
</ol>

<div class="box table-responsive no-padding " style="width: auto;">
                                <div class="box-body">
                                    {!! Form::open(['method' => 'POST', 'id' => 'formulariop' ]) !!}
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 90%;">
                                        <tr style="background-color: #0489B1; color: white;">  
                                        <h4> <th style="text-align: center; vertical-align: middle;" colspan="13"><b>REPORTE DE ZONA POR PROVINCIA </b></th></h4>
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;">                                              
                                            <th style="text-align: left;  vertical-align: middle; padding: 3px;">
                                                &nbsp;&nbsp; <input type="checkbox" id="checkProvincias"> PROVINCIA
                                            </th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">COORDINADOR</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">ZONA</th>
                                            <th style="text-align:center;  vertical-align: middle; padding: 3px;">N° SEDES</th>
                                            <th style="text-align:center;  vertical-align: middle; padding: 3px;" colspan="2">DESCARGÓ</th>
                                            <th style="text-align:center;  vertical-align: middle; padding: 3px" colspan="2">INSTALÓ</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;" colspan="2">INICIÓ SESIÓN</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;" colspan="3">ESTADO DE LLAMADAS</th>
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;">
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">  </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> SÍ </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> NO </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> SÍ </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> NO </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> SÍ </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> NO </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> CONTACTADO </td>                                            
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> POR CONTACTAR </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> NO CONTACTADO </td>
                                            <!-- <td style="text-align: center; vertical-align: middle; padding: 3px;"> PROBLEMA DE LLAMADA </td> -->
                                            
                                        </tr>                                        
                                        <?php $i = 0;?>
                                            @foreach( $reporteprovincia as $provincia )
                                            
                                            <?php 
                                            $i++;
                                            
                                            if( $provincia->laboratorio == 0){
                                            $porcedesc = number_format((($provincia->descargasi / 1) * 100), '2',',',''); 
                                            $porceinstalo = number_format((($provincia->instalosi / 1) * 100), '2',',',''); 
                                            $porcesesion = number_format((($provincia->sesionsi / 1) * 100), '2',',',''); 
                                            $porcecontactado = number_format((($provincia->contactado / 1) * 100), '2',',',''); 
                                            $porcenocontactado = number_format((($provincia->nocontactado / 1) * 100), '2',',',''); 
                                            $porceenproceso = number_format((($provincia->enproceso / 1) * 100), '2',',',''); 
                                            $porcenovedad = number_format((($provincia->novedadllamada / 1) * 100), '2',',',''); 
                                            }else{
                                            $porcedesc = number_format((($provincia->descargasi / $provincia->laboratorio) * 100), '2',',',''); 
                                            $porceinstalo = number_format((($provincia->instalosi / $provincia->laboratorio) * 100), '2',',',''); 
                                            $porcesesion = number_format((($provincia->sesionsi / $provincia->laboratorio) * 100), '2',',',''); 
                                            $porcecontactado = number_format((($provincia->contactado / $provincia->laboratorio) * 100), '2',',','');
                                            $porcenocontactado = number_format((($provincia->nocontactado / $provincia->laboratorio) * 100), '2',',','');
                                            $porceenproceso = number_format((($provincia->enproceso / $provincia->laboratorio) * 100), '2',',','');
                                            $porcenovedad = number_format((($provincia->novedadllamada / $provincia->laboratorio) * 100), '2',',','');
                                            
                                            }
                                            ?>
                                        <tr> 
                                            <td style="text-align: left; vertical-align: middle; padding: 3px;">
                                                    <label>
                                                        <input type="checkbox" id="provincias{{ $i }}" value="{{ $provincia->provincia }}" name="provincia[]" onclick="selectCoordinador({{ $i }}); select_provincia();">
                                                        {{ $provincia->provincia }}
                                                    </label>
                                            </td> 
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">
                                                    <label>    
                                                        <input type="checkbox" id="coordinador{{ $i }}" value="{{ $provincia->coordinador }}" name="coordinador_p[]" hidden>
                                                    {{ $provincia->coordinador }}
                                                    </label>
                                            </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">
                                                    <label>    
                                                    <input type="checkbox" id="zonas{{ $i }}" value="{{ $provincia->zona }}" name="zona[]" hidden>
                                                    {{ $provincia->zona }}
                                                    </label>
                                            </td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">{{ $provincia->laboratorio }}</td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                                <div class="progress">
                                                    <?php if($porcedesc <= 44){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">     
                                                    <?php }elseif($porcedesc >=45 && $porcedesc <=99) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($porcedesc >= 100) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>( {{ $provincia->descargasi }} ) {{ $porcedesc.'%' }}</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">{{ $provincia->descargano }}</td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php if( $porceinstalo <= 44 ){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">     
                                                    <?php }elseif($porceinstalo >= 45 && $porceinstalo <= 99) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($porceinstalo >= 100) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">
                                                    <?php } ?>        
                                                      <span class="skill"><i class="val"><b>( {{ $provincia->instalosi }} ) {{ $porceinstalo.'%' }}</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">{{ $provincia->instalono }}</td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php if($porcesesion <= 44){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">     
                                                    <?php }elseif($porcesesion >= 45 && $porcesesion <= 99) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($porcesesion >=100) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>( {{ $provincia->sesionsi }} ) {{ $porcesesion.'%' }}</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">{{ $provincia->sesionno }}</td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php if($porcecontactado <= 44){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">     
                                                    <?php }elseif($porcecontactado >= 45 && $porcecontactado <= 99) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($porcecontactado >= 100) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>( {{ $provincia->contactado }} ) {{ $porcecontactado }}%</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>    
                                            <td style="text-align: right; vertical-align: middle; padding-right: 5px;"><b>{{ $provincia->enproceso }}</b></td>
                                           <!-- <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php /* if($provincia->enproceso ==0){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                    <?php }elseif($provincia->enproceso != $provincia->laboratorio) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($provincia->enproceso == $provincia->laboratorio) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">
                                                    <?php } */ ?>        
                                                        <span class="skill"><i class="val"><b>( {{ $provincia->enproceso }} ) {{ $porceenproceso }}%</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td> -->
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php if($porcenocontactado <= '0,00'){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                    <?php }elseif($porcenocontactado >= 0 && $porcenocontactado <= 19) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($porcenocontactado >= 20) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>( {{ $provincia->nocontactado }} ) {{ $porcenocontactado }}%</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>
                                            <!--
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php /* if($provincia->novedadllamada == $provincia->nocontactado){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                    <?php }elseif($provincia->novedadllamada != $provincia->nocontactado) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">    
                                                    <?php } */?>        
                                                        <span class="skill"><i class="val"><b>( {{ $provincia->novedadllamada }} ) {{ $porcenovedad }}%</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>                                           
                                            -->
                                        </tr>
                                            @endforeach
                                            <tr>
                                             <td style="background-color: #0489B1; color: white;"  colspan="3"><b>TOTALES</b></td>
                                             <?php
                                             if($TotalLab == 0){
                                            $porcedesca = ($TotalDescargosi / 1); 
                                            $porceinstal = ($TotalInstalosi / 1); 
                                            $porcesesio = ($TotalSesionsi / 1) ; 
                                            $porcecontac = ($TotalContactado / 1) ; 
                                            $porcenocontac = ($TotalNoContactado / 1) ; 
                                            $porceenproc = ($TotalEnProceso / 1) ; 
                                            $porcenoved = ($TotalNovedad / 1) ; 
                                            }else{
                                            $porcedesca = number_format((($TotalDescargosi / $TotalLab) * 100), '2',',',''); 
                                            $porceinstal = number_format((($TotalInstalosi / $TotalLab) * 100), '2',',',''); 
                                            $porcesesio = number_format((($TotalSesionsi / $TotalLab) * 100), '2',',',''); 
                                            $porcecontac = number_format((($TotalContactado / $TotalLab) * 100), '2',',',''); 
                                            $porcenocontac = number_format((($TotalNoContactado / $TotalLab) * 100), '2',',',''); 
                                            $porceenproc = number_format((($TotalEnProceso / $TotalLab) * 100), '2',',',''); 
                                            $porcenoved = number_format((($TotalNovedad / $TotalLab) * 100), '2',',',''); 
                                            }
                                             ?>
                                             <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalLab }}</b></td>
                                            <?php if( $TotalDescargosi ==0){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalDescargosi }} &nbsp;&nbsp;
                                            <?php }elseif( $TotalDescargosi != $TotalLab){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalDescargosi }} &nbsp;&nbsp;
                                            <?php }elseif( $TotalDescargosi == $TotalLab){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalDescargosi }} &nbsp;&nbsp;        
                                            <?php } ?>
                                             ({{ $porcedesca }}) %</b></td>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalDescargono }}  </b></td> 
                                             <?php if( $TotalInstalosi ==0){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalInstalosi }} &nbsp;&nbsp;
                                            <?php }elseif( $TotalInstalosi != $TotalLab){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalInstalosi }} &nbsp;&nbsp;
                                            <?php }elseif(  $TotalInstalosi == $TotalLab){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalInstalosi }} &nbsp;&nbsp;        
                                            <?php } ?>        
                                                    ({{ $porceinstal }}) %</b></td>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalInstalono }} </b></td>
                                             <?php if( $TotalSesionsi==0 ){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalSesionsi }} &nbsp;&nbsp;
                                            <?php }elseif( $TotalSesionsi != $TotalLab){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalSesionsi }} &nbsp;&nbsp;
                                            <?php }elseif( $TotalSesionsi == $TotalLab){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalSesionsi }} &nbsp;&nbsp;        
                                            <?php } ?>         
                                            ({{ $porcesesio }}) %</b></td>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalSesionno }} </b></td>
                                            <?php if( $TotalContactado ==0){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ $TotalContactado }} ) {{ $porcecontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalContactado != $TotalLab){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ $TotalContactado }} ) {{ $porcecontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalContactado == $TotalLab){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ $TotalContactado }} ) {{ $porcecontac }}% &nbsp;&nbsp;        
                                            <?php } ?>       
                                            <?php if( $TotalEnProceso ==0){?>
                                            <td align="right" style="background-color: #0489B1; color: white; padding-right: 5px;"><b> {{ $TotalEnProceso }} <!-- {{ $porceenproc }}% &nbsp;&nbsp; -->
                                            <?php }elseif( $TotalEnProceso != $TotalLab){?>
                                            <td align="right" style="background-color: #0489B1; color: white; padding-right: 5px;"><b> {{ $TotalEnProceso }} <!-- {{ $porceenproc }}% &nbsp;&nbsp; --> 
                                            <?php }elseif( $TotalEnProceso == $TotalLab){?>
                                            <td align="right" style="background-color: #0489B1; color: white; padding-right: 5px;"><b> {{ $TotalEnProceso }}  <!-- {{ $porceenproc }}%&nbsp;&nbsp; -->       
                                            <?php } ?> 
                                            <?php if( $TotalNoContactado == 0 && $porcecontac =='100,00'){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ $TotalNoContactado }} ) {{ $porcenocontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalNoContactado > 0 && $porcecontac !='100,00'){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ $TotalNoContactado }} ) {{ $porcenocontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalNoContactado == 0 && $porcecontac !='100,00'){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ $TotalNoContactado }} ) {{ $porcenocontac }}% &nbsp;&nbsp;        
                                            <?php } ?>    
                                                    
                                            <?php /* if( $TotalNovedad == $TotalNoContactado){?>
                                            <td align="right" style="background-color: #5FB404; color: white;"><b>( {{ $TotalNovedad }} ) {{ $porcenoved }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalNovedad != $TotalNoContactado){?>
                                            <td align="right" style="background-color: #D7DF01; color: white;"><b>( {{ $TotalNovedad }} ) {{ $porcenoved }}% &nbsp;&nbsp;      
                                            <?php } */ ?>        
                                            
                                        </tr>
                                        <tr>
                                           <td colspan="13"> 
                                              <!-- {!! Form::submit('Para consulta de Distrito por monitor hacer Click aqui', ['class' => 'btn btn-block btn-primary', 'onclick' => 'alert(hola);']) !!} -->
                                              <button class="btn btn-block btn-info" type="submit" id="enviar_provincia" style="display: none;"><b>Para consulta de Provincia por Distrito hacer Click aqui</b></button>
                                           </td>
                                       </tr>
                                    </table>
                                   {!! Form::close() !!}
                                </div>
                            </div>                          