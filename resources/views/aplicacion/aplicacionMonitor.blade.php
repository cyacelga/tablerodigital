<script>
$("#checkMonitor").change(function(){
    var i =0;
    @foreach( $monitores as $monit )
    i = i + 1;
   $("#monitor"+i).prop('checked', $(this).prop("checked"));
    $("#distritom"+i).prop('checked', $(this).prop("checked"));
    $("#provinciam"+i).prop('checked', $(this).prop("checked"));
    $("#coordinadorm"+i).prop('checked', $(this).prop("checked"));
    $("#zonasm"+i).prop('checked', $(this).prop("checked"));
    
    if($("#monitor"+i).prop('checked')=== true ){
       $("#enviar_labmonitor").css("display","block");
   }else{
       $("#enviar_labmonitor").css("display","none");
   }
    @endforeach;
});

function select_monitor(){
    var p = 0;
    var i = 0;
    @foreach( $monitores as $selectm );
    p = p + 1;
    var selectMonitor = $("#monitor"+p).prop('checked');     
    if (selectMonitor){
         i = i + 1;
       $('#enviar_labmonitor').css("display", "block");
    }else if (i === 0 ){
       $('#enviar_labmonitor').css("display", "none"); 
    }  
    @endforeach;
};

function selectDistrito(i){
    
    $("#monitor"+i).change(function(){
    $("#distritom"+i).prop('checked', $(this).prop("checked"));
    $("#provinciam"+i).prop('checked', $(this).prop("checked"));
    $("#coordinadorm"+i).prop('checked', $(this).prop("checked"));
    $("#zonasm"+i).prop('checked', $(this).prop("checked"));
    });
};

$(function(){
$("#enviar_labmonitor").click(function(){         
    var id_periodo = $("#periodo").val();
    var fecha = $("#fecha_programada").val();
    var sesion = $("#sesion").val();
    var tp_reporte = $("#tp_reporte").val();
    
    if (id_periodo === "" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "0") {
                //alert("Debe seleccionar un periodo");
                            notif({
                            msg: 'Debe Seleccionar un Periodo !',
                            type: 'warning',
                            opacity: 1,
                            });
            } else {
                $(".monitord").css("display", "none");
                    $(".labmonitores").css("display", "block");
                    document.getElementById("loading").style.display = "block";
        if (id_periodo !== "" && fecha === "" && sesion === "") {
                    var ur = "laboratorio_aplicacion/" + tp_reporte + "/" + id_periodo + "";
                } else {
                    if (id_periodo !== "" && fecha !== "" && sesion === "") {
                        var ur = "laboratorio_aplicacion/" + tp_reporte + "/" + id_periodo + "/" + fecha + "";
                    } else {
                        var ur = "laboratorio_aplicacion/" + tp_reporte + "/" + id_periodo + "/" + fecha + "/" + sesion + "";
                    }
                }
                 var url = ur;
                $.ajax({
                type: "POST",
                url: url,
                data: $("#formulariom").serialize(),
                success: function(data)
                {
                $("#laboratorio").html(data);
                document.getElementById("loading").style.display="none";
                }
                }); 
         }
   return false;
   //alert(data);
});

});

$(function(){
    $(".azona").click(function(){
    $(".zona").css("display", "block");
    $(".provincia").css("display", "none");
    $(".distritod").css("display", "none");
    $(".monitord").css("display", "none");          
    $(".labmonitores").css("display","none"); 
    });
    });
    
$(function(){
    $(".aprovincia").click(function(){
    $(".provincia").css("display", "block");
    $(".zona").css("display", "none");
    $(".distritod").css("display", "none");
    $(".monitord").css("display", "none");          
    $(".labmonitores").css("display","none"); 
    });
});

$(function(){
    $(".adistrito").click(function(){
    $(".provincia").css("display", "none");
    $(".zona").css("display", "none");
    $(".distritod").css("display", "block");
    $(".monitord").css("display", "none");          
    $(".labmonitores").css("display","none"); 
    });
});
</script>

    <ol class="breadcrumb">
        <li><a href="exportar_info_aplicacion/{{ $idperiodo }}/excel">
                <img src="images/excel.png" title="DESCARGAR EXCEL" style=" height: 23px; margin-right: -13px;">
            </a> &nbsp; &nbsp; &nbsp;
        </li>
        <li><a href="#" class="azona">Zonas</a></li>
        <li><a href="#" class="aprovincia">Provincias</a></li>
        <li><a href="#" class="adistrito">Distritos</a></li>
        <li class="active">Monitores</li>
    </ol>

<div class="box table-responsive no-padding " style="width: auto;">
                                <div class="box-body">
                                    {!! Form::open(['method' => 'POST', 'id' => 'formulariom' ]) !!}
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 90%;">
                                        <tr style="background-color: #0489B1; color: white;">  
                                        <h4> <th style="text-align: center; vertical-align: middle;" colspan="15"><b>REPORTE DE MONITOR POR DISTRITO </b></th></h4>
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;">                                              
                                            <th style="text-align: left;  vertical-align: middle; padding: 3px;">
                                                &nbsp;&nbsp; <input type="checkbox" id="checkMonitor"> MONITOR
                                            </th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">DISTRITO</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">PROVINCIA</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">COORDINADOR</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">ZONA</th>
                                            <th style="text-align:center;  vertical-align: middle; padding: 3px;">N° SEDES</th>
                                            <th style="text-align:center;  vertical-align: middle; padding: 3px;" colspan="2">DESCARGÓ</th>
                                            <th style="text-align:center;  vertical-align: middle; padding: 3px" colspan="2">INSTALÓ</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;" colspan="2">INICIÓ SESIÓN</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;" colspan="4">ESTADO DE LLAMADAS</th>
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;">
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">  </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> SÍ </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> NO </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> SÍ </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> NO </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> SÍ </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> NO </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> CONTACTADO </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> POR CONTACTAR </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> NO CONTACTADO </td>
                                            
                                        </tr>                                        
                                        <?php $i = 0;?>
                                            @foreach( $monitores as $monitor )
                                            
                                            <?php 
                                            $i++;
                                            
                                            if( $monitor->laboratorio == 0){
                                            $porcedesc = number_format((($monitor->descargasi / 1) * 100), '2',',',''); 
                                            $porceinstalo = number_format((($monitor->instalosi / 1) * 100), '2',',',''); 
                                            $porcesesion = number_format((($monitor->sesionsi / 1) * 100), '2',',',''); 
                                            $porcecontactado = number_format((($monitor->contactado / 1) * 100), '2',',',''); 
                                            $porcenocontactado = number_format((($monitor->nocontactado / 1) * 100), '2',',',''); 
                                            $porceenproceso = number_format((($monitor->enproceso / 1) * 100), '2',',',''); 
                                            $porcenovedad = number_format((($monitor->novedadllamada / 1) * 100), '2',',',''); 
                                            }else{
                                            $porcedesc = number_format((($monitor->descargasi / $monitor->laboratorio) * 100), '2',',',''); 
                                            $porceinstalo = number_format((($monitor->instalosi / $monitor->laboratorio) * 100), '2',',',''); 
                                            $porcesesion = number_format((($monitor->sesionsi / $monitor->laboratorio) * 100), '2',',',''); 
                                            $porcecontactado = number_format((($monitor->contactado / $monitor->laboratorio) * 100), '2',',','');
                                            $porcenocontactado = number_format((($monitor->nocontactado / $monitor->laboratorio) * 100), '2',',','');
                                            $porceenproceso = number_format((($monitor->enproceso / $monitor->laboratorio) * 100), '2',',','');
                                            $porcenovedad = number_format((($monitor->novedadllamada / $monitor->laboratorio) * 100), '2',',','');
                                            
                                            }
                                            ?>
                                        <tr> 
                                            <td style="text-align: left; vertical-align: middle; padding: 3px;">
                                                    <label>
                                                        <input type="checkbox" id="monitor{{ $i }}" value="{{ $monitor->monitor }}" name="monitor[]" onclick="selectDistrito({{ $i }}); select_monitor();">
                                                        {{ $monitor->name_monitor }}
                                                    </label>
                                            </td> 
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">
                                                    <label>    
                                                        <input type="checkbox" id="distritom{{ $i }}" value="{{ $monitor->distrito_id }}" name="distrito_m[]" hidden>
                                                    {{ $monitor->distrito_id }}
                                                    </label>
                                            </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">
                                                    <label>    
                                                        <input type="checkbox" id="provinciam{{ $i }}" value="{{ $monitor->provincia }}" name="provincia_m[]" hidden>
                                                    {{ $monitor->provincia }}
                                                    </label>
                                            </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">
                                                    <label>    
                                                        <input type="checkbox" id="coordinadorm{{ $i }}" value="{{ $monitor->coordinador }}" name="coordinador_m[]" hidden>
                                                    {{ $monitor->coordinador }}
                                                    </label>
                                            </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">
                                                    <label>    
                                                        <input type="checkbox" id="zonasm{{ $i }}" value="{{ $monitor->zona }}" name="zona_m[]" hidden>
                                                    {{ $monitor->zona }}
                                                    </label>
                                            </td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">{{ $monitor->laboratorio }}</td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                                <div class="progress">
                                                    <?php if($porcedesc <= 44 ){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">     
                                                    <?php }elseif($porcedesc >= 45 && $porcedesc <= 99  ) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($porcedesc >= 100 ) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>( {{ $monitor->descargasi }} ) {{ $porcedesc.'%' }}</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">{{ $monitor->descargano }}</td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php if( $porceinstalo <= 44 ){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">     
                                                    <?php }elseif($porceinstalo >= 45 && $porceinstalo <= 99 ) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($porceinstalo >= 100) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">
                                                    <?php } ?>        
                                                      <span class="skill"><i class="val"><b>( {{ $monitor->instalosi }} ) {{ $porceinstalo.'%' }}</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">{{ $monitor->instalono }}</td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php if($porcesesion <=44 ){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">     
                                                    <?php }elseif($porcesesion >= 45 && $porcesesion <= 99) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($porcesesion >= 100) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>( {{ $monitor->sesionsi }} ) {{ $porcesesion.'%' }}</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">{{ $monitor->sesionno }}</td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php if($porcecontactado <= 44){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">     
                                                    <?php }elseif($porcecontactado >= 45 && $porcecontactado <= 99) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($porcecontactado >= 100) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>( {{ $monitor->contactado }} ) {{ $porcecontactado }}%</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>
                                            <td style="text-align: right; vertical-align: middle; padding-right: 5px;"><b>{{ $monitor->enproceso }}</b></td>
                                            <!-- <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php /* if($monitor->enproceso ==0){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                    <?php }elseif($monitor->enproceso != $monitor->laboratorio) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($monitor->enproceso == $monitor->laboratorio) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">
                                                    <?php } */ ?>        
                                                        <span class="skill"><i class="val"><b>( {{ $monitor->enproceso }} ) {{ $porceenproceso }}%</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td> -->
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php if($porcenocontactado <= '0,00' ){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                    <?php }elseif($porcenocontactado >=0 && $porcenocontactado <= 19) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($porcenocontactado >= 20) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>( {{ $monitor->nocontactado }} ) {{ $porcenocontactado }}%</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>
                                            <!-- <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php /* if($monitor->novedadllamada == $monitor->nocontactado){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                    <?php }elseif($monitor->novedadllamada != $monitor->nocontactado) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">    
                                                    <?php } */?>        
                                                        <span class="skill"><i class="val"><b>( {{ $monitor->novedadllamada }} ) {{ $porcenovedad }}%</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td> -->                                           
                                        </tr>
                                            @endforeach
                                            <tr>
                                             <td style="background-color: #0489B1; color: white;"  colspan="5"><b>TOTALES</b></td>
                                             <?php
                                             if($TotalLab == 0){
                                            $porcedesca = ($TotalDescargosi / 1); 
                                            $porceinstal = ($TotalInstalosi / 1); 
                                            $porcesesio = ($TotalSesionsi / 1) ; 
                                            $porcecontac = ($TotalContactado / 1) ; 
                                            $porcenocontac = ($TotalNoContactado / 1) ; 
                                            $porceenproc = ($TotalEnProceso / 1) ; 
                                            $porcenoved = ($TotalNovedad / 1) ; 
                                            }else{
                                            $porcedesca = number_format((($TotalDescargosi / $TotalLab) * 100), '2',',',''); 
                                            $porceinstal = number_format((($TotalInstalosi / $TotalLab) * 100), '2',',',''); 
                                            $porcesesio = number_format((($TotalSesionsi / $TotalLab) * 100), '2',',',''); 
                                            $porcecontac = number_format((($TotalContactado / $TotalLab) * 100), '2',',',''); 
                                            $porcenocontac = number_format((($TotalNoContactado / $TotalLab) * 100), '2',',',''); 
                                            $porceenproc = number_format((($TotalEnProceso / $TotalLab) * 100), '2',',',''); 
                                            $porcenoved = number_format((($TotalNovedad / $TotalLab) * 100), '2',',',''); 
                                            }
                                             ?>
                                             <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalLab }}</b></td>
                                            <?php if( $TotalDescargosi ==0){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalDescargosi }} &nbsp;&nbsp;
                                            <?php }elseif( $TotalDescargosi != $TotalLab){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalDescargosi }} &nbsp;&nbsp;
                                            <?php }elseif( $TotalDescargosi == $TotalLab){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalDescargosi }} &nbsp;&nbsp;        
                                            <?php } ?>
                                             ({{ $porcedesca }}) %</b></td>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalDescargono }}  </b></td> 
                                             <?php if( $TotalInstalosi ==0){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalInstalosi }} &nbsp;&nbsp;
                                            <?php }elseif( $TotalInstalosi != $TotalLab){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalInstalosi }} &nbsp;&nbsp;
                                            <?php }elseif(  $TotalInstalosi == $TotalLab){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalInstalosi }} &nbsp;&nbsp;        
                                            <?php } ?>        
                                                    ({{ $porceinstal }}) %</b></td>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalInstalono }} </b></td>
                                             <?php if( $TotalSesionsi==0 ){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalSesionsi }} &nbsp;&nbsp;
                                            <?php }elseif( $TotalSesionsi != $TotalLab){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalSesionsi }} &nbsp;&nbsp;
                                            <?php }elseif( $TotalSesionsi == $TotalLab){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalSesionsi }} &nbsp;&nbsp;        
                                            <?php } ?>         
                                            ({{ $porcesesio }}) %</b></td>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalSesionno }} </b></td>
                                            <?php if( $TotalContactado ==0){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ $TotalContactado }} ) {{ $porcecontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalContactado != $TotalLab){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ $TotalContactado }} ) {{ $porcecontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalContactado == $TotalLab){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ $TotalContactado }} ) {{ $porcecontac }}% &nbsp;&nbsp;        
                                            <?php } ?>     
                                            <?php if( $TotalEnProceso ==0){?>
                                            <td align="right" style="background-color: #0489B1; color: white; padding-right: 5px;"><b> {{ $TotalEnProceso }} <!-- ) {{ $porceenproc }}% &nbsp;&nbsp; -->
                                            <?php }elseif( $TotalEnProceso != $TotalLab){?>
                                            <td align="right" style="background-color: #0489B1; color: white; padding-right: 5px;"><b> {{ $TotalEnProceso }}  <!-- ) {{ $porceenproc }}% &nbsp;&nbsp; -->
                                            <?php }elseif( $TotalEnProceso == $TotalLab){?>
                                            <td align="right" style="background-color: #0489B1; color: white; padding-right: 5px;"><b> {{ $TotalEnProceso }}  <!-- ) {{ $porceenproc }}% &nbsp;&nbsp; -->
                                            <?php } ?>
                                            <?php if( $TotalNoContactado ==0 && $porcecontac =='100,00' ){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ $TotalNoContactado }} ) {{ $porcenocontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalNoContactado >0 && $porcecontac !='100,00'){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ $TotalNoContactado }} ) {{ $porcenocontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalNoContactado ==0 && $porcecontac !='100,00' ){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ $TotalNoContactado }} ) {{ $porcenocontac }}% &nbsp;&nbsp;        
                                            <?php } ?>           
                                            <?php /* if( $TotalNovedad == $TotalNoContactado){?>
                                            <td align="right" style="background-color: #5FB404; color: white;"><b>( {{ $TotalNovedad }} ) {{ $porcenoved }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalNovedad != $TotalNoContactado){?>
                                            <td align="right" style="background-color: #D7DF01; color: white;"><b>( {{ $TotalNovedad }} ) {{ $porcenoved }}% &nbsp;&nbsp;      
                                            <?php } */ ?>        
                                            
                                        </tr>
                                        <tr>
                                           <td colspan="15"> 
                                              <!-- {!! Form::submit('Para consulta de Distrito por monitor hacer Click aqui', ['class' => 'btn btn-block btn-primary', 'onclick' => 'alert(hola);']) !!} -->
                                              <button class="btn btn-block btn-info" type="submit" id="enviar_labmonitor" style="display: none;"><b>Para consulta de Laboratorio por Monitor hacer Click aqui</b></button>
                                           </td>
                                       </tr>
                                    </table>
                                   {!! Form::close() !!}
                                </div>
                            </div>                        