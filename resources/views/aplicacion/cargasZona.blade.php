<script>
$("#checkZona").change(function(){
    @foreach( $reportecarga as $cargazona )
   $("#zona{{ $cargazona->zona }}").prop('checked', $(this).prop("checked"));   
   if($("#zona{{ $cargazona->zona }}").prop('checked')=== true ){
       $("#enviar_zona").css("display","block");
   }else{
       $("#enviar_zona").css("display","none");
   }
    @endforeach;
});      

function select_zona(){
    var i = 0;
    @foreach( $reportecarga as $selectz )
    var selectZona = $("#zona{{ $selectz->zona }}").prop('checked');     
    if (selectZona){
         i = i + 1;
       $('#enviar_zona').css("display", "block");
    }else if (i == 0 ){
       $('#enviar_zona').css("display", "none"); 
    }  
    @endforeach;
};
    
$(function(){
$("#enviar_zona").click(function(){        
    var id_periodo = $("#periodo").val();
    var fecha = $("#fecha_programada").val();
    var sesion = $("#sesion").val();
    var tp_reporte = $("#tp_reporte").val();    
    if (id_periodo === "" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "0") {
                //alert("Debe seleccionar un periodo");
                            notif({
                            msg: 'Debe Seleccionar un Periodo !',
                            type: 'warning',
                            opacity: 1,
                            });
            } else {
                $(".zona").css("display","none");
    $(".provincia").css("display","block");
    document.getElementById("loading").style.display="block";
        if (id_periodo !== "" && fecha === "" && sesion === "") {
                    var ur = "provincia_aplicacion/" + tp_reporte + "/" + id_periodo + "";
                } else {
                    if (id_periodo !== "" && fecha !== "" && sesion === "") {
                        var ur = "provincia_aplicacion/" + tp_reporte + "/" + id_periodo + "/" + fecha + "";
                    } else {
                        var ur = "provincia_aplicacion/" + tp_reporte + "/" + id_periodo + "/" + fecha + "/" + sesion + "";
                    }
                }
                 var url = ur;
                $.ajax({
                type: "POST",
                url: url,
                data: $("#formulario").serialize(),
                success: function(data)
                {
                $("#provincia").html(data);
                document.getElementById("loading").style.display="none";
                }
                }); 
         }
   return false;
   //alert(data);
});

});
</script>
<div class="zona">
        <ol class="breadcrumb">
          <li><a href="#">Zonas</a> /</li>
        </ol>
</div>

<div class="row zona">
<div class="col-lg-3 col-sm-12" style="width: auto">
                            <div class="box table-responsive no-padding">
                                <!-- <div class="box-header with-border">
                                    <h5><i class="fa fa-paste"></i> <b> Reporte de Descarga del Aplicativo</b></h5>
                                </div> /.box-header -->
                                <div class="box-body">
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 100%;">
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >{{ $periodo }}</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >CARGAS DE ARCHIVOS</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >ESTADO</th>
                                            <th style="text-align: center;">CANTIDAD</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                        </tr>
                                        <?php 
                                        $porcargar=  $totalpro - $totalcargas ;
                                        if($totalpro == 0){
                                            $porcecarg= number_format(((  $totalcargas /  1 )*100),'2',',',''); 
                                              $porcenocarg= number_format(((  $porcargar /  1 )*100),'2',',','');
                                        }else{
                                              $porcecarg= number_format(((  $totalcargas /  $totalpro )*100),'2',',',''); 
                                              $porcenocarg= number_format(((  $porcargar /  $totalpro )*100),'2',',',''); 
                                        }
                                         ?>
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;"><b>ARCHIVOS CARGADOS</b></td>                                           
                                            <td style="text-align: right;"><b> {{ number_format($totalcargas,0,",",".") }} </b></td> 
                                            
                                            <?php if($porcecarg <= 44){ ?>                                            
                                            <td style="text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcecarg }} % </b></td>
                                            <?php }elseif($porcecarg >=45 && $porcecarg <= 99){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: white;"><b> {{ $porcecarg }} % </b></td>
                                            <?php }elseif($porcecarg >= 100){ ?>
                                            <td style="text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcecarg }} % </b></td>
                                            <?php } ?>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;"><b>ARCHIVOS POR CARGAR</b></td>                                           
                                            <td style="text-align: right;"><b> {{ number_format($porcargar,0,",",".") }} </b></td>        
                                            
                                            <?php if($porcenocarg <= '0.00'){ ?>                                            
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ $porcenocarg }} % </b></td>
                                            <?php }elseif($porcenocarg >= 0 && $porcenocarg <= 19){ ?>
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ $porcenocarg }} % </b></td>
                                            <?php }elseif($porcenocarg >= 20){ ?>
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ $porcenocarg }} % </b></td>
                                            <?php } ?>                                            
                                        </tr>   
                                        <tr>
                                            <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ number_format($totalpro,0,",",".") }}</b></td>
                                            <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100,00 % </b></td>
                                       </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div> 
    
                        <div class="col-lg-3 col-sm-12" style="width: auto">
                            <div class="box table-responsive no-padding">
                                <!-- <div class="box-header with-border">
                                    <h5><i class="fa fa-paste"></i> <b> Reporte de Descarga del Aplicativo</b></h5>
                                </div> /.box-header -->
                                <div class="box-body">
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 100%;">
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >{{ $periodo }}</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >SUSTENTANTES PROGRAMADOS</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >ESTADO</th>
                                            <th style="text-align: center;">CANTIDAD</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                        </tr>
                                         <?php 
                                            if($totalpro ==0){
                                            $porcenttAsist= number_format((($totalasis / 1)*100),'2',',','');
                                            $porcenttAusest= number_format((($TotalAusent / 1)*100),'2',',','');
                                            }else{
                                            $porcenttAsist= number_format((($totalasis / $totalpro)*100),'2',',','');
                                            $porcenttAusest= number_format((($TotalAusent / $totalpro)*100),'2',',','');
                                            }
                                            ?>                                       
                                            
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;"><b>ASISTENCIA</b></td>                                           
                                            <td align="right"><b>{{ number_format($totalasis,0,",",".") }}  </b></td>  
                                            <?php if( $porcenttAsist <=44){?>
                                            <td align="right" style="background-color: #FA5858; color: white;">
                                            <?php }elseif( $porcenttAsist >=45 && $porcenttAsist <=99){?>
                                            <td align="right" style="background-color: #D7DF01; color: white;">
                                            <?php }elseif( $porcenttAsist >=100){?>
                                            <td align="right" style="background-color: #5FB404; color: white;">        
                                            <?php } ?><b>{{ $porcenttAsist }}% </b></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;"><b>AUSENTES</b></td>                                           
                                            <td align="right" ><b>{{ number_format($TotalAusent,0,",",".") }}  </b></td>
                                            <?php if( $porcenttAusest <='0,00'){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">
                                            <?php }elseif( $porcenttAusest >=0 && $porcenttAusest <=19){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">
                                            <?php }elseif( $porcenttAusest >20){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">        
                                            <?php } ?><b>{{ $porcenttAusest }}% </b></td>                                         
                                        </tr>   
                                        <tr>
                                            <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTAL</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ number_format($totalpro,0,",",".") }}</b></td>
                                            <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100,00 % </b></td>
                                       </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div>
   
</div>

<div class="row zona" style="display: block;">
    <div class="col-sm-12 col-md-9" style="width: auto;">
<div class="box table-responsive no-padding">                               
                                <div class="box-body">
                                     {!! Form::open(['method' => 'POST', 'id' => 'formulario' ]) !!}
                                    <table class="table table-bordered table-striped table-hover" style="width: auto; padding: 0px;">
                                        <tr style="background-color: #0489B1; color: white;">  
                                        <h4> <th style="text-align: center; vertical-align: middle;" colspan="11"><b>REPORTE DE CARGAS POR ZONA</b></th></h4>
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;">  
                                             <th style="width: 20px; text-align:center;" >
                                            <label>
                                                <input type="checkbox" id="checkZona"> <b>ZONA</b>                                                
                                            </label>
                                            </th>
                                            <th style="text-align: center;">Nº SEDES</th>
                                            <th style="text-align:center;" >PROGRAMADOS</th>
                                            <th style="text-align:center;">ASISTENCIA</th>
                                            <th style="text-align:center;">% ASISTENCIA</th>                                            
                                            <th style="text-align:center;">AUSENTES</th>
                                            <th style="text-align:center;">% AUSENTES</th>
                                            <th style="text-align:center;">CARGAS</th>
                                            <th style="text-align: center;">% DE CARGA</th>
                                            <th style="text-align: center;">POR CARGAR <br>(ASISTENCIA)</th>
                                            <!-- <th style="text-align: center;">% POR CARGAR</th> -->
                                        </tr>
                                        <?php $totalAusen=0; $totalLab=0; ?>
                                        @foreach( $reportecarga as $cargas)
                                         <?php 
                                            $totalLab += ($cargas->laboratorio);
                                            $totalAusen += ($cargas->programados - $cargas->asistencia);
                                            $ausentes= ($cargas->programados - $cargas->asistencia) ;
                                            $por_cargar= ($cargas->asistencia - $cargas->cargas) ;
                                            
                                            if( $cargas->programados == 0){
                                            $porcecarga = number_format((($cargas->cargas / 1) * 100), '2',',',''); 
                                            $porceasis = number_format((($cargas->asistencia / 1) * 100), '2',',',''); 
                                            $porceausente = number_format((($ausentes / 1) * 100), '2',',',''); 
                                            $porceporcargar = number_format((($por_cargar / 1) * 100), '2',',',''); 
                                            }else{
                                            $porcecarga = number_format((($cargas->cargas / $cargas->programados) * 100), '2',',',''); 
                                            $porceasis = number_format((($cargas->asistencia / $cargas->programados) * 100), '2',',',''); 
                                            $porceausente = number_format((($ausentes / $cargas->programados) * 100), '2',',','');
                                            //$porceporcargar = number_format((($por_cargar / 1) * 100), '2',',',''); 
                                            /* if($cargas->asistencia == 0){
                                            $porceporcargar = number_format((($por_cargar / 1) * 100), '2',',','');     
                                            } else {                                            
                                            $porceporcargar = number_format((($por_cargar / $cargas->asistencia) * 100), '2',',','');
                                            }
                                             */
                                            }
                                            ?>
                                        <tr style="padding: 0px;">
                                            <td style="text-align: center; vertical-align: middle; padding: 0px;">
                                                    <label>    
                                                    <input type="checkbox" id="zona{{ $cargas->zona }}" value="{{ $cargas->zona }}" name="zona[]" onclick="select_zona();">
                                                    {{ $cargas->zona }}
                                                    </label>
                                            </td>
                                            <td align="right" style="padding: 0px;"><b>{{ number_format($cargas->laboratorio,0,",",".") }}</b></td>
                                            <td align="right" style="padding: 0px;"><b>{{ number_format($cargas->programados,0,",",".") }}</b></td>
                                            <td align="right" style="padding: 0px;"><b>{{ number_format($cargas->asistencia,0,",",".") }}</b></td>
                                            <td align="right" style="padding: 0px;">
                                             <div class="progress">
                                                    <?php if($porceasis <=44){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">     
                                                    <?php }elseif($porceasis >=45 && $porceasis <=99) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01; color: #000000;">                                                        
                                                    <?php }elseif($porceasis >= 100) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>{{ $porceasis.'%' }}</b></i></span>                                                        
                                                    </div> 
                                                </div>   
                                            </td>    
                                            <td align="right" style="padding: 0px;"><b>{{ number_format($ausentes,0,",",".") }}</b></td>
                                            <td align="right" style="padding: 0px;">
                                             <div class="progress">
                                                    <?php if($porceausente <='0,00' ){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                    <?php }elseif($porceausente >=0 && $porceausente <=19) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01; color: #000000">                                                        
                                                    <?php }elseif($porceausente >= 20) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>{{ $porceausente.'%' }}</b></i></span>                                                        
                                                    </div> 
                                                </div>   
                                            </td>
                                            <td align="right" style="padding: 0px;"><b>{{ number_format($cargas->cargas,0,",",".") }}</b></td>
                                            <td align="right" style="padding: 0px;">
                                            <div class="progress">
                                                    <?php if($porcecarga <=44){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">     
                                                    <?php }elseif($porcecarga >=45 && $porcecarga <=99) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01; color: #000000;">                                                        
                                                    <?php }elseif($porcecarga >= 100) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>{{ $porcecarga.'%' }}</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>  
                                            <td align="right" style="padding: 0px;"><b>{{ number_format($por_cargar,0,",",".") }}</b></td>

                                        </tr>
                                        @endforeach
                                        <tr><input type="hidden" value="{{ $coordinador }}" name="coordinador">
                                            <?php 
                                            if($totalpro ==0){
                                            $porcenttAsis= number_format((($totalasis / 1)*100),'2',',','');
                                            $porcenttAuses= number_format((($totalAusen / 1)*100),'2',',','');
                                            $porcenttPorCargar= number_format((($TotalPorCargar / 1)*100),'2',',','');    
                                            }else{
                                            $porcenttAsis= number_format((($totalasis / $totalpro)*100),'2',',','');
                                            $porcenttAuses= number_format((($totalAusen / $totalpro)*100),'2',',','');
                                            //$porcenttPorCargar= number_format((($TotalPorCargar / $totalasis)*100),'2',',','');
                                            if ($totalasis == 0){
                                            $porcenttPorCargar= number_format((($TotalPorCargar / 1)*100),'2',',','');    
                                            }else{
                                            $porcenttPorCargar= number_format((($TotalPorCargar / $totalasis)*100),'2',',','');
                                                }                                                                                       
                                            }
                                            ?>
                                             <td style="background-color: #0489B1; color: white;"><b>TOTALES</b></td>
                                             <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ number_format($totalLab,0,",",".") }}</b></td>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ number_format($totalpro,0,",",".") }}</b></td>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ number_format($totalasis,0,",",".") }}  </b></td>  
                                            <?php if( $porcenttAsis <=75){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">
                                            <?php }elseif( $porcenttAsis >75 && $porcenttAsis <=99){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">
                                            <?php }elseif( $porcenttAsis >=100){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">        
                                            <?php } ?><b>{{ $porcenttAsis }}% </b></td>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ number_format($totalAusen,0,",",".") }}  </b></td>
                                            <?php if( $porcenttAuses ==0){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">
                                            <?php }elseif( $porcenttAuses >0 && $porcenttAuses <=25){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">
                                            <?php }elseif( $porcenttAuses >25){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">        
                                            <?php } ?><b>{{ $porcenttAuses }}% </b></td>                                                          
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ number_format($totalcargas,0,",",".") }} </b></td>                                            
                                            <?php if( $porcentajecar <=75){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">
                                            <?php }elseif( $porcentajecar >75 && $porcentajecar <=99){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">
                                            <?php }elseif( $porcentajecar >=100){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">        
                                            <?php } ?><b>{{ $porcentajecar }}% </b></td>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ number_format($TotalPorCargar,0,",",".") }}  </b></td>
                                             
                                        </tr>
                                        <tr>
                                           <td colspan="11"> 
                                              <!-- {!! Form::submit('Para consulta de Distrito por Zona hacer Click aqui', ['class' => 'btn btn-block btn-primary', 'onclick' => 'alert(hola);']) !!} -->
                                              <button class="btn btn-block btn-info" type="submit" id="enviar_zona" style="display: none;"><b>Para consulta de Zona por Provincia hacer Click aqui</b></button>
                                           </td>
                                       </tr>
                                    </table>
                                      {!! Form::close() !!}
                                </div>
                            </div>
                         </div>
                       </div>
                                                
                                                <!-- Reporte Provincia-->
                                                <div class="row provincia" style="display: none;">                       
                                                    <div id="provincia" class="col-lg-7 col-sm-12" style=" width: auto;">                            
                                                    </div>
                                                </div>                     
                                                <!-- Reporte Distrito-->                    
                                                <div class="row distritod" style="display: none;">                        
                                                    <div id="distrito" class="col-lg-12 col-sm-12">

                                                    </div>
                                                </div> 
                                                <!-- Reporte Monitor--> 
                                                <div class="row monitord" style="display: none;">
                                                    <div id="monitor" class="col-sm-12">
                                                    </div>
                                                </div>
                                                <!-- Reporte Laboratorios-->
                                                <div class="row labmonitores" style="display: none;"> 
                                                    <div id="laboratorio" class="col-lg-7 col-sm-12" style=" width: auto;">
                                                    </div>
                                                </div>
                                                <!-- Reporte Sustentantes-->
                                                 <div class="row sustentantes" style="display: none;"> 
                                                     <div id="sustentante" class="col-sm-12" style=" width: auto;">
                                                     </div>
                                                 </div>