<script>
$("#checkDistrito").change(function(){
    var i =0;
    @foreach( $cargasDistritos as $dist )
    i = i + 1;
   $("#distrito"+i).prop('checked', $(this).prop("checked"));
   $("#provinciasd"+i).prop('checked', $(this).prop("checked"));
   $("#coordinadord"+i).prop('checked', $(this).prop("checked"));
   $("#zonasd"+i).prop('checked', $(this).prop("checked")); 
   if($("#distrito"+i).prop('checked')=== true ){
       $("#envio_distrito").css("display","block");
   }else{
       $("#envio_distrito").css("display","none");
   }
    @endforeach;
});

function select_distrito(){
    var p = 0;
    var i = 0;
    @foreach( $cargasDistritos as $selectd )
    p = p + 1;
    var selectDistrito = $("#distrito"+p).prop('checked');     
    if (selectDistrito){
         i = i + 1;
       $('#envio_distrito').css("display", "block");
    }else if (i == 0 ){
       $('#envio_distrito').css("display", "none"); 
    }  
    @endforeach;
};

function selectProvincia(i){
    
    $("#distrito"+i).change(function(){
    $("#provinciasd"+i).prop('checked', $(this).prop("checked"));
    $("#coordinadord"+i).prop('checked', $(this).prop("checked"));
    $("#zonasd"+i).prop('checked', $(this).prop("checked"));
    });
};

$(function(){
$("#envio_distrito").click(function(){        
    var id_periodo = $("#periodo").val();
    var fecha = $("#fecha_programada").val();
    var sesion = $("#sesion").val();
    var tp_reporte = $("#tp_reporte").val();
    
    if (id_periodo === "" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "0") {
                //alert("Debe seleccionar un periodo");
                            notif({
                            msg: 'Debe Seleccionar un Periodo !',
                            type: 'warning',
                            opacity: 1,
                            });
            } else {
        $(".distritod").css("display","none");
    $(".monitord").css("display","block");
    document.getElementById("loading").style.display="block";
        if (id_periodo !== "" && fecha === "" && sesion === "") {
                    var ur = "monitor_aplicacion/" + tp_reporte + "/" + id_periodo + "";
                } else {
                    if (id_periodo !== "" && fecha !== "" && sesion === "") {
                        var ur = "monitor_aplicacion/" + tp_reporte + "/" + id_periodo + "/" + fecha + "";
                    } else {
                        var ur = "monitor_aplicacion/" + tp_reporte + "/" + id_periodo + "/" + fecha + "/" + sesion + "";
                    }
                }
                 var url = ur;
                $.ajax({
                type: "POST",
                url: url,
                data: $("#formulariod").serialize(),
                success: function(data)
                {
                $("#monitor").html(data);
                document.getElementById("loading").style.display="none";
                }
                }); 
         }
   return false;
   //alert(data);
});

});

$(function(){
    $(".azona").click(function(){
    $(".zona").css("display", "block");
    $(".provincia").css("display", "none");
    $(".distritod").css("display", "none");
    $(".monitord").css("display", "none");          
    $(".labmonitores").css("display","none"); 
    });
    });
    
$(function(){
    $(".aprovincia").click(function(){
    $(".provincia").css("display", "block");
    $(".zona").css("display", "none");
    $(".distritod").css("display", "none");
    $(".monitord").css("display", "none");          
    $(".labmonitores").css("display","none"); 
    });
});

</script>
<ol class="breadcrumb">
    <li><a href="#" class="azona">Zona</a></li>
    <li><a href="#" class="aprovincia">Provincia</a></li>
    <li class="active">Distrito</li>
</ol>
<div class="row">
    <div class="col-sm-12 col-md-10" style="width: auto;">
<div class="box table-responsive no-padding">                               
                                <div class="box-body">
                                     {!! Form::open(['method' => 'POST', 'id' => 'formulariod' ]) !!}
                                    <table class="table table-bordered table-striped table-hover" style="width: auto; padding: 0px;">
                                        <tr style="background-color: #0489B1; color: white;">  
                                        <h4> <th style="text-align: center; vertical-align: middle;" colspan="14"><b>REPORTE DE CARGA DE PROVINCIA POR DISTRITO</b></th></h4>
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;">  
                                            <th style="text-align: left;  vertical-align: middle; padding: 3px;">
                                                &nbsp;&nbsp; <input type="checkbox" id="checkDistrito"> DISTRITO
                                            </th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">PROVINCIA</th>
                                            <th style="text-align:center; vertical-align: middle; padding: 3px;">COORDINADOR</th>
                                            <th style="text-align:center; vertical-align: middle; padding: 3px;">ZONA</th>
                                            <th style="text-align:center; vertical-align: middle; padding: 3px;">Nº SEDES</th>
                                            <th style="text-align:center; vertical-align: middle; padding: 3px;" >PROGRAMADOS</th>
                                            <th style="text-align:center; vertical-align: middle; padding: 3px;">ASISTENCIA</th>
                                            <th style="text-align:center; vertical-align: middle; padding: 3px;">% ASISTENCIA</th>                                            
                                            <th style="text-align:center; vertical-align: middle; padding: 3px;">AUSENTES</th>
                                            <th style="text-align:center; vertical-align: middle; padding: 3px;">% AUSENTES</th>
                                            <th style="text-align:center; vertical-align: middle; padding: 3px;">CARGAS</th>
                                            <th style="text-align: center; vertical-align: middle; padding: 3px;">% DE CARGA</th>
                                            <th style="text-align: center; vertical-align: middle; padding: 3px;">POR CARGAR <br>(ASISTENCIA)</th>
                                            <th style="text-align: center; vertical-align: middle; padding: 3px;">% POR CARGAR</th>
                                        </tr>
                                        <?php $totalAusen=0; $totalLab=0; $i=0;?>
                                        @foreach( $cargasDistritos as $cargasDistrito)
                                         <?php 
                                         $i++;
                                            $totalLab += ($cargasDistrito->laboratorio);
                                            $totalAusen += ($cargasDistrito->programados - $cargasDistrito->asistencia);
                                            $ausentes= ($cargasDistrito->programados - $cargasDistrito->asistencia) ;
                                            $por_cargar= ($cargasDistrito->asistencia - $cargasDistrito->cargas) ;
                                            if( $cargasDistrito->programados == 0){
                                            $porcecarga = number_format((($cargasDistrito->cargas / 1) * 100), '2',',',''); 
                                            $porceasis = number_format((($cargasDistrito->asistencia / 1) * 100), '2',',',''); 
                                            $porceausente = number_format((($ausentes / 1) * 100), '2',',',''); 
                                            }else{
                                            $porcecarga = number_format((($cargasDistrito->cargas / $cargasDistrito->programados) * 100), '2',',',''); 
                                            $porceasis = number_format((($cargasDistrito->asistencia / $cargasDistrito->programados) * 100), '2',',',''); 
                                            $porceausente = number_format((($ausentes / $cargasDistrito->programados) * 100), '2',',',''); 
                                            if($cargasDistrito->asistencia == 0){
                                            $porceporcargar = number_format((($por_cargar / 1) * 100), '2',',','');     
                                            } else {                                            
                                            $porceporcargar = number_format((($por_cargar / $cargasDistrito->asistencia) * 100), '2',',','');
                                            }
                                            }
                                            ?>
                                        <tr style="padding: 0px;">
                                             <td style="text-align: left; vertical-align: middle; padding: 3px;">
                                                    <label>
                                                        <input type="checkbox" id="distrito{{ $i }}" value="{{ $cargasDistrito->distrito_id }}" name="distrito[]" onclick="selectProvincia({{ $i }}); select_distrito();">
                                                        {{ $cargasDistrito->distrito_id }}
                                                    </label>
                                            </td>
                                            <td style="text-align: left; vertical-align: middle; padding: 3px;">
                                                    <label>
                                                        <input type="checkbox" id="provinciasd{{ $i }}" value="{{ $cargasDistrito->provincia }}" name="provincia_d[]" hidden>
                                                        {{ $cargasDistrito->provincia }}
                                                    </label>
                                            </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">
                                                    <label>    
                                                        <input type="checkbox" id="coordinadord{{ $i }}" value="{{ $cargasDistrito->coordinador }}" name="coordinador_d[]" hidden>
                                                    {{ $cargasDistrito->coordinador }}
                                                    </label>
                                            </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">
                                                    <label>    
                                                        <input type="checkbox" id="zonasd{{ $i }}" value="{{ $cargasDistrito->zona }}" name="zona_d[]" hidden>
                                                    {{ $cargasDistrito->zona }}
                                                    </label>
                                            </td>
                                            <td align="right" style="padding: 0px;">{{ $cargasDistrito->laboratorio }}</td>
                                            <td align="right" style="padding: 0px;">{{ $cargasDistrito->programados }}</td>
                                            <td align="right" style="padding: 0px;">{{ $cargasDistrito->asistencia }}</td>
                                            <td align="right" style="padding: 0px;">
                                             <div class="progress">
                                                    <?php if($porceasis <=30){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">     
                                                    <?php }elseif($porceasis >=31 && $porceasis <=79) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($porceasis >= 80) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>{{ $porceasis.'%' }}</b></i></span>                                                        
                                                    </div> 
                                                </div>   
                                            </td>    
                                            <td align="right" style="padding: 0px;">{{ $ausentes }}</td>
                                            <td align="right" style="padding: 0px;">
                                             <div class="progress">
                                                    <?php if($porceausente <=30 ){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                    <?php }elseif($porceausente >=31 && $porceausente <=79) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($porceausente >= 80) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>{{ $porceausente.'%' }}</b></i></span>                                                        
                                                    </div> 
                                                </div>   
                                            </td>
                                            <td align="right" style="padding: 0px;">{{ $cargasDistrito->cargas }}</td>
                                            <td align="right" style="padding: 0px;">
                                            <div class="progress">
                                                    <?php if($porcecarga <=30){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">     
                                                    <?php }elseif($porcecarga >=31 && $porcecarga <=79) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($porcecarga >= 80) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>{{ $porcecarga.'%' }}</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>   
                                            <td align="right" style="padding: 0px;"><b>{{ $por_cargar }}</b></td>
                                            <td align="right" style="padding: 0px;">
                                             <div class="progress">
                                                    <?php if($porceporcargar <='0,00' ){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                    <?php }elseif($porceporcargar >=0 && $porceporcargar <=19) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01; color: #000000">                                                        
                                                    <?php }elseif($porceporcargar >= 20) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>{{ $porceporcargar.'%' }}</b></i></span>                                                        
                                                    </div> 
                                                </div>   
                                            </td>
                                        </tr>
                                        @endforeach
                                        <tr>
                                            <?php 
                                            if($totalpro ==0){
                                                $porcenttAsis= number_format((($totalasis / 1)*100),'2',',','');
                                            $porcenttAuses= number_format((($TotalAusent / 1)*100),'2',',','');
                                            }else{
                                            $porcenttAsis= number_format((($totalasis / $totalpro)*100),'2',',','');
                                            $porcenttAuses= number_format((($TotalAusent / $totalpro)*100),'2',',','');
                                            if ($totalasis == 0){
                                            $porcenttPorCargar= number_format((($TotalPorCargar / 1)*100),'2',',','');    
                                            }else{
                                            $porcenttPorCargar= number_format((($TotalPorCargar / $totalasis)*100),'2',',','');
                                                }                                           
                                            }
                                            ?>
                                        <td style="background-color: #0489B1; color: white;" colspan="4"><b>TOTALES</b></td>
                                             <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ $totalLab }}</b></td>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $totalpro }}</b></td>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $totalasis }}  </b></td>  
                                            <?php if( $porcenttAsis <=75){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">
                                            <?php }elseif( $porcenttAsis >75 && $porcenttAsis <=99){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">
                                            <?php }elseif( $porcenttAsis >=100){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">        
                                            <?php } ?><b>{{ $porcenttAsis }}% </b></td>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalAusent }}  </b></td>
                                            <?php if( $porcenttAuses ==0){?>
                                            <td align="right" style="background-color: #5FB404; color: white;">
                                            <?php }elseif( $porcenttAuses >0 && $porcenttAuses <=25){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">
                                            <?php }elseif( $porcenttAuses >25){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">        
                                            <?php } ?><b>{{ $porcenttAuses }}% </b></td>                                                          
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $totalcargas }} </b></td>                                            
                                            <?php if( $porcentajecar <=75){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">
                                            <?php }elseif( $porcentajecar >75 && $porcentajecar <=99){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">
                                            <?php }elseif( $porcentajecar >=100){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">        
                                            <?php } ?><b>{{ $porcentajecar }}% </b></td>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalPorCargar }}  </b></td>
                                            <?php if( $porcenttPorCargar ==0){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">
                                            <?php }elseif( $porcenttPorCargar >0 && $porcenttPorCargar <=25){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">
                                            <?php }elseif( $porcenttPorCargar >25){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">        
                                            <?php } ?><b>{{ $porcenttPorCargar }}% </b></td> 
                                        </tr>
                                        <tr>
                                           <td colspan="14">                                               
                                               <button class="btn btn-block btn-info" type="submit" id="envio_distrito" style="display: none;"><b>Para consulta de Distrito por Monitor hacer Click aqui</b></button>
                                           </td>
                                       </tr>
                                    </table>
                                      {!! Form::close() !!}
                                </div>
                            </div>
                         </div>
                       </div>