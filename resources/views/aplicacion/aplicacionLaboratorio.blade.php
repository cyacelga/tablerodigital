<script>
$(function(){
    $(".azona").click(function(){
    $(".zona").css("display", "block");
    $(".provincia").css("display", "none");
    $(".distritod").css("display", "none");
    $(".monitord").css("display", "none");
    $(".labmonitores").css("display","none"); 
    });
    });
    
$(function(){
    $(".aprovincia").click(function(){
    $(".provincia").css("display", "block");
    $(".zona").css("display", "none");
    $(".distritod").css("display", "none");
    $(".monitord").css("display", "none");          
    $(".labmonitores").css("display","none"); 
    });
});

$(function(){
    $(".adistrito").click(function(){
    $(".provincia").css("display", "none");
    $(".zona").css("display", "none");
    $(".distritod").css("display", "block");
    $(".monitord").css("display", "none");          
    $(".labmonitores").css("display","none"); 
    });
});

$(function(){
    $(".amonitor").click(function(){
    $(".provincia").css("display", "none");
    $(".zona").css("display", "none");
    $(".distritod").css("display", "none");
    $(".monitord").css("display", "block");          
    $(".labmonitores").css("display","none"); 
    });
});
</script>
  
    <ol class="breadcrumb">
        <li><a href="exportar_info_aplicacion/{{ $idperiodo }}/excel">
            <img src="images/excel.png" title="DESCARGAR EXCEL" style=" height: 23px; margin-right: -13px;">
            </a> &nbsp; &nbsp; &nbsp;
        </li>
        <li><a href="#" class="azona">Zonas</a></li>
        <li><a href="#" class="aprovincia">Provincias</a></li>
        <li><a href="#" class="adistrito">Distritos</a></li>
        <li><a href="#" class="amonitor">Monitores</a></li>
        <li class="active">Laboratorios</li>
    </ol>
   
<div class="box table-responsive no-padding " style="width: auto;">
                                <div class="box-body">
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 90%;">
                                        <tr style="background-color: #0489B1; color: white;">  
                                        <h4> <th style="text-align: center; vertical-align: middle;" colspan="16"><b>REPORTE DE LABORATORIOS POR MONITOR</b></th></h4>
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;">  
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">Nº</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">MONITOR</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">DISTRITO</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">PROVINCIA</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">COORDINADOR</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">ZONA</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">INSTITUCIÓN</th>
                                            <th style="text-align:center;  vertical-align: middle; padding: 3px;">AMIE</th>
                                            <th style="text-align:center;  vertical-align: middle; padding: 3px;">LABORATORIO</th>
                                            <th style="text-align:center;  vertical-align: middle; padding: 3px;">SESIÓN</th>
                                            <th style="text-align:center;  vertical-align: middle; padding: 3px;">DESCARGÓ</th>
                                            <th style="text-align:center;  vertical-align: middle; padding: 3px">INSTALÓ</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">INICIÓ SESIÓN</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">PROBLEMA DE SESIÓN.</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">ESTADO LLAMADA</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">PROBLEMA LLAMADA</th>
                                        </tr>                                   
                                        <?php $i = 0;?>
                                            @foreach( $labMonitor as $labmonitor )
                                        <?php $i = $i + 1;?>                                                
                                            <tr>                                            
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"><b> {{ $i }} </b></td>
                                            <td style="text-align: left; vertical-align: middle; padding: 3px;">{{ $labmonitor->name_monitor }}</td>
                                            <td style="text-align: left; vertical-align: middle; padding: 3px;">{{ $labmonitor->distrito_id }}</td>
                                            <td style="text-align: left; vertical-align: middle; padding: 3px;">{{ $labmonitor->provincia }}</td>
                                            <td style="text-align: left; vertical-align: middle; padding: 3px;">{{ $labmonitor->coordinador }}</td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">{{ $labmonitor->zona }}</td>
                                            <td style="text-align: left; vertical-align: middle; padding: 3px;">{{ $labmonitor->institucion }}</td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">{{ $labmonitor->amie }}</td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">{{ $labmonitor->sede_laboratorio }}</td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">{{ $labmonitor->sesion }}</td>
                                            <?php if( $labmonitor->descarga=='SI' ){?>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px; color: white; background-color: #5FB404;">
                                            <?php }elseif( $labmonitor->descarga=='NO' ){?>    
                                            <td style="text-align: center; vertical-align: middle; padding: 3px; color: white; background-color: #FA5858;">
                                            <?php } ?>    
                                                {{ $labmonitor->descarga }}</td>
                                            <?php if( $labmonitor->instalo=='SI' ){?>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px; color: white; background-color: #5FB404;">
                                            <?php }elseif( $labmonitor->instalo=='NO' ){?>    
                                            <td style="text-align: center; vertical-align: middle; padding: 3px; color: white; background-color: #FA5858;">
                                            <?php } ?>
                                                {{ $labmonitor->instalo }}</td>                                           
                                            <?php if( $labmonitor->estadosesion=='SI' ){?>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px; color: white; background-color: #5FB404;">
                                            <?php }elseif( $labmonitor->estadosesion=='NO' ){?>    
                                            <td style="text-align: center; vertical-align: middle; padding: 3px; color: white; background-color: #FA5858;">
                                            <?php } ?>
                                                {{ $labmonitor->estadosesion }}</td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">{{ $labmonitor->observacion_sesion }}</td>
                                            <?php if( $labmonitor->estadollamada=='CONTACTADO' ){?>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px; color: white; background-color: #5FB404;">
                                            <?php }elseif( $labmonitor->estadollamada=='NO CONTACTADO' ){?>    
                                            <td style="text-align: center; vertical-align: middle; padding: 3px; color: white; background-color: #FA5858;">
                                            <?php }elseif( $labmonitor->estadollamada=='POR CONTACTAR' ){?>    
                                            <td style="text-align: center; vertical-align: middle; padding: 3px; color: white; background-color: #D7DF01;">
                                            <?php } ?>
                                                {{ $labmonitor->estadollamada }}</td>
                                            <?php if( $labmonitor->novedadllamada=='' ){?>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">
                                            <?php }elseif( $labmonitor->novedadllamada!='VOLVER A LLAMAR' && $labmonitor->novedadllamada!='' ){?>    
                                            <td style="text-align: center; vertical-align: middle; padding: 3px; color: white; background-color: #FA5858;">
                                            <?php }elseif( $labmonitor->novedadllamada=='VOLVER A LLAMAR' ){?>    
                                            <td style="text-align: center; vertical-align: middle; padding: 3px; color: white; background-color: #D7DF01;">
                                            <?php } ?>
                                                {{ $labmonitor->novedadllamada }}</td>
                                        </tr>
                                        
                                         @endforeach
                                    </table>
                                </div>
                            </div>