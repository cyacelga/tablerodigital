
<div class="zona">
        <ol class="breadcrumb">                   
            <li><a href="exportar_info_aplicacion_online/{{ $idperiodo }}/excel">
                <img src="images/excel.png" title="DESCARGAR EXCEL" style=" height: 23px; margin-right: -13px;">
                </a> &nbsp; &nbsp; &nbsp;
            </li>
            <li><a href="#">Zonas</a></li>
        </ol>
</div>
<div class="row zona">
    <div class="col-lg-3 col-sm-12" style="width: auto;">
                            <div class="box table-responsive no-padding">
                                <div class="box-body">
                                    <table class="table table-bordered table-striped table-hover" style="width: auto; font-size: 83%;">
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >{{ $periodo }}</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >ESTADO DE REUNIÓN</th>
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >ESTADO</th>
                                            <th style="text-align: center;">CANTIDAD</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                        </tr>
                                         @foreach( $reporteLlamada as $llamada)
                                        <?php 
                                        if( $TotalLlamada == 0){
                                            $porcenllamada = number_format((($llamada->cantidadllamada / 1) * 100), '2',',','') ;
                                        }else{
                                            $porcenllamada = number_format((($llamada->cantidadllamada / $TotalLlamada) * 100), '2',',',''); 
                                        }
                                        ?>
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;"><b>{{ $llamada->estadollamada }}</b></td>                                           
                                            <td style="text-align: right;"><b> {{ number_format($llamada->cantidadllamada,0,",",".") }} </b></td>
                                            <?php
                                            if( $llamada->estadollamada == 'INICIADA'){
                                            if($porcenllamada <= 44){ ?>                                            
                                            <td style="text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcenllamada }} % </b></td>
                                            <?php }elseif($porcenllamada >= 45 && $porcenllamada <= 99){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: #000000;"><b> {{ $porcenllamada }} % </b></td>
                                            <?php }elseif($porcenllamada >= 100){ ?>
                                            <td style="text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcenllamada }} % </b></td>
                                            <?php } 
                                            }elseif( $llamada->estadollamada == 'NO INICIADA'){
                                            if($porcenllamada <= '0,00'){ ?>
                                            <td style="text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcenllamada }} % </b></td>
                                            <?php }elseif($porcenllamada >= 0 && $porcenllamada <= 19){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: #000000;"><b> {{ $porcenllamada }} % </b></td>
                                            <?php }elseif($porcenllamada >= 20 ){ ?>
                                            <td style="text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcenllamada }} % </b></td>
                                            <?php } }elseif($llamada->estadollamada == 'POR INICIAR'){ ?>
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ $porcenllamada }} % </b></td>
                                            <?php } ?>
                                        </tr>                                        
                                        @endforeach
                                        <tr>
                                            <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ number_format($TotalLlamada,0,",",".") }}</b></td>
                                            <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                                       </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div>
    <div class="col-lg-3 col-sm-12" style="width: auto">
                            <div class="box table-responsive no-padding">
                                <div class="box-body">
                                     <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 83%;">
                                         <tr style="background-color: #0489B1; color: white;">
                                             <th style="text-align:center;" colspan="3" >{{ $periodo }}</th>
                                         </tr>
                                         <tr style="background-color: #0489B1; color: white;">
                                            <th style="text-align:center;" colspan="3" >NOVEDADES ESTADO DE REUNIÓN</th>
                                        </tr> 
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >PROBLEMA</th>
                                            <th style="text-align: center;">CANTIDAD</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                        </tr>
                                        @foreach( $novedad as $novedad)
                                        <?php
                                        if($ttnovedad == 0){
                                        $porcenovedada = number_format((($novedad->totalnovedad_apli / 1) * 100), '2',',','');
                                        }elseif($ttnovedad != 0 ){
                                        $porcenovedada = number_format((($novedad->totalnovedad_apli / $ttnovedad) * 100), '2',',','') ;
                                        }        
                                        ?>
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;"><b>{{ $novedad->novedadllamada_apli }}</b></td>                                           
                                            <td style="text-align: right;"><b> {{ number_format($novedad->totalnovedad_apli,0,",",".") }} </b></td>
                                            
                                            <?php if($porcenovedada == '0,00' ){ ?>                                            
                                            <td style="text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcenovedada }} % </b></td>
                                            <?php }elseif($porcenovedada >= 0 && $porcenovedada <= 19 ){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: white;"><b> {{ $porcenovedada }} % </b></td>
                                            <?php }elseif($porcenovedada >= 20 ){ ?>
                                            <td style="text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcenovedada }} % </b></td>
                                            <?php } ?>
                                            
                                        </tr>                                        
                                        @endforeach
                                        <tr>
                                            <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ number_format($ttnovedad,0,",",".") }} </b></td>
                                            <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                                       </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div>  

</div>
<div class="row zona">
    <div class="col-lg-3 col-sm-12" style="width: auto;">
                            <div class="box table-responsive no-padding">
                                <div class="box-body">
                                    <table class="table table-bordered table-striped table-hover" style="width: auto; font-size: 83%;">
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >{{ $periodo }}</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >AVANCE DE LLAMADAS DEL APLICADOR</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >ESTADO</th>
                                            <th style="text-align: center;">CANTIDAD</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                        </tr>
                                         @foreach( $reporteLlamadaAvance as $llamada)
                                        <?php 
                                        if( $TotalLlamadaAvance == 0){
                                            $porcenllamada = number_format((($llamada->cantidadllamada / 1) * 100), '2',',','') ;
                                        }else{
                                            $porcenllamada = number_format((($llamada->cantidadllamada / $TotalLlamadaAvance) * 100), '2',',',''); 
                                        }
                                        ?>
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;"><b>{{ $llamada->estadollamada }}</b></td>                                           
                                            <td style="text-align: right;"><b> {{ number_format($llamada->cantidadllamada,0,",",".") }} </b></td> 
                                            
                                            <?php
                                            if( $llamada->estadollamada == 'CONTACTADO'){
                                            if($porcenllamada <= 44){ ?>                                            
                                            <td style="text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcenllamada }} % </b></td>
                                            <?php }elseif($porcenllamada >= 45 && $porcenllamada <= 99){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: #000000;"><b> {{ $porcenllamada }} % </b></td>
                                            <?php }elseif($porcenllamada >= 100){ ?>
                                            <td style="text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcenllamada }} % </b></td>
                                            <?php } 
                                            }elseif( $llamada->estadollamada == 'NO CONTACTADO'){
                                            if($porcenllamada <= '0,00'){ ?>
                                            <td style="text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcenllamada }} % </b></td>
                                            <?php }elseif($porcenllamada >= 0 && $porcenllamada <= 19){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: #000000;"><b> {{ $porcenllamada }} % </b></td>
                                            <?php }elseif($porcenllamada >= 20 ){ ?>
                                            <td style="text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcenllamada }} % </b></td>
                                            
                                            <?php } }elseif($llamada->estadollamada == 'POR CONTACTAR'){ ?> 
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ $porcenllamada }} % </b></td>
                                            <?php } ?>
                                            
                                        </tr>                                        
                                        @endforeach
                                        <tr>
                                            <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ number_format($TotalLlamada,0,",",".") }}</b></td>
                                            <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                                       </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div>
    
                        <div class="col-lg-3 col-sm-12" style="width: auto">
                            <div class="box table-responsive no-padding">
                                    <!-- <div class="box-header with-border" style="padding: 0px;">
                                    <h5>&nbsp;&nbsp;&nbsp;<i class="fa fa-paste"></i> <b> Reporte de Problema de Llamadas</b></h5>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                        <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 83%;">
                                            <tr style="background-color: #0489B1; color: white;"> 
                                                <th style="text-align:center;" colspan="3" >{{ $periodo }}</th> 
                                            </tr>    
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >NOVEDADES DE LLAMADA</th> 
                                        </tr> 
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >PROBLEMA</th>
                                            <th style="text-align: center;">CANTIDAD</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                        </tr>
                                        @foreach( $novedadllamadaAvance as $novedad)
                                        <?php  
                                        
                                        if($ttnovedad == 0){
                                        $porcenovedada = number_format((($novedad->totalnovedad_apli / 1) * 100), '2',',','');
                                        }elseif($ttnovedad != 0 ){
                                        $porcenovedada = number_format((($novedad->totalnovedad_apli / $ttnovedad) * 100), '2',',','') ;
                                        }        
                                        ?>
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;"><b>{{ $novedad->novedadllamada_aplicadores }}</b></td>                                           
                                            <td style="text-align: right;"><b> {{ number_format($novedad->totalnovedad_apli,0,",",".") }} </b></td>
                                            
                                            <?php if($porcenovedada == '0,00' ){ ?>                                            
                                            <td style="text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcenovedada }} % </b></td>
                                            <?php }elseif($porcenovedada >= 0 && $porcenovedada <= 19 ){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: white;"><b> {{ $porcenovedada }} % </b></td>
                                            <?php }elseif($porcenovedada >= 20 ){ ?>
                                            <td style="text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcenovedada }} % </b></td>
                                            <?php } ?>
                                            
                                        </tr>                                        
                                        @endforeach
                                        <tr>
                                            <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ number_format($ttnovedad,0,",",".") }} </b></td>
                                            <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div>           

</div>

<div class="row zona">
    <div  class="col-lg-3 col-sm-12" style="width: auto;">
                            <div class="box table-responsive no-padding">
                                <!-- <div class="box-header with-border">
                                    <h5><i class="fa fa-paste"></i> <b> Reporte de Descarga del Aplicativo</b></h5>
                                </div> /.box-header -->
                                <div class="box-body">
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 83%;">
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >{{ $periodo }}</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >DESCARGA DE EXTENSI&Oacute;N INEVAL</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >ESTADO</th>
                                            <th style="text-align: center;">CANTIDAD</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                        </tr>
                                        @foreach($reportdescgeneralExtension as $generalDesc)
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;">
                                              <b> @if ( $generalDesc->descarga == 'SI') DESCARGÓ @else {{$generalDesc->descarga}} DESCARGÓ @endif </b></td>                                           
                                            <td style="text-align: right;">
                                                <?php if($ss == ''){$ss =0;}else{$ss = $ss;} ?>
                                             @if ( $generalDesc->descarga != 'SI')  
                                                <b> {{ number_format($generalDesc->cantidad,0,",",".") }} </b></a>@else <b> {{ number_format($generalDesc->cantidad,0,",",".") }} </b></td>
                                            @endif
                                            <!--<b> {{ number_format($generalDesc->cantidad,0,",",".") }} </b>-->
                                            <?php 
                                            if($TotalDescExtension == 0){
                                            $porcen = number_format($generalDesc->cantidad / $TotalDescExtension );      
                                            }elseif($TotalDescExtension !=0){
                                            $porcen = number_format((($generalDesc->cantidad / $TotalDescExtension ) * 100), '2',',','');  
                                            }
                                            ?>
                                            <?php if($porcen <= 44 && $generalDesc->descarga == 'SI' ){ ?>                                            
                                            <td style="text-align: right; background-color: #FA5858; color: white;">
                                            <?php }elseif($porcen >= 45 && $porcen <= 99 && $generalDesc->descarga == 'SI'){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: #000000;">
                                            <?php }elseif($porcen >= 100 && $generalDesc->descarga == 'SI'){ ?>
                                            <td style="text-align: right; background-color: #5FB404; color: white;">
                                            <?php }elseif($generalDesc->descarga != 'SI'){ ?>
                                            <td style="text-align: right; background-color: #0489B1; color: white;">
                                            <?php } ?>     
                                                <b> {{ $porcen }} % </b></td>
                                        </tr>   
                                        @endforeach
                                        <tr>
                                            <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ number_format($TotalDescExtension,0,",",".") }}</b></td>
                                            <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                                       </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div>
                        <div class="col-lg-3 col-sm-12" id="noInicio" style="width: auto; ">
                            <div class="box table-responsive no-padding">
                                 <!-- <div class="box-header with-border" style="padding: 0px;">
                                   <h5>&nbsp;&nbsp;&nbsp;<i class="fa fa-paste"></i> <b> Reporte de Problema de Llamadas</b></h5>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                     <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 83%;">
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="4" >NO DESCARGÓ</th> 
                                        </tr> 
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >PROBLEMA</th>
                                            <th style="text-align: center;">CANTIDAD PROBLEMA</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                            <th style="text-align: center;">SUSTENTANTES AFECTADOS</th> 
                                        </tr>
                                        @foreach( $noDescargoExtension as $noDescargo)
                                        <?php  
                                         
                                        if($ttNoDescargoExtension == 0){
                                        $porcenoDescarg = number_format((($noDescargo->totalobservacion / 1) * 100), '2',',','');
                                        }elseif($ttNoDescargoExtension != 0 ){
                                        $porcenoDescarg = number_format((($noDescargo->totalobservacion / $ttNoDescargoExtension) * 100), '2',',','') ;
                                        }        
                                        ?>
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;"><b>{{ $noDescargo->observacion_descarga }}</b></td>                                           
                                            <td style="text-align: right;"><b> {{ number_format($noDescargo->totalobservacion,0,",",".") }} </b></td>
                                            
                                            <?php if($porcenoDescarg == '0,00' ){ ?>                                            
                                            <td style="text-align: right; background-color: #5FB404; color: white;">
                                            <?php }elseif($porcenoDescarg >= 0 && $porcenoDescarg <= 19 ){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: white;">
                                            <?php }elseif($porcenoDescarg >= 20 ){ ?>
                                            <td style="text-align: right; background-color: #FA5858; color: white;">
                                            <?php } ?>
                                                <b> {{ $porcenoDescarg }} % </b></td>
                                            <td style="text-align: right;"><b> {{ number_format($noDescargo->sustentantes_afect,0,",",".") }} </b></td>
                                            
                                        </tr>                                        
                                        @endforeach
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;"><b>TOTAL NO DESCARGÓ</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ number_format($ttNoDescargoExtension,0,",",".") }} </b></td>
                                            <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ number_format($ttDSusAfectadosExtension,0,",",".") }} </b></td>
                                       </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div>                                                
                       
</div>

<div class="row zona">
    <div  class="col-md-3 col-sm-12" style="width: auto;">
                                <div class="box table-responsive no-padding">
                                    <div class="box-body">
                                        <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 83%;">
                                            <tr style="background-color: #0489B1; color: white;"> 
                                                <th style="text-align:center;" colspan="3" >{{ $periodo }}</th> 
                                            </tr>
                                            <tr style="background-color: #0489B1; color: white;"> 
                                                <th style="text-align:center;" colspan="3" >INSTALACIÓN DE EXTENSI&Oacute;N INEVAL</th> 
                                            </tr>
                                            <tr style="background-color: #0489B1; color: white;"> 
                                                <th style="text-align:center;" >ESTADO</th>
                                                <th style="text-align: center;">CANTIDAD</th>
                                                <th style="text-align: center;">% AVANCE</th> 
                                            </tr>
                                            @foreach( $reporteinstaloapliExtension as $generalInst)
                                            <tr>
                                                <td style="text-align: left; background-color: #0489B1; color: white;">
                                                <b> @if ( $generalInst->instalo == 'SI') INSTALÓ @else {{ $generalInst->instalo }} INSTALÓ @endif </b>
                                                </td>                                               
                                                <td style="text-align: right;">
                                                    <?php if($ss == ''){$ss =0;}else{$ss = $ss;} ?>
                                                 @if ( $generalInst->instalo != 'SI') 
                                                    <b> {{ number_format($generalInst->cantidad,0,",",".") }} </b></a>@else <b> {{ number_format($generalInst->cantidad,0,",",".") }} </b></td>
                                                @endif 
                                                <!--<b> {{ number_format($generalInst->cantidad,0,",",".") }} </b>-->
                                                <?php 
                                                if($TotalInstExtension == 0){
                                                $porcen = number_format($generalInst->cantidad / $TotalInstExtension );      
                                                }elseif($TotalInstExtension !=0){
                                                $porcen = number_format((($generalInst->cantidad / $TotalInstExtension ) * 100), '2',',','');  
                                                }
                                                ?>
                                                <?php if($porcen <= 44 && $generalInst->instalo == 'SI'){ ?>                                            
                                                <td style="text-align: right; background-color: #FA5858; color: white;">
                                                <?php }elseif($porcen >= 45 && $porcen <= 99 && $generalInst->instalo == 'SI'){ ?>
                                                <td style="text-align: right; background-color: #D7DF01; color: #000000;">
                                                <?php }elseif($porcen >= 100 && $generalInst->instalo == 'SI'){ ?>
                                                <td style="text-align: right; background-color: #5FB404; color: white;">
                                                <?php }elseif($generalInst->instalo != 'SI'){ ?>
                                                <td style="text-align: right; background-color: #0489B1; color: white;">
                                                <?php } ?>  
                                                    <b> {{ $porcen }} % </b></td>
                                            </tr>   
                                            @endforeach
                                            <tr>
                                                <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>    
                                                <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ number_format($TotalInstExtension,0,",",".") }}</b></td>
                                                <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                                           </tr>
                                        </table>
                                    </div>
                                </div>                                               
                            </div>
                            <div class="col-lg-3 col-sm-12" id="noInicio" style="width: auto; ">
                                <div class="box table-responsive no-padding">
                                    <div class="box-body">
                                         <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 83%;">
                                            <tr style="background-color: #0489B1; color: white;"> 
                                                <th style="text-align:center;" colspan="4" >NO INSTALÓ</th> 
                                            </tr> 
                                            <tr style="background-color: #0489B1; color: white;"> 
                                                <th style="text-align:center;" >PROBLEMA</th>
                                                <th style="text-align: center;">CANTIDAD PROBLEMA</th>
                                                <th style="text-align: center;">% AVANCE</th> 
                                                <th style="text-align: center;">SUSTENTANTES AFECTADOS</th> 
                                            </tr>
                                            @foreach( $noInstaloExtension as $noInstalo)
                                            <?php  
                                             
                                            if($ttNoInstaloExtension == 0){
                                            $porcenoDescarg = number_format((($noInstalo->totalobservacion / 1) * 100), '2',',','');
                                            }elseif($ttNoInstaloExtension != 0 ){
                                            $porcenoDescarg = number_format((($noInstalo->totalobservacion / $ttNoInstaloExtension) * 100), '2',',','') ;
                                            }        
                                            ?>
                                            <tr>
                                                <td style="text-align: left; background-color: #0489B1; color: white;"><b>{{ $noInstalo->observacion_instala }}</b></td>                                           
                                                <td style="text-align: right;"><b> {{ number_format($noInstalo->totalobservacion,0,",",".") }} </b></td>
                                                
                                                <?php if($porcenoDescarg == '0,00' ){ ?>                                            
                                                <td style="text-align: right; background-color: #5FB404; color: white;">
                                                <?php }elseif($porcenoDescarg >= 0 && $porcenoDescarg <= 19 ){ ?>
                                                <td style="text-align: right; background-color: #D7DF01; color: white;">
                                                <?php }elseif($porcenoDescarg >= 20 ){ ?>
                                                <td style="text-align: right; background-color: #FA5858; color: white;">
                                                <?php } ?>
                                                    <b> {{ $porcenoDescarg }} % </b></td>
                                                <td style="text-align: right;"><b> {{ number_format($noInstalo->sustentantes_afect,0,",",".") }} </b></td>
                                                
                                            </tr>                                        
                                            @endforeach
                                            <tr>
                                                <td style="text-align: left; background-color: #0489B1; color: white;"><b>TOTAL NO DESCARGÓ</b></td>    
                                                <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ number_format($ttNoInstaloExtension,0,",",".") }} </b></td>
                                                <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                                                <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ number_format($ttISusAfectadosExtension,0,",",".") }} </b></td>
                                           </tr>
                                        </table>
                                    </div>
                                </div>                                               
                            </div>                                                                            
    
</div>

<div class="row zona">
    <div class="col-sm-12 col-md-3" style="width: auto;">
        <div class="box table-responsive no-padding">
            <div class="box-body">
                <table class="table table-bordered table-striped table-hover" style="width: auto; font-size: 83%;">
                    <tr style="background-color: #0489B1; color: white;"> 
                        <th style="text-align:center;" colspan="3" >{{ $periodo }}</th> 
                    </tr>
                    <tr style="background-color: #0489B1; color: white;"> 
                        <th style="text-align:center;" colspan="3" >INICIO DE SESIÓN POR LABORATORIO</th> 
                    </tr>
                    <tr style="background-color: #0489B1; color: white;"> 
                        <th style="text-align:center;" >ESTADO</th>
                        <th style="text-align: center;">CANTIDAD</th>
                        <th style="text-align: center;">% AVANCE</th> 
                    </tr>
                     @foreach( $reporteinisesionOnline as $sesion)
                    <?php 
                    if( $TotalSesionOnline == 0){
                        $porcensesion = number_format((($sesion->cantidad / 1) * 100), '2',',','') ;
                    }else{
                        $porcensesion = number_format((($sesion->cantidad / $TotalSesionOnline) * 100), '2',',',''); 
                    }
                    ?>
                    <tr>
                        <td style="text-align: left; background-color: #0489B1; color: white;">
                        <b> @if ( $sesion->sesion == 'SI') INICIÓ @else {{ $sesion->sesion }} INICIÓ @endif </b>
                        </td> 
                        <td style="text-align: right;">
                            <?php if($ss == ''){$ss =0;}else{$ss = $ss;} ?>
                          @if ( $sesion->sesion != 'SI') 
                          <b> {{ number_format($sesion->cantidad,0,",",".") }} </b> </a>@else <b> {{ number_format($sesion->cantidad,0,",",".") }} </b>
                          @endif
                        </td>
                        <?php if($porcensesion <= 44 && $sesion->sesion == 'SI'){ ?>                                            
                        <td style="text-align: right; background-color: #FA5858; color: white;">
                        <?php }elseif($porcensesion >= 45 && $porcensesion <= 99 && $sesion->sesion == 'SI'){ ?>
                        <td style="text-align: right; background-color: #D7DF01; color: #000000;">
                        <?php }elseif($porcensesion >= 100 && $sesion->sesion == 'SI'){ ?>
                        <td style="text-align: right; background-color: #5FB404; color: white;">
                        <?php }elseif($sesion->sesion != 'SI'){ ?>
                        <td style="text-align: right; background-color: #0489B1; color: white;">
                        <?php } ?>                  
                           <b> {{ $porcensesion }} % </b></td>
                    </tr>                                        
                    @endforeach
                    <tr>
                        <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>    
                        <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ number_format($TotalSesionOnline,0,",",".") }}</b></td>
                        <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                   </tr>
                </table>
            </div>
        </div>                                               
    </div>
    <div class="col-md-3 col-sm-12" id="noInicio" style="width: auto; ">
        <div class="box table-responsive no-padding">             
            <div class="box-body">
                 <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 83%;">
                    <tr style="background-color: #0489B1; color: white;"> 
                        <th style="text-align:center;" colspan="4" >NO INICIÓ SESIÓN</th> 
                    </tr> 
                    <tr style="background-color: #0489B1; color: white;"> 
                        <th style="text-align:center;" >PROBLEMA</th>
                        <th style="text-align: center;">CANTIDAD PROBLEMA</th>
                        <th style="text-align: center;">% AVANCE</th> 
                        <th style="text-align: center;">SUSTENTANTES AFECTADOS</th> 
                    </tr>
                    @foreach( $noInicioSsesion as $noInicioSsesion)
                    <?php  
                    
                    if($ttNoInicio == 0){
                    $porcenoInicioSs = number_format((($noInicioSsesion->totalobservacion / 1) * 100), '2',',','');
                    }elseif($ttNoInicio != 0 ){
                    $porcenoInicioSs = number_format((($noInicioSsesion->totalobservacion / $ttNoInicio) * 100), '2',',','') ;
                    }        
                    ?>
                    <tr>
                        <td style="text-align: left; background-color: #0489B1; color: white;"><b>{{ $noInicioSsesion->observacion_sesion }}</b></td>                                           
                        <td style="text-align: right;"><b> {{ number_format($noInicioSsesion->totalobservacion,0,",",".") }} </b></td>
                        
                        <?php if($porcenoInicioSs == '0,00' ){ ?>                                            
                        <td style="text-align: right; background-color: #5FB404; color: white;">
                        <?php }elseif($porcenoInicioSs >= 0 && $porcenoInicioSs <= 19 ){ ?>
                        <td style="text-align: right; background-color: #D7DF01; color: white;">
                        <?php }elseif($porcenoInicioSs >= 20 ){ ?>
                        <td style="text-align: right; background-color: #FA5858; color: white;">
                        <?php } ?>
                            <b> {{ $porcenoInicioSs }} % </b></td>
                        <td style="text-align: right;"><b> {{ number_format($noInicioSsesion->sustentantes_afect,0,",",".") }} </b></td>
                        
                    </tr>                                        
                    @endforeach
                    <tr>
                        <td style="text-align: left; background-color: #0489B1; color: white;"><b>TOTAL NO INICIÓ</b></td>    
                        <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ number_format($ttNoInicio,0,",",".") }} </b></td>
                        <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                        <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ number_format($ttSusAfectados,0,",",".") }} </b></td>
                   </tr>
                   @foreach($n_Sustentantes as $n_Sustentantes)
                   <?php 
                    if( $Total_Sustentantes == 0){
                        $porcenTtSust = number_format((($n_Sustentantes->programados / 1) * 100), '2',',','') ;
                    }else{
                        $porcenTtSust = number_format((($n_Sustentantes->programados / $Total_Sustentantes) * 100), '2',',',''); 
                    }
                    ?>
                    @endforeach
                </table>
            </div>
        </div>                                               
    </div>
</div>

<div class="row zona">
    <div  class="col-lg-3 col-sm-12" style="width: auto;">
        <div class="box table-responsive no-padding">
            <!-- <div class="box-header with-border">
                <h5><i class="fa fa-paste"></i> <b> Reporte de Descarga del Aplicativo</b></h5>
            </div> /.box-header -->
            <div class="box-body">
                <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 83%;">
                    <tr style="background-color: #0489B1; color: white;">
                        <th style="text-align:center;" colspan="3" >{{ $periodo }}</th>
                    </tr>
                    <tr style="background-color: #0489B1; color: white;">
                        <th style="text-align:center;" colspan="3" >DESCARGA DE CREDENCIALES</th>
                    </tr>
                    <tr style="background-color: #0489B1; color: white;">
                        <th style="text-align:center;" >ESTADO</th>
                        <th style="text-align: center;">CANTIDAD</th>
                        <th style="text-align: center;">% AVANCE</th>
                    </tr>
                    @foreach($reportdescgeneral as $generalDesc)
                        <tr>
                            <td style="text-align: left; background-color: #0489B1; color: white;">
                                <b> @if ( $generalDesc->descarga == 'SI') DESCARGÓ @else {{$generalDesc->descarga}} DESCARGÓ @endif </b></td>
                            <td style="text-align: right;">
                                <?php if($ss == ''){$ss =0;}else{$ss = $ss;} ?>
                                @if ( $generalDesc->descarga != 'SI')
                                    <b> {{ number_format($generalDesc->cantidad,0,",",".") }} </b>@else <b> {{ number_format($generalDesc->cantidad,0,",",".") }} </b></td>
                        @endif
                        <!--<b> {{ number_format($generalDesc->cantidad,0,",",".") }} </b>-->
                            <?php
                            if($totaldesc == 0){
                                $porcen = number_format($generalDesc->cantidad / $totaldesc );
                            }elseif($totaldesc !=0){
                                $porcen = number_format((($generalDesc->cantidad / $totaldesc ) * 100), '2',',','');
                            }
                            ?>
                            <?php if($porcen <= 44 && $generalDesc->descarga == 'SI' ){ ?>
                            <td style="text-align: right; background-color: #FA5858; color: white;">
                            <?php }elseif($porcen >= 45 && $porcen <= 99 && $generalDesc->descarga == 'SI'){ ?>
                            <td style="text-align: right; background-color: #D7DF01; color: #000000;">
                            <?php }elseif($porcen >= 100 && $generalDesc->descarga == 'SI'){ ?>
                            <td style="text-align: right; background-color: #5FB404; color: white;">
                            <?php }elseif($generalDesc->descarga != 'SI'){ ?>
                            <td style="text-align: right; background-color: #0489B1; color: white;">
                                <?php } ?>
                                <b> {{ $porcen }} % </b></td>
                        </tr>
                    @endforeach
                    <tr>
                        <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>
                        <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ number_format($totaldesc,0,",",".") }}</b></td>
                        <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-3" style="width: auto;">
                            <div class="box table-responsive no-padding">
                                <div class="box-body">
                                    <table class="table table-bordered table-striped table-hover" style="width: auto; font-size: 83%;">
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >{{ $periodo }}</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >INICIO DE SESIÓN POR SUSTENTANTE</th>
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >ESTADO</th>
                                            <th style="text-align: center;">CANTIDAD</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                        </tr>
                                         @foreach( $reporteinisesion as $sesion)
                                        <?php 
                                        if( $TotalSesion == 0){
                                            $porcensesion = number_format((($sesion->cantidad / 1) * 100), '2',',','') ;
                                        }else{
                                            $porcensesion = number_format((($sesion->cantidad / $TotalSesion) * 100), '2',',',''); 
                                        }
                                        ?>
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;">
                                            <b> @if ( $sesion->sesion == 'SI') INICIÓ @else {{ $sesion->sesion }} INICIÓ @endif </b>
                                            </td> 
                                            <td style="text-align: right;">
                                                <?php if($ss == ''){$ss =0;}else{$ss = $ss;} ?>
                                              @if ( $sesion->sesion != 'SI') 
                                              <b> {{ number_format($sesion->cantidad,0,",",".") }} </b> @else <b> {{ number_format($sesion->cantidad,0,",",".") }} </b>
                                              @endif
                                            </td>
                                            <?php if($porcensesion <= 44 && $sesion->sesion == 'SI'){ ?>                                            
                                            <td style="text-align: right; background-color: #FA5858; color: white;">
                                            <?php }elseif($porcensesion >= 45 && $porcensesion <= 99 && $sesion->sesion == 'SI'){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: #000000;">
                                            <?php }elseif($porcensesion >= 100 && $sesion->sesion == 'SI'){ ?>
                                            <td style="text-align: right; background-color: #5FB404; color: white;">
                                            <?php }elseif($sesion->sesion != 'SI'){ ?>
                                            <td style="text-align: right; background-color: #0489B1; color: white;">
                                            <?php } ?>                  
                                               <b> {{ $porcensesion }} % </b></td>
                                        </tr>                                        
                                        @endforeach
                                        <tr>
                                            <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ number_format($TotalSesion,0,",",".") }}</b></td>
                                            <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                                       </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div>
</div>
 
<div class="row zona" style="display: block;">
    <div class="col-sm-12 col-md-10" style="width: auto;">
        <div class="box table-responsive no-padding " style="width: auto;">
            <div class="box-body">
                {!! Form::open(['method' => 'POST', 'id' => 'formulario' ]) !!}
                <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 90%;">
                    <tr style="background-color: #0489B1; color: white;">
                        <h4> <th style="text-align: center; vertical-align: middle;" colspan="12"><b>REPORTE DE ZONAS POR LABORATORIO</b></th></h4>
                    </tr>
                    <tr style="background-color: #0489B1; color: white;">
                        <th style="width: 20px; text-align:center;" >ZONA  </th>
                        <th style="text-align:center;  vertical-align: middle; padding: 3px;">N° SEDES</th>
                        <th style="text-align:center;  vertical-align: middle; padding: 3px;" colspan="2">INICIO DE REUNIÓN</th>
                        <th style="text-align:center;  vertical-align: middle; padding: 3px" colspan="2">INICIO DE SESIÓN</th>
                    </tr>
                    <tr style="background-color: #0489B1; color: white;">
                        <td style="text-align: center; vertical-align: middle; padding: 3px;"> </td>
                        <td style="text-align: center; vertical-align: middle; padding: 3px;">  </td>
                        <td style="text-align: center; vertical-align: middle; padding: 3px;"> SI </td>
                        <td style="text-align: center; vertical-align: middle; padding: 3px;"> NO </td>
                        <td style="text-align: center; vertical-align: middle; padding: 3px;"> SÍ </td>
                        <td style="text-align: center; vertical-align: middle; padding: 3px;"> NO </td>
                    </tr>
                    @foreach( $monitores as $monitor )
                        <?php
                        if( $monitor->laboratorio == 0){

                            $porcesesion = number_format((($monitor->sesionsi / 1) * 100), '2',',','');
                            $porcecontactado = number_format((($monitor->contactado / 1) * 100), '2',',','');
                            $porcenocontactado = number_format((($monitor->nocontactado / 1) * 100), '2',',','');
                            $porcenovedad = number_format((($monitor->novedadllamada / 1) * 100), '2',',','');
                        }else{

                            $porcesesion = number_format((($monitor->sesionsi / $monitor->laboratorio) * 100), '2',',','');
                            $porcecontactado = number_format((($monitor->contactado / $monitor->laboratorio) * 100), '2',',','');
                            $porcenocontactado = number_format((($monitor->nocontactado / $monitor->laboratorio) * 100), '2',',','');
                            $porcenovedad = number_format((($monitor->novedadllamada / $monitor->laboratorio) * 100), '2',',','');
                        }
                        ?>
                        <tr>
                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> {{ $monitor->zona }}</td>
                            <td style="text-align: right; vertical-align: middle; padding: 3px;"><b>{{ number_format($monitor->laboratorio,0,",",".") }}</b></td>
                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                <div class="progress">
                                    <?php if($porcecontactado <= 44){ ?>
                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">
                                        <?php }elseif($porcecontactado >= 45 && $porcecontactado <=99) {?>
                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01; color: #000000;">
                                            <?php }elseif($porcecontactado >= 100) {?>
                                            <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">
                                                <?php } ?>
                                                <span class="skill"><i class="val"><b>( {{ number_format($monitor->contactado,0,",",".") }} ) {{ $porcecontactado }}%</b></i></span>
                                            </div>
                                        </div>
                            </td>
                            <td style="text-align: right; vertical-align: middle; padding: 8px;"><b>{{ number_format($monitor->nocontactado,0,",",".") }}</b></td>
                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                <div class="progress">
                                    <?php if($porcesesion <=44){ ?>
                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">
                                        <?php }elseif($porcesesion >=45 && $porcesesion <=99 ) {?>
                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01; color: #000000;">
                                            <?php }elseif($porcesesion >=100 ) {?>
                                            <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">
                                                <?php } ?>
                                                <span class="skill"><i class="val"><b>( {{ number_format($monitor->sesionsi,0,",",".") }} ) {{ $porcesesion.'%' }}</b></i></span>
                                            </div>
                                        </div>
                            </td>
                            <td style="text-align: right; vertical-align: middle; padding: 8px;"><b>{{ number_format($monitor->sesionno,0,",",".") }}</b></td>
                        </tr>
                    @endforeach
                    <tr><input type="hidden" value="{{ $coordinador }}" name="coordinador">
                        <td style="background-color: #0489B1; color: white;"><b>TOTALES</b></td>
                        <?php
                        if($TotalLab == 0){
                            $porcesesio = ($TotalSesionsi / 1) ;
                            $porcecontac = ($TotalContactado / 1) ;
                            $porcenocontac = ($TotalNoContactado / 1) ;                                                        
                        }else{
                            $porcesesio = number_format((($TotalSesionsi / $TotalLab) * 100), '2',',','');
                            $porcecontac = number_format((($TotalContactado / $TotalLab) * 100), '2',',','');
                            $porcenocontac = number_format((($TotalNoContactado / $TotalLab) * 100), '2',',','');                                                        
                        }
                        ?>
                        <td align="right" style="background-color: #0489B1; color: white;"><b>{{ number_format($TotalLab,0,",",".") }}</b></td>
                        <?php if( $TotalContactado ==0){?>
                        <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ number_format($TotalContactado,0,",",".") }} ) {{ $porcecontac }}% &nbsp;&nbsp;
                        <?php }elseif( $TotalContactado != $TotalLab){?>
                        <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ number_format($TotalContactado,0,",",".") }} ) {{ $porcecontac }}% &nbsp;&nbsp;
                        <?php }elseif( $TotalContactado == $TotalLab){?>
                        <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ number_format($TotalContactado,0,",",".") }} ) {{ $porcecontac }}% &nbsp;&nbsp;
                        <?php } ?>
                        <?php if( $TotalNoContactado ==0 && $porcecontac == '100,00'){?>
                        <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ number_format($TotalNoContactado,0,",",".") }} ) {{ $porcenocontac }}% &nbsp;&nbsp;
                        <?php }elseif( $TotalNoContactado > 0 && $porcecontac != '100,00'){?>
                        <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ number_format($TotalNoContactado,0,",",".") }} ) {{ $porcenocontac }}% &nbsp;&nbsp;
                        <?php }elseif( $TotalNoContactado == 0 && $porcecontac != '100,00'){?>
                        <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ number_format($TotalNoContactado,0,",",".") }} ) {{ $porcenocontac }}% &nbsp;&nbsp;
                        <?php } ?>
                        <?php if( $TotalSesionsi==0 ){?>
                        <td align="right" style="background-color: #0489B1; color: white;"><b>{{ number_format($TotalSesionsi,0,",",".") }} &nbsp;&nbsp;
                        <?php }elseif( $TotalSesionsi != $TotalLab){?>
                        <td align="right" style="background-color: #0489B1; color: white;"><b>{{ number_format($TotalSesionsi,0,",",".") }} &nbsp;&nbsp;
                        <?php }elseif( $TotalSesionsi == $TotalLab){?>
                        <td align="right" style="background-color: #0489B1; color: white;"><b>{{ number_format($TotalSesionsi,0,",",".") }} &nbsp;&nbsp;
                                <?php } ?>
                                ({{ $porcesesio }}) %</b></td>
                        <td align="right" style="background-color: #0489B1; color: white;"><b>{{ number_format($TotalSesionno,0,",",".") }} </b></td>
                    </tr>
                    <tr>
                        <td colspan="12">
                        <!-- {!! Form::submit('Para consulta de Distrito por Zona hacer Click aqui', ['class' => 'btn btn-block btn-primary', 'onclick' => 'alert(hola);']) !!} -->
                            <button class="btn btn-block btn-info" type="submit" id="enviar_zona" style="display: none;" ><b>Para consulta de Zona por Provincia hacer Click aqui</b></button>
                        </td>
                    </tr>
                </table>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>