<script>
$("#checkZona").change(function(){
    @foreach( $reportecarga as $cargazona )
   $("#zona{{ $cargazona->zona }}").prop('checked', $(this).prop("checked"));   
   if($("#zona{{ $cargazona->zona }}").prop('checked')=== true ){
       $("#enviar_zona").css("display","block");
   }else{
       $("#enviar_zona").css("display","none");
   }
    @endforeach;
});      

function select_zona(){
    var i = 0;
    @foreach( $reportecarga as $selectz )
    var selectZona = $("#zona{{ $selectz->zona }}").prop('checked');     
    if (selectZona){
         i = i + 1;
       $('#enviar_zona').css("display", "block");
    }else if (i == 0 ){
       $('#enviar_zona').css("display", "none"); 
    }  
    @endforeach;
};
    
$(function(){
$("#enviar_zona").click(function(){        
    var id_periodo = $("#periodo").val();
    var fecha = $("#fecha_programada").val();
    var sesion = $("#sesion").val();
    var tp_reporte = $("#tp_reporte").val();    
    if (id_periodo === "" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "0") {
                //alert("Debe seleccionar un periodo");
                            notif({
                            msg: 'Debe Seleccionar un Periodo !',
                            type: 'warning',
                            opacity: 1,
                            });
            } else {
                $(".zona").css("display","none");
    $(".provincia").css("display","block");
    document.getElementById("loading").style.display="block";
        if (id_periodo !== "" && fecha === "" && sesion === "") {
                    var ur = "provincia_aplicacion/" + tp_reporte + "/" + id_periodo + "";
                } else {
                    if (id_periodo !== "" && fecha !== "" && sesion === "") {
                        var ur = "provincia_aplicacion/" + tp_reporte + "/" + id_periodo + "/" + fecha + "";
                    } else {
                        var ur = "provincia_aplicacion/" + tp_reporte + "/" + id_periodo + "/" + fecha + "/" + sesion + "";
                    }
                }
                 var url = ur;
                $.ajax({
                type: "POST",
                url: url,
                data: $("#formulario").serialize(),
                success: function(data)
                {
                $("#provincia").html(data);
                document.getElementById("loading").style.display="none";
                }
                }); 
         }
   return false;
   //alert(data);
});

});
</script>
<div class="zona">
        <ol class="breadcrumb">
          <li><a href="#">Zonas</a> /</li>
        </ol>
</div>

<div class="row zona">
    <div class="col-lg-3 col-sm-12" style="width: auto">
        <div class="box table-responsive no-padding">
            <div class="box-body">
                <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 100%;">
                    <tr style="background-color: #0489B1; color: white;"> 
                        <th style="text-align:center;" colspan="3" >{{ $periodo }}</th> 
                    </tr>
                    <tr style="background-color: #0489B1; color: white;"> 
                        <th style="text-align:center;" colspan="3" >REGISTRO DE ASISTENCIA</th> 
                    </tr>
                    <tr style="background-color: #0489B1; color: white;"> 
                        <th style="text-align:center;" >ESTADO</th>
                        <th style="text-align: center;">CANTIDAD</th>
                        <th style="text-align: center;">% AVANCE</th> 
                    </tr>
                        <?php 
                        if($totalpro ==0){
                        $porcenttAsist= number_format((($totalasis / 1)*100),'2',',','');
                        $porcenttAusest= number_format((($TotalAusent / 1)*100),'2',',','');
                        }else{
                        $porcenttAsist= number_format((($totalasis / $totalpro)*100),'2',',','');
                        $porcenttAusest= number_format((($TotalAusent / $totalpro)*100),'2',',','');
                        }
                        ?>   
                    <tr>
                        <td style="text-align: left; background-color: #0489B1; color: white;"><b>PRESENTES</b></td>                                           
                        <td align="right"><b>{{ number_format($totalasis,0,",",".") }}  </b></td>  
                        <?php if( $porcenttAsist <=44){?>
                        <td align="right" style="background-color: #FA5858; color: white;">
                        <?php }elseif( $porcenttAsist >=45 && $porcenttAsist <=99){?>
                        <td align="right" style="background-color: #D7DF01; color: white;">
                        <?php }elseif( $porcenttAsist >=100){?>
                        <td align="right" style="background-color: #5FB404; color: white;">        
                        <?php } ?><b>{{ $porcenttAsist }}% </b></td>
                    </tr>
                    <tr>
                        <td style="text-align: left; background-color: #0489B1; color: white;"><b>AUSENTES</b></td>                                           
                        <td align="right" ><b>{{ number_format($TotalAusent,0,",",".") }}  </b></td>
                        <?php if( $porcenttAusest <='0,00'){?>
                        <td align="right" style="background-color: #0489B1; color: white;">
                        <?php }elseif( $porcenttAusest >=0 && $porcenttAusest <=19){?>
                        <td align="right" style="background-color: #0489B1; color: white;">
                        <?php }elseif( $porcenttAusest >20){?>
                        <td align="right" style="background-color: #0489B1; color: white;">        
                        <?php } ?><b>{{ $porcenttAusest }}% </b></td>                                         
                    </tr>   
                    <tr>
                        <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTAL</b></td>    
                        <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ number_format($totalpro,0,",",".") }}</b></td>
                        <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100,00 % </b></td>
                    </tr>
                </table>
            </div>
        </div>                                               
    </div> 
    <div class="col-sm-12 col-md-3" style="width: auto;">
        <div class="box table-responsive no-padding">
            <div class="box-body">
                <table class="table table-bordered table-striped table-hover" style="width: auto; font-size: 83%;">
                    <tr style="background-color: #0489B1; color: white;"> 
                        <th style="text-align:center;" colspan="3" >{{ $periodo }}</th> 
                    </tr>
                    <tr style="background-color: #0489B1; color: white;"> 
                        <th style="text-align:center;" colspan="3" >INICIO DE SESIÓN</th>
                    </tr>
                    <tr style="background-color: #0489B1; color: white;"> 
                        <th style="text-align:center;" >ESTADO</th>
                        <th style="text-align: center;">CANTIDAD</th>
                        <th style="text-align: center;">% AVANCE</th> 
                    </tr>
                     @foreach( $reporteinisesion as $sesion)
                    <?php 
                    if( $TotalSesion == 0){
                        $porcensesion = number_format((($sesion->cantidad / 1) * 100), '2',',','') ;
                    }else{
                        $porcensesion = number_format((($sesion->cantidad / $TotalSesion) * 100), '2',',',''); 
                    }
                    ?>
                    <tr>
                        <td style="text-align: left; background-color: #0489B1; color: white;">
                        <b> @if ( $sesion->sesion == 'SI') INICIÓ @else {{ $sesion->sesion }} INICIÓ @endif </b>
                        </td> 
                        <td style="text-align: right;">
                          @if ( $sesion->sesion != 'SI') 
                          <b> {{ number_format($sesion->cantidad,0,",",".") }} </b> @else <b> {{ number_format($sesion->cantidad,0,",",".") }} </b>
                          @endif
                        </td>
                        <?php if($porcensesion <= 44 && $sesion->sesion == 'SI'){ ?>                                            
                        <td style="text-align: right; background-color: #FA5858; color: white;">
                        <?php }elseif($porcensesion >= 45 && $porcensesion <= 99 && $sesion->sesion == 'SI'){ ?>
                        <td style="text-align: right; background-color: #D7DF01; color: #000000;">
                        <?php }elseif($porcensesion >= 100 && $sesion->sesion == 'SI'){ ?>
                        <td style="text-align: right; background-color: #5FB404; color: white;">
                        <?php }elseif($sesion->sesion != 'SI'){ ?>
                        <td style="text-align: right; background-color: #0489B1; color: white;">
                        <?php } ?>                  
                           <b> {{ $porcensesion }} % </b></td>
                    </tr>                                        
                    @endforeach
                    <tr>
                        <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>    
                        <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ number_format($TotalSesion,0,",",".") }}</b></td>
                        <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                   </tr>
                </table>
            </div>
        </div>                                               
    </div>  
    <div class="col-sm-12 col-md-3" style="width: auto;">
        <div class="box table-responsive no-padding">
            <div class="box-body">
                <table class="table table-bordered table-striped table-hover" style="width: auto; font-size: 83%;">
                    <tr style="background-color: #0489B1; color: white;"> 
                        <th style="text-align:center;" colspan="3" >{{ $periodo }}</th> 
                    </tr>
                    <tr style="background-color: #0489B1; color: white;"> 
                        <th style="text-align:center;" colspan="3" >SUSTENTANTES PROGRAMADOS</th>
                    </tr>
                    <tr style="background-color: #0489B1; color: white;"> 
                        <th style="text-align:center;" >ESTADO</th>
                        <th style="text-align: center;">CANTIDAD</th>
                        <th style="text-align: center;">% AVANCE</th> 
                    </tr>   
                    @if($reporteestadoevaluacion->inicio_sesion_c > 0)                                      
                    <tr>
                        <td style="text-align: left; background-color: #0489B1; color: white;">
                        <b>Finalizado </b>
                        </td> 
                        <td style="text-align: right;">                          
                          <b> {{$reporteestadoevaluacion->finalizado_c}}</b>                          
                        </td>
                        <?php if($reporteestadoevaluacion->finalizado_pi <= 44){ ?>                                            
                        <td style="text-align: right; background-color: #FA5858; color: white;">
                        <?php }elseif($reporteestadoevaluacion->finalizado_pi >= 45 && $reporteestadoevaluacion->finalizado_pi <= 99 ){ ?>
                        <td style="text-align: right; background-color: #D7DF01; color: #000000;">
                        <?php }elseif($reporteestadoevaluacion->finalizado_pi >= 100 ){ ?>
                        <td style="text-align: right; background-color: #5FB404; color: white;">                       
                        <?php } ?>                  
                           <b> {{$reporteestadoevaluacion->finalizado_pi}} % </b></td>
                    </tr>  
                    <tr>
                        <td style="text-align: left; background-color: #0489B1; color: white;">
                        <b>En curso </b>
                        </td> 
                        <td style="text-align: right;">                          
                          <b> {{$reporteestadoevaluacion->en_curso_c}}</b>                          
                        </td>
                        <?php if($reporteestadoevaluacion->en_curso_pi <= 44){ ?>                                            
                        <td style="text-align: right; background-color: #FA5858; color: white;">
                        <?php }elseif($reporteestadoevaluacion->en_curso_pi >= 45 && $reporteestadoevaluacion->en_curso_pi <= 99 ){ ?>
                        <td style="text-align: right; background-color: #D7DF01; color: #000000;">
                        <?php }elseif($reporteestadoevaluacion->en_curso_pi >= 100 ){ ?>
                        <td style="text-align: right; background-color: #5FB404; color: white;">                       
                        <?php } ?>                  
                           <b>                   
                            {{number_format(($reporteestadoevaluacion->en_curso_pi ), '2',',','')}} % </b></td>
                    </tr>     
                    <tr>
                        <td style="text-align: left; background-color: #0489B1; color: white;">
                        <b>Suspendido por sistema </b>
                        </td> 
                        <td style="text-align: right;">                          
                          <b> {{$reporteestadoevaluacion->suspendido_c}}</b>                          
                        </td>
                        <?php if($reporteestadoevaluacion->suspendido_pi <= 44){ ?>                                            
                        <td style="text-align: right; background-color: #D7DF01; color: #000000;">
                        <?php }elseif($reporteestadoevaluacion->suspendido_pi >= 45 && $reporteestadoevaluacion->suspendido_pi <= 100 ){ ?>
                        <td style="text-align: right; background-color: #FA5858; color: white;">
                        <?php }elseif($reporteestadoevaluacion->suspendido_pi = 0 ){ ?>
                        <td style="text-align: right; background-color: #5FB404; color: white;">                       
                        <?php } ?>                  
                           <b> {{$reporteestadoevaluacion->suspendido_pi}} % </b></td>
                    </tr>  
                    <tr>
                        <td style="text-align: left; background-color: #0489B1; color: white;">
                        <b>Reprogramados </b>
                        </td> 
                        <td style="text-align: right;">                          
                          <b> {{$reporteestadoevaluacion->reprogra_c}}</b>                          
                        </td>
                        <?php if($reporteestadoevaluacion->reprogra_pi <= 44){ ?>                                            
                        <td style="text-align: right; background-color: #D7DF01; color: #000000;">
                        <?php }elseif($reporteestadoevaluacion->reprogra_pi >= 45 && $reporteestadoevaluacion->reprogra_pi <= 100 ){ ?>
                        <td style="text-align: right; background-color: #FA5858; color: white;">
                        <?php }elseif($reporteestadoevaluacion->reprogra_pi = 0 ){ ?>
                        <td style="text-align: right; background-color: #5FB404; color: white;">                       
                        <?php } ?>                  
                           <b> {{$reporteestadoevaluacion->reprogra_pi}} % </b></td>
                    </tr> 
                    @endif                                                                                                         
                    <tr>
                        <td style="text-align: rigth; background-color: #0489B1; color: white;"><b>Iniciaron sesión</b></td>    
                        <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ $reporteestadoevaluacion->inicio_sesion_c > 0 ? $reporteestadoevaluacion->inicio_sesion_c : 0}}</b></td>
                        <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                   </tr>
                </table>
            </div>
        </div>                                               
    </div> 
</div>