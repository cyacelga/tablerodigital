<script>
$("#checkLaboratorio").change(function(){
    @foreach( $labcargMonitor as $laboratorio )
   $("#laboratorio{{ $laboratorio->laboratorio }}").prop('checked', $(this).prop("checked"));   
   
   if($("#laboratorio{{ $laboratorio->laboratorio }}").prop('checked')=== true ){
       $("#enviar_laboratorio").css("display","block");
   }else{
       $("#enviar_laboratorio").css("display","none");
   }
    @endforeach;
});  

function select_laboratorio(){
    var i = 0;
    @foreach( $labcargMonitor as $selectl )
    var selectLab = $("#laboratorio{{ $selectl->laboratorio }}").prop('checked');     
    if (selectLab){
         i = i + 1;
       $('#enviar_laboratorio').css("display", "block");
    }else if (i == 0 ){
       $('#enviar_laboratorio').css("display", "none"); 
    }  
    @endforeach;
};

$(function(){
$("#enviar_laboratorio").click(function(){        
    var id_periodo = $("#periodo").val();
    var fecha = $("#fecha_programada").val();
    var sesion = $("#sesion").val();
    var tp_reporte = $("#tp_reporte").val();
    
   if (id_periodo === "" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "0") {
                //alert("Debe seleccionar un periodo");
                            notif({
                            msg: 'Debe Seleccionar un Periodo !',
                            type: 'warning',
                            opacity: 1,
                            });
            } else {
                $(".labmonitores").css("display", "none");
                    $(".sustentantes").css("display", "block");
                    document.getElementById("loading").style.display = "block";
        if (id_periodo !== "" && fecha === "" && sesion === "") {
                    var ur = "sustentante_aplicacion/" + tp_reporte + "/" + id_periodo + "";
                } else {
                    if (id_periodo !== "" && fecha !== "" && sesion === "") {
                        var ur = "sustentante_aplicacion/" + tp_reporte + "/" + id_periodo + "/" + fecha + "";
                    } else {
                        var ur = "sustentante_aplicacion/" + tp_reporte + "/" + id_periodo + "/" + fecha + "/" + sesion + "";
                    }
                }
                 var url = ur;
                $.ajax({
                type: "POST",
                url: url,
                data: $("#formulariol").serialize(),
                success: function(data)
                {
                $("#sustentante").html(data);
                document.getElementById("loading").style.display="none";
                }
                }); 
         }
   return false;
   //alert(data);
});

}); 

$(function(){
    $(".azona").click(function(){
    $(".zona").css("display", "block");
    $(".provincia").css("display", "none");
    $(".distritod").css("display", "none");
    $(".monitord").css("display", "none");
    $(".labmonitores").css("display","none"); 
    });
    });
    
$(function(){
    $(".aprovincia").click(function(){
    $(".provincia").css("display", "block");
    $(".zona").css("display", "none");
    $(".distritod").css("display", "none");
    $(".monitord").css("display", "none");          
    $(".labmonitores").css("display","none"); 
    });
});

$(function(){
    $(".adistrito").click(function(){
    $(".provincia").css("display", "none");
    $(".zona").css("display", "none");
    $(".distritod").css("display", "block");
    $(".monitord").css("display", "none");          
    $(".labmonitores").css("display","none"); 
    });
});

$(function(){
    $(".amonitor").click(function(){
    $(".provincia").css("display", "none");
    $(".zona").css("display", "none");
    $(".distritod").css("display", "none");
    $(".monitord").css("display", "block");          
    $(".labmonitores").css("display","none"); 
    });
});
</script>

    <ol class="breadcrumb">
        <li><a href="#" class="azona">Zonas</a></li>
        <li><a href="#" class="aprovincia">Provincias</a></li>
        <li><a href="#" class="adistrito">Distritos</a></li>
        <li><a href="#" class="amonitor">Monitores</a></li>
        <li class="active">Laboratorios</li>
    </ol>

<div class="box table-responsive no-padding " style="width: auto;">
                                <div class="box-body">
                                    {!! Form::open(['method' => 'POST', 'id' => 'formulariol' ]) !!}
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 90%;">
                                        <tr style="background-color: #0489B1; color: white;">  
                                        <h4> <th style="text-align: center; vertical-align: middle;" colspan="18"><b>REPORTE DE LABORATORIO POR MONITOR</b></th></h4>
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;">  
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">Nº</th>
                                            <th style="text-align:center;  vertical-align: middle; padding: 3px;"> 
                                                &nbsp;&nbsp; <input type="checkbox" id="checkLaboratorio" hidden> LABORATORIO
                                            </th>
                                            <th style="text-align:center;  vertical-align: middle; padding: 3px;">AMIE</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">MONITOR</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">DISTRITO</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">PROVINCIA</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">COORDINADOR</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">ZONA</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">INSTITUCION</th>                                            
                                            <th style="text-align:center;  vertical-align: middle; padding: 3px;">PROGRAMADOS</th>
                                            <th style="text-align:center;  vertical-align: middle; padding: 3px;">ASISTENCIA</th>
                                            <th style="text-align:center;  vertical-align: middle; padding: 3px;">% ASISTENCIA</th>
                                            <th style="text-align:center;  vertical-align: middle; padding: 3px">AUSENTES</th>
                                            <th style="text-align:center;  vertical-align: middle; padding: 3px">% AUSENTES</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">CARGAS</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;">% CARGAS</th>
                                            <th style="text-align: center; vertical-align: middle; padding: 3px;">POR CARGAR <br>(ASISTENCIA)</th>
                                            <th style="text-align: center; vertical-align: middle; padding: 3px;">% POR CARGAR</th>
                                        </tr>                                   
                                        <?php $i=0; 
                                        ?>
                                        @foreach( $labcargMonitor as $labCarg)  
                                        <?php 
                                        $i= $i + 1;
                                        $Ausent =($labCarg->programados - $labCarg->asistencia);
                                        $porceAsis = number_format((($labCarg->asistencia / $labCarg->programados)* 100),'2',',','');
                                        $porceAuse = number_format((($Ausent / $labCarg->programados)* 100),'2',',','');
                                        $porceCargas = number_format((($labCarg->cargas / $labCarg->programados)* 100),'2',',','');
                                        $por_cargar= ($labCarg->asistencia - $labCarg->cargas) ;
                                        if($labCarg->asistencia == 0){
                                            $porceporcargar = number_format((($por_cargar / 1) * 100), '2',',','');     
                                            } else {                                            
                                            $porceporcargar = number_format((($por_cargar / $labCarg->asistencia) * 100), '2',',','');
                                            }
                                        ?>
                                            <tr>                                            
                                            <td style="text-align: center; vertical-align: middle; padding: 0px;"> <b> {{ $i }} </b> </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 0px;">
                                                <input type="checkbox" id="laboratorio{{ $labCarg->laboratorio }}" value="{{ $labCarg->laboratorio }}" name="laboratorio[]" onclick="select_laboratorio();" hidden>
                                                {{ $labCarg->sede_laboratorio }}
                                            </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 0px;">{{ $labCarg->amie }}</td>
                                            <td style="text-align: left; vertical-align: middle; padding: 3px;">{{ $labCarg->name_monitor }}</td>
                                            <td style="text-align: left; vertical-align: middle; padding: 3px;">{{ $labCarg->distrito_id }}</td>
                                            <td style="text-align: left; vertical-align: middle; padding: 3px;">{{ $labCarg->provincia }}</td>
                                            <td style="text-align: left; vertical-align: middle; padding: 3px;">{{ $labCarg->coordinador }}</td>
                                            <td style="text-align: center; vertical-align: middle; padding: 0px;">{{ $labCarg->zona }}</td>
                                            <td style="text-align: left; vertical-align: middle; padding: 0px;">&nbsp; {{ $labCarg->institucion }} &nbsp;</td>
                                            <td style="text-align: right; vertical-align: middle; padding: 0px;">{{ $labCarg->programados }}</td>
                                            <td style="text-align: right; vertical-align: middle; padding: 0px;">{{ $labCarg->asistencia }}</td>
                                            <td align="right" style="vertical-align: middle; padding: 0px;">
                                            <div class="progress">
                                                    <?php if($porceAsis <=30){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">     
                                                    <?php }elseif($porceAsis >=31 && $porceAsis <=79) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($porceAsis >= 80) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>{{ $porceAsis.'%' }}</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>
                                            
                                            <td style="text-align: right; vertical-align: middle; padding: 0px;">{{ $Ausent }}</td>
                                            <td align="right" style="vertical-align: middle; padding: 0px;">
                                             <div class="progress">
                                                    <?php if($porceAuse <=30 ){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                    <?php }elseif($porceAuse >=31 && $porceAuse <=79) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($porceAuse >= 80) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>{{ $porceAuse.'%' }}</b></i></span>                                                        
                                                    </div> 
                                                </div>   
                                            </td>
                                            
                                            <td style="text-align: right; vertical-align: middle; padding: 0px;">{{ $labCarg->cargas }}</td>
                                            <td align="right" style="vertical-align: middle; padding: 0px;">
                                            <div class="progress">
                                                    <?php if($porceCargas <=30){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">     
                                                    <?php }elseif($porceCargas >=31 && $porceCargas <=79) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($porceCargas >= 80) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>{{ $porceCargas.'%' }}</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>  
                                            <td align="right" style="padding: 0px;"><b>{{ $por_cargar }}</b></td>
                                            <td align="right" style="padding: 0px;">
                                             <div class="progress">
                                                    <?php if($porceporcargar <='0,00' ){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                    <?php }elseif($porceporcargar >=0 && $porceporcargar <=19) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01; color: #000000">                                                        
                                                    <?php }elseif($porceporcargar >= 20) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>{{ $porceporcargar.'%' }}</b></i></span>                                                        
                                                    </div> 
                                                </div>   
                                            </td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <?php 
                                            if($totalpro ==0){
                                                $porcenttAsis= number_format((($totalasis / 1)*100),'2',',','');
                                            $porcenttAuses= number_format((($TotalAusent / 1)*100),'2',',','');
                                            }else{
                                            $porcenttAsis= number_format((($totalasis / $totalpro)*100),'2',',','');
                                            $porcenttAuses= number_format((($TotalAusent / $totalpro)*100),'2',',','');
                                            if ($totalasis == 0){
                                            $porcenttPorCargar= number_format((($TotalPorCargar / 1)*100),'2',',','');    
                                            }else{
                                            $porcenttPorCargar= number_format((($TotalPorCargar / $totalasis)*100),'2',',','');
                                                }                                           
                                            }
                                            ?>
                                        <td style="background-color: #0489B1; color: white;" colspan="9"><b>TOTALES</b></td>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $totalpro }}</b></td>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $totalasis }}  </b></td>  
                                            <?php if( $porcenttAsis <=75){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">
                                            <?php }elseif( $porcenttAsis >75 && $porcenttAsis <=99){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">
                                            <?php }elseif( $porcenttAsis >=100){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">        
                                            <?php } ?><b>{{ $porcenttAsis }}% </b></td>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalAusent }}  </b></td>
                                            <?php if( $porcenttAuses ==0){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">
                                            <?php }elseif( $porcenttAuses >0 && $porcenttAuses <=25){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">
                                            <?php }elseif( $porcenttAuses >25){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">        
                                            <?php } ?><b>{{ $porcenttAuses }}% </b></td>                                                          
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $totalcargas }} </b></td>                                            
                                            <?php if( $porcentajecar <=75){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">
                                            <?php }elseif( $porcentajecar >75 && $porcentajecar <=99){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">
                                            <?php }elseif( $porcentajecar >=100){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">        
                                            <?php } ?><b>{{ $porcentajecar }}% </b></td>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalPorCargar }}  </b></td>
                                            <?php if( $porcenttPorCargar ==0){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">
                                            <?php }elseif( $porcenttPorCargar >0 && $porcenttPorCargar <=25){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">
                                            <?php }elseif( $porcenttPorCargar >25){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">        
                                            <?php } ?><b>{{ $porcenttPorCargar }}% </b></td>
                                        </tr><tr>
                                           <td colspan="18"> 
                                              <!-- {!! Form::submit('Para consulta de Distrito por monitor hacer Click aqui', ['class' => 'btn btn-block btn-primary', 'onclick' => 'alert(hola);']) !!} -->
                                              <button class="btn btn-block btn-info" type="submit" id="enviar_laboratorio" style="display: none;"><b>Para consulta de Sustentantes por Laboratorio hacer Click aqui</b></button>
                                           </td>
                                       </tr>
                                    </table>
                                    {!! Form::close() !!}
                                </div>
                            </div>                            