<script>
$("#checkMonitor").change(function(){
    var i =0;
    @foreach( $cargasMonitor as $monit )
    i = i + 1;
   $("#monitor"+i).prop('checked', $(this).prop("checked"));
    $("#distritom"+i).prop('checked', $(this).prop("checked"));
    $("#provinciam"+i).prop('checked', $(this).prop("checked"));
    $("#coordinadorm"+i).prop('checked', $(this).prop("checked"));
    $("#zonasm"+i).prop('checked', $(this).prop("checked"));
    
    if($("#monitor"+i).prop('checked')=== true ){
       $("#enviar_labmonitor").css("display","block");
   }else{
       $("#enviar_labmonitor").css("display","none");
   }
    @endforeach;
});

function select_monitor(){
    var p = 0;
    var i = 0;
    @foreach( $cargasMonitor as $selectm );
    p = p + 1;
    var selectMonitor = $("#monitor"+p).prop('checked');     
    if (selectMonitor){
         i = i + 1;
       $('#enviar_labmonitor').css("display", "block");
    }else if (i === 0 ){
       $('#enviar_labmonitor').css("display", "none"); 
    }  
    @endforeach;
};

function selectDistrito(i){
    
    $("#monitor"+i).change(function(){
    $("#distritom"+i).prop('checked', $(this).prop("checked"));
    $("#provinciam"+i).prop('checked', $(this).prop("checked"));
    $("#coordinadorm"+i).prop('checked', $(this).prop("checked"));
    $("#zonasm"+i).prop('checked', $(this).prop("checked"));
    });
};

$(function(){
$("#enviar_labmonitor").click(function(){        
    var id_periodo = $("#periodo").val();
    var fecha = $("#fecha_programada").val();
    var sesion = $("#sesion").val();
    var tp_reporte = $("#tp_reporte").val();
    
    if (id_periodo === "" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "0") {
                //alert("Debe seleccionar un periodo");
                            notif({
                            msg: 'Debe Seleccionar un Periodo !',
                            type: 'warning',
                            opacity: 1,
                            });
            } else {
                $(".monitord").css("display","none");
    $(".labmonitores").css("display","block");
    document.getElementById("loading").style.display="block";
        if (id_periodo !== "" && fecha === "" && sesion === "") {
                    var ur = "laboratorio_aplicacion/" + tp_reporte + "/" + id_periodo + "";
                } else {
                    if (id_periodo !== "" && fecha !== "" && sesion === "") {
                        var ur = "laboratorio_aplicacion/" + tp_reporte + "/" + id_periodo + "/" + fecha + "";
                    } else {
                        var ur = "laboratorio_aplicacion/" + tp_reporte + "/" + id_periodo + "/" + fecha + "/" + sesion + "";
                    }
                }
                 var url = ur;
                $.ajax({
                type: "POST",
                url: url,
                data: $("#formulariom").serialize(),
                success: function(data)
                {
                $("#laboratorio").html(data);
                document.getElementById("loading").style.display="none";
                }
                }); 
         }
   return false;
   //alert(data);
});

});

$(function(){
    $(".azona").click(function(){
    $(".zona").css("display", "block");
    $(".provincia").css("display", "none");
    $(".distritod").css("display", "none");
    $(".monitord").css("display", "none");          
    $(".labmonitores").css("display","none"); 
    });
    });
    
$(function(){
    $(".aprovincia").click(function(){
    $(".provincia").css("display", "block");
    $(".zona").css("display", "none");
    $(".distritod").css("display", "none");
    $(".monitord").css("display", "none");          
    $(".labmonitores").css("display","none"); 
    });
});

$(function(){
    $(".adistrito").click(function(){
    $(".provincia").css("display", "none");
    $(".zona").css("display", "none");
    $(".distritod").css("display", "block");
    $(".monitord").css("display", "none");          
    $(".labmonitores").css("display","none"); 
    });
});
</script>
<ol class="breadcrumb">
    <li><a href="#" class="azona">Zonas</a></li>
    <li><a href="#" class="aprovincia">Provincias</a></li>
    <li><a href="#" class="adistrito">Distritos</a></li>
    <li class="active">Monitores</li>
</ol>

<div class="row">
    <div class="col-sm-12 col-md-10" style="width: auto;">
<div class="box table-responsive no-padding">                               
                                <div class="box-body">
                                     {!! Form::open(['method' => 'POST', 'id' => 'formulariom' ]) !!}
                                    <table class="table table-bordered table-striped table-hover" style="width: auto; padding: 0px;">
                                        <tr style="background-color: #0489B1; color: white;">  
                                        <h4> <th style="text-align: center; vertical-align: middle;" colspan="15"><b>REPORTE DE CARGA DE DISTRITO POR MONITOR</b></th></h4>
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;">  
                                            <th style="text-align: left;  vertical-align: middle; padding: 3px;">
                                                &nbsp;&nbsp; <input type="checkbox" id="checkMonitor"> MONITOR
                                            </th>
                                            <th style="text-align:center; vertical-align: middle; padding: 3px;">DISTRITO</th>
                                            <th style="text-align:center; vertical-align: middle; padding: 3px;">PROVINCIA</th>
                                            <th style="text-align:center; vertical-align: middle; padding: 3px;">COORDINADOR</th>
                                            <th style="text-align:center; vertical-align: middle; padding: 3px;">ZONA</th>
                                            <th style="text-align:center; vertical-align: middle; padding: 3px;">Nº SEDES</th>
                                            <th style="text-align:center; vertical-align: middle; padding: 3px;" >PROGRAMADOS</th>
                                            <th style="text-align:center; vertical-align: middle; padding: 3px;">ASISTENCIA</th>
                                            <th style="text-align:center; vertical-align: middle; padding: 3px;">% ASISTENCIA</th>                                            
                                            <th style="text-align:center; vertical-align: middle; padding: 3px;">AUSENTES</th>
                                            <th style="text-align:center; vertical-align: middle; padding: 3px;">% AUSENTES</th>
                                            <th style="text-align:center; vertical-align: middle; padding: 3px;">CARGAS</th>
                                            <th style="text-align:center; vertical-align: middle; padding: 3px;">% DE CARGA</th>
                                            <th style="text-align: center; vertical-align: middle; padding: 3px;">POR CARGAR <br>(ASISTENCIA)</th>
                                            <th style="text-align: center; vertical-align: middle; padding: 3px;">% POR CARGAR</th>
                                        </tr>
                                        <?php $totalAusen=0; $totalLab=0; $i=0;?>
                                        @foreach( $cargasMonitor as $cargasMonitor)
                                         <?php 
                                         $i++;
                                            $totalLab += ($cargasMonitor->laboratorio);
                                            $totalAusen += ($cargasMonitor->programados - $cargasMonitor->asistencia);
                                            $ausentes= ($cargasMonitor->programados - $cargasMonitor->asistencia) ;
                                            $por_cargar= ($cargasMonitor->asistencia - $cargasMonitor->cargas) ;
                                            if( $cargasMonitor->programados == 0){
                                            $porcecarga = number_format((($cargasMonitor->cargas / 1) * 100), '2',',',''); 
                                            $porceasis = number_format((($cargasMonitor->asistencia / 1) * 100), '2',',',''); 
                                            $porceausente = number_format((($ausentes / 1) * 100), '2',',',''); 
                                            }else{
                                            $porcecarga = number_format((($cargasMonitor->cargas / $cargasMonitor->programados) * 100), '2',',',''); 
                                            $porceasis = number_format((($cargasMonitor->asistencia / $cargasMonitor->programados) * 100), '2',',',''); 
                                            $porceausente = number_format((($ausentes / $cargasMonitor->programados) * 100), '2',',',''); 
                                            if($cargasMonitor->asistencia == 0){
                                            $porceporcargar = number_format((($por_cargar / 1) * 100), '2',',','');     
                                            } else {                                            
                                            $porceporcargar = number_format((($por_cargar / $cargasMonitor->asistencia) * 100), '2',',','');
                                            }
                                            }
                                            ?>
                                        <tr style="padding: 0px;">
                                             <td style="text-align: left; vertical-align: middle; padding: 3px;">
                                                    <label>
                                                        <input type="checkbox" id="monitor{{ $i }}" value="{{ $cargasMonitor->monitor }}" name="monitor[]" onclick="selectDistrito({{ $i }}); select_monitor();">
                                                        {{ $cargasMonitor->name_monitor }}
                                                    </label>
                                            </td> 
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">
                                                    <label>    
                                                        <input type="checkbox" id="distritom{{ $i }}" value="{{ $cargasMonitor->distrito_id }}" name="distrito_m[]" hidden>
                                                    {{ $cargasMonitor->distrito_id }}
                                                    </label>
                                            </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">
                                                    <label>    
                                                        <input type="checkbox" id="provinciam{{ $i }}" value="{{ $cargasMonitor->provincia }}" name="provincia_m[]" hidden>
                                                    {{ $cargasMonitor->provincia }}
                                                    </label>
                                            </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">
                                                    <label>    
                                                        <input type="checkbox" id="coordinadorm{{ $i }}" value="{{ $cargasMonitor->coordinador }}" name="coordinador_m[]" hidden>
                                                    {{ $cargasMonitor->coordinador }}
                                                    </label>
                                            </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">
                                                    <label>    
                                                        <input type="checkbox" id="zonasm{{ $i }}" value="{{ $cargasMonitor->zona }}" name="zona_m[]" hidden>
                                                    {{ $cargasMonitor->zona }}
                                                    </label>
                                            </td>
                                            <td align="right" style="padding: 0px;">{{ $cargasMonitor->laboratorio }}</td>
                                            <td align="right" style="padding: 0px;">{{ $cargasMonitor->programados }}</td>
                                            <td align="right" style="padding: 0px;">{{ $cargasMonitor->asistencia }}</td>
                                            <td align="right" style="padding: 0px;">
                                             <div class="progress">
                                                    <?php if($porceasis <=30){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">     
                                                    <?php }elseif($porceasis >=31 && $porceasis <=79) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($porceasis >= 80) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>{{ $porceasis.'%' }}</b></i></span>                                                        
                                                    </div> 
                                                </div>   
                                            </td>    
                                            <td align="right" style="padding: 0px;">{{ $ausentes }}</td>
                                            <td align="right" style="padding: 0px;">
                                             <div class="progress">
                                                    <?php if($porceausente <=30 ){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                    <?php }elseif($porceausente >=31 && $porceausente <=79) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($porceausente >= 80) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>{{ $porceausente.'%' }}</b></i></span>                                                        
                                                    </div> 
                                                </div>   
                                            </td>
                                            <td align="right" style="padding: 0px;">{{ $cargasMonitor->cargas }}</td>
                                            <td align="right" style="padding: 0px;">
                                            <div class="progress">
                                                    <?php if($porcecarga <=30){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">     
                                                    <?php }elseif($porcecarga >=31 && $porcecarga <=79) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;">                                                        
                                                    <?php }elseif($porcecarga >= 80) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>{{ $porcecarga.'%' }}</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>  
                                            <td align="right" style="padding: 0px;"><b>{{ $por_cargar }}</b></td>
                                            <td align="right" style="padding: 0px;">
                                             <div class="progress">
                                                    <?php if($porceporcargar <='0,00' ){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                    <?php }elseif($porceporcargar >=0 && $porceporcargar <=19) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01; color: #000000">                                                        
                                                    <?php }elseif($porceporcargar >= 20) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>{{ $porceporcargar.'%' }}</b></i></span>                                                        
                                                    </div> 
                                                </div>   
                                            </td>
                                        </tr>
                                        @endforeach
                                        <tr>
                                            <?php 
                                            if($totalpro ==0){
                                                $porcenttAsis= number_format((($totalasis / 1)*100),'2',',','');
                                            $porcenttAuses= number_format((($TotalAusent / 1)*100),'2',',','');
                                            }else{
                                            $porcenttAsis= number_format((($totalasis / $totalpro)*100),'2',',','');
                                            $porcenttAuses= number_format((($TotalAusent / $totalpro)*100),'2',',','');
                                            if ($totalasis == 0){
                                            $porcenttPorCargar= number_format((($TotalPorCargar / 1)*100),'2',',','');    
                                            }else{
                                            $porcenttPorCargar= number_format((($TotalPorCargar / $totalasis)*100),'2',',','');
                                                }                                           
                                            }
                                            ?>
                                        <td style="background-color: #0489B1; color: white;" colspan="5"><b>TOTALES</b></td>
                                             <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ $totalLab }}</b></td>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $totalpro }}</b></td>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $totalasis }}  </b></td>  
                                            <?php if( $porcenttAsis <=75){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">
                                            <?php }elseif( $porcenttAsis >75 && $porcenttAsis <=99){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">
                                            <?php }elseif( $porcenttAsis >=100){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">        
                                            <?php } ?><b>{{ $porcenttAsis }}% </b></td>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalAusent }}  </b></td>
                                            <?php if( $porcenttAuses ==0){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">
                                            <?php }elseif( $porcenttAuses >0 && $porcenttAuses <=25){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">
                                            <?php }elseif( $porcenttAuses >25){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">        
                                            <?php } ?><b>{{ $porcenttAuses }}% </b></td>                                                          
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $totalcargas }} </b></td>                                            
                                            <?php if( $porcentajecar <=75){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">
                                            <?php }elseif( $porcentajecar >75 && $porcentajecar <=99){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">
                                            <?php }elseif( $porcentajecar >=100){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">        
                                            <?php } ?><b>{{ $porcentajecar }}% </b></td>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ $TotalPorCargar }}  </b></td>
                                            <?php if( $porcenttPorCargar ==0){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">
                                            <?php }elseif( $porcenttPorCargar >0 && $porcenttPorCargar <=25){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">
                                            <?php }elseif( $porcenttPorCargar >25){?>
                                            <td align="right" style="background-color: #0489B1; color: white;">        
                                            <?php } ?><b>{{ $porcenttPorCargar }}% </b></td> 
                                        </tr>
                                        <tr>
                                           <td colspan="15"> 
                                              <!-- {!! Form::submit('Para consulta de Distrito por monitor hacer Click aqui', ['class' => 'btn btn-block btn-primary', 'onclick' => 'alert(hola);']) !!} -->
                                              <button class="btn btn-block btn-info" type="submit" id="enviar_labmonitor" style="display: none;"><b>Para consulta de Laboratorio por Monitor hacer Click aqui</b></button>
                                           </td>
                                       </tr>
                                    </table>
                                      {!! Form::close() !!}
                                </div>
                            </div>
                         </div>
                       </div>