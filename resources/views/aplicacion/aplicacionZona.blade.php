<script>
$("#checkZona").change(function(){
    @foreach( $monitores as $descargazona )
   $("#zona{{ $descargazona->zona }}").prop('checked', $(this).prop("checked"));   
   $('#enviar_zona').css("display", "block");
    if($("#zona{{ $descargazona->zona }}").prop('checked')=== true ){
       $("#enviar_zona").css("display","block");
   }else{
       $("#enviar_zona").css("display","none");
   }
    @endforeach;   
});  

function select_zona(){
    var i = 0;
    @foreach( $monitores as $selectz )
    var selectZona = $("#zona{{ $selectz->zona }}").prop('checked');     
    if (selectZona){
         i = i + 1;
       $('#enviar_zona').css("display", "block");
    }else if (i == 0 ){
       $('#enviar_zona').css("display", "none"); 
    }  
    @endforeach;
};


$(function(){
$("#enviar_zona").click(function(){        
    var id_periodo = $("#periodo").val();
    var fecha = $("#fecha_programada").val();
    var sesion = $("#sesion").val();
    var tp_reporte = $("#tp_reporte").val();
    
   if (id_periodo === "" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "0") {
                //alert("Debe seleccionar un periodo");
                            notif({
                            msg: 'Debe Seleccionar un Periodo !',
                            type: 'warning',
                            opacity: 1,
                            });
            } else {
                $(".zona").css("display", "none");
                    $(".provincia").css("display", "block");
                    document.getElementById("loading").style.display = "block";
        if (id_periodo !== "" && fecha === "" && sesion === "") {
                    var ur = "provincia_aplicacion/" + tp_reporte + "/" + id_periodo + "";
                } else {
                    if (id_periodo !== "" && fecha !== "" && sesion === "") {
                        var ur = "provincia_aplicacion/" + tp_reporte + "/" + id_periodo + "/" + fecha + "";
                    } else {
                        var ur = "provincia_aplicacion/" + tp_reporte + "/" + id_periodo + "/" + fecha + "/" + sesion + "";
                    }
                }
                 var url = ur;
                $.ajax({
                type: "POST",
                url: url,
                data: $("#formulario").serialize(),
                success: function(data)
                {
                $("#provincia").html(data);
                document.getElementById("loading").style.display="none";
                }
                }); 
         }
   return false;
   //alert(data);
});

});


</script>
<div class="zona">
        <ol class="breadcrumb">
                    <li><a href="exportar_info_aplicacion/{{ $idperiodo }}/excel">
                        <img src="images/excel.png" title="DESCARGAR EXCEL" style=" height: 23px; margin-right: -13px;">
                        </a> &nbsp; &nbsp; &nbsp;
                    </li>
<!--                    <li><a href="exportar_info_sustentantes_off/{{ $idperiodo }}/excel">
                        <img src="images/excel.png" title="DESCARGAR EXCEL" style=" height: 23px; margin-right: -13px;">
                        </a> &nbsp; &nbsp; &nbsp;
                    </li>-->
                    <li><a href="#">Zonas</a></li>
                    </ol>
</div>
<div class="row zona">
    <div class="col-lg-3 col-sm-12" style="width: auto;">
                            <div class="box table-responsive no-padding">
                                <div class="box-body">
                                    <table class="table table-bordered table-striped table-hover" style="width: auto; font-size: 83%;">
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >{{ $periodo }}</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >AVANCE DE LLAMADAS</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >ESTADO</th>
                                            <th style="text-align: center;">CANTIDAD</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                        </tr>
                                         @foreach( $reporteLlamada as $llamada)
                                        <?php 
                                        if( $TotalLlamada == 0){
                                            $porcenllamada = number_format((($llamada->cantidadllamada / 1) * 100), '2',',','') ;
                                        }else{
                                            $porcenllamada = number_format((($llamada->cantidadllamada / $TotalLlamada) * 100), '2',',',''); 
                                        }
                                        ?>
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;"><b>{{ $llamada->estadollamada }}</b></td>                                           
                                            <td style="text-align: right;"><b> {{ number_format($llamada->cantidadllamada,0,",",".") }} </b></td> 
                                            
                                            <?php
                                            if( $llamada->estadollamada == 'CONTACTADO'){
                                            if($porcenllamada <= 44){ ?>                                            
                                            <td style="text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcenllamada }} % </b></td>
                                            <?php }elseif($porcenllamada >= 45 && $porcenllamada <= 99){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: #000000;"><b> {{ $porcenllamada }} % </b></td>
                                            <?php }elseif($porcenllamada >= 100){ ?>
                                            <td style="text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcenllamada }} % </b></td>
                                            <?php } 
                                            }elseif( $llamada->estadollamada == 'NO CONTACTADO'){
                                            if($porcenllamada <= '0,00'){ ?>
                                            <td style="text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcenllamada }} % </b></td>
                                            <?php }elseif($porcenllamada >= 0 && $porcenllamada <= 19){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: #000000;"><b> {{ $porcenllamada }} % </b></td>
                                            <?php }elseif($porcenllamada >= 20 ){ ?>
                                            <td style="text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcenllamada }} % </b></td>
                                            
                                            <?php } }elseif($llamada->estadollamada == 'POR CONTACTAR'){ ?> 
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ $porcenllamada }} % </b></td>
                                            <?php } ?>
                                            
                                        </tr>                                        
                                        @endforeach
                                        <tr>
                                            <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ number_format($TotalLlamada,0,",",".") }}</b></td>
                                            <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                                       </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div>
    
    <div class="col-lg-3 col-sm-12" style="width: auto">
                            <div class="box table-responsive no-padding">
                                 <!-- <div class="box-header with-border" style="padding: 0px;">
                                   <h5>&nbsp;&nbsp;&nbsp;<i class="fa fa-paste"></i> <b> Reporte de Problema de Llamadas</b></h5>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                     <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 83%;">
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >NO CONTACTADOS</th> 
                                        </tr> 
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >PROBLEMA</th>
                                            <th style="text-align: center;">CANTIDAD</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                        </tr>
                                        @foreach( $novedad as $novedad)
                                        <?php  
                                        
                                        if($ttnovedad == 0){
                                        $porcenovedada = number_format((($novedad->totalnovedad_apli / 1) * 100), '2',',','');
                                        }elseif($ttnovedad != 0 ){
                                        $porcenovedada = number_format((($novedad->totalnovedad_apli / $ttnovedad) * 100), '2',',','') ;
                                        }        
                                        ?>
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;"><b>{{ $novedad->novedadllamada_apli }}</b></td>                                           
                                            <td style="text-align: right;"><b> {{ number_format($novedad->totalnovedad_apli,0,",",".") }} </b></td>
                                            
                                            <?php if($porcenovedada == '0,00' ){ ?>                                            
                                            <td style="text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcenovedada }} % </b></td>
                                            <?php }elseif($porcenovedada >= 0 && $porcenovedada <= 19 ){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: white;"><b> {{ $porcenovedada }} % </b></td>
                                            <?php }elseif($porcenovedada >= 20 ){ ?>
                                            <td style="text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcenovedada }} % </b></td>
                                            <?php } ?>
                                            
                                        </tr>                                        
                                        @endforeach
                                        <tr>
                                            <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ number_format($ttnovedad,0,",",".") }} </b></td>
                                            <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                                       </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div>  

</div>
<div class="row zona">
    <div  class="col-lg-3 col-sm-12" style="width: auto;">
                            <div class="box table-responsive no-padding">
                                <!-- <div class="box-header with-border">
                                    <h5><i class="fa fa-paste"></i> <b> Reporte de Descarga del Aplicativo</b></h5>
                                </div> /.box-header -->
                                <div class="box-body">
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 83%;">
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >{{ $periodo }}</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >DESCARGA DE APLICATIVO</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >ESTADO</th>
                                            <th style="text-align: center;">CANTIDAD</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                        </tr>
                                        @foreach($reportdescgeneral as $generalDesc)
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;">
                                              <b> @if ( $generalDesc->descarga == 'SI') DESCARGÓ @else {{$generalDesc->descarga}} DESCARGÓ @endif </b></td>                                           
                                            <td style="text-align: right;">
                                                <?php if($ss == ''){$ss =0;}else{$ss = $ss;} ?>
                                             @if ( $generalDesc->descarga != 'SI') <a href="observacion_descarga/{{ $idperiodo }}/{{ $fecha }}/{{ $ss }}/excel"> 
                                                <b> {{ number_format($generalDesc->cantidad,0,",",".") }} </b></a>@else <b> {{ number_format($generalDesc->cantidad,0,",",".") }} </b></td>
                                            @endif
                                            <!--<b> {{ number_format($generalDesc->cantidad,0,",",".") }} </b>-->
                                            <?php 
                                            if($totaldesc == 0){
                                            $porcen = number_format($generalDesc->cantidad / $totaldesc );      
                                            }elseif($totaldesc !=0){
                                            $porcen = number_format((($generalDesc->cantidad / $totaldesc ) * 100), '2',',','');  
                                            }
                                            ?>
                                            <?php if($porcen <= 44 && $generalDesc->descarga == 'SI' ){ ?>                                            
                                            <td style="text-align: right; background-color: #FA5858; color: white;">
                                            <?php }elseif($porcen >= 45 && $porcen <= 99 && $generalDesc->descarga == 'SI'){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: #000000;">
                                            <?php }elseif($porcen >= 100 && $generalDesc->descarga == 'SI'){ ?>
                                            <td style="text-align: right; background-color: #5FB404; color: white;">
                                            <?php }elseif($generalDesc->descarga != 'SI'){ ?>
                                            <td style="text-align: right; background-color: #0489B1; color: white;">
                                            <?php } ?>     
                                                <b> {{ $porcen }} % </b></td>
                                        </tr>   
                                        @endforeach
                                        <tr>
                                            <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ number_format($totaldesc,0,",",".") }}</b></td>
                                            <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                                       </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div>

                         <div class="col-lg-3 col-sm-12" id="noInicio" style="width: auto; ">
                            <div class="box table-responsive no-padding">
                                 <!-- <div class="box-header with-border" style="padding: 0px;">
                                   <h5>&nbsp;&nbsp;&nbsp;<i class="fa fa-paste"></i> <b> Reporte de Problema de Llamadas</b></h5>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                     <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 83%;">
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="4" >NO DESCARGÓ</th> 
                                        </tr> 
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >PROBLEMA</th>
                                            <th style="text-align: center;">CANTIDAD PROBLEMA</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                            <th style="text-align: center;">SUSTENTANTES AFECTADOS</th> 
                                        </tr>
                                        @foreach( $noDescargo as $noDescargo)
                                        <?php  
                                         
                                        if($ttNoDescargo == 0){
                                        $porcenoDescarg = number_format((($noDescargo->totalobservacion / 1) * 100), '2',',','');
                                        }elseif($ttNoDescargo != 0 ){
                                        $porcenoDescarg = number_format((($noDescargo->totalobservacion / $ttNoDescargo) * 100), '2',',','') ;
                                        }        
                                        ?>
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;"><b>{{ $noDescargo->observacion_descarga }}</b></td>                                           
                                            <td style="text-align: right;"><b> {{ number_format($noDescargo->totalobservacion,0,",",".") }} </b></td>
                                            
                                            <?php if($porcenoDescarg == '0,00' ){ ?>                                            
                                            <td style="text-align: right; background-color: #5FB404; color: white;">
                                            <?php }elseif($porcenoDescarg >= 0 && $porcenoDescarg <= 19 ){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: white;">
                                            <?php }elseif($porcenoDescarg >= 20 ){ ?>
                                            <td style="text-align: right; background-color: #FA5858; color: white;">
                                            <?php } ?>
                                                <b> {{ $porcenoDescarg }} % </b></td>
                                            <td style="text-align: right;"><b> {{ number_format($noDescargo->sustentantes_afect,0,",",".") }} </b></td>
                                            
                                        </tr>                                        
                                        @endforeach
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;"><b>TOTAL NO DESCARGÓ</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ number_format($ttNoDescargo,0,",",".") }} </b></td>
                                            <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ number_format($ttDSusAfectados,0,",",".") }} </b></td>
                                       </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div>

                         
</div>    
<div class="row zona">
<div  class="col-md-3 col-sm-12" style="width: auto;">
                            <div class="box table-responsive no-padding">
                                <div class="box-body">
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 83%;">
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >{{ $periodo }}</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >INSTALACIÓN DE APLICATIVO</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >ESTADO</th>
                                            <th style="text-align: center;">CANTIDAD</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                        </tr>
                                        @foreach( $reporteinstaloapli as $generalInst)
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;">
                                            <b> @if ( $generalInst->instalo == 'SI') INSTALÓ @else {{ $generalInst->instalo }} INSTALÓ @endif </b>
                                            </td>                                               
                                            <td style="text-align: right;">
                                                <?php if($ss == ''){$ss =0;}else{$ss = $ss;} ?>
                                             @if ( $generalInst->instalo != 'SI') <a href="observacion_instalo/{{ $idperiodo }}/{{ $fecha }}/{{ $ss }}/excel"> 
                                                <b> {{ number_format($generalInst->cantidad,0,",",".") }} </b></a>@else <b> {{ number_format($generalInst->cantidad,0,",",".") }} </b></td>
                                            @endif 
                                            <!--<b> {{ number_format($generalInst->cantidad,0,",",".") }} </b>-->
                                            <?php 
                                            if($TotalInst == 0){
                                            $porcen = number_format($generalInst->cantidad / $TotalInst );      
                                            }elseif($TotalInst !=0){
                                            $porcen = number_format((($generalInst->cantidad / $TotalInst ) * 100), '2',',','');  
                                            }
                                            ?>
                                            <?php if($porcen <= 44 && $generalInst->instalo == 'SI'){ ?>                                            
                                            <td style="text-align: right; background-color: #FA5858; color: white;">
                                            <?php }elseif($porcen >= 45 && $porcen <= 99 && $generalInst->instalo == 'SI'){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: #000000;">
                                            <?php }elseif($porcen >= 100 && $generalInst->instalo == 'SI'){ ?>
                                            <td style="text-align: right; background-color: #5FB404; color: white;">
                                            <?php }elseif($generalInst->instalo != 'SI'){ ?>
                                            <td style="text-align: right; background-color: #0489B1; color: white;">
                                            <?php } ?>  
                                                <b> {{ $porcen }} % </b></td>
                                        </tr>   
                                        @endforeach
                                        <tr>
                                            <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ number_format($TotalInst,0,",",".") }}</b></td>
                                            <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                                       </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div>
    
    
    <div class="col-sm-12 col-md-3" style="width: auto;">
                            <div class="box table-responsive no-padding">
                                <div class="box-body">
                                    <table class="table table-bordered table-striped table-hover" style="width: auto; font-size: 83%;">
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >{{ $periodo }}</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >INICIO DE SESIÓN</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >ESTADO</th>
                                            <th style="text-align: center;">CANTIDAD</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                        </tr>
                                         @foreach( $reporteinisesion as $sesion)
                                        <?php 
                                        if( $TotalSesion == 0){
                                            $porcensesion = number_format((($sesion->cantidad / 1) * 100), '2',',','') ;
                                        }else{
                                            $porcensesion = number_format((($sesion->cantidad / $TotalSesion) * 100), '2',',',''); 
                                        }
                                        ?>
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;">
                                            <b> @if ( $sesion->sesion == 'SI') INICIÓ @else {{ $sesion->sesion }} INICIÓ @endif </b>
                                            </td> 
                                            <td style="text-align: right;">
                                                <?php if($ss == ''){$ss =0;}else{$ss = $ss;} ?>
                                              @if ( $sesion->sesion != 'SI') <a href="observacion_inicio/{{ $idperiodo }}/{{ $fecha }}/{{ $ss }}/excel"> 
                                              <b> {{ number_format($sesion->cantidad,0,",",".") }} </b> </a>@else <b> {{ number_format($sesion->cantidad,0,",",".") }} </b>
                                              @endif
                                            </td>
                                            <?php if($porcensesion <= 44 && $sesion->sesion == 'SI'){ ?>                                            
                                            <td style="text-align: right; background-color: #FA5858; color: white;">
                                            <?php }elseif($porcensesion >= 45 && $porcensesion <= 99 && $sesion->sesion == 'SI'){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: #000000;">
                                            <?php }elseif($porcensesion >= 100 && $sesion->sesion == 'SI'){ ?>
                                            <td style="text-align: right; background-color: #5FB404; color: white;">
                                            <?php }elseif($sesion->sesion != 'SI'){ ?>
                                            <td style="text-align: right; background-color: #0489B1; color: white;">
                                            <?php } ?>                  
                                               <b> {{ $porcensesion }} % </b></td>
                                        </tr>                                        
                                        @endforeach
                                        <tr>
                                            <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ number_format($TotalSesion,0,",",".") }}</b></td>
                                            <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                                       </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div>

                         <div class="col-md-3 col-sm-12" id="noInicio" style="width: auto; ">
                            <div class="box table-responsive no-padding">
                                 <!-- <div class="box-header with-border" style="padding: 0px;">
                                   <h5>&nbsp;&nbsp;&nbsp;<i class="fa fa-paste"></i> <b> Reporte de Problema de Llamadas</b></h5>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                     <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 83%;">
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="4" >NO INICIÓ SESIÓN</th> 
                                        </tr> 
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >PROBLEMA</th>
                                            <th style="text-align: center;">CANTIDAD PROBLEMA</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                            <th style="text-align: center;">SUSTENTANTES AFECTADOS</th> 
                                        </tr>
                                        @foreach( $noInicioSsesion as $noInicioSsesion)
                                        <?php  
                                        
                                        if($ttNoInicio == 0){
                                        $porcenoInicioSs = number_format((($noInicioSsesion->totalobservacion / 1) * 100), '2',',','');
                                        }elseif($ttNoInicio != 0 ){
                                        $porcenoInicioSs = number_format((($noInicioSsesion->totalobservacion / $ttNoInicio) * 100), '2',',','') ;
                                        }        
                                        ?>
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;"><b>{{ $noInicioSsesion->observacion_sesion }}</b></td>                                           
                                            <td style="text-align: right;"><b> {{ number_format($noInicioSsesion->totalobservacion,0,",",".") }} </b></td>
                                            
                                            <?php if($porcenoInicioSs == '0,00' ){ ?>                                            
                                            <td style="text-align: right; background-color: #5FB404; color: white;">
                                            <?php }elseif($porcenoInicioSs >= 0 && $porcenoInicioSs <= 19 ){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: white;">
                                            <?php }elseif($porcenoInicioSs >= 20 ){ ?>
                                            <td style="text-align: right; background-color: #FA5858; color: white;">
                                            <?php } ?>
                                                <b> {{ $porcenoInicioSs }} % </b></td>
                                            <td style="text-align: right;"><b> {{ number_format($noInicioSsesion->sustentantes_afect,0,",",".") }} </b></td>
                                            
                                        </tr>                                        
                                        @endforeach
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;"><b>TOTAL NO INICIÓ</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ number_format($ttNoInicio,0,",",".") }} </b></td>
                                            <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ number_format($ttSusAfectados,0,",",".") }} </b></td>
                                       </tr>
                                       @foreach($n_Sustentantes as $n_Sustentantes)
                                       <?php 
                                        if( $Total_Sustentantes == 0){
                                            $porcenTtSust = number_format((($n_Sustentantes->programados / 1) * 100), '2',',','') ;
                                        }else{
                                            $porcenTtSust = number_format((($n_Sustentantes->programados / $Total_Sustentantes) * 100), '2',',',''); 
                                        }
                                        ?>
                                      <!-- <tr>
                                            <td style="background-color: #0489B1; color: white; width: 140px;"><b>SUSTENTANTES AFECTADOS</b></td>    
                                            <td style="text-align: right;"><b>@IF ($n_Sustentantes->programados == '') 0 @ELSE {{ $n_Sustentantes->programados }} @ENDIF </b></td>
                                            
                                            <?php /* if($porcenTtSust <= '0.00'){ ?>
                                            <td style="text-align: right; background-color: #5FB404; color: white;">
                                            <?php }elseif($porcenTtSust >= 0 && $porcenTtSust <= 19){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: white;">
                                            <?php }elseif($porcenTtSust >= 20){ ?>                                            
                                            <td style="text-align: right; background-color: #FA5858; color: white;">
                                            <?php } */?>   
                                            <b> {{ $porcenTtSust }} % </b></td>
                                       </tr>
                                       @endforeach                                                                             
                                       <tr>
                                            <td style="background-color: #0489B1; color: white; width: 140px;"><b>TOTAL SUSTENTANTES</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ $Total_Sustentantes }} </b></td>
                                            <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                                       </tr> -->
                                    </table>
                                </div>
                            </div>                                               
                        </div>
    
</div>
<div class="row zona">

 <div hidden class="col-lg-3 col-sm-12" style="width: auto;">
                            <div class="box table-responsive no-padding">
                                <div class="box-body">
                                    <table class="table table-bordered table-striped table-hover" style="width: auto; font-size: 83%;">
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >{{ $periodo }}</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >INICIO DE SESIÓN CONTEXTO</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >ESTADO</th>
                                            <th style="text-align: center;">CANTIDAD</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                        </tr>
                                         @foreach( $reporteinisesion2 as $sesion)
                                        <?php 
                                        if( $TotalSesion2 == 0){
                                            $porcensesion = number_format((($sesion->cantidad / 1) * 100), '2',',','') ;
                                        }else{
                                            $porcensesion = number_format((($sesion->cantidad / $TotalSesion2) * 100), '2',',',''); 
                                        }
                                        ?>
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;">
                                            <b> @if ( $sesion->sesion == 'SI') INICIÓ @else {{ $sesion->sesion }} INICIÓ @endif </b>
                                            </td> 
                                            <td style="text-align: right;">
                                                <?php if($ss == ''){$ss =0;}else{$ss = $ss;} ?>
                                              @if ( $sesion->sesion != 'SI') <a href="#"> 
                                              <b> {{ number_format($sesion->cantidad,0,",",".") }} </b> </a>@else <b> {{ number_format($sesion->cantidad,0,",",".") }} </b>
                                              @endif
                                            </td>
                                            <?php if($porcensesion <= 44 && $sesion->sesion == 'SI'){ ?>                                            
                                            <td style="text-align: right; background-color: #FA5858; color: white;">
                                            <?php }elseif($porcensesion >= 45 && $porcensesion <= 99 && $sesion->sesion == 'SI'){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: #000000;">
                                            <?php }elseif($porcensesion >= 100 && $sesion->sesion == 'SI'){ ?>
                                            <td style="text-align: right; background-color: #5FB404; color: white;">
                                            <?php }elseif($sesion->sesion != 'SI'){ ?>
                                            <td style="text-align: right; background-color: #0489B1; color: white;">
                                            <?php } ?>                  
                                               <b> {{ $porcensesion }} % </b></td>
                                        </tr>                                        
                                        @endforeach
                                        <tr>
                                            <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ number_format($TotalSesion2,0,",",".") }}</b></td>
                                            <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                                       </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div>
</div>

<div class="row zona" hidden>
    <div class="col-lg-3 col-sm-12" style="width: auto">
                            <div class="box table-responsive no-padding">
                                 <!-- <div class="box-header with-border" style="padding: 0px;">
                                   <h5>&nbsp;&nbsp;&nbsp;<i class="fa fa-paste"></i> <b> Reporte de Problema de Llamadas</b></h5>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                     <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 83%;">
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >CUADRE DE ARCHIVO {{ $periodo }}</th> 
                                        </tr> 
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >ESTADO</th>
                                            <th style="text-align: center;">CANTIDAD</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                        </tr>
                                        @foreach( $reportesustentantes as $sustentantes)
                                        <?php  
                                        
                                        if($TotalListsus == 0){
                                        $porcesusten = number_format((($sustentantes->cantidad / 1) * 100), '2',',','');
                                        }elseif($TotalListsus != 0 ){
                                        $porcesusten = number_format((($sustentantes->cantidad / $TotalListsus) * 100), '2',',','') ;
                                        }        
                                        ?>
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;"><b>{{ $sustentantes->status }}</b></td>                                           
                                            <td style="text-align: right;"><b> {{ number_format(($sustentantes->cantidad - 3 ),0,",",".") }} </b></td>
                                            
                                            <?php if($porcesusten == '0,00' ){ ?>                                        
                                            <td style="text-align: right; background-color: #5FB404; color: white;"><b> {{ $porcesusten }} % </b></td>
                                            <?php }
                                            elseif($porcesusten >= 0 && $porcesusten <= 19 ){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: white;"><b> {{ $porcesusten }} % </b></td>
                                            <?php }
                                            elseif($porcesusten >= 20 ){ ?>
                                            <td style="text-align: right; background-color: #FA5858; color: white;"><b> {{ $porcesusten }} % </b></td>
                                            <?php } ?>
                                            
                                        </tr>                                        
                                        @endforeach
                                        <tr>
                                            <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b> {{ number_format($TotalListsus,0,",",".") }} </b></td>
                                            <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                                       </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div>  
</div>    
<div class="row zona">
 <!--reprorte de listado de asistencia -->
 <div hidden class="col-lg-3 col-sm-12" style="width: auto;">
                            <div class="box table-responsive no-padding">
                                <!-- <div class="box-header with-border">
                                    <h5><i class="fa fa-paste"></i> <b> Reporte de Descarga del Aplicativo</b></h5>
                                </div> /.box-header -->
                                <div class="box-body">
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 85%;">
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >{{ $periodo }}</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >RECEPCIÓN DE LISTADO DE ASISTENCIA</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >ESTADO</th>
                                            <th style="text-align: center;">CANTIDAD</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                        </tr>
                                        @foreach($reportelistadosasistencia as $generalDesc)
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;">
                                              <b> @if ( $generalDesc->listado == 'SI') RECIBIDO @else {{$generalDesc->listado}} RECIBIDO @endif </b></td>                                           
                                            <td style="text-align: right;">
                                                <?php if($ss == ''){$ss =0;}else{$ss = $ss;} ?>
                                            <!-- -->
                                            <b> {{ number_format($generalDesc->cantidad,0,",",".") }} </b>
                                            <?php 
                                            if($TotalList == 0){
                                            $porcen = number_format($generalDesc->cantidad / $TotalList );      
                                            }elseif($TotalList !=0){
                                            $porcen = number_format((($generalDesc->cantidad / $TotalList ) * 100), '2',',','');  
                                            }
                                            ?>
                                            <?php if($porcen <= 44 && $generalDesc->listado == 'SI' ){ ?>                                            
                                            <td style="text-align: right; background-color: #FA5858; color: white;">
                                            <?php }elseif($porcen >= 45 && $porcen <= 99 && $generalDesc->listado == 'SI'){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: #000000;">
                                            <?php }elseif($porcen >= 100 && $generalDesc->listado == 'SI'){ ?>
                                            <td style="text-align: right; background-color: #5FB404; color: white;">
                                            <?php }elseif($generalDesc->listado != 'SI'){ ?>
                                            <td style="text-align: right; background-color: #0489B1; color: white;">
                                            <?php } ?>     
                                                <b> {{ $porcen }} % </b></td>
                                        </tr>   
                                        @endforeach
                                        <tr>
                                            <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ number_format($TotalList,0,",",".") }}</b></td>
                                            <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                                       </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div>      

                        <!--reprorte de listado de asistencia problemas -->
    <div hidden class="col-lg-3 col-sm-12" style="width: auto;">
                            <div class="box table-responsive no-padding">
                                <!-- <div class="box-header with-border">
                                    <h5><i class="fa fa-paste"></i> <b> Reporte de Descarga del Aplicativo</b></h5>
                                </div> /.box-header -->
                                <div class="box-body">
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 85%;">
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >{{ $periodo }}</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" colspan="3" >LISTADO DE ASISTENCIA RECIBIDOS</th> 
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;"> 
                                            <th style="text-align:center;" >ESTADO</th>
                                            <th style="text-align: center;">CANTIDAD</th>
                                            <th style="text-align: center;">% AVANCE</th> 
                                        </tr>
                                        @foreach($reportelistadosasistenciarecibidos as $generalDesc)
                                        <tr>
                                            <td style="text-align: left; background-color: #0489B1; color: white;">
                                              <b> @if ( $generalDesc->listadosrecibidos == 'VALIDADOS') VALIDADOS @else {{$generalDesc->listadosrecibidos}}  @endif </b></td>                                           
                                            <td style="text-align: right;">
                                                <?php if($ss == ''){$ss =0;}else{$ss = $ss;} ?>
                                            <!-- -->
                                            <b> {{ number_format($generalDesc->cantidad,0,",",".") }} </b>
                                            <?php 
                                            if($TotalListreci == 0){
                                            $porcen = number_format($generalDesc->cantidad / $TotalListreci );      
                                            }elseif($TotalListreci !=0){
                                            $porcen = number_format((($generalDesc->cantidad / $TotalListreci ) * 100), '2',',','');  
                                            }
                                            ?>
                                            <?php if($porcen <= 44 && $generalDesc->listadosrecibidos == 'VALIDADOS' ){ ?>                                            
                                            <td style="text-align: right; background-color: #FA5858; color: white;">
                                            <?php }elseif($porcen >= 45 && $porcen <= 99 && $generalDesc->listadosrecibidos == 'VALIDADOS'){ ?>
                                            <td style="text-align: right; background-color: #D7DF01; color: #000000;">
                                            <?php }elseif($porcen >= 100 && $generalDesc->listadosrecibidos == 'VALIDADOS'){ ?>
                                            <td style="text-align: right; background-color: #5FB404; color: white;">
                                            <?php }elseif($generalDesc->listadosrecibidos != 'VALIDADOS'){ ?>
                                            <td style="text-align: right; background-color: #0489B1; color: white;">
                                            <?php } ?>     
                                                <b> {{ $porcen }} % </b></td>
                                        </tr>   
                                        @endforeach
                                        <tr>
                                            <td style="text-align: center; background-color: #0489B1; color: white;"><b>TOTALES</b></td>    
                                            <td style="text-align: right; background-color: #0489B1; color: white;"><b>{{ number_format($TotalListreci,0,",",".") }}</b></td>
                                            <td style=" text-align: right; background-color: #0489B1; color: white;"><b> 100 % </b></td>
                                       </tr>
                                    </table>
                                </div>
                            </div>                                               
                        </div>      
</div>
<div class="row zona">
   
    
    
</div>
<div class="row zona">
   
    
    
</div>



<div class="row zona" style="display: block;">
   
<div class="col-sm-12 col-md-10" style="width: auto;">
<div class="box table-responsive no-padding " style="width: auto;">
                                <div class="box-body">
                                    {!! Form::open(['method' => 'POST', 'id' => 'formulario' ]) !!}
                                    <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 90%;">
                                        <tr style="background-color: #0489B1; color: white;">  
                                        <h4> <th style="text-align: center; vertical-align: middle;" colspan="12"><b>REPORTE POR ZONAS</b></th></h4>
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;">  
                                            <th style="width: 20px; text-align:center;" >
                                            <label>
                                                <input type="checkbox" id="checkZona"> <b>ZONA</b>                                                
                                            </label>
                                            </th>
                                            <th style="text-align:center;  vertical-align: middle; padding: 3px;">N° SEDES</th>
                                            <th style="text-align:center;  vertical-align: middle; padding: 3px;" colspan="2">DESCARGÓ</th>
                                            <th style="text-align:center;  vertical-align: middle; padding: 3px" colspan="2">INSTALÓ</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;" colspan="2">INICIÓ SESIÓN</th>
                                            <th style="text-align: center;  vertical-align: middle; padding: 3px;" colspan="4">ESTADO DE LLAMADAS</th>
                                        </tr>
                                        <tr style="background-color: #0489B1; color: white;">
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">  </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> SÍ </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> NO </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> SÍ </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> NO </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> SÍ </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> NO </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> CONTACTADO </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> POR CONTACTAR</td>
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;"> NO CONTACTADO </td>
                                            <!-- <td style="text-align: center; vertical-align: middle; padding: 3px;"> PROBLEMA DE LLAMADA </td> -->
                                            
                                        </tr>                                        
                                        
                                            @foreach( $monitores as $monitor )
                                            
                                            <?php 
                                            if( $monitor->laboratorio == 0){
                                            $porcedesc = number_format((($monitor->descargasi / 1) * 100), '2',',',''); 
                                            $porceinstalo = number_format((($monitor->instalosi / 1) * 100), '2',',',''); 
                                            $porcesesion = number_format((($monitor->sesionsi / 1) * 100), '2',',',''); 
                                            $porcecontactado = number_format((($monitor->contactado / 1) * 100), '2',',',''); 
                                            $porcenocontactado = number_format((($monitor->nocontactado / 1) * 100), '2',',',''); 
                                            $porceenproceso = number_format((($monitor->enproceso / 1) * 100), '2',',',''); 
                                            $porcenovedad = number_format((($monitor->novedadllamada / 1) * 100), '2',',',''); 
                                            }else{
                                            $porcedesc = number_format((($monitor->descargasi / $monitor->laboratorio) * 100), '2',',',''); 
                                            $porceinstalo = number_format((($monitor->instalosi / $monitor->laboratorio) * 100), '2',',',''); 
                                            $porcesesion = number_format((($monitor->sesionsi / $monitor->laboratorio) * 100), '2',',',''); 
                                            $porcecontactado = number_format((($monitor->contactado / $monitor->laboratorio) * 100), '2',',','');
                                            $porcenocontactado = number_format((($monitor->nocontactado / $monitor->laboratorio) * 100), '2',',','');
                                            $porceenproceso = number_format((($monitor->enproceso / $monitor->laboratorio) * 100), '2',',','');
                                            $porcenovedad = number_format((($monitor->novedadllamada / $monitor->laboratorio) * 100), '2',',','');
                                            
                                            }
                                            ?>
                                        <tr>                                            
                                            <td style="text-align: center; vertical-align: middle; padding: 3px;">
                                                    <label>    
                                                        <input type="checkbox" id="zona{{ $monitor->zona }}" value="{{ $monitor->zona }}" name="zona[]" onclick="select_zona();">
                                                    {{ $monitor->zona }}
                                                    </label>
                                            </td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;"><b>{{ number_format($monitor->laboratorio,0,",",".") }}</b></td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                                <div class="progress">
                                                    <?php if($porcedesc <=44){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">     
                                                    <?php }elseif($porcedesc >= 45 && $porcedesc <= 99 ) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01;  color: #000000;">
                                                    <?php }elseif($porcedesc >= 100) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">
                                                    <?php } ?>        
                                                            <span class="skill"><i class="val"><b>( {{ number_format($monitor->descargasi,0,",",".") }} ) {{ $porcedesc.'%' }}</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;"><b>{{ number_format($monitor->descargano,0,",",".") }}</b></td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php if( $porceinstalo <=44 ){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">     
                                                    <?php }elseif($porceinstalo >=45 && $porceinstalo <=99) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01; color: #000000;">                                                        
                                                    <?php }elseif($porceinstalo >= 100) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">
                                                    <?php } ?>        
                                                      <span class="skill"><i class="val"><b>( {{ number_format($monitor->instalosi,0,",",".") }} ) {{ $porceinstalo.'%' }}</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;"><b>{{ number_format($monitor->instalono,0,",",".") }}</b></td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php if($porcesesion <=44){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">     
                                                    <?php }elseif($porcesesion >=45 && $porcesesion <=99 ) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01; color: #000000;">                                                        
                                                    <?php }elseif($porcesesion >=100 ) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>( {{ number_format($monitor->sesionsi,0,",",".") }} ) {{ $porcesesion.'%' }}</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>
                                            <td style="text-align: right; vertical-align: middle; padding: 8px;"><b>{{ number_format($monitor->sesionno,0,",",".") }}</b></td>
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php if($porcecontactado <= 44){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">     
                                                    <?php }elseif($porcecontactado >= 45 && $porcecontactado <=99) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01; color: #000000;">                                                        
                                                    <?php }elseif($porcecontactado >= 100) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>( {{ number_format($monitor->contactado,0,",",".") }} ) {{ $porcecontactado }}%</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>
                                            <td style="text-align: right; vertical-align: middle; padding-right: 5px;"><b>{{ number_format($monitor->enproceso,0,",",".") }}</b></td>
                                            <!--
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php /* if($monitor->enproceso ==0){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: white; color: #000000;">     
                                                    <?php }elseif($monitor->enproceso != $monitor->laboratorio) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: white; color: #000000;">                                                        
                                                    <?php }elseif($monitor->enproceso == $monitor->laboratorio) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: white; color: #000000;">
                                                    <?php } */?>        
                                                        <span class="skill"><i class="val"><b>{{ $monitor->enproceso }}</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>
                                            -->
                                            <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php if($porcenocontactado <= '0,00'){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                    <?php }elseif($porcenocontactado >=0 && $porcenocontactado <=19) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01; color: #000000;">                                                        
                                                    <?php }elseif($porcenocontactado >= 20) {?>
                                                        <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #FA5858;">
                                                    <?php } ?>        
                                                        <span class="skill"><i class="val"><b>( {{ number_format($monitor->nocontactado,0,",",".") }} ) {{ $porcenocontactado }}%</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>
                                            
                                            <!-- <td style="text-align: right; vertical-align: middle; padding: 3px;">
                                             <div class="progress">
                                                    <?php /* if($monitor->novedadllamada == $monitor->nocontactado){ ?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #5FB404;">     
                                                    <?php }elseif($monitor->novedadllamada != $monitor->nocontactado) {?>
                                                    <div class="progress-bar" role="progressbar" style="width: 100%; background-color: #D7DF01; color: #000000;">    
                                                    <?php } */ ?>        
                                                        <span class="skill"><i class="val"><b>( {{ $monitor->novedadllamada }} ) {{ $porcenovedad }}%</b></i></span>                                                        
                                                    </div> 
                                                </div>
                                            </td>             -->                              
                                        </tr>
                                            @endforeach
                                            <tr><input type="hidden" value="{{ $coordinador }}" name="coordinador">
                                             <td style="background-color: #0489B1; color: white;"><b>TOTALES</b></td>
                                             <?php
                                             if($TotalLab == 0){
                                            $porcedesca = ($TotalDescargosi / 1); 
                                            $porceinstal = ($TotalInstalosi / 1); 
                                            $porcesesio = ($TotalSesionsi / 1) ; 
                                            $porcecontac = ($TotalContactado / 1) ; 
                                            $porcenocontac = ($TotalNoContactado / 1) ; 
                                            $porceenproc = ($TotalEnProceso / 1) ; 
                                            $porcenoved = ($TotalNovedad / 1) ; 
                                            }else{
                                            $porcedesca = number_format((($TotalDescargosi / $TotalLab) * 100), '2',',',''); 
                                            $porceinstal = number_format((($TotalInstalosi / $TotalLab) * 100), '2',',',''); 
                                            $porcesesio = number_format((($TotalSesionsi / $TotalLab) * 100), '2',',',''); 
                                            $porcecontac = number_format((($TotalContactado / $TotalLab) * 100), '2',',',''); 
                                            $porcenocontac = number_format((($TotalNoContactado / $TotalLab) * 100), '2',',',''); 
                                            $porceenproc = number_format((($TotalEnProceso / $TotalLab) * 100), '2',',',''); 
                                            $porcenoved = number_format((($TotalNovedad / $TotalLab) * 100), '2',',',''); 
                                            }
                                             ?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ number_format($TotalLab,0,",",".") }}</b></td>
                                            <?php if( $TotalDescargosi ==0){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ number_format($TotalDescargosi,0,",",".") }} &nbsp;&nbsp;
                                            <?php }elseif( $TotalDescargosi != $TotalLab){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ number_format($TotalDescargosi,0,",",".") }} &nbsp;&nbsp;
                                            <?php }elseif( $TotalDescargosi == $TotalLab){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ number_format($TotalDescargosi,0,",",".") }} &nbsp;&nbsp;        
                                            <?php } ?>
                                             ({{ $porcedesca }}) %</b></td>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ number_format($TotalDescargono,0,",",".") }}  </b></td> 
                                             <?php if( $TotalInstalosi ==0){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ number_format($TotalInstalosi,0,",",".") }} &nbsp;&nbsp;
                                            <?php }elseif( $TotalInstalosi != $TotalLab){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ number_format($TotalInstalosi,0,",",".") }} &nbsp;&nbsp;
                                            <?php }elseif(  $TotalInstalosi == $TotalLab){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ number_format($TotalInstalosi,0,",",".") }} &nbsp;&nbsp;        
                                            <?php } ?>        
                                                    ({{ $porceinstal }}) %</b></td>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ number_format($TotalInstalono,0,",",".") }} </b></td>
                                             <?php if( $TotalSesionsi==0 ){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ number_format($TotalSesionsi,0,",",".") }} &nbsp;&nbsp;
                                            <?php }elseif( $TotalSesionsi != $TotalLab){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ number_format($TotalSesionsi,0,",",".") }} &nbsp;&nbsp;
                                            <?php }elseif( $TotalSesionsi == $TotalLab){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ number_format($TotalSesionsi,0,",",".") }} &nbsp;&nbsp;        
                                            <?php } ?>         
                                            ({{ $porcesesio }}) %</b></td>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>{{ number_format($TotalSesionno,0,",",".") }} </b></td>
                                            <?php if( $TotalContactado ==0){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ number_format($TotalContactado,0,",",".") }} ) {{ $porcecontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalContactado != $TotalLab){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ number_format($TotalContactado,0,",",".") }} ) {{ $porcecontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalContactado == $TotalLab){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ number_format($TotalContactado,0,",",".") }} ) {{ $porcecontac }}% &nbsp;&nbsp;        
                                            <?php } ?>
                                            <?php if( $TotalEnProceso ==0){?>
                                            <td align="right" style="background-color: #0489B1; color: white; padding-right: 5px;"><b> {{ number_format($TotalEnProceso,0,",",".") }}
                                            <?php }elseif( $TotalEnProceso != $TotalLab){?>
                                            <td align="right" style="background-color: #0489B1; color: white; padding-right: 5px;"><b> {{ number_format($TotalEnProceso,0,",",".") }}
                                            <?php }elseif( $TotalEnProceso == $TotalLab){?>
                                            <td align="right" style="background-color: #0489B1; color: white; padding-right: 5px;"><b> {{ number_format($TotalEnProceso,0,",",".") }}
                                            <?php } ?> 
                                            <?php if( $TotalNoContactado ==0 && $porcecontac == '100,00'){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ number_format($TotalNoContactado,0,",",".") }} ) {{ $porcenocontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalNoContactado > 0 && $porcecontac != '100,00'){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ number_format($TotalNoContactado,0,",",".") }} ) {{ $porcenocontac }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalNoContactado == 0 && $porcecontac != '100,00'){?>
                                            <td align="right" style="background-color: #0489B1; color: white;"><b>( {{ number_format($TotalNoContactado,0,",",".") }} ) {{ $porcenocontac }}% &nbsp;&nbsp;        
                                            <?php } ?>        
                                                   
                                            <?php /* if( $TotalNovedad == $TotalNoContactado){?>
                                            <td align="right" style="background-color: #5FB404; color: white;"><b>( {{ $TotalNovedad }} ) {{ $porcenoved }}% &nbsp;&nbsp;
                                            <?php }elseif( $TotalNovedad != $TotalNoContactado){?>
                                            <td align="right" style="background-color: #D7DF01; color: #000000;"><b>( {{ $TotalNovedad }} ) {{ $porcenoved }}% &nbsp;&nbsp;      
                                            <?php } */ ?>        
                                            
                                        </tr>
                                        <tr>
                                           <td colspan="12"> 
                                              <!-- {!! Form::submit('Para consulta de Distrito por Zona hacer Click aqui', ['class' => 'btn btn-block btn-primary', 'onclick' => 'alert(hola);']) !!} -->
                                              <button class="btn btn-block btn-info" type="submit" id="enviar_zona" style="display: none;" ><b>Para consulta de Zona por Provincia hacer Click aqui</b></button>
                                           </td>
                                       </tr>
                                    </table>
                                   {!! Form::close() !!}
                                </div>
                            </div>
                            </div>       
                            </div>
                          <div class=" row zona" style="display: block;">
                           <div class="col-lg-6 col-md-12" >
                            <div class="box">
                            <div class="box-header with-border" style="padding: 0px;">
                                <h5>&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-bar-chart"></i>&nbsp; <b> Descarga por Zona</b></h5>                             
                            </div>
                            <div id="area_descargo">
                
                            </div>
                            </div>    
                          </div> 
                           <div class="col-lg-6 col-md-12" >
                            <div class="box">
                            <div class="box-header with-border" style="padding: 0px;">
                                <h5>&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-bar-chart"></i>&nbsp; <b> Instalación por Zona</b></h5>                             
                            </div>
                            <div id="area_instalo">
                
                            </div>
                            </div>    
                          </div>
                         </div>
                         <div class=" row zona" style="display: block;">
                           <div class="col-lg-6 col-md-12" >
                            <div class="box">
                            <div class="box-header with-border" style="padding: 0px;">
                                <h5>&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-bar-chart"></i>&nbsp; <b> Sesión por Zona</b></h5>                             
                            </div>
                            <div id="area_sesion">
                
                            </div>
                            </div>    
                          </div> 
                           <div class="col-lg-6 col-md-12" >
                            <div class="box">
                            <div class="box-header with-border" style="padding: 0px;">
                                <h5>&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-bar-chart"></i>&nbsp; <b> Estado de Llamada por Zona</b></h5>                             
                            </div>
                            <div id="area_llamada">
                
                            </div>
                            </div>    
                          </div>
     </div>
                                                 <!-- Reporte Provincia-->
                                                 <div class="row provincia" style="display: none;">                       
                                                     <div id="provincia" class="col-lg-7 col-sm-12" style=" width: auto;">                            
                                                     </div>
                                                 </div>                     
                                                 <!-- Reporte Distrito-->                    
                                                 <div class="row distritod" style="display: none;">                        
                                                     <div id="distrito" class="col-lg-12 col-sm-12">

                                                     </div>
                                                 </div> 
                                                 <!-- Reporte Monitor--> 
                                                 <div class="row monitord" style="display: none;">
                                                     <div id="monitor" class="col-sm-12">
                                                     </div>
                                                 </div>
                                                 <!-- Reporte Laboratorios-->
                                                 <div class="row labmonitores" style="display: none;"> 
                                                     <div id="laboratorio" class="col-lg-7 col-sm-12" style=" width: auto;">
                                                     </div>
                                                 </div>                                                 
                                                 
   <script type="text/javascript"> 
  google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ['Zona', 'Descargó', 'No Descargó'],
        @foreach( $monitores as $grafmonitores )
        ['Zona {{ $grafmonitores->zona }}', {{ $grafmonitores->descargasi }}, {{ $grafmonitores->descargano }}],
        @endforeach
      ]);

      var options = {
        isStacked: 'percent',
        height: 350,
        legend: { position: 'top', maxLines: 3 },
        vAxis: {
            minValue: 0,
            ticks: [0, .3, .6, .8, 1]
        }
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("area_descargo"));
      chart.draw(data, options);
  }
  
  
    google.charts.setOnLoadCallback(drawChartd);
    function drawChartd() {
      var datad = google.visualization.arrayToDataTable([
        ['Zona', 'Instaló', 'No Instaló'],
        @foreach( $monitores as $grafmonitins )
        ['Zona {{ $grafmonitins->zona }}', {{ $grafmonitins->instalosi }}, {{ $grafmonitins->instalono }}],
        @endforeach
      ]);

      var optionsd = {
        isStacked: 'percent',
        height: 350,
        legend: { position: 'top', maxLines: 3 },
        vAxis: {
            minValue: 0,
            ticks: [0, .3, .6, .8, 1]
        }
      };
      var chartd = new google.visualization.ColumnChart(document.getElementById("area_instalo"));
      chartd.draw(datad, optionsd);
  }
  
    google.charts.setOnLoadCallback(drawCharts);
    function drawCharts() {
      var datas = google.visualization.arrayToDataTable([
        ['Zona', 'Inició Sesión', 'No Inició Sesión'],
        @foreach( $monitores as $grafmonitses )
        ['Zona {{ $grafmonitses->zona }}', {{ $grafmonitses->sesionsi }}, {{ $grafmonitses->sesionno }}],
        @endforeach
      ]);

      var optionss = {
        isStacked: 'percent',
        height: 350,
        legend: { position: 'top', maxLines: 3 },
        vAxis: {
            minValue: 0,
            ticks: [0, .3, .6, .8, 1]
        }
      };
      var charts = new google.visualization.ColumnChart(document.getElementById("area_sesion"));
      charts.draw(datas, optionss);
  }
  
  
    google.charts.setOnLoadCallback(drawChartll);
    function drawChartll() {
      var datall = google.visualization.arrayToDataTable([
        ['Zona', 'Contactado', 'Por Contactar', 'No Contactado'],
        @foreach( $monitores as $grafmonitllam )
        ['Zona {{ $grafmonitllam->zona }}', {{ $grafmonitllam->contactado }}, {{ $grafmonitllam->enproceso }}, {{ $grafmonitllam->nocontactado }}],
        @endforeach
      ]);

      var optionsll = {
        isStacked: 'percent',
        height: 350,
        legend: { position: 'top', maxLines: 3 },
        vAxis: {
            minValue: 0,
            ticks: [0, .3, .6, .8, 1]
        }
      };
      var chartll = new google.visualization.ColumnChart(document.getElementById("area_llamada"));
      chartll.draw(datall, optionsll);
  }

</script>