<script>
    $(function () {
        $(".azona").click(function () {
            $(".zona").css("display", "block");
            $(".provincia").css("display", "none");
            $(".distritod").css("display", "none");
            $(".monitord").css("display", "none");
            $(".labmonitores").css("display", "none");
            $(".sustentantes").css("display", "none");
        });
    });

    $(function () {
        $(".aprovincia").click(function () {
            $(".provincia").css("display", "block");
            $(".zona").css("display", "none");
            $(".distritod").css("display", "none");
            $(".monitord").css("display", "none");
            $(".labmonitores").css("display", "none");
            $(".sustentantes").css("display", "none");
        });
    });

    $(function () {
        $(".adistrito").click(function () {
            $(".provincia").css("display", "none");
            $(".zona").css("display", "none");
            $(".distritod").css("display", "block");
            $(".monitord").css("display", "none");
            $(".labmonitores").css("display", "none");
            $(".sustentantes").css("display", "none");
        });
    });

    $(function () {
        $(".amonitor").click(function () {
            $(".provincia").css("display", "none");
            $(".zona").css("display", "none");
            $(".distritod").css("display", "none");
            $(".monitord").css("display", "block");
            $(".labmonitores").css("display", "none");
            $(".sustentantes").css("display", "none");
        });
    });
    
    $(function () {
        $(".alaboratorio").click(function () {
            $(".provincia").css("display", "none");
            $(".zona").css("display", "none");
            $(".distritod").css("display", "none");
            $(".monitord").css("display", "none");
            $(".labmonitores").css("display", "block");
            $(".sustentantes").css("display", "none");
        });
    });
</script>

<ol class="breadcrumb">
    <li><a href="#" class="azona">Zonas</a></li>
    <li><a href="#" class="aprovincia">Provincias</a></li>
    <li><a href="#" class="adistrito">Distritos</a></li>
    <li><a href="#" class="amonitor">Monitores</a></li>
    <li><a href="#" class="alaboratorio">Laboratorios</a></li>
    <li class="active">Sustentantes</li>
</ol>
<div class="box table-responsive no-padding " style="width: auto;">
    <div class="box-body">
        <table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 90%;">
            <tr style="background-color: #0489B1; color: white;">  
            <h4> <th style="text-align: center; vertical-align: middle;" colspan="13"><b>REPORTE DE SUSTENTANTE POR LABORATORIO</b></th></h4>
            </tr>
            <tr style="background-color: #0489B1; color: white;">  
                <th style="text-align: center;  vertical-align: middle; padding: 3px;">Nº</th>
                <th style="text-align:center;  vertical-align: middle; padding: 3px;">LABORATORIO</th>
                <th style="text-align:center;  vertical-align: middle; padding: 3px;">CÓDIGO SUSTENTANTES</th>
                <th style="text-align:center;  vertical-align: middle; padding: 3px;">CÉDULA</th>
                <th style="text-align: center;  vertical-align: middle; padding: 3px;">APELLIDOS</th>
                <th style="text-align: center;  vertical-align: middle; padding: 3px;">NOMBRES</th>
                <th style="text-align: center;  vertical-align: middle; padding: 3px;">REPROGRAMACIÓN</th>
                <th style="text-align: center;  vertical-align: middle; padding: 3px;">ASISTENCIA</th>
                <th style="text-align: center;  vertical-align: middle; padding: 3px;">ASIGNATURA</th>                                            
                <th style="text-align:center;  vertical-align: middle; padding: 3px;">ESTADO CARGA</th>
                <th style="text-align:center;  vertical-align: middle; padding: 3px;">FECHA EVALUACIÓN</th>
                <th style="text-align:center;  vertical-align: middle; padding: 3px">SESIÓN EVALUACIÓN</th>                                        
                <th style="text-align: center;  vertical-align: middle; padding: 3px;">OBSERVACIÓN</th>
            </tr>                                   
            <?php $i=0; ?>
                                        @foreach( $sustentantes as $sustentantes)
             <?php $i++; ?>
                                        <tr>
                                            <td style="text-align: center; vertical-align: middle; padding: 0px;"> <b> {{ $i }} </b> </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 0px;"> <b> {{ $sustentantes->cgi_laboratorio_id }} <b> </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 0px;"> {{ $sustentantes->codigo_sustentante }} </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 0px;"> {{ $sustentantes->cedula }} </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 0px;"> {{ $sustentantes->apellidos }} </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 0px;"> {{ $sustentantes->nombres }} </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 0px;"> {{ $sustentantes->reprogramacion }} </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 0px;"> 
                                            <span class="<?php if($sustentantes->asistencia == '1'){echo "label label-success";}else{echo "label label-danger";} ?>">   
                                              @if($sustentantes->asistencia == '1') SI @else NO @endif
                                            </span> 
                                            </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 0px;"> {{ $sustentantes->asignatura }} </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 0px;"> 
                                            <span class="<?php if($sustentantes->estado_carga != 'SIN CARGA'){echo "label label-success";}else{echo "label label-danger";} ?>">   
                                              @if($sustentantes->estado_carga == 'SIN CARGA') {{ $sustentantes->estado_carga }} @else CON CARGA @endif
                                            </span>     
                                            </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 0px;"> {{ $sustentantes->fecha_evaluacion }} </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 0px;"> {{ $sustentantes->sesion_evaluacion }} </td>
                                            <td style="text-align: center; vertical-align: middle; padding: 0px;"> {{ $sustentantes->observacion }} </td>
                                        </tr>
                                        @endforeach

        </table>
    </div>
</div>   