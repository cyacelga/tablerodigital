<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>AdminLTE 2 | Dashboard</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons 
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
        <!-- Morris chart -->
        <link rel="stylesheet" href="plugins/morris/morris.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <!-- Date Picker -->
        <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
        <!-- Notificaciones -->
        <link rel="stylesheet" href="css/notifit.min.css">
       
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>    
  <style type='text/css'>
		#loading{
		width:100%;
		height:100%;
                background-color:#ffffff;
		position:fixed;
		top:0;
		left:0;
		z-index:9999;
		opacity: 0.8;
		filter: alpha(opacity=80);
		}
		</style>
    <script src="js/jquery.min.js"></script>
    <!-- <script type="text/javascript" src="js/loader.js"></script> -->
    <script type="text/javascript" src="js/loader.js"></script>
    <script src="js/highcharts.js"></script>
    <script src="js/exporting.js"></script>
     
    <script type="text/javascript">
              
        function buscar_datos() {
                       
            var id_periodo = $("#periodo").val();
            var fecha = $("#fecha_programada").val();
            var sesion = $("#sesion").val();
            var tp_reporte = $("#tp_reporte").val();
            var coordinador = $("#coordinador").val();
                                        
            if (id_periodo === "" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "0") {
                //alert("Debe seleccionar un periodo");
                            notif({
                            msg: 'Debe Seleccionar un Periodo !',
                            type: 'warning',
                            opacity: 1,
                            });
            } else {
                $("#loading").css("display","block");
                if (id_periodo !== "" && fecha === "" && sesion === "") {
                    var url = "reporte_aplicacion_borrar/" + tp_reporte + "/" + id_periodo + "/" + coordinador + "";
                } else {
                    if (id_periodo !== "" && fecha !== "" && sesion === "") {
                        var url = "reporte_aplicacion_borrar/" + tp_reporte + "/" + id_periodo + "/" + coordinador +  "/" + fecha + "";
                    } else {
                        var url = "reporte_aplicacion_borrar/" + tp_reporte + "/" + id_periodo + "/" + coordinador +  "/" + fecha + "/" + sesion + "";
                    }
                }  
                $("#contenido").html();
                $.get(url, function(resul) {
                    $("#contenido").html(resul);
                    // dialogo.dialog("close");
                    $("#loading").css("display","none")
                });
            }            
           }
        function mostrarOpcion(){
            var tp_reporte = $("#tp_reporte").val();
            
            if(tp_reporte !==''){
               $("#contenido").html('');
               $('#seleccion').css('display','block');
            }else{
               $('#seleccion').css('display','none');
            }
        }        
                           
    </script>

    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="index2.html" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>A</b>LT</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>Admin</b>LTE</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- Messages: style can be found in dropdown.less-->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="dist/img/LOGO.png" class="user-image" alt="User Image">
                                    <span class="hidden-xs">Ineval</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="dist/img/LOGO.png" class="img-bordered" alt="User Image">
                                        <p>
                                            Ineval
                                            <small>Enero. 2016</small>
                                        </p>
                                    </li>
                                    <!-- Menu Body -->
                                    <li class="user-body">
                                        <div class="col-xs-4 text-center">
                                            <a href="#">Followers</a>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <a href="#">Sales</a>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <a href="#">Friends</a>
                                        </div>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="#" class="btn btn-default btn-flat">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="#" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="dist/img/LOGO.png" class="img-rounded" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p>Ineval</p>
                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->

                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content style="position: fixed; z-index: 100;" -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
               
                <div>
            <!-- Copiar solo el <section class="content-header"> Hasta el  </section>  Al momento de subir al servidor -->
                <section class="content-header"> 
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <select class="form-control" id="tp_reporte" onchange="mostrarOpcion();" >
                                <option value=""> Tipo de Reporte </option>
                               <!-- <option value="llamadas_aplicadores">Llamadas a Aplicadores</option> -->
                               <option value="inicio_aplicacion">Descarga, Instalación e Inicio de Sesi&oacute;n</option>
                                <option value="fin_aplicacion">Cargas de Archivos</option>
                            </select>
                        </div>
                    </div><br>
                    <div class="row" id="seleccion" style="display: none;">
                        <div class="col-sm-12 col-md-3">
                            <select class="form-control" id="periodo">
                                <option value="0"> Seleccione Periodo </option>
                                @foreach($periodos as $periodo)
                                <option value="{{ $periodo->cgi_periodo_id }}">{{ $periodo->name_periodo }}</option>
                                @endforeach
                            </select>
                        </div>                        
                        <div class="col-sm-12 col-md-3">
                            <select class="form-control" id="fecha_programada" name="fecha_programada">
                                <option value="">Seleccione Fecha</option>
                            </select>                                                        
                        </div>
                        <div class="col-sm-12 col-md-2">
                            <select class="form-control" id="sesion">
                                <option value="">Seleccione Sesion</option>
                            </select>
                        </div> 
                        <div class="col-sm-12 col-md-2">
                            <select class="form-control" id="coordinador">
                                <option value="">Coordinador</option>
                            </select>
                        </div>
                        <div class="col-sm-12 col-md-2">
                            <button class="btn btn-block btn-info" type="button" onclick="buscar_datos();"><b>Consultar</b></button>
                        </div>
                    </div>

                </section>                     
            <!-- Copiar solo el <section class="content-header"> Hasta el  </section>  Al momento de subir al servidor -->
                </div>
                <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
                <!-- Main content -->
                   
                <div class="row" id="load" style="display: none;">
                        <img src="images/loading1.gif" style='margin:0 auto; position: absolute; top: 50%; left: 95%; margin: -230px 0 0 -30px;'>
                    </div>
                 <div class="row" id="loading" style="display: none;">
                        <img src="images/loading0.gif" style='margin:0 auto; position: absolute; top: 50%; left: 50%; margin: -30px 0 0 -30px;'>
                    </div>

                <section id="contenido" class="content">
                    
                   
                </section><!-- /.content -->



            </div>      

        </div>   



        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 2.3.0
            </div>
            <strong>Copyright &copy; 2014-2015 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights reserved.
        </footer>

        <!-- Control Sidebar -->
        <!-- /.control-sidebar -->
        <!-- Add the sidebar's background. This div must be placed
             immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
    <script src="js/selectCoord.js"></script> <!-- consulta de los select -->
    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script type="text/javascript" src="http://code.jquery.com/ui/jquery-ui-git.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
                            $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="plugins/morris/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="plugins/knob/jquery.knob.js"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
   <!-- <script src="dist/js/pages/dashboard.js"></script> -->
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <script src="js/notifit.min.js"></script>
</body>
</html>
