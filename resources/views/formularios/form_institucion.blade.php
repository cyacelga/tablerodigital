<script>
$(".grado").attr('readonly', true);
$("#cuarto_egb").change(function() {
    var cuarto= $("#cuarto_egb").val();
    if(cuarto !== 'SI'){$("#cuarto_egb_n").attr('readonly', true); }else{ $("#cuarto_egb_n").attr('readonly', false); }
});

$("#septimo_egb").change(function() {
    var septimo= $("#septimo_egb").val();
    if(septimo !== 'SI'){$("#septimo_egb_n").attr('readonly', true); }else{ $("#septimo_egb_n").attr('readonly', false); }
});

$("#decimo_egb").change(function() {
    var decimo= $("#decimo_egb").val();
    if(decimo !== 'SI'){$("#decimo_egb_n").attr('readonly', true); }else{ $("#decimo_egb_n").attr('readonly', false); }
});

$("#tercero_bgu").change(function() {
    var tercero= $("#tercero_bgu").val();
    if(tercero !== 'SI'){$("#tercero_bgu_n").attr('readonly', true); }else{ $("#tercero_bgu_n").attr('readonly', false); }
});
</script>

<script type="text/javascript">
    $('#estadollamada').on('change', function(e) {
        let $novedadLlamada = $('#novedadllamada option:selected').val();
        if(e.target.value === 'NO CONTACTADO' && $novedadLlamada === 'VOLVER A LLAMAR') {
            alert('No se puede guardar la novedad "VOLVER A LLAMAR" con el estado "NO CONTACTADO');
            $('#estadollamada').val('');
        }
    });
    $('#novedadllamada').on('change', function(e) {
        let $estadoLlamada = $('#estadollamada option:selected').val();
        if(e.target.value === 'VOLVER A LLAMAR' && $estadoLlamada === 'NO CONTACTADO') {
            alert('No se puede guardar la novedad "VOLVER A LLAMAR" con el estado "NO CONTACTADO');
            $('#novedadllamada').val('');
        }
    });
</script>

<div class="row">  

  
       
        <div id="notificacion_resul_feu"></div>



        <form  id="f_editar_rector"  method="post"  action="editar_rector" class="form-horizontal form_entrada" >   
        <div class="col-md-6">

        <div class="box box-primary">
                        
                        <div class="box-header">
                          <h3 class="box-title">Información de la institución Educativa</h3>
                        </div><!-- /.box-header -->             
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"> 
          <!--id oculto-->              

         <input type="hidden" name="id10" id="id10" value="<?= $id; ?>"> 
        <div class="box-body ">
        <div class="form-group col-xs-12">
                              <div class="row">
                              <div class="col-xs-6">
                              <label for="nombresr">Nombre del responsable de sede</label>
                              <input type="text" class="form-control" id="nombresr" name="nombresr"  value="{{$institucion->rector}}"  >
                              </div>
                              <div class="col-xs-6">
                              <label for="numero_id_rector">Número ID</label>
                              <input type="text" class="form-control" id="numero_id_rector" name="numero_id_rector"  value="{{$institucion->numero_id_rector}}"  >
                              </div>
                              </div>
        </div>
        <div class="form-group col-xs-8">
                              <div class="row">
                              <div class="col-xs-8">
                              <label for="nombresr">Celular 1</label>
                              <input type="text" class="form-control" id="celular" name="celular"  value="{{$institucion->celular}}"  >
                              </div>
                               <div class="col-xs-4">
                               <label for="celular1_wa">Whatsapp 1</label>
                              {{ Form::checkbox('celular1_wa', $institucion->celular1_wa, $institucion->celular1_wa, ['id' => 'toggleden' , 'type' => 'checkbox', 'class' => 'toggleden', 'href' => 'AsistTxt' . $institucion->id ]) }}
                              </div>
                              </div>
        </div>
        <div class="form-group col-xs-8">
                              <div class="row">
                              <div class="col-xs-8">
                              <label for="tele1">Celular 2</label>
                              <input type="text" class="form-control" id="celularw" name="celularw"  value="{{$institucion->celular_whatsapp}}"  >
                              </div>
                              <div class="col-xs-4">
                               <label for="celular2_wa">Whatsapp 2</label>
                              {{ Form::checkbox('celular2_wa', $institucion->celular2_wa, $institucion->celular2_wa, ['id' => 'toggleden' , 'type' => 'checkbox', 'class' => 'toggleden', 'href' => 'AsistTxt' . $institucion->id ]) }}
                              </div>
                              </div>
        </div>
       
        <div class="form-group col-xs-12">
                               <div class="row">
                              <div class="col-xs-3">
                             <label for="tele1">Teléfono fijo 1</label>
                              <input type="text" class="form-control" id="telefono1" name="telefono1"  value="{{$institucion->telefono1}}"  >
                             </div>
                             <div class="col-xs-3">
                             <label for="tele1">Teléfono fijo 2</label>
                              <input type="text" class="form-control" id="telefono2" name="telefono2"  value="{{$institucion->telefono2}}"  >
                             </div>
                             <div class="col-xs-3">
                            <label for="tele1">Teléfono fijo 3 (opcional)</label>
                            <input type="text" class="form-control" id="tele1" name="tele1"  value="{{$institucion->contactoinstitucion}}"  >
                             </div>
                              <div class="col-xs-3">
                            <label for="telefono_contactado">Teléfono Contactado</label>
                              <input type="text" class="form-control" id="telefono_contactado" name="telefono_contactado"  value="{{$institucion->telefono_contactado}}"  >
                             </div>
                             </div>
        
        </div>
         <div class="form-group col-xs-12">
                              <div class="row">
                              <div class="col-xs-6">
                              <label for="emailr">Correo electrónico 1</label>
                              <input type="text" class="form-control" id="emailr" name="emailr"   value="{{$institucion->correoinstitucion}}" >
                              </div>
                              <div class="col-xs-6">
                              <label for="tele1">Correo electrónico 2</label>
                              <input type="text" class="form-control" id="correo" name="correo"  value="{{$institucion->correo}}"  >
                              </div>
                              </div>
        </div>
         <div class="form-group col-xs-12">
                              <div class="row">
                                  <div class="col-xs-6" hidden>
                              <label for="motivo1">Motivo de inasistencia</label>
                              <select id="motivo1" value = "<?= $institucion->motivo1; ?>" name="motivo1" class="form-control">
                               <option value="SIN MOTIVO" > Seleccione </option>
                                <?php foreach($subCategoriaetiqueta as $tipo){  ?>                                   
                                   <option value="<?= $tipo->descripcion_cat; ?>" <?php if($institucion->motivo1==$tipo->descripcion_cat){echo "selected";}?>> <?= $tipo->descripcion_cat; ?> </option>
                                <?php } ?>
                               </select>
                               </div>
                               <div class="col-xs-6">
                                <label for="tele1">Comunicación rápida</label>
                              <select id="comunicacion" name="comunicacion" value="{{$institucion->comunicacion}}" class="form-control">
                                   <option value="" > SELECCIONE </option>
                                   <option value="WHATSAPP" <?php if($institucion->comunicacion=="WHATSAPP"){echo "selected";}?>> WHATSAPP </option>
                                   <option value="email" <?php if($institucion->comunicacion=="email"){echo "selected";}?>> CORREO ELECTRóNICO </option>
                                   <option value="msm" <?php if($institucion->comunicacion=="msm"){echo "selected";}?>> MENSAJE DE TEXTO </option>
                               </select>
                               </div>
                               </div>
        
        </div>
        </div>
     
  </div> <!-- end col mod 6 -->
  </div>
            <div class="col-md-6" hidden>

        <div class="box box-primary">
                        
                        <div class="box-header">
                          <h3 class="box-title">Información del rector 2</h3>
                        </div><!-- /.box-header -->             
          
          <!--id oculto--> 
        <div class="box-body ">
        <div class="form-group col-xs-12">
                              <label for="nombresrs">Nombre del rector</label>
                              <input type="text" class="form-control" id="nombresrs" name="nombresrs"  value="{{$institucion->rector_suplente}}"  >
        </div>
        <div class="form-group col-xs-8">
                               <div class="row">
                               <div class="col-xs-8">
                              <label for="celulars">Celular 1</label>
                              <input type="text" class="form-control" id="celulars" name="celulars"  value="{{$institucion->celular_suplente}}"  >
                               </div>
                               <div class="col-xs-4">
                               <label for="celular1_swa">Whatsapp 1</label>
                              {{ Form::checkbox('celular1_swa', $institucion->celular1_swa, $institucion->celular1_swa, ['id' => 'toggleden' , 'type' => 'checkbox', 'class' => 'toggleden', 'href' => 'AsistTxt' . $institucion->id ]) }}
                              </div>
                               </div>
        </div>
        <div class="form-group col-xs-8">
                             <div class="row">
                             <div class="col-xs-8"> 
                              <label for="celularws">Celular 2</label>
                              <input type="text" class="form-control" id="celularws" name="celularws"  value="{{$institucion->celular_whatsapp_suplente}}"  >
                              </div>
                              <div class="col-xs-4">
                               <label for="celular2_swa">Whatsapp 2</label>
                              {{ Form::checkbox('celular2_swa', $institucion->celular2_swa, $institucion->celular2_swa, ['id' => 'toggleden' , 'type' => 'checkbox', 'class' => 'toggleden', 'href' => 'AsistTxt' . $institucion->id ]) }}
                              </div>
                              </div>
        </div>
       
        <div class="form-group col-xs-12">
                              <div class="row">
                              <div class="col-xs-4">
                              <label for="telefono1s">Teléfono fijo 1</label>
                              <input type="text" class="form-control" id="telefono1s" name="telefono1s"  value="{{$institucion->telefono1_suplente}}"  >
                              </div>
                              <div class="col-xs-4">
                              <label for="telefono2s">Teléfono fijo 2</label>
                              <input type="text" class="form-control" id="telefono2s" name="telefono2s"  value="{{$institucion->telefono2_suplente}}"  >
                              </div>
                              <div class="col-xs-4">
                              <label for="tele1s">Teléfono fijo 3 (opcional)</label>
                              <input type="text" class="form-control" id="tele1s" name="tele1s"  value="{{$institucion->contactoinstitucion_suplente}}"  > 
                              </div>
                              </div>
        
        </div>
         <div class="form-group col-xs-12">
                              <div class="row">
                              <div class="col-xs-6">
                              <label for="emailrs">Correo electrónico 1</label>
                              <input type="text" class="form-control" id="emailrs" name="emailrs"   value="{{$institucion->correoinstitucion_suplente}}" >
                              </div>
                              <div class="col-xs-6">
                              <label for="correos">Correo electrónico 2</label>
                              <input type="email" class="form-control" id="correos" name="correos"  value="{{$institucion->correo_suplente}}"  >
                              </div>  
                              </div>
        </div>
        <div class="form-group col-xs-12">
                              <div class="row">
                              <div class="col-xs-6">  
                              <label for="motivo2">Motivo de inasistencia</label>
                              <select id="" value = "<?= $institucion->motivo2; ?>" name="motivo2" class="form-control">
                               <option value="SIN MOTIVO" > Seleccione </option>
                                <?php foreach($subCategoriaetiqueta as $tipo){  ?>
                                   <option value="<?= $tipo->descripcion_cat; ?>" <?php if($institucion->motivo2==$tipo->descripcion_cat){echo "selected";}?>> <?= $tipo->descripcion_cat; ?> </option>
                                <?php } ?>                                
                               </select>
                               </div>
                               <div class="col-xs-6">
                                <label for="comunicacion_suplente">Comunicación rápida</label>
                              <select id="comunicacions" name="comunicacions" value="{{$institucion->comunicacion_suplente}}" class="form-control">
                                   <option value="" > SELECCIONE </option>
                                   <option value="WHATSAPP" <?php if($institucion->comunicacion_suplente=="WHATSAPP"){echo "selected";}?>> WHATSAPP </option>
                                   <option value="email" <?php if($institucion->comunicacion_suplente=="email"){echo "selected";}?>> CORREO ELECTRóNICO </option>
                                   <option value="msm" <?php if($institucion->comunicacion_suplente=="msm"){echo "selected";}?>> MENSAJE DE TEXTO </option>
                               </select>
                               </div>
                               </div>
        
        </div>
        </div>

  </div> <!-- end col mod 6 -->
 </div>

<div class="col-md-6">

<div class="box box-primary" hidden>
 <div class="box-header">
                          <h3 class="box-title">Información del personal en campo</h3>
                        </div>
                        <div class="box-body "> 
        <div class="form-group col-xs-12">
                              <label for="observador">Nombres</label>
                              <h5 class="box-title"><?php echo $institucion->observador; ?> </h5>
        
        </div>
        <div class="form-group col-xs-12">
                              <label for="celular_observador">Celular</label>
                              <h5 class="box-title"><?php echo $institucion->celular_observador; ?> </h5>
        
        </div>
        <div class="form-group col-xs-12">
                              <label for="correo_observador">Correo</label>
                              <h5 class="box-title"><?php echo $institucion->correo_observador; ?> </h5>
        
        </div>
</div>
</div>

<?php foreach($umonitoreo as $aplicador){ ?>
<div class="box box-primary">
 <div class="box-header">
	<h3 class="box-title">Información Aplicador laboratorio {{ $aplicador->id_sede }} </h3>
	</div>
	<div class="box-body "> 						
	<div class="form-group col-xs-12">
						  <label for="aplicador">Nombres</label>
						  <h5 class="box-title"><?php echo $aplicador->apli_nombre1; ?> </h5>
	
	</div>
	<div class="form-group col-xs-12">
						  <label for="cedula_aplicador">Número ID</label>
						  <h5 class="box-title"><?php echo $aplicador->apli_cedula; ?> </h5>
	
	</div>
	<div class="form-group col-xs-12">
						  <label for="correo1_aplicador">Correo 1</label>
						  <h5 class="box-title"><?php echo $aplicador->apli_email; ?> </h5>
	
	</div>
	<div class="form-group col-xs-12">
						  <label for="correo2_aplicador">Correo 2</label>
						  <h5 class="box-title"><?php echo $aplicador->apli_email2; ?> </h5>
	
	</div>
	<div class="form-group col-xs-12">
						  <label for="celular1">Celular</label>
						  <h5 class="box-title"><?php echo $aplicador->apli_telef1; ?> </h5>
	
	</div>
	<div class="form-group col-xs-12">
						  <label for="celular2">Celular (opcional)</label>
						  <h5 class="box-title"><?php echo $aplicador->apli_telef2; ?> </h5>
	
	</div>
	<div class="form-group col-xs-12">
						  <label for="telefono">Teléfono fijo</label>
						  <h5 class="box-title"><?php echo $aplicador->apli_telef3; ?> </h5>
	
	</div>
	</div>
</div>
<?php } ?>

<!-- Agregado Mayreth -->
<div hidden class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Información Adicional</h3>
        </div>   
        <div class="box-body ">
            <div class="form-group col-xs-12">
                <div class="row">
                    <div class="col-xs-6">
                        <label for="cuarto_egb">TIENE 4TO EGB?</label>
                        <select id="cuarto_egb" name="cuarto_egb" class="form-control">
                            <option value="" > SELECCIONE </option>
                            <option value="SI" <?php if ($institucion->cuarto_egb == "SI") { echo "selected"; } ?>> SI </option>
                            <option value="NO" <?php if ($institucion->cuarto_egb == "NO") { echo "selected"; } ?>> NO </option>
                        </select>
                    </div>
                    <div class="col-xs-6">
                        <label for="cuarto_egb_n">Cantidad 4TO EGB</label>
                        <input type="number" class="form-control grado" id="cuarto_egb_n" name="cuarto_egb_n" min="0" max="1000" value="<?php if ($institucion->cuarto_egb_n != "") { echo $institucion->cuarto_egb_n; }else{ echo 0; } ?>"  >
                    </div>
                    <div class="col-xs-6">
                        <label for="septimo_egb">TIENE 7MO EGB?</label>
                        <select id="septimo_egb" name="septimo_egb" class="form-control">
                            <option value="" > SELECCIONE </option>
                            <option value="SI" <?php if ($institucion->septimo_egb == "SI") { echo "selected"; } ?>> SI </option>
                            <option value="NO" <?php if ($institucion->septimo_egb == "NO") { echo "selected"; } ?>> NO </option>
                        </select>
                    </div>
                    <div class="col-xs-6">
                        <label for="septimo_egb_n">Cantidad 7MO EGB</label>
                        <input type="number" class="form-control grado" id="septimo_egb_n" name="septimo_egb_n" min="0" max="1000" value="<?php if ($institucion->septimo_egb_n != "") { echo $institucion->septimo_egb_n; }else{ echo 0; } ?>"  >
                    </div>
                    <div class="col-xs-6">
                        <label for="decimo_egb">TIENE 10MO EGB?</label>
                        <select id="decimo_egb" name="decimo_egb" class="form-control">
                            <option value="" > SELECCIONE </option>
                            <option value="SI" <?php if ($institucion->decimo_egb == "SI") { echo "selected"; } ?>> SI </option>
                            <option value="NO" <?php if ($institucion->decimo_egb == "NO") { echo "selected"; } ?>> NO </option>
                        </select>
                    </div>
                    <div class="col-xs-6">
                        <label for="decimo_egb_n">Cantidad 10MO EGB</label>
                        <input type="number" class="form-control grado" id="decimo_egb_n" name="decimo_egb_nu" min="0" max="1000" value="{{$institucion->decimo_egb_n}}"  >  
                    </div>
                    <div class="col-xs-6">
                        <label for="tercero_bgu">TIENE 3ERO BGU?</label>
                        <select id="tercero_bgu" name="tercero_bgu" class="form-control">
                            <option value="" > SELECCIONE </option>
                            <option value="SI" <?php if ($institucion->tercero_bgu == "SI") { echo "selected"; } ?>> SI </option>
                            <option value="NO" <?php if ($institucion->tercero_bgu == "NO") { echo "selected"; } ?>> NO </option>
                        </select>
                    </div>
                    <div class="col-xs-6">
                        <label for="tercero_bgu_n">Cantidad 3ERO BGU</label>
                        <input type="number" class="form-control grado" id="tercero_bgu_n" name="tercero_bgu_n" min="0" max="1000" value="{{$institucion->tercero_bach_n}}"  >  
                    </div>
                </div>

            </div>        
        </div>
    </div>
<!-- Fin Agregado -->
    <div hidden class="box box-primary">
 <div class="box-header">
                          <h3 class="box-title">Información de la institución</h3>
                        </div>   
                        <div class="box-body ">
        <div class="form-group col-xs-12">
                              <div class="row">
                              <div class="col-xs-6">  
                              <label for="amie_o">Amie origen</label>
                              <h5 class="box-title"><?php echo $institucion->amie; ?> </h5>
                              </div>
                              <div class="col-xs-6">
                              <label for="institucion_o">Institución origen</label>
                              <h5 class="box-title"><?php echo $institucion->institucion; ?> </h5>
                              </div>
                              </div>
        
        </div>
        <div class="form-group col-xs-12">
                              <div class="row">
                              <div class="col-xs-6">  
                              <label for="amie_actual">Amie actual</label>
                              <input type="text" class="form-control" id="amie_actual" name="amie_actual"  value="{{$institucion->amie_actual}}"  >
                              </div>
                              <div class="col-xs-6"> 
                              <label for="institucion_actual">Institución actual</label>
                              <input type="text" class="form-control" id="institucion_actual" name="institucion_actual"  value="{{$institucion->institucion_actual}}"  >  
                              </div>  
                              </div>
        
        </div>
        <div class="form-group col-xs-12">
                              <div class="row">
                              <div class="col-xs-4">
                              <label for="provincia">Provincia</label>
                              <select id="" value = "<?= $institucion->provincia; ?>" name="provincia" class="form-control">
                               <option value="" > Seleccione </option>
                                <?php foreach($provincia as $tipo){  ?>
                                   
                                   <option value="<?= $tipo->provincia; ?>" <?php if($institucion->provincia==$tipo->provincia){echo "selected";}?>> <?= $tipo->provincia; ?> </option>
                                
                                <?php } ?>
                                
                               </select>
                               </div>
                               <div class="col-xs-4">
                               <label for="canton">Cantón</label>
                              <select id="" value = "<?= $institucion->canton; ?>" name="canton" class="form-control">
                               <option value="" > Seleccione </option>
                                <?php foreach($canton as $tipo){  ?>
                                   
                                   <option value="<?= $tipo->canton; ?>" <?php if($institucion->canton==$tipo->canton){echo "selected";}?>> <?= $tipo->canton; ?> </option>
                                
                                <?php } ?>
                                
                               </select>
                               </div>
                               <div class="col-xs-4">
                               <label for="parroquia">Parroquia</label>
                              <select id="" value = "<?= $institucion->parroquia; ?>" name="parroquia" class="form-control">
                               <option value="" > Seleccione </option>
                                <?php foreach($parroquia as $tipo){  ?>
                                   
                                   <option value="<?= $tipo->parroquia; ?>" <?php if($institucion->parroquia==$tipo->parroquia){echo "selected";}?>> <?= $tipo->parroquia; ?> </option>
                                
                                <?php } ?>
                                
                               </select>
                               </div>
                               </div>
        
        </div>
        <div class="form-group col-xs-12">
                              <div class="row">
                              <div class="col-xs-4">
                              <label for="octavo_egb_n">Cantidad 8VO EGB</label>
                              <input type="text" class="form-control" id="octavo_egb_n" name="octavo_egb_n"  value="{{$institucion->octavo_egb_n}}"  >
                              </div>
                              <div class="col-xs-4">
                              <label for="noveno_egb_n">Cantidad 9NO EGB</label>
                              <input type="text" class="form-control" id="noveno_egb_n" name="noveno_egb_n"  value="{{$institucion->noveno_egb_n}}"  >
                              </div>
                              <div class="col-xs-4">
                              <label for="decimo_egb_n">Cantidad 10MO EGB</label>
                              <input type="text" class="form-control" id="decimo_egb_n" name="decimo_egb_n"  value="{{$institucion->decimo_egb_n}}"  >  
                              </div>
                              </div>
        
        </div>
        <div class="form-group col-xs-12">
                              <div class="row">
                              <div class="col-xs-4">
                              <label for="primero_bach_n">Cantidad 1ER BACHILLERATO</label>
                              <input type="text" class="form-control" id="primero_bach_n" name="primero_bach_n"  value="{{$institucion->primero_bach_n}}"  >
                              </div>
                              <div class="col-xs-4">
                              <label for="segundo_bach_n">Cantidad 2DO BACHILLERATO</label>
                              <input type="text" class="form-control" id="segundo_bach_n" name="segundo_bach_n"  value="{{$institucion->segundo_bach_n}}"  >  
                              </div>
                              <div class="col-xs-4">
                              <label for="tercero_bach_n">Cantidad 3ER BACHILLERATO</label>
                              <input type="text" class="form-control" id="tercero_bach_n" name="tercero_bach_n"  value="{{$institucion->tercero_bach_n}}"  >  
                              </div>
                              </div>
        
        </div>
        <div class="form-group col-xs-12">
                              <div class="row">
                              <div class="col-xs-6">
                              <label for="direccion">Dirección de la institución</label>
                              <input type="text" class="form-control" id="direccion" name="direccion"  value="{{$institucion->direccion}}"  >
                              </div>
                              <div class="col-xs-6">
                              <label for="sostenimiento">Sostenimiento</label>
                              <select id="" value = "<?= $institucion->sostenimiento; ?>" name="sostenimiento" class="form-control">
                               <option value="" > Seleccione </option>
                                <?php foreach($sostenimiento as $tipo){  ?>
                                   
                                   <option value="<?= $tipo->sostenimiento; ?>" <?php if($institucion->sostenimiento==$tipo->sostenimiento){echo "selected";}?>> <?= $tipo->sostenimiento; ?> </option>
                                
                                <?php } ?>
                                
                               </select>  
                              </div>
                              </div>
        
        </div>
        <div class="form-group col-xs-12">
                              <div class="row">
                              <div class="col-xs-6">
                              <label for="estado_institucion">Estado de la institución</label>
                              <select id="" value = "<?= $institucion->estado_institucion; ?>" name="estado_institucion" class="form-control">
                               <option value="" > Seleccione </option>
                                <?php foreach($subCategoriaetiquetaInstitucion as $tipo){  ?>
                                   
                                   <option value="<?= $tipo->descripcion_cat; ?>" <?php if($institucion->estado_institucion==$tipo->descripcion_cat){echo "selected";}?>> <?= $tipo->descripcion_cat; ?> </option>
                                
                                <?php } ?>
                                
                               </select>
                               </div>
                               <!-- Antes estaba el campo de observacion aqui. 'Mayreth'-->
                               </div>
        
        </div>
         <div class="form-group col-xs-12" hidden>
                              <label for="egb">TIENE EGB?</label>
                              <select id="egb" name="egb" class="form-control">
                                   <option value="" > SELECCIONE </option>
                                    <option value="SI" <?php if($institucion->egb=="SI"){echo "selected";}?>> SI </option>
                                   <option value="NO" <?php if($institucion->egb=="NO"){echo "selected";}?>> NO </option>
                               </select>
        
        </div>
        <div class="form-group col-xs-12" hidden>
                              <label for="bachillerato">TIENE BACHILLERATO?</label>
                              <select id="bachillerato" name="bachillerato" class="form-control">
                                   <option value="" > SELECCIONE </option>
                                    <option value="SI" <?php if($institucion->bachillerato=="SI"){echo "selected";}?>> SI </option>
                                   <option value="NO" <?php if($institucion->bachillerato=="NO"){echo "selected";}?>> NO </option>
                               </select>
        
        </div>
        <div class="form-group col-xs-12" hidden>
                              <label for="egb_bachillerato">TIENE EGB Y BACHILLERATO?</label>
                              <select id="egb_bachillerato" name="egb_bachillerato" class="form-control">
                                   <option value="" > SELECCIONE </option>
                                    <option value="SI" <?php if($institucion->egb_bachillerato=="SI"){echo "selected";}?>> SI </option>
                                   <option value="NO" <?php if($institucion->egb_bachillerato=="NO"){echo "selected";}?>> NO </option>
                               </select>
        
        </div>
</div>
</div>


  <div class="col-md-15">

        <div class="box box-primary">
                        
                        <div class="box-header">
                          <h3 class="box-title">Información de llamada</h3>
                        </div><!-- /.box-header -->   
                        <div class="box-body "> 
        <div class="form-group col-xs-12">
                              <label for="tele1">Estado de llamada</label>
                               <select style="<?php if($institucion->estadollamada=="CONTACTADO"){echo "color:White;background:#5FB404;width:550px;";} else {echo "color:White;background:#FA5858;width:550px;";}?>" id="estadollamada" name="estadollamada" class="form-control">
                                   <option value="" > SELECCIONE </option>
                                    <option value="CONTACTADO" <?php if($institucion->estadollamada=="CONTACTADO"){echo "selected";}?>> CONTACTADO </option>
                                   <option value="NO CONTACTADO" <?php if($institucion->estadollamada=="NO CONTACTADO"){echo "selected";}?>> NO CONTACTADO </option>
                               </select>
        
        </div>
        <div class="form-group col-xs-12">
                              <label for="tele1">Problema de llamada</label>
                              <select style="<?php if($institucion->novedadllamada=="VOLVER A LLAMAR" or $institucion->novedadllamada=="NO CONTESTA") {echo "color:White;background:#D7DF01;width:550px;";} ?>" id="novedadllamada" name="novedadllamada" class="form-control">
                                   <option value="" > SELECCIONE </option>
                                   <option value="NUMERO INCORRECTO" <?php if($institucion->novedadllamada=="NUMERO INCORRECTO"){echo "selected";}?>> NUMERO INCORRECTO </option>
                                   <option value="NUMERO EQUIVOCADO" <?php if($institucion->novedadllamada=="NUMERO EQUIVOCADO"){echo "selected";}?>> NUMERO EQUIVOCADO </option>
                                   <option value="SIN NUMERO" <?php if($institucion->novedadllamada=="SIN NUMERO"){echo "selected";}?> > SIN NUMERO </option>
                                   <option value="NO CONTESTA" <?php if($institucion->novedadllamada=="NO CONTESTA"){echo "selected";}?> > NO CONTESTA </option>
                                   <option value="VOLVER A LLAMAR" <?php if($institucion->novedadllamada=="VOLVER A LLAMAR"){echo "selected";}?> > VOLVER A LLAMAR </option>
                               </select>
        
        </div>                     
        <div hidden class="form-group col-xs-12">
                              <label for="tele1">Considerá usted que el responsable de sede colaborará? (Pregunta para el MONITOR)</label>
                              <select id="participacion" name="participacion" class="form-control">
                                   <option value="" > SELECCIONE </option>
                                    <option value="SI" <?php if($institucion->participacion=="SI"){echo "selected";}?>> SI </option>
                                   <option value="NO" <?php if($institucion->participacion=="NO"){echo "selected";}?>> NO </option>
                               </select>
        
        </div>
        
        <div class="form-group col-xs-12">
                              <label for="recibiocomunicado">Recepción de Comunicado</label>
                              <select id="recibiocomunicado" name="recibiocomunicado" class="form-control">
                                   <option value="" > SELECCIONE </option>
                                    <option value="SI" <?php if($institucion->recibiocomunicado=="SI"){echo "selected";}?>> SI </option>
                                   <option value="NO" <?php if($institucion->recibiocomunicado=="NO"){echo "selected";}?>> NO </option>
                                   <option value="REVISARA" <?php if($institucion->recibiocomunicado=="REVISARA"){echo "selected";}?>> REVISARÁ </option>
                               </select>
        
        </div>
        <div class="form-group col-xs-12">
            <label for="observacion">Observación</label>
            <input type="text" class="form-control" id="observacion" name="observacion"  value="{{$institucion->observacion}}"  >
        </div>
        <!--<div class="form-group col-xs-12">
                              <label for="descarga">Descarga</label>
                              <select id="descarga" name="descarga" class="form-control">
                                   <option value="" > SELECCIONE </option>
                                    <option value="SI" < ?php if($institucion->descarga=="SI"){echo "selected";}?>> SI </option>
                                   <option value="NO" < ?php if($institucion->descarga=="NO"){echo "selected";}?>> NO </option>
                               </select>
        
        </div>
        <div class="form-group col-xs-12">
                              <label for="instalacion">Instalación</label>
                              <select id="instalacion" name="instalacion" class="form-control">
                                   <option value="" > SELECCIONE </option>
                                    <option value="SI" < ?php if($institucion->instalacion=="SI"){echo "selected";}?>> SI </option>
                                   <option value="NO" < ?php if($institucion->instalacion=="NO"){echo "selected";}?>> NO </option>
                               </select>
        
        </div>
        <div class="form-group col-xs-12">
                              <label for="observacion_instala">Observación de instalación</label>
                              <select id="observacion_instala" value = "< ?= $institucion->observacion_instala; ?>" name="observacion_instala" class="form-control">
                               <option value="SIN PROBLEMA" > Seleccione </option>
                                < ?php foreach($subCategoriaetiquetainstlacion as $tipo){  ?>
                                   
                                   <option value="< ?= $tipo->descripcion_cat; ?>" < ?php if($institucion->observacion_instala==$tipo->descripcion_cat){echo "selected";}?>> < ?= $tipo->descripcion_cat; ?> </option>
                                
                                < ?php } ?>
                                
                               </select>
        
        </div>-->
        <div hidden class="form-group col-xs-12">
                              <label for="visita_institucion">Visitó institución</label>
                              <select id="visita_institucion" name="visita_institucion" class="form-control">
                                   <option value="" > SELECCIONE </option>
                                    <option value="SI" <?php if($institucion->visita_institucion=="SI"){echo "selected";}?>> SI </option>
                                   <option value="NO" <?php if($institucion->visita_institucion=="NO"){echo "selected";}?>> NO </option>
                               </select>
        
        </div>
        <div hidden class="form-group col-xs-12">
                              <label for="crm_institucion">Carga de información</label>
                              <select id="crm_institucion" name="crm_institucion" class="form-control">
                                   <option value="" > SELECCIONE </option>
                                    <option value="SI" <?php if($institucion->crm_institucion=="SI"){echo "selected";}?>> SI </option>
                                   <option value="NO" <?php if($institucion->crm_institucion=="NO"){echo "selected";}?>> NO </option>
                                   <option value="PROCESO" <?php if($institucion->crm_institucion=="PROCESO"){echo "selected";}?>> EN PROCESO </option>
                               </select>
        
        </div>



        <div class="box-footer">
             <button type="submit" class="btn btn-primary">Actualizar Registro</button>
        </div>
        </div>
        </div>
        </div>
        </form>
        

  

</div> <!-- end row -->

<script>
jQuery( document ).ready( function( $ ) {   
    
    $(function() {
        $('.toggleden').bootstrapToggle({
            on: 'SI',
            off: 'NO'
            
        });
    });

        $('.toggleden').on('change', function(){
            
            $( this ).each(function(){
                
                if($(this).val() === "0" || $(this).val() === "1" || $(this).val() ===""){
                
                    var valor = parseInt($(this).val());
                    ///alert(valor);
                    var inputID = $(this).attr('href');
                        console.log( "'" + inputID + "'  |  " + $(this).val());
                    
                    switch(valor) {
                    case 1:
                        $(this).attr('value', parseInt(0));
                        $(this).attr('checked', false);
                        $( 'input[id=' + inputID + ']' ).val(0);
                        break;
                    case 0:
                        $(this).attr('value', parseInt(1));
                        $(this).attr('checked', true);
                        $( 'input[id=' + inputID + ']' ).val(1);
                        break;
                    case "":
                        $(this).attr('value', parseInt(1));
                        $(this).attr('checked', true);
                        $( 'input[id=' + inputID + ']' ).val(1);
                        break;
                    default:
                        $(this).attr('value', null);
                    }
                }
                
                
               
            });
      });   
  });  
</script>
