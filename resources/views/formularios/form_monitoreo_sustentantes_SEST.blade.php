<div class="box box-primary table-responsive">

                <div class="box-header">
                  <h3 class="box-title">Listado de sustentantes</h3>
                </div>
                <div class="box-header">
                        <div class="input-group input-group-lg">
                                            <input type="text" class="form-control" id="dato_sustentantes" onkeyup="javascript:this.value=this.value.toUpperCase();">
                                            <input type="hidden"  id="aux" value="0">
                                            <span class="input-group-btn">
                                              <button class="btn btn-warning dropdown-toggle" type="button" onclick="buscarsustentantes();" >Buscar!</button>
                                            </span>
                        </div>            
                </div>

<div class="box-body">              
<?php 

if( count($listadoSustentantes) >0){
?>

<table class="table table-bordered table-striped table-hover" style=" width: auto; font-size: 100%;">
       
        <thead>
            <tr>
             <th>N°</th>
             <th>Identificación</th>
             <th>Apellidos</th>
             <th>Nombres</th>
             <!--<th>Sesión</th>-->
             <th>Asistencia</th>
             <th>Carga de archivo</th>
             <!--<th>Reprogramación</th>--><th>Reemplazo</th>
             <th>estado</th>
             <th>Observaciones</th>
             <th>Opción</th>
            </tr>
        </thead>
 
       
<tbody>
    <?php $contador = 0 ?>
@foreach($listadoSustentantes as $sustentates)
 <tr role="row" class="odd" id="<?php echo $sustentates->id; ?>">
 {{ Form::open( array('url' => 'editar_monitoreo/' . $sustentates->id , 'name' => 'editar_monitoreoo' . $sustentates->id , 'id' => 'editar_monitoreoo' . $sustentates->id))}} 
    <td class="sorting_1">{{$contador = $contador + 1}}</td>
    <td class="sorting_1">{{$sustentates->cedula}}</td>
    <td class="sorting_1">{{$sustentates->apellidos}}</td>
    <td class="sorting_1">{{$sustentates->nombres}}</td>
    <!--<td class="sorting_1">{{$sustentates->sesion}}</td>-->
    <td class="sorting_1">
        {{ Form::checkbox('asistencia', $sustentates->asistencia, $sustentates->asistencia, ['id' => 'toggleden' , 'type' => 'checkbox', 'class' => 'toggleden', 'href' => 'AsistTxt' . $sustentates->id ]) }}
    </td>
    <td class="sorting_1"><?php if($sustentates->estado=="SIN CARGA"){echo'<img src="imagenes/folder_no.png" align="middle" alt="nocargador">';}else{echo'<img src="imagenes/folder_ok.png" align="middle" alt="cargador">';}?></td>
    <td class="sorting_1">
        {{ Form::checkbox('reprogramacion', $sustentates->reprogramacion, $sustentates->reprogramacion, ['id' => 'toggleden' , 'type' => 'checkbox', 'class' => 'toggleden reemplazo'.$contador, 'onchange'=>"reemplazo($contador);" , 'href' => 'ReprograTxt' . $sustentates->id,  ]) }}
    </td>
    <td class="sorting_1">@if($sustentates->reprogramacion != 1) {{$sustentates->status}} @else Reemplazo @endif</td>
    <!--<td class="sorting_1">{{$sustentates->status}}</td>-->
    <td class="sorting_1">
        <select style="<?php if($sustentates->reprogramacion != 1){ ?> display: block; <?php }else{?> display: none; <?php } ?>" id="id_subcategoria_etiqueta" value = "<?= $sustentates->sustentante_observacion; ?>" name="id_subcategoria_etiqueta" class="form-control reemplazoSelect{{ $contador }}" onchange="reemplazoObservacion({{$contador}});">
            <option value="" > Seleccione Novedad </option>
            <?php foreach($subCategoriaetiqueta as $tipo){  ?>
            <option <?php if($sustentates->status=='EVALUADO' and $tipo->categoria_etiqueta_id==1){ } else {echo "hidden";}?> value="<?= $tipo->descripcion_cat; ?>" <?php if($sustentates->sustentante_observacion==$tipo->descripcion_cat){echo "selected";}?>> <?= $tipo->descripcion_cat; ?> </option>
            <option <?php if($sustentates->status=='REVISAR ARCHIVO' and $tipo->categoria_etiqueta_id==17){ } else {echo "hidden";}?> value="<?= $tipo->descripcion_cat; ?>" <?php if($sustentates->sustentante_observacion==$tipo->descripcion_cat){echo "selected";}?>> <?= $tipo->descripcion_cat; ?> </option>
            <option <?php if($sustentates->status=='AUSENTE' and $tipo->categoria_etiqueta_id==3){ } else {echo "hidden";}?> value="<?= $tipo->descripcion_cat; ?>" <?php if($sustentates->sustentante_observacion==$tipo->descripcion_cat){echo "selected";}?>> <?= $tipo->descripcion_cat; ?> </option>
            <option <?php if($sustentates->status=='POR CARGAR ARCHIVO' and $tipo->categoria_etiqueta_id==18){ } else {echo "hidden";}?> value="<?= $tipo->descripcion_cat; ?>" <?php if($sustentates->sustentante_observacion==$tipo->descripcion_cat){echo "selected";}?>> <?= $tipo->descripcion_cat; ?> </option>
            <option <?php if($sustentates->status=='REVISAR ASISTENCIA' and $tipo->categoria_etiqueta_id==19){ } else {echo "hidden";}?> value="<?= $tipo->descripcion_cat; ?>" <?php if($sustentates->sustentante_observacion==$tipo->descripcion_cat){echo "selected";}?>> <?= $tipo->descripcion_cat; ?> </option>
            <option <?php if($sustentates->status=='REPROGRAMAR' and $tipo->categoria_etiqueta_id==20){ } else {echo "hidden";}?> value="<?= $tipo->descripcion_cat; ?>" <?php if($sustentates->sustentante_observacion==$tipo->descripcion_cat){echo "selected";}?>> <?= $tipo->descripcion_cat; ?> </option>
           
     <?php } ?>
    </select>
        <input style="<?php if($sustentates->reprogramacion != 1){ ?> display: none; <?php }else{?> display: block; <?php } ?>" type="text" name="id_subcategoria_etiqueta" value="<?= $sustentates->sustentante_observacion; ?>" class="form-control reemplazoInput{{ $contador }}" href="<?php if($sustentates->reprogramacion != 1){ ?>0<?php }else{?>1<?php } ?>" required>
    </td>
    <td class="sorting_1">
      {{ Form::submit('Actualizar', array('id' => 'submitUpdate1' . $sustentates->id, 'href' => $sustentates->id, 'class' => 'btn btn-block btn-primary submitUpdate1'))}}  
    </td>
    {{ Form::close() }}
</tr>

@endforeach


  

    </table>



    <?php


echo str_replace('/?', '?', $listadoSustentantes->render() )  ;

}
else
{

?>


<br/><div class='rechazado'><label style='color:#FA206A'>...No se ha encontrado ningun usuario...</label>  </div> 

<?php
}

?>
</div>


<script>
jQuery( document ).ready( function( $ ) {   
    
    $(function() {
        $('.toggleden').bootstrapToggle({
            on: 'SI',
            off: 'NO'
            
        });
    });

        $('.toggleden').on('change', function(){
            
            $( this ).each(function(){
                
                if($(this).val() === "0" || $(this).val() === "1" || $(this).val() ===""){
                
                    var valor = parseInt($(this).val());
                    ///alert(valor);
                    var inputID = $(this).attr('href');
                        console.log( "'" + inputID + "'  |  " + $(this).val());
                    
                    switch(valor) {
                    case 1:
                        $(this).attr('value', parseInt(0));
                        $(this).attr('checked', false);
                        $( 'input[id=' + inputID + ']' ).val(0);
                        break;
                    case 0:
                        $(this).attr('value', parseInt(1));
                        $(this).attr('checked', true);
                        $( 'input[id=' + inputID + ']' ).val(1);
                        break;
                    case "":
                        $(this).attr('value', parseInt(1));
                        $(this).attr('checked', true);
                        $( 'input[id=' + inputID + ']' ).val(1);
                        break;
                    default:
                        $(this).attr('value', null);
                    }
                }
                
                
               
            });
      });   
  });  
</script>

<script>
  jQuery( document ).ready( function( $ ) {     


        $('.submitUpdate1').on('click', function(e){
            e.preventDefault();
            var monId = $(this).attr('href');
            ///sereal = $('#editar_monitoreoo' + monId).serialize();
            sereal =  $('#'+monId+' :input').serialize()+'&id='+monId;

             var form_data = $(this).serialize();
            var myUrl = 'editar_sustentante/' + monId;;
            var sesion = $("#myoption").val();
            
                $.ajax({
                    url: myUrl,
                    type: 'POST',
                    ///cache: false,
                    data: sereal,
                    success: function(data){
                        notif({
                        msg: data.message,
                        type: data.status,
                        width: 300,
                        opacity: 1,
                         
                        });
                        // $("#SaveAlert").alert("Registro Actualizado");
                        // $('#search_results_div').html(data); 
                    },
                    error: function(xhr, textStatus, thrownError){
                        notif({
                        msg: "<b>¡Registro no guardado!</b>",
                        type: "error"

                        });                 // $('#SaveAlert').alert('Se produjo un Error.');
                    }
                });
            ///alert(sereal);
        });
    });
</script>