

<div class="row">  

  <div class="col-md-6">

        <div class="box box-primary">
                        
                        <div class="box-header">
                          <h3 class="box-title">Información del Aplicador Titular</h3>
                        </div><!-- /.box-header -->

       



        <form  id="f_editar_aplicador"  method="post"  action="editar_aplicador" class="form-horizontal form_entrada" >                
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"> 
          <!--id oculto-->              
           <input type="hidden" name="id2" id="id2" value="<?= $id; ?>"> 

        <div class="box-body ">
        <div class="form-group col-xs-12">
                              <label for="nombres1">Nombres 1</label>
                              <input type="text" class="form-control" id="nombres1" name="nombres1" required  value="{{$aplicador->apli_nombre1}}"  >
        </div>
        <div class="form-group col-xs-12">
                              <label for="nombres2">Nombres 2</label>
                              <input type="text" class="form-control" id="nombres2" name="nombres2" required  value="{{$aplicador->apli_nombre2}}"  >
        </div>

        <div class="form-group col-xs-12">
                              <label for="apellido1">Apellido 1</label>
                              <input type="text" class="form-control" id="apellido1" name="apellido1" required  value="{{$aplicador->apli_apellido1}}"  >
        </div>

        <div class="form-group col-xs-12">
                              <label for="apellido2">Apellido 2</label>
                              <input type="text" class="form-control" id="apellido2" name="apellido2" required  value="{{$aplicador->apli_apellido2}}"  >
        </div>

        <div class="form-group col-xs-12">
                              <label for="email">Email*</label>
                              <input type="email" class="form-control" id="email" name="email" required  value="{{$aplicador->apli_email}}" >
        </div>
        <div class="form-group col-xs-12">
                              <label for="celular1">Celular 1</label>
                              <input type="text" class="form-control" id="celular1" class="form-control" data-inputmask="'mask': '(999) 999-9999'" data-mask  name="celular1" required value="{{$aplicador->apli_telef1}}"  >
        </div>
        <div class="form-group col-xs-12">
                              <label for="celular2">Celular</label>
                              <input type="text" class="form-control" id="celular2" name="celular2" required value="{{$aplicador->apli_telef2}}"  >
        </div>

        <div class="form-group col-xs-12">
                              <label for="sesion_aplicador">Asistencia</label>
                               <select id="sesion_aplicador" name="sesion_aplicador" class="form-control" required value="{{$aplicador->sesion_aplicador}}">
                                   <option value="" > SELECCIONE ASISTENCIA </option>
                                   <option value="SI" <?php if($aplicador->sesion_aplicador=="SI"){echo "selected";}?>> SI </option>
                                   <option value="NO" <?php if($aplicador->sesion_aplicador=="NO"){echo "selected";}?>> NO </option>
                               </select>
                       </div>
        <div class="form-group col-xs-12">
                              <label for="mosistencia">Motivo de inasistencia</label>
                               <select id="mosistencia" name="mosistencia" class="form-control" value="{{$aplicador->apli_motivo}}">
                                   <option value="" > SELECCIONE MOTIVO </option>
                               </select>
                       </div>
       <div class="form-group col-xs-12">
                              <label for="repcionuc">Recepción de usuario y clave</label>
                               <select id="repcionuc" name="repcionuc" class="form-control" required value="{{$aplicador->apli_recepcion}}">
                                   <option value="" > SELECCIONE </option>
                                   <option value="SI" <?php if($aplicador->apli_recepcion=="SI"){echo "selected";}?>> SI </option>
                                   <option value="NO" <?php if($aplicador->apli_recepcion=="NO"){echo "selected";}?>> NO </option>
                               </select>
                       </div>

        </div>



        <div class="box-footer">
             <button type="submit" class="btn btn-primary">Actualizar Registro</button>
        </div>
        </form>
        </div>

  </div> <!-- end col mod 6 -->


  <div class="col-md-6">

        <div class="box box-primary">
                        
                        <div class="box-header">
                          <h3 class="box-title">Información de la institución Educativa</h3>
                        </div><!-- /.box-header -->
       
        <div id="notificacion_resul_feu"></div>



        <form  id="f_editar_institucion"  method="post"  action="editar_institucion" class="form-horizontal form_entrada" >                
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"> 
          <!--id oculto-->              

         <input type="hidden" name="id5" id="id5" value="<?= $id; ?>"> 
        <div class="box-body ">
        <div class="form-group col-xs-12">
                              <label for="nombresr">Nombre del rector*</label>
                              <input type="text" class="form-control" id="nombresr" name="nombresr"  value="{{$aplicador->ie_rector}}"  >
        </div>
        <div class="form-group col-xs-12">
                              <label for="emailr">Email*</label>
                              <input type="text" class="form-control" id="emailr" name="emailr"   value="{{$aplicador->ie_correo}}" >
        </div>
        <div class="form-group col-xs-12">
                              <label for="tele1">Telefono 1</label>
                              <input type="text" class="form-control" id="tele1" name="tele1"  value="{{$aplicador->ie_telefono1}}"  >
        
        </div>
        <div class="form-group col-xs-12">
                              <label for="tele2">Telefono 2</label>
                              <input type="text" class="form-control" id="tele2" name="tele2"  value="{{$aplicador->ie_telefono2}}"  >
        
        </div>



        <div class="box-footer">
             <button type="submit" class="btn btn-primary">Actualizar Registro</button>
        </div>
        </form>
        </div>

  </div> <!-- end col mod 6 -->

  

</div> <!-- end row -->
