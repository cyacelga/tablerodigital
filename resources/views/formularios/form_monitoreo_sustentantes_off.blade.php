<div class="box box-primary table-responsive">

                <div class="box-header">
                  <h3 class="box-title">Listado de sustentantes</h3>
                </div>
                <div class="box-header">
                        <div class="input-group input-group-lg">
                                            <input type="text" class="form-control" id="dato_sustentantes" onkeyup="javascript:this.value=this.value.toUpperCase();">
                                            <input type="hidden"  id="aux" value="0">
                                            <span class="input-group-btn">
                                              <button class="btn btn-warning dropdown-toggle" type="button" onclick="buscarsustentantes();" >Buscar!</button>
                                            </span>
                        </div>            
                </div>

<div class="box-body">              
<?php 

if( count($listadoSustentantes) >0){
?>

<table id="tabla_pacientes" Style="width:auto" class="table table-bordered table-striped dataTable" cellspacing="0" >
       
        <thead>
            <tr>
             <th>N°</th>
             <th>Indentificación</th>
             <th>Apellidos</th>
             <th>Nombres</th>
             <!--<th>Sesión</th>-->
             <th>Actor</th>
             <!--<th>Asistencia Pisa</th>--><th>Confirmación</th>
             <th>Observación</th>
<!--             <th>Reprogramación Pisa</th>
             <th>Asistencia Contexto</th>
             <th>Reprogramación Contexto</th>-->
             <th>Opción</th>
            </tr>
        </thead>
 
       
<tbody>
   <?php $contador = 0 ?>
@foreach($listadoSustentantes as $sustentates)
 <tr role="row" class="odd" id="<?php echo $sustentates->id; ?>">
 {{ Form::open( array('url' => 'editar_monitoreo/' . $sustentates->id , 'name' => 'editar_monitoreoo' . $sustentates->id , 'id' => 'editar_monitoreoo' . $sustentates->id))}} 
    <td class="sorting_1">{{$contador = $contador + 1}}</td>
    <td class="sorting_1">{{$sustentates->identificacion}}</td>
    <td class="sorting_1">{{$sustentates->apellidos}}</td>
    <td class="sorting_1">{{$sustentates->nombres}}</td>
    <!--<td class="sorting_1">{{$sustentates->sesion_evaluacion}}</td>-->
    <td class="sorting_1">
        <select id="asistencia" name="cargo" class="form-control"> value="<?= $sustentates->cargo; ?>">
                                   <option value="" > Seleccione Actor </option>
                                   <option value="Rector" <?php if($sustentates->cargo=="Rector"){echo "selected";}?>> Rector </option>
                                   <option value="Vicerrector" <?php if($sustentates->cargo=="Vicerrector"){echo "selected";}?>> Vicerrector </option>
                                   <option value="Inspector" <?php if($sustentates->cargo=="Inspector"){echo "selected";}?>> Inspector </option>
                                   <option value="Docente rector" <?php if($sustentates->cargo=="Docente rector"){echo "selected";}?>> Docente rector </option>
                                   <option value="Docente vicerrector" <?php if($sustentates->cargo=="Docente vicerrector"){echo "selected";}?>> Docente vicerrector </option>
                                   <option value="Docente-Inspector" <?php if($sustentates->cargo=="Docente-Inspector"){echo "selected";}?>> Docente-Inspector </option>
                                   <option value="Docente-otras autoridades" <?php if($sustentates->cargo=="Docente-otras autoridades"){echo "selected";}?>> Docente-otras autoridades </option>
                                   <option value="Docente" <?php if($sustentates->cargo=="Docente"){echo "selected";}?>> Docente </option>
                                   <option value="Otras autoridades" <?php if($sustentates->cargo=="Otras autoridades"){echo "selected";}?>> Otras autoridades </option>
                               </select>
    </td>    
    <td class="sorting_1">
        <select id="asistencia{{$contador}}" name="asistencia" class="form-control"> value="<?= $sustentates->asistencia; ?>">
                                                    <option value="" > Seleccione asistencia </option>
                                 <!-- value="1" -->  <option value="1" <?php if($sustentates->asistencia=="1"){echo "selected";}?>> SI </option>
                                 <!-- value="0" -->  <option value="0" <?php if($sustentates->asistencia=="0"){echo "selected";}?>> NO </option>
                                   <!--<option value="2" < ?php if($sustentates->asistencia=="2"){echo "selected";}?>> PARCIAL </option>-->
                               </select>
    </td>
    <td class="sorting_1">
    <select id="sustentante_observacion{{$contador}}" value = "<?= $sustentates->sustentante_observacion; ?>" name="id_subcategoria_etiqueta" class="sustentante_observacion form-control" >
            <option value="" > Seleccione observación </option>
            <?php foreach($subCategoriaetiqueta as $tipo){  ?>
            <option value="<?= $tipo->descripcion_cat; ?>" <?php if($sustentates->sustentante_observacion==$tipo->descripcion_cat){echo "selected";}?>> <?= $tipo->descripcion_cat; ?> </option>
     <?php } ?>
    </select>
    </td>
<!--    <td class="sorting_1">
        {{ Form::checkbox('reprogramacion', $sustentates->reprogramacion, $sustentates->reprogramacion, ['id' => 'toggleden' , 'type' => 'checkbox', 'class' => 'toggleden', 'href' => 'ReprograTxt' . $sustentates->id ]) }}
    </td>
    <td class="sorting_1">
        {{ Form::checkbox('asistencia2', $sustentates->asistencia2, $sustentates->asistencia2, ['id' => 'toggleden' , 'type' => 'checkbox', 'class' => 'toggleden', 'href' => 'AsistTxt' . $sustentates->id ]) }}
    </td>
    <td class="sorting_1">
        {{ Form::checkbox('reprogramacion2', $sustentates->reprogramacion2, $sustentates->reprogramacion2, ['id' => 'toggleden' , 'type' => 'checkbox', 'class' => 'toggleden', 'href' => 'ReprograTxt' . $sustentates->id ]) }}
    </td>-->
    <td class="sorting_1">
      {{ Form::submit('Actualizar', array('id' => 'submitUpdate1' . $sustentates->id, 'href' => $sustentates->id, 'class' => 'btn btn-block btn-primary submitUpdate1'))}}  
    </td>
    {{ Form::close() }}
</tr>

@endforeach


  

    </table>



    <?php


echo str_replace('/?', '?', $listadoSustentantes->render() )  ;

}
else
{

?>


<br/><div class='rechazado'><label style='color:#FA206A'>...No se ha encontrado ningun usuario...</label>  </div> 

<?php
}

?>
</div>


<script>
jQuery( document ).ready( function( $ ) {   
    
    $(function() {
        $('.toggleden').bootstrapToggle({
            on: 'SI',
            off: 'NO'
            
        });
    });

        $('.toggleden').on('change', function(){
            
            $( this ).each(function(){
                
                if($(this).val() === "0" || $(this).val() === "1" || $(this).val() ===""){
                
                    var valor = parseInt($(this).val());
                    ///alert(valor);
                    var inputID = $(this).attr('href');
                        console.log( "'" + inputID + "'  |  " + $(this).val());
                    
                    switch(valor) {
                    case 1:
                        $(this).attr('value', parseInt(0));
                        $(this).attr('checked', false);
                        $( 'input[id=' + inputID + ']' ).val(0);
                        break;
                    case 0:
                        $(this).attr('value', parseInt(1));
                        $(this).attr('checked', true);
                        $( 'input[id=' + inputID + ']' ).val(1);
                        break;
                    case "":
                        $(this).attr('value', parseInt(1));
                        $(this).attr('checked', true);
                        $( 'input[id=' + inputID + ']' ).val(1);
                        break;
                    default:
                        $(this).attr('value', null);
                    }
                }
                
                
               
            });
      });   
  });  
</script>

<script>
  jQuery( document ).ready( function( $ ) {     


        $('.submitUpdate1').on('click', function(e){
            e.preventDefault();
            var monId = $(this).attr('href');
            ///sereal = $('#editar_monitoreoo' + monId).serialize();
            sereal =  $('#'+monId+' :input').serialize()+'&id='+monId;

             var form_data = $(this).serialize();
            var myUrl = 'editar_sustentante/' + monId;;
            var sesion = $("#myoption").val();
            
                $.ajax({
                    url: myUrl,
                    type: 'POST',
                    ///cache: false,
                    data: sereal,
                    success: function(data){
                        notif({
                        msg: data.message,
                        type: data.status,
                        width: 300,
                        opacity: 1,
                         
                        });
                        // $("#SaveAlert").alert("Registro Actualizado");
                        // $('#search_results_div').html(data); 
                    },
                    error: function(xhr, textStatus, thrownError){
                        notif({
                        msg: "<b>¡Registro no guardado!</b>",
                        type: "error"

                        });                 // $('#SaveAlert').alert('Se produjo un Error.');
                    }
                });
            ///alert(sereal);
        });
    });
</script>