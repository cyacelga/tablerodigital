<div class="row">

    <div class="col-md-6">

        <div class="box box-primary">

            <div class="box-header">
                <h3 class="box-title">Agregar Informes de Aplicación</h3>
            </div><!-- /.box-header -->

            <!--<div id="notificacion_resul_fap"></div>-->

            <form id="f_agregar_publicacion" method="post" action="agregar_publicacion_usuario" class="formarchivo">

                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="hidden" name="id_monitoreo" value="<?= $id2; ?>">

                <div class="box-body ">

                    <div class="col-xs-12">
                        <label for="pais">Tipo de Novedad</label>
                        <select id="id_subcategoria_etiqueta" required name="id_subcategoria_etiqueta"
                                class="form-control" onchange="mostrardiv_publicaciones(this.value);">
                            <option value=""> Seleccione Novedad</option>
                            <?php foreach($subCategoriaetiqueta as $tipo){  ?>

                            <option value="<?= $tipo->id; ?>"> <?= $tipo->descripcion_cat; ?> </option>

                            <?php } ?>

                        </select>
                    </div>
                    <!--
                     <div class="col-xs-12">
                           <label for="apellido">Titulo del Trabajo</label>
                           <input type="text" class="form-control" id="titulo_publicacion" name="titulo_publicacion" value="" required>
                      </div>

                        <div class="col-xs-12">
                           <label for="apellido">Autores</label>
                           <input type="text" class="form-control" id="autores_publicacion" name="autores_publicacion" value="" required>
                      </div>

                      <div class=" col-xs-8">
                           <label for="apellido">Universidad</label>
                           <input type="text" class="form-control" id="universidad_publicacion" name="universidad_publicacion" value="" >
                      </div>

                      <div class=" col-xs-4">
                           <label for="apellido">Año</label>
                           <input type="text" class="form-control" id="anio_publicacion" name="anio_publicacion" value="" >
                      </div>

                       <div class=" col-xs-12">
                           <label for="apellido">Pais</label>
                           <input type="text" class="form-control" id="pais_publicacion" name="pais_publicacion" value="" >
                      </div>-->
                    <div class=" col-xs-12">
                        <label for="apellido">Observación</label>
                        <input type="text" class="form-control" id="observacion_publicacion"
                               name="observacion_publicacion" value="">
                    </div>
                    <!--
                    <div class=" col-xs-12" id="info_revista"  style="display:none;" >
                               <div class=" col-xs-8">
                               <label for="apellido">Revista</label>
                               <input type="text" class="form-control" id="revista_publicacion" name="revista_publicacion" value="" >
                          </div>

                           <div class=" col-xs-4">
                               <label for="apellido">Volumen</label>
                               <input type="text" class="form-control" id="volumen_publicacion" name="volumen_publicacion" value="" >
                          </div>

                           <div class=" col-xs-4">
                               <label for="apellido">Número</label>
                               <input type="text" class="form-control" id="numero_publicacion" name="numero_publicacion" value="" >
                          </div>

                              <div class=" col-xs-4">
                               <label for="apellido">Pagina Inicial</label>
                               <input type="text" class="form-control" id="pageI_publicacion" name="pageI_publicacion" value="" >
                              </div>

                             <div class=" col-xs-4">
                               <label for="apellido">Pagina final</label>
                               <input type="text" class="form-control" id="pageF_publicacion" name="pageF_publicacion" value="" >
                             </div>


                    </div>-->


                    <!-- <div class=" col-xs-12" id="info_libro"  style="display:none;" >

                             <div class=" col-xs-4">
                                <label for="apellido">Volumen</label>
                                <input type="text" class="form-control" id="vlibro_publicacion" name="vlibro_publicacion" value="" >
                           </div>

                            <div class=" col-xs-4">
                                <label for="apellido">Número</label>
                                <input type="text" class="form-control" id="nlibro_publicacion" name="nlibro_publicacion" value="" >
                           </div>

                           <div class=" col-xs-4">
                                <label for="apellido">Ciudad</label>
                                <input type="text" class="form-control" id="nlibro_publicacion" name="nlibro_publicacion" value="" >
                           </div>


                            <div class=" col-xs-4">
                                <label for="apellido">ISSN - ISBN</label>
                                <input type="text" class="form-control" id="ISBN_publicacion" name="ISBN_publicacion" value="" >
                           </div>

                            <div class=" col-xs-4">
                                <label for="apellido">Edicion</label>
                                <input type="text" class="form-control" id="edicion_publicacion" name="edicion_publicacion" value="" >
                           </div>

                           <div class=" col-xs-4">
                                <label for="apellido">Editorial</label>
                                <input type="text" class="form-control" id="editorial_publicacion" name="editorial_publicacion" value=""  >
                           </div>
                      </div>    -->
                    <div class=" col-xs-12" style="background-color:rgb(229, 245, 253);">
                        <label for="apellido">Archivo a subir (Formato: PDF) </label>
                        <input type="file" class="form-control" id="file" name="file" required>
                    </div>


                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Agregar Informe</button>
                </div>

            </form>
        </div>


    </div>

    <!--instlacion del listado-->
    <div class="col-md-6">

        <div class="box box-primary">

            <div class="box-header">
                <h3 class="box-title">Información de listado de asistencia</h3>
            </div><!-- /.box-header -->

            <div id="notificacion_resul_feu"></div>

            <form id="f_editar_listados" method="post" action="f_editar_listados" class="form-horizontal form_entrada"
                  onsubmit="return validarListado(event);">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <!--id oculto-->
                <input type="hidden" name="id5" id="id5" value="<?= $id2; ?>">

                <div class="box-body ">
                    <div class="form-group col-xs-12">
                        <label for="listado_asistencia">Recepción de listado de asistencia*</label>
                        <select id="listado_asistencia" name="listado_asistencia" class="form-control">
                            <option value=""> SELECCIONE</option>
                            <option value="SI" <?php if ($monitoreo->listado_asistencia == "SI") {
                                echo "selected";
                            }?> > SI
                            </option>
                            <option value="NO" <?php if ($monitoreo->listado_asistencia == "NO") {
                                echo "selected";
                            }?> > NO
                            </option>
                        </select>
                    </div>
                    <div class="form-group col-xs-12">
                        <label for="observacion_listado_asistencia">Problema de listado de asistencia*</label>
                        <select id="observacion_listado_asistencia"
                                value="<?= $monitoreo->observacion_listado_asistencia; ?>"
                                name="observacion_listado_asistencia" class="form-control">
                            <option value="SIN NOVEDAD"> Seleccione Problema</option>
                            <?php foreach($subCategoriaetiquetaListado as $tipo){  ?>

                            <option value="<?= $tipo->descripcion_cat; ?>" <?php if ($monitoreo->observacion_listado_asistencia == $tipo->descripcion_cat) {
                                echo "selected";
                            }?>> <?= $tipo->descripcion_cat; ?> </option>

                            <?php } ?>

                        </select>
                    </div>
                    <div class="form-group col-xs-12">
                        <label for="listado_validacion">Cuadre de listado*</label>
                        <select id="listado_validacion" name="listado_validacion" class="form-control">
                            <option value=""> SELECCIONE</option>
                            <option value="SI" <?php if ($monitoreo->listado_validacion == "SI") {
                                echo "selected";
                            }?>> SI
                            </option>
                            <option value="NO" <?php if ($monitoreo->listado_validacion == "NO") {
                                echo "selected";
                            }?>> NO
                            </option>
                        </select>
                    </div>

                </div>


                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Actualizar Registro</button>
                </div>
            </form>

        </div>

    </div> <!-- end col mod 6 -->

    <div class="col-md-6">

        <div class="box box-primary">
            <div class="box-body box-profile">
                <!--<img class="profile-user-img img-responsive img-circle" src="imagenes/avatar.jpg" alt="User profile picture">-->
                <h3 class="profile-username text-center"><?= $monitoreo->institucion; ?></h3>
                <p class="text-muted text-center"><?= $monitoreo->fecha_programada_inicio; ?>
                    -<?= $monitoreo->sesion; ?></p>

                <div id="notificacion_resul_fapu"></div>
                <ul class="list-group list-group-unbordered">


                    <?php foreach($subCategoriaetiqueta as $tipo){  ?>
                    <li class="list-group-item">
                        <i class="fa fa-file-pdf-o"></i></i><b>--<?= $tipo->descripcion_cat; ?></b> <a
                                class="pull-right"></a>

                        <?php foreach($informe1 as $archivo){  ?>

                        <?php  if($archivo->id_subcategoria_etiqueta == $tipo->id){    ?>

                        <br/> <i class="fa fa-circle-o text-yellow"></i> <span class="text-light-blue">-Informe de aplicación</span>
                        <br/> <span><b>observaciones: </b>-<?=  $archivo->observaciones;  ?></span> <span
                                class="tools pull-right">
                                <a href="javascript:void(0);"
                                                            onclick="borrarpublicacion(<?= $archivo->id;  ?> );"><i
                                        class="fa fa-trash-o"></i></a></span>
                            <?php  if($archivo->id_subcategoria_etiqueta == getenv('APP_ID_SUBCATEGORIA_ETIQUETA')){    ?>
                                <br/><a href="<?= getenv('APP_SERVER_IMG_URL') . $archivo->ruta;  ?>" style="display:block;" target="_blank">
                                    <button class="btn btn-block btn-success btn-xs">Descargar</button>
                                </a>
                            <?php } else { ?>
                                <br/><a href="<?= 'tablerodigital/' .$rutaarchivos . $archivo->ruta;  ?>" style="display:block;" target="_blank">
                                    <button class="btn btn-block btn-success btn-xs">Descargar</button>
                                </a>
                            <?php } ?>


                        <?php } ?>
                        <?php } ?>

                    </li>

                    <?php } ?>


                </ul>

                <a href="javascript:void(0);" class="btn btn-primary btn-block"><b>-</b></a>
            </div><!-- /.box-body -->
        </div>
    </div>
</div>
<script type="text/javascript">
    function validarListado(e) {
        $receptaListado = $('#listado_asistencia option:selected').val();
        $novedadListado = $('#observacion_listado_asistencia option:selected').val();
        if ($receptaListado === 'NO' && $novedadListado === 'SIN NOVEDAD') {
            alert('Por favor especifique el problema del listado de asistencia.');
            e.cancelBubble = true;
            e.preventDefault();
            return false;
        }
        return true;
    }
</script>