<div class="row">

    <div class="col-md-6">

        <div class="box box-primary">

            <div class="box-header">
                <h3 class="box-title">Información de la institución Educativa</h3>
            </div><!-- /.box-header -->

            <div id="notificacion_resul_feu"></div>


            <form id="f_editar_rector_monitoreo" method="post" action="editar_rector_monitoreo"
                  class="form-horizontal form_entrada" onsubmit="return validarIncompleta(event);">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <!--id oculto-->
                <input type="hidden" name="id111" id="id111" value="{{$idmo}}">
                <input type="hidden" name="id11" id="id11" value="{{$institucion->id}}">
                <div class="box-body ">
                    <div class="form-group col-xs-12">
                        <label for="institucion">Nombre de la Institución </label>
                        <input type="text" class="form-control" id="institucion" name="institucion"
                               value="{{$institucion->institucion}}">
                    </div>
                    <!--div  class="form-group col-xs-12">
                        <div class="row">
                            <div class="col-xs-6">
                                <label for="nombresr">Nombre del responsable de Sede</label>
                                <input type="text" class="form-control" id="nombresr" name="nombresr"
                                       value="{ {$institucion->rector}}">
                            </div>
                            <div class="col-xs-6">
                                <label for="numero_id_rector">Número ID del responsable de Sede</label>
                                <input type="text" class="form-control" id="numero_id_rector" name="numero_id_rector"
                                       value="{ {$institucion->numero_id_rector}}">
                            </div>
                        </div>
                    </div-->
                    <div  class="form-group col-xs-8">
                        <div class="row">
                            <div class="col-xs-8">
                                <label for="celular">Celular 1</label>
                                <input type="text" class="form-control" id="celular" name="celular"
                                       value="{{$institucion->celular}}">
                            </div>
                            <div class="col-xs-4">
                                <label for="celular1_wa">Whatsapp 1</label>
                                {{ Form::checkbox('celular1_wa', $institucion->celular1_wa, $institucion->celular1_wa, ['id' => 'toggleden' , 'type' => 'checkbox', 'class' => 'toggleden', 'href' => 'AsistTxt' . $institucion->id ]) }}
                            </div>
                        </div>
                    </div>
                    <div  class="form-group col-xs-8">
                        <div class="row">
                            <div class="col-xs-8">
                                <label for="celularw">Celular 2</label>
                                <input type="text" class="form-control" id="celularw" name="celularw"
                                       value="{{$institucion->celular_whatsapp}}">
                            </div>
                            <div class="col-xs-4">
                                <label for="celular2_wa">Whatsapp 2</label>
                                {{ Form::checkbox('celular2_wa', $institucion->celular2_wa, $institucion->celular2_wa, ['id' => 'toggleden' , 'type' => 'checkbox', 'class' => 'toggleden', 'href' => 'AsistTxt' . $institucion->id ]) }}
                            </div>
                        </div>
                    </div>

                    <div  class="form-group col-xs-12">
                        <div class="row">
                            <div class="col-xs-6">
                                <label for="telefono1">Teléfono fijo 1</label>
                                <input type="text" class="form-control" id="telefono1" name="telefono1"
                                       value="{{$institucion->telefono1}}">
                            </div>
                            <div class="col-xs-6">
                                <label for="telefono2">Teléfono fijo 2</label>
                                <input type="text" class="form-control" id="telefono2" name="telefono2"
                                       value="{{$institucion->telefono2}}">
                            </div>
                        </div>

                    </div>
                    <div  class="form-group col-xs-12">
                        <div class="row">
                            <div class="col-xs-6">
                                <label for="tele1">Teléfono fijo 3 (opcional)</label>
                                <input type="text" class="form-control" id="tele1" name="tele1"
                                       value="{{$institucion->contactoinstitucion}}">
                            </div>
                            <div class="col-xs-6">
                                <label for="telefono_contactado">Teléfono Contactado</label>
                                <input type="text" class="form-control" id="telefono_contactado"
                                       name="telefono_contactado" value="{{$institucion->telefono_contactado}}">
                            </div>
                        </div>
                    </div>
                    <!--div  class="form-group col-xs-12">
                        <label for="emailr">Correo electrónico 1</label>
                        <input type="text" class="form-control" id="emailr" name="emailr"
                               value="{ {$institucion->correoinstitucion}}">
                    </div>

                    <div  class="form-group col-xs-12">
                        <label for="correo">Correo electrónico 2</label>
                        <input type="text" class="form-control" id="correo" name="correo"
                               value="{ {$institucion->correo}}">

                    </div-->
                    <div  class="form-group col-xs-12">
                        <label for="comunicacion">Comunicación rápida</label>
                        <select id="comunicacion" name="comunicacion" value="{{$institucion->comunicacion}}"
                                class="form-control">
                            <option value=""> SELECCIONE</option>
                            <option value="WHATSAPP" <?php if ($institucion->comunicacion == "WHATSAPP") {
                                echo "selected";
                            }?>> WHATSAPP
                            </option>
                            <option value="email" <?php if ($institucion->comunicacion == "email") {
                                echo "selected";
                            }?>> CORREO ELECTRóNICO
                            </option>
                            <option value="msm" <?php if ($institucion->comunicacion == "msm") {
                                echo "selected";
                            }?>> MENSAJE DE TEXTO
                            </option>
                        </select>

                    </div>
                    <div class="form-group col-xs-12">
                        <label for="estadollamada_apli">Estado de llamada</label>
                        <select style="<?php if ($umonitoreo->estadollamada_apli == "CONTACTADO") {
                            echo "color:White;background:#5FB404;width:550px;";
                        } else {
                            echo "color:White;background:#FA5858;width:550px;";
                        }?>" id="estadollamada" name="estadollamada" class="form-control">
                            <option value=""> SELECCIONE</option>
                            <option value="CONTACTADO" <?php if ($umonitoreo->estadollamada_apli == "CONTACTADO") {
                                echo "selected";
                            }?>> CONTACTADO
                            </option>
                            <option value="NO CONTACTADO" <?php if ($umonitoreo->estadollamada_apli == "NO CONTACTADO") {
                                echo "selected";
                            }?>> NO CONTACTADO
                            </option>
                        </select>

                    </div>
                    <div class="form-group col-xs-12">
                        <label for="novedadllamada_apli">Problema de llamada</label>
                        <select style="<?php if ($umonitoreo->novedadllamada_apli == "VOLVER A LLAMAR" or $umonitoreo->novedadllamada_apli == "NO CONTESTA") {
                            echo "color:White;background:#D7DF01;width:550px;";
                        } ?>" id="novedadllamada" name="novedadllamada" class="form-control">
                            <option value=""> SELECCIONE</option>
                            <option value="NUMERO INCORRECTO" <?php if ($umonitoreo->novedadllamada_apli == "NUMERO INCORRECTO") {
                                echo "selected";
                            }?>> NúMERO INCORRECTO
                            </option>
                        <!--<option value="NUMERO EQUIVOCADO" <?php if ($umonitoreo->novedadllamada_apli == "NUMERO EQUIVOCADO") {
                            echo "selected";
                        }?>> NúMERO EQUIVOCADO </option>-->
                            <option value="SIN NUMERO" <?php if ($umonitoreo->novedadllamada_apli == "SIN NUMERO") {
                                echo "selected";
                            }?> > SIN NUMERO
                            </option>
                            <option value="NO CONTESTA" <?php if ($umonitoreo->novedadllamada_apli == "NO CONTESTA") {
                                echo "selected";
                            }?> > NO CONTESTA
                            </option>
                            <option value="VOLVER A LLAMAR" <?php if ($umonitoreo->novedadllamada_apli == "VOLVER A LLAMAR") {
                                echo "selected";
                            }?> > VOLVER A LLAMAR
                            </option>
                        </select>

                    </div>
                    <div class="form-group col-xs-12" hidden> <!-- Se encuentra en corresponsable -->
                        <label for="recibiocomunicado">Recepción de Comunicado</label>
                        <select id="recibiocomunicado" name="recibiocomunicado" class="form-control">
                            <option value=""> SELECCIONE</option>
                            <option value="SI" <?php if ($institucion->recibiocomunicado == "SI") {
                                echo "selected";
                            }?>> SI
                            </option>
                            <option value="NO" <?php if ($institucion->recibiocomunicado == "NO") {
                                echo "selected";
                            }?>> NO
                            </option>
                            <option value="REVISARA" <?php if ($institucion->recibiocomunicado == "REVISARA") {
                                echo "selected";
                            }?>> REVISARÁ
                            </option>
                        </select>

                    </div>
                    <div class="form-group col-xs-12">
                        <label for="observacion">Observación</label>
                        <input type="text" class="form-control" id="observacion" name="observacion"
                               value="{{$institucion->observacion}}">
                    </div>
                </div>

                <div class="col-md-15">

                    <div class="box box-primary" hidden>

                        <div class="box-header">
                            <h3 class="box-title">Información del responsable de sede 2</h3>
                        </div><!-- /.box-header -->

                        <!--id oculto-->
                        <div class="box-body ">
                            <div class="form-group col-xs-12">
                                <label for="nombresrs">Nombre del responsable de Sede</label>
                                <input type="text" class="form-control" id="nombresrs" name="nombresrs"
                                       value="{{$institucion->rector_suplente}}">
                            </div>
                            <div class="form-group col-xs-8">
                                <div class="row">
                                    <div class="col-xs-8">
                                        <label for="celulars">Celular 1</label>
                                        <input type="text" class="form-control" id="celulars" name="celulars"
                                               value="{{$institucion->celular_suplente}}">
                                    </div>
                                    <div class="col-xs-4">
                                        <label for="celular1_swa">Whatsapp 1</label>
                                        {{ Form::checkbox('celular1_swa', $institucion->celular1_swa, $institucion->celular1_swa, ['id' => 'toggleden' , 'type' => 'checkbox', 'class' => 'toggleden', 'href' => 'AsistTxt' . $institucion->id ]) }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-xs-8">
                                <div class="row">
                                    <div class="col-xs-8">
                                        <label for="celularws">Celular 2</label>
                                        <input type="text" class="form-control" id="celularws" name="celularws"
                                               value="{{$institucion->celular_whatsapp_suplente}}">
                                    </div>
                                    <div class="col-xs-4">
                                        <label for="celular2_swa">Whatsapp 2</label>
                                        {{ Form::checkbox('celular2_swa', $institucion->celular2_swa, $institucion->celular2_swa, ['id' => 'toggleden' , 'type' => 'checkbox', 'class' => 'toggleden', 'href' => 'AsistTxt' . $institucion->id ]) }}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-xs-12">
                                <label for="telefono1s">Teléfono fijo 1</label>
                                <input type="text" class="form-control" id="telefono1s" name="telefono1s"
                                       value="{{$institucion->telefono1_suplente}}">

                            </div>
                            <div class="form-group col-xs-12">
                                <label for="telefono2s">Teléfono fijo 2</label>
                                <input type="text" class="form-control" id="telefono2s" name="telefono2s"
                                       value="{{$institucion->telefono2_suplente}}">

                            </div>
                            <div class="form-group col-xs-12">
                                <label for="tele1s">Teléfono fijo 3 (opcional)</label>
                                <input type="text" class="form-control" id="tele1s" name="tele1s"
                                       value="{{$institucion->contactoinstitucion_suplente}}">

                            </div>
                            <div class="form-group col-xs-12">
                                <label for="emailrs">Correo electrónico 1</label>
                                <input type="text" class="form-control" id="emailrs" name="emailrs"
                                       value="{{$institucion->correoinstitucion_suplente}}">
                            </div>
                            <div class="form-group col-xs-12">
                                <label for="correos">Correo electrónico 2</label>
                                <input type="email" class="form-control" id="correos" name="correos"
                                       value="{{$institucion->correo_suplente}}">

                            </div>
                            <div class="form-group col-xs-12">
                                <label for="comunicacion_suplente">Comunicación rápida</label>
                                <select id="comunicacions" name="comunicacions"
                                        value="{{$institucion->comunicacion_suplente}}" class="form-control">
                                    <option value=""> SELECCIONE</option>
                                    <option value="WHATSAPP" <?php if ($institucion->comunicacion_suplente == "WHATSAPP") {
                                        echo "selected";
                                    }?>> WHATSAPP
                                    </option>
                                    <option value="email" <?php if ($institucion->comunicacion_suplente == "email") {
                                        echo "selected";
                                    }?>> CORREO ELECTRóNICO
                                    </option>
                                    <option value="msm" <?php if ($institucion->comunicacion_suplente == "msm") {
                                        echo "selected";
                                    }?>> MENSAJE DE TEXTO
                                    </option>
                                </select>

                            </div>
                        </div>

                    </div> <!-- end col mod 6 -->


 <div  class="box box-primary" >
                            <div class="box-header">
                                <h3 class="box-title">Información del corresponsable</h3> <!-- corresponsable o Supervisor-->
                            </div><!-- /.box-header -->
                            <div class="box-body ">
                                <div class="form-group col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <label for="corresponsable">Nombres</label>
                                            <input type="text" class="form-control" id="corresponsable"
                                                   name="corresponsable" value="{{$umonitoreo->corresponsable}}">
                                        </div>
                                        <div class="col-xs-6">
                                            <label for="corresponsable_cedula">Número ID del corresponsable</label> <!-- corresponsable o Supervisor-->
                                            <input type="text" class="form-control" id="corresponsable_cedula"
                                                   name="corresponsable_cedula"
                                                   value="{{$umonitoreo->corresponsable_cedula}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-xs-8">
                                    <div class="row">
                                        <div class="col-xs-8">
                                            <label for="corresponsable_celular">Celular</label>
                                            <input type="text" class="form-control" id="corresponsable_celular"
                                                   name="corresponsable_celular"
                                                   value="{{$umonitoreo->corresponsable_celular}}">
                                        </div>
                                        <div class="col-xs-4" hidden>
                                            <label for="corresponsable_celular_wa">Whatsapp</label>
                                            {{ Form::checkbox('corresponsable_celular_wa', $umonitoreo->corresponsable_celular_wa, $umonitoreo->corresponsable_celular_wa, ['id' => 'toggleden' , 'type' => 'checkbox', 'class' => 'toggleden', 'href' => 'AsistTxt' . $umonitoreo->corresponsable_celular_wa ]) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="corresponsable_telefono">teléfono</label>
                                    <input type="text" class="form-control" id="corresponsable_telefono"
                                           name="corresponsable_telefono"
                                           value="{{$umonitoreo->corresponsable_telefono}}">

                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="corresponsable_correo">Correo electrónico 1</label>
                                    <input type="text" class="form-control" id="corresponsable_correo"
                                           name="corresponsable_correo" value="{{$umonitoreo->corresponsable_correo}}">

                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="corresponsable_correo2">Correo electrónico 2</label>
                                    <input type="text" class="form-control" id="corresponsable_correo2"
                                           name="corresponsable_correo2"
                                           value="{{$umonitoreo->corresponsable_correo2}}">

                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="corresponsable_comunicacion">Comunicación rápida</label>
                                    <select id="tecni_comunicacion" name="corresponsable_comunicacion"
                                            value="{{$umonitoreo->corresponsable_comunicacion}}" class="form-control">
                                        <option value=""> SELECCIONE</option>
                                        <option value="WHATSAPP" <?php if ($umonitoreo->corresponsable_comunicacion == "WHATSAPP") {
                                            echo "selected";
                                        }?>> WHATSAPP
                                        </option>
                                        <option value="email" <?php if ($umonitoreo->corresponsable_comunicacion == "email") {
                                            echo "selected";
                                        }?>> CORREO ELECTRóNICO
                                        </option>
                                        <option value="msm" <?php if ($umonitoreo->corresponsable_comunicacion == "msm") {
                                            echo "selected";
                                        }?>> MENSAJE DE TEXTO
                                        </option>
                                    </select>
                                </div>
								 <div class="form-group col-xs-12"> 
									<label for="recibiocomunicado">Recepción de Comunicado</label>
									<select id="recibiocomunicado" name="recibiocomunicado" class="form-control">
										<option value=""> SELECCIONE</option>
										<option value="SI" <?php if ($institucion->recibiocomunicado == "SI") {
											echo "selected";
										}?>> SI
										</option>
										<option value="NO" <?php if ($institucion->recibiocomunicado == "NO") {
											echo "selected";
										}?>> NO
										</option>
										<option value="REVISARA" <?php if ($institucion->recibiocomunicado == "REVISARA") {
											echo "selected";
										}?>> REVISARÁ
										</option>
									</select>							
								</div>
                                <div class="form-group col-xs-12">
                                    <label for="estadollamada_corresponsable">Estado de llamada</label>
                                    <select style="<?php if ($umonitoreo->estadollamada_corresponsable == "CONTACTADO") {
                                        echo "color:White;background:#5FB404;width:550px;";
                                    } else {
                                        echo "color:White;background:#FA5858;width:550px;";
                                    }?>" id="estadollamada_corresponsable" name="estadollamada_corresponsable"
                                            class="form-control">
                                        <option value=""> SELECCIONE</option>
                                        <option value="CONTACTADO" <?php if ($umonitoreo->estadollamada_corresponsable == "CONTACTADO") {
                                            echo "selected";
                                        }?>> CONTACTADO
                                        </option>
                                        <option value="NO CONTACTADO" <?php if ($umonitoreo->estadollamada_corresponsable == "NO CONTACTADO") {
                                            echo "selected";
                                        }?>> NO CONTACTADO
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="novedadllamada_corresponsable">Problema de llamada</label>
                                    <select style="<?php if ($umonitoreo->novedadllamada_corresponsable == "VOLVER A LLAMAR" or $umonitoreo->novedadllamada_corresponsable == "NO CONTESTA") {
                                        echo "color:White;background:#D7DF01;width:550px;";
                                    } ?>" id="novedadllamada_corresponsable" name="novedadllamada_corresponsable"
                                            class="form-control">
                                        <option value=""> SELECCIONE</option>
                                        <option value="NUMERO INCORRECTO" <?php if ($umonitoreo->novedadllamada_corresponsable == "NUMERO INCORRECTO") {
                                            echo "selected";
                                        }?>> NúMERO INCORRECTO
                                        </option>
                                    <!--<option value="NUMERO EQUIVOCADO" <?php if ($umonitoreo->novedadllamada_corresponsable == "NUMERO EQUIVOCADO") {
                                        echo "selected";
                                    }?>> NúMERO EQUIVOCADO </option>-->
                                        <option value="SIN NUMERO" <?php if ($umonitoreo->novedadllamada_corresponsable == "SIN NUMERO") {
                                            echo "selected";
                                        }?> > SIN NUMERO
                                        </option>
                                        <option value="NO CONTESTA" <?php if ($umonitoreo->novedadllamada_corresponsable == "NO CONTESTA") {
                                            echo "selected";
                                        }?> > NO CONTESTA
                                        </option>
                                        <option value="VOLVER A LLAMAR" <?php if ($umonitoreo->novedadllamada_corresponsable == "VOLVER A LLAMAR") {
                                            echo "selected";
                                        }?> > VOLVER A LLAMAR
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="asistencia_corresponsable">Asistencia corresponsable</label>
                                    <select id="asistencia_corresponsable" name="asistencia_corresponsable"
                                            class="form-control">
                                        <option value=""> SELECCIONE</option>
                                        <option value="SI" <?php if ($umonitoreo->asistencia_corresponsable == "SI") {
                                            echo "selected";
                                        }?>> Sí
                                        </option>
                                        <option value="NO" <?php if ($umonitoreo->asistencia_corresponsable == "NO") {
                                            echo "selected";
                                        }?>> NO
                                        </option>
                                    </select>

                                </div>
                                <div class="form-group col-xs-12" hidden>
                                    <label for="recepcion_clave_corresponsable">Recepción de claves
                                        Corresponsable</label>
                                    <select id="recepcion_clave_corresponsable" name="recepcion_clave_corresponsable"
                                            class="form-control">
                                        <option value=""> SELECCIONE</option>
                                        <option value="SI" <?php if ($umonitoreo->recepcion_clave_corresponsable == "SI") {
                                            echo "selected";
                                        }?>> Sí
                                        </option>
                                        <option value="NO" <?php if ($umonitoreo->recepcion_clave_corresponsable == "NO") {
                                            echo "selected";
                                        }?>> NO
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="corresponsable_motivo">Observación</label>
                                    <input type="text" class="form-control" id="corresponsable_motivo"
                                           name="corresponsable_motivo" value="{{$umonitoreo->corresponsable_motivo}}">
                                </div>
                            </div>
                        </div>
                    <div class="col-md-15">
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title">Información del Aplicador</h3> <!-- Aplicador -->
                            </div><!-- /.box-header -->
                            <div class="box-body ">
                                <div class="form-group col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <label for="apli_nombre1">Nombres</label>
                                            <input type="text" class="form-control" id="apli_nombre1"
                                                   name="apli_nombre1" value="{{$umonitoreo->apli_nombre1}}">
                                        </div>
                                        <div class="col-xs-6">
                                            <label for="apli_cedula">Número ID del Aplicador</label>
                                            <input type="text" class="form-control" id="apli_cedula" name="apli_cedula"
                                                   value="{{$umonitoreo->apli_cedula}}">
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group col-xs-8">
                                    <div class="row">
                                        <div class="col-xs-8">
                                            <label for="apli_telef1">Celular</label>
                                            <input type="text" class="form-control" id="apli_telef1" name="apli_telef1"
                                                   value="{{$umonitoreo->apli_telef1}}">
                                        </div>
                                        <div class="col-xs-4">
                                            <label for="apli_telef1_wa">Whatsapp</label>
                                            {{ Form::checkbox('apli_telef1_wa', $umonitoreo->apli_telef1_wa, $umonitoreo->apli_telef1_wa, ['id' => 'toggleden' , 'type' => 'checkbox', 'class' => 'toggleden', 'href' => 'AsistTxt' . $umonitoreo->apli_telef1_wa ]) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-xs-8">
                                    <div class="row">
                                        <div class="col-xs-8">
                                            <label for="apli_telef2">Celular (opcional) </label>
                                            <input type="text" class="form-control" id="apli_telef2" name="apli_telef2"
                                                   value="{{$umonitoreo->apli_telef2}}">
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <label for="apli_telef3">Teléfono fijo</label>
                                            <input type="text" class="form-control" id="apli_telef3" name="apli_telef3"
                                                   value="{{$umonitoreo->apli_telef3}}">
                                        </div>
                                        <div class="col-xs-6">
                                            <label for="apli_telef_contactado">Teléfono Contactado</label>
                                            <input type="text" class="form-control" id="apli_telef_contactado"
                                                   name="apli_telef_contactado"
                                                   value="{{$umonitoreo->apli_telef_contactado}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="apli_email">Correo electrónico 1</label>
                                    <input type="text" class="form-control" id="apli_email" name="apli_email"
                                           value="{{$umonitoreo->apli_email}}">

                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="apli_email2">Correo electrónico 2</label>
                                    <input type="text" class="form-control" id="apli_email2" name="apli_email2"
                                           value="{{$umonitoreo->apli_email2}}">

                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="apli_comunicacion">Comunicación rápida</label>
                                    <select id="apli_comunicacion" name="apli_comunicacion"
                                            value="{{$umonitoreo->apli_comunicacion}}" class="form-control">
                                        <option value=""> SELECCIONE</option>
                                        <option value="WHATSAPP" <?php if ($umonitoreo->apli_comunicacion == "WHATSAPP") {
                                            echo "selected";
                                        }?>> WHATSAPP
                                        </option>
                                        <option value="email" <?php if ($umonitoreo->apli_comunicacion == "email") {
                                            echo "selected";
                                        }?>> CORREO ELECTRóNICO
                                        </option>
                                        <option value="msm" <?php if ($umonitoreo->apli_comunicacion == "msm") {
                                            echo "selected";
                                        }?>> MENSAJE DE TEXTO
                                        </option>
                                    </select>

                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="tele1">Estado de llamada</label>
                                    <select style="<?php if ($umonitoreo->estadollamada_aplicadores == "CONTACTADO") {
                                        echo "color:White;background:#5FB404;width:550px;";
                                    } else {
                                        echo "color:White;background:#FA5858;width:550px;";
                                    }?>" id="estadollamada_aplicadores" name="estadollamada_aplicadores"
                                            class="form-control">
                                        <option value=""> SELECCIONE</option>
                                        <option value="CONTACTADO" <?php if ($umonitoreo->estadollamada_aplicadores == "CONTACTADO") {
                                            echo "selected";
                                        }?>> CONTACTADO
                                        </option>
                                        <option value="NO CONTACTADO" <?php if ($umonitoreo->estadollamada_aplicadores == "NO CONTACTADO") {
                                            echo "selected";
                                        }?>> NO CONTACTADO
                                        </option>
                                    </select>

                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="tele1">Problema de llamada</label>
                                    <select style="<?php if ($umonitoreo->novedadllamada_aplicadores == "VOLVER A LLAMAR" or $umonitoreo->novedadllamada_aplicadores == "NO CONTESTA") {
                                        echo "color:White;background:#D7DF01;width:550px;";
                                    } ?>" id="novedadllamada_aplicadores" name="novedadllamada_aplicadores"
                                            class="form-control">
                                        <option value=""> SELECCIONE</option>
                                        <option value="NUMERO INCORRECTO" <?php if ($umonitoreo->novedadllamada_aplicadores == "NUMERO INCORRECTO") {
                                            echo "selected";
                                        }?>> NúMERO INCORRECTO
                                        </option>
                                    <!--<option value="NUMERO EQUIVOCADO" <?php if ($umonitoreo->novedadllamada_aplicadores == "NUMERO EQUIVOCADO") {
                                        echo "selected";
                                    }?>> NúMERO EQUIVOCADO </option>-->
                                        <option value="SIN NUMERO" <?php if ($umonitoreo->novedadllamada_aplicadores == "SIN NUMERO") {
                                            echo "selected";
                                        }?> > SIN NUMERO
                                        </option>
                                        <option value="NO CONTESTA" <?php if ($umonitoreo->novedadllamada_aplicadores == "NO CONTESTA") {
                                            echo "selected";
                                        }?> > NO CONTESTA
                                        </option>
                                        <option value="VOLVER A LLAMAR" <?php if ($umonitoreo->novedadllamada_aplicadores == "VOLVER A LLAMAR") {
                                            echo "selected";
                                        }?> > VOLVER A LLAMAR
                                        </option>
                                    </select>

                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="asistencia_ineval">Asistencia del Aplicador</label>
                                    <select id="asistencia_ineval" name="asistencia_ineval" class="form-control">
                                        <option value=""> SELECCIONE</option>
                                        <option value="SI" <?php if ($umonitoreo->asistencia_ineval == "SI") {
                                            echo "selected";
                                        }?>> Sí
                                        </option>
                                        <option value="NO" <?php if ($umonitoreo->asistencia_ineval == "NO") {
                                            echo "selected";
                                        }?>> NO
                                        </option>
                                    </select>

                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="apli_recepcion">Recepción de Comunicado</label>
                                    <select id="apli_recepcion" name="apli_recepcion" class="form-control">
                                        <option value=""> SELECCIONE</option>
                                        <option value="SI" <?php if ($umonitoreo->apli_recepcion == "SI") {
                                            echo "selected";
                                        }?>> SI
                                        </option>
                                        <option value="NO" <?php if ($umonitoreo->apli_recepcion == "NO") {
                                            echo "selected";
                                        }?>> NO
                                        </option>
                                        <option value="REVISARA" <?php if ($umonitoreo->apli_recepcion == "REVISARA") {
                                            echo "selected";
                                        }?>> REVISARÁ
                                        </option>
                                    </select>

                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="apli_motivo">Observación</label>
                                    <input type="text" class="form-control" id="apli_motivo" name="apli_motivo"
                                           value="{{$umonitoreo->apli_motivo}}">
                                </div>
                            </div>
                        </div>
                        <!--tecnico de control-->
                        <div hidden class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title">Información del Técnico de control</h3>
                                <!-- TÉCNICO DE CONTROL -->
                            </div><!-- /.box-header -->
                            <div class="box-body ">
                                <div class="form-group col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <label for="tecni_nombre1">Nombres</label>
                                            <input type="text" class="form-control" id="tecni_nombre1"
                                                   name="tecni_nombre1" value="{{$umonitoreo->tecni_nombre1}}">
                                        </div>
                                        <div class="col-xs-6">
                                            <label for="tecni_cedula">Número ID del Técnico de control</label>
                                            <input type="text" class="form-control" id="tecni_cedula"
                                                   name="tecni_cedula" value="{{$umonitoreo->tecni_cedula}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-xs-8">
                                    <div class="row">
                                        <div class="col-xs-8">
                                            <label for="tecni_telef1">Celular</label>
                                            <input type="text" class="form-control" id="tecni_telef1"
                                                   name="tecni_telef1" value="{{$umonitoreo->tecni_telef1}}">
                                        </div>
                                        <div class="col-xs-4">
                                            <label for="tecni_telef1_wa">Whatsapp</label>
                                            {{ Form::checkbox('tecni_telef1_wa', $umonitoreo->tecni_telef1_wa, $umonitoreo->tecni_telef1_wa, ['id' => 'toggleden' , 'type' => 'checkbox', 'class' => 'toggleden', 'href' => 'AsistTxt' . $umonitoreo->tecni_telef1_wa ]) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-xs-8">
                                    <div class="row">
                                        <div class="col-xs-8">
                                            <label for="tecni_telef2">Celular (opcional) </label>
                                            <input type="text" class="form-control" id="apli_telef2" name="tecni_telef2"
                                                   value="{{$umonitoreo->tecni_telef2}}">
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <label for="tecni_telef3">Teléfono fijo</label>
                                            <input type="text" class="form-control" id="tecni_telef3"
                                                   name="tecni_telef3" value="{{$umonitoreo->tecni_telef3}}">
                                        </div>
                                        <div class="col-xs-6">
                                            <label for="tecni_telef_contactado">Teléfono Contactado</label>
                                            <input type="text" class="form-control" id="tecni_telef_contactado"
                                                   name="tecni_telef_contactado"
                                                   value="{{$umonitoreo->tecni_telef_contactado}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="tecni_email">Correo electrónico 1</label>
                                    <input type="text" class="form-control" id="tecni_email" name="tecni_email"
                                           value="{{$umonitoreo->tecni_email}}">

                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="tecni_email2">Correo electrónico 2</label>
                                    <input type="text" class="form-control" id="tecni_email2" name="tecni_email2"
                                           value="{{$umonitoreo->tecni_email2}}">

                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="tecni_comunicacion">Comunicación rápida</label>
                                    <select id="tecni_comunicacion" name="tecni_comunicacion"
                                            value="{{$umonitoreo->tecni_comunicacion}}" class="form-control">
                                        <option value=""> SELECCIONE</option>
                                        <option value="WHATSAPP" <?php if ($umonitoreo->tecni_comunicacion == "WHATSAPP") {
                                            echo "selected";
                                        }?>> WHATSAPP
                                        </option>
                                        <option value="email" <?php if ($umonitoreo->tecni_comunicacion == "email") {
                                            echo "selected";
                                        }?>> CORREO ELECTRóNICO
                                        </option>
                                        <option value="msm" <?php if ($umonitoreo->tecni_comunicacion == "msm") {
                                            echo "selected";
                                        }?>> MENSAJE DE TEXTO
                                        </option>
                                    </select>

                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="estadollamada_tecnico">Estado de llamada</label>
                                    <select style="<?php if ($umonitoreo->estadollamada_tecnico == "CONTACTADO") {
                                        echo "color:White;background:#5FB404;width:550px;";
                                    } else {
                                        echo "color:White;background:#FA5858;width:550px;";
                                    }?>" id="estadollamada_tecnico" name="estadollamada_tecnico" class="form-control">
                                        <option value=""> SELECCIONE</option>
                                        <option value="CONTACTADO" <?php if ($umonitoreo->estadollamada_tecnico == "CONTACTADO") {
                                            echo "selected";
                                        }?>> CONTACTADO
                                        </option>
                                        <option value="NO CONTACTADO" <?php if ($umonitoreo->estadollamada_tecnico == "NO CONTACTADO") {
                                            echo "selected";
                                        }?>> NO CONTACTADO
                                        </option>
                                    </select>

                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="novedadllamada_tecnico">Problema de llamada</label>
                                    <select style="<?php if ($umonitoreo->novedadllamada_tecnico == "VOLVER A LLAMAR" or $umonitoreo->novedadllamada_tecnico == "NO CONTESTA") {
                                        echo "color:White;background:#D7DF01;width:550px;";
                                    } ?>" id="novedadllamada_tecnico" name="novedadllamada_tecnico"
                                            class="form-control">
                                        <option value=""> SELECCIONE</option>
                                        <option value="NUMERO INCORRECTO" <?php if ($umonitoreo->novedadllamada_tecnico == "NUMERO INCORRECTO") {
                                            echo "selected";
                                        }?>> NúMERO INCORRECTO
                                        </option>
                                    <!--<option value="NUMERO EQUIVOCADO" <?php if ($umonitoreo->novedadllamada_tecnico == "NUMERO EQUIVOCADO") {
                                        echo "selected";
                                    }?>> NúMERO EQUIVOCADO </option>-->
                                        <option value="SIN NUMERO" <?php if ($umonitoreo->novedadllamada_tecnico == "SIN NUMERO") {
                                            echo "selected";
                                        }?> > SIN NUMERO
                                        </option>
                                        <option value="NO CONTESTA" <?php if ($umonitoreo->novedadllamada_tecnico == "NO CONTESTA") {
                                            echo "selected";
                                        }?> > NO CONTESTA
                                        </option>
                                        <option value="VOLVER A LLAMAR" <?php if ($umonitoreo->novedadllamada_tecnico == "VOLVER A LLAMAR") {
                                            echo "selected";
                                        }?> > VOLVER A LLAMAR
                                        </option>
                                    </select>

                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="tecni_asistencia">Asistencia del Técnico de control</label>
                                    <select id="tecni_asistencia" name="tecni_asistencia" class="form-control">
                                        <option value=""> SELECCIONE</option>
                                        <option value="SI" <?php if ($umonitoreo->tecni_asistencia == "SI") {
                                            echo "selected";
                                        }?>> Sí
                                        </option>
                                        <option value="NO" <?php if ($umonitoreo->tecni_asistencia == "NO") {
                                            echo "selected";
                                        }?>> NO
                                        </option>
                                    </select>

                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="tecni_recibiocomunicado">Recepción de Comunicado</label>
                                    <select id="tecni_recibiocomunicado" name="tecni_recibiocomunicado"
                                            class="form-control">
                                        <option value=""> SELECCIONE</option>
                                        <option value="SI" <?php if ($umonitoreo->tecni_recibiocomunicado == "SI") {
                                            echo "selected";
                                        }?>> SI
                                        </option>
                                        <option value="NO" <?php if ($umonitoreo->tecni_recibiocomunicado == "NO") {
                                            echo "selected";
                                        }?>> NO
                                        </option>
                                        <option value="REVISARA" <?php if ($umonitoreo->tecni_recibiocomunicado == "REVISARA") {
                                            echo "selected";
                                        }?>> REVISARÁ
                                        </option>
                                    </select>

                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="tecni_motivo">Observación</label>
                                    <input type="text" class="form-control" id="tecni_motivo" name="tecni_motivo"
                                           value="{{$umonitoreo->tecni_motivo}}">
                                </div>
                            </div>
                        </div>

                       

                        <div hidden class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title">Información del corresponsable suplente</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body ">
                                <div class="form-group col-xs-12">
                                    <label for="corresponsable2">Nombres</label>
                                    <input type="text" class="form-control" id="corresponsable2" name="corresponsable2"
                                           value="{{$umonitoreo->corresponsable2}}">

                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="corresponsable2_celular">Celular</label>
                                    <input type="text" class="form-control" id="corresponsable2_celular"
                                           name="corresponsable2_celular"
                                           value="{{$umonitoreo->corresponsable2_celular}}">

                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="corresponsable2_telefono">teléfono</label>
                                    <input type="text" class="form-control" id="corresponsable2_telefono"
                                           name="corresponsable2_telefono"
                                           value="{{$umonitoreo->corresponsable2_telefono}}">

                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="corresponsable2_correo">Correo</label>
                                    <input type="text" class="form-control" id="corresponsable2_correo"
                                           name="corresponsable2_correo"
                                           value="{{$umonitoreo->corresponsable2_correo}}">

                                </div>
                            </div>
                        </div>

                        <div class="box box-primary">

                            <div class="box-header">
                                <!--                          <h3 class="box-title">Información de llamada</h3>-->
                                <h3 class="box-title">Actualizar Información</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body ">
                                <div hidden class="form-group col-xs-12">
                                    <label for="estadoll">Estado de llamada (Validación)</label>
                                    <input type="text" disabled class="form-control" id="estadoll" name="estadoll"
                                           value="{{$institucion->estadollamada}}">

                                </div>
                                <div hidden class="form-group col-xs-12">
                                    <label for="recepcion_clave_ineval">Recepción de claves del aplicador</label>
                                    <select id="recepcion_clave_ineval" name="recepcion_clave_ineval"
                                            class="form-control">
                                        <option value=""> SELECCIONE</option>
                                        <option value="SI" <?php if ($umonitoreo->recepcion_clave_ineval == "SI") {
                                            echo "selected";
                                        }?>> Sí
                                        </option>
                                        <option value="NO" <?php if ($umonitoreo->recepcion_clave_ineval == "NO") {
                                            echo "selected";
                                        }?>> NO
                                        </option>
                                    </select>

                                </div>
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-primary btn-block">Actualizar</button>
                                </div>
                            </div>
            </form>


        </div> <!-- end col mod 6 -->


    </div> <!-- end row -->
    <script>
        jQuery(document).ready(function ($) {

            $(function () {
                $('.toggleden').bootstrapToggle({
                    on: 'SI',
                    off: 'NO'

                });
            });

            $('.toggleden').on('change', function () {

                $(this).each(function () {

                    if ($(this).val() === "0" || $(this).val() === "1" || $(this).val() === "") {

                        var valor = parseInt($(this).val());
                        ///alert(valor);
                        var inputID = $(this).attr('href');
                        console.log("'" + inputID + "'  |  " + $(this).val());

                        switch (valor) {
                            case 1:
                                $(this).attr('value', parseInt(0));
                                $(this).attr('checked', false);
                                $('input[id=' + inputID + ']').val(0);
                                break;
                            case 0:
                                $(this).attr('value', parseInt(1));
                                $(this).attr('checked', true);
                                $('input[id=' + inputID + ']').val(1);
                                break;
                            case "":
                                $(this).attr('value', parseInt(1));
                                $(this).attr('checked', true);
                                $('input[id=' + inputID + ']').val(1);
                                break;
                            default:
                                $(this).attr('value', null);
                        }
                    }


                });
            });
        });
    </script>
    <script type="text/javascript">
        $('#estadollamada').on('change', function (e) {
            let $novedadLlamada = $('#novedadllamada option:selected').val();
            if (e.target.value === 'NO CONTACTADO' && $novedadLlamada === 'VOLVER A LLAMAR') {
                alert('No se puede guardar la novedad "VOLVER A LLAMAR" con el estado "NO CONTACTADO');
                $('#estadollamada').val('');
                return false;
            }
            if (e.target.value === 'CONTACTADO' && $novedadLlamada !== 'VOLVER A LLAMAR' &&
                $novedadLlamada !== "") {
                alert('Estado "CONTACTADO" solo permite novedad "VOLVER A LLAMAR"');
                $('#estadollamada').val('');
                return false;
            }
            if (e.target.value === 'NO CONTACTADO' && $novedadLlamada === 'VOLVER A LLAMAR') {
                alert('Estado "NO CONTACTADO" solo permite novedad "VOLVER A LLAMAR"');
                $('#estadollamada').val('');
                return false;
            }
            if (e.target.value === 'NO CONTACTADO') {
                if(validateEstadoLlamada()) {
                    alert('No se puede seleccionar estado "NO CONTACTADO" con información registrada.');
                    $('#estadollamada').val('');
                    return false;
                }
            }
        });
        $('#novedadllamada').on('change', function (e) {
            let $estadoLlamada = $('#estadollamada option:selected').val();
            if (e.target.value === 'VOLVER A LLAMAR' && $estadoLlamada === 'NO CONTACTADO') {
                alert('No se puede guardar la novedad "VOLVER A LLAMAR" con el estado "NO CONTACTADO');
                $('#novedadllamada').val('');
            }
            if ($estadoLlamada === 'CONTACTADO' && e.target.value !== 'VOLVER A LLAMAR'
                && e.target.value !== '') {
                alert('Estado "CONTACTADO" solo permite novedad "VOLVER A LLAMAR"');
                $('#novedadllamada').val('');
                return false;
            }
            if ($estadoLlamada === 'NO CONTACTADO' && e.target.value === 'VOLVER A LLAMAR'
                && e.target.value !== '') {
                alert('Estado "NO CONTACTADO" no permite novedad "VOLVER A LLAMAR"');
                $('#novedadllamada').val('');
                return false;
            }
        });
        $('#recibiocomunicado').on('change', function (e) {
            let $novedadLlamada = $('#novedadllamada option:selected').val();
            if (e.target.value !== 'SI' && $novedadLlamada !== 'VOLVER A LLAMAR') {
                alert('Si no recibió el comunicado, se debe poner la novedad "VOLVER A LLAMAR".');
                $('#recibiocomunicado').val('');
            }
            if ($estadoLlamada === 'CONTACTADO' && e.target.value !== 'VOLVER A LLAMAR'
                && e.target.value !== '') {
                alert('Estado "CONTACTADO" solo permite novedad "VOLVER A LLAMAR"');
                $('#novedadllamada').val('');
                return false;
            }
            if ($estadoLlamada === 'NO CONTACTADO' && e.target.value === 'VOLVER A LLAMAR'
                && e.target.value !== '') {
                alert('Estado "NO CONTACTADO" no permite novedad "VOLVER A LLAMAR"');
                $('#novedadllamada').val('');
                return false;
            }
        });
        $('#estadollamada_aplicadores').on('change', function (e) {
            /*let $novedadLlamada = $('#novedadllamada_aplicadores option:selected').val();
            let $apliRecep = $('#apli_recepcion option:selected').val();
            if (e.target.value === 'NO CONTACTADO' && $novedadLlamada === 'VOLVER A LLAMAR') {
                alert('No se puede guardar la novedad "VOLVER A LLAMAR" con el estado "NO CONTACTADO');
                $('#estadollamada_aplicadores').val('');
                return false;
            }
            if (e.target.value === 'NO CONTACTADO') {
                if(validateEstadoLlamadaApli()) {
                    alert('No se puede seleccionar estado "NO CONTACTADO" con información registrada.');
                    $('#estadollamada_aplicadores').val('');
                    return false;
                }
            }
            if ($apliRecep === 'SI') {
                alert('No se puede seleccionar estado "NO CONTACTADO" con información registrada.');
                $('#estadollamada_aplicadores').val('');
                return false;
            }*/
        });
        $('#novedadllamada_aplicadores').on('change', function (e) {
            let $estadoLlamada = $('#estadollamada_aplicadores option:selected').val();
            /*if (e.target.value === 'VOLVER A LLAMAR' && $estadoLlamada === 'NO CONTACTADO') {
                alert('No se puede guardar la novedad "VOLVER A LLAMAR" con el estado "NO CONTACTADO');
                $('#novedadllamada_aplicadores').val('');
            }*/
        });

        function validateEstadoLlamada() {
            let valida=false;
            if ($('#comunicacion').val() != ''
                || $('#recibiocomunicado').val() == 'SI' || $('#observacion').val() != '') {
                valida=true;
            }
            return valida;
        }

        function validateEstadoLlamadaApli() {
            let valida=false;
            /*$('.llamada2').each(function() {
                if(this.value)
                    valida=true;
            });*/
            return valida;
        }

        function validarIncompleta(e) {
            let $estadoLlamada = $('#estadollamada option:selected').val();
            let $novedadLlamada = $('#novedadllamada option:selected').val();
            let $novedadLlamadaApli = $('#novedadllamada_aplicadores option:selected').val();
            let incompleto = false;

            if ($novedadLlamada !== 'VOLVER A LLAMAR') {
                if ($('#comunicacion').val() === '') {
                    incompleto=true;
                }
                else if ($('#recibiocomunicado').val() === '') {
                    incompleto=true;
                }
                else if ($('#recibiocomunicado').val() !== 'SI' && $('#observacion').val() === '') {
                    incompleto=true;
                }

                 if (incompleto&& $estadoLlamada !== 'NO CONTACTADO') {
                    alert('Si la información no está completa, se selecciona la novedad "VOLVER A LLAMAR"');
                    $('#novedadllamada').val('VOLVER A LLAMAR');
                    e.cancelBubble=true;
                    e.preventDefault();
                    return false;
                }
            }

            /*if ($novedadLlamadaApli !== 'VOLVER A LLAMAR') {
                $('.llamada2').each(function() {
                    if(!this.value)
                        incompleto=true;
                });

                if (incompleto) {
                    alert('Si la información no está completa, se selecciona la novedad "VOLVER A LLAMAR"');
                    $('#novedadllamada_aplicadores').val('VOLVER A LLAMAR');
                    e.cancelBubble=true;
                    e.preventDefault();
                    return false;
                }
            }*/

            return true;
        }
    </script>