<div class="row">

    <div class="col-md-6">

        <div class="box box-primary">

            <div class="box-header">
                <h3 class="box-title">Información de descarga del aplicativo</h3>
            </div><!-- /.box-header -->

            <div id="notificacion_resul_feu"></div>


            <form id="f_editar_descarga" method="post" action="editar_descarga" class="form-horizontal form_entrada"
                  onsubmit="return validarInformacion(event);">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <!--id oculto-->
                <input type="hidden" name="id3" id="id3" value="<?= $id; ?>">
                <div class="box-body ">
                    <div class="form-group col-xs-12">
                        <label for="raplicativo">Recepción del aplicativo*</label>
                        <select id="raplicativo" name="raplicativo" class="form-control" required>
                            value="<?= $monitoreoInstalacion->recepcion_aplicativo; ?>">
                            <option value=""> SELECCIONE</option>
                            <option value="SI" <?php if ($monitoreoInstalacion->recepcion_aplicativo == "SI") {
                                echo "selected";
                            }?>> SI
                            </option>
                            <option value="NO" <?php if ($monitoreoInstalacion->recepcion_aplicativo == "NO") {
                                echo "selected";
                            }?>> NO
                            </option>
                        </select>
                    </div>
                    <div class="form-group col-xs-12">
                        <label for="rdaplicativo">Descarga del aplicativo*</label>
                        <select id="rdaplicativo" name="rdaplicativo" class="form-control" required
                                value="<?= $monitoreoInstalacion->descarga_aplicativo; ?>">
                            <option value=""> SELECCIONE</option>
                            <option value="SI" <?php if ($monitoreoInstalacion->descarga_aplicativo == "SI") {
                                echo "selected";
                            }?>> SI
                            </option>
                            <option value="NO" <?php if ($monitoreoInstalacion->descarga_aplicativo == "NO") {
                                echo "selected";
                            }?>> NO
                            </option>
                        </select>
                    </div>
                    <div class="form-group col-xs-12">
                        <label for="ndaplicativo">Problema de descarga*</label>
                        <select id="ndaplicativo" value="<?= $monitoreoInstalacion->observacion_descarga; ?>"
                                name="ndaplicativo" class="form-control">
                            <option value="SIN NOVEDAD"> Seleccione Problema</option>
                            <?php foreach($subCategoriaetiqueta as $tipo){  ?>

                            <option value="<?= $tipo->descripcion_cat; ?>" <?php if ($monitoreoInstalacion->observacion_descarga == $tipo->descripcion_cat) {
                                echo "selected";
                            }?>> <?= $tipo->descripcion_cat; ?> </option>

                            <?php } ?>

                        </select>
                    </div>
                    <div class="form-group col-xs-12">
                        <label for="codigopc">Código Hash PC</label>
                        <input type="text" class="form-control" id="codigopc"
                               name="codigopc" value="<?= $codigoVerificacionpc; ?>" readonly>
                    </div>
                    <div class="form-group col-xs-12">
                        <label for="hash_compu_ok">Código SHA-1 Correcto</label>
                        <select id="hash_compu_ok" name="hash_compu_ok" class="form-control">
                            <option value='SI' <?php if ($monitoreoInstalacion->hash_compu_ok == 'SI') {
                                echo "selected";
                            }?>>SI</option>
                            <option value='NO' <?php if ($monitoreoInstalacion->hash_compu_ok == 'NO') {
                                echo "selected";
                            }?>>NO</option>
                            <option value=null <?php if ($monitoreoInstalacion->hash_compu_ok != 'SI' and $monitoreoInstalacion->hash_compu_ok != 'NO') {
                                echo "selected";
                            }?>>Seleccione...</option>
                        </select>
                    </div>
                    <div class="form-group col-xs-12">
                        <label for="codigomovil">Código Hash MOVIL</label>
                        <input type="text" class="form-control" id="codigomovil"
                               name="codigomovil" value="<?= $codigoVerificacionmovil; ?>" readonly>
                    </div>
                    <div class="form-group col-xs-12">
                        <label for="hash_movil_ok">Código SHA-1 Correcto</label>
                        <select id="hash_movil_ok" name="hash_movil_ok" class="form-control">
                            <option value='SI' <?php if ($monitoreoInstalacion->hash_movil_ok == 'SI') {
                                echo "selected";
                            }?>>SI</option>
                            <option value='NO' <?php if ($monitoreoInstalacion->hash_movil_ok == 'NO') {
                                echo "selected";
                            }?>>NO</option>
                            <option value=null <?php if ($monitoreoInstalacion->hash_movil_ok != 'SI' AND $monitoreoInstalacion->hash_movil_ok != 'NO') {
                                echo "selected";
                            }?>>Seleccione...</option>
                        </select>
                    </div>

                </div>


                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Actualizar Registro</button>
                </div>
            </form>
        </div>

    </div> <!-- end col mod 6 -->

    <!--instlacion del aplicativo-->
    <div class="col-md-6">

        <div class="box box-primary">

            <div class="box-header">
                <h3 class="box-title">Información de instalación del aplicativo</h3>
            </div><!-- /.box-header -->

            <div id="notificacion_resul_feu"></div>


            <form id="f_editar_instalacion" method="post" action="editar_instalacion"
                  class="form-horizontal form_entrada"
                  onsubmit="return validarInstalacion(event);">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <!--id oculto-->
                <input type="hidden" name="id4" id="id4" value="<?= $id; ?>">

                <div class="box-body ">
                    <div class="form-group col-xs-12">
                        <label for="iaplicativo">Instalación del aplicativo*</label>
                        <select id="iaplicativo" required name="iaplicativo" class="form-control"
                                value="<?= $monitoreoInstalacion->instalo_aplicativo; ?>">
                            <option value=""> SELECCIONE</option>
                            <option value="SI" <?php if ($monitoreoInstalacion->instalo_aplicativo == "SI") {
                                echo "selected";
                            }?>> SI
                            </option>
                            <option value="NO" <?php if ($monitoreoInstalacion->instalo_aplicativo == "NO") {
                                echo "selected";
                            }?>> NO
                            </option>
                        </select>
                    </div>
                    <div class="form-group col-xs-12">
                        <label for="erequeridos">Equipos requeridos </label>
                        <input type="text" class="form-control" id="erequeridos" name="erequeridos"
                               value="{{$monitoreoInstalacion->programados}}" disabled>
                    </div>
                    <div class="form-group col-xs-12">
                        <label for="einstalados">Equipos Instalados </label>
                        <input type="text" class="form-control" id="einstalados" name="einstalados"
                               value="{{$monitoreoInstalacion->comp_inst}}" required>
                    </div>
                    <div class="form-group col-xs-12">
                        <label for="efaltantes">Equipos Faltantes</label>
                        <input type="text" class="form-control" id="efaltantes" name="efaltantes"
                               value="{{$monitoreoInstalacion->programados - $monitoreoInstalacion->comp_inst}}"
                               disabled>
                    </div>
                    <div class="form-group col-xs-12">
                        <label for="ninstlacion">Problema de instalación*</label>
                        <select id="ninstlacion" value="<?= $monitoreoInstalacion->observacion_instala; ?>"
                                name="ninstlacion" class="form-control">
                            <option value="SIN NOVEDAD"> Seleccione problema</option>
                            <?php foreach($subCategoriaetiquetaInsta as $tipo){  ?>

                            <option value="<?= $tipo->descripcion_cat; ?>" <?php if ($monitoreoInstalacion->observacion_instala == $tipo->descripcion_cat) {
                                echo "selected";
                            }?>> <?= $tipo->descripcion_cat; ?> </option>

                            <?php } ?>

                        </select>
                    </div>
                    <div class="form-group col-xs-12">
                        <label for="internet">Internet*</label>
                        <select id="internet" name="internet" class="form-control" required>
                            <option value=""> SELECCIONE</option>
                            <option value="SI" <?php if ($monitoreoInstalacion->internet == "SI") {
                                echo "selected";
                            }?> > SI
                            </option>
                            <option value="NO" <?php if ($monitoreoInstalacion->internet == "NO") {
                                echo "selected";
                            }?> > NO
                            </option>
                        </select>
                    </div>
                    <div class="form-group col-xs-12">
                        <label for="impresora">Impresora*</label>
                        <select id="impresora" name="impresora" class="form-control" required>
                            <option value=""> SELECCIONE</option>
                            <option value="SI" <?php if ($monitoreoInstalacion->impresora == "SI") {
                                echo "selected";
                            }?>> SI
                            </option>
                            <option value="NO" <?php if ($monitoreoInstalacion->impresora == "NO") {
                                echo "selected";
                            }?>> NO
                            </option>
                        </select>
                    </div>

                </div>


                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Actualizar Registro</button>
                </div>
            </form>

        </div>

    </div> <!-- end col mod 6 -->

</div> <!-- end row -->
<script type="text/javascript">
    function validarInformacion(e) {
        $recepcionAp = $('#raplicativo option:selected').val();
        $descargaAp = $('#rdaplicativo option:selected').val();
        $novedadAp = $('#ndaplicativo option:selected').val();

        if ($recepcionAp === 'NO' && $descargaAp === 'SI') {
            alert('No es posible confirmar la descarga sin haber confirmado la recepción del aplicatrivo.');
            $('#rdaplicativo').val('NO')
            e.cancelBubble = true;
            e.preventDefault();
            return false;
        }
        if ($recepcionAp === 'NO' && $novedadAp === 'SIN NOVEDAD') {
            alert('Por favor especifique el problema de la descarga del aplicativo.');
            e.cancelBubble = true;
            e.preventDefault();
            return false;
        }
        if ($descargaAp === 'SI' && $novedadAp !== 'SIN NOVEDAD') {
            alert('Si la descarga es correcta, no seleccione un problema de descarga.');
            $('#ndaplicativo').val('SIN NOVEDAD');
            e.cancelBubble = true;
            e.preventDefault();
            return false;
        }
        return true;
    }

    function validarInstalacion(e) {
        $instalacionAp = $('#iaplicativo option:selected').val();
        $novedadInstalacionAp = $('#ninstlacion option:selected').val();
        if ($instalacionAp === 'NO' && $novedadInstalacionAp === 'SIN NOVEDAD') {
            alert('Por favor especifique el problema de la instalación del aplicativo.');
            e.cancelBubble = true;
            e.preventDefault();
            return false;
        }
        if ($instalacionAp === 'SI' && $novedadInstalacionAp !== 'SIN NOVEDAD') {
            alert('Si la instalación es correcta, no seleccione un problema de instalación.');
            $('#ninstlacion').val('SIN NOVEDAD');
            e.cancelBubble = true;
            e.preventDefault();
            return false;
        }
        return true;
    }
</script>