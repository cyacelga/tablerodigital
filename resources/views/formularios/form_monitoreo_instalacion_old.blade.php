

<div class="row">  

  <div class="col-md-6" >

        <div class="box box-primary">
                        
                        <div class="box-header">
                          <h3 class="box-title">Información de descarga del aplicativo</h3>
                        </div><!-- /.box-header -->

        <div id="notificacion_resul_feu"></div>


        <form  id="f_editar_descarga"  method="post"  action="editar_descarga" class="form-horizontal form_entrada" >                
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"> 
          <!--id oculto-->              
          <input type="hidden" name="id3" id="id3" value="<?= $id; ?>"> 
        <div class="box-body ">
        <div class="form-group col-xs-12">
                              <label for="raplicativo">Recepción del aplicativo*</label>
                               <select id="raplicativo" name="raplicativo" class="form-control" required> value="<?= $monitoreoInstalacion->recepcion_aplicativo; ?>">
                                   <option value="" > SELECCIONE </option>
                                   <option value="SI" <?php if($monitoreoInstalacion->recepcion_aplicativo=="SI"){echo "selected";}?>> SI </option>
                                   <option value="NO" <?php if($monitoreoInstalacion->recepcion_aplicativo=="NO"){echo "selected";}?>> NO </option>
                               </select>
        </div>
        <div class="form-group col-xs-12">
                             <label for="rdaplicativo">Descarga del aplicativo*</label>
                               <select id="rdaplicativo" name="rdaplicativo" class="form-control" required value="<?= $monitoreoInstalacion->descarga_aplicativo; ?>">
                                   <option value="" > SELECCIONE </option>
                                   <option value="SI" <?php if($monitoreoInstalacion->descarga_aplicativo=="SI"){echo "selected";}?>> SI </option>
                                   <option value="NO" <?php if($monitoreoInstalacion->descarga_aplicativo=="NO"){echo "selected";}?>> NO </option>
                               </select>
        </div>
        <div class="form-group col-xs-12">
                             <label for="ndaplicativo">Problema de descarga*</label>
                               <select id="ndaplicativo" value = "<?= $monitoreoInstalacion->observacion_descarga; ?>" name="ndaplicativo" class="form-control">
                               <option value="SIN NOVEDAD" > Seleccione Problema </option>
                                <?php foreach($subCategoriaetiqueta as $tipo){  ?>
                                   
                                   <option value="<?= $tipo->descripcion_cat; ?>" <?php if($monitoreoInstalacion->observacion_descarga==$tipo->descripcion_cat){echo "selected";}?>> <?= $tipo->descripcion_cat; ?> </option>
                                
                                <?php } ?>
                                
                               </select>
        </div>
        <div class="form-group col-xs-12">
                              <label for="codigopc">Código Hash PC</label>
                              <input type="text" class="form-control" id="codigopc" style="<?php if($monitoreoInstalacion->hash_compu==$codigoVerificacionpc){echo "color:White;background:#5FB404;width:440px;";} else {echo "color:White;background:#FA5858;width:440px;";}?>" name="codigopc"  value="<?= $monitoreoInstalacion->hash_compu; ?>"  >
        </div>
        <div class="form-group col-xs-12">
                              <label for="codigomovil">Código Hash MOVIL</label>
                              <input type="text" class="form-control" id="codigomovil" style="<?php if($monitoreoInstalacion->hash_movil==$codigoVerificacionmovil){echo "color:White;background:#5FB404;width:440px;";} else {echo "color:White;background:#FA5858;width:440px;";}?>" name="codigomovil"  value="<?= $monitoreoInstalacion->hash_movil; ?>"  >
        </div>


        </div>



        <div class="box-footer">
             <button type="submit" class="btn btn-primary">Actualizar Registro</button>
        </div>
        </form>
        </div>

  </div> <!-- end col mod 6 -->

 <!--instlacion del aplicativo-->
 <div class="col-md-6" >

        <div class="box box-primary">
                        
                        <div class="box-header">
                          <h3 class="box-title">Información de instalación del aplicativo</h3>
                        </div><!-- /.box-header -->

        <div id="notificacion_resul_feu"></div>



        <form  id="f_editar_instalacion"  method="post"  action="editar_instalacion" class="form-horizontal form_entrada" >                
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"> 
          <!--id oculto-->     
          <input type="hidden" name="id4" id="id4" value="<?= $id; ?>">          

        <div class="box-body ">
        <div class="form-group col-xs-12">
                              <label for="iaplicativo">Instalación del aplicativo*</label>
                               <select id="iaplicativo" required name="iaplicativo" class="form-control" value="<?= $monitoreoInstalacion->instalo_aplicativo; ?>">
                                   <option value="" > SELECCIONE </option>
                                   <option value="SI" <?php if($monitoreoInstalacion->instalo_aplicativo=="SI"){echo "selected";}?>> SI </option>
                                   <option value="NO" <?php if($monitoreoInstalacion->instalo_aplicativo=="NO"){echo "selected";}?>> NO </option>
                               </select>
        </div>
        <div class="form-group col-xs-12">
                             <label for="erequeridos">Equipos requeridos </label>
                              <input type="text" class="form-control" id="erequeridos" name="erequeridos"  value="{{$monitoreoInstalacion->programados}}" disabled  >
        </div>
        <div class="form-group col-xs-12">
                             <label for="einstalados">Equipos Instalados </label>
                              <input type="text" class="form-control" id="einstalados" name="einstalados"  value="{{$monitoreoInstalacion->comp_inst}}" required >
        </div>
        <div class="form-group col-xs-12">
                              <label for="efaltantes">Equipos Faltantes</label>
                              <input type="text" class="form-control" id="efaltantes" name="efaltantes"  value="{{$monitoreoInstalacion->programados - $monitoreoInstalacion->comp_inst}}" disabled  >
        </div>
        <div class="form-group col-xs-12">
                              <label for="ninstlacion">Problema de instalación*</label>
                               <select id="ninstlacion" value = "<?= $monitoreoInstalacion->observacion_instala; ?>" name="ninstlacion" class="form-control">
                               <option value="SIN NOVEDAD" > Seleccione problema </option>
                                <?php foreach($subCategoriaetiquetaInsta as $tipo){  ?>
                                   
                                   <option value="<?= $tipo->descripcion_cat; ?>" <?php if($monitoreoInstalacion->observacion_instala==$tipo->descripcion_cat){echo "selected";}?>> <?= $tipo->descripcion_cat; ?> </option>
                                
                                <?php } ?>
                                
                               </select>
        </div>
         <div class="form-group col-xs-12">
                              <label for="internet">Internet*</label>
                               <select id="internet" name="internet" class="form-control" required>
                                   <option value="" > SELECCIONE </option>
                                   <option value="SI" <?php if($monitoreoInstalacion->internet=="SI"){echo "selected";}?> > SI </option>
                                   <option value="NO" <?php if($monitoreoInstalacion->internet=="NO"){echo "selected";}?> > NO </option>
                               </select>
        </div>
         <div class="form-group col-xs-12">
                              <label for="impresora">Impresora*</label>
                               <select id="impresora" name="impresora" class="form-control" required>
                                   <option value="" > SELECCIONE </option>
                                   <option value="SI" <?php if($monitoreoInstalacion->impresora=="SI"){echo "selected";}?>> SI </option>
                                   <option value="NO" <?php if($monitoreoInstalacion->impresora=="NO"){echo "selected";}?>> NO </option>
                               </select>
        </div>

        </div>



        <div class="box-footer">
             <button type="submit" class="btn btn-primary">Actualizar Registro</button>
        </div>
        </form>

        </div>

  </div> <!-- end col mod 6 -->

  <!--instlacion del aplicativo-->
 <div class="col-md-6">

        <div class="box box-primary">
                        
                        <div class="box-header">
                          <h3 class="box-title">Información de listado de asistencia</h3>
                        </div><!-- /.box-header -->

        <div id="notificacion_resul_feu"></div>

        <form  id="f_editar_listados"  method="post"  action="f_editar_listados" class="form-horizontal form_entrada" >                
          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"> 
          <!--id oculto-->     
          <input type="hidden" name="id5" id="id5" value="<?= $id; ?>">          

        <div class="box-body ">
         <div class="form-group col-xs-12">
                              <label for="listado_asistencia">Recepción de listado de asistencia*</label>
                               <select id="listado_asistencia" name="listado_asistencia" class="form-control" >
                                   <option value="" > SELECCIONE </option>
                                   <option value="SI" <?php if($monitoreoInstalacion->listado_asistencia=="SI"){echo "selected";}?> > SI </option>
                                   <option value="NO" <?php if($monitoreoInstalacion->listado_asistencia=="NO"){echo "selected";}?> > NO </option>
                               </select>
        </div>
        <div class="form-group col-xs-12">
                             <label for="observacion_listado_asistencia">Problema de listado de asistencia*</label>
                               <select id="observacion_listado_asistencia" value = "<?= $monitoreoInstalacion->observacion_listado_asistencia; ?>" name="observacion_listado_asistencia" class="form-control">
                               <option value="SIN NOVEDAD" > Seleccione Problema </option>
                                <?php foreach($subCategoriaetiquetaListado as $tipo){  ?>
                                   
                                   <option value="<?= $tipo->descripcion_cat; ?>" <?php if($monitoreoInstalacion->observacion_listado_asistencia==$tipo->descripcion_cat){echo "selected";}?>> <?= $tipo->descripcion_cat; ?> </option>
                                
                                <?php } ?>
                                
                               </select>
        </div>
         <div class="form-group col-xs-12">
                              <label for="listado_validacion">Cuadre de listado*</label>
                               <select id="listado_validacion" name="listado_validacion" class="form-control" >
                                   <option value="" > SELECCIONE </option>
                                   <option value="SI" <?php if($monitoreoInstalacion->listado_validacion=="SI"){echo "selected";}?>> SI </option>
                                   <option value="NO" <?php if($monitoreoInstalacion->listado_validacion=="NO"){echo "selected";}?>> NO </option>
                               </select>
        </div>

        </div>



        <div class="box-footer">
             <button type="submit" class="btn btn-primary">Actualizar Registro</button>
        </div>
        </form>

        </div>

  </div> <!-- end col mod 6 -->

</div> <!-- end row -->
