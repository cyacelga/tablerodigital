

<div class="box box-primary table-responsive">

                <div class="box-header">
                  <h3 class="box-title">Listado de Aplicadores</h3>
                </div>

<div class="box-body">              
<?php 

if( count($listadoAplicadores) >0){
?>

<table id="tabla_pacientes" Style="width:auto" class="table table-bordered table-striped dataTable" cellspacing="0" >
       
        <thead>
            <tr>
            <th>Sesión</th>
             <th>Identificación</th>
             <th>Nombres</th>
             <th>Teléfono</th>
             <th>Celular</th>
             <th>Correo institucional</th>
             <th>Correo personal</th>
             <th>Asistencia</th>
             <!--<th>Recepción de usuario y clave</th>-->
             <th>Estado llamada</th>
             <th>Problema llamada</th>
             <th>Observaciones</th>
             <th>Opciones</th>
            </tr>
        </thead>
 
       
<tbody>
@foreach($listadoAplicadores as $aplicadores)
 <tr role="row" class="odd" id="<?php echo $aplicadores->id; ?>">
 {{ Form::open( array('url' => 'editar_laboratorio/' . $aplicadores->id , 'name' => 'editar_monitoreoo' . $aplicadores->id , 'id' => 'editar_monitoreoo' . $aplicadores->id))}} 
    <td class="sorting_1">
{{$aplicadores->sesion}}
    </td>
    <td class="sorting_1">
{{$aplicadores->identificacion}}
    </td>
    <td class="sorting_1">
<input type="text" class="form-control" id="nombres" name="nombres"  value="{{$aplicadores->nombres}}">
    </td>
    <td>
<input type="text" class="form-control" id="telefono" name="telefono"  value="{{$aplicadores->telefono}}">
    </td>
    <td class="sorting_1">
<input type="text" class="form-control" id="celular" name="celular"  value="{{$aplicadores->celular}}">
    </td>
    <td class="sorting_1">
<input type="text" class="form-control" id="correoinstitucional" name="correoinstitucional"  value="{{$aplicadores->correoinstitucional}}">
    </td>
    <td class="sorting_1">
<input type="text" class="form-control" id="correopersonal" name="correopersonal"  value="{{$aplicadores->correopersonal}}">
    </td>
    <td class="sorting_1"> 
    <select id="asistencia" name="asistencia" class="form-control" value="{{$aplicadores->asistencia_apli}}" required>
                                   <option value="" > SELECCIONE </option>
                                   <option value="SI" <?php if($aplicadores->asistencia_apli=="SI"){echo "selected";}?>> SI </option>
                                   <option value="NO" <?php if($aplicadores->asistencia_apli=="NO"){echo "selected";}?>> NO </option>
          </select>
    
    
    </td>
    <!--<td class="sorting_1"> 
    <select id="claveyusuario" name="claveyusuario" class="form-control" value="{{$aplicadores->claveyusuario}}" required>
                                   <option value="" > SELECCIONE </option>
                                   <option value="SI" <?php if($aplicadores->claveyusuario=="SI"){echo "selected";}?>> SI </option>
                                   <option value="NO" <?php if($aplicadores->claveyusuario=="NO"){echo "selected";}?>> NO </option>
          </select>
    
    
    </td>-->
    <td>
                               <select style="<?php if($aplicadores->estadollamada_apli=="CONTACTADO"){echo "color:White;background:#5FB404;width:80px;";} else {echo "color:White;background:#FA5858;width:80px;";}?>" id="estadollamada" name="estadollamada" class="form-control">
                                   <option value="" > SELECCIONE </option>
                                    <option value="CONTACTADO" <?php if($aplicadores->estadollamada_apli=="CONTACTADO"){echo "selected";}?>> CONTACTADO </option>
                                   <option value="NO CONTACTADO" <?php if($aplicadores->estadollamada_apli=="NO CONTACTADO"){echo "selected";}?>> NO CONTACTADO </option>
                               </select>
    </td>
    <td>
                            <select style="<?php if($aplicadores->novedadllamada_apli=="VOLVER A LLAMAR" or $aplicadores->novedadllamada_apli=="NO CONTESTA") {echo "color:White;background:#D7DF01;width:80px;";} ?>" id="novedadllamada" name="novedadllamada" class="form-control">
                                   <option value="" > SELECCIONE </option>
                                   <option value="NUMERO INCORRECTO" <?php if($aplicadores->novedadllamada_apli=="NUMERO INCORRECTO"){echo "selected";}?>> NUMERO INCORRECTO </option>
                                   <option value="NUMERO EQUIVOCADO" <?php if($aplicadores->novedadllamada_apli=="NUMERO EQUIVOCADO"){echo "selected";}?>> NUMERO EQUIVOCADO </option>
                                   <option value="SIN NUMERO" <?php if($aplicadores->novedadllamada_apli=="SIN NUMERO"){echo "selected";}?> > SIN NUMERO </option>
                                   <option value="NO CONTESTA" <?php if($aplicadores->novedadllamada_apli=="NO CONTESTA"){echo "selected";}?> > NO CONTESTA </option>
                                   <option value="VOLVER A LLAMAR" <?php if($aplicadores->novedadllamada_apli=="VOLVER A LLAMAR"){echo "selected";}?> > VOLVER A LLAMAR </option>
                               </select>
    </td>
    <td>
    <select id="id_subcategoria_etiqueta" value = "<?= $aplicadores->observaciones; ?>" name="id_subcategoria_etiqueta" class="form-control">
                               <option value="" > Seleccione Novedad </option>
                                <?php foreach($subCategoriaetiqueta as $tipo){  ?>
                                   
                                   <option value="<?= $tipo->descripcion_cat; ?>" <?php if($aplicadores->observaciones==$tipo->descripcion_cat){echo "selected";}?>> <?= $tipo->descripcion_cat; ?> </option>
                                
                                <?php } ?>
                                
                               </select>


    </td>
    <td>
    {{ Form::submit('Actualizar', array('id' => 'submitUpdate9' . $aplicadores->id, 'href' => $aplicadores->id, 'class' => 'btn btn-block btn-primary submitUpdate9'))}}

    </td>
    {{ Form::close() }}
</tr>

@endforeach


  

    </table>



    <?php


echo str_replace('/?', '?', $listadoAplicadores->render() )  ;

}
else
{

?>


<br/><div class='rechazado'><label style='color:#FA206A'>...No se ha encontrado ningun usuario...</label>  </div> 

<?php
}

?>
</div>


<script>
jQuery( document ).ready( function( $ ) {   
    
    $(function() {
        $('.toggleden').bootstrapToggle({
            on: 'SI',
            off: 'NO'
            
        });
    });

        $('.toggleden').on('change', function(){
            
            $( this ).each(function(){
                
                if($(this).val() === "0" || $(this).val() === "1" || $(this).val() ===""){
                
                    var valor = parseInt($(this).val());
                    ///alert(valor);
                    var inputID = $(this).attr('href');
                        console.log( "'" + inputID + "'  |  " + $(this).val());
                    
                    switch(valor) {
                    case 1:
                        $(this).attr('value', parseInt(0));
                        $(this).attr('checked', false);
                        $( 'input[id=' + inputID + ']' ).val(0);
                        break;
                    case 0:
                        $(this).attr('value', parseInt(1));
                        $(this).attr('checked', true);
                        $( 'input[id=' + inputID + ']' ).val(1);
                        break;
                    case "":
                        $(this).attr('value', parseInt(1));
                        $(this).attr('checked', true);
                        $( 'input[id=' + inputID + ']' ).val(1);
                        break;
                    default:
                        $(this).attr('value', null);
                    }
                }
                
                
               
            });
      });   
  });  
</script>

<script>
  jQuery( document ).ready( function( $ ) {     


        $('.submitUpdate9').on('click', function(e){
            e.preventDefault();
            var monId = $(this).attr('href');
            ///sereal = $('#editar_monitoreoo' + monId).serialize();
            sereal =  $('#'+monId+' :input').serialize()+'&id='+monId;
             var id_sede=$("#id_sede").val();
             var form_data = $(this).serialize();
             ///var myUrl = 'editar_aplicadores/' + monId;
            var myUrl = 'editar_aplicadores/' + monId + '/' + id_sede;
            var sesion = $("#myoption").val();
            
                $.ajax({
                    url: myUrl,
                    type: 'POST',
                    ///cache: false,
                    data: sereal,
                    success: function(data){
                        notif({
                        msg: data.message,
                        type: data.status,
                        width: 300,
                        opacity: 1,
                         
                        });
                        // $("#SaveAlert").alert("Registro Actualizado");
                        // $('#search_results_div').html(data); 
                    },
                    error: function(xhr, textStatus, thrownError){
                        notif({
                        msg: "<b>¡Registro no guardado!</b>",
                        type: "error"

                        });                 // $('#SaveAlert').alert('Se produjo un Error.');
                    }
                });
            ///alert(sereal);
        });
    });
</script>
<script type="text/javascript">
    $('#estadollamada').on('change', function(e) {
        let $novedadLlamada = $('#novedadllamada option:selected').val();
        if(e.target.value === 'NO CONTACTADO' && $novedadLlamada === 'VOLVER A LLAMAR') {
            alert('No se puede guardar la novedad "VOLVER A LLAMAR" con el estado "NO CONTACTADO');
            $('#estadollamada').val('');
        }
    });
    $('#novedadllamada').on('change', function(e) {
        let $estadoLlamada = $('#estadollamada option:selected').val();
        if(e.target.value === 'VOLVER A LLAMAR' && $estadoLlamada === 'NO CONTACTADO') {
            alert('No se puede guardar la novedad "VOLVER A LLAMAR" con el estado "NO CONTACTADO');
            $('#novedadllamada').val('');
        }
    });
</script>