<!DOCTYPE html>
<html>
        
  <style type='text/css'>
		#loading{
		width:100%;
		height:100%;
                background-color:#ffffff;
		position:fixed;
		top:0;
		left:0;
		z-index:9999;
		opacity: 0.8;
		filter: alpha(opacity=80);
		}
		</style>
     
    <script type="text/javascript">
              
        function buscar_datos() {
            
           // var dialogo = $("#dialogo");
           // dialogo.dialog({
           //    open: function(ui){ $(".ui-dialog-titlebar-close", ui.dialogo).hide(); } 
           // });
           
            var id_periodo = $("#periodo").val();
            var fecha = $("#fecha_programada").val();
            var sesion = $("#sesion").val();
            var tp_reporte = $("#tp_reporte").val();
            var coordinador = ("0");
                            
            if (id_periodo === "" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "" || id_periodo === "0" && fecha === "0" && sesion === "0") {
                alert("Debe seleccionar un periodo");
            } else {
                $("#loading").css("display","block");
                if (id_periodo !== "" && fecha === "" && sesion === "") {
                    var url = "reporte_general/" + tp_reporte + "/" + id_periodo + "/" + coordinador +  "";
                } else {
                    if (id_periodo !== "" && fecha !== "" && sesion === "") {
                        var url = "reporte_general/" + tp_reporte + "/" + id_periodo + "/" + coordinador +  "/" + fecha + "";
                    } else {
                        var url = "reporte_general/" + tp_reporte + "/" + id_periodo + "/" + coordinador +  "/" + fecha + "/" + sesion + "";
                    }
                }  
                $("#contenido").html();
                $.get(url, function(resul) {
                    $("#contenido").html(resul);
                    // dialogo.dialog("close");
                    $("#loading").css("display","none")
                });
            }            
           }
        function mostrarOpcion(){
            var tp_reporte = $("#tp_reporte").val();
            
            if(tp_reporte !==''){
               $("#contenido").html('');
               $('#seleccion').css('display','block');
            }else{
               $('#seleccion').css('display','none');
            }
        }        
                           
    </script>

    <body class="hold-transition skin-blue sidebar-mini">
     
                <!-- Content Header (Page header) -->
               
                <section class="content-header"> 
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <select class="form-control" id="tp_reporte" onchange="mostrarOpcion();" >
                                <option value=""> Tipo de Reporte </option>
                                <option value="reporte_general">Reporte General</option>
                                <option value="descarga_aplicativo">Descarga de Aplicativo</option>
                                <option value="instalacion_aplicativo">Instalación de Aplicativo</option>
                                <option value="inicio_sesion">Inicio de Sesión</option>
                            </select>
                        </div>
                    </div><br>
                    <div class="row" id="seleccion" style="display: none;">
                        <div class="col-sm-12 col-md-3">
                            <select class="form-control" id="periodo">
                                <option value="0"> Seleccione Periodo </option>
                                @foreach($periodos as $periodo)
                                <option value="{{ $periodo->id }}">{{ $periodo->periodo }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-12 col-md-3">
                            <select class="form-control" id="fecha_programada" name="fecha_programada">
                                <option value="">Seleccione Fecha</option>
                            </select>                                                        
                        </div>
                        <div class="col-sm-12 col-md-3">
                            <select class="form-control" id="sesion">
                                <option value="">Seleccione Sesion</option>
                            </select>
                        </div>
                        <div class="col-sm-12 col-md-2">
                            <button class="btn btn-block btn-info" type="button" onclick="buscar_datos();"><b>Buscar</b></button>
                        </div>
                    </div>

                </section>   
                <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
                <!-- Main content -->
                   
                <div class="row" id="load" style="display: none;">
                        <img src="images/loading1.gif" style='margin:0 auto; position: absolute; top: 50%; left: 95%; margin: -230px 0 0 -30px;'>
                    </div>
                 <div class="row" id="loading" style="display: none;">
                        <img src="images/loading0.gif" style='margin:0 auto; position: absolute; top: 50%; left: 50%; margin: -30px 0 0 -30px;'>
                    </div>

                <section id="contenido" class="content">
                    
                   
                </section><!-- /.content -->


 

        <!-- Control Sidebar -->
        <!-- /.control-sidebar -->
        <!-- Add the sidebar's background. This div must be placed
             immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
    <script src="js/select.js"></script> <!-- consulta de los select -->
    
</body>
</html>
