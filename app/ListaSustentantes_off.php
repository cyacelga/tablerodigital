<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Session;

class ListaSustentantes_off extends Model
{
    protected $table = 'sig_programados';


    public function scopeBusqueda($query,$id_lab,$dato_sustentantes="")
     {

            if($id_lab==0){ 
            $resultado= $query->where('proceso_id', Session::get('Periodo', 0))->Where(function($q) use ($dato_sustentantes){
                                    $q->where('identificacion','like','%'.$dato_sustentantes.'%')
                                      ->orWhere('nombres','like', '%'.$dato_sustentantes.'%')
                                      ->orWhere('usuarios','like', '%'.$dato_sustentantes.'%');       
                                   });
            }
            else{
               
            $resultado= $query->where("laboratorio","=",$id_lab)->Where(function($q) use ($dato_sustentantes)  {
                                    $q->where('identificacion','like','%'.$dato_sustentantes.'%')
                                      ->orWhere('nombres','like', '%'.$dato_sustentantes.'%')
                                      ->orWhere('usuarios','like', '%'.$dato_sustentantes.'%');       
                                   });

             }                     
        
        return  $resultado;
     }
}