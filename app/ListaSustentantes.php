<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Session;

class ListaSustentantes extends Model
{
    protected $table = 'view_sustensesionlab_otielnet'; //_his


    public function scopeBusqueda($query,$id_lab,$dato_sustentantes="")
     {

            if($id_lab==0){ 
            $resultado= $query->where('cgi_periodo_id', Session::get('Periodo', 0))->Where(function($q) use ($dato_sustentantes){
                                    $q->where('apellidos','like','%'.$dato_sustentantes.'%')
                                      ->orWhere('nombres','like', '%'.$dato_sustentantes.'%')
                                      ->orWhere('cedula','like', '%'.$dato_sustentantes.'%')
                                      ->orWhere('status','like', '%'.$dato_sustentantes.'%');       
                                   });
            }
            else{
               
            $resultado= $query->where("cgi_laboratorio_id","=",$id_lab)->Where(function($q) use ($dato_sustentantes)  {
                                    $q->where('apellidos','like','%'.$dato_sustentantes.'%')
                                      ->orWhere('nombres','like', '%'.$dato_sustentantes.'%')
                                      ->orWhere('cedula','like', '%'.$dato_sustentantes.'%')
                                      ->orWhere('status','like', '%'.$dato_sustentantes.'%');       
                                   });

             }                     
        
        return  $resultado;
     }
}