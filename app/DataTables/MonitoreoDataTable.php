<?php

namespace App\DataTables;

use App\TipoEducacion;
use Yajra\Datatables\Services\DataTable;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class MonitoreoDataTable extends DataTable
{
    protected $alias = 'tipoeducacaion';

    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        $datatable = $this->datatables
            ->eloquent($this->query())
            ->editColumn('created_at', function ($user) {
                return $user->created_at ? with(new Carbon($user->created_at))->format('d/m/Y') : '';
            })
            ->editColumn('updated_at', function ($user) {
                return $user->updated_at ? with(new Carbon($user->updated_at))->format('d/m/Y') : '';
            });        
            $datatable->addColumn('action', function ($project) {
                $buttons = '';
                    $buttons .= '';
                return $buttons;
            });
        return $datatable->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $data = TipoEducacion::query();
        return $this->applyScopes($data);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->parameters($this->getParameters());
    }

    private function getColumns()
    {
        $columns = [
            'Id' => ['data' => 'id', 'name' => 'id', 'editable' => false, 'type' => 'text', 'visible' => true],
            'Proyecto' => ['data' => 'titulo', 'name' => 'titulo', 'editable' => true, 'type' => 'text', 'required' => true],
            'Creado' => ['data' => 'created_at', 'name' => 'created_at'],
            'Modificado' => ['data' => 'updated_at', 'name' => 'updated_at']
        ];
            $columns['Acciones'] = ['data' => 'action', 'name' => 'action', 'orderable' => false, 'searchable' => false, 'printable' => false, 'exportable' => false];
        
        return $columns;
    }

    private function getParameters()
    {
        $buttons = ['excel', 'print', 'reload'];
            array_unshift($buttons, 'createModal');
        $parameters = [
            'buttons' => $buttons
        ];
        return $parameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Proyectos' . date('_Ymd_hi');
    }
}
