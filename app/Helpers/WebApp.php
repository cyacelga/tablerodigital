<?php

namespace App\Helpers;
use App\Models\IntegracionAplicacione;

class WebApp{
    
    public static function verificarAutenticacion() {
        
        $url=getenv('URL_WEBAPP').'authentication/login';
        $token= IntegracionAplicacione::where('app','webApp')->where('campo','token')->first(['campo','valor']);
        $time= IntegracionAplicacione::where('app','webApp')->where('campo','time')->first(['campo','valor']);
        
        if((!isset($token->valor) || $token->valor == NULL) || (!isset($time->valor) || $time->valor == NULL) ||  date('Y-m-d H:i:s',$time->valor) < date('Y-m-d H:i:s')){                

             $client = new \GuzzleHttp\Client([
             'headers' => ['Content-Type' => 'application/json']
             ]);
             $data = array('username' => getenv('APP_SERVICE_USER'),
                           'password' => getenv('APP_SERVICE_PWD'));

             $json_str = json_encode($data);
             $response = $client->post($url,[
                 'body' => $json_str,
             ]);

             $respuesta = json_decode($response->getBody()->getContents());

             $token = IntegracionAplicacione::updateOrCreate(['app' => 'webApp', 'campo' => 'token'],
                                                                  ['valor' => $respuesta->access_token ]);

             $time = IntegracionAplicacione::updateOrCreate(['app' => 'webApp', 'campo' => 'time'],
                                                           ['valor' => time() +  $respuesta->expires_in - 10400 ]);

        } 
        return $token->valor;  
    }   
}

