<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategoriaEtiqueta extends Model
{
    protected $table = 'subcategoria_etiqueta';
    public $timestamps=false;
}
