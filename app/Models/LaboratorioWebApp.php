<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LaboratorioWebApp extends Model
{
    protected $connection='pgsql_webapp';
    protected $table='laboratorio';
    
    public function sesion($fecha, $sesion) {
        return $this->hasOne('\App\Models\SesionWebApp','laboratorio_id')->where('fecha',$fecha)->where('numero_sesion', 'SESION_'.$sesion);
    }
}
