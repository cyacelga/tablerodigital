<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IntegracionAplicacione extends Model
{
	protected $table='sig_integracion_aplicaciones';
	
    protected $fillable = [
        'app', 'campo', 'valor',
    ];
}
