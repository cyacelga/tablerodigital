<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SesionWebApp extends Model
{
    protected $connection='pgsql_webapp';
    protected $table='sesion';
    
}
