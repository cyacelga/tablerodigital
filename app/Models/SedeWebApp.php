<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SedeWebApp extends Model
{
    protected $connection='pgsql_webapp';
    protected $table='sede';
}
