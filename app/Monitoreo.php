<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Session;

class Monitoreo extends Model
{
    protected $table = 'umonitoreo';


   
/*
    public function informe2()
    {
        return $this->HasMany('App\SigInformeAplicacion', 'id_monitoreo', 'id');
    }*/

     public function scopeBusqueda($query,$fecha,$dato="")
     {

            if($fecha==0){ 
            $resultado= $query->where('estado', 1)->where('cgi_periodo_id', Session::get('Periodo', 0))->Where(function($q) use ($dato){
                                    $q->where('id_sede','like','%'.$dato.'%')
                                      ->orWhere('codigoamei','like', '%'.$dato.'%')
                                      ->orWhere('institucion','like', '%'.$dato.'%');       
                                   });
            }
            else{
               
            $resultado= $query->where("sesion","=",$fecha)->where('estado', 1)->where('cgi_periodo_id', Session::get('Periodo', 0))->Where(function($q) use ($dato)  {
                                    $q->where('id_sede','like','%'.$dato.'%')
                                      ->orWhere('codigoamei','like', '%'.$dato.'%')
                                      ->orWhere('institucion','like', '%'.$dato.'%');       
                                   });

             }                     
        
        return  $resultado;
     }
}
