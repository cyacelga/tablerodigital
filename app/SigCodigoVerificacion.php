<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SigCodigoVerificacion extends Model
{
    protected $table = 'sig_codigo_verificacion';
}
