<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoEducacion extends Model
{
     protected $table = 'sig_tiposeducacion';
}
