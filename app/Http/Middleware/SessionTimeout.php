<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Session\Store;

class SessionTimeout {

    protected $session;
    protected $timeout = 1200;

    public function __construct(Store $session){
        $this->session = $session;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //lastActivityTime
        $isLoggedIn = $request->path() != 'dashboard/logout';
        if(! session('Periodo'))
            $this->session->put('Periodo', time());
        elseif(time() - $this->Session->get('Periodo') > $this->timeout){
            $this->session->forget('Periodo');
            $cookie = cookie('intend', $isLoggedIn ? url()->current() : 'dashboard');
            $email = $request->user()->email;
            auth()->logout();
            return message('You had not activity in '.$this->timeout/60 .' minutes ago.', 'warning', 'login')->withInput(compact('email'))->withCookie($cookie);
        }
        $isLoggedIn ? $this->session->put('Periodo', time()) : $this->session->forget('Periodo');
        return $next($request);
    }

}