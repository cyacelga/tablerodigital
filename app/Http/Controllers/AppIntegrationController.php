<?php

namespace App\Http\Controllers;

use App\Monitoreo;
use Illuminate\Http\Request;

use App\Http\Requests;

class AppIntegrationController extends Controller
{
    /**
     * Guarda las respuestas que vienen de la app en la tabla umonitoreo
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cargarRespuestasApp(Request $request) {
        $respuesta = '';
        $status = 200;

        try {
            $idSede = $request->codigoLaboratorio;
            $fechaProgramada = substr($request->fechaHoraRespuesta, 0, 10);
            $horaInicio = $request->horaInicio;
            //$periodo = $request->periodo;
            $pregunta = $request->identificadorPregunta;
            $tipoRespuesta = $request->tipoRespuesta;
            $rValor = $request->valor;
            $sesion = '';
            $sesionConcat = '';

            switch ($request->numeroSesion) {
                case 'SESION_1':
                    $sesion = 'S1';
                    break;
                case 'SESION_2':
                    $sesion = 'S2';
                    break;
                case 'SESION_3':
                    $sesion = 'S3';
                    break;
                case 'SESION_4':
                    $sesion = 'S4';
                    break;
                default:
                    $sesion = '';
                    break;
            }
            $sesionConcat = $fechaProgramada . '-' . $sesion;

            $monitoreo = Monitoreo::where('id_sede', $idSede)
                ->where('fecha_programada_inicio', $fechaProgramada)
                ->where('sesion', $sesionConcat)->first();

            if (!$monitoreo) {
                $respuesta = array('codigo' => 404, 'mensaje' => 'No se encontró ese registro en el monitor.');
                return response()->json($respuesta, 404);
            }

            if (substr($pregunta, 0, 17) === 'FORM_DURANTE_FOTO'
                || substr($pregunta, 0, 14) === 'FORM_POST_FOTO') {

                $observaciones = '';
                $ruta = $rValor;

                switch ($pregunta) {
                    case 'FORM_DURANTE_FOTO_ASISTENTES_PAG_1':
                        $observaciones = 'Foto Durante - Asistentes Página 1';
                        break;
                    case 'FORM_DURANTE_FOTO_ASISTENTES_PAG_2':
                        $observaciones = 'Foto Durante - Asistentes Página 2';
                        break;
                    case 'FORM_DURANTE_FOTO_ASISTENTES_PAG_3':
                        $observaciones = 'Foto Durante - Asistentes Página 3';
                        break;
                    case 'FORM_POST_FOTO_ASISTENTES_FINAL_PAG_1':
                        $observaciones = 'Foto Post - Asistentes Página 1';
                        break;
                    case 'FORM_POST_FOTO_ASISTENTES_FINAL_PAG_2':
                        $observaciones = 'Foto Post - Asistentes Página 2';
                        break;
                    case 'FORM_POST_FOTO_ASISTENTES_FINAL_PAG_3':
                        $observaciones = 'Foto Post - Asistentes Página 3';
                        break;
                }

                $idSubEtiqueta = getenv('APP_ID_SUBCATEGORIA_ETIQUETA');
                try {
                    InformeController::agregar_imagenes_app($monitoreo->id2, $idSubEtiqueta, $observaciones, $ruta);
                    return response()->json($respuesta, $status);
                } catch (\Exception $ex) {
                    $respuesta = array('codigo' => 500, 'mensaje' => $ex->getMessage());
                    $status = 500;
                    return response()->json($respuesta, $status);
                }
            }

            switch ($pregunta) {
                case 'FORM_PREVIO_COMUNICADO':
                    $monitoreo->recepcion_aplicativo = $rValor;
                    break;
                case 'FORM_PREVIO_CUANTAS_INSTALACIONES':
                    $monitoreo->comp_inst = $rValor;
                    break;
                case 'FORM_PREVIO_TIENE_INTERNET':
                    $monitoreo->internet = $rValor;
                    break;
                case 'FORM_PREVIO_TIENE_IMPRESORA':
                    $monitoreo->impresora = $rValor;
                    break;
                case 'FORM_DURANTE_INICIO_SESION':
                    $monitoreo->estado_sesion = $rValor;
                    break;
                case 'FORM_DURANTE_ASISTENTES':
                    $monitoreo->asistencia = $rValor;
                    break;
                case 'FORM_POST_RESUMEN_EVALUADOS':
                    $monitoreo->asistencia = $rValor;
                    break;
                case 'FORM_POST_RESUMEN_REPROGRAMADOS':
                    $monitoreo->reprogramacion = $rValor;
                    break;
                case 'FORM_PREVIO_DESCARGA':
                    $monitoreo->descarga_aplicativo = $rValor;
                    break;
                case 'FORM_PREVIO_INSTALACION':
                    $monitoreo->instalo_aplicativo = $rValor;
                    break;
            }
            $monitoreo->save();
            return response()->json($respuesta, $status);
        } catch (\Exception $ex) {
            $respuesta = array('codigo' => 500, 'mensaje' => $ex->getMessage());
            $status = 500;
            return response()->json($respuesta, $status);
        }
    }

    public function cambioUsuariosApp($rol, $monitoreo, $siginstitucion) {
        $respuesta = '';

        try {
            $activarIntegracion = getenv('APP_ACTIVAR_INTEGRACION');
            if($activarIntegracion === 'false')
                return $respuesta;

            $client = new \GuzzleHttp\Client([
                'headers' => ['Content-Type' => 'application/json']
            ]);
            $url = getenv('URL_APP_USERS_SERVICE');
            $user = getenv('APP_SERVICE_USER');
            $pwd = getenv('APP_SERVICE_PWD');
            $data = '';

            if ($rol === 'APLICADOR') {
                $data = array('cedula' => $monitoreo->apli_cedula,
                    'nombre' => $monitoreo->apli_nombre1,
                    'correo1' => $monitoreo->apli_email,
                    'correo2' => $monitoreo->apli_email2,
                    'telefono1' => $monitoreo->apli_telef3,
                    'telefono2' => $monitoreo->apli_telef2,
                    'celular' => $monitoreo->apli_telef1,
                    'rol' => 'APLICADOR',
                    'codigoLaboratorio' => $monitoreo->id_sede,
                    'numeroSesion' => 'SESION_'.substr($monitoreo->sesion, -1),
                    'codigoZona' => null,
                    'codigoProvincia' => null,
                    'codigoSede' => null);
            }
            if ($rol === 'RESPONSABLE_SEDE') {
                $data = array('cedula' => $siginstitucion->numero_id_rector,
                    'nombre' => $siginstitucion->rector,
                    'correo1' => $siginstitucion->correoinstitucion,
                    'correo2' => $siginstitucion->correo,
                    'telefono1' => $siginstitucion->telefono1,
                    'telefono2' => $siginstitucion->telefono2,
                    'celular' => $siginstitucion->celular,
                    'rol' => 'RESPONSABLE_SEDE',
                    'codigoLaboratorio' => null,
                    'numeroSesion' => null,
                    'codigoZona' => null,
                    'codigoProvincia' => null,
                    'codigoSede' => $siginstitucion->amie);
            }
            if ($rol === 'SUPERVISOR_TECNICO') {
                $data = array('cedula' => $monitoreo->tecni_cedula,
                    'nombre' => $monitoreo->tecni_nombre1,
                    'correo1' => $monitoreo->tecni_email,
                    'correo2' => $monitoreo->tecni_email2,
                    'telefono1' => $monitoreo->tecni_telef1,
                    'telefono2' => $monitoreo->tecni_telef2,
                    'celular' => $monitoreo->tecni_telef3,
                    'rol' => 'SUPERVISOR_TECNICO',
                    'codigoLaboratorio' => $monitoreo->id_sede,
                    'numeroSesion' => 'SESION_'.substr($monitoreo->sesion, -1),
                    'codigoZona' => null,
                    'codigoProvincia' => null,
                    'codigoSede' => null);
            }
            if ($rol === 'SUPERVISOR_PROVINCIAL') {
                $codigoProvincia = '';
                switch ($monitoreo->provincia) {
                    case 'AZUAY':
                        $codigoProvincia = '01';
                        break;
                    case 'BOLIVAR':
                        $codigoProvincia = '02';
                        break;
                    case 'CAÑAR':
                        $codigoProvincia = '03';
                        break;
                    case 'CARCHI':
                        $codigoProvincia = '04';
                        break;
                    case 'COTOPAXI':
                        $codigoProvincia = '05';
                        break;
                    case 'CHIMBORAZO':
                        $codigoProvincia = '06';
                        break;
                    case 'EL ORO':
                        $codigoProvincia = '07';
                        break;
                    case 'ESMERALDAS':
                        $codigoProvincia = '08';
                        break;
                    case 'GUAYAS':
                        $codigoProvincia = '09';
                        break;
                    case 'IMBABURA':
                        $codigoProvincia = '10';
                        break;
                    case 'LOJA':
                        $codigoProvincia = '11';
                        break;
                    case 'LOS RIOS':
                        $codigoProvincia = '12';
                        break;
                    case 'MANABI':
                        $codigoProvincia = '13';
                        break;
                    case 'MORONA SANTIAGO':
                        $codigoProvincia = '14';
                        break;
                    case 'NAPO':
                        $codigoProvincia = '15';
                        break;
                    case 'PASTAZA':
                        $codigoProvincia = '16';
                        break;
                    case 'PICHINCHA':
                        $codigoProvincia = '17';
                        break;
                    case 'TUNGURAHUA':
                        $codigoProvincia = '18';
                        break;
                    case 'ZAMORA CHINCHIPE':
                        $codigoProvincia = '19';
                        break;
                    case 'GALAPAGOS':
                        $codigoProvincia = '20';
                        break;
                    case 'SUCUMBIOS':
                        $codigoProvincia = '21';
                        break;
                    case 'ORELLANA':
                        $codigoProvincia = '22';
                        break;
                    case 'STO DGO TSACHILAS':
                        $codigoProvincia = '23';
                        break;
                    case 'SANTA ELENA':
                        $codigoProvincia = '24';
                        break;
                    case 'ZONA NO DELIMITADA':
                        $codigoProvincia = '90';
                        break;
                    default:
                        $codigoProvincia = '';
                }
                $data = array('cedula' => $monitoreo->corresponsable_cedula,
                    'nombre' => $monitoreo->corresponsable,
                    'correo1' => $monitoreo->corresponsable_correo,
                    'correo2' => $monitoreo->corresponsable_correo2,
                    'telefono1' => $monitoreo->corresponsable_telefono,
                    'telefono2' => $monitoreo->corresponsable_telefono,
                    'celular' => $monitoreo->corresponsable_celular,
                    'rol' => 'SUPERVISOR_PROVINCIAL',
                    'codigoLaboratorio' => $monitoreo->id_sede,
                    'numeroSesion' => 'SESION_'.substr($monitoreo->sesion, -1),
                    'codigoZona' => null,
                    'codigoProvincia' => $codigoProvincia,
                    'codigoSede' => null);
            }

            $json_str = json_encode($data);
            $response = $client->post($url, [
                'auth' => [
                    $user,
                    $pwd
                ],
                //body instead of json
                'body' => $json_str,
            ]);
            $respuesta = json_decode($response->getBody()->getContents());

            return $respuesta;
        } catch (\Exception $ex) {
            $respuesta = $ex->getMessage();
            return $respuesta;
        }
    }
}
