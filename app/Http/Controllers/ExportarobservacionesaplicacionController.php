<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Maatwebsite\Excel\Facades\Excel;

class ExportarobservacionesaplicacionController extends Controller
{
    
   public function observacion_descarga($id, $fch, $ss)
    {
       if($fch == 0){$fecha = '';}else{ $fecha = "AND fecha_programada_inicio='$fch'"; }        
       if( $ss ==0){$sesion = '';}else{$sesion = "AND sesion='$ss'";}  
       
         $export_info_apli = \DB::select("SELECT codigoamei, id_sede, cgi_laboratorio_id, institucion, fecha_programada_inicio, sesion, zona, provincia, distrito_id, coordinador, monitor, name_monitor,  sum(programados) as sustentantes_afect, 
descarga_aplicativo, (case when observacion_descarga='' then 'SIN DESCARGAR APLICATIVO' 
                 when observacion_descarga IS NULL then 'SIN DESCARGAR APLICATIVO'
                 when descarga_aplicativo='' and observacion_descarga='SIN PROBLEMA' then 'SIN DESCARGAR APLICATIVO'
                 when descarga_aplicativo IS NULl and observacion_descarga='SIN PROBLEMA' then 'SIN DESCARGAR APLICATIVO'
                 when descarga_aplicativo ='NO' and observacion_descarga='SIN PROBLEMA' then 'SIN PROBLEMA REGISTRADO'
                 when descarga_aplicativo IS NULL and observacion_descarga!='SIN PROBLEMA' then 'SIN SELECCIONAR DESCARGA DEL APLICATIVO'
                 else observacion_descarga end) as observacion_descarga, count(*) AS totalobservacion, sum(programados) as sustentantes_afect,
                 sesion_aplicador as asistencia_responsable, estadollamada_apli, novedadllamada_apli FROM umonitoreo 
                 where cgi_periodo_id = $id $fecha $sesion AND estado=1 and (descarga_aplicativo is null or descarga_aplicativo=''  or descarga_aplicativo ='NO')
group by codigoamei, id_sede, cgi_laboratorio_id, institucion, fecha_programada_inicio, sesion, zona, provincia, distrito_id, coordinador, monitor, name_monitor, descarga_aplicativo, observacion_descarga, 
sesion_aplicador, estadollamada_apli, novedadllamada_apli");    
                
    Excel::create('DACT_APLICACION_OBSERVACIONES_DESCARGA', function($excel) use ($export_info_apli) {
        
         $excel->sheet('DD', function($sheet){
       
        // Header
        //$sheet->mergeCells('A1:B1');        
        $sheet->row(1, ['CAMPO','DESCRIPCIÓN']);
        $sheet->row(2, ['CÓDIGO AMIE','CÓDIGO PERTENECIENTE A LA INSTITUCIÓN EDUCATIVA']);
        $sheet->row(3, ['INSTITUCIÓN','NOMBRE DE LA INSTITUCIÓN EDUCATIVA']);
        $sheet->row(4, ['FECHA PROGRAMADA','FECHA PROGRAMADA DE LA EVALUACIÓN']);
        $sheet->row(5, ['SESIÓN','SESIÓN DE LA EVALUACIÓN']);
        $sheet->row(6, ['ZONA','ZONA A LA QUE PERTENECE LA INSTITUCIÓN EDUCATIVA']);
        $sheet->row(7, ['PROVINCIA','PROVINCIA A LA QUE PERTENECE LA INSTITUCIÓN']);
        $sheet->row(8, ['DISTRITO','DISTRITO AL QUE PERTENECE LA INSTITUCIÓN']);
        $sheet->row(9, ['COORDINADOR','COORDINADOR ZONAL ENCARGADO DE TENER COMUNICACIÓN CON LA INSTITUCIÓN']);
        $sheet->row(10, ['CÓDIGO MONITOR','CÓDIGO MONITOR']);
        $sheet->row(11, ['NOMBRE MONITOR','NOMBRE DEL MONITOR ENCARGADO DE TENER COMUNICACIÓN CON LA INSTITUCIÓN']);
        $sheet->row(12, ['DESCARGÓ APLICATIVO','CONFIRMACIÓN SI EL RESPONSABLE DE SEDE DESCARGÓ EL APLICATIVO']);
        $sheet->row(13, ['OBSERVACIÓN DESCARGA','OBSERVACIÓN SI OCURRIO UN PROBLEMA CON LA DESCARGA']);
        $sheet->row(14, ['SUSTENTANTES AFECTADOS','SUSTENTANTES AFECTADOS POR NO INICIAR SESIÓN']);
        $sheet->row(15, ['ASISTENCIA RESPONSABLE SEDE','ASISTENCIA RESPONSABLE DE SEDE']);
        $sheet->row(16, ['ESTADO DE LLAMADA','ESTADO DE LLAMADA']);
        $sheet->row(17, ['PROBLEMA DE LLAMADA','PROBLEMA DE LLAMADA']);
        $sheet->row(18, ['LABORATORIO','CÓDIGO DE LABORATORIO']);


            
        $sheet->cells('A1:B1', function($cells){
            $cells->setBackground('#0489B1');
            $cells->setFontColor('#FFFFFF');
            $cells->setAlignment('center');
            $cells->setValignment('center');
            $cells->setFontweight('bold');
        });
                 
        });
        
        $excel->sheet('Datos', function($sheet) use ($export_info_apli) {
       
          $sheet->row(1, ['CÓDIGO AMIE', 'INSTITUCIÓN', 'FECHA PROGRAMADA', 'SESIÓN', 'ZONA', 'PROVINCIA', 'DISTRITO', 'COORDINADOR', 'CÓDIGO MONITOR', 'NOMBRE MONITOR',
          'DESCARGÓ APLICATIVO', 'OBSERVACIÓN DESCARGA', 'SUSTENTANTES AFECTADOS', 'ASISTENCIA RESPONSABLE SEDE', 'ESTADO DE LLAMADA', 'PROBLEMA DE LLAMADA', 'LABORATORIO'
            ]);  
            
        $sheet->cells('A1:Q1', function($cells){
            $cells->setBackground('#0489B1');
            $cells->setFontColor('#FFFFFF');
            $cells->setAlignment('center');
            $cells->setValignment('center');
            $cells->setFontweight('bold');
        });
        
        // Data       
        $export_info_apli;
        
        foreach ($export_info_apli as $llamadas){
        
            $row = [];
            $row[0] = $llamadas->codigoamei;
            $row[1] = $llamadas->institucion; 
            $row[2] = $llamadas->fecha_programada_inicio;
            $row[3] = $llamadas->sesion;           
            $row[4] = $llamadas->zona;
            $row[5] = $llamadas->provincia;
            $row[6] = $llamadas->distrito_id;
            $row[7] = $llamadas->coordinador;
            $row[8] = $llamadas->monitor;
            $row[9] = $llamadas->name_monitor;
            $row[10] = $llamadas->descarga_aplicativo;
            $row[11] = $llamadas->observacion_descarga;
            $row[12] = $llamadas->sustentantes_afect;
            $row[13] = $llamadas->asistencia_responsable;
            $row[14] = $llamadas->estadollamada_apli;
            $row[15] = $llamadas->novedadllamada_apli; 
            $row[16] = $llamadas->id_sede;          
            
            $sheet->appendRow($row);
            }                
        });       
        
    })->export('xlsx');
    
    }
    
    public function observacion_instalo($id, $fch, $ss)
    {
       if($fch == 0){$fecha = '';}else{ $fecha = "AND fecha_programada_inicio='$fch'"; }        
       if( $ss ==0){$sesion = '';}else{$sesion = "AND sesion='$ss'";}  
       
         $export_info_apli = \DB::select("SELECT codigoamei, id_sede, cgi_laboratorio_id, institucion, fecha_programada_inicio, sesion, zona, provincia, distrito_id, coordinador, monitor, name_monitor,  sum(programados) as sustentantes_afect, 
instalo_aplicativo, (case when observacion_instala='' then 'SIN INSTALAR APLICATIVO' 
                 when observacion_instala IS NULL then 'SIN INSTALAR APLICATIVO'
                 when instalo_aplicativo='' and observacion_instala='SIN PROBLEMA' then 'SIN INSTALAR APLICATIVO'
                 when instalo_aplicativo IS NULl and observacion_instala='SIN PROBLEMA' then 'SIN INSTALAR APLICATIVO'
                 when instalo_aplicativo ='NO' and observacion_instala='SIN PROBLEMA' then 'SIN PROBLEMA REGISTRADO'
                 when instalo_aplicativo ='NO' and observacion_instala='SIN NOVEDAD' then 'SIN PROBLEMA REGISTRADO'
                 when instalo_aplicativo ='NO' and observacion_instala is null then 'SIN PROBLEMA REGISTRADO'
                 when instalo_aplicativo IS NULL and observacion_instala!='SIN PROBLEMA' then 'SIN SELECCIONAR INSTALACION DEL APLICATIVO'
                 when instalo_aplicativo IS NULL and observacion_instala is not null then 'SIN SELECCIONAR INSTALACION DEL APLICATIVO'
                 else observacion_instala end) as observacion_instala, count(*) AS totalobservacion, sum(programados) as sustentantes_afect,
                 sesion_aplicador as asistencia_responsable, estadollamada_apli, novedadllamada_apli FROM umonitoreo 
                 where cgi_periodo_id = $id $fecha $sesion AND estado=1 and (instalo_aplicativo is null or instalo_aplicativo=''  or instalo_aplicativo ='NO')
group by codigoamei, id_sede, cgi_laboratorio_id, institucion, fecha_programada_inicio, sesion, zona, provincia, distrito_id, coordinador, monitor, name_monitor, instalo_aplicativo, observacion_instala, 
sesion_aplicador, estadollamada_apli, novedadllamada_apli");    
                
    Excel::create('DACT_APLICACION_OBSERVACIONES_INSTALACION', function($excel) use ($export_info_apli) {
        
         $excel->sheet('DD', function($sheet){
       
        // Header
        //$sheet->mergeCells('A1:B1');        
        $sheet->row(1, ['CAMPO','DESCRIPCIÓN']);
        $sheet->row(2, ['CÓDIGO AMIE','CÓDIGO PERTENECIENTE A LA INSTITUCIÓN EDUCATIVA']);
        $sheet->row(3, ['INSTITUCIÓN','NOMBRE DE LA INSTITUCIÓN EDUCATIVA']);
        $sheet->row(4, ['FECHA PROGRAMADA','FECHA PROGRAMADA DE LA EVALUACIÓN']);
        $sheet->row(5, ['SESIÓN','SESIÓN DE LA EVALUACIÓN']);
        $sheet->row(6, ['ZONA','ZONA A LA QUE PERTENECE LA INSTITUCIÓN EDUCATIVA']);
        $sheet->row(7, ['PROVINCIA','PROVINCIA A LA QUE PERTENECE LA INSTITUCIÓN']);
        $sheet->row(8, ['DISTRITO','DISTRITO AL QUE PERTENECE LA INSTITUCIÓN']);
        $sheet->row(9, ['COORDINADOR','COORDINADOR ZONAL ENCARGADO DE TENER COMUNICACIÓN CON LA INSTITUCIÓN']);
        $sheet->row(10, ['CÓDIGO MONITOR','CÓDIGO MONITOR']);
        $sheet->row(11, ['NOMBRE MONITOR','NOMBRE DEL MONITOR ENCARGADO DE TENER COMUNICACIÓN CON LA INSTITUCIÓN']);
        $sheet->row(12, ['INSTALÓ APLICATIVO','CONFIRMACIÓN SI EL APLICATIVO FUE INSTALADO']);
        $sheet->row(13, ['OBSERVACIÓN INSTALACIÓN','OBSERVACIÓN PORQUE NO SE PUDO INSTALAR EL APLICATIVO']);
        $sheet->row(14, ['SUSTENTANTES AFECTADOS','SUSTENTANTES AFECTADOS POR NO INICIAR SESIÓN']);
        $sheet->row(15, ['ASISTENCIA RESPONSABLE SEDE','ASISTENCIA RESPONSABLE DE SEDE']);
        $sheet->row(16, ['ESTADO DE LLAMADA','ESTADO DE LLAMADA']);
        $sheet->row(17, ['PROBLEMA DE LLAMADA','PROBLEMA DE LLAMADA']);
        $sheet->row(18, ['LABORATORIO','CÓDIGO DE LABORATORIO']);


            
        $sheet->cells('A1:B1', function($cells){
            $cells->setBackground('#0489B1');
            $cells->setFontColor('#FFFFFF');
            $cells->setAlignment('center');
            $cells->setValignment('center');
            $cells->setFontweight('bold');
        });
                 
        });
        
        $excel->sheet('Datos', function($sheet) use ($export_info_apli) {
       
          $sheet->row(1, ['CÓDIGO AMIE', 'INSTITUCIÓN', 'FECHA PROGRAMADA', 'SESIÓN', 'ZONA', 'PROVINCIA', 'DISTRITO', 'COORDINADOR', 'CÓDIGO MONITOR', 'NOMBRE MONITOR',
          'INSTALÓ APLICATIVO', 'OBSERVACIÓN INSTALACIÓN', 'SUSTENTANTES AFECTADOS', 'ASISTENCIA RESPONSABLE SEDE', 'ESTADO DE LLAMADA', 'PROBLEMA DE LLAMADA', 'LABORATORIO'
            ]);  
            
        $sheet->cells('A1:Q1', function($cells){
            $cells->setBackground('#0489B1');
            $cells->setFontColor('#FFFFFF');
            $cells->setAlignment('center');
            $cells->setValignment('center');
            $cells->setFontweight('bold');
        });
        
        // Data       
        $export_info_apli;
        
        foreach ($export_info_apli as $llamadas){
        
            $row = [];
            $row[0] = $llamadas->codigoamei;
            $row[1] = $llamadas->institucion; 
            $row[2] = $llamadas->fecha_programada_inicio;
            $row[3] = $llamadas->sesion;           
            $row[4] = $llamadas->zona;
            $row[5] = $llamadas->provincia;
            $row[6] = $llamadas->distrito_id;
            $row[7] = $llamadas->coordinador;
            $row[8] = $llamadas->monitor;
            $row[9] = $llamadas->name_monitor;
            $row[10] = $llamadas->instalo_aplicativo;
            $row[11] = $llamadas->observacion_instala;
            $row[12] = $llamadas->sustentantes_afect;
            $row[13] = $llamadas->asistencia_responsable;
            $row[14] = $llamadas->estadollamada_apli;
            $row[15] = $llamadas->novedadllamada_apli;
            $row[16] = $llamadas->id_sede;           
            
            $sheet->appendRow($row);
            }                
        });       
        
    })->export('xlsx');
    
    }
    
    
    public function observacion_inicio($id, $fch, $ss)
    {
       if($fch == 0){$fecha = '';}else{ $fecha = "AND fecha_programada_inicio='$fch'"; }        
       if( $ss ==0){$sesion = '';}else{$sesion = "AND sesion='$ss'";}  
       
         $export_info_apli = \DB::select("select codigoamei, id_sede, cgi_laboratorio_id, institucion, fecha_programada_inicio, sesion, zona, provincia, distrito_id, coordinador, monitor, name_monitor,  sum(programados) as sustentantes_afect, 
estado_sesion, 
(case when observacion_sesion='' then 'SIN INICIAR SESION' 
      when observacion_sesion IS NULL then 'SIN INICIAR SESION'
      when estado_sesion='' and observacion_sesion='SIN PROBLEMA' then 'SIN INICIAR SESION'
      when estado_sesion IS NULl and observacion_sesion='SIN PROBLEMA' then 'SIN INICIAR SESION'
      when estado_sesion ='NO' and observacion_sesion='SIN PROBLEMA' then 'SIN PROBLEMA REGISTRADO'
      when estado_sesion IS NULL and observacion_sesion!='SIN PROBLEMA' then 'SIN SELECCIONAR ESTADO DE SESIÓN'
      else observacion_sesion end) as observacion_sesion, sesion_aplicador as asistencia_responsable, 
estadollamada_apli, novedadllamada_apli from umonitoreo where cgi_periodo_id = $id $fecha $sesion AND estado=1 and (estado_sesion is null or estado_sesion=''  or estado_sesion ='NO')
group by codigoamei, id_sede, cgi_laboratorio_id, institucion, fecha_programada_inicio, sesion, zona, provincia, distrito_id, coordinador, monitor, name_monitor, estado_sesion, observacion_sesion, 
sesion_aplicador, estadollamada_apli, novedadllamada_apli");    
                
    Excel::create('DACT_APLICACION_OBSERVACIONES_SESION', function($excel) use ($export_info_apli) {
        
         $excel->sheet('DD', function($sheet){
       
        // Header
        //$sheet->mergeCells('A1:B1');        
        $sheet->row(1, ['CAMPO','DESCRIPCIÓN']);
        $sheet->row(2, ['CÓDIGO AMIE','CÓDIGO PERTENECIENTE A LA INSTITUCIÓN EDUCATIVA']);
        $sheet->row(3, ['INSTITUCIÓN','NOMBRE DE LA INSTITUCIÓN EDUCATIVA']);
        $sheet->row(4, ['FECHA PROGRAMADA','FECHA PROGRAMADA DE LA EVALUACIÓN']);
        $sheet->row(5, ['SESIÓN','SESIÓN DE LA EVALUACIÓN']);
        $sheet->row(6, ['ZONA','ZONA A LA QUE PERTENECE LA INSTITUCIÓN EDUCATIVA']);
        $sheet->row(7, ['PROVINCIA','PROVINCIA A LA QUE PERTENECE LA INSTITUCIÓN']);
        $sheet->row(8, ['DISTRITO','DISTRITO AL QUE PERTENECE LA INSTITUCIÓN']);
        $sheet->row(9, ['COORDINADOR','COORDINADOR ZONAL ENCARGADO DE TENER COMUNICACIÓN CON LA INSTITUCIÓN']);
        $sheet->row(10, ['CÓDIGO MONITOR','CÓDIGO MONITOR']);
        $sheet->row(11, ['NOMBRE MONITOR','NOMBRE DEL MONITOR ENCARGADO DE TENER COMUNICACIÓN CON LA INSTITUCIÓN']);
        $sheet->row(12, ['INICIÓ SESIÓN','INICIO DE LA EVALUACIÓN EN LA SESIÓN CORRESPONDIENTE']);
        $sheet->row(13, ['OBSERVACIÓN SESIÓN','OBSERVACIÓN DE PORQUE NO SE INICIÓ LA EVALUACIÓN']);
        $sheet->row(14, ['SUSTENTANTES AFECTADOS','SUSTENTANTES AFECTADOS POR NO INICIAR SESIÓN']);
        $sheet->row(15, ['ASISTENCIA RESPONSABLE SEDE','ASISTENCIA RESPONSABLE DE SEDE']);
        $sheet->row(16, ['ESTADO DE LLAMADA','ESTADO DE LLAMADA']);
        $sheet->row(17, ['PROBLEMA DE LLAMADA','PROBLEMA DE LLAMADA']);
        $sheet->row(18, ['LABORATORIO','CÓDIGO DE LABORATORIO']);


            
        $sheet->cells('A1:B1', function($cells){
            $cells->setBackground('#0489B1');
            $cells->setFontColor('#FFFFFF');
            $cells->setAlignment('center');
            $cells->setValignment('center');
            $cells->setFontweight('bold');
        });
                 
        });
        
        $excel->sheet('Datos', function($sheet) use ($export_info_apli) {
       
          $sheet->row(1, ['CÓDIGO AMIE', 'INSTITUCIÓN', 'FECHA PROGRAMADA', 'SESIÓN', 'ZONA', 'PROVINCIA', 'DISTRITO', 'COORDINADOR', 'CÓDIGO MONITOR', 'NOMBRE MONITOR',
          'INICIÓ SESIÓN', 'OBSERVACIÓN SESIÓN', 'SUSTENTANTES AFECTADOS', 'ASISTENCIA RESPONSABLE SEDE', 'ESTADO DE LLAMADA', 'PROBLEMA DE LLAMADA', 'LABORATORIO'
            ]);  
            
        $sheet->cells('A1:Q1', function($cells){
            $cells->setBackground('#0489B1');
            $cells->setFontColor('#FFFFFF');
            $cells->setAlignment('center');
            $cells->setValignment('center');
            $cells->setFontweight('bold');
        });
        
        // Data       
        $export_info_apli;
        
        foreach ($export_info_apli as $llamadas){
        
            $row = [];
            $row[0] = $llamadas->codigoamei;
            $row[1] = $llamadas->institucion; 
            $row[2] = $llamadas->fecha_programada_inicio;
            $row[3] = $llamadas->sesion;           
            $row[4] = $llamadas->zona;
            $row[5] = $llamadas->provincia;
            $row[6] = $llamadas->distrito_id;
            $row[7] = $llamadas->coordinador;
            $row[8] = $llamadas->monitor;
            $row[9] = $llamadas->name_monitor;
            $row[10] = $llamadas->estado_sesion;
            $row[11] = $llamadas->observacion_sesion;
            $row[12] = $llamadas->sustentantes_afect;
            $row[13] = $llamadas->asistencia_responsable;
            $row[14] = $llamadas->estadollamada_apli;
            $row[15] = $llamadas->novedadllamada_apli;
            $row[16] = $llamadas->id_sede;           
            
            $sheet->appendRow($row);
            }                
        });       
        
    })->export('xlsx');
    
    }
    
    
}
