<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;

class mapaController extends Controller
{    
    public function mapa()
    {
        $periodo = \DB::select("SELECT cgi_periodo_id, name_periodo FROM umonitoreo WHERE estado=1 GROUP BY cgi_periodo_id, name_periodo ORDER BY cgi_periodo_id DESC");

        return view('mapa')->with('periodos', $periodo);
    }
    
    Public function mapa_zona($tp_reporte, $id_periodo, $fch="", $sesion="") {
        
        ///if($coord =='0'){ $coordinador = '';}else{ $coordinador = "AND coordinador='$coord'"; }        
        if($fch == 0){$fecha = '';}else{ $fecha = "AND fecha_programada_inicio='$fch'"; }        
        if( $sesion ==''){$sesion = '';}else{$sesion = "AND sesion='$sesion'";}   
        
        if($tp_reporte == "descarga"){
            
            return view('descarga.lab_zonas.index')->with('tp_reporte', $tp_reporte)->with('periodo', $id_periodo)->with('fecha', $fecha)->with('sesion', $sesion);
            
        } elseif($tp_reporte == "instalacion"){
            
            return view('instalacion.lab_zonas.index')->with('tp_reporte', $tp_reporte)->with('periodo', $id_periodo)->with('fecha', $fecha)->with('sesion', $sesion);
        } elseif($tp_reporte == "inicio_sesion"){
            
            return view('iniciosesion.lab_zonas.index')->with('tp_reporte', $tp_reporte)->with('periodo', $id_periodo)->with('fecha', $fecha)->with('sesion', $sesion);
        }
        }
        
        Public function mapa_zona_n($zona, $tp_reporte, $id_periodo, $fch="", $sesion="") {
        if($zona== 0){$z='_no_delimitada';}else{$z= $zona; };
        ///if($coord =='0'){ $coordinador = '';}else{ $coordinador = "AND coordinador='$coord'"; }        
        if($fch == 0){$fecha = '';}else{ $fecha = "AND fecha_programada_inicio='$fch'"; }        
        if( $sesion ==''){$sesion = '';}else{$sesion = "AND sesion='$sesion'";}
                
        if($tp_reporte == "descarga"){
            
            return view('descarga.lab_zonas.zona'.$z)->with('tp_reporte', $tp_reporte)->with('id_periodo', $id_periodo)->with('fecha', $fecha)->with('sesion', $sesion);
            
        } elseif($tp_reporte == "instalacion"){
            
            return view('instalacion.lab_zonas.zona'.$z)->with('tp_reporte', $tp_reporte)->with('id_periodo', $id_periodo)->with('fecha', $fecha)->with('sesion', $sesion);
        } elseif($tp_reporte == "inicio_sesion"){
            
            return view('iniciosesion.lab_zonas.zona'.$z)->with('tp_reporte', $tp_reporte)->with('id_periodo', $id_periodo)->with('fecha', $fecha)->with('sesion', $sesion);
        }
        }
    
    
}
