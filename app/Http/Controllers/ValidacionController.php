<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\SigInstiitucion;
use App\SigAplicadores;
use App\Laboratories;
use App\Provincia;
use Session;
/*@return void
   
  public function __construct()
  {
    $this->middleware('auth');
  }*/

class ValidacionController extends Controller
{
   public function index()
    {
        /*$laboratorios = \DB::select('select amie, proceso, tipo, monitor, institucion, zona, provincia, canton, parroquia, distrito, direccion, correo from laboratories where proceso = 104
         group by amie, proceso, tipo, monitor, institucion, zona, provincia, canton, parroquia, distrito, direccion, correo');*/

        $laboratorios = SigInstiitucion::where('proceso', Session::get('Periodo', 0))->where('monitor', Auth::user()->username)->where('estado', 1)->orderBy('amie', 'asc')->paginate(20);
         /*$laboratorios = SigInstiitucion::where('proceso', Auth::user()->institucion_periodo_id)->where('monitor', Auth::user()->username)->orderBy('validacionnombres', 'desc')->paginate(15);*/
         return view('listados.listado_validacion')->with("laboratorios", $laboratorios);

    }

    public function form_validacion_laboratorio($id)
    {
         $validacion = Laboratories::where('proceso', Session::get('Periodo', 0))->where('amie',$id)->where('estado','1')->paginate(50);
         $subCategoriaetiqueta=\DB::select('select * from subcategoria_etiqueta where categoria_etiqueta_id = 10');
         $periodo = Session::get('Periodo', 0);
         $validacion2 = \DB::select("select * from sig_institucionv where proceso='$periodo' and amie='$id'");  ///////modificar 02/06/2017
         $nombre="";
         $telefono="";
         $correo="";
         $llamada="";
         $id2="";
         $novedadllamada="";
         foreach ($validacion2 as $val) {

          $nombre = $val->validacionnombres;
          $telefono = $val->validaciontelefono;
           $correo = $val->correo;
           $llamada = $val->estadollamadavalidacion;
           $id2 = $val->id;
           $novedadllamada = $val->novedadllamadavalidacion;
         }
    	 return view("formularios.form_validacion_laboratorio")->with('validacion',$validacion)->with('id', $id)
       ->with('nombre', $nombre)
       ->with('telefono', $telefono)
       ->with('correo', $correo)
       ->with('llamada', $llamada)
       ->with('subCategoriaetiqueta', $subCategoriaetiqueta)
       ->with('id2', $id2)->with('novedadllamada', $novedadllamada);
    }


    public function update_labo(Request $request)
    {

       $celular1_wa=0;
       $celular2_wa=0;
       $celular1_swa=0;
       $celular2_swa=0;
        if($request->input("celular1_wa") == "on" or $request->input("celular1_wa")=="1")
        {
            $celular1_wa=1;
        }
        else
        {
            $celular1_wa=null;
        }
        /////celular2_wa
        if($request->input("celular2_wa") == "on" or $request->input("celular2_wa")=="1")
        {
            $celular2_wa=1;
        }
        else
        {
            $celular2_wa=null;
        }
        /////celular1_swa
        if($request->input("celular1_swa") == "on" or $request->input("celular1_swa")=="1")
        {
            $celular1_swa=1;
        }
        else
        {
            $celular1_swa=null;
        }
        /////celular2_swa
        if($request->input("celular2_swa") == "on" or $request->input("celular2_swa")=="1")
        {
            $celular2_swa=1;
        }
        else
        {
            $celular2_swa=null;
        }
       $data=$request->all();
    $id2=$data["id10"];
      $validaciones = SigInstiitucion::find($id2);
      if($validaciones)
        {
           /*return response()->json(['estadollamada' => $data["estadollamada"], 'novedadllamada' => $data["novedadllamada"], 'id' => $id, 'comunicado' => $data["comunicado"], 'traslado' => $data["traslado"], 'tiempotraslado' => $data["tiempotraslado"]]);*/
           if ($data["estadollamada"]=="CONTACTADO" and $data["novedadllamada"]=="")
              {
                     $validaciones->estadollamada = $data["estadollamada"];
                     $validaciones->novedadllamada = $data["novedadllamada"];
                     $validaciones->participacion = $data["participacion"];
                     //////para responsable de sede titular
                     $validaciones->numero_id_rector = $data["numero_id_rector"];
                     $validaciones->correoinstitucion = $data["emailr"];
                     $validaciones->rector = $data["nombresr"];
                     $validaciones->contactoinstitucion = $data["tele1"];
                     $validaciones->celular = $data["celular"];
                     $validaciones->telefono1 = $data["telefono1"];
                     $validaciones->telefono2 = $data["telefono2"];
                     $validaciones->correo = $data["correo"];
                     $validaciones->celular_whatsapp = $data["celularw"];
                     $validaciones->comunicacion = $data["comunicacion"];
                     $validaciones->celular1_wa = $celular1_wa;
                     $validaciones->celular2_wa = $celular2_wa;
                     $validaciones->celular1_swa = $celular1_swa;
                     $validaciones->celular2_swa = $celular2_swa;
                     $validaciones->recibiocomunicado = $data["recibiocomunicado"];
                     //$validaciones->motivo1 = $data["motivo1"];
                     $validaciones->motivo2 = $data["motivo2"];
                     
                     $validaciones->cuarto_egb = $data["cuarto_egb"]; // Mayreth                     
                     if($data["cuarto_egb_n"] != '' || $data["cuarto_egb_n"] != null){ $validaciones->cuarto_egb_n = $data["cuarto_egb_n"]; }else{ $validaciones->cuarto_egb_n = 0; }  // Mayreth
                     $validaciones->septimo_egb = $data["septimo_egb"]; // Mayreth
                     if($data["septimo_egb_n"] != '' || $data["septimo_egb_n"] != null){ $validaciones->septimo_egb_n = $data["septimo_egb_n"]; }else{ $validaciones->septimo_egb_n = 0; }  // Mayreth
                     $validaciones->decimo_egb = $data["decimo_egb"]; // Mayreth
                     $validaciones->tercero_bgu = $data["tercero_bgu"]; // Mayreth
                     
                     if($data["decimo_egb_nu"] != '' || $data["decimo_egb_nu"] != null){ $validaciones->decimo_egb_n = $data["decimo_egb_nu"]; }else{ $validaciones->decimo_egb_n = 0; }  // Mayreth
                     if($data["tercero_bgu_n"] != '' || $data["tercero_bgu_n"] != null){ $validaciones->tercero_bach_n = $data["tercero_bgu_n"]; }else{ $validaciones->tercero_bach_n = 0; }  // Mayreth
                     
                     /*$validaciones->octavo_egb_n = $data["octavo_egb_n"];
                     $validaciones->noveno_egb_n = $data["noveno_egb_n"];*/
                     
                     /*$validaciones->primero_bach_n = $data["primero_bach_n"];
                     $validaciones->segundo_bach_n = $data["segundo_bach_n"];*/
                     
                     /*$validaciones->direccion = $data["direccion"];
                     $validaciones->sostenimiento = $data["sostenimiento"];
                     $validaciones->estado_institucion = $data["estado_institucion"];*/
                     /*$validaciones->provincia = $data["provincia"];
                     $validaciones->canton = $data["canton"];
                     $validaciones->parroquia = $data["parroquia"];
                     $validaciones->amie_actual = $data["amie_actual"];
                     $validaciones->institucion_actual = $data["institucion_actual"];
                     $validaciones->egb = $data["egb"];
                     $validaciones->bachillerato = $data["bachillerato"];
                     $validaciones->egb_bachillerato = $data["egb_bachillerato"];*/
                     $validaciones->telefono_contactado = $data["telefono_contactado"];
                     $validaciones->observacion = $data["observacion"];
                     $validaciones->visita_institucion = $data["visita_institucion"];
                     $validaciones->crm_institucion = $data["crm_institucion"];

                     /*$validaciones->descarga = $data["descarga"];
                     $validaciones->instalacion = $data["instalacion"];
                     $validaciones->impresionaplicacion = $data["impresionaplicacion"];
                     $validaciones->observacion_instala = $data["observacion_instala"];*/
                     //$validaciones->registrodigital = $data["registrodigital"];
                     /*$validaciones->primero_egb = $data["primero_egb"];
                     $validaciones->primero_egb_n = $data["primero_egb_n"];
                     $validaciones->primero_egb_d = $data["primero_egb_d"];
                     $validaciones->registro_motivos = $data["registro_motivos"];

                     $validaciones->iniciall_2 = $data["iniciall_2"];
                     $validaciones->inicial_2_n = $data["inicial_2_n"];
                     $validaciones->inicial_2_d = $data["inicial_2_d"];*/
                     ////para responsable de sede suplente
                     $validaciones->correoinstitucion_suplente = $data["emailrs"];
                     $validaciones->rector_suplente = $data["nombresrs"];
                     $validaciones->contactoinstitucion_suplente = $data["tele1s"];
                     $validaciones->celular_suplente = $data["celulars"];
                     $validaciones->telefono1_suplente = $data["telefono1s"];
                     $validaciones->telefono2_suplente = $data["telefono2s"];
                     $validaciones->correo_suplente = $data["correos"];
                     $validaciones->celular_whatsapp_suplente = $data["celularws"];
                     $validaciones->comunicacion_suplente = $data["comunicacions"];
                     if($validaciones->save())
                     {
                      return response()->json(['status' => 'success', 'message' => '<b>¡Registro guardado!</b>']); 

                     }
              }

            elseif ($data["estadollamada"]=="CONTACTADO" and $data["novedadllamada"]=="VOLVER A LLAMAR")
              {
                     $validaciones->estadollamada = $data["estadollamada"];
                     $validaciones->novedadllamada = $data["novedadllamada"];
                     $validaciones->participacion = $data["participacion"];
                     
                     $validaciones->numero_id_rector = $data["numero_id_rector"];
                     $validaciones->correoinstitucion = $data["emailr"];
                     $validaciones->rector = $data["nombresr"];
                     $validaciones->contactoinstitucion = $data["tele1"];
                     $validaciones->celular = $data["celular"];
                     $validaciones->telefono1 = $data["telefono1"];
                     $validaciones->telefono2 = $data["telefono2"];
                     $validaciones->correo = $data["correo"];
                     $validaciones->celular_whatsapp = $data["celularw"];
                     $validaciones->comunicacion = $data["comunicacion"];
                     $validaciones->celular1_wa = $celular1_wa;
                     $validaciones->celular2_wa = $celular2_wa;
                     $validaciones->celular1_swa = $celular1_swa;
                     $validaciones->celular2_swa = $celular2_swa;
                     $validaciones->recibiocomunicado = $data["recibiocomunicado"];
                     //$validaciones->motivo1 = $data["motivo1"];
                     $validaciones->motivo2 = $data["motivo2"];
                     
                     $validaciones->cuarto_egb = $data["cuarto_egb"]; // Mayreth                     
                     if($data["cuarto_egb_n"] != '' || $data["cuarto_egb_n"] != null){ $validaciones->cuarto_egb_n = $data["cuarto_egb_n"]; }else{ $validaciones->cuarto_egb_n = 0; }  // Mayreth
                     $validaciones->septimo_egb = $data["septimo_egb"]; // Mayreth
                     if($data["septimo_egb_n"] != '' || $data["septimo_egb_n"] != null){ $validaciones->septimo_egb_n = $data["septimo_egb_n"]; }else{ $validaciones->septimo_egb_n = 0; }  // Mayreth                     
                     $validaciones->decimo_egb = $data["decimo_egb"]; // Mayreth
                     $validaciones->tercero_bgu = $data["tercero_bgu"]; // Mayreth
                     
                     /*$validaciones->octavo_egb_n = $data["octavo_egb_n"];
                     $validaciones->noveno_egb_n = $data["noveno_egb_n"];*/
                     if($data["decimo_egb_nu"] != '' || $data["decimo_egb_nu"] != null){ $validaciones->decimo_egb_n = $data["decimo_egb_nu"]; }else{ $validaciones->decimo_egb_n = 0; }  // Mayreth
                     /*$validaciones->primero_bach_n = $data["primero_bach_n"];
                     $validaciones->segundo_bach_n = $data["segundo_bach_n"];*/
                     if($data["tercero_bgu_n"] != '' || $data["tercero_bgu_n"] != null){ $validaciones->tercero_bach_n = $data["tercero_bgu_n"]; }else{ $validaciones->tercero_bach_n = 0; }  // Mayreth
                     /*$validaciones->direccion = $data["direccion"];
                     $validaciones->sostenimiento = $data["sostenimiento"];
                     $validaciones->estado_institucion = $data["estado_institucion"];*/
                     
                     /*$validaciones->provincia = $data["provincia"];
                     $validaciones->canton = $data["canton"];
                     $validaciones->parroquia = $data["parroquia"];
                     $validaciones->amie_actual = $data["amie_actual"];
                     $validaciones->institucion_actual = $data["institucion_actual"];*/
                     $validaciones->telefono_contactado = $data["telefono_contactado"];
                     $validaciones->observacion = $data["observacion"];
                     
                     $validaciones->visita_institucion = $data["visita_institucion"];
                     $validaciones->crm_institucion = $data["crm_institucion"];
                    /* 
                     $validaciones->egb = $data["egb"];
                     $validaciones->bachillerato = $data["bachillerato"];
                     $validaciones->egb_bachillerato = $data["egb_bachillerato"];*/
                     /*$validaciones->descarga = $data["descarga"];
                     $validaciones->instalacion = $data["instalacion"];
                     $validaciones->impresionaplicacion = $data["impresionaplicacion"];
                     $validaciones->observacion_instala = $data["observacion_instala"];*/
                    /// $validaciones->registrodigital = $data["registrodigital"];
                     /*$validaciones->primero_egb = $data["primero_egb"];
                     $validaciones->primero_egb_n = $data["primero_egb_n"];
                     $validaciones->primero_egb_d = $data["primero_egb_d"];
                     $validaciones->registro_motivos = $data["registro_motivos"];
                     $validaciones->iniciall_2 = $data["iniciall_2"];
                     $validaciones->inicial_2_n = $data["inicial_2_n"];
                     $validaciones->inicial_2_d = $data["inicial_2_d"];*/
                     ////para responsable de sede suplente
                     $validaciones->correoinstitucion_suplente = $data["emailrs"];
                     $validaciones->rector_suplente = $data["nombresrs"];
                     $validaciones->contactoinstitucion_suplente = $data["tele1s"];
                     $validaciones->celular_suplente = $data["celulars"];
                     $validaciones->telefono1_suplente = $data["telefono1s"];
                     $validaciones->telefono2_suplente = $data["telefono2s"];
                     $validaciones->correo_suplente = $data["correos"];
                     $validaciones->celular_whatsapp_suplente = $data["celularws"];
                     $validaciones->comunicacion_suplente = $data["comunicacions"];
                     if($validaciones->save())
                     {
                      return response()->json(['status' => 'success', 'message' => '<b>¡Registro guardado!</b>']); 

                     }
              }  
              elseif ($data["estadollamada"]=="NO CONTACTADO" and $data["novedadllamada"]!="") 
              {
                      $validaciones->estadollamada = $data["estadollamada"];
                     $validaciones->novedadllamada = $data["novedadllamada"];
                     $validaciones->observacion = $data["observacion"];
                     if($validaciones->save())
                     {
                      return response()->json(['status' => 'success', 'message' => '<b>¡Registro guardado!</b>']); 

                     }
              }
              else {
                 return response()->json(['status' => 'warning', 'message' => '<b>¡Selección Incorrecta!</b>']); 
              }
        }
        else
        {


        } 
    


    }



    public function editar_laboratorio(Request $request, $id)
    {


     
      $validaciones = Laboratories::find($id);
      if($validaciones)
        {
           /*return response()->json(['estadollamada' => $request->input("estadollamada"), 'novedadllamada' => $request->input("novedadllamada"), 'id' => $id, 'comunicado' => $request->input("comunicado"), 'traslado' => $request->input("traslado"), 'tiempotraslado' => $request->input("tiempotraslado")]);*/
               $validaciones->computadorc = $request->input("computadorc");
               $validaciones->impresorac = $request->input("impresorac");
               $validaciones->internetc = $request->input("internetc");
               $validaciones->sistemac = $request->input("sistemaoperativoc");
               $validaciones->observacion = $request->input("observacion");
               $validaciones->papelc = $request->input("papelc");
               $validaciones->suministroc = $request->input("suministroc");
               ////$validaciones->dictamen = $request->input("dictamen");
               //$validaciones->guiaevaluador = $request->input("guiaevaluador");
               //$validaciones->telefonoguia = $request->input("telefonoguia");
               $validaciones->visita = $request->input("visita");
               $validaciones->estadollamadaval = $request->input("estadollamadaval");
               $validaciones->novedadllamadaval = $request->input("novedadllamadaval");

               if($validaciones->save())
               {
                return response()->json(['status' => 'success', 'message' => '<b>¡Registro guardado!</b>']); 

               }
        }
    


    }


    public function agregar_laboratorio(Request $request)
  {
  

     $data=$request->all();
    $labo= new Laboratories;
    $labo_sede = $data["amie"]."_".$data["nlaboratorio"];
    ////preguntar si el labora existe para este proceso- sin lab duplicados
    $labexit = Laboratories::where('proceso', Session::get('Periodo', 0))->where('id_sede', $labo_sede);
    $labo->amie  =  $data["amie"];
    $labo->proceso  =  Session::get('Periodo', 0);
    $labo->id_sede  =  $labo_sede;
    $labo->maxasignada  =  $data["efuncionales"];
    $labo->computadorc  =  $data["efuncionales"];
    $labo->impresorac  =  $data["impresora"];
    $labo->internetc  =  $data["internet"];
    $labo->sistemac  =  $data["sistemaoperativo"];
	$labo->estado = 1;
    
    /*if ($labexit)
    {
          return view("mensajes.msj_correcto")->with("msj","lbor");
    }
    else
    {*/
    if($labo->save()){
            
            return view("mensajes.msj_correcto")->with("msj","Usuario Registrado Correctamente");   
    }
    else
    {
             
             return view("mensajes.msj_rechazado")->with("msj","hubo un error vuelva a intentarlo");  

    }
        /*}*/

  }




  public function editar_validador(Request $request)
    {

      
      $data=$request->all();
    $idUsuario=$data["id"];
    $usuario=SigInstiitucion::find($idUsuario);
        $usuario->validacionnombres  =  $data["nombre"];
    $usuario->validaciontelefono=$data["telefono"];
    $usuario->validacioncorreos=$data["correo"];
    $usuario->estadollamadavalidacion=$data["estadollamadavalidacion"];
    $usuario->novedadllamadavalidacion=$data["novedadllamadavalidacion"];
    
    $resul= $usuario->save();

    if($resul){
            
            return view("mensajes.msj_correcto")->with("msj","Datos actualizados Correctamente");   
    }
    else
    {
             
            return view("mensajes.msj_rechazado")->with("msj","hubo un error vuelva a intentarlo");  

    }
     



    }

   public function form_institucion($id)
   {

    $institucion = SigInstiitucion::find($id);
	$umonitoreo = \DB::table('umonitoreo')->select('id_sede','apli_nombre1','apli_cedula','apli_email','apli_email2','apli_telef1','apli_telef2','apli_telef3')->where('codigoamei', $institucion->amie)->where('cgi_periodo_id', Session::get('Periodo', 0))->where('estado','1')->groupBy('id_sede','apli_nombre1','apli_cedula','apli_email','apli_email2','apli_telef1','apli_telef2','apli_telef3')->get();
    /*$institucion = \DB::select("select * from sig_institucionv where proceso=104 and amie='08H00145'");*/
    $subCategoriaetiqueta=\DB::select('select * from subcategoria_etiqueta where categoria_etiqueta_id = 13 order by 1');
    $subCategoriaetiquetaInstitucion=\DB::select('select * from subcategoria_etiqueta where categoria_etiqueta_id = 15 order by 1');
    $subCategoriaetiquetainstlacion=\DB::select('select * from subcategoria_etiqueta where categoria_etiqueta_id = 6 order by 1');
    $sostenimiento=\DB::select('select * from sostenimientos');
    $provincia=\DB::select('select * from provincias order by 1');
    $canton=\DB::select('select * from cantones order by 2');
    $parroquia=\DB::select('select * from parroquias order by 2');
        return view("formularios.form_institucion")->with("institucion", $institucion)->with("id", $id)->with("subCategoriaetiqueta", $subCategoriaetiqueta)->with("subCategoriaetiquetainstlacion", $subCategoriaetiquetainstlacion)
        ->with("sostenimiento", $sostenimiento)
        ->with("subCategoriaetiquetaInstitucion", $subCategoriaetiquetaInstitucion)
        ->with("provincia", $provincia)
        ->with("canton", $canton)
        ->with("parroquia", $parroquia)
		->with("umonitoreo", $umonitoreo);

   }

   public function form_validacion_aplicadores($id)
    {
        ///SigAplicadores ///Provincia
        $listadoAplicadores=SigAplicadores::where('cgi_periodo_id',Session::get('Periodo', 0))->where('amie', $id)->orderBy('id_sede', 'asc')->paginate(30);
        /////$listadoAplicadores=SigAplicadores::where('cgi_periodo_id',104)->where('amie', $id)->orderBy('id_sede', 'asc')->paginate(30);
        $subCategoriaetiqueta=\DB::select('select * from subcategoria_etiqueta where categoria_etiqueta_id = 11');
        $provincia = Provincia::all();
        return view("formularios.form_validacion_aplicadores")->with("listadoAplicadores",$listadoAplicadores)->with('provincia',$provincia)->with("subCategoriaetiqueta",$subCategoriaetiqueta); 

    }

    public function buscar_validacion($dato="")
    {

        $laboratorios= SigInstiitucion::Busqueda($dato)->paginate(10);
        ///$paises=Pais::all();
        ///$paissel=$paises->find($fecha);
        return view('listados.listado_validacion')
        ->with("laboratorios", $laboratorios );      
    } 
}
