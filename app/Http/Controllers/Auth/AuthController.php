<?php

namespace App\Http\Controllers\Auth;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\Monitoreo;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Session;
use App\SigCodigoVerificacion;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use ThrottlesLogins;


    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
        $this->middleware('guest', ['except' => 'getLogout']);
      
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
   


//login

       protected function getLogin()
    {
        //$skills = Skill::where('active', true)->orderBy('name', 'asc')->lists('name', 'id');
        //$periodo = CgiPeriodo::where('estado', 'A')->whereIn('id', [122])->orderBy('id', 'asc');
        $periodo = \DB::table('umonitoreo')
                ->select('name_periodo', 'cgi_periodo_id')
                ->where('estado', 1)
                ->groupBy('name_periodo', 'cgi_periodo_id')
                ->orderBy('cgi_periodo_id', 'desc')
                ->get();

        ///$periodo = SigCodigoVerificacion::all();
        
        return view("login")->with("periodo",$periodo);
    }


       

        public function postLogin(Request $request)
   {
    $this->validate($request, [
        'email' => 'required',
        'password' => 'required',
        'periodo' => 'required',
    ]);



    $credentials = $request->only('email', 'password');

    if ($this->auth->attempt($credentials, $request->has('remember')))
    {
        Session::put('Periodo', $request->input('periodo'));
        ///Session::put('Periodo', Auth::user()->institucion_periodo_id);
        
        $usuarioactual=\Auth::user();
        return view('home')->with("usuario",  $usuarioactual);
    }

    return "credenciales incorrectas";

    }


//login

 //registro   


        protected function getRegister()
    {
        return view("registro");
    }


        

        protected function postRegister(Request $request)

   {
    $this->validate($request, [
        'usernam' => 'required',
        'email' => 'required',
        'password' => 'required',
    ]);


    $data = $request;


    $user=new User;
    $user->username=$data['name'];
    $user->email=$data['email'];
    $user->password=bcrypt($data['password']);
    $user->file='DACT_SIGET';

    if($user->save()){

         return "se ha registrado correctamente el usuario";
               
    }
   

   

}

//registro

protected function getLogout()
    {
        $this->auth->logout();
        
        Session::flush();
         Session::forget('Periodo');

        return redirect('login');
    }






}
