<?php

namespace App\Http\Controllers;

use App\Monitoreo;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\ListaSustentantes;
use App\ListaSustentantes_off;
use App\TableroMonitoreo;
use App\CgiDetalleSustentante;
use App\SigAplicadores;
use App\SigInstiitucion;
use App\Provincia;
use App\DataTables\MonitoreoDataTable;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Session;

class MonitoreoController extends AppIntegrationController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $usuario_actual = \Auth::user();
        if ($usuario_actual->user_group_id == 1) {
            $monitoreo = Monitoreo::where('cgi_periodo_id', Session::get('Periodo', 0))->whereNull('estado_sesion2')->where('estado', 1)->orderBy('id_sede', 'asc')->orderBy('fecha_programada_inicio', 'asc')->paginate(10);
            $subCategoriaetiqueta = \DB::select('select * from subcategoria_etiqueta where categoria_etiqueta_id = 7 and estado=TRUE order by 3');
            $periodo = Session::get('Periodo', 0);
            $fechaProramadaInicio = \DB::select("select distinct(sesion) as sesion from umonitoreo where estado=1 and cgi_periodo_id = '$periodo' and estado_sesion2 is null order by 1");
            $fecha = 0;
            $avanceasistencia = \DB::table('umonitoreo')->select(\DB::raw('SUM(programados) as programadosa'), \DB::raw('SUM(asistencia) as asistencia'), \DB::raw('round(sum(asistencia)/sum(programados)*100) as porcentajeasistencia'), \DB::raw('round(sum(cargas)/sum(programados)*100) as porcentajecargas'), \DB::raw('round(sum(cargas)) as cargas'))->where('cgi_periodo_id', Session::get('Periodo', 0))->whereNull('estado_sesion2')->where('estado', 1)->first();
            $name_periodo = \DB::table('umonitoreo')->where('estado', 1)->where('cgi_periodo_id', Session::get('Periodo', 0))->whereNull('estado_sesion2')->first();
            $avanceiniciar = \DB::select("select (case when estado_sesion ='' then 'NO'
             when estado_sesion = 'NO' then estado_sesion
             when estado_sesion is null then 'NO' end) as sesion, count(*) as cantidad from umonitoreo where estado=1 and cgi_periodo_id = '$periodo'  and estado_sesion2 is null and
             (case when estado_sesion ='' then 'NO'
             when estado_sesion = 'NO' then estado_sesion
             when estado_sesion is null then 'NO' end)='NO'
             group by
             (case when estado_sesion ='' then 'NO'
             when estado_sesion = 'NO' then estado_sesion
             when estado_sesion is null then 'NO' end)");
            $avancesesion = \DB::select("select 
a.cantidad, b.sesion, round((a.cantidad::numeric / b.sesion::numeric)*100, 2) as porcentaje from
( 
SELECT (case when estado_sesion ='SI' then estado_sesion end) as sesion, COUNT(*) as cantidad
FROM umonitoreo WHERE estado=1 and cgi_periodo_id = '$periodo' and estado_sesion2 is null and (case when estado_sesion ='SI' then estado_sesion end) = 'SI'   
AND estado = 1 
GROUP BY (case when estado_sesion ='SI' then estado_sesion end)) as a,
(select count(*) as sesion from umonitoreo where estado=1 and cgi_periodo_id = '$periodo' and estado_sesion2 is null ) as b");
            
            return view('listados.listado_monitoreo')->with("monitoreo", $monitoreo)->with("subCategoriaetiqueta", $subCategoriaetiqueta)->with("fechaProramadaInicio", $fechaProramadaInicio)->with("fecha", $fecha)->with("avanceasistencia", $avanceasistencia)->with("avancesesion", $avancesesion)->with("avanceiniciar", $avanceiniciar)->with("name_periodo", $name_periodo);
        } else {
            $monitoreo = Monitoreo::where('cgi_periodo_id', Session::get('Periodo', 0))->whereNull('estado_sesion2')->where('estado', 1)->where('monitor', Auth::user()->username)->orderBy('sesion', 'asc')->orderBy('id_sede', 'asc')->paginate(10);
            $subCategoriaetiqueta = \DB::select('select * from subcategoria_etiqueta where categoria_etiqueta_id = 7 and estado = true order by 1');
            $periodo = Session::get('Periodo', 0);
            $monitor = Auth::user()->username;
            $fechaProramadaInicio = \DB::select("select distinct(sesion) as sesion from umonitoreo where estado=1 and cgi_periodo_id = '$periodo' and estado_sesion2 is null and monitor='$monitor' order by 1");
            $fecha = 0;
            $avanceasistencia = \DB::table('umonitoreo')->select(\DB::raw('SUM(programados) as programadosa'), \DB::raw('SUM(asistencia) as asistencia'), \DB::raw('round(sum(asistencia)/sum(programados)*100) as porcentajeasistencia'), \DB::raw('round(sum(cargas)/sum(programados)*100) as porcentajecargas'), \DB::raw('round(sum(cargas)) as cargas'))->where('cgi_periodo_id', Session::get('Periodo', 0))->whereNull('estado_sesion2')->where('monitor', Auth::user()->username)->where('estado', 1)->first();
            $name_periodo = \DB::table('umonitoreo')->where('estado', 1)->where('cgi_periodo_id', Session::get('Periodo', 0))->whereNull('estado_sesion2')->first();
            $avanceiniciar = \DB::select("select (case when estado_sesion ='' then 'NO'
             when estado_sesion = 'NO' then estado_sesion
             when estado_sesion is null then 'NO' end) as sesion, count(*) as cantidad from umonitoreo where estado=1 and cgi_periodo_id = '$periodo' and estado_sesion2 is null and
             (case when estado_sesion ='' then 'NO'
             when estado_sesion = 'NO' then estado_sesion
             when estado_sesion is null then 'NO' end)='NO' and monitor='$monitor'
             group by
             (case when estado_sesion ='' then 'NO'
             when estado_sesion = 'NO' then estado_sesion
             when estado_sesion is null then 'NO' end)");
            $avancesesion = \DB::select("select 
a.cantidad, b.sesion, round((a.cantidad::numeric / b.sesion::numeric)*100, 2) as porcentaje from
( 
SELECT (case when estado_sesion ='SI' then estado_sesion end) as sesion, COUNT(*) as cantidad
FROM umonitoreo WHERE estado=1 and cgi_periodo_id = '$periodo' and estado_sesion2 is null and monitor='$monitor' and (case when estado_sesion ='SI' then estado_sesion end) = 'SI'   
AND estado = 1 
GROUP BY (case when estado_sesion ='SI' then estado_sesion end)) as a,
(select count(*) as sesion from umonitoreo where estado=1 and cgi_periodo_id = '$periodo' and estado_sesion2 is null and monitor='$monitor' ) as b");

            return view('listados.listado_monitoreo')->with("monitoreo", $monitoreo)->with("subCategoriaetiqueta", $subCategoriaetiqueta)->with("fechaProramadaInicio", $fechaProramadaInicio)->with("fecha", $fecha)->with("avanceasistencia", $avanceasistencia)->with("avancesesion", $avancesesion)->with("avanceiniciar", $avanceiniciar)->with("name_periodo", $name_periodo);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function buscar_monitoreo($fecha, $dato = "")
    {
        if ($fecha == 0) {
            $fech = '';
        } else {
            $fech = "AND sesion='$fecha'";
        }
        $usuario_actual = \Auth::user();
        if ($usuario_actual->user_group_id == 1) {
            $avanceasistencia = \DB::table('umonitoreo')->select(\DB::raw('SUM(programados) as programadosa'), \DB::raw('SUM(asistencia) as asistencia'), \DB::raw('round(sum(asistencia)/sum(programados)*100) as porcentajeasistencia'), \DB::raw('round(sum(cargas)/sum(programados)*100) as porcentajecargas'), \DB::raw('round(sum(cargas)) as cargas'))->where('cgi_periodo_id', Session::get('Periodo', 0))->whereNull('estado_sesion2')->where('estado', 1)->first();
            $name_periodo = \DB::table('umonitoreo')->where('estado', 1)->where('cgi_periodo_id', Session::get('Periodo', 0))->whereNull('estado_sesion2')->first();
            $monitoreo = Monitoreo::Busqueda($fecha, $dato)->whereNull('estado_sesion2')->orderBy('codigoamei', 'asc')->orderBy('fecha_programada_inicio', 'asc')->paginate(10);
            $subCategoriaetiqueta = \DB::select('select * from subcategoria_etiqueta where categoria_etiqueta_id = 7 and estado = true order by 1');
            $periodo = Session::get('Periodo', 0);
            $monitor = Auth::user()->username;
            $fechaProramadaInicio = \DB::select("select distinct(sesion) as sesion from umonitoreo where estado=1 and cgi_periodo_id = '$periodo' and estado_sesion2 is null order by 1");
            ///$paises=Pais::all();
            ///$paissel=$paises->find($fecha);
            $avanceiniciar = \DB::select("select (case when estado_sesion ='' then 'NO'
             when estado_sesion = 'NO' then estado_sesion
             when estado_sesion is null then 'NO' end) as sesion, count(*) as cantidad from umonitoreo where estado=1 and cgi_periodo_id = '$periodo' and estado_sesion2 is null and
             (case when estado_sesion ='' then 'NO'
             when estado_sesion = 'NO' then estado_sesion
             when estado_sesion is null then 'NO' end)='NO' $fech
             group by
             (case when estado_sesion ='' then 'NO'
             when estado_sesion = 'NO' then estado_sesion
             when estado_sesion is null then 'NO' end)");
            $avancesesion = \DB::select("select 
a.cantidad, b.sesion, round((a.cantidad::numeric / b.sesion::numeric)*100, 2) as porcentaje from
( 
SELECT (case when estado_sesion ='SI' then estado_sesion end) as sesion, COUNT(*) as cantidad
FROM umonitoreo WHERE estado=1 and cgi_periodo_id = '$periodo' and estado_sesion2 is null $fech and (case when estado_sesion ='SI' then estado_sesion end) = 'SI'   
AND estado = 1 
GROUP BY (case when estado_sesion ='SI' then estado_sesion end)) as a,
(select count(*) as sesion from umonitoreo where estado=1 $fech and cgi_periodo_id = '$periodo'  and estado_sesion2 is null) as b");
            return view('listados.listado_monitoreo')
                ->with("subCategoriaetiqueta", $subCategoriaetiqueta)
                ->with("fechaProramadaInicio", $fechaProramadaInicio)
                ->with("monitoreo", $monitoreo)
                ->with("fecha", $fecha)
                ->with("avanceasistencia", $avanceasistencia)
                ->with("avancesesion", $avancesesion)
                ->with("avanceiniciar", $avanceiniciar)
                ->with("name_periodo", $name_periodo);
        } else {
            $avanceasistencia = \DB::table('umonitoreo')->select(\DB::raw('SUM(programados) as programadosa'), \DB::raw('SUM(asistencia) as asistencia'), \DB::raw('round(sum(asistencia)/sum(programados)*100) as porcentajeasistencia'), \DB::raw('round(sum(cargas)/sum(programados)*100) as porcentajecargas'), \DB::raw('round(sum(cargas)) as cargas'))->where('cgi_periodo_id', Session::get('Periodo', 0))->whereNull('estado_sesion2')->where('monitor', Auth::user()->username)->where('estado', 1)->first();
            $name_periodo = \DB::table('umonitoreo')->where('estado', 1)->where('cgi_periodo_id', Session::get('Periodo', 0))->whereNull('estado_sesion2')->first();
            $monitoreo = Monitoreo::Busqueda($fecha, $dato)->whereNull('estado_sesion2')->where('monitor', Auth::user()->username)->orderBy('codigoamei', 'asc')->orderBy('fecha_programada_inicio', 'asc')->paginate(10);
            $subCategoriaetiqueta = \DB::select('select * from subcategoria_etiqueta where categoria_etiqueta_id = 7 and estado = true order by 1');
            $periodo = Session::get('Periodo', 0);
            $monitor = Auth::user()->username;
            $fechaProramadaInicio = \DB::select("select distinct(sesion) as sesion from umonitoreo where estado=1 and cgi_periodo_id = '$periodo' and estado_sesion2 is null and monitor='$monitor' order by 1");
            ///$paises=Pais::all();
            ///$paissel=$paises->find($fecha);
            $avanceiniciar = \DB::select("select (case when estado_sesion ='' then 'NO'
             when estado_sesion = 'NO' then estado_sesion
             when estado_sesion is null then 'NO' end) as sesion, count(*) as cantidad from umonitoreo where estado=1 and cgi_periodo_id = '$periodo' and estado_sesion2 is null and
             (case when estado_sesion ='' then 'NO'
             when estado_sesion = 'NO' then estado_sesion
             when estado_sesion is null then 'NO' end)='NO' and monitor='$monitor' $fech
             group by
             (case when estado_sesion ='' then 'NO'
             when estado_sesion = 'NO' then estado_sesion
             when estado_sesion is null then 'NO' end)");
            $avancesesion = \DB::select("select 
a.cantidad, b.sesion, round((a.cantidad::numeric / b.sesion::numeric)*100, 2) as porcentaje from
( 
SELECT (case when estado_sesion ='SI' then estado_sesion end) as sesion, COUNT(*) as cantidad
FROM umonitoreo WHERE estado=1 and cgi_periodo_id = '$periodo' and estado_sesion2 is null $fech and monitor='$monitor' and (case when estado_sesion ='SI' then estado_sesion end) = 'SI'   
AND estado = 1 
GROUP BY (case when estado_sesion ='SI' then estado_sesion end)) as a,
(select count(*) as sesion from umonitoreo where estado=1 $fech and cgi_periodo_id = '$periodo' and monitor='$monitor' and estado_sesion2 is null ) as b");
            return view('listados.listado_monitoreo')
                ->with("subCategoriaetiqueta", $subCategoriaetiqueta)
                ->with("fechaProramadaInicio", $fechaProramadaInicio)
                ->with("monitoreo", $monitoreo)
                ->with("fecha", $fecha)
                ->with("avanceasistencia", $avanceasistencia)
                ->with("avancesesion", $avancesesion)
                ->with("avanceiniciar", $avanceiniciar)
                ->with("name_periodo", $name_periodo);
        }

    }


    public function buscar_sustentantes($id_lab, $dato_sustentantes = "")
    {
        /////1 on y 2 off
        $variableaplicacion = 1;
        if ($variableaplicacion == 2) {
            $listadoSustentantes = ListaSustentantes_off::Busqueda($id_lab, $dato_sustentantes)->orderBy('nombres', 'asc')->paginate(5000);
            $subCategoriaetiqueta = \DB::select('select * from subcategoria_etiqueta where categoria_etiqueta_id = 8');
            return view('formularios.form_monitoreo_sustentantes_off')
                ->with("listadoSustentantes", $listadoSustentantes)->with("subCategoriaetiqueta", $subCategoriaetiqueta);
        } else if ($variableaplicacion == 1) {

            $listadoSustentantes = ListaSustentantes::Busqueda($id_lab, $dato_sustentantes)->orderBy('apellidos', 'asc')->paginate(5000);
            $subCategoriaetiqueta = \DB::select('select * from subcategoria_etiqueta where categoria_etiqueta_id in (1,3,17,18,19,20)');
            return view('formularios.form_monitoreo_sustentantes')
                ->with("listadoSustentantes", $listadoSustentantes)->with("subCategoriaetiqueta", $subCategoriaetiqueta);
        }


    }


    public function create()
    {
        //
    }

    public function form_monitoreo_sustentantes($id, $amie, $fecha, $sesion, $id_usuario)
    {
        //////1 on y 2 off
        $variableaplicacion = 1;
        if ($variableaplicacion == 2) {

            $listadoSustentantes = ListaSustentantes_off::where('fecha_evaluacion', $fecha)
                ->where('institucion_id', $amie)
                ->where('sesion_evaluacion', $sesion)
                ->orderBy('nombres', 'asc')->paginate(5000);
            // dd($listadoSustentantes);
            $subCategoriaetiqueta = \DB::select('select * from subcategoria_etiqueta where categoria_etiqueta_id = 8 AND estado=TRUE');
            return view("formularios.form_monitoreo_sustentantes_off")->with("listadoSustentantes", $listadoSustentantes)
                ->with("subCategoriaetiqueta", $subCategoriaetiqueta)
                ->with("amie", $amie)
                ->with("fecha", $fecha)
                ->with("sesion", $sesion)
                ->with("id_usuario", $id_usuario);


        } else if ($variableaplicacion == 1) {
            $listadoSustentantes = ListaSustentantes::where('cgi_laboratorio_id', $id)->orderBy('apellidos', 'asc')->paginate(5000);

            $subCategoriaetiqueta = \DB::select('select * from subcategoria_etiqueta where categoria_etiqueta_id in (1,3,17,18,19,20)');
            return view("formularios.form_monitoreo_sustentantes")->with("listadoSustentantes", $listadoSustentantes)
                ->with("subCategoriaetiqueta", $subCategoriaetiqueta);
        }

    }

    public function editar_aplicadores(Request $request, $id, $id_sede)
    {


        $validaciones = SigAplicadores::find($id);
        if ($validaciones) {
            if ($request->input("estadollamada") == "CONTACTADO" and $request->input("novedadllamada") == "") {
                $validaciones->estadollamada_apli = $request->input("estadollamada");
                $validaciones->novedadllamada_apli = $request->input("novedadllamada");
                //$validaciones->identificacion = $request->input("identificacion");
                $validaciones->nombres = $request->input("nombres");
                $validaciones->telefono = $request->input("telefono");
                $validaciones->celular = $request->input("celular");
                $validaciones->correoinstitucional = $request->input("correoinstitucional");
                $validaciones->asistencia_apli = $request->input("asistencia");
                $validaciones->observaciones = $request->input("id_subcategoria_etiqueta");
                $validaciones->correopersonal = $request->input("correopersonal");
                $validaciones->claveyusuario = $request->input("claveyusuario");

                if ($validaciones->save()) {
                    return response()->json(['status' => 'success', 'message' => '<b>¡Registro guardado!</b>']);

                }
            } elseif ($request->input("estadollamada") == "CONTACTADO" and $request->input("novedadllamada") == "VOLVER A LLAMAR") {
                $validaciones->estadollamada_apli = $request->input("estadollamada");
                $validaciones->novedadllamada_apli = $request->input("novedadllamada");
                //$validaciones->identificacion = $request->input("identificacion");
                $validaciones->nombres = $request->input("nombres");
                $validaciones->telefono = $request->input("telefono");
                $validaciones->celular = $request->input("celular");
                $validaciones->correoinstitucional = $request->input("correoinstitucional");
                $validaciones->asistencia_apli = $request->input("asistencia");
                $validaciones->observaciones = $request->input("id_subcategoria_etiqueta");
                $validaciones->correopersonal = $request->input("correopersonal");
                $validaciones->claveyusuario = $request->input("claveyusuario");
                if ($validaciones->save()) {
                    return response()->json(['status' => 'success', 'message' => '<b>¡Registro guardado!</b>']);

                }
            } elseif ($request->input("estadollamada") == "NO CONTACTADO" and $request->input("novedadllamada") != "") {
                $validaciones->estadollamada_apli = $request->input("estadollamada");
                $validaciones->novedadllamada_apli = $request->input("novedadllamada");
                if ($validaciones->save()) {
                    return response()->json(['status' => 'success', 'message' => '<b>¡Registro guardado!</b>']);

                }
            } else {
                return response()->json(['status' => 'warning', 'message' => '<b>¡Selección Incorrecta!</b>']);
            }
        } else {


        }


    }

    public function form_monitoreo_aplicadores($id)
    {
        ///SigAplicadores ///Provincia
        $listadoAplicadores = SigAplicadores::where('cgi_periodo_id', Auth::user()->institucion_periodo_id)->where('amie', $id)->orderBy('jornada', 'asc')->paginate(50);
        $subCategoriaetiqueta = \DB::select('select * from subcategoria_etiqueta where categoria_etiqueta_id = 11');
        $provincia = Provincia::all();
        return view("formularios.form_monitoreo_aplicadores")->with("listadoAplicadores", $listadoAplicadores)->with('provincia', $provincia)->with("subCategoriaetiqueta", $subCategoriaetiqueta);

    }

    public function update_sustentante(Request $request, $id)
    {
        //// 1 on y 2 off
        $variableaplicacion = 1;
        if ($variableaplicacion == 1) {
            $asistencia = 0;
            $reprogramacion = 0;
            $subcategoria = null;
            ////asistencia
            if ($request->input("asistencia") == "on" or $request->input("asistencia") == "1") {
                $asistencia = 1;
            } else {
                $asistencia = null;
            }
            ////reprogramacion
            if ($request->input("reprogramacion") == "on" or $request->input("reprogramacion") == "1") {
                $reprogramacion = 1;
            } else {
                $reprogramacion = null;
            }

            if ($request->input("id_subcategoria_etiqueta") == "") {
                $subcategoria = null;
            } else {
                $subcategoria = $request->input("id_subcategoria_etiqueta");
            }

            $variable = (int)$id;

            /*if($reprogramacion != null and $subcategoria == null)
             {
                 return response()->json(['status' => 'warning', 'message' => '<b>¡Reprogramación se requiere Novedad!</b>']);
             }*/
            $sustentantes = CgiDetalleSustentante::find($id);
            if ($sustentantes) {
                $sustentantes->asistencia = $asistencia;
                $sustentantes->reprogramacion = $reprogramacion;
                $sustentantes->monitor = $subcategoria;
                if ($sustentantes->save()) {
                    return response()->json(['status' => 'success', 'message' => '<b>¡Registro guardado!</b>']);
                }
            }


            return response()->json(['categoria' => $subcategoria, 'asistencia' => $asistencia, 'id' => $variable, 'reprogramacion' => $reprogramacion]);
        } else if ($variableaplicacion == 2) {

            $asistencia = 0;
            $asistencia2 = 0;
            $subcategoria = null;
            $reprogramacion = 0;
            $reprogramacion2 = 0;
            ////asistencia
            if ($request->input("asistencia") == "on" or $request->input("asistencia") == "1") {
                $asistencia = 1;
            } else if ($request->input("asistencia") == "2") {
                $asistencia = 2;
            } else if ($request->input("asistencia") == "0") {
                $asistencia = 0;
            } else {
                $asistencia = null;
            }
            ////asistencia 2
            if ($request->input("asistencia2") == "on" or $request->input("asistencia2") == "1") {
                $asistencia2 = 1;
            } else {
                $asistencia2 = null;
            }
            ////reprogramacion
            if ($request->input("reprogramacion") == "on" or $request->input("reprogramacion") == "1") {
                $reprogramacion = 1;
            } else {
                $reprogramacion = null;
            }
            ////reprogramacion2
            if ($request->input("reprogramacion2") == "on" or $request->input("reprogramacion2") == "1") {
                $reprogramacion2 = 1;
            } else {
                $reprogramacion2 = null;
            }

            if ($request->input("id_subcategoria_etiqueta") == "") {
                $subcategoria = null;
            } else {
                $subcategoria = $request->input("id_subcategoria_etiqueta");
            }

            $variable = (int)$id;

            /*if($reprogramacion != null and $subcategoria == null)
             {
                 return response()->json(['status' => 'warning', 'message' => '<b>¡Reprogramación se requiere Novedad!</b>']);
             }*/
            $sustentantes = ListaSustentantes_off::find($id);
            if ($sustentantes) {
                if ($asistencia === 1 and $subcategoria === null) {

                    $sustentantes->asistencia = $asistencia;
                    $sustentantes->asistencia2 = $asistencia2;
                    $sustentantes->reprogramacion = $reprogramacion;
                    $sustentantes->reprogramacion2 = $reprogramacion2;
                    $sustentantes->sustentante_observacion = $subcategoria;
                    $sustentantes->cargo = $request->input("cargo");
                    if ($sustentantes->save()) {
                        return response()->json(['status' => 'success', 'message' => '<b>¡Registro guardado!</b>']);
                    } else {
                        return response()->json(['status' => 'warning', 'message' => '<b>¡Ocurrió un error!</b>']);
                    }
                } else if ($asistencia === 0 and $subcategoria === null) {

                    return response()->json(['status' => 'warning', 'message' => '<b>¡La observación es requerida!</b>']);

                } else if ($asistencia === 0 and $subcategoria !== null) {
                    $sustentantes->asistencia = $asistencia;
                    $sustentantes->asistencia2 = $asistencia2;
                    $sustentantes->reprogramacion = $reprogramacion;
                    $sustentantes->reprogramacion2 = $reprogramacion2;
                    $sustentantes->sustentante_observacion = $subcategoria;
                    $sustentantes->cargo = $request->input("cargo");
                    if ($sustentantes->save()) {
                        return response()->json(['status' => 'success', 'message' => '<b>¡Registro guardado!</b>']);
                    } else {
                        return response()->json(['status' => 'warning', 'message' => '<b>¡Ocurrió un error!</b>']);
                    }
                } elseif ($asistencia === null and $subcategoria === null) {
                    $sustentantes->asistencia = $asistencia;
                    $sustentantes->asistencia2 = $asistencia2;
                    $sustentantes->reprogramacion = $reprogramacion;
                    $sustentantes->reprogramacion2 = $reprogramacion2;
                    $sustentantes->sustentante_observacion = $subcategoria;
                    $sustentantes->cargo = $request->input("cargo");
                    if ($sustentantes->save()) {
                        return response()->json(['status' => 'success', 'message' => '<b>¡Registro guardado!</b>']);
                    } else {
                        return response()->json(['status' => 'warning', 'message' => '<b>¡Ocurrió un error!</b>']);
                    }
                } else {
                    return response()->json(['status' => 'warning', 'message' => '<b>¡Debe corregir la observación!</b>']);

                }
            }


            return response()->json(['categoria' => $subcategoria, 'asistencia' => $asistencia, 'id' => $variable, 'asistencia2' => $asistencia2]);

        }
    }

    public function form_monitoreo_corresponsable($id)
    {
        $aplicador = TableroMonitoreo::find($id);
        return view("formularios.form_monitoreo_corresponsable")->with("aplicador", $aplicador)->with("id", $id);
    }

    public function editar_aplicador(Request $request)
    {

        $data = $request->all();
        $idUsuario = $data["id2"];
        $usuario = TableroMonitoreo::find($idUsuario);
        /* if($usuario)
         {*/
        $usuario->apli_nombre1 = $data["nombres1"];
        $usuario->apli_nombre2 = $data["nombres2"];
        $usuario->apli_apellido1 = $data["apellido1"];
        $usuario->apli_apellido2 = $data["apellido2"];
        $usuario->apli_telef1 = $data["celular1"];
        $usuario->apli_telef2 = $data["celular2"];
        $usuario->apli_email = $data["email"];
        $usuario->apli_motivo = $data["mosistencia"];
        $usuario->apli_recepcion = $data["repcionuc"];
        $usuario->sesion_aplicador = $data["sesion_aplicador"];

        $resul = $usuario->save();

        if ($resul) {
            return response()->json(['status' => 'success', 'message' => '<b>¡Registro guardado!</b>']);
            //return view("mensajes.msj_correcto")->with("msj","Datos actualizados Correctamente");
        } else {
            return response()->json(['status' => 'warning', 'message' => '<b>¡Error!</b>']);
            // return view("mensajes.msj_rechazado")->with("msj","hubo un error vuelva a intentarlo");

        }

        /* }*/
    }

    public function editar_descarga(Request $request)
    {


        $data = $request->all();
        $idUsuario = $data["id3"];
        $usuario = TableroMonitoreo::find($idUsuario);
        /* if($usuario)
         {*/
        $usuario->recepcion_aplicativo = $data["raplicativo"];
        $usuario->descarga_aplicativo = $data["rdaplicativo"];
        $usuario->observacion_descarga = $data["ndaplicativo"];
        $usuario->hash_compu = $data["codigopc"];
        $usuario->hash_compu_ok = $data["hash_compu_ok"];
        $usuario->hash_movil = $data["codigomovil"];
        $usuario->hash_movil_ok = $data["hash_movil_ok"];

        $resul = $usuario->save();

        if ($resul) {
            
            $client = new \GuzzleHttp\Client([
                'headers' => ['Content-Type' => 'application/json']
            ]);
            $url = 'http://192.168.110.29:8085/innevalWebApp/api/form/respuesta';
            $user = getenv('APP_SERVICE_USER');
            $pwd = getenv('APP_SERVICE_PWD');
            $data = '';
            
            $data = array('idSesion' => 6343,
                          'identificadorPregunta' =>'FORM_PREVIO_COMUNICADO',
                          'respuesta' => $resul->recepcion_aplicativo);
            
            $json_str = json_encode($data);
            $response = $client->post($url, [
                'auth' => [
                    $user,
                    $pwd
                ],
                //body instead of json
                'body' => $json_str,
            ]);
            $respuesta = json_decode($response->getBody()->getContents());

            return $respuesta;
            
            //return response()->json(['status' => 'success', 'message' => '<b>¡Registro guardado!</b>']);
            /// return view("mensajes.msj_correcto")->with("msj","Datos actualizados Correctamente");
        } else {
            return response()->json(['status' => 'warning', 'message' => '<b>hubo un error vuelva a intentarlo!</b>']);
            //return view("mensajes.msj_rechazado")->with("msj","hubo un error vuelva a intentarlo");

        }

        /* }*/

    }

    public function editar_listados(Request $request)
    {


        $data = $request->all();
        $idUsuario = $data["id5"];
        $usuario = TableroMonitoreo::find($idUsuario);
        /* if($usuario)
         {*/
        $usuario->listado_asistencia = $data["listado_asistencia"];
        $usuario->listado_validacion = $data["listado_validacion"];
        $usuario->observacion_listado_asistencia = $data["observacion_listado_asistencia"];

        $resul = $usuario->save();

        if ($resul) {
            return response()->json(['status' => 'success', 'message' => '<b>¡Registro guardado!</b>']);
            /// return view("mensajes.msj_correcto")->with("msj","Datos actualizados Correctamente");
        } else {
            return response()->json(['status' => 'warning', 'message' => '<b>hubo un error vuelva a intentarlo!</b>']);
            //return view("mensajes.msj_rechazado")->with("msj","hubo un error vuelva a intentarlo");

        }

        /* }*/

    }

    public function editar_instalacion(Request $request)
    {


        $data = $request->all();
        $idUsuario = $data["id4"];
        $usuario = TableroMonitoreo::find($idUsuario);
        /* if($usuario)
         {*/
        $usuario->instalo_aplicativo = $data["iaplicativo"];
        $usuario->comp_inst = $data["einstalados"];
        $usuario->observacion_instala = $data["ninstlacion"];
        $usuario->internet = $data["internet"];
        $usuario->impresora = $data["impresora"];

        $resul = $usuario->save();

        if ($resul) {

            return response()->json(['status' => 'success', 'message' => '<b>¡Registro guardado!</b>']);
        } else {

            return response()->json(['status' => 'warning', 'message' => '<b>hubo un error vuelva a intentarlo!</b>']);

        }

        /* }*/

    }

    public function editar_institucion(Request $request)
    {
        $data = $request->all();
        $idUsuario = $data["id5"];
        $usuario = TableroMonitoreo::find($idUsuario);
        /* if($usuario)
         {*/
        $usuario->ie_rector = $data["nombresr"];
        $usuario->ie_correo = $data["emailr"];
        $usuario->ie_telefono1 = $data["tele1"];
        $usuario->ie_telefono2 = $data["tele2"];

        $resul = $usuario->save();

        if ($resul) {

            return view("mensajes.msj_correcto")->with("msj", "Datos actualizados Correctamente");
        } else {

            return view("mensajes.msj_rechazado")->with("msj", "hubo un error vuelva a intentarlo");

        }

    }

    public function form_monitoreo_instalacion($id)
    {   /////$periodo=Session::get('Periodo', 0);
        $monitoreoInstalacion = TableroMonitoreo::find($id);
        $subCategoriaetiqueta = \DB::select('select * from subcategoria_etiqueta where categoria_etiqueta_id = 5');
        $subCategoriaetiquetaInsta = \DB::select('select * from subcategoria_etiqueta where categoria_etiqueta_id = 6');
        $subCategoriaetiquetaListado = \DB::select('select * from subcategoria_etiqueta where categoria_etiqueta_id = 14');
        $periodo = Session::get('Periodo', 0);
        $codigoveri = \DB::select("select * from sig_codigo_verificacion where cgi_periodo_id = '$periodo'");
        foreach ($codigoveri as $codigo) {
            $codigoVerificacionpc = $codigo->hashpc;
            $codigoVerificacionmovil = $codigo->hashmovil;
        }
        return view("formularios.form_monitoreo_instalacion")->with("subCategoriaetiquetaListado", $subCategoriaetiquetaListado)->with("monitoreoInstalacion", $monitoreoInstalacion)->with("subCategoriaetiqueta", $subCategoriaetiqueta)->with("codigoVerificacionpc", $codigoVerificacionpc)->with("codigoVerificacionmovil", $codigoVerificacionmovil)->with("subCategoriaetiquetaInsta", $subCategoriaetiquetaInsta)->with('id', $id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $monitoreo = TableroMonitoreo::find($id);
        if ($monitoreo) {
            $sesion = "";
            $sesion2 = "";
            $aplicador = "";
            $categoria = "";
            $presentes = "";
            //////sesion
            if ($request->input("sesion") == null) {
                $sesion = $monitoreo->estado_sesion;
            } elseif ($request->input("sesion") != null) {
                $sesion = $request->input("sesion");
            }
            //////sesion2
            if ($request->input("sesion2") == null) {
                $sesion2 = $monitoreo->estado_sesion2;
            } elseif ($request->input("sesion2") != null) {
                $sesion2 = $request->input("sesion2");
            }
            //////aplicador
            if ($request->input("aplicador") == null) {
                $aplicador = $monitoreo->sesion_aplicador;
            } elseif ($request->input("aplicador") != null) {
                $aplicador = $request->input("aplicador");
            }
            //////categoria
            if ($request->input("id_subcategoria_etiqueta") == "") {
                $categoria = $monitoreo->observacion_sesion;
            } elseif ($request->input("id_subcategoria_etiqueta") != "") {
                $categoria = $request->input("id_subcategoria_etiqueta");
            }

            if ($request->input("presentes") == "") {
                $presentes = 0;
            } elseif ($request->input("presentes") != "") {
                $presentes = $request->input("presentes");
            }

            //// update
            $monitoreo->presentes = $presentes;
            $monitoreo->estado_sesion = $sesion;
            ///$monitoreo->estado_sesion2 = $sesion2;
            $monitoreo->sesion_aplicador = $aplicador;
            $monitoreo->observacion_sesion = $categoria;
            if ($monitoreo->save()) {
                return response()->json(['status' => 'success', 'message' => '<b>¡Registro guardado!</b>']);

            }
            ////return response()->json(['sesion' => $sesion]); 

            //return response()->json(['sesion' => $sesion,'aplica' => $aplicador, 'id' => $id, 'categoria' => $categoria]);

        } else {
            /////agregar los campos necesarios
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function form_monitoreo_institucion($id, $id_sede, $idmo)
    {
        ///$institucion = SigInstiitucion::find($id, 'amie');
        ///$institucion = SigInstiitucion::where('proceso', 104)->where('amie', $id);
        //$institucion = \DB::table('sig_institucionv')->where('amie', $id)->where('proceso', Auth::user()->institucion_periodo_id)->first();
        $institucion = \DB::table('sig_institucionv')->where('amie', $id)->first();
        $umonitoreo = \DB::table('umonitoreo')->where('id2', $idmo)->where('cgi_periodo_id', Session::get('Periodo', 0))->first();
        /// $institucion = \DB::select("select * from sig_institucionv where proceso=104 and amie='10H00094'");
        /*$institucion = \DB::select("select * from sig_institucionv where proceso=104 and amie='08H00145'");*/
        return view("formularios.form_monitoreo_institucion")->with("institucion", $institucion)->with("id", $id)->with("umonitoreo", $umonitoreo)->with('idmo', $idmo);

    }


    public function editar_rector_monitoreo(Request $request)
    {
        $editarAplicador = false;
        $editarResponsable = false;
        $editarSProvincial = false;
        $editarSTecnico = false;
        $respuesta = '';

        //TableroMonitoreo
        $celular1_wa = 0;
        $celular2_wa = 0;
        $celular1_swa = 0;
        $celular2_swa = 0;
        $apli_telef1_wa = 0;
        $tecni_telef1_wa = 0;
        if ($request->input("celular1_wa") == "on" or $request->input("celular1_wa") == "1") {
            $celular1_wa = 1;
        } else {
            $celular1_wa = null;
        }
        /////celular2_wa
        if ($request->input("celular2_wa") == "on" or $request->input("celular2_wa") == "1") {
            $celular2_wa = 1;
        } else {
            $celular2_wa = null;
        }
        /////celular1_swa
        if ($request->input("celular1_swa") == "on" or $request->input("celular1_swa") == "1") {
            $celular1_swa = 1;
        } else {
            $celular1_swa = null;
        }
        /////celular2_swa
        if ($request->input("celular2_swa") == "on" or $request->input("celular2_swa") == "1") {
            $celular2_swa = 1;
        } else {
            $celular2_swa = null;
        }
        /////apli_telef1_wa
        if ($request->input("apli_telef1_wa") == "on" or $request->input("apli_telef1_wa") == "1") {
            $apli_telef1_wa = 1;
        } else {
            $apli_telef1_wa = null;
        }
        /////tecni_telef1_wa
        if ($request->input("tecni_telef1_wa") == "on" or $request->input("tecni_telef1_wa") == "1") {
            $tecni_telef1_wa = 1;
        } else {
            $tecni_telef1_wa = null;
        }
        /////corresponsable_celular_wa
        if ($request->input("corresponsable_celular_wa") == "on" or $request->input("corresponsable_celular_wa") == "1") {
            $corresponsable_celular_wa = 1;
        } else {
            $corresponsable_celular_wa = null;
        }
        $data = $request->all();
        $id2 = $data["id11"];
        $amie = $data["id111"];
        ///$umonitoreos = \DB::table('umonitoreo')->where('codigoamei', $amie)->where('cgi_periodo_id', Session::get('Periodo', 0))->first();
        $umonitoreos = TableroMonitoreo::find($amie);
        /////$umonitoreos = \DB:select("select * from umonitoreo where codigoamei='$amie'");
        $validaciones = SigInstiitucion::find($id2);
        if ($validaciones) {
            /*return response()->json(['estadollamada' => $data["estadollamada"], 'novedadllamada' => $data["novedadllamada"], 'id' => $id, 'comunicado' => $data["comunicado"], 'traslado' => $data["traslado"], 'tiempotraslado' => $data["tiempotraslado"]]);*/
            if ($data["estadollamada"] == "CONTACTADO" and $data["novedadllamada"] == "") ///if ($data["estadollamada"]=="" and $data["novedadllamada"]=="")
            {
                ////Responsable de sede
                //$validaciones->participacion = $data["participacion"];
                //$validaciones->institucion = $data["institucion"];
                if ($validaciones->rector !== $data["nombresr"])
                    $editarResponsable = true;
                $validaciones->rector = $data["nombresr"];
                if ($validaciones->numero_id_rector !== $data["numero_id_rector"])
                    $editarResponsable = true;
                $validaciones->numero_id_rector = $data["numero_id_rector"];
                if ($validaciones->celular !== $data["celular"])
                    $editarResponsable = true;
                $validaciones->celular = $data["celular"];
                $validaciones->celular1_wa = $celular1_wa;
                if ($validaciones->celular_whatsapp !== $data["celularw"])
                    $editarResponsable = true;
                $validaciones->celular_whatsapp = $data["celularw"];
                $validaciones->celular2_wa = $celular2_wa;
                if ($validaciones->telefono1 !== $data["telefono1"])
                    $editarResponsable = true;
                $validaciones->telefono1 = $data["telefono1"];
                if ($validaciones->telefono2 !== $data["telefono2"])
                    $editarResponsable = true;
                $validaciones->telefono2 = $data["telefono2"];
                if ($validaciones->contactoinstitucion !== $data["tele1"])
                    $editarResponsable = true;
                $validaciones->contactoinstitucion = $data["tele1"];
                $validaciones->telefono_contactado = $data["telefono_contactado"];
                if ($validaciones->correoinstitucion !== $data["emailr"])
                    $editarResponsable = true;
                $validaciones->correoinstitucion = $data["emailr"];
                if ($validaciones->correo !== $data["correo"])
                    $editarResponsable = true;
                $validaciones->correo = $data["correo"];
                $validaciones->comunicacion = $data["comunicacion"];
                $umonitoreos->estadollamada_apli = $data["estadollamada"];
                $umonitoreos->novedadllamada_apli = $data["novedadllamada"];
                $validaciones->recibiocomunicado = $data["recibiocomunicado"];
                $validaciones->observacion = $data["observacion"];

                ///guia evaluador (aplicador)
                if ($umonitoreos->apli_nombre1 !== $data["apli_nombre1"])
                    $editarAplicador = true;
                $umonitoreos->apli_nombre1 = $data["apli_nombre1"];
                if ($umonitoreos->apli_cedula !== $data["apli_cedula"])
                    $editarAplicador = true;
                $umonitoreos->apli_cedula = $data["apli_cedula"];
                if ($umonitoreos->apli_telef1 !== $data["apli_telef1"])
                    $editarAplicador = true;
                $umonitoreos->apli_telef1 = $data["apli_telef1"];
                $umonitoreos->apli_telef1_wa = $apli_telef1_wa;
                if ($umonitoreos->apli_telef2 !== $data["apli_telef2"])
                    $editarAplicador = true;
                $umonitoreos->apli_telef2 = $data["apli_telef2"];
                if ($umonitoreos->apli_telef3 !== $data["apli_telef3"])
                    $editarAplicador = true;
                $umonitoreos->apli_telef3 = $data["apli_telef3"];
                $umonitoreos->apli_telef_contactado = $data["apli_telef_contactado"];
                if ($umonitoreos->apli_email !== $data["apli_email"])
                    $editarAplicador = true;
                $umonitoreos->apli_email = $data["apli_email"];
                if ($umonitoreos->apli_email2 !== $data["apli_email2"])
                    $editarAplicador = true;
                $umonitoreos->apli_email2 = $data["apli_email2"];
                $umonitoreos->apli_comunicacion = $data["apli_comunicacion"];
                $umonitoreos->estadollamada_aplicadores = $data["estadollamada_aplicadores"];
                $umonitoreos->novedadllamada_aplicadores = $data["novedadllamada_aplicadores"];
                $umonitoreos->asistencia_ineval = $data["asistencia_ineval"];
                $umonitoreos->apli_recepcion = $data["apli_recepcion"];
                $umonitoreos->apli_motivo = $data["apli_motivo"];

                ///tecnico de control
                if ($umonitoreos->tecni_nombre1 !== $data["tecni_nombre1"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_nombre1 = $data["tecni_nombre1"];
                if ($umonitoreos->tecni_cedula !== $data["tecni_cedula"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_cedula = $data["tecni_cedula"];
                if ($umonitoreos->tecni_telef1 !== $data["tecni_telef1"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_telef1 = $data["tecni_telef1"];
                $umonitoreos->tecni_telef1_wa = $tecni_telef1_wa;
                if ($umonitoreos->tecni_telef2 !== $data["tecni_telef2"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_telef2 = $data["tecni_telef2"];
                if ($umonitoreos->tecni_telef3 !== $data["tecni_telef3"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_telef3 = $data["tecni_telef3"];
                $umonitoreos->tecni_telef_contactado = $data["tecni_telef_contactado"];
                if ($umonitoreos->tecni_email !== $data["tecni_email"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_email = $data["tecni_email"];
                if ($umonitoreos->tecni_email2 !== $data["tecni_email2"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_email2 = $data["tecni_email2"];
                $umonitoreos->tecni_comunicacion = $data["tecni_comunicacion"];
                $umonitoreos->estadollamada_tecnico = $data["estadollamada_tecnico"];
                $umonitoreos->novedadllamada_tecnico = $data["novedadllamada_tecnico"];
                $umonitoreos->tecni_asistencia = $data["tecni_asistencia"];
                $umonitoreos->tecni_recibiocomunicado = $data["tecni_recibiocomunicado"];
                $umonitoreos->tecni_motivo = $data["tecni_motivo"];

                ///corresponsables
                if ($umonitoreos->corresponsable_cedula !== $data["corresponsable_cedula"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable_cedula = $data["corresponsable_cedula"];
                if ($umonitoreos->corresponsable !== $data["corresponsable"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable = $data["corresponsable"];
                if ($umonitoreos->corresponsable_celular !== $data["corresponsable_celular"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable_celular = $data["corresponsable_celular"];
                $umonitoreos->corresponsable_celular_wa = $corresponsable_celular_wa;
                if ($umonitoreos->corresponsable_telefono !== $data["corresponsable_telefono"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable_telefono = $data["corresponsable_telefono"];
                if ($umonitoreos->corresponsable_correo !== $data["corresponsable_correo"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable_correo = $data["corresponsable_correo"];
                if ($umonitoreos->corresponsable_correo2 !== $data["corresponsable_correo2"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable_correo2 = $data["corresponsable_correo2"];
                $umonitoreos->corresponsable_comunicacion = $data["corresponsable_comunicacion"];
                $umonitoreos->estadollamada_corresponsable = $data["estadollamada_corresponsable"];
                $umonitoreos->novedadllamada_corresponsable = $data["novedadllamada_corresponsable"];
                $umonitoreos->asistencia_corresponsable = $data["asistencia_corresponsable"];
                $umonitoreos->recepcion_clave_corresponsable = $data["recepcion_clave_corresponsable"];
                $umonitoreos->corresponsable_motivo = $data["corresponsable_motivo"];

                /* ///corresponsable suplete
                $umonitoreos->corresponsable2 = $data["corresponsable2"];
                $umonitoreos->corresponsable2_celular = $data["corresponsable2_celular"];
                $umonitoreos->corresponsable2_telefono = $data["corresponsable2_telefono"];
                $umonitoreos->corresponsable2_correo = $data["corresponsable2_correo"];

                ////para responsable de sede suplente
                $validaciones->celular1_swa = $celular1_swa;
                $validaciones->celular2_swa = $celular2_swa;
                $validaciones->correoinstitucion_suplente = $data["emailrs"];
                $validaciones->rector_suplente = $data["nombresrs"];
                $validaciones->contactoinstitucion_suplente = $data["tele1s"];
                $validaciones->celular_suplente = $data["celulars"];
                $validaciones->telefono1_suplente = $data["telefono1s"];
                $validaciones->telefono2_suplente = $data["telefono2s"];
                $validaciones->correo_suplente = $data["correos"];
                $validaciones->celular_whatsapp_suplente = $data["celularws"];
                $validaciones->comunicacion_suplente = $data["comunicacions"];
                 *
                 */

                if ($validaciones->save()) {
                    $umonitoreos->save();
                    if ($editarAplicador)
                        $respuesta = $this->cambioUsuariosApp('APLICADOR', $umonitoreos, null);
                    if ($editarResponsable)
                        $respuesta = $this->cambioUsuariosApp('RESPONSABLE_SEDE', $umonitoreos, $validaciones);
                    if ($editarSTecnico)
                        $respuesta = $this->cambioUsuariosApp('SUPERVISOR_TECNICO', $umonitoreos, null);
                    if ($editarSProvincial)
                        $respuesta = $this->cambioUsuariosApp('SUPERVISOR_PROVINCIAL', $umonitoreos, null);

                    return response()->json(['status' => 'success', 'message' => '<b>¡Registro guardado!</b>']);
                }
            } elseif ($data["estadollamada"] == "CONTACTADO" and $data["novedadllamada"] == "VOLVER A LLAMAR") {
                ////Responsable de sede
                //$validaciones->participacion = $data["participacion"];
                //$validaciones->institucion = $data["institucion"];
                if ($validaciones->rector !== $data["nombresr"])
                    $editarResponsable = true;
                $validaciones->rector = $data["nombresr"];
                if ($validaciones->numero_id_rector !== $data["numero_id_rector"])
                    $editarResponsable = true;
                $validaciones->numero_id_rector = $data["numero_id_rector"];
                if ($validaciones->celular !== $data["celular"])
                    $editarResponsable = true;
                $validaciones->celular = $data["celular"];
                $validaciones->celular1_wa = $celular1_wa;
                if ($validaciones->celular_whatsapp !== $data["celularw"])
                    $editarResponsable = true;
                $validaciones->celular_whatsapp = $data["celularw"];
                $validaciones->celular2_wa = $celular2_wa;
                if ($validaciones->telefono1 !== $data["telefono1"])
                    $editarResponsable = true;
                $validaciones->telefono1 = $data["telefono1"];
                if ($validaciones->telefono2 !== $data["telefono2"])
                    $editarResponsable = true;
                $validaciones->telefono2 = $data["telefono2"];
                if ($validaciones->contactoinstitucion !== $data["tele1"])
                    $editarResponsable = true;
                $validaciones->contactoinstitucion = $data["tele1"];
                $validaciones->telefono_contactado = $data["telefono_contactado"];
                if ($validaciones->correoinstitucion !== $data["emailr"])
                    $editarResponsable = true;
                $validaciones->correoinstitucion = $data["emailr"];
                if ($validaciones->correo !== $data["correo"])
                    $editarResponsable = true;
                $validaciones->correo = $data["correo"];
                $validaciones->comunicacion = $data["comunicacion"];
                $umonitoreos->estadollamada_apli = $data["estadollamada"];
                $umonitoreos->novedadllamada_apli = $data["novedadllamada"];
                $validaciones->recibiocomunicado = $data["recibiocomunicado"];
                $validaciones->observacion = $data["observacion"];

                ///guia evaluador (aplicador)
                if ($umonitoreos->apli_nombre1 !== $data["apli_nombre1"])
                    $editarAplicador = true;
                $umonitoreos->apli_nombre1 = $data["apli_nombre1"];
                if ($umonitoreos->apli_cedula !== $data["apli_cedula"])
                    $editarAplicador = true;
                $umonitoreos->apli_cedula = $data["apli_cedula"];
                if ($umonitoreos->apli_telef1 !== $data["apli_telef1"])
                    $editarAplicador = true;
                $umonitoreos->apli_telef1 = $data["apli_telef1"];
                $umonitoreos->apli_telef1_wa = $apli_telef1_wa;
                if ($umonitoreos->apli_telef2 !== $data["apli_telef2"])
                    $editarAplicador = true;
                $umonitoreos->apli_telef2 = $data["apli_telef2"];
                if ($umonitoreos->apli_telef3 !== $data["apli_telef3"])
                    $editarAplicador = true;
                $umonitoreos->apli_telef3 = $data["apli_telef3"];
                $umonitoreos->apli_telef_contactado = $data["apli_telef_contactado"];
                if ($umonitoreos->apli_email !== $data["apli_email"])
                    $editarAplicador = true;
                $umonitoreos->apli_email = $data["apli_email"];
                if ($umonitoreos->apli_email2 !== $data["apli_email2"])
                    $editarAplicador = true;
                $umonitoreos->apli_email2 = $data["apli_email2"];
                $umonitoreos->apli_comunicacion = $data["apli_comunicacion"];
                $umonitoreos->estadollamada_aplicadores = $data["estadollamada_aplicadores"];
                $umonitoreos->novedadllamada_aplicadores = $data["novedadllamada_aplicadores"];
                $umonitoreos->asistencia_ineval = $data["asistencia_ineval"];
                $umonitoreos->apli_recepcion = $data["apli_recepcion"];
                $umonitoreos->apli_motivo = $data["apli_motivo"];

                ///tecnico de control
                if ($umonitoreos->tecni_nombre1 !== $data["tecni_nombre1"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_nombre1 = $data["tecni_nombre1"];
                if ($umonitoreos->tecni_cedula !== $data["tecni_cedula"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_cedula = $data["tecni_cedula"];
                if ($umonitoreos->tecni_telef1 !== $data["tecni_telef1"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_telef1 = $data["tecni_telef1"];
                $umonitoreos->tecni_telef1_wa = $tecni_telef1_wa;
                if ($umonitoreos->tecni_telef2 !== $data["tecni_telef2"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_telef2 = $data["tecni_telef2"];
                if ($umonitoreos->tecni_telef3 !== $data["tecni_telef3"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_telef3 = $data["tecni_telef3"];
                $umonitoreos->tecni_telef_contactado = $data["tecni_telef_contactado"];
                if ($umonitoreos->tecni_email !== $data["tecni_email"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_email = $data["tecni_email"];
                if ($umonitoreos->tecni_email2 !== $data["tecni_email2"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_email2 = $data["tecni_email2"];
                $umonitoreos->tecni_comunicacion = $data["tecni_comunicacion"];
                $umonitoreos->estadollamada_tecnico = $data["estadollamada_tecnico"];
                $umonitoreos->novedadllamada_tecnico = $data["novedadllamada_tecnico"];
                $umonitoreos->tecni_asistencia = $data["tecni_asistencia"];
                $umonitoreos->tecni_recibiocomunicado = $data["tecni_recibiocomunicado"];
                $umonitoreos->tecni_motivo = $data["tecni_motivo"];

                ///corresponsables
                if ($umonitoreos->corresponsable_cedula !== $data["corresponsable_cedula"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable_cedula = $data["corresponsable_cedula"];
                if ($umonitoreos->corresponsable !== $data["corresponsable"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable = $data["corresponsable"];
                if ($umonitoreos->corresponsable_celular !== $data["corresponsable_celular"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable_celular = $data["corresponsable_celular"];
                $umonitoreos->corresponsable_celular_wa = $corresponsable_celular_wa;
                if ($umonitoreos->corresponsable_telefono !== $data["corresponsable_telefono"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable_telefono = $data["corresponsable_telefono"];
                if ($umonitoreos->corresponsable_correo !== $data["corresponsable_correo"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable_correo = $data["corresponsable_correo"];
                if ($umonitoreos->corresponsable_correo2 !== $data["corresponsable_correo2"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable_correo2 = $data["corresponsable_correo2"];
                $umonitoreos->corresponsable_comunicacion = $data["corresponsable_comunicacion"];
                $umonitoreos->estadollamada_corresponsable = $data["estadollamada_corresponsable"];
                $umonitoreos->novedadllamada_corresponsable = $data["novedadllamada_corresponsable"];
                $umonitoreos->asistencia_corresponsable = $data["asistencia_corresponsable"];
                $umonitoreos->recepcion_clave_corresponsable = $data["recepcion_clave_corresponsable"];
                $umonitoreos->corresponsable_motivo = $data["corresponsable_motivo"];

                /*///corresponsable suplete
                $umonitoreos->corresponsable2 = $data["corresponsable2"];
                $umonitoreos->corresponsable2_celular = $data["corresponsable2_celular"];
                $umonitoreos->corresponsable2_telefono = $data["corresponsable2_telefono"];
                $umonitoreos->corresponsable2_correo = $data["corresponsable2_correo"];

                ////para responsable de sede suplente
                $validaciones->celular1_swa = $celular1_swa;
                $validaciones->celular2_swa = $celular2_swa;
                $validaciones->correoinstitucion_suplente = $data["emailrs"];
                $validaciones->rector_suplente = $data["nombresrs"];
                $validaciones->contactoinstitucion_suplente = $data["tele1s"];
                $validaciones->celular_suplente = $data["celulars"];
                $validaciones->telefono1_suplente = $data["telefono1s"];
                $validaciones->telefono2_suplente = $data["telefono2s"];
                $validaciones->correo_suplente = $data["correos"];
                $validaciones->celular_whatsapp_suplente = $data["celularws"];
                $validaciones->comunicacion_suplente = $data["comunicacions"];
                 *
                 */

                if ($validaciones->save()) {
                    $umonitoreos->save();
                    if ($editarAplicador)
                        $respuesta = $this->cambioUsuariosApp('APLICADOR', $umonitoreos, null);
                    if ($editarResponsable)
                        $respuesta = $this->cambioUsuariosApp('RESPONSABLE_SEDE', $umonitoreos, $validaciones);
                    if ($editarSTecnico)
                        $respuesta = $this->cambioUsuariosApp('SUPERVISOR_TECNICO', $umonitoreos, null);
                    if ($editarSProvincial)
                        $respuesta = $this->cambioUsuariosApp('SUPERVISOR_PROVINCIAL', $umonitoreos, null);

                    return response()->json(['status' => 'success', 'message' => '<b>¡Registro guardado!</b>']);

                }
            } elseif ($data["estadollamada"] == "NO CONTACTADO" and $data["novedadllamada"] != "") {
                ////Responsable de sede
                $umonitoreos->estadollamada_apli = $data["estadollamada"];
                $umonitoreos->novedadllamada_apli = $data["novedadllamada"];
                $validaciones->observacion = $data["observacion"];

                ///guia evaluador (aplicador)
                if ($umonitoreos->apli_nombre1 !== $data["apli_nombre1"])
                    $editarAplicador = true;
                $umonitoreos->apli_nombre1 = $data["apli_nombre1"];
                if ($umonitoreos->apli_cedula !== $data["apli_cedula"])
                    $editarAplicador = true;
                $umonitoreos->apli_cedula = $data["apli_cedula"];
                if ($umonitoreos->apli_telef1 !== $data["apli_telef1"])
                    $editarAplicador = true;
                $umonitoreos->apli_telef1 = $data["apli_telef1"];
                $umonitoreos->apli_telef1_wa = $apli_telef1_wa;
                if ($umonitoreos->apli_telef2 !== $data["apli_telef2"])
                    $editarAplicador = true;
                $umonitoreos->apli_telef2 = $data["apli_telef2"];
                if ($umonitoreos->apli_telef3 !== $data["apli_telef3"])
                    $editarAplicador = true;
                $umonitoreos->apli_telef3 = $data["apli_telef3"];
                $umonitoreos->apli_telef_contactado = $data["apli_telef_contactado"];
                if ($umonitoreos->apli_email !== $data["apli_email"])
                    $editarAplicador = true;
                $umonitoreos->apli_email = $data["apli_email"];
                if ($umonitoreos->apli_email2 !== $data["apli_email2"])
                    $editarAplicador = true;
                $umonitoreos->apli_email2 = $data["apli_email2"];
                $umonitoreos->apli_comunicacion = $data["apli_comunicacion"];
                $umonitoreos->estadollamada_aplicadores = $data["estadollamada_aplicadores"];
                $umonitoreos->novedadllamada_aplicadores = $data["novedadllamada_aplicadores"];
                $umonitoreos->asistencia_ineval = $data["asistencia_ineval"];
                $umonitoreos->apli_recepcion = $data["apli_recepcion"];
                $umonitoreos->apli_motivo = $data["apli_motivo"];

                ///tecnico de control
                if ($umonitoreos->tecni_nombre1 !== $data["tecni_nombre1"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_nombre1 = $data["tecni_nombre1"];
                if ($umonitoreos->tecni_cedula !== $data["tecni_cedula"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_cedula = $data["tecni_cedula"];
                if ($umonitoreos->tecni_telef1 !== $data["tecni_telef1"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_telef1 = $data["tecni_telef1"];
                $umonitoreos->tecni_telef1_wa = $tecni_telef1_wa;
                if ($umonitoreos->tecni_telef2 !== $data["tecni_telef2"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_telef2 = $data["tecni_telef2"];
                if ($umonitoreos->tecni_telef3 !== $data["tecni_telef3"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_telef3 = $data["tecni_telef3"];
                $umonitoreos->tecni_telef_contactado = $data["tecni_telef_contactado"];
                if ($umonitoreos->tecni_email !== $data["tecni_email"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_email = $data["tecni_email"];
                if ($umonitoreos->tecni_email2 !== $data["tecni_email2"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_comunicacion = $data["tecni_comunicacion"];
                $umonitoreos->estadollamada_tecnico = $data["estadollamada_tecnico"];
                $umonitoreos->novedadllamada_tecnico = $data["novedadllamada_tecnico"];
                $umonitoreos->tecni_asistencia = $data["tecni_asistencia"];
                $umonitoreos->tecni_recibiocomunicado = $data["tecni_recibiocomunicado"];
                $umonitoreos->tecni_motivo = $data["tecni_motivo"];

                ///corresponsables
                if ($umonitoreos->corresponsable_cedula !== $data["corresponsable_cedula"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable_cedula = $data["corresponsable_cedula"];
                if ($umonitoreos->corresponsable !== $data["corresponsable"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable = $data["corresponsable"];
                if ($umonitoreos->corresponsable_celular !== $data["corresponsable_celular"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable_celular = $data["corresponsable_celular"];
                $umonitoreos->corresponsable_celular_wa = $corresponsable_celular_wa;
                if ($umonitoreos->corresponsable_telefono !== $data["corresponsable_telefono"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable_telefono = $data["corresponsable_telefono"];
                if ($umonitoreos->corresponsable_correo !== $data["corresponsable_correo"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable_correo = $data["corresponsable_correo"];
                if ($umonitoreos->corresponsable_correo2 !== $data["corresponsable_correo2"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable_correo2 = $data["corresponsable_correo2"];
                $umonitoreos->corresponsable_comunicacion = $data["corresponsable_comunicacion"];
                $umonitoreos->estadollamada_corresponsable = $data["estadollamada_corresponsable"];
                $umonitoreos->novedadllamada_corresponsable = $data["novedadllamada_corresponsable"];
                $umonitoreos->asistencia_corresponsable = $data["asistencia_corresponsable"];
                $umonitoreos->recepcion_clave_corresponsable = $data["recepcion_clave_corresponsable"];
                $umonitoreos->corresponsable_motivo = $data["corresponsable_motivo"];


                if ($umonitoreos->save()) {
                    $validaciones->save();
                    if ($editarAplicador)
                        $respuesta = $this->cambioUsuariosApp('APLICADOR', $umonitoreos, null);
                    if ($editarSTecnico)
                        $respuesta = $this->cambioUsuariosApp('SUPERVISOR_TECNICO', $umonitoreos, null);
                    if ($editarSProvincial)
                        $respuesta = $this->cambioUsuariosApp('SUPERVISOR_PROVINCIAL', $umonitoreos, null);

                    return response()->json(['status' => 'success', 'message' => '<b>¡Registro guardado!</b>']);
                } else {
                    return response()->json(['status' => 'warning', 'message' => '<b>¡Error al actualizar.!</b>']);
                }
            } else {
                return response()->json(['status' => 'warning', 'message' => '<b>¡Selección Incorrecta!</b>']);
            }
        } else {
            return response()->json(['status' => 'warning', 'message' => '<b>¡Error al actualizar!</b>']);
        }
    }

}
