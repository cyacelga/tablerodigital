<?php namespace App\Http\Controllers;

use App\User;
use App\Monitoreo;

use Illuminate\Support\Facades\Auth;
use Storage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Excel;
use Session;

class UsuariosController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function form_nuevo_usuario()
	{
		 /*$monitoreo= Monitoreo::where('cgi_periodo_id', Session::get('Periodo', 0))->where('monitor', Auth::user()->username)->orderBy('codigoamei', 'asc')->orderBy('fecha_programada_inicio', 'asc')->paginate(10);*/
    ////$subcategoria = 
        ///return view('listados.listado_monitoreo')->with("monitoreo", $monitoreo );
		return view('formularios.form_nuevo_usuario');
	}


	//presenta el formulario para nuevo usuario
	public function agregar_nuevo_usuario(Request $request)
	{

		$data=$request->all();

		//////VALIDAR QUE EL CORREO NO SE REPITA

		$usuario= new User;
		$usuario->first_name  =  $data["nombres"];
		$usuario->last_name=$data["apellidos"];
		$usuario->email=$data["email"];
		$usuario->active='1';
		$usuario->file='DACT_SIGET';
		$usuario->password=bcrypt($data["password"]);

		$resul= $usuario->save();

		if($resul){
            
            return view("mensajes.msj_correcto")->with("msj","Usuario Registrado Correctamente");   
		}
		else
		{
             
             return view("mensajes.msj_rechazado")->with("msj","hubo un error vuelva a intentarlo");  

		}


	}

			public function listado_usuarios()
   {

     ////$roles = Role::where('active', true)->orderBy('name', 'asc')->get();
        //$usuarios= User::paginate(10);
         $usuarios= User::where('file', 'DACT_SIGET')->orderBy('id', 'asc')->paginate(10);
         //$usuarios= User::where('file', 'DACT_SIGET')->orderBy('id', 'asc')->paginate(10);
        return view('listados.listado_usuarios')->with("usuarios", $usuarios );
        
	}

	public function form_editar_usuario($id)
	{
		//funcion para cargar los datos de cada usuario en la ficha
		$usuario=User::find($id);
		$contador=count($usuario);
		if($contador>0){          
            return view("formularios.form_editar_usuario")->with("usuario",$usuario);   
		}
		else
		{            
            return view("mensajes.msj_rechazado")->with("msj","el usuario con ese id no existe o fue borrado");  
		}
	}



		public function editar_usuario(Request $request)
	{

		$data=$request->all();
		$idUsuario=$data["id_usuario"];
		$usuario=User::find($idUsuario);
        $usuario->first_name  =  $data["nombres"];
		$usuario->last_name=$data["apellidos"];
		$usuario->email=$data["email"];
		
		$resul= $usuario->save();

		if($resul){
            
            return view("mensajes.msj_correcto")->with("msj","Datos actualizados Correctamente");   
		}
		else
		{
             
            return view("mensajes.msj_rechazado")->with("msj","hubo un error vuelva a intentarlo");  

		}
	}

	///l8
	public function subir_imagen_usuario(Request $request)
	{

	    $id=$request->input('id_usuario_foto');
		$archivo = $request->file('archivo');
        $input  = array('image' => $archivo) ;
        $reglas = array('image' => 'required|image|mimes:jpeg,jpg,bmp,png,gif|max:2000');
        $validacion = Validator::make($input,  $reglas);
        if ($validacion->fails())
        {
          return view("mensajes.msj_rechazado")->with("msj","El archivo no es una imagen valida");
        }
        else
        {

	        $nombre_original=$archivo->getClientOriginalName();
			$extension=$archivo->getClientOriginalExtension();
			$nuevo_nombre="userimagen-".$id.".".$extension;
		    $r1=Storage::disk('fotografias')->put($nuevo_nombre,  \File::get($archivo) );
		    $rutadelaimagen="../storage/fotografias/".$nuevo_nombre;
	    
		    if ($r1){

			    $usuario=User::find($id);
			    $usuario->image=$rutadelaimagen;
			    $r2=$usuario->save();
		        return view("mensajes.msj_correcto")->with("msj","Imagen agregada correctamente");
		    }
		    else
		    {
		    	

		    }


        }



	


	}


	public function cambiar_password(Request $request){

		$id=$request->input("id_usuario_password");
		$email=$request->input("email_usuario");
		$password=$request->input("password_usuario");
		$usuario=User::find($id);
	    $usuario->email=$email;
	    $usuario->password=bcrypt($password);
	    $r=$usuario->save();

	    if($r){
           return view("mensajes.msj_correcto")->with("msj","password actualizado");
	    }
	    else
	    {
	    	return view("mensajes.msj_rechazado")->with("msj","Error al actualizar el password");
	    }



	}



	public function form_cargar_datos_usuarios(){

       return view("formularios.form_cargar_datos_usuarios");

	}


    public function cargar_datos_usuarios(Request $request)
	{
       $archivo = $request->file('archivo');
       $nombre_original=$archivo->getClientOriginalName();
	   $extension=$archivo->getClientOriginalExtension();
       $r1=Storage::disk('archivos')->put($nombre_original,  \File::get($archivo) );
       $ruta  =  storage_path('archivos') ."/". $nombre_original;
       
       if($r1){
       	    $ct=0;
            Excel::selectSheetsByIndex(0)->load($ruta, function($hoja) {
		        
		        $hoja->each(function($fila) {
			        $usersemails=User::where("email","=",$fila->email)->first();
			        if(count( $usersemails)==0){
				      	$usuario=new User;
				        $usuario->first_name= $fila->nombres;
				        $usuario->last_name= $fila->apellidos;
				        $usuario->email= $fila->email;
		                $usuario->password= bcrypt($fila->password);
		                $usuario->file= $fila->file;
		                $usuario->save();
	                }
		     
		        });

            });

            return view("mensajes.msj_correcto")->with("msj"," Usuarios Cargados Correctamente");
    	
       }
       else
       {
       	    return view("mensajes.msj_rechazado")->with("msj","Error al subir el archivo");
       }

	}

}