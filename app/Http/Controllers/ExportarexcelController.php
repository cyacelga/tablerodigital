<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\SigInstiitucion;
use App\ViewTabValidacionLab;
use App\ViewTabAplicacion;
use App\ListaSustentantes_off;
use Maatwebsite\Excel\Facades\Excel;

class ExportarexcelController extends Controller
{
    
    public function exportar_exceluno($id)
    {      
        Excel::create('DACT_VALIDACION_LABORATORIOS', function($excel) use ($id) {
        $excel->sheet('SEDES_CON_LABORATORIOS', function($sheet) use ($id) {        
        // Header
       // $sheet->mergeCells('A1:F1'); 
        //$sheet->row(1, ['Reporte de Prueba Uno']);        
        $sheet->cells('A1:R1', function($cells){
            $cells->setBackground('#0489B1');
            $cells->setFontColor('#FFFFFF');
            $cells->setAlignment('center');
            $cells->setValignment('center');
            $cells->setFontweight('bold');
        });        
        // Data            
        $reporteexcel = ViewTabValidacionLab::where('proceso',$id)->get();
        $sheet->fromArray($reporteexcel);    
        $sheet->row(1, ['PERIODO', 'RESPONSABLE', 'AMIE', 'INSTITUCION', 'ZONA', 'PROVINCIA', 'DISTRITO', 'MAQUINAS VALIDADAS','IMPRESORA', 'INTERNET', 'SISTEMA',
        'OBSERVACIÓN', 'MAQUINAS REQUERIDAS', 'LABORATORIO','ESTADO DE LLAMADA','NOVEDAD DE LLAMADA','DICTAMEN','LABORATORIO NUEVO']);
        });
    })->export('xlsx');    
    }
    
    public function exportar_llamadas($id)
    {      
        Excel::create('DACT_LLAMADAS_RECTORES', function($excel) use ($id) {            
        $excel->sheet('DD', function($sheet){       
              
        $sheet->cells('A1:B1', function($cells){
            $cells->setBackground('#0489B1');
            $cells->setFontColor('#FFFFFF');
            $cells->setAlignment('center');
            $cells->setValignment('center');
            $cells->setFontweight('bold');
        });
        
        $sheet->row(1,['CAMPO','DESCRIPCIÓN']);
        $sheet->row(2,['COD_RESPONSABLE','CÓDIGO RESPONSABLE']);
        $sheet->row(3,['AMIE','AMIE DE LA INSTITUCIÓN EDUCATIVA']);
        $sheet->row(4,['INSTITUCION','NOMBRE DE LA INSTITUCIÓN EDUCATIVA']);
        $sheet->row(5,['ZONA','ZONA A LA QUE PERTENECE LA INSTITUCIÓN EDUCATIVA']);
        $sheet->row(6,['PROVINCIA','PROVINCIA A LA QUE PERTENECE LA INSTITUCIÓN']);
        $sheet->row(7,['ID_DISTRITO','DISTRITO A LA QUE PERTENECE LA INSTITUCIÓN']);
        $sheet->row(8,['RECTOR','NOMBRE DEL RESPONSABLE DE SEDE']);
        $sheet->row(9,['ID_RECTOR','IDENTIFICADOR DEL RESPONSABLE DE SEDE']);
        $sheet->row(10,['CELULAR_1','NÚMERO CELULAR 1 RESPONSABLE DE SEDE']);
        $sheet->row(11,['WHATSAPP_1','SÍ EL CELULAR 1 CUENTA CON WHATSAPP']);
        $sheet->row(12,['CELULAR_2','NÚMERO CELULAR 2 DEL RESPONSABLE DE SEDE']);
        $sheet->row(13,['WHATSAPP_2','SÍ EL CELULAR 2 CUENTA CON WHATSAPP']);
        $sheet->row(14,['TELÉFONO FIJO 1','NÚMERO TELÉFONO FIJO PRINCIPAL']);
        $sheet->row(15,['TELÉFONO FIJO 2','NÚMERO TELÉFONO FIJO  SECUNDARIO']);
        $sheet->row(16,['TELÉFONO CONTACTADO','NÚMERO TELÉFONO CONTACTADO']);
        $sheet->row(17,['CORREO 1','CORREO ELECTRÓNICO PRINCIPAL']);
        $sheet->row(18,['CORREO 2','CORREO ELECTRÓNICO SECUNDARIO']);
        $sheet->row(19,['COMUNICACIÓN RÁPIDA','MEDIO DE COMUNICACIÓN DEL RESPONSABLE DE SEDE']);
        $sheet->row(20,['ESTADO DE LLAMADA','ESTADO DE LLAMADA']);
        $sheet->row(21,['NOVEDAD DE LLAMADA','NOVEDAD DE LLAMADA']);
        $sheet->row(22,['RECEPCIÓN DE COMUNICADO','SI EL RESPONSABLE DE SEDE RECIBIÓ EL COMUNICADO']);
        $sheet->row(23,['4TO EGB','SI CUENTA CON 4TO DE EDUCACIÓN GENERAL BÁSICA']);
        $sheet->row(24,['N° 4TO EGB','NÚMERO DE ESTUDIANTES DE 4TO DE EDUCACIÓN GENERAL BÁSICA']);
        $sheet->row(25,['7MO EGB','SI CUENTA CON 7MO DE EDUCACIÓN GENERAL BÁSICA']);
        $sheet->row(26,['N° 7MO EGB','NÚMERO DE ESTUDIANTES  DE 7MO DE EDUCACIÓN GENERAL BÁSICA']);
        $sheet->row(27,['10MO EGB','SI CUENTA CON 10MO DE EDUCACIÓN GENERAL BÁSICA']);
        $sheet->row(28,['N° 10MO EGB','NÚMERO DE ESTUDIANTES DE 10MO DE EDUCACIÓN GENERAL BÁSICA']);
        $sheet->row(29,['3ERO BGU','SI CUENTA CON 3ERO DE BACHILLERATO GENERAL UNIFICADO']);
        $sheet->row(30,['N° 3ERO BGU','NÚMERO DE ESTUDIANTES DE 3ERO DE BACHILLERATO GENERAL UNIFICADO']);                 
        });    

        $excel->sheet('Datos', function($sheet) use ($id) {        
        // Header
       // $sheet->mergeCells('A1:F1'); 
        //$sheet->row(1, ['Reporte de Prueba Uno']);        
        $sheet->cells('A1:AD1', function($cells){
            $cells->setBackground('#0489B1');
            $cells->setFontColor('#FFFFFF');
            $cells->setAlignment('center');
            $cells->setValignment('center');
            $cells->setFontweight('bold');
        });
        
        // Data  
        $exportLlamadas= SigInstiitucion::select('monitor','amie','institucion', 'zona', 'provincia','distrito','rector','numero_id_rector','celular','celular1_wa','celular_whatsapp','celular2_wa',
	'telefono1', 'telefono2','telefono_contactado','correoinstitucion','correo','comunicacion','estadollamada','novedadllamada','recibiocomunicado',
	'cuarto_egb', 'cuarto_egb_n', 'septimo_egb','septimo_egb_n','decimo_egb','decimo_egb_n','tercero_bgu','tercero_bach_n','observacion')->where('proceso',$id)->where('estado',1)->get();
        $sheet->fromArray($exportLlamadas); 
        $sheet->row(1, ['COD_RESPONSABLE','AMIE','INSTITUCION','ZONA','PROVINCIA','ID_DISTRITO','RECTOR','ID_RECTOR','CELULAR_1','WHATSAPP_1','CELULAR_2','WHATSAPP_2',
            'TELÉFONO FIJO 1','TELÉFONO FIJO 2','TELÉFONO CONTACTADO','CORREO 1','CORREO 2','COMUNICACIÓN RÁPIDA','ESTADO DE LLAMADA','NOVEDAD DE LLAMADA',
            'RECEPCIÓN DE COMUNICADO','4TO EGB','N° 4TO EGB','7MO EGB','N° 7MO EGB','10MO EGB','N° 10MO EGB','3ERO BGU','N° 3ERO BGU','OBSERVACIÓN' ]);                
        });
    })->export('xlsx');    
    }
    
    public function exportar_llamadas_apliValidacion($id)
    {      
        Excel::create('DACT_LLAMADAS_APLICADORES_VALIDACION', function($excel) use ($id) {
                        
        $excel->sheet('Datos', function($sheet) use ($id) {
        
        // Header
       // $sheet->mergeCells('A1:F1'); 
        //$sheet->row(1, ['Reporte de Prueba Uno']);
        $sheet->row(1, ['LABORATORIO', 'JORNADA', 'SESION', 'HORA CONVOCATORIA', 'HORA SALIDA', 'FECHA DE APLICACION', 'IDENTIFICACION APLICADOR', 'NOMBRES DEL APLICADOR',
            'TELEFONO APLICADOR', 'CELULAR APLICADOR', 'CORREO INSTITUCIONAL', 'CORREO PERSONAL', 'ASISTENCIA', 'ESTADO DE LLAMADA', 'NOVEDAD DE LLAMADA',
            'AMIE ORIGEN', 'INSTITUCION ORIGEN' ]);
        
        $sheet->cells('A1:Q1', function($cells){
            $cells->setBackground('#0489B1');
            $cells->setFontColor('#FFFFFF');
            $cells->setAlignment('center');
            $cells->setValignment('center');
            $cells->setFontweight('bold');
        });
                
        // Data            
        $exportLlamadas = \DB::select("select id_sede as laboratorio, jornada, sesion, hora_convocatoria, hora_salida,fecha_aplicacion,
        identificacion as identificacion_aplicador, nombres as nombres_aplicador,
        telefono as telefono_aplicador, celular as celular_aplicador, correoinstitucional, correopersonal,
        asistencia, estadollamada, novedadllamada, amie_origen, institucion_origen  
        from sig_aplicadores where cgi_periodo_id = $id ");
        
        foreach ($exportLlamadas as $llamadas){
        
            $row = [];
            $row[0] = $llamadas->laboratorio;                  
            $row[1] = $llamadas->jornada;
            $row[2] = $llamadas->sesion;
            $row[3] = $llamadas->hora_convocatoria;
            $row[4] = $llamadas->hora_salida;
            $row[5] = $llamadas->fecha_aplicacion;
            $row[6] = $llamadas->identificacion_aplicador;
            $row[7] = $llamadas->nombres_aplicador;
            $row[8] = $llamadas->telefono_aplicador;
            $row[9] = $llamadas->celular_aplicador;
            $row[10] = $llamadas->correoinstitucional;
            $row[11] = $llamadas->correopersonal;
            $row[12] = $llamadas->asistencia;
            $row[13] = $llamadas->estadollamada;
            $row[14] = $llamadas->novedadllamada;
            $row[15] = $llamadas->amie_origen;
            $row[16] = $llamadas->institucion_origen;
            
            $sheet->appendRow($row);
            }                
        });

    })->export('csv');
    
    }
    
    public function exportar_llamadas_apli($id)
    {      
        Excel::create('DACT_LLAMADAS_APLICADORES', function($excel) use ($id) {

        $excel->sheet('Datos', function($sheet) use ($id) {
        
        // Header
       // $sheet->mergeCells('A1:F1'); 
        //$sheet->row(1, ['Reporte de Prueba Uno']);
        $sheet->row(1, ['AMIE', 'LABORATORIO', 'IDENTIFICACION','NOMBRES', 'TELEFONO', 'CELULAR','CORREO INSTITUCIONAL', 'CORREO PERSONAL', 
            'ASISTENCIA', 'ESTADO LLAMADA', 'NOVEDAD LLAMADA', 'OBSERVACIONES', /* 'FECHA PROGRAMADA INICIO', 'SESION',*/ 'COORDINADOR', 'ZONA', 'PERIODO'
            ]);
        
        $sheet->cells('A1:O1', function($cells){
            $cells->setBackground('#0489B1');
            $cells->setFontColor('#FFFFFF');
            $cells->setAlignment('center');
            $cells->setValignment('center');
            $cells->setFontweight('bold');
        });
        
        // Data            
        $exportLlamadas_apli = \DB::select("select ap.amie, ap.id_sede, ap.identificacion, ap.nombres, ap.telefono, ap.celular, ap.correoinstitucional, ap.correopersonal, 
ap.asistencia_apli, ap.estadollamada_apli, ap.novedadllamada_apli, ap.observaciones, ap.claveyusuario, u.fecha_programada_inicio, u.sesion, u.coordinador, u.zona, u.periodo_monitoreo
from (select fecha_programada_inicio, sesion, coordinador, zona, periodo_monitoreo, codigoamei from umonitoreo 
where cgi_periodo_id = $id and estado=1
group by fecha_programada_inicio, sesion, coordinador, zona, periodo_monitoreo, codigoamei
) as  u, sig_aplicadores ap 
where ap.amie=u.codigoamei and ap.cgi_periodo_id=104");
        
        foreach ($exportLlamadas_apli as $llamadas){
        
            $row = [];
            $row[0] = $llamadas->amie;
            $row[1] = $llamadas->id_sede;
            $row[2] = $llamadas->identificacion;
            $row[3] = $llamadas->nombres;
            $row[4] = $llamadas->telefono;
            $row[5] = $llamadas->celular;
            $row[6] = $llamadas->correoinstitucional;
            $row[7] = $llamadas->correopersonal;
            $row[8] = $llamadas->asistencia_apli;
            $row[9] = $llamadas->estadollamada_apli;
            $row[10] = $llamadas->novedadllamada_apli;
            $row[11] = $llamadas->observaciones;
           /* $row[12] = $llamadas->fecha_programada_inicio;
            $row[13] = $llamadas->sesion; */
            $row[12] = $llamadas->coordinador;
            $row[13] = $llamadas->zona;
            $row[14] = $llamadas->periodo_monitoreo;
            
            
            $sheet->appendRow($row);
            }                
        });

    })->export('xlsx');
    
    }
    
     public function exportar_info_aplicacion($id)
    {      
        Excel::create('DACT_APLICACION', function($excel) use ($id) {
        $excel->sheet('DATOS', function($sheet) use ($id) {        
        // Header
       // $sheet->mergeCells('A1:F1'); 
        //$sheet->row(1, ['Reporte de Prueba Uno']);        
        $sheet->cells('A1:BR1', function($cells){ 
            $cells->setBackground('#0489B1');
            $cells->setFontColor('#FFFFFF');
            $cells->setAlignment('center');
            $cells->setValignment('center');
            $cells->setFontweight('bold');
        });        
        // Data    
/* Campos de la vista
'monitor','name_monitor','cgi_periodo_id','amie','laboratorio','institucion','zona','provincia','canton','parroquia','distrito','direccion','fecha_programada_inicio','sesion',
'rector','numero_id_rector','correoinstitucion','correo','telefono1','telefono2','celular','celular1_wa','celular_whatsapp','celular2_wa','contactoinstitucion',
'comunicacion','telefono_contactado','estadollamada_apli','novedadllamada_apli','recibiocomunicado','observacion','programados','asistencia','ausentes','
corresponsable','corresponsable_cedula','corresponsable_celular','corresponsable_celular_wa','corresponsable_telefono','corresponsable_correo','corresponsable_correo2','corresponsable_comunicacion','estadollamada_corresponsable','novedadllamada_corresponsable','asistencia_corresponsable','corresponsable_motivo','
apli_nombre1','apli_cedula','apli_telef1','apli_telef1_wa','apli_telef2','apli_telef3','apli_telef_contactado','apli_email','apli_email2','apli_comunicacion','estadollamada_aplicadores','novedadllamada_aplicadores','asistencia_ineval','apli_recepcion','apli_motivo',
'tecni_nombre1','tecni_cedula','tecni_telef1','tecni_telef1_wa','tecni_telef2','tecni_telef3','tecni_telef_contactado','tecni_email','tecni_email2','tecni_comunicacion','estadollamada_tecnico','novedadllamada_tecnico','tecni_asistencia','tecni_recibiocomunicado','tecni_motivo',
'recepcion_aplicativo','descarga_aplicativo','observacion_descarga','instalo_aplicativo','observacion_instala','maquinas_instaladas','maquinas_faltantes','estado_sesion','observacion_sesion','poblacion','internet','impresora','listado_asistencia','observacion_listado_asistencia','listado_validacion'
*/        
        $reporteexcel = ViewTabAplicacion::select('monitor','name_monitor','cgi_periodo_id','amie','laboratorio','institucion','zona','provincia','canton','parroquia','distrito','direccion','fecha_programada_inicio','sesion',
		'correoinstitucion','telefono1','telefono2','celular','celular1_wa',
		'comunicacion','telefono_contactado','estadollamada_apli','novedadllamada_apli','recibiocomunicado','observacion','programados','asistencia','ausentes',
		'corresponsable','corresponsable_cedula','corresponsable_celular','corresponsable_celular_wa','corresponsable_telefono','corresponsable_correo','corresponsable_correo2','corresponsable_comunicacion','estadollamada_corresponsable','novedadllamada_corresponsable','asistencia_corresponsable','corresponsable_motivo',
		'apli_nombre1','apli_cedula','apli_telef1','apli_telef1_wa','apli_telef2','apli_telef3','apli_telef_contactado','apli_email','apli_email2','apli_comunicacion','estadollamada_aplicadores','novedadllamada_aplicadores','asistencia_ineval','apli_recepcion','apli_motivo',
		'recepcion_aplicativo','descarga_aplicativo','observacion_descarga','instalo_aplicativo','observacion_instala','maquinas_instaladas','maquinas_faltantes','estado_sesion','observacion_sesion','poblacion','internet','impresora','listado_asistencia','observacion_listado_asistencia','listado_validacion')
		->where('cgi_periodo_id',$id)->whereNull('estado_sesion2')->get();
        $sheet->fromArray($reporteexcel);    
        $sheet->row(1, ['MONITOR', 'NOMBRE MONITOR', 'PERIODO', 'AMIE', 'LABORATORIO', 'INSTITUCION', 'ZONA', 'PROVINCIA', 'CANTON', 'PARROQUIA', 'DISTRITO', 'DIRECCIÓN', 'FECHA_PROCESO', 'SESION', 
        /*'NOMBRE RECTOR', 'CÉDULA RECTOR',*/ 'CORREO INSTITUCIÓN', /*'CORREO RECTOR',*/ 'TELEFONO1', 'TELEFONO2', 'CELULAR 1', 'WHATSAPP CELULAR 1', /*'CELULAR 2 RECTOR', 'WHATSAPP CELULAR 2 RECTOR','TELEFONO3 RECTOR',*/ 
        'COMUNICACIÓN', 'TELEFONO CONTACTADO', 'ESTADO DE LLAMADA', 'PROBLEMA DE LLAMADA','RECIBIÓ COMUNICADO','OBSERVACIÓN','SUSTENTANTES PROGRAMADOS','SUSTENTANTES CONFIRMADOS', 'SUSTENTANTES AUSENTES',
        'NOMBRE CORRESPONSABLE', 'CÉDULA CORRESPONSABLE', 'CELULAR CORRESPONSABLE','WHATSAPP CELULAR CORRESPONSABLE', 'TELEFONO CORRESPONSABLE', 'CORREO 1 CORRESPONSABLE', 'CORREO 2 CORRESPONSABLE', 'COMUNICACIÓN CORRESPONSABLE', 'ESTADO DE LLAMADA CORRESPONSABLE', 'PROBLEMA DE LLAMADA CORRESPONSABLE', 'ASISTENCIA DEL CORRESPONSABLE', /*'RECIBIÓ CLAVE CORRESPONSABLE',*/'OBSERVACIÓN CORRESPONSABLE',
        /*'NOMBRE SUPERVISOR', 'CÉDULA SUPERVISOR', 'CELULAR SUPERVISOR','WHATSAPP CELULAR SUPERVISOR', 'TELEFONO SUPERVISOR', 'CORREO 1 SUPERVISOR', 'CORREO 2 SUPERVISOR', 'COMUNICACIÓN SUPERVISOR', 'ESTADO DE LLAMADA SUPERVISOR', 'PROBLEMA DE LLAMADA SUPERVISOR', 'ASISTENCIA DEL SUPERVISOR', 'OBSERVACIÓN SUPERVISOR',*/
        'NOMBRE GUÍA EVALUADOR', 'CÉDULA GUÍA EVALUADOR', 'CELULAR 1 GUÍA EVALUADOR','WHATSAPP CELULAR 1 GUÍA EVALUADOR', 'CELULAR 2 GUÍA EVALUADOR', 'TELEFONO 1 GUÍA EVALUADOR', 'TELEFONO CONTACTADO GUÍA EVALUADOR', 'CORREO 1 GUÍA EVALUADOR', 'CORREO 2 GUÍA EVALUADOR', 'COMUNICACIÓN GUÍA EVALUADOR', 'ESTADO DE LLAMADA GUÍA EVALUADOR', 'PROBLEMA DE LLAMADA GUÍA EVALUADOR', 'ASISTENCIA DEL GUÍA EVALUADOR', 'RECIBIÓ COMUNICADO GUÍA EVALUADOR','OBSERVACIÓN GUÍA EVALUADOR',
        /*'NOMBRE GUÍA EVALUADOR 1', 'CÉDULA GUÍA EVALUADOR 1', 'CELULAR 1 GUÍA EVALUADOR 1','WHATSAPP CELULAR 1 GUÍA EVALUADOR 1', 'CELULAR 2 GUÍA EVALUADOR 1', 'TELEFONO 1 GUÍA EVALUADOR 1', 'TELEFONO CONTACTADO GUÍA EVALUADOR 1', 'CORREO 1 GUÍA EVALUADOR 1', 'CORREO 2 GUÍA EVALUADOR 1', 'COMUNICACIÓN GUÍA EVALUADOR 1', 'ESTADO DE LLAMADA GUÍA EVALUADOR 1', 'PROBLEMA DE LLAMADA GUÍA EVALUADOR 1', 'ASISTENCIA DEL GUÍA EVALUADOR 1', 'RECIBIÓ COMUNICADO GUÍA EVALUADOR 1','OBSERVACIÓN GUÍA EVALUADOR 1',*/
        /*'NOMBRE TÉCNICO ', 'CÉDULA TÉCNICO ', 'CELULAR 1 TÉCNICO ','WHATSAPP CELULAR 1 TÉCNICO ', 'CELULAR 2 TÉCNICO ', 'TELEFONO 1 TÉCNICO ', 'TELEFONO CONTACTADO TÉCNICO ', 'CORREO 1 TÉCNICO ', 'CORREO 2 TÉCNICO ', 'COMUNICACIÓN TÉCNICO ', 'ESTADO DE LLAMADA TÉCNICO ', 'PROBLEMA DE LLAMADA TÉCNICO', 'ASISTENCIA DEL TÉCNICO', 'RECIBIÓ COMUNICADO TÉCNICO','OBSERVACIÓN DEL TÉCNICO',*/
        /*'NOMBRE GUÍA EVALUADOR 2 ', 'CÉDULA GUÍA EVALUADOR 2 ', 'CELULAR 1 GUÍA EVALUADOR 2 ','WHATSAPP CELULAR 1 GUÍA EVALUADOR2 ', 'CELULAR 2 GUÍA EVALUADOR 2 ', 'TELEFONO 1 GUÍA EVALUADOR 2 ', 'TELEFONO CONTACTADO GUÍA EVALUADOR 2 ', 'CORREO 1 GUÍA EVALUADOR 2 ', 'CORREO 2 GUÍA EVALUADOR 2 ', 'COMUNICACIÓN GUÍA EVALUADOR 2 ', 'ESTADO DE LLAMADA GUÍA EVALUADOR 2 ', 
        'PROBLEMA DE LLAMADA GUÍA EVALUADOR 2 ', 'ASISTENCIA DEL GUÍA EVALUADOR 2 ', 'RECIBIÓ COMUNICADO GUÍA EVALUADOR 2 ','OBSERVACIÓN GUÍA EVALUADOR 2', */ 
		'RECIBIÓ APLICATIVO', 'DESCARGÓ APLICATIVO', 'OBSERVACIÓN DESCARGA', 'INSTALÓ APLICATIVO', 'OBSERVACIÓN INSTALACIÓN', 'MAQUINAS INSTALADAS', 'MAQUINAS FALTANTES','ESTADO SESIÓN', 'OBSERVACIÓN SESIÓN', 'POBLACIÓN', 'INTERNET', 'IMPRESORA', 'LISTADO','INCIDENCIA LISTADO','CUADRE LISTADO']);
        });
    })->export('xlsx');    
    }
    /// HETEROEVALUACIÓN
    public function exportar_info_sustentantes_off($id)
    {      
        Excel::create('DACT_HETEROEVALUACION_DOCENTES', function($excel) use ($id) {
        $excel->sheet('DATOS', function($sheet) use ($id) {        
        // Header
       // $sheet->mergeCells('A1:F1'); 
        //$sheet->row(1, ['Reporte de Prueba Uno']);        
        $sheet->cells('A1:H1', function($cells){
            $cells->setBackground('#0489B1');
            $cells->setFontColor('#FFFFFF');
            $cells->setAlignment('center');
            $cells->setValignment('center');
            $cells->setFontweight('bold');
        });        
        // Data            
        $reporteexcel = ListaSustentantes_off::select('proceso_id', 'institucion_id', 'institucion', 'identificacion', 'nombres', \DB::raw('asistencia::text'), 'sustentante_observacion', 'cargo')->where('proceso_id',$id)->get();
        $sheet->fromArray($reporteexcel);    
        $sheet->row(1, ['PERIODO', 'AMIE', 'INSTITUCION', 'IDENTIFICACIÓN', 'NOMBRES', 'CONFIRMACIÓN', 'OBSERVACIÓN', 'ACTOR']);
        });
    })->export('xlsx');    
    }

    public function exportar_info_aplicacion_online($id){      
        Excel::create('DACT_APLICACION_ONLINE', function($excel) use ($id) {
        $excel->sheet('DATOS', function($sheet) use ($id) {        
        $sheet->cells('A1:BQ1', function($cells){ 
            $cells->setBackground('#0489B1');
            $cells->setFontColor('#FFFFFF');
            $cells->setAlignment('center');
            $cells->setValignment('center');
            $cells->setFontweight('bold');
        });
        $reporteexcel = ViewTabAplicacion::select('monitor','name_monitor','cgi_periodo_id','amie','laboratorio','institucion','zona','provincia','canton','parroquia','distrito','direccion','fecha_programada_inicio','sesion',
        'correoinstitucion','telefono1','telefono2','celular','celular1_wa',
        'comunicacion','telefono_contactado','estadollamada_apli','novedadllamada_apli','recibiocomunicado','observacion','programados','asistencia','ausentes',
        'corresponsable','corresponsable_cedula','corresponsable_celular','corresponsable_celular_wa','corresponsable_telefono','corresponsable_correo','corresponsable_correo2','corresponsable_comunicacion','estadollamada_corresponsable','novedadllamada_corresponsable','asistencia_corresponsable','corresponsable_motivo',
        'apli_nombre1','apli_cedula','apli_telef1','apli_telef1_wa','apli_telef2','apli_telef3','apli_telef_contactado','apli_email','apli_email2','apli_comunicacion','estadollamada_aplicadores','novedadllamada_aplicadores','asistencia_ineval','apli_recepcion','apli_motivo',
        'recepcion_aplicativo','descarga_aplicativo','observacion_descarga','instalo_aplicativo','observacion_instala','maquinas_instaladas','maquinas_faltantes','estado_sesion','observacion_sesion','internet','impresora','listado_asistencia','observacion_listado_asistencia','listado_validacion')
        ->where('cgi_periodo_id',$id)->where('estado_sesion2','online')->get();
        $sheet->fromArray($reporteexcel);    
        $sheet->row(1, ['MONITOR', 'NOMBRE MONITOR', 'PERIODO', 'AMIE', 'LABORATORIO', 'INSTITUCION', 'ZONA', 'PROVINCIA', 'CANTON', 'PARROQUIA', 'DISTRITO', 'DIRECCIÓN', 'FECHA_PROCESO', 'SESION', 
        /*'NOMBRE RECTOR', 'CÉDULA RECTOR',*/ 'CORREO INSTITUCIÓN', /*'CORREO RECTOR',*/ 'TELEFONO1', 'TELEFONO2', 'CELULAR 1', 'WHATSAPP CELULAR 1', /*'CELULAR 2 RECTOR', 'WHATSAPP CELULAR 2 RECTOR','TELEFONO3 RECTOR',*/ 
        'COMUNICACIÓN', 'TELEFONO CONTACTADO', 'ESTADO DE LLAMADA', 'PROBLEMA DE LLAMADA','RECIBIÓ COMUNICADO','OBSERVACIÓN','SUSTENTANTES PROGRAMADOS','SUSTENTANTES CONFIRMADOS', 'SUSTENTANTES AUSENTES',
        'NOMBRE CORRESPONSABLE', 'CÉDULA CORRESPONSABLE', 'CELULAR CORRESPONSABLE','WHATSAPP CELULAR CORRESPONSABLE', 'TELEFONO CORRESPONSABLE', 'CORREO 1 CORRESPONSABLE', 'CORREO 2 CORRESPONSABLE', 'COMUNICACIÓN CORRESPONSABLE', 'ESTADO DE LLAMADA CORRESPONSABLE', 'PROBLEMA DE LLAMADA CORRESPONSABLE', 'ASISTENCIA DEL CORRESPONSABLE', /*'RECIBIÓ CLAVE CORRESPONSABLE',*/'OBSERVACIÓN CORRESPONSABLE',
        /*'NOMBRE SUPERVISOR', 'CÉDULA SUPERVISOR', 'CELULAR SUPERVISOR','WHATSAPP CELULAR SUPERVISOR', 'TELEFONO SUPERVISOR', 'CORREO 1 SUPERVISOR', 'CORREO 2 SUPERVISOR', 'COMUNICACIÓN SUPERVISOR', 'ESTADO DE LLAMADA SUPERVISOR', 'PROBLEMA DE LLAMADA SUPERVISOR', 'ASISTENCIA DEL SUPERVISOR', 'OBSERVACIÓN SUPERVISOR',*/
        'NOMBRE GUÍA EVALUADOR', 'CÉDULA GUÍA EVALUADOR', 'CELULAR 1 GUÍA EVALUADOR','WHATSAPP CELULAR 1 GUÍA EVALUADOR', 'CELULAR 2 GUÍA EVALUADOR', 'TELEFONO 1 GUÍA EVALUADOR', 'TELEFONO CONTACTADO GUÍA EVALUADOR', 'CORREO 1 GUÍA EVALUADOR', 'CORREO 2 GUÍA EVALUADOR', 'COMUNICACIÓN GUÍA EVALUADOR', 'ESTADO DE LLAMADA GUÍA EVALUADOR', 'PROBLEMA DE LLAMADA GUÍA EVALUADOR', 'ASISTENCIA DEL GUÍA EVALUADOR', 'RECIBIÓ COMUNICADO GUÍA EVALUADOR','OBSERVACIÓN GUÍA EVALUADOR',
        /*'NOMBRE GUÍA EVALUADOR 1', 'CÉDULA GUÍA EVALUADOR 1', 'CELULAR 1 GUÍA EVALUADOR 1','WHATSAPP CELULAR 1 GUÍA EVALUADOR 1', 'CELULAR 2 GUÍA EVALUADOR 1', 'TELEFONO 1 GUÍA EVALUADOR 1', 'TELEFONO CONTACTADO GUÍA EVALUADOR 1', 'CORREO 1 GUÍA EVALUADOR 1', 'CORREO 2 GUÍA EVALUADOR 1', 'COMUNICACIÓN GUÍA EVALUADOR 1', 'ESTADO DE LLAMADA GUÍA EVALUADOR 1', 'PROBLEMA DE LLAMADA GUÍA EVALUADOR 1', 'ASISTENCIA DEL GUÍA EVALUADOR 1', 'RECIBIÓ COMUNICADO GUÍA EVALUADOR 1','OBSERVACIÓN GUÍA EVALUADOR 1',*/
        /*'NOMBRE TÉCNICO ', 'CÉDULA TÉCNICO ', 'CELULAR 1 TÉCNICO ','WHATSAPP CELULAR 1 TÉCNICO ', 'CELULAR 2 TÉCNICO ', 'TELEFONO 1 TÉCNICO ', 'TELEFONO CONTACTADO TÉCNICO ', 'CORREO 1 TÉCNICO ', 'CORREO 2 TÉCNICO ', 'COMUNICACIÓN TÉCNICO ', 'ESTADO DE LLAMADA TÉCNICO ', 'PROBLEMA DE LLAMADA TÉCNICO', 'ASISTENCIA DEL TÉCNICO', 'RECIBIÓ COMUNICADO TÉCNICO','OBSERVACIÓN DEL TÉCNICO',*/
        /*'NOMBRE GUÍA EVALUADOR 2 ', 'CÉDULA GUÍA EVALUADOR 2 ', 'CELULAR 1 GUÍA EVALUADOR 2 ','WHATSAPP CELULAR 1 GUÍA EVALUADOR2 ', 'CELULAR 2 GUÍA EVALUADOR 2 ', 'TELEFONO 1 GUÍA EVALUADOR 2 ', 'TELEFONO CONTACTADO GUÍA EVALUADOR 2 ', 'CORREO 1 GUÍA EVALUADOR 2 ', 'CORREO 2 GUÍA EVALUADOR 2 ', 'COMUNICACIÓN GUÍA EVALUADOR 2 ', 'ESTADO DE LLAMADA GUÍA EVALUADOR 2 ', 
        'PROBLEMA DE LLAMADA GUÍA EVALUADOR 2 ', 'ASISTENCIA DEL GUÍA EVALUADOR 2 ', 'RECIBIÓ COMUNICADO GUÍA EVALUADOR 2 ','OBSERVACIÓN GUÍA EVALUADOR 2', */ 
        'RECIBIÓ EXTENSIÓN INEVAL', 'DESCARGÓ EXTENSIÓN INEVAL', 'OBSERVACIÓN DESCARGA', 'INSTALÓ EXTENSIÓN INEVAL', 'OBSERVACIÓN INSTALACIÓN', 'MAQUINAS INSTALADAS', 'MAQUINAS FALTANTES','ESTADO SESIÓN', 'OBSERVACIÓN SESIÓN', 'INTERNET', 'GOOGLE CHROME', 'LISTADO','INCIDENCIA LISTADO','CUADRE LISTADO']);
        });
    })->export('xlsx');    
    }
    
}