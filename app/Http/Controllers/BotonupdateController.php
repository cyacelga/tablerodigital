<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class BotonupdateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $periodo = \DB::select("SELECT cgi_periodo_id, name_periodo FROM umonitoreo WHERE estado=1 GROUP BY cgi_periodo_id, name_periodo ORDER BY cgi_periodo_id DESC");
       $periodos = \DB::select("SELECT periodo_monitoreo FROM umonitoreo WHERE estado=1 GROUP BY periodo_monitoreo ORDER BY periodo_monitoreo DESC");
       
       return view("boton")->with('periodo',$periodo)->with('periodos',$periodos);
    }
    
    /**
     * Cambiar Manualmente where cgi_periodo_id IN ('166','167')) p (Linea 51)
     */
    
    public function boton_update(Request $request, $id_periodo){
        
        set_time_limit(900);
        
        /* $periodos= $request->periodos;
        
         foreach ($periodos as $periodo){
            $val_p = "'".$periodo."'";
            $periodo_aux[] = $val_p;
        }
        $periodo_aplicacion = implode(',', $periodo_aux); // separar los valores con coma(,).
        $sql_periodos = "(" .$periodo_aplicacion. ")";
        */
        $actualizar= \DB::select("update umonitoreo set asistencia=p.asistencia, cargas=p.cargas, reprogramacion=p.reprogramacion
        from (select * from dact_view_monitoreo1 where cgi_periodo_id IN (240,241,256,257)) p
        where p.id2=umonitoreo.id");
        ///214,217,218  ///214,217,218,219,220,221,222,223,224,225,226,227,228,229
        
        $reporteCargas = \DB::select("SELECT zona, count(cgi_laboratorio_id) as laboratorio, SUM(programados) as programados, sum(asistencia) as asistencia, sum(cargas) as cargas 
        FROM umonitoreo WHERE cgi_periodo_id =$id_periodo AND created_at is null group by zona order by zona");
        
        $TotalProg = 0.00;
        $TotalAsis = 0.00;
        $TotalAusent = 0.00;
        $TotalCargas = 0.00;
        
            foreach ($reporteCargas as $v) {
                $TotalProg += $v->programados;
                $TotalAsis += $v->asistencia;
                $TotalAusent += ($v->programados - $v->asistencia);
                $TotalCargas += $v->cargas;
            }
        
            if($TotalProg == 0){
                $porcencarga = (($TotalCargas) / 1 );
            }else{
            $porcencarga = (($TotalCargas) / ($TotalProg));
            }
            $porcentajecar = number_format(($porcencarga * 100), 2, ',', ' ');
            
        return view("boton_update")->with('periodo',$id_periodo)->with('reportecarga', $reporteCargas)->with('totalpro', $TotalProg)->with('totalasis', $TotalAsis)->with('TotalAusent',$TotalAusent)->with('totalcargas', $TotalCargas)->with('porcentajecar', $porcentajecar);
    }

}
