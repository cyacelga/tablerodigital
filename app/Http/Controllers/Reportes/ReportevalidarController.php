<?php

namespace App\Http\Controllers\Reportes;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ReportevalidarController extends Controller {
    
    public function aplicacion()
    {
        $periodo = \DB::select("SELECT a.id, a.periodo FROM cgi_periodos a INNER JOIN umonitoreo b ON a.id = b.cgi_periodo_id WHERE a.estado='A' AND b.estado=1 GROUP BY a.id, a.periodo ORDER BY a.id DESC");

        return view('reporte')->with('periodos', $periodo);
    }

    public function reporte_validar() {
        // REVISAR CON LEO $periodo = \DB::select("SELECT a.id, a.periodo FROM cgi_periodos a INNER JOIN laboratories b ON a.id = b.proceso WHERE a.estado='A' AND b.estado=1 GROUP BY a.id, a.periodo ORDER BY id DESC");
        // $periodo = \DB::select("SELECT a.id, a.periodo FROM cgi_periodos a INNER JOIN sig_institucionv b ON a.id = b.proceso WHERE b.estado=1 GROUP BY a.id, a.periodo ORDER BY id DESC");
		$periodo = \DB::select("SELECT cgi_periodo_id, name_periodo FROM umonitoreo WHERE estado=1 GROUP BY cgi_periodo_id, name_periodo ORDER BY cgi_periodo_id DESC");

        return view('laboratorios.reporteLaboratorio')->with('periodos', $periodo);
    }

    public function fecha_programada(Request $request, $id) {

        if ($request->ajax()) {
            $fechap = \DB::select("SELECT fecha_programada_inicio FROM umonitoreo WHERE cgi_periodo_id=$id AND sesion !=' ' AND estado='1' GROUP BY fecha_programada_inicio ORDER BY fecha_programada_inicio");
            return response()->json($fechap);
        }
    }

    public function coordPeriodo(Request $request, $id) {

        if ($request->ajax()) {
            $coord = \DB::select("SELECT coordinador FROM umonitoreo where cgi_periodo_id =$id  AND estado='1' group by coordinador order by coordinador");
            return response()->json($coord);
        }
    }

    public function coordPeriodo_rect(Request $request, $id) {

        if ($request->ajax()) {
            $coord = \DB::select("SELECT coordinador FROM sig_institucionv where proceso =$id group by coordinador order by coordinador");
            return response()->json($coord);
        }
    }

    public function sesion(Request $request, $fecha) {

        if ($request->ajax()) {
            $sesion = \DB::select("SELECT sesion FROM umonitoreo WHERE fecha_programada_inicio='$fecha' AND sesion !=' ' AND estado='1' GROUP BY sesion ORDER BY sesion");
            return response()->json($sesion);
        }
    }

    public function coordfecha(Request $request, $fecha) {

        if ($request->ajax()) {
            $coord = \DB::select("SELECT coordinador FROM umonitoreo where fecha_programada_inicio ='$fecha' AND estado='1' group by coordinador order by coordinador");
            return response()->json($coord);
        }
    }

    Public function reporte_general($tp_reporte, $id_periodo, $coord, $fch = "", $sesion = "") {

        if ($coord == '0') { $coordinador = ''; }else{ $coordinador = "AND coordinador='$coord'"; }
        if ($fch == 0) { $fecha = ''; }else{$fecha = "AND fecha_programada_inicio='$fch'"; }
        if ($sesion == "") { $sesion = ''; }else{ $sesion = "AND sesion='$sesion'"; }

        /// REPORTE GENERAL
        if ($tp_reporte == "reporte_general") {

            $reportedescarga = \DB::select("SELECT (case when descarga_aplicativo is null then 'NO' when descarga_aplicativo ='' then 'NO' else descarga_aplicativo end) as descarga, COUNT(*) as cantidad
            FROM umonitoreo WHERE cgi_periodo_id = $id_periodo $coordinador and estado=1 $fecha $sesion GROUP BY (case when descarga_aplicativo is null then 'NO' when descarga_aplicativo ='' then 'NO' else descarga_aplicativo end)");

            $reporteinstaloapli = \DB::select("SELECT (case when instalo_aplicativo is null then 'NO' when instalo_aplicativo ='' then 'NO' else instalo_aplicativo end) as instalo, COUNT(*) as cantidad
            FROM umonitoreo WHERE cgi_periodo_id = $id_periodo $coordinador and estado=1 $fecha $sesion  GROUP BY (case when instalo_aplicativo is null then 'NO' when instalo_aplicativo ='' then 'NO' else instalo_aplicativo end)");

            $reporteinisesion = \DB::select("SELECT (case when estado_sesion is null then 'NO' when estado_sesion ='' then 'NO' else estado_sesion end) as sesion, COUNT(*) as cantidad
            FROM umonitoreo WHERE cgi_periodo_id = $id_periodo $coordinador and estado=1 $fecha $sesion GROUP BY (case when estado_sesion is null then 'NO' when estado_sesion ='' then 'NO' else estado_sesion end)");

            $TotalDesc = 0.00;
            foreach ($reportedescarga as $generaldesc) {
                $TotalDesc += $generaldesc->cantidad;
            }

            $TotalInst = 0.00;
            foreach ($reporteinstaloapli as $generalInstal) {
                $TotalInst += $generalInstal->cantidad;
            }

            $TotalSesion = 0.00;
            foreach ($reporteinisesion as $generalSesion) {
                $TotalSesion += $generalSesion->cantidad;
            }
            return view('reportGeneral_aplicativo')->with('reportdescgeneral', $reportedescarga)->with('totaldesc', $TotalDesc)->with('reporteinstaloapli', $reporteinstaloapli)->with('TotalInst', $TotalInst)->with('reporteinisesion', $reporteinisesion)->with('TotalSesion', $TotalSesion);

            /// VALIDACION DE LABORATORIO    
        } elseif ($tp_reporte == "validacion_lab") {

            $reporteedoLab = \DB::select("select a.dictamen2, count(*) as cantidadedo 
                   from (select (case when maxasignada<=computadorc and impresorac='SI'  then 'OPTIMO'
                   when maxasignada<=computadorc then 'RECOMENDABLE' 
                   when computadorc=0 and impresorac ='' and internetc ='' then 'POR CONTACTAR'
                   when computadorc is not null or impresorac is not null or internetc is not null then 'POR GESTIONAR'
                   ---when computadorc=0 and impresorac is null and internetc is null then 'POR CONTACTAR'
                   when computadorc is null and impresorac is null and internetc is null then 'POR CONTACTAR'
                   ELSE 'POR CONTACTAR'
                   end) as dictamen2, * from laboratories where proceso = $id_periodo AND estado=1) as a group by a.dictamen2"); // quite el  AND estado=1

            $totalEdoLab = 0.00;

            foreach ($reporteedoLab as $v) {
                $totalEdoLab += $v->cantidadedo;
            }

            $reportObsLab = \DB::select("SELECT observacion, COUNT(observacion) as cantobservacion  FROM laboratories WHERE proceso =$id_periodo AND observacion!='' GROUP BY observacion"); // quite el  AND estado=1

            $totalObsLab = 0.00;
            foreach ($reportObsLab as $obs) {
                $totalObsLab += $obs->cantobservacion;
            }

            $reportevalida = \DB::select("select a.zona, COUNT(*) as laboratorio, count(case when a.dictamen2 = 'OPTIMO' then 1 end) as optimo, count(case when a.dictamen2 = 'POR GESTIONAR' then 1 end) as porgestionar, count(case when a.dictamen2 = 'RECOMENDABLE' then 1 end) as recomendable, count(case when a.dictamen2 = 'POR CONTACTAR' then 1 end) as norecomendable
                   from (select (case when maxasignada<=computadorc and impresorac='SI'  then 'OPTIMO'
                   when maxasignada<=computadorc then 'RECOMENDABLE' 
                   when computadorc=0 and impresorac ='' and internetc ='' then 'POR CONTACTAR'
                   when computadorc is not null or impresorac is not null or internetc is not null then 'POR GESTIONAR'
                   ---when computadorc=0 and impresorac is null and internetc is null then 'POR CONTACTAR'
                   when computadorc is null and impresorac is null and internetc is null then 'POR CONTACTAR'
                   ELSE 'POR CONTACTAR'
                   end) as dictamen2, * from laboratories where proceso = $id_periodo AND estado=1) as a 
                   group by a.zona order by a.zona "); // quite el  AND estado=1

            $TotalLab = 0.00;
            $totalOptimo = 0.00;
            $totalgestionar = 0.00;
            $totalRecomen = 0.00;
            $totalNoReco = 0.00;

            if ($reportevalida != NULL) {
                foreach ($reportevalida as $v) {
                    $TotalLab += $v->laboratorio;
                    $totalOptimo += $v->optimo;
                    $totalgestionar += $v->porgestionar;
                    $totalRecomen += $v->recomendable;
                    $totalNoReco += $v->norecomendable;
                }
            }
            if ($reportevalida != NULL) {
                if ($totalOptimo == 0) {
                    $porcenoptimo = ((($totalOptimo) - ($TotalLab)) / 1 );
                } else {
                    $porcenoptimo = (($totalOptimo) / ($TotalLab));
                }
                $porcentaje = number_format(($porcenoptimo * 100), 2, ',', ' ');
            } else {
                $porcentaje = 0.00;
            }

            return view('laboratorios.valLab_zona')->with('valida_lab', $reportevalida)->with('totalLab', $TotalLab)->with('totalOptimo', $totalOptimo)->with('totalgestionar', $totalgestionar)->with('totalRecomen', $totalRecomen)->with('totalNoReco', $totalNoReco)->with('porcentaje', $porcentaje)->with('idperiodo', $id_periodo)->with('estadoLaboratorio', $reporteedoLab)->with('totalEdoLab', $totalEdoLab)->with('observacionLab', $reportObsLab)->with('totalObsLab', $totalObsLab);

            /// INSTALACION DE APLICATIVO
        } elseif ($tp_reporte == "llamadas_rectores") {
            
            $reportedescarga= \DB::select("SELECT (case when descarga is null then 'NO' when descarga ='' then 'NO' else descarga end) as descarga, COUNT(*) as cantidad
        FROM sig_institucionv WHERE proceso = $id_periodo $coordinador $fecha $sesion and estado=1 GROUP BY 1");
            
            $TotalDesc = 0.00;
        foreach ($reportedescarga as $generaldesc){
        $TotalDesc += $generaldesc->cantidad;
        }
        
        $reporteinstaloapli= \DB::select("SELECT (case when instalacion is null then 'NO' when instalacion ='' then 'NO' else instalacion end) as instalo, COUNT(*) as cantidad
        FROM sig_institucionv WHERE proceso = $id_periodo $coordinador $fecha $sesion AND estado=1 GROUP BY 1");
        
            $TotalInst = 0.00;
        foreach ($reporteinstaloapli as $generalInstal){
        $TotalInst += $generalInstal->cantidad;
        }
        
        $recepcioncorreo= \DB::select("SELECT (case when recibiocomunicado is null then 'NO' when recibiocomunicado ='' then 'NO' else recibiocomunicado end) as comunicado, COUNT(*) as cantidad
        FROM sig_institucionv WHERE proceso = $id_periodo $coordinador $fecha $sesion AND estado=1 GROUP BY 1");
        
            $TotalRecepcion = 0.00;
        foreach ($recepcioncorreo as $correo){
        $TotalRecepcion += $correo->cantidad;
        }
            
            
            $capacitacion=\DB::select("SELECT (CASE WHEN capacitacion is null THEN 'NO' WHEN capacitacion ='' THEN 'NO' ELSE capacitacion END) AS capacitacion, COUNT(*) AS totalcapacitadas
FROM sig_institucionv WHERE proceso=$id_periodo and estado=1 $fecha $sesion $coordinador GROUP BY (CASE WHEN capacitacion is null THEN 'NO' WHEN capacitacion ='' THEN 'NO' ELSE capacitacion END)");

            $TotalSedes= 0.00;
            
            foreach($capacitacion as $cantSedes){
                $TotalSedes += $cantSedes->totalcapacitadas;
            }
            
            $n_Sustentantes = \DB::select("SELECT SUM(a.n_sustentantes) AS n_sustentantes 
FROM (SELECT (CASE WHEN capacitacion is null THEN 'NO' WHEN capacitacion ='' THEN 'NO' ELSE capacitacion END) AS capacitacion, n_sustentantes, proceso, estado FROM sig_institucionv) as a
WHERE a.proceso=$id_periodo $fecha $sesion $coordinador and a.estado=1 AND a.capacitacion='NO'");
            
            $Tt_Sustentantes = \DB::select("SELECT SUM(n_sustentantes) AS t_sustentantes FROM sig_institucionv WHERE proceso=$id_periodo $fecha $sesion $coordinador and estado=1");
            
            $Total_Sustentantes= 0.00;
            
            foreach($Tt_Sustentantes as $Tt_Sustentantes){
                $Total_Sustentantes += $Tt_Sustentantes->t_sustentantes;
            }
            
            $reportellamada = \DB::select("select (case when estadollamada is null then 'POR CONTACTAR' when estadollamada ='' then 'POR CONTACTAR' else estadollamada END)as estadollamada, count(*) as totalestado
from sig_institucionv where proceso=$id_periodo AND estado=1 $fecha $sesion $coordinador group by (case when estadollamada is null then 'POR CONTACTAR' when estadollamada ='' then 'POR CONTACTAR' else estadollamada END)");

            $reportellamadarec = \DB::select("SELECT zona, count(amie) as programados_n_amie, count(case when estadollamada ='CONTACTADO' then 'CONTACTADO' end) as contactado,
              count(case when estadollamada ='NO CONTACTADO' then 'NO CONTACTADO' end) as nocontactado,
              count(case when estadollamada ='' then 'EN PROCESO' when estadollamada is null then 'EN PROCESO' end) as enproceso,
              count(case when novedadllamada !='' and estadollamada ='NO CONTACTADO' then novedadllamada end) as novedadllamada 
              FROM sig_institucionv where proceso=$id_periodo AND estado=1 $fecha $sesion $coordinador
              group by zona order by zona");

            $novedadllamada = \DB::select("SELECT (case when novedadllamada='' then 'NO CONTESTA' else novedadllamada end) as novedadllamada, count(*) AS totalnovedad FROM sig_institucionv where proceso = $id_periodo AND estado=1 $coordinador and estadollamada='NO CONTACTADO' group by (case when novedadllamada='' then 'NO CONTESTA' else novedadllamada end) order by totalnovedad desc");

            $TotalLlamada = 0.00;

            foreach ($reportellamada as $v) {
                $TotalLlamada += $v->totalestado;
            }

            $TotalNoContactado = 0.00;

            foreach ($novedadllamada as $v) {
                $TotalNoContactado += $v->totalnovedad;
            }

            $TotalProgramados = 0.00;
            $TotalContactado = 0.00;
            $TotalNoContactadoz = 0.00;
            $TotalEnProceso = 0.00;
            $TotalNovedad = 0.00;


            foreach ($reportellamadarec as $v) {
                $TotalProgramados += $v->programados_n_amie;
                $TotalContactado += $v->contactado;
                $TotalNoContactadoz += $v->nocontactado;
                $TotalEnProceso += $v->enproceso;
                $TotalNovedad += $v->novedadllamada;
            }

            return view('rectores_validacion.llamadas_rectores')->with('reportdescgeneral',$reportedescarga)->with('totaldesc',$TotalDesc)->with('reporteinstaloapli',$reporteinstaloapli)->with('TotalInst',$TotalInst)->with('recepcioncorreo',$recepcioncorreo)->with('TotalRecepcion',$TotalRecepcion)->with('estadollamada', $reportellamada)->with('totalLlamada', $TotalLlamada)->with('capacitacion', $capacitacion)->with('TotalSedes', $TotalSedes)->with('n_Sustentantes', $n_Sustentantes)->with('Total_Sustentantes', $Total_Sustentantes)->with('novedad', $novedadllamada)->with('ttnovedad', $TotalNoContactado)->with('idperiodo', $id_periodo)->with('reportellamadarec', $reportellamadarec)->with('totalLlamadarec', $TotalProgramados)->with('TotalContactado', $TotalContactado)->with('TotalNoContactado', $TotalNoContactado)->with('TotalEnProceso', $TotalEnProceso)->with('TotalNovedad', $TotalNovedad)->with('coordinador', $coordinador);

            /// LLAMADAS APLICADORES
        } elseif ($tp_reporte == "llamadas_aplicadores") {

            $reporteLlamada = \DB::select("SELECT (case when ap.estadollamada is null then 'EN PROCESO' when ap.estadollamada ='' then 'EN PROCESO' else ap.estadollamada END)as estadollamada, count(*) as totalestado
       from sig_institucionv i, sig_aplicadores ap where ap.amie=i.amie and i.proceso=ap.cgi_periodo_id and i.proceso=$id_periodo $fecha $sesion $coordinador
       GROUP BY (case when ap.estadollamada is null then 'EN PROCESO' when ap.estadollamada ='' then 'EN PROCESO' else ap.estadollamada END)");

            $reporteAplicador = \DB::select("select i.zona, count(i.amie) as amie,
       count(case when ap.estadollamada ='CONTACTADO' then 'CONTACTADO' end) as contactado,
       count(case when ap.estadollamada ='NO CONTACTADO' then 'NO CONTACTADO' end) as nocontactado,
       count(case when ap.estadollamada ='' then 'EN PROCESO' when ap.estadollamada is null then 'EN PROCESO' end) as enproceso,
       count(case when ap.novedadllamada !='' AND ap.estadollamada='NO CONTACTADO' then ap.novedadllamada end) as novedadllamada
       from sig_institucionv i, sig_aplicadores ap 
       where ap.amie=i.amie and i.proceso=ap.cgi_periodo_id and i.proceso=$id_periodo $fecha $sesion $coordinador
       group by i.zona order by i.zona");

            $novedadllamada = \DB::select("SELECT (case when novedadllamada='' then 'NO CONTESTA' else novedadllamada end) as novedadllamada, count(*) AS totalnovedad FROM sig_institucionv where proceso = $id_periodo $coordinador  and estadollamada='NO CONTACTADO' group by (case when novedadllamada='' then 'NO CONTESTA' else novedadllamada end)");

            $TotalLlamada = 0.00;

            foreach ($reporteLlamada as $v) {
                $TotalLlamada += $v->totalestado;
            }

            $TotalNoContactadon = 0.00;

            foreach ($novedadllamada as $v) {
                $TotalNoContactadon += $v->totalnovedad;
            }

            $TotalProgAmie = 0.00;
            $TotalContactado = 0.00;
            $TotalNoContactado = 0.00;
            $TotalEnProceso = 0.00;
            $TotalNovedad = 0.00;

            foreach ($reporteAplicador as $v) {
                $TotalProgAmie += $v->amie;
                $TotalContactado += $v->contactado;
                $TotalNoContactado += $v->nocontactado;
                $TotalEnProceso += $v->enproceso;
                $TotalNovedad += $v->novedadllamada;
            }
            return view('aplicadores_validacion.llamada_aplicadores_valzona')->with('idperiodo', $id_periodo)->with('estadollamada', $reporteLlamada)->with('totalLlamada', $TotalLlamada)->with('novedad', $novedadllamada)->with('ttnovedad', $TotalNoContactado)->with('coordinador', $coordinador)->with('reporteAplicador', $reporteAplicador)->with('TotalProgAmie', $TotalProgAmie)->with('TotalContactado', $TotalContactado)->with('TotalNoContactado', $TotalNoContactado)->with('TotalEnProceso', $TotalEnProceso)->with('TotalNovedad', $TotalNovedad);
        }
    }

    public function provincia(Request $request, $tp_reporte, $id_periodo, $fch = "", $sesion = "") {

        if ($fch == 0) {$fecha = '';}else{ $fecha = "AND fecha_programada_inicio='$fch'"; }
        if ($sesion == "") {$sesion = '';}else{$sesion = "AND sesion='$sesion'";}

        $zona = $request->zona;

        foreach ($zona as $zona) {
            $val = "'" . $zona . "'"; // agregar ("") a los calores para cuando sea de tipo caracter.
            $zona_aux[] = $val;
        }
        $zonas = implode(',', $zona_aux); // separar los valores con coma(,).
        $sql_zona = "(" . $zonas . ")";
        
        if ($tp_reporte == "llamadas_rectores") {
        $reportellamadarec = \DB::select("SELECT zona, count(amie) as programados_n_amie, count(case when estadollamada ='CONTACTADO' then 'CONTACTADO' end) as contactado,
              count(case when estadollamada ='NO CONTACTADO' then 'NO CONTACTADO' end) as nocontactado,
              count(case when estadollamada ='' then 'EN PROCESO' when estadollamada is null then 'EN PROCESO' end) as enproceso,
              count(case when novedadllamada !='' and estadollamada ='NO CONTACTADO' then novedadllamada end) as novedadllamada 
              FROM sig_institucionv where proceso=$id_periodo AND estado=1 AND zona IN $sql_zona
              group by zona order by zona");

            $TotalProgramados = 0.00;
            $TotalContactado = 0.00;
            $TotalNoContactadoz = 0.00;
            $TotalEnProceso = 0.00;
            $TotalNovedad = 0.00;


            foreach ($reportellamadarec as $v) {
                $TotalProgramados += $v->programados_n_amie;
                $TotalContactado += $v->contactado;
                $TotalNoContactadoz += $v->nocontactado;
                $TotalEnProceso += $v->enproceso;
                $TotalNovedad += $v->novedadllamada;
            }

            return view('rectores_validacion.llamadasRectores_provincia')->with('idperiodo', $id_periodo)->with('reportellamadarec', $reportellamadarec)->with('totalLlamadarec', $TotalProgramados)->with('TotalContactado', $TotalContactado)->with('TotalEnProceso', $TotalEnProceso)->with('TotalNovedad', $TotalNovedad);

            /// LLAMADAS APLICADORES
            } elseif ($tp_reporte == "validacion_lab") {

            $reportevalida = \DB::select("select a.provincia, a.zona, COUNT(zona) as laboratorio, count(case when a.dictamen2 = 'OPTIMO' then 1 end) as optimo, count(case when a.dictamen2 = 'POR GESTIONAR' then 1 end) as porgestionar, count(case when a.dictamen2 = 'RECOMENDABLE' then 1 end) as recomendable, count(case when a.dictamen2 = 'POR CONTACTAR' then 1 end) as norecomendable
                   from (select (case when maxasignada<=computadorc and impresorac='SI'  then 'OPTIMO'
                   when maxasignada<=computadorc then 'RECOMENDABLE' 
                   when computadorc=0 and impresorac ='' and internetc ='' then 'POR CONTACTAR'
                   when computadorc is not null or impresorac is not null or internetc is not null then 'POR GESTIONAR'
                   ---when computadorc=0 and impresorac is null and internetc is null then 'POR CONTACTAR'
                   when computadorc is null and impresorac is null and internetc is null then 'POR CONTACTAR'
                   ELSE 'POR CONTACTAR'
                   end) as dictamen2, * from laboratories where proceso = $id_periodo AND estado=1 AND zona IN $sql_zona AND created_at is null ) as a group by a.provincia, a.zona order by 1");

            $TotalLab = 0.00;
            $totalOptimo = 0.00;
            $totalgestionar = 0.00;
            $totalRecomen = 0.00;
            $totalNoReco = 0.00;

            if ($reportevalida != NULL) {
                foreach ($reportevalida as $v) {
                    $TotalLab += $v->laboratorio;
                    $totalOptimo += $v->optimo;
                    $totalgestionar += $v->porgestionar;
                    $totalRecomen += $v->recomendable;
                    $totalNoReco += $v->norecomendable;
                }
            }
            if ($reportevalida != NULL) {
                if ($totalOptimo == 0) {
                    $porcenoptimo = ((($totalOptimo) - ($TotalLab)) / 1 );
                } else {
                    $porcenoptimo = (($totalOptimo) / ($TotalLab));
                }
                $porcentaje = number_format(($porcenoptimo * 100), 2, ',', ' ');
            } else {
                $porcentaje = 0.00;
            }

            return view('laboratorios.valLab_provincia')->with('valida_lab', $reportevalida)->with('idperiodo', $id_periodo)->with('totalLab', $TotalLab)->with('totalOptimo', $totalOptimo)->with('totalgestionar', $totalgestionar)->with('totalRecomen', $totalRecomen)->with('totalNoReco', $totalNoReco)->with('porcentaje', $porcentaje);
        }
    }

    public function distrito(Request $request, $tp_reporte, $id_periodo, $fch = "", $sesion = "") {

        if ($fch == 0) {
            $fecha = '';
        } else {
            $fecha = "AND fecha_programada_inicio='$fch'";
        }
        if ($sesion == "") {
            $sesion = '';
        } else {
            $sesion = "AND sesion='$sesion'";
        }
        /// Validación de Laboratotio
        if ($tp_reporte == "validacion_lab") {

            $provincia = $request->provincia;

            foreach ($provincia as $provincia) {
                $val = "'" . $provincia . "'"; // agregar ("") a los calores para cuando sea de tipo caracter.
                $provincia_aux[] = $val;
            }
            $provincias = implode(',', $provincia_aux); // separar los valores con coma(,).
            $sql_provincia = "(" . $provincias . ")";

            $reportevalida = \DB::select("select a.distrito, a.provincia, a.zona, COUNT(zona) as laboratorio, count(case when a.dictamen2 = 'OPTIMO' then 1 end) as optimo, count(case when a.dictamen2 = 'POR GESTIONAR' then 1 end) as porgestionar, count(case when a.dictamen2 = 'RECOMENDABLE' then 1 end) as recomendable, count(case when a.dictamen2 = 'POR CONTACTAR' then 1 end) as norecomendable
                   from (select (case when maxasignada<=computadorc and impresorac='SI'  then 'OPTIMO'
                   when maxasignada<=computadorc then 'RECOMENDABLE' 
                   when computadorc=0 and impresorac ='' and internetc ='' then 'POR CONTACTAR'
                   when computadorc is not null or impresorac is not null or internetc is not null then 'POR GESTIONAR'
                   ---when computadorc=0 and impresorac is null and internetc is null then 'POR CONTACTAR'
                   when computadorc is null and impresorac is null and internetc is null then 'POR CONTACTAR'
                    ELSE 'POR CONTACTAR'
                    end) as dictamen2, * from laboratories where proceso = $id_periodo AND estado=1 AND provincia IN $sql_provincia AND created_at is null ) as a group by a.distrito, a.provincia, a.zona order by 1");

            $TotalLab = 0.00;
            $totalOptimo = 0.00;
            $totalgestionar = 0.00;
            $totalRecomen = 0.00;
            $totalNoReco = 0.00;

            if ($reportevalida != NULL) {
                foreach ($reportevalida as $v) {
                    $TotalLab += $v->laboratorio;
                    $totalOptimo += $v->optimo;
                    $totalgestionar += $v->porgestionar;
                    $totalRecomen += $v->recomendable;
                    $totalNoReco += $v->norecomendable;
                }
            }
            if ($reportevalida != NULL) {
                if ($totalOptimo == 0) {
                    $porcenoptimo = ((($totalOptimo) - ($TotalLab)) / 1 );
                } else {
                    $porcenoptimo = (($totalOptimo) / ($TotalLab));
                }
                $porcentaje = number_format(($porcenoptimo * 100), 2, ',', ' ');
            } else {
                $porcentaje = 0.00;
            }
            return view('laboratorios.valLab_distrito')->with('valida_lab', $reportevalida)->with('idperiodo', $id_periodo)->with('totalLab', $TotalLab)->with('totalOptimo', $totalOptimo)->with('totalgestionar', $totalgestionar)->with('totalRecomen', $totalRecomen)->with('totalNoReco', $totalNoReco)->with('porcentaje', $porcentaje);
    }
    }
    
     public function monitor(Request $request, $tp_reporte, $id_periodo, $coord, $fch ="", $sesion=""){
        
        if ($coord == '0') { $coordinador = ''; }else{ $coordinador = "AND coordinador='$coord'"; }
        if ($fch == 0) { $fecha = ''; }else{$fecha = "AND fecha_programada_inicio='$fch'"; }
        if ($sesion == "") { $sesion = ''; }else{ $sesion = "AND sesion='$sesion'"; }
        
        if($tp_reporte == "validacion_lab"){
            
             $distrito = $request->distrito;
        
        foreach ($distrito as $distrito){
            $val = "'".$distrito."'"; // agregar ("") a los calores para cuando sea de tipo caracter.
            $distrito_aux[] = $val;
        }
        $distritos = implode(',', $distrito_aux); // separar los valores con coma(,).
        $sql_distrito = "(" .$distritos. ")";
                                    
        $reportevalida = \DB::select("select a.monitor, a.distrito, a.zona, COUNT(zona) as laboratorio, count(case when a.dictamen2 = 'OPTIMO' then 1 end) as optimo, count(case when a.dictamen2 = 'POR GESTIONAR' then 1 end) as porgestionar, count(case when a.dictamen2 = 'RECOMENDABLE' then 1 end) as recomendable, count(case when a.dictamen2 = 'POR CONTACTAR' then 1 end) as norecomendable
                   from (select (case when maxasignada<=computadorc and impresorac='SI'  then 'OPTIMO'
                   when maxasignada<=computadorc then 'RECOMENDABLE' 
                   when computadorc=0 and impresorac ='' and internetc ='' then 'POR CONTACTAR'
                   when computadorc is not null or impresorac is not null or internetc is not null then 'POR GESTIONAR'
                   ---when computadorc=0 and impresorac is null and internetc is null then 'POR CONTACTAR'
                   when computadorc is null and impresorac is null and internetc is null then 'POR CONTACTAR'
                    ELSE 'POR CONTACTAR'
                       end) as dictamen2, * from laboratories where proceso = $id_periodo AND estado=1 AND distrito IN $sql_distrito AND created_at is null ) as a group by a.monitor, a.distrito, a.zona order by 1");    
          
        $TotalLab = 0.00;
        $totalOptimo = 0.00;
        $totalgestionar = 0.00;
        $totalRecomen = 0.00;
        $totalNoReco = 0.00;
        
        if ($reportevalida != NULL) {
            foreach ($reportevalida as $v) {
                $TotalLab += $v->laboratorio;
                $totalOptimo += $v->optimo;
                $totalgestionar += $v->porgestionar;
                $totalRecomen += $v->recomendable;
                $totalNoReco += $v->norecomendable;           
            }
        }
        if ($reportevalida != NULL) {
            if($totalOptimo == 0){
                $porcenoptimo = ((($totalOptimo) - ($TotalLab)) / 1 );
            }else{
            $porcenoptimo = (($totalOptimo) / ($TotalLab));
            }
            $porcentaje = number_format(($porcenoptimo * 100), 2, ',', ' ');
        } else {
            $porcentaje = 0.00;
        }             
        return view('laboratorios.valLab_monitor')->with('valida_lab', $reportevalida)->with('idperiodo', $id_periodo)->with('totalLab',$TotalLab)->with('totalOptimo',$totalOptimo)->with('totalgestionar',$totalgestionar)->with('totalRecomen',$totalRecomen)->with('totalNoReco',$totalNoReco)->with('porcentaje',$porcentaje);
        
        // LLAMADAS RECTORES
        }elseif($tp_reporte == "llamadas_rectores"){
            
             $zona = $request->zona;       
       
         foreach ($zona as $zona){
            $val = "'".$zona."'"; // agregar ("") a los calores para cuando sea de tipo caracter.
            $zona_aux[] = $val;
        }
        $zonas = implode(',', $zona_aux); // separar los valores con coma(,).
        $sql_zona = "(" .$zonas. ")";
        
            $reportellamada = \DB::select("select (case when estadollamada is null then 'EN PROCESO' when estadollamada ='' then 'EN PROCESO' else estadollamada END)as estadollamada, count(*) as totalestado
from sig_institucionv where proceso=$id_periodo and estado=1 AND zona IN $sql_zona $coordinador $fecha $sesion  group by (case when estadollamada is null then 'EN PROCESO' when estadollamada ='' then 'EN PROCESO' else estadollamada END)");    
            
            $reportellamadarec= \DB::select("SELECT monitor, zona, count(amie) as programados_n_amie, count(case when estadollamada ='CONTACTADO' then 'CONTACTADO' end) as contactado,
              count(case when estadollamada ='NO CONTACTADO' then 'NO CONTACTADO' end) as nocontactado,
              count(case when estadollamada ='' then 'EN PROCESO' when estadollamada is null then 'EN PROCESO' end) as enproceso,
              count(case when novedadllamada !='' and estadollamada ='NO CONTACTADO' then novedadllamada end) as novedadllamada 
              FROM sig_institucionv where proceso=$id_periodo AND estado=1 AND zona IN $sql_zona $coordinador $fecha $sesion
              group by monitor, zona order by zona");
                     
            $novedadllamada= \DB::select("SELECT (case when novedadllamada='' then 'NO CONTESTA' else novedadllamada end) as novedadllamada, count(*) AS totalnovedad FROM sig_institucionv where proceso = $id_periodo $coordinador  and estadollamada='NO CONTACTADO' group by (case when novedadllamada='' then 'NO CONTESTA' else novedadllamada end)");
                          
            $TotalLlamada = 0.00;
        
            foreach ($reportellamada as $v) {
                $TotalLlamada += $v->totalestado;           
            }           
            
            $TotalNoContactado = 0.00;
        
            foreach ($novedadllamada as $v) {
                $TotalNoContactado += $v->totalnovedad;           
            }
        
        $TotalProgramados= 0.00;
        $TotalContactado = 0.00;
        $TotalNoContactadoz = 0.00;
        $TotalEnProceso = 0.00;
        $TotalNovedad = 0.00;        
        
            foreach ($reportellamadarec as $v) {               
                $TotalProgramados += $v->programados_n_amie;
                $TotalContactado += $v->contactado;
                $TotalNoContactadoz += $v->nocontactado;
                $TotalEnProceso += $v->enproceso;
                $TotalNovedad += $v->novedadllamada;           
            }             
        return view('rectores_validacion.llamadas_rectores_monit')->with('coordinador',$coordinador)->with('estadollamada', $reportellamada)->with('totalLlamada', $TotalLlamada)->with('novedad',$novedadllamada)->with('ttnovedad',$TotalNoContactado)->with('idperiodo',$id_periodo)->with('reportellamadarec',$reportellamadarec)->with('totalLlamadarec',$TotalProgramados)->with('TotalContactado',$TotalContactado)->with('TotalNoContactado',$TotalNoContactadoz)->with('TotalEnProceso',$TotalEnProceso)->with('TotalNovedad',$TotalNovedad);
        
        /// LLAMADAS APLICADORES
        }elseif($tp_reporte == "llamadas_aplicadores"){
            
         $zona =  $distrito = $request->zona;       
       
         foreach ($zona as $zona){
            $val = "'".$zona."'"; // agregar ("") a los calores para cuando sea de tipo caracter.
            $zona_aux[] = $val;
        }
        $zonas = implode(',', $zona_aux); // separar los valores con coma(,).
        $sql_zona = "(" .$zonas. ")";
        
        $reporteLlamada= \DB::select("SELECT (case when ap.estadollamada is null then 'EN PROCESO' when ap.estadollamada ='' then 'EN PROCESO' else ap.estadollamada END)as estadollamada, count(*) as totalestado
       from sig_institucionv i, sig_aplicadores ap where ap.amie=i.amie and i.proceso=ap.cgi_periodo_id and i.proceso=$id_periodo $coordinador $fecha $sesion
       GROUP BY (case when ap.estadollamada is null then 'EN PROCESO' when ap.estadollamada ='' then 'EN PROCESO' else ap.estadollamada END)");
                
        $reporteAplicador = \DB::select("select i.monitor, i.zona, i.coordinador, count(i.amie) as amie,
       count(case when ap.estadollamada ='CONTACTADO' then 'CONTACTADO' end) as contactado,
       count(case when ap.estadollamada ='NO CONTACTADO' then 'NO CONTACTADO' end) as nocontactado,
       count(case when ap.estadollamada ='' then 'EN PROCESO' when ap.estadollamada is null then 'EN PROCESO' end) as enproceso,
       count(case when ap.novedadllamada !='' AND ap.estadollamada='NO CONTACTADO' then ap.novedadllamada end) as novedadllamada
       from sig_institucionv i, sig_aplicadores ap 
       where ap.amie=i.amie and i.proceso=ap.cgi_periodo_id and i.proceso=$id_periodo $coordinador AND zona IN $sql_zona $fecha $sesion
       group by i.monitor, i.zona, i.coordinador order by i.monitor, i.zona, i.coordinador");        
        
            $TotalLlamada = 0.00;
        
            foreach ($reporteLlamada as $v) {
                $TotalLlamada += $v->totalestado;           
            }
            
        $TotalProgAmie = 0.00;        
        $TotalContactado = 0.00;
        $TotalNoContactado = 0.00;
        $TotalEnProceso = 0.00;
        $TotalNovedad = 0.00;
        
            foreach ($reporteAplicador as $v) {
                $TotalProgAmie += $v->amie;                
                $TotalContactado += $v->contactado;
                $TotalNoContactado += $v->nocontactado;
                $TotalEnProceso += $v->enproceso;
                $TotalNovedad += $v->novedadllamada;           
            }       
        return view('aplicadores_validacion.llamada_aplicador_valmont')->with('idperiodo',$id_periodo)->with('estadollamada', $reporteLlamada)->with('totalLlamada', $TotalLlamada)->with('coordinador',$coordinador)->with('reporteAplicadormont',$reporteAplicador)->with('TotalProgAmie',$TotalProgAmie)->with('TotalContactado',$TotalContactado)->with('TotalNoContactado',$TotalNoContactado)->with('TotalEnProceso',$TotalEnProceso)->with('TotalNovedad',$TotalNovedad);
        }
    }

public function laboratorio(Request $request, $tp_reporte, $id_periodo, $coord, $fch ="", $sesion=""){
        
        if ($coord == '0') { $coordinador = ''; }else{ $coordinador = "AND coordinador='$coord'"; }
        if ($fch == 0) { $fecha = ''; }else{$fecha = "AND fecha_programada_inicio='$fch'"; }
        if ($sesion == "") { $sesion = ''; }else{ $sesion = "AND sesion='$sesion'"; }
        
         $monitor = $request->monitor;
         
          foreach ($monitor as $monitor){
            $val_m = "'".$monitor."'";
            $monitor_aux[] = $val_m;
        }
         $monitores = implode(',', $monitor_aux);
        $sql_monitor = "(" .$monitores. ")";
        
        if($tp_reporte == "llamadas_rectores"){
            
            $zona = $request->zona;       
       
         foreach ($zona as $zona){
            $val = "'".$zona."'"; // agregar ("") a los calores para cuando sea de tipo caracter.
            $zona_aux[] = $val;
        }
        $zonas = implode(',', $zona_aux); // separar los valores con coma(,).
        $sql_zona = "(" .$zonas. ")";
        
              $reporteLLamadaRec = \DB::select("SELECT monitor, zona, amie as programados_n_amie, institucion, rector,
	(case when estadollamada ='CONTACTADO' then 'CONTACTADO' when estadollamada ='NO CONTACTADO' then 'NO CONTACTADO'  else 'POR CONTACTAR' end) as estadollamada,
	(case when novedadllamada is not null then novedadllamada end) as novedadllamada
         FROM sig_institucionv where proceso=$id_periodo and estado=1 AND monitor IN $sql_monitor AND zona IN $sql_zona $coordinador
         group by amie, monitor, zona, estadollamada, novedadllamada, institucion, rector  order by amie, monitor, zona, institucion, rector");   
         
        return view('rectores_validacion.llamadas_rectLab_monitor')->with('rector_lab', $reporteLLamadaRec)->with('idperiodo',$id_periodo);
        
        /// LLAMDA APLICADORES
        }elseif($tp_reporte == "llamadas_aplicadores"){
                             
        $reportelabapli = \DB::select("SELECT i.coordinador, i.monitor, i.zona, i.amie as amie, i.institucion, i.rector,       
        (case when ap.estadollamada ='CONTACTADO' then 'CONTACTADO' when ap.estadollamada ='NO CONTACTADO' then 'NO CONTACTADO'  else 'EN PROCESO' end) as estadollamada,
	(case when ap.novedadllamada !='' then ap.novedadllamada end) as novedadllamada
        from sig_institucionv i, sig_aplicadores ap where ap.amie=i.amie and i.proceso=ap.cgi_periodo_id and i.proceso=$id_periodo $coordinador AND monitor IN $sql_monitor
        ORDER BY coordinador, monitor, amie, i.rector");    
             
        return view('rectores_aplicadores.llamada_aplicador_valLab')->with('reportelabapli',$reportelabapli);
        
        }elseif($tp_reporte == "validacion_lab"){            
           
        $distrito = $request->distrito;        
       
        foreach ($distrito as $distrito){
            $val_d = "'".$distrito."'"; // agregar ("") a los calores para cuando sea de tipo caracter.
            $distrito_aux[] = $val_d;
        }
       
        $distritos = implode(',', $distrito_aux); // separar los valores con coma(,).
        $sql_distrito = "(" .$distritos. ")";
            
        $reportevalida = \DB::select("select a.id_sede as laboratorio, a.monitor, a.distrito, a.zona, a.tipo, a.maxasignada, a.computadorc, a.impresorac, a.internetc, a.observacion, a.dictamen from (select (
                   case when maxasignada<=computadorc and impresorac='SI'  then 'OPTIMO'
                   when maxasignada<=computadorc then 'RECOMENDABLE' 
                   when computadorc=0 and impresorac ='' and internetc ='' then 'POR CONTACTAR'
                   when computadorc is not null or impresorac is not null or internetc is not null then 'POR GESTIONAR'
                   ---when computadorc=0 and impresorac is null and internetc is null then 'POR CONTACTAR'
                   when computadorc is null and impresorac is null and internetc is null then 'POR CONTACTAR'
                    ELSE 'POR CONTACTAR'
                    end) as dictamen, monitor, id_sede, distrito, zona, tipo, maxasignada, computadorc, impresorac, internetc, observacion from laboratories where proceso = $id_periodo and estado=1 AND monitor IN $sql_monitor AND distrito IN $sql_distrito AND created_at is null ) as a group by a.dictamen, a.monitor, a.id_sede, a.distrito, a.zona, a.tipo, a.maxasignada, a.computadorc, a.impresorac, a.internetc, a.observacion");    
                    
        return view('laboratorios.valLab_laboratorio')->with('valida_labo', $reportevalida)->with('idperiodo',$id_periodo);
        }
    }
    
}