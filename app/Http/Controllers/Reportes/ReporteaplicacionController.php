<?php

namespace App\Http\Controllers\Reportes;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ReporteaplicacionController extends Controller
{    
    public function borrar()
    {
        $periodo = \DB::select("SELECT cgi_periodo_id, name_periodo FROM umonitoreo WHERE estado=1 GROUP BY cgi_periodo_id, name_periodo ORDER BY cgi_periodo_id DESC");

        return view('aplicacion.reporte_zonal_borrar')->with('periodos', $periodo);
    } 
    
    public function index()
    {
        $periodo = \DB::select("SELECT cgi_periodo_id, name_periodo FROM umonitoreo WHERE estado=1 GROUP BY cgi_periodo_id, name_periodo ORDER BY cgi_periodo_id DESC");

        return view('aplicacion.reporte_zonal')->with('periodos', $periodo);
    }   
    
    Public function reporte_aplicacion_borrar($tp_reporte, $id_periodo, $coord, $fch="", $ss="") {
        
        if($coord =='0'){ $coordinador = '';}else{ $coordinador = "AND coordinador='$coord'"; }        
        if($fch == 0){$fecha = '';}else{ $fecha = "AND fecha_programada_inicio='$fch'"; }        
        if( $ss ==""){$sesion = '';}else{$sesion = "AND sesion='$ss'";}        
        
        if($tp_reporte == "inicio_aplicacion"){
                                   
        $reportedescarga= \DB::select("SELECT (case when descarga_aplicativo is null then 'NO' when descarga_aplicativo ='' then 'NO' else descarga_aplicativo end) as descarga, COUNT(*) as cantidad
        FROM umonitoreo WHERE cgi_periodo_id = $id_periodo $coordinador $fecha $sesion and estado=1  GROUP BY (case when descarga_aplicativo is null then 'NO' when descarga_aplicativo ='' then 'NO' else descarga_aplicativo end)");            
                                    
        $reporteinstaloapli= \DB::select("SELECT (case when instalo_aplicativo is null then 'NO' when instalo_aplicativo ='' then 'NO' else instalo_aplicativo end) as instalo, COUNT(*) as cantidad
        FROM umonitoreo WHERE cgi_periodo_id = $id_periodo $coordinador $fecha $sesion AND estado=1  GROUP BY (case when instalo_aplicativo is null then 'NO' when instalo_aplicativo ='' then 'NO' else instalo_aplicativo end)");            
                                    
        $reporteinisesion = \DB::select("SELECT (case when estado_sesion is null then 'NO' when estado_sesion ='' then 'NO' else estado_sesion end) as sesion, COUNT(*) as cantidad
        FROM umonitoreo WHERE cgi_periodo_id = $id_periodo $coordinador $fecha $sesion AND estado = 1 GROUP BY (case when estado_sesion is null then 'NO' when estado_sesion ='' then 'NO' else estado_sesion end)");                
            
        $noInicioSsesion= \DB::select("SELECT (case when observacion_sesion='' then 'SIN INICIAR SESION' 
                 when observacion_sesion IS NULL then 'SIN INICIAR SESION'
                 when estado_sesion='' and observacion_sesion='SIN PROBLEMA' then 'SIN INICIAR SESION'
                 when estado_sesion IS NULl and observacion_sesion='SIN PROBLEMA' then 'SIN INICIAR SESION'
                 when estado_sesion ='NO' and observacion_sesion='SIN PROBLEMA' then 'SIN PROBLEMA REGISTRADO'
                 when estado_sesion IS NULL and observacion_sesion!='SIN PROBLEMA' then 'SIN SELECCIONAR ESTADO DE SESIÓN'
                 else observacion_sesion end) as observacion_sesion, count(*) AS totalobservacion, sum(programados) as sustentantes_afect
                 FROM umonitoreo 
                 where cgi_periodo_id = $id_periodo $coordinador $fecha $sesion AND estado=1 and (estado_sesion is null or estado_sesion=''  or estado_sesion ='NO')
                 group by 1 order by 2 desc");
        
        $ttNoInicio = 0.00;
        $ttSusAfectados= 0.00;
        
            foreach ($noInicioSsesion as $v) {
                $ttNoInicio += $v->totalobservacion;
                $ttSusAfectados += $v->sustentantes_afect;
            }
        
        $n_Sustentantes = \DB::select("SELECT SUM(a.programados) AS programados 
FROM (SELECT (CASE WHEN estado_sesion is null THEN 'NO' WHEN estado_sesion ='' THEN 'NO' ELSE estado_sesion END) AS estado_sesion, programados, cgi_periodo_id, fecha_programada_inicio, sesion, estado FROM umonitoreo) as a
WHERE a.cgi_periodo_id=$id_periodo $coordinador $fecha $sesion  and a.estado=1 AND a.estado_sesion='NO'");
        
        $Tt_Sustentantes = \DB::select("SELECT SUM(programados) AS t_programados FROM umonitoreo WHERE cgi_periodo_id=$id_periodo $coordinador $fecha $sesion and estado=1");
        
        $Total_Sustentantes= 0.00;
            
            foreach($Tt_Sustentantes as $Tt_Sustentantes){
                $Total_Sustentantes += $Tt_Sustentantes->t_programados;
            }
            
        $reporteLlamada = \DB::select("SELECT (case when estadollamada_apli is null then 'POR CONTACTAR' when estadollamada_apli ='' then 'POR CONTACTAR' else estadollamada_apli end) as estadollamada, COUNT(*) as cantidadllamada
        FROM umonitoreo WHERE cgi_periodo_id = $id_periodo $coordinador $fecha $sesion AND estado = 1 GROUP BY (case when estadollamada_apli is null then 'POR CONTACTAR' when estadollamada_apli ='' then 'POR CONTACTAR' else estadollamada_apli end) ORDER BY estadollamada");
        
        $novedadllamada = \DB::select("SELECT (case when novedadllamada_apli='' then 'NO CONTESTA' else novedadllamada_apli end) as novedadllamada_apli, count(*) AS totalnovedad_apli FROM umonitoreo where cgi_periodo_id = $id_periodo $coordinador $fecha $sesion AND estado=1 and estadollamada_apli='NO CONTACTADO' group by (case when novedadllamada_apli='' then 'NO CONTESTA' else novedadllamada_apli end) order by totalnovedad_apli desc");
        
        $TotalNoContactadoa = 0.00;

            foreach ($novedadllamada as $v) {
                $TotalNoContactadoa += $v->totalnovedad_apli;
            }
        
        $monitores= \DB::select("SELECT zona, count(cgi_laboratorio_id) as laboratorio, name_periodo,
	count(case when descarga_aplicativo ='' then 'NO' when descarga_aplicativo is null then 'NO' when descarga_aplicativo ='NO' then 'NO' end) as descargano,
	count(case when descarga_aplicativo ='SI' then 'SI' end) as descargasi,
	count(case when instalo_aplicativo ='' then 'NO' when instalo_aplicativo is null then 'NO' when instalo_aplicativo ='NO' then 'NO' end) as instalono, 
	count(case when instalo_aplicativo ='SI' then 'SI' end) as instalosi,
	count(case when estado_sesion ='' then 'NO' when estado_sesion is null then 'NO' when estado_sesion ='NO' then 'NO' end) as sesionno,
	count(case when estado_sesion ='SI' then 'SI' end) as sesionsi,
	count(case when estadollamada_apli ='CONTACTADO' then 'CONTACTADO' end) as contactado,
	count(case when estadollamada_apli ='NO CONTACTADO' then 'NO CONTACTADO' end) as nocontactado,
	count(case when estadollamada_apli ='' then 'EN PROCESO' when estadollamada_apli is null then 'EN PROCESO' end) as enproceso,
	count(case when novedadllamada_apli !='' then novedadllamada_apli end) as novedadllamada
        FROM umonitoreo WHERE cgi_periodo_id =$id_periodo $coordinador AND estado=1 $fecha $sesion
        GROUP BY zona, name_periodo order by zona, laboratorio");  
                  
        $TotalDesc = 0.00;
        foreach ($reportedescarga as $generaldesc){
        $TotalDesc += $generaldesc->cantidad;
        }
            
        $TotalInst = 0.00;
        foreach ($reporteinstaloapli as $generalInstal){
        $TotalInst += $generalInstal->cantidad;
        }
            
        $TotalSesion = 0.00;
        foreach ($reporteinisesion as $generalSesion){
        $TotalSesion += $generalSesion->cantidad;
        }
            
        $TotalLlamada = 0.00;
        foreach ($reporteLlamada as $generalLlamada){
        $TotalLlamada += $generalLlamada->cantidadllamada;
        }
        $periodo = 0.00;
        $TotalLab = 0.00;
        $TotalDescargosi = 0.00;
        $TotalDescargono = 0.00;
        $TotalInstalosi = 0.00;
        $TotalInstalono = 0.00;
        $TotalSesionsi = 0.00;
        $TotalSesionno = 0.00;
        $TotalContactado = 0.00;
        $TotalNoContactado = 0.00;
        $TotalEnProceso = 0.00;
        $TotalNovedad = 0.00;
              
        foreach ($monitores as $v) {
        $periodo = $v->name_periodo;
        $TotalLab += $v->laboratorio;
        $TotalDescargosi += $v->descargasi;
        $TotalDescargono += $v->descargano;
        $TotalInstalosi += $v->instalosi;
        $TotalInstalono += $v->instalono;
        $TotalSesionsi += $v->sesionsi;
        $TotalSesionno += $v->sesionno;
        $TotalContactado += $v->contactado;
        $TotalNoContactado += $v->nocontactado;
        $TotalEnProceso += $v->enproceso;
        $TotalNovedad += $v->novedadllamada;
        }        
        return view('aplicacion.aplicacionZona_borrar')->with('fecha', $fecha)->with('ss', $ss)->with('novedad', $novedadllamada)->with('ttnovedad', $TotalNoContactadoa)->with('idperiodo',$id_periodo)->with('fecha',$fch)->with('sesion',$sesion)->with('reportdescgeneral',$reportedescarga)->with('totaldesc',$TotalDesc)->with('reporteinstaloapli',$reporteinstaloapli)->with('TotalInst',$TotalInst)->with('reporteLlamada',$reporteLlamada)->with('TotalLlamada',$TotalLlamada)->with('reporteinisesion',$reporteinisesion)->with('TotalSesion',$TotalSesion)->with('periodo',$periodo)->with('coordinador',$coordinador)->with('n_Sustentantes', $n_Sustentantes)->with('Total_Sustentantes', $Total_Sustentantes)->with('monitores',$monitores)->with('TotalLab',$TotalLab)->with('TotalDescargosi',$TotalDescargosi)->with('TotalDescargono',$TotalDescargono)->with('TotalInstalosi',$TotalInstalosi)->with('TotalInstalono',$TotalInstalono)->with('TotalSesionsi',$TotalSesionsi)->with('TotalSesionno',$TotalSesionno)->with('noInicioSsesion',$noInicioSsesion)->with('ttNoInicio',$ttNoInicio)->with('ttSusAfectados',$ttSusAfectados)->with('TotalContactado',$TotalContactado)->with('TotalNoContactado',$TotalNoContactado)->with('TotalEnProceso',$TotalEnProceso)->with('TotalNovedad',$TotalNovedad);
        
        //// CARGA DE ARCHIVOS        
        }
        }
        
    
    Public function reporte_aplicacion($tp_reporte, $id_periodo, $coord, $fch="", $ss="") {
        
        if($coord =='0'){ $coordinador = '';}else{ $coordinador = "AND coordinador='$coord'"; }        
        if($fch == 0){$fecha = '';}else{ $fecha = "AND fecha_programada_inicio='$fch'"; }        
        if( $ss ==""){$sesion = '';}else{$sesion = "AND sesion='$ss'";}        
        
        /// LLAMADAS A LOS APLICADOSRES, PILOTO 24/02/2017
        if($tp_reporte == "llamadas_aplicadores"){
                         
        $reporteLlamada= \DB::select("select (case when ap.estadollamada_apli is null then 'EN PROCESO' when ap.estadollamada_apli ='' then 'EN PROCESO' else ap.estadollamada_apli END)as estadollamada, count(*) as totalestado from (
        select codigoamei, sesion from umonitoreo where cgi_periodo_id = $id_periodo $coordinador and estado=1 $fecha $sesion group by codigoamei, sesion) as  u, sig_aplicadores ap 
        where ap.amie=u.codigoamei  and ap.cgi_periodo_id=104
        GROUP BY (case when ap.estadollamada_apli is null then 'EN PROCESO' when ap.estadollamada_apli ='' then 'EN PROCESO' else ap.estadollamada_apli END)");
        
        $novedadllamada= \DB::select("select (case when ap.novedadllamada_apli='' and ap.estadollamada_apli='CONTACTADO' then 'CONTACTADO' else ap.novedadllamada_apli END) as novedadllamada, count(*) as totalnovedad from (
        select codigoamei, sesion from umonitoreo where cgi_periodo_id = $id_periodo $coordinador and estado=1 $fecha $sesion and estado=1 group by codigoamei, sesion) as  u, sig_aplicadores ap 
        where ap.amie=u.codigoamei  and ap.cgi_periodo_id=104
        GROUP BY (case when ap.novedadllamada_apli='' and ap.estadollamada_apli='CONTACTADO' then 'CONTACTADO' else ap.novedadllamada_apli END) ORDER BY 1");
                
        $reporteAplicador = \DB::select("select zona,  count(codigoamei) as instituciones, count(u.id_sede) as laboratorios, name_periodo,
        count(case when ap.estadollamada_apli ='CONTACTADO' then 'CONTACTADO' end) as contactado,
        count(case when ap.estadollamada_apli ='NO CONTACTADO' then 'NO CONTACTADO' end) as nocontactado,
        count(case when ap.estadollamada_apli ='' then 'EN PROCESO' when ap.estadollamada_apli is null then 'EN PROCESO' end) as enproceso,
        count(case when ap.novedadllamada_apli !='' then ap.novedadllamada_apli end) as novedadllamada
        from (select zona, codigoamei, id_sede, sesion, name_periodo from umonitoreo where cgi_periodo_id = $id_periodo $coordinador $fecha $sesion  and estado=1 
        group by zona, codigoamei, id_sede, sesion, name_periodo) as  u, sig_aplicadores ap 
        where ap.amie=u.codigoamei and ap.cgi_periodo_id=104
        group by u.zona, u.name_periodo order by u.zona");              
            
        $TotalNovLlamada = 0.00;
        
        foreach ($novedadllamada as $v) {
        $TotalNovLlamada += $v->totalnovedad;           
        }
        
        $TotalLlamada = 0.00;
        
        foreach ($reporteLlamada as $v) {
        $TotalLlamada += $v->totalestado;           
        }
        
        $periodo = 0.00;    
        $TotalLab = 0.00;        
        $TotalInst = 0.00;        
        $TotalContactado = 0.00;
        $TotalNoContactado = 0.00;
        $TotalEnProceso = 0.00;
        $TotalNovedad = 0.00;        
        
        foreach ($reporteAplicador as $v) {
        $periodo = $v->name_periodo;                
        $TotalLab += $v->laboratorios;                
        $TotalInst += $v->instituciones;                
        $TotalContactado += $v->contactado;
        $TotalNoContactado += $v->nocontactado;
        $TotalEnProceso += $v->enproceso;
        $TotalNovedad += $v->novedadllamada;
        }
              
        return view('aplicadores_aplicacion.aplicadorZona')->with('idperiodo',$id_periodo)->with('estadollamada', $reporteLlamada)->with('totalLlamada', $TotalLlamada)->with('novedadllamada',$novedadllamada)->with('TotalNovLlamada',$TotalNovLlamada)->with('coordinador',$coordinador)->with('reporteAplicador',$reporteAplicador)->with('TotalLab',$TotalLab)->with('TotalInst',$TotalInst)->with('TotalContactado',$TotalContactado)->with('TotalNoContactado',$TotalNoContactado)->with('TotalEnProceso',$TotalEnProceso)->with('TotalNovedad',$TotalNovedad)->with('name_periodo',$periodo);
    
        //// Descarga de aplicativo, Instalacion e Inicio de Sesión. 
            }elseif($tp_reporte == "inicio_aplicacion"){
                                   
        $reportedescarga= \DB::select("SELECT (case when descarga_aplicativo is null then 'NO' when descarga_aplicativo ='' then 'NO' else descarga_aplicativo end) as descarga, COUNT(*) as cantidad
        FROM umonitoreo WHERE cgi_periodo_id = $id_periodo $coordinador $fecha $sesion and estado=1 and estado_sesion2 is null  GROUP BY (case when descarga_aplicativo is null then 'NO' when descarga_aplicativo ='' then 'NO' else descarga_aplicativo end)");
        
        if($fecha == '' and $sesion == '')
        {
            $reportesustentantes= \DB::select("SELECT status, count(*) as cantidad from view_sustensesionlab_otielnet where cgi_periodo_id in (select periodo_monitoreo from umonitoreo where cgi_periodo_id=$id_periodo  and estado_sesion2 is null group by periodo_monitoreo) group by status");
        }
        else if ($fecha != '' and $sesion == '')
        {
            $reportesustentantes= \DB::select("SELECT status, count(*) as cantidad from view_sustensesionlab_otielnet where cgi_periodo_id in (select periodo_monitoreo from umonitoreo where cgi_periodo_id=$id_periodo  and estado_sesion2 is null group by periodo_monitoreo) $fecha group by status");
        }
        else if ($fecha != '' and $sesion != '')
        {
            $reportesustentantes= \DB::select("SELECT status, count(*) as cantidad from view_sustensesionlab_otielnet where cgi_periodo_id in (select periodo_monitoreo from umonitoreo where cgi_periodo_id=$id_periodo  and estado_sesion2 is null group by periodo_monitoreo) $sesion group by status");
        }
        

        $reportelistadosasistencia= \DB::select("SELECT (case when listado_asistencia is null then 'NO' when listado_asistencia ='' then 'NO' else listado_asistencia end) as listado, COUNT(*) as cantidad
        FROM umonitoreo WHERE cgi_periodo_id = $id_periodo $coordinador $fecha $sesion and estado=1 and estado_sesion2 is null GROUP BY (case when listado_asistencia is null then 'NO' when listado_asistencia ='' then 'NO' else listado_asistencia end)"); 
        //////para cantidades de los listados recibidos por parte del monitor solo falta cambiar el sql 
        $reportelistadosasistenciarecibidos= \DB::select("SELECT (case when observacion_listado_asistencia='' then 'VALIDADOS' when observacion_listado_asistencia='SIN NOVEDAD' then 'VALIDADOS' else 'CON ERROR' end) as listadosrecibidos, COUNT(*) as cantidad 
from umonitoreo where cgi_periodo_id = $id_periodo $coordinador $fecha $sesion and estado=1  and estado_sesion2 is null and listado_asistencia='SI'
group by (case when observacion_listado_asistencia='' then 'VALIDADOS' when observacion_listado_asistencia='SIN NOVEDAD' then 'VALIDADOS' else 'CON ERROR' end) order by 1 desc");           
                                    
        $reporteinstaloapli= \DB::select("SELECT (case when instalo_aplicativo is null then 'NO' when instalo_aplicativo ='' then 'NO' else instalo_aplicativo end) as instalo, COUNT(*) as cantidad
        FROM umonitoreo WHERE cgi_periodo_id = $id_periodo $coordinador $fecha $sesion AND estado=1 and estado_sesion2 is null GROUP BY (case when instalo_aplicativo is null then 'NO' when instalo_aplicativo ='' then 'NO' else instalo_aplicativo end)");            
                                    
        $reporteinisesion = \DB::select("SELECT (case when estado_sesion is null then 'NO' when estado_sesion ='' then 'NO' else estado_sesion end) as sesion, COUNT(*) as cantidad
        FROM umonitoreo WHERE cgi_periodo_id = $id_periodo $coordinador $fecha $sesion AND estado = 1 and estado_sesion2 is null GROUP BY (case when estado_sesion is null then 'NO' when estado_sesion ='' then 'NO' else estado_sesion end)");            
        $reporteinisesion2 = \DB::select("SELECT (case when estado_sesion2 is null then 'NO' when estado_sesion2 ='' then 'NO' else estado_sesion2 end) as sesion, COUNT(*) as cantidad
        FROM umonitoreo WHERE cgi_periodo_id = $id_periodo $coordinador $fecha $sesion AND estado = 1 and estado_sesion2 is null GROUP BY (case when estado_sesion2 is null then 'NO' when estado_sesion2 ='' then 'NO' else estado_sesion2 end)");    
            
        $noDescargo= \DB::select("SELECT (case when observacion_descarga='' then 'SIN DESCARGAR APLICATIVO' 
                 when observacion_descarga IS NULL then 'SIN DESCARGAR APLICATIVO'
                 when descarga_aplicativo='' and observacion_descarga='SIN PROBLEMA' then 'SIN DESCARGAR APLICATIVO'
                 when descarga_aplicativo IS NULl and observacion_descarga='SIN PROBLEMA' then 'SIN DESCARGAR APLICATIVO'
                 when descarga_aplicativo ='NO' and observacion_descarga='SIN PROBLEMA' then 'SIN PROBLEMA REGISTRADO'
                 when descarga_aplicativo IS NULL and observacion_descarga!='SIN PROBLEMA' then 'SIN SELECCIONAR DESCARGA DEL APLICATIVO'
                 else observacion_descarga end) as observacion_descarga, count(*) AS totalobservacion, sum(programados) as sustentantes_afect
                 FROM umonitoreo 
                 where cgi_periodo_id = $id_periodo $coordinador $fecha $sesion AND estado=1 and estado_sesion2 is null and (descarga_aplicativo is null or descarga_aplicativo=''  or descarga_aplicativo ='NO')
                 group by 1 order by 2 desc");
        
        $ttNoDescargo = 0.00;
        $ttDSusAfectados= 0.00;
        
            foreach ($noDescargo as $v) {
                $ttNoDescargo += $v->totalobservacion;
                $ttDSusAfectados += $v->sustentantes_afect;
            }
            
        $noInstalo= \DB::select("SELECT (case when observacion_instala='' then 'SIN INSTALAR APLICATIVO' 
                 when observacion_instala IS NULL then 'SIN INSTALAR APLICATIVO'
                 when instalo_aplicativo='' and observacion_instala='SIN PROBLEMA' then 'SIN INSTALAR APLICATIVO'
                 when instalo_aplicativo IS NULl and observacion_instala='SIN PROBLEMA' then 'SIN INSTALAR APLICATIVO'
                 when instalo_aplicativo ='NO' and observacion_instala='SIN PROBLEMA' then 'SIN PROBLEMA REGISTRADO'
                 when instalo_aplicativo IS NULL and observacion_instala!='SIN PROBLEMA' then 'SIN SELECCIONAR INSTALACION DEL APLICATIVO'
                 else observacion_instala end) as observacion_instala, count(*) AS totalobservacion, sum(programados) as sustentantes_afect
                 FROM umonitoreo 
                 where cgi_periodo_id = $id_periodo $coordinador $fecha $sesion AND estado=1 and estado_sesion2 is null and (instalo_aplicativo is null or instalo_aplicativo=''  or instalo_aplicativo ='NO')
                 group by 1 order by 2 desc");
        
        $ttNoInstalo = 0.00;
        $ttISusAfectados= 0.00;
        
            foreach ($noInstalo as $v) {
                $ttNoInstalo += $v->totalobservacion;
                $ttISusAfectados += $v->sustentantes_afect;
            }
            
        $noInicioSsesion= \DB::select("SELECT (case when observacion_sesion='' then 'SIN INICIAR SESION' 
                 when observacion_sesion IS NULL then 'SIN INICIAR SESION'
                 when estado_sesion='' and observacion_sesion='SIN PROBLEMA' then 'SIN INICIAR SESION'
                 when estado_sesion IS NULl and observacion_sesion='SIN PROBLEMA' then 'SIN INICIAR SESION'
                 when estado_sesion ='NO' and observacion_sesion='SIN PROBLEMA' then 'SIN PROBLEMA REGISTRADO'
                 when estado_sesion IS NULL and observacion_sesion!='SIN PROBLEMA' then 'SIN SELECCIONAR ESTADO DE SESIÓN'
                 else observacion_sesion end) as observacion_sesion, count(*) AS totalobservacion, sum(programados) as sustentantes_afect
                 FROM umonitoreo 
                 where cgi_periodo_id = $id_periodo $coordinador $fecha $sesion AND estado=1 and estado_sesion2 is null and (estado_sesion is null or estado_sesion=''  or estado_sesion ='NO')
                 group by 1 order by 2 desc");
        
        $ttNoInicio = 0.00;
        $ttSusAfectados= 0.00;
        
            foreach ($noInicioSsesion as $v) {
                $ttNoInicio += $v->totalobservacion;
                $ttSusAfectados += $v->sustentantes_afect;
            }    
        
        $n_Sustentantes = \DB::select("SELECT SUM(a.programados) AS programados 
FROM (SELECT (CASE WHEN estado_sesion is null THEN 'NO' WHEN estado_sesion ='' THEN 'NO' ELSE estado_sesion END) AS estado_sesion, programados, cgi_periodo_id, fecha_programada_inicio, sesion, estado, estado_sesion2 FROM umonitoreo) as a
WHERE a.cgi_periodo_id=$id_periodo $coordinador $fecha $sesion  and a.estado=1 and a.estado_sesion2 is null AND a.estado_sesion='NO'");
        
        $Tt_Sustentantes = \DB::select("SELECT SUM(programados) AS t_programados FROM umonitoreo WHERE cgi_periodo_id=$id_periodo $coordinador $fecha $sesion and estado=1 and estado_sesion2 is null");
        
        $Total_Sustentantes= 0.00;
            
            foreach($Tt_Sustentantes as $Tt_Sustentantes){
                $Total_Sustentantes += $Tt_Sustentantes->t_programados;
            }
            
        $reporteLlamada = \DB::select("SELECT (case when estadollamada_apli is null then 'POR CONTACTAR' when estadollamada_apli ='' then 'POR CONTACTAR' else estadollamada_apli end) as estadollamada, COUNT(*) as cantidadllamada
        FROM umonitoreo WHERE cgi_periodo_id = $id_periodo $coordinador $fecha $sesion AND estado = 1 and estado_sesion2 is null GROUP BY (case when estadollamada_apli is null then 'POR CONTACTAR' when estadollamada_apli ='' then 'POR CONTACTAR' else estadollamada_apli end) ORDER BY estadollamada");
        
        $novedadllamada = \DB::select("SELECT (case when novedadllamada_apli='' then 'NO CONTESTA' else novedadllamada_apli end) as novedadllamada_apli, count(*) AS totalnovedad_apli FROM umonitoreo where cgi_periodo_id = $id_periodo $coordinador $fecha $sesion AND estado=1 and estado_sesion2 is null and estadollamada_apli='NO CONTACTADO' group by (case when novedadllamada_apli='' then 'NO CONTESTA' else novedadllamada_apli end) order by totalnovedad_apli desc");
        
        $TotalNoContactadoa = 0.00;

            foreach ($novedadllamada as $v) {
                $TotalNoContactadoa += $v->totalnovedad_apli;
            }
        
        $monitores= \DB::select("SELECT zona, count(cgi_laboratorio_id) as laboratorio, name_periodo,
	count(case when descarga_aplicativo ='' then 'NO' when descarga_aplicativo is null then 'NO' when descarga_aplicativo ='NO' then 'NO' end) as descargano,
	count(case when descarga_aplicativo ='SI' then 'SI' end) as descargasi,
	count(case when instalo_aplicativo ='' then 'NO' when instalo_aplicativo is null then 'NO' when instalo_aplicativo ='NO' then 'NO' end) as instalono, 
	count(case when instalo_aplicativo ='SI' then 'SI' end) as instalosi,
	count(case when estado_sesion ='' then 'NO' when estado_sesion is null then 'NO' when estado_sesion ='NO' then 'NO' end) as sesionno,
	count(case when estado_sesion ='SI' then 'SI' end) as sesionsi,
	count(case when estadollamada_apli ='CONTACTADO' then 'CONTACTADO' end) as contactado,
	count(case when estadollamada_apli ='NO CONTACTADO' then 'NO CONTACTADO' end) as nocontactado,
	count(case when estadollamada_apli ='' then 'EN PROCESO' when estadollamada_apli is null then 'EN PROCESO' end) as enproceso,
	count(case when novedadllamada_apli !='' then novedadllamada_apli end) as novedadllamada
        FROM umonitoreo WHERE cgi_periodo_id =$id_periodo $coordinador AND estado=1 and estado_sesion2 is null $fecha $sesion
        GROUP BY zona, name_periodo order by zona, laboratorio");  
                  
        $TotalDesc = 0.00;
        foreach ($reportedescarga as $generaldesc){
        $TotalDesc += $generaldesc->cantidad;
        }

        $TotalList = 0.00;
        foreach ($reportelistadosasistencia as $generallist){
        $TotalList += $generallist->cantidad;
        }

        
        $TotalListsus = 0.00;
        foreach ($reportesustentantes as $generallistsus){
        $TotalListsus += $generallistsus->cantidad;
        }

        $TotalListreci = 0.00;
        foreach ($reportelistadosasistenciarecibidos as $generallistreci){
        $TotalListreci += $generallistreci->cantidad;
        }
            
        $TotalInst = 0.00;
        foreach ($reporteinstaloapli as $generalInstal){
        $TotalInst += $generalInstal->cantidad;
        }
            
        $TotalSesion = 0.00;
        foreach ($reporteinisesion as $generalSesion){
        $TotalSesion += $generalSesion->cantidad;
        }

         $TotalSesion2 = 0.00;
        foreach ($reporteinisesion2 as $generalSesion2){
        $TotalSesion2 += $generalSesion2->cantidad;
        }
            
        $TotalLlamada = 0.00;
        foreach ($reporteLlamada as $generalLlamada){
        $TotalLlamada += $generalLlamada->cantidadllamada;
        }
        $periodo = 0.00;
        $TotalLab = 0.00;
        $TotalDescargosi = 0.00;
        $TotalDescargono = 0.00;
        $TotalInstalosi = 0.00;
        $TotalInstalono = 0.00;
        $TotalSesionsi = 0.00;
        $TotalSesionno = 0.00;
        $TotalContactado = 0.00;
        $TotalNoContactado = 0.00;
        $TotalEnProceso = 0.00;
        $TotalNovedad = 0.00;
              
        foreach ($monitores as $v) {
        $periodo = $v->name_periodo;
        $TotalLab += $v->laboratorio;
        $TotalDescargosi += $v->descargasi;
        $TotalDescargono += $v->descargano;
        $TotalInstalosi += $v->instalosi;
        $TotalInstalono += $v->instalono;
        $TotalSesionsi += $v->sesionsi;
        $TotalSesionno += $v->sesionno;
        $TotalContactado += $v->contactado;
        $TotalNoContactado += $v->nocontactado;
        $TotalEnProceso += $v->enproceso;
        $TotalNovedad += $v->novedadllamada;
        }

        $Total_status = 0.00;
        foreach ($reportesustentantes as $v) {
        $Total_status += $v->cantidad;
        }



        return view('aplicacion.aplicacionZona')->with('fecha', $fecha)->with('ss', $ss)->with('novedad', $novedadllamada)->with('ttnovedad', $TotalNoContactadoa)->with('idperiodo',$id_periodo)->with('fecha',$fch)->with('sesion',$sesion)->with('reportdescgeneral',$reportedescarga)->with('totaldesc',$TotalDesc)->with('reporteinstaloapli',$reporteinstaloapli)->with('TotalInst',$TotalInst)->with('reporteLlamada',$reporteLlamada)->with('TotalLlamada',$TotalLlamada)->with('reporteinisesion',$reporteinisesion)->with('TotalSesion',$TotalSesion)->with('reporteinisesion2',$reporteinisesion2)->with('TotalSesion2',$TotalSesion2)->with('periodo',$periodo)->with('coordinador',$coordinador)->with('n_Sustentantes', $n_Sustentantes)->with('Total_Sustentantes', $Total_Sustentantes)->with('monitores',$monitores)->with('TotalLab',$TotalLab)->with('TotalDescargosi',$TotalDescargosi)->with('TotalDescargono',$TotalDescargono)->with('TotalInstalosi',$TotalInstalosi)->with('TotalInstalono',$TotalInstalono)->with('TotalSesionsi',$TotalSesionsi)->with('TotalSesionno',$TotalSesionno)->with('noInicioSsesion',$noInicioSsesion)->with('ttNoInicio',$ttNoInicio)->with('ttSusAfectados',$ttSusAfectados)->with('noDescargo',$noDescargo)->with('ttNoDescargo',$ttNoDescargo)->with('ttDSusAfectados',$ttDSusAfectados)->with('noInstalo',$noInstalo)->with('ttNoInstalo',$ttNoInstalo)->with('ttISusAfectados',$ttISusAfectados)->with('TotalContactado',$TotalContactado)->with('TotalNoContactado',$TotalNoContactado)->with('TotalEnProceso',$TotalEnProceso)->with('TotalNovedad',$TotalNovedad)->with('TotalList',$TotalList)->with('reportelistadosasistencia',$reportelistadosasistencia)->with('TotalListreci',$TotalListreci)->with('reportelistadosasistenciarecibidos',$reportelistadosasistenciarecibidos)
            ->with('reportesustentantes',$reportesustentantes)->with('TotalListsus',$TotalListsus)
            ->with('Total_status',$Total_status);
        
        //// CARGA DE ARCHIVOS        
        } elseif($tp_reporte == "fin_aplicacion"){
            
        $reporteCargas = \DB::select("select a.zona, count(a.laboratorio) as laboratorio, SUM(a.programados) as programados, sum(a.asistencia) as asistencia, sum(a.cargas) as cargas, a.name_periodo
from (SELECT zona, count(cgi_laboratorio_id) as laboratorio, SUM(programados) as programados, sum(asistencia) as asistencia, sum(cargas) as cargas, name_periodo 
FROM umonitoreo WHERE cgi_periodo_id =$id_periodo $coordinador $fecha $sesion AND estado=1 and estado_sesion2 is null AND created_at is null group by zona, cgi_laboratorio_id, name_periodo order by zona) as a 
GROUP BY a.zona, a.name_periodo order by zona");        
        
        $periodo = 0.00;            
        $TotalProg = 0.00;
        $TotalAsis = 0.00;
        $TotalAusent = 0.00;
        $TotalCargas = 0.00;
        $TotalPorCargar = 0.00;
        
        foreach ($reporteCargas as $v) {
        $periodo = $v->name_periodo;
        $TotalProg += $v->programados;
        $TotalAsis += $v->asistencia;
        $TotalAusent += ($v->programados - $v->asistencia);
        $TotalCargas += $v->cargas;
        $TotalPorCargar += ($v->asistencia - $v->cargas);
        }
        
        if($TotalProg == 0){
        $porcencarga = (($TotalCargas) / 1 );
        }else{
        $porcencarga = (($TotalCargas) / ($TotalProg));
        }
        $porcentajecar = number_format(($porcencarga * 100), 2, ',', ' ');

        return view('aplicacion.cargasZona')->with('coordinador',$coordinador)->with('periodo', $periodo)->with('reportecarga', $reporteCargas)->with('totalpro', $TotalProg)->with('totalasis', $TotalAsis)->with('TotalAusent',$TotalAusent)->with('totalcargas', $TotalCargas)->with('porcentajecar', $porcentajecar)->with('TotalPorCargar',$TotalPorCargar);     
        }
        }
        
        public function provincia_aplicacion(Request $request, $tp_reporte, $id_periodo, $fch ="", $sesion="") {
            
        $coord= $request->coordinador;    
        if($coord ==''){ $coordinador = '';}else{ $coordinador = "$coord";}        
        if($fch == 0){ $fecha = ''; }else{ $fecha = "AND fecha_programada_inicio='$fch'";}        
        if( $sesion ==""){ $sesion = ''; }else{$sesion = "AND sesion='$sesion'"; }    
                
        $zona= $request->zona;
        
         foreach ($zona as $zona){
            $val_z = "'".$zona."'";
            $zona_aux[] = $val_z;
        }
        $zonas = implode(',', $zona_aux); // separar los valores con coma(,).
        $sql_zona = "(" .$zonas. ")";
        
        //// Descarga de aplicativo, Instalacion e Inicio de Sesión.
        if($tp_reporte == "inicio_aplicacion"){                   
        
        $reporteprovincia = \DB::select("SELECT provincia, zona, coordinador, count(cgi_laboratorio_id) as laboratorio, 
	count(case when descarga_aplicativo ='' then 'NO' when descarga_aplicativo is null then 'NO' when descarga_aplicativo ='NO' then 'NO' end) as descargano,
	count(case when descarga_aplicativo ='SI' then 'SI' end) as descargasi,
	count(case when instalo_aplicativo ='' then 'NO' when instalo_aplicativo is null then 'NO' when instalo_aplicativo ='NO' then 'NO' end) as instalono, 
	count(case when instalo_aplicativo ='SI' then 'SI' end) as instalosi,
	count(case when estado_sesion ='' then 'NO' when estado_sesion is null then 'NO' when estado_sesion ='NO' then 'NO' end) as sesionno,
	count(case when estado_sesion ='SI' then 'SI' end) as sesionsi,
	count(case when estadollamada_apli ='CONTACTADO' then 'CONTACTADO' end) as contactado,
	count(case when estadollamada_apli ='NO CONTACTADO' then 'NO CONTACTADO' end) as nocontactado,
	count(case when estadollamada_apli ='' then 'EN PROCESO' when estadollamada_apli is null then 'EN PROCESO' end) as enproceso,
	count(case when novedadllamada_apli !='' then novedadllamada_apli end) as novedadllamada
        FROM umonitoreo WHERE cgi_periodo_id=$id_periodo AND estado=1 AND zona IN $sql_zona $coordinador $fecha $sesion
        GROUP BY provincia, zona, coordinador order by provincia, zona, coordinador, laboratorio");
        
        
        $TotalLab = 0.00;
        $TotalDescargosi = 0.00;
        $TotalDescargono = 0.00;
        $TotalInstalosi = 0.00;
        $TotalInstalono = 0.00;
        $TotalSesionsi = 0.00;
        $TotalSesionno = 0.00;
        $TotalContactado = 0.00;
        $TotalNoContactado = 0.00;
        $TotalEnProceso = 0.00;
        $TotalNovedad = 0.00;        
        
            foreach ($reporteprovincia as $v) {
                $TotalLab += $v->laboratorio;
                $TotalDescargosi += $v->descargasi;
                $TotalDescargono += $v->descargano;
                $TotalInstalosi += $v->instalosi;
                $TotalInstalono += $v->instalono;
                $TotalSesionsi += $v->sesionsi;
                $TotalSesionno += $v->sesionno;
                $TotalContactado += $v->contactado;
                $TotalNoContactado += $v->nocontactado;
                $TotalEnProceso += $v->enproceso;
                $TotalNovedad += $v->novedadllamada;           
            }       
                    
        return view('aplicacion.aplicacionProvincia')->with('idperiodo',$id_periodo)->with('fecha',$fch)->with('sesion',$sesion)->with('reporteprovincia', $reporteprovincia)->with('TotalLab',$TotalLab)->with('TotalDescargosi',$TotalDescargosi)->with('TotalDescargono',$TotalDescargono)->with('TotalInstalosi',$TotalInstalosi)->with('TotalInstalono',$TotalInstalono)->with('TotalSesionsi',$TotalSesionsi)->with('TotalSesionno',$TotalSesionno)->with('TotalContactado',$TotalContactado)->with('TotalNoContactado',$TotalNoContactado)->with('TotalEnProceso',$TotalEnProceso)->with('TotalNovedad',$TotalNovedad)->with('coordinador',$coordinador);
        
        /// CARGA DE ARCHIVOS
        }elseif($tp_reporte == "fin_aplicacion"){
       
            $cargasProvincias = \DB::select("select a.provincia, a.zona, a.coordinador, count(a.laboratorio) as laboratorio, SUM(a.programados) as programados, sum(a.asistencia) as asistencia, sum(a.cargas) as cargas
        from (SELECT provincia, zona, coordinador, count(cgi_laboratorio_id) as laboratorio, SUM(programados) as programados, sum(asistencia) as asistencia, sum(cargas) as cargas 
        FROM umonitoreo WHERE cgi_periodo_id =$id_periodo $coordinador $fecha $sesion AND zona IN $sql_zona AND estado=1 AND created_at is null group by provincia, zona, coordinador, cgi_laboratorio_id order by provincia, zona, coordinador ) as a 
        GROUP BY a.provincia, a.zona, a.coordinador order by a.provincia, a.zona, a.coordinador");
            
        $TotalProg = 0.00;
        $TotalAsis = 0.00;
        $TotalAusent = 0.00;
        $TotalCargas = 0.00;        
        $TotalPorCargar = 0.00;
        
            foreach ($cargasProvincias as $v) {
                $TotalProg += $v->programados;
                $TotalAsis += $v->asistencia;
                $TotalAusent += ($v->programados - $v->asistencia);
                $TotalCargas += $v->cargas;
                $TotalPorCargar += ($v->asistencia - $v->cargas);
            }
        
            if($TotalProg == 0){
                $porcencarga = (($TotalCargas) / 1 );
            }else{
            $porcencarga = (($TotalCargas) / ($TotalProg));
            }
            $porcentajecar = number_format(($porcencarga * 100), 2, ',', ' ');
        
        return view('aplicacion.cargasProvincia')->with('cargasProvincias',$cargasProvincias)->with('coordinador',$coord)->with('totalpro', $TotalProg)->with('totalasis', $TotalAsis)->with('TotalAusent',$TotalAusent)->with('totalcargas', $TotalCargas)->with('porcentajecar', $porcentajecar)->with('TotalPorCargar',$TotalPorCargar);                        
               
        }
        }
        
        public function distrito_aplicacion(Request $request, $tp_reporte, $id_periodo, $fch ="", $sesion="") {
                
        if($fch == 0){ $fecha = ''; }else{ $fecha = "AND fecha_programada_inicio='$fch'";}        
        if( $sesion ==""){ $sesion = ''; }else{$sesion = "AND sesion='$sesion'"; }
        
        $provincia= $request->provincia;        
         foreach ($provincia as $provincia){
            $val_prov = "'".$provincia."'";
            $prov_aux[] = $val_prov;
        }
        $provincias = implode(',', $prov_aux); // separar los valores con coma(,).
        $sql_provincia = "(" .$provincias. ")";
        
        $coordinador_p= $request->coordinador_p;        
        if($coordinador_p == ''){
         $sql_coordinador ='';   
        }else{                    
         foreach ($coordinador_p as $coordinador_p){
            $val_p = "'".$coordinador_p."'";
            $coordinador_aux[] = $val_p;
        }        
        $coordinadores = implode(',', $coordinador_aux); // separar los valores con coma(,).
        $sql_coordinador = "(" .$coordinadores. ")";
        }        
        $coord="AND coordinador IN $sql_coordinador";
                
        $zona= $request->zona;        
         foreach ($zona as $zona){
            $val_z = "'".$zona."'";
            $zona_aux[] = $val_z;
        }
        $zonas = implode(',', $zona_aux); // separar los valores con coma(,).
        $sql_zona = "(" .$zonas. ")";
        
        //// Descarga de aplicativo, Instalacion e Inicio de Sesión.
        if($tp_reporte == "inicio_aplicacion"){                   
        
        $reportedistrito = \DB::select("SELECT distrito_id, provincia, zona, coordinador, count(cgi_laboratorio_id) as laboratorio, 
	count(case when descarga_aplicativo ='' then 'NO' when descarga_aplicativo is null then 'NO' when descarga_aplicativo ='NO' then 'NO' end) as descargano,
	count(case when descarga_aplicativo ='SI' then 'SI' end) as descargasi,
	count(case when instalo_aplicativo ='' then 'NO' when instalo_aplicativo is null then 'NO' when instalo_aplicativo ='NO' then 'NO' end) as instalono, 
	count(case when instalo_aplicativo ='SI' then 'SI' end) as instalosi,
	count(case when estado_sesion ='' then 'NO' when estado_sesion is null then 'NO' when estado_sesion ='NO' then 'NO' end) as sesionno,
	count(case when estado_sesion ='SI' then 'SI' end) as sesionsi,
	count(case when estadollamada_apli ='CONTACTADO' then 'CONTACTADO' end) as contactado,
	count(case when estadollamada_apli ='NO CONTACTADO' then 'NO CONTACTADO' end) as nocontactado,
	count(case when estadollamada_apli ='' then 'EN PROCESO' when estadollamada_apli is null then 'EN PROCESO' end) as enproceso,
	count(case when novedadllamada_apli !='' then novedadllamada_apli end) as novedadllamada
        FROM umonitoreo WHERE cgi_periodo_id=$id_periodo AND estado=1 $fecha $sesion $coord AND zona IN $sql_zona AND provincia IN $sql_provincia
        GROUP BY distrito_id, provincia, zona, coordinador order by distrito_id, provincia, zona, coordinador, laboratorio");
           
        $TotalLab = 0.00;
        $TotalDescargosi = 0.00;
        $TotalDescargono = 0.00;
        $TotalInstalosi = 0.00;
        $TotalInstalono = 0.00;
        $TotalSesionsi = 0.00;
        $TotalSesionno = 0.00;
        $TotalContactado = 0.00;
        $TotalNoContactado = 0.00;
        $TotalEnProceso = 0.00;
        $TotalNovedad = 0.00;        
        
            foreach ($reportedistrito as $v) {
                $TotalLab += $v->laboratorio;
                $TotalDescargosi += $v->descargasi;
                $TotalDescargono += $v->descargano;
                $TotalInstalosi += $v->instalosi;
                $TotalInstalono += $v->instalono;
                $TotalSesionsi += $v->sesionsi;
                $TotalSesionno += $v->sesionno;
                $TotalContactado += $v->contactado;
                $TotalNoContactado += $v->nocontactado;
                $TotalEnProceso += $v->enproceso;
                $TotalNovedad += $v->novedadllamada;           
            }       
                    
        return view('aplicacion.aplicacionDistrito')->with('idperiodo',$id_periodo)->with('fecha',$fch)->with('sesion',$sesion)->with('reportedistrito', $reportedistrito)->with('TotalLab',$TotalLab)->with('TotalDescargosi',$TotalDescargosi)->with('TotalDescargono',$TotalDescargono)->with('TotalInstalosi',$TotalInstalosi)->with('TotalInstalono',$TotalInstalono)->with('TotalSesionsi',$TotalSesionsi)->with('TotalSesionno',$TotalSesionno)->with('TotalContactado',$TotalContactado)->with('TotalNoContactado',$TotalNoContactado)->with('TotalEnProceso',$TotalEnProceso)->with('TotalNovedad',$TotalNovedad);
        
        /// CARGA DE ARCHIVOS
        }elseif($tp_reporte == "fin_aplicacion"){
        $cargasDistritos = \DB::select("select a.distrito_id, a.provincia, a.zona, a.coordinador, count(a.laboratorio) as laboratorio, SUM(a.programados) as programados, sum(a.asistencia) as asistencia, sum(a.cargas) as cargas
        from (SELECT distrito_id, provincia, zona, coordinador, count(cgi_laboratorio_id) as laboratorio, SUM(programados) as programados, sum(asistencia) as asistencia, sum(cargas) as cargas 
        FROM umonitoreo WHERE cgi_periodo_id =$id_periodo AND estado=1 $fecha $sesion $coord AND zona IN $sql_zona AND provincia IN $sql_provincia AND created_at is null group by distrito_id, provincia, zona, coordinador, cgi_laboratorio_id order by distrito_id, provincia, zona, coordinador) as a 
        GROUP BY a.distrito_id, a.provincia, a.zona, a.coordinador order by a.distrito_id, a.provincia, a.zona, a.coordinador");
            
        $TotalProg = 0.00;
        $TotalAsis = 0.00;
        $TotalAusent = 0.00;
        $TotalCargas = 0.00;
        $TotalPorCargar = 0.00;
        
            foreach ($cargasDistritos as $v) {
                $TotalProg += $v->programados;
                $TotalAsis += $v->asistencia;
                $TotalAusent += ($v->programados - $v->asistencia);
                $TotalCargas += $v->cargas;
                $TotalPorCargar += ($v->asistencia - $v->cargas);
            }
        
            if($TotalProg == 0){
                $porcencarga = (($TotalCargas) / 1 );
            }else{
            $porcencarga = (($TotalCargas) / ($TotalProg));
            }
            $porcentajecar = number_format(($porcencarga * 100), 2, ',', ' ');
        
        return view('aplicacion.cargasDistrito')->with('cargasDistritos',$cargasDistritos)->with('totalpro', $TotalProg)->with('totalasis', $TotalAsis)->with('TotalAusent',$TotalAusent)->with('totalcargas', $TotalCargas)->with('porcentajecar', $porcentajecar)->with('TotalPorCargar',$TotalPorCargar);                        
        
            }
        }
        
        public function monitor_aplicacion(Request $request, $tp_reporte, $id_periodo, $fch ="", $sesion="") {
                
        if($fch == 0){$fecha = ''; }else{ $fecha = "AND fecha_programada_inicio='$fch'";}        
        if( $sesion ==""){$sesion = '';}else{$sesion = "AND sesion='$sesion'";}    
                        
        $zona= $request->zona_d;        
         foreach ($zona as $zona){
            $val_z = "'".$zona."'";
            $zona_aux[] = $val_z;
        }
        $zonas = implode(',', $zona_aux); // separar los valores con coma(,).
        $sql_zona = "(" .$zonas. ")";
        
        /// LLAMADAS A LOS APLICADOSRES, PILOTO 24/02/2017
        if($tp_reporte == "llamadas_aplicadores"){            
            
        $reporteAplicadormont = \DB::select("select monitor, name_monitor, distrito_id, provincia, coordinador, zona, count(codigoamei) as instituciones, u.id_sede as laboratorio,
count(case when ap.estadollamada_apli ='CONTACTADO' then 'CONTACTADO' end) as contactado,
count(case when ap.estadollamada_apli ='NO CONTACTADO' then 'NO CONTACTADO' end) as nocontactado,
count(case when ap.estadollamada_apli ='' then 'EN PROCESO' when ap.estadollamada_apli is null then 'EN PROCESO' end) as enproceso,
count(case when ap.novedadllamada_apli !='' then ap.novedadllamada_apli end) as novedadllamada
from (select distrito_id, provincia, monitor, name_monitor, coordinador, zona, codigoamei, id_sede, sesion from umonitoreo where cgi_periodo_id = $id_periodo AND zona IN $sql_zona $fecha $sesion and estado=1 
group by distrito_id, provincia, monitor, name_monitor, coordinador, zona, codigoamei, id_sede, sesion) as  u, sig_aplicadores ap 
where ap.amie=u.codigoamei and ap.cgi_periodo_id=104
group by u.distrito_id, u.provincia, u.monitor, u.name_monitor, u.coordinador, u.zona, u.id_sede order by u.distrito_id, u.provincia, u.monitor, u.name_monitor, u.coordinador, u.zona, u.id_sede");  
           
        $TotalLab = 0.00;        
        $TotalInst = 0.00;        
        $TotalContactado = 0.00;
        $TotalNoContactado = 0.00;
        $TotalEnProceso = 0.00;
        $TotalNovedad = 0.00;        
        
            foreach ($reporteAplicadormont as $v) {
                $TotalLab += $v->laboratorio;                
                $TotalInst += $v->instituciones;                
                $TotalContactado += $v->contactado;
                $TotalNoContactado += $v->nocontactado;
                $TotalEnProceso += $v->enproceso;
                $TotalNovedad += $v->novedadllamada;           
            }              

        return view('aplicadores_aplicacion.aplicadorMonitor')->with('idperiodo',$id_periodo)->with('reporteAplicadormont',$reporteAplicadormont)->with('TotalLab',$TotalLab)->with('TotalInst',$TotalInst)->with('TotalContactado',$TotalContactado)->with('TotalNoContactado',$TotalNoContactado)->with('TotalEnProceso',$TotalEnProceso)->with('TotalNovedad',$TotalNovedad);
    
        //// Descarga de aplicativo, Instalacion e Inicio de Sesión.
        }elseif($tp_reporte == "inicio_aplicacion"){
            
            $distrito= $request->distrito;        
         foreach ($distrito as $distrito){
            $val_dist = "'".$distrito."'";
            $distrito_aux[] = $val_dist;
        }
        $distritos = implode(',', $distrito_aux); // separar los valores con coma(,).
        $sql_distrito = "(" .$distritos. ")";
        
        $provincia= $request->provincia_d;        
         foreach ($provincia as $provincia){
            $val_prov = "'".$provincia."'";
            $provincia_aux[] = $val_prov;
        }
        $provincias = implode(',', $provincia_aux); // separar los valores con coma(,).
        $sql_provincia = "(" .$provincias. ")";
                
        $coordinador_p= $request->coordinador_d;        
        if($coordinador_p == ''){
         $sql_coordinador ='';   
        }else{                    
         foreach ($coordinador_p as $coordinador_p){
            $val_p = "'".$coordinador_p."'";
            $coordinador_aux[] = $val_p;
        }        
        $coordinadores = implode(',', $coordinador_aux); // separar los valores con coma(,).
        $sql_coordinador = "(" .$coordinadores. ")";
        }        
        $coord="AND coordinador IN $sql_coordinador";
        
        $reportemonitor = \DB::select("SELECT distrito_id, provincia, zona, coordinador, monitor, name_monitor, count(cgi_laboratorio_id) as laboratorio, 
	count(case when descarga_aplicativo ='' then 'NO' when descarga_aplicativo is null then 'NO' when descarga_aplicativo ='NO' then 'NO' end) as descargano,
	count(case when descarga_aplicativo ='SI' then 'SI' end) as descargasi,
	count(case when instalo_aplicativo ='' then 'NO' when instalo_aplicativo is null then 'NO' when instalo_aplicativo ='NO' then 'NO' end) as instalono, 
	count(case when instalo_aplicativo ='SI' then 'SI' end) as instalosi,
	count(case when estado_sesion ='' then 'NO' when estado_sesion is null then 'NO' when estado_sesion ='NO' then 'NO' end) as sesionno,
	count(case when estado_sesion ='SI' then 'SI' end) as sesionsi,
	count(case when estadollamada_apli ='CONTACTADO' then 'CONTACTADO' end) as contactado,
	count(case when estadollamada_apli ='NO CONTACTADO' then 'NO CONTACTADO' end) as nocontactado,
	count(case when estadollamada_apli ='' then 'EN PROCESO' when estadollamada_apli is null then 'EN PROCESO' end) as enproceso,
	count(case when novedadllamada_apli !='' then novedadllamada_apli end) as novedadllamada
        FROM umonitoreo WHERE cgi_periodo_id=$id_periodo AND estado=1 AND zona IN $sql_zona AND distrito_id IN $sql_distrito AND provincia IN $sql_provincia $coord $fecha $sesion 
        GROUP BY distrito_id, provincia, coordinador, monitor, name_monitor, zona order by name_monitor, monitor, distrito_id, provincia, coordinador, zona, laboratorio");
           
        $TotalLab = 0.00;
        $TotalDescargosi = 0.00;
        $TotalDescargono = 0.00;
        $TotalInstalosi = 0.00;
        $TotalInstalono = 0.00;
        $TotalSesionsi = 0.00;
        $TotalSesionno = 0.00;
        $TotalContactado = 0.00;
        $TotalNoContactado = 0.00;
        $TotalEnProceso = 0.00;
        $TotalNovedad = 0.00;        
        
            foreach ($reportemonitor as $v) {
                $TotalLab += $v->laboratorio;
                $TotalDescargosi += $v->descargasi;
                $TotalDescargono += $v->descargano;
                $TotalInstalosi += $v->instalosi;
                $TotalInstalono += $v->instalono;
                $TotalSesionsi += $v->sesionsi;
                $TotalSesionno += $v->sesionno;
                $TotalContactado += $v->contactado;
                $TotalNoContactado += $v->nocontactado;
                $TotalEnProceso += $v->enproceso;
                $TotalNovedad += $v->novedadllamada;           
            }       
                    
        return view('aplicacion.aplicacionMonitor')->with('idperiodo',$id_periodo)->with('fecha',$fch)->with('sesion',$sesion)->with('monitores', $reportemonitor)->with('TotalLab',$TotalLab)->with('TotalDescargosi',$TotalDescargosi)->with('TotalDescargono',$TotalDescargono)->with('TotalInstalosi',$TotalInstalosi)->with('TotalInstalono',$TotalInstalono)->with('TotalSesionsi',$TotalSesionsi)->with('TotalSesionno',$TotalSesionno)->with('TotalContactado',$TotalContactado)->with('TotalNoContactado',$TotalNoContactado)->with('TotalEnProceso',$TotalEnProceso)->with('TotalNovedad',$TotalNovedad);
        
        /// CARGA DE ARCHIVOS
        }elseif($tp_reporte == "fin_aplicacion"){
            
            $distrito= $request->distrito;        
         foreach ($distrito as $distrito){
            $val_dist = "'".$distrito."'";
            $distrito_aux[] = $val_dist;
        }
        $distritos = implode(',', $distrito_aux); // separar los valores con coma(,).
        $sql_distrito = "(" .$distritos. ")";
        
        $provincia= $request->provincia_d;        
         foreach ($provincia as $provincia){
            $val_prov = "'".$provincia."'";
            $provincia_aux[] = $val_prov;
        }
        $provincias = implode(',', $provincia_aux); // separar los valores con coma(,).
        $sql_provincia = "(" .$provincias. ")";
                
        $coordinador_p= $request->coordinador_d;        
        if($coordinador_p == ''){
         $sql_coordinador ='';   
        }else{                    
         foreach ($coordinador_p as $coordinador_p){
            $val_p = "'".$coordinador_p."'";
            $coordinador_aux[] = $val_p;
        }        
        $coordinadores = implode(',', $coordinador_aux); // separar los valores con coma(,).
        $sql_coordinador = "(" .$coordinadores. ")";
        }        
        $coord="AND coordinador IN $sql_coordinador";
            
        $cargasMonitor = \DB::select("select a.monitor, a.name_monitor, a.distrito_id, a.provincia, a.zona, a.coordinador, count(a.laboratorio) as laboratorio, SUM(a.programados) as programados, sum(a.asistencia) as asistencia, sum(a.cargas) as cargas
        from (SELECT monitor, name_monitor, distrito_id, provincia, zona, coordinador, count(cgi_laboratorio_id) as laboratorio, SUM(programados) as programados, sum(asistencia) as asistencia, sum(cargas) as cargas 
        FROM umonitoreo WHERE cgi_periodo_id =$id_periodo AND estado=1 $fecha $sesion $coord AND zona IN $sql_zona AND provincia IN $sql_provincia AND distrito_id IN $sql_distrito AND created_at is null 
        group by monitor, name_monitor, distrito_id, provincia, zona, coordinador, cgi_laboratorio_id order by monitor, name_monitor, distrito_id, provincia, zona, coordinador) as a 
        GROUP BY a.monitor, a.name_monitor, a.distrito_id, a.provincia, a.zona, a.coordinador order by a.monitor, a.name_monitor, a.distrito_id, a.provincia, a.zona, a.coordinador");
            
        $TotalProg = 0.00;
        $TotalAsis = 0.00;
        $TotalAusent = 0.00;
        $TotalCargas = 0.00;
        $TotalPorCargar= 0.00;
        
            foreach ($cargasMonitor as $v) {
                $TotalProg += $v->programados;
                $TotalAsis += $v->asistencia;
                $TotalAusent += ($v->programados - $v->asistencia);
                $TotalCargas += $v->cargas;
                $TotalPorCargar += ($v->asistencia - $v->cargas);
            }
        
            if($TotalProg == 0){
                $porcencarga = (($TotalCargas) / 1 );
            }else{
            $porcencarga = (($TotalCargas) / ($TotalProg));
            }
            $porcentajecar = number_format(($porcencarga * 100), 2, ',', ' ');
        
        return view('aplicacion.cargasMonitor')->with('cargasMonitor',$cargasMonitor)->with('totalpro', $TotalProg)->with('totalasis', $TotalAsis)->with('TotalAusent',$TotalAusent)->with('totalcargas', $TotalCargas)->with('porcentajecar', $porcentajecar)->with('TotalPorCargar',$TotalPorCargar);                        
               
        }
        }
        
        public function laboratorio_aplicacion(Request $request, $tp_reporte, $id_periodo, $fch ="", $sesion="") {
        
        if($fch == 0){$fecha = '';}else{$fecha = "AND fecha_programada_inicio='$fch'";}
        if( $sesion ==""){$sesion = '';}else{$sesion = "AND sesion='$sesion'";}       
        
        $monitor= $request->monitor;        
        foreach ($monitor as $monitor){
        $val_m = "'".$monitor."'";
        $monitor_aux[] = $val_m;
        }
        $monitores = implode(',', $monitor_aux); // separar los valores con coma(,).
        $sql_monitor = "(" .$monitores. ")";
        
        $distrito= $request->distrito_m;        
         foreach ($distrito as $distrito){
            $val_dist = "'".$distrito."'";
            $distrito_aux[] = $val_dist;
        }
        $distritos = implode(',', $distrito_aux); // separar los valores con coma(,).
        $sql_distrito = "(" .$distritos. ")";
        
        $provincia= $request->provincia_m;        
         foreach ($provincia as $provincia){
            $val_prov = "'".$provincia."'";
            $provincia_aux[] = $val_prov;
        }
        $provincias = implode(',', $provincia_aux); // separar los valores con coma(,).
        $sql_provincia = "(" .$provincias. ")";
        
        $coordinador_m= $request->coordinador_m;        
        if($coordinador_m == ''){
         $sql_coordinador ='';   
        }else{                    
         foreach ($coordinador_m as $coordinador_m){
            $val_m = "'".$coordinador_m."'";
            $coordinador_aux[] = $val_m;
        }        
        $coordinadores = implode(',', $coordinador_aux); // separar los valores con coma(,).
        $sql_coordinador = "(" .$coordinadores. ")";
        }        
        $coord="AND coordinador IN $sql_coordinador";
                      
        $zona_m= $request->zona_m;            
        foreach ($zona_m as $zona_m){
        $val_m = "'".$zona_m."'";
        $zona_aux[] = $val_m;
        }
        $zonas = implode(',', $zona_aux); // separar los valores con coma(,).
        $sql_zonas = "(" .$zonas. ")";        
        
        // LLAMADAS A APLICADORES, PILOTO 24/02/207 
        if($tp_reporte == "llamadas_aplicadores"){
            
        $reportelabapli = \DB::select("select distrito_id, provincia, coordinador, monitor, name_monitor, zona, institucion, codigoamei as instituciones, u.id_sede as laboratorios, u.sesion,
        (case when ap.estadollamada_apli ='CONTACTADO' then 'CONTACTADO' when ap.estadollamada_apli ='NO CONTACTADO' then 'NO CONTACTADO'  else 'EN PROCESO' end) as estadollamada,
        (case when ap.novedadllamada_apli !='' then ap.novedadllamada_apli end) as novedadllamada, ap.observaciones
        from (select distrito_id, provincia, coordinador, monitor, name_monitor, zona, institucion, codigoamei, id_sede, sesion from umonitoreo 
        where cgi_periodo_id = $id_periodo $coord AND zona IN $sql_zonas and estado=1 AND monitor IN $sql_monitor $fecha $sesion 
        group by distrito_id, provincia, coordinador, monitor,  name_monitor, zona, institucion, codigoamei, id_sede, sesion) as  u, sig_aplicadores ap 
        where ap.amie=u.codigoamei and ap.cgi_periodo_id=104 order by u.zona, u.coordinador, u.name_monitor, u.monitor, u.institucion, u.codigoamei, u.sesion");    
        
        return view('aplicadores_aplicacion.aplicadorLaboratorio')->with('idperiodo',$id_periodo)->with('reportelabapli',$reportelabapli);
                
        //// Descarga de aplicativo, Instalacion e Inicio de Sesión.
        }elseif($tp_reporte == "inicio_aplicacion"){
            
        $reportelabmonitor = \DB::select("SELECT monitor, name_monitor, distrito_id, provincia, zona, coordinador, institucion, codigoamei as amie, id_sede as sede_laboratorio, cgi_laboratorio_id as laboratorio, sesion, observacion_sesion,
        (case when descarga_aplicativo='' then 'NO' when descarga_aplicativo is null then 'NO' else descarga_aplicativo end) as descarga,
        (case when instalo_aplicativo='' then 'NO' when instalo_aplicativo is null then 'NO' else instalo_aplicativo end) as instalo, 
        (case when estado_sesion='' then'NO' when estado_sesion is null then 'NO' else estado_sesion end) as estadosesion,
        (case when estadollamada_apli ='CONTACTADO' then 'CONTACTADO' when estadollamada_apli ='NO CONTACTADO' then 'NO CONTACTADO'  else 'POR CONTACTAR' end) as estadollamada,
	(case when novedadllamada_apli is not null then novedadllamada_apli end) as novedadllamada 
        from umonitoreo WHERE cgi_periodo_id=$id_periodo $coord  AND monitor IN $sql_monitor AND distrito_id IN $sql_distrito AND provincia IN $sql_provincia AND zona IN $sql_zonas AND estado=1 $fecha $sesion
        ORDER BY name_monitor, monitor, distrito_id, provincia, zona, coordinador, codigoamei, institucion, cgi_laboratorio_id, sesion");
                            
        return view('aplicacion.aplicacionLaboratorio')->with('idperiodo',$id_periodo)->with('fecha',$fch)->with('sesion',$sesion)->with('labMonitor', $reportelabmonitor);
        
        /// CARGA DE ARCHIVOS
        }elseif($tp_reporte == "fin_aplicacion"){            
               
        $reportecarg_labmont = \DB::select("select a.monitor, a.name_monitor, a.distrito_id, a.provincia, a.zona, a.coordinador, a.institucion, a.amie, a.sede_laboratorio, a.laboratorio, SUM(a.programados) as programados, sum(a.asistencia) as asistencia, sum(a.cargas) as cargas
        from (SELECT monitor, name_monitor, distrito_id, provincia, zona, coordinador, institucion, codigoamei as amie, id_sede as sede_laboratorio, cgi_laboratorio_id as laboratorio, SUM(programados) as programados, sum(asistencia) as asistencia, sum(cargas) as cargas
        FROM umonitoreo WHERE cgi_periodo_id=$id_periodo $coord  AND monitor IN $sql_monitor AND distrito_id IN $sql_distrito AND provincia IN $sql_provincia AND zona IN $sql_zonas AND estado=1 $fecha $sesion
        AND estado=1 AND created_at is null group by monitor, name_monitor, distrito_id, provincia, zona, coordinador, institucion, codigoamei, id_sede,cgi_laboratorio_id order by zona, monitor, name_monitor, id_sede, cgi_laboratorio_id) as a 
        GROUP BY a.monitor, a.name_monitor, a.distrito_id, a.provincia, a.zona, a.coordinador, a.institucion, a.amie, a.sede_laboratorio, a.laboratorio order by a.monitor, a.name_monitor, a.distrito_id, a.provincia, a.zona, a.coordinador, a.institucion, a.amie, a.sede_laboratorio");
               
        $TotalProg = 0.00;
        $TotalAsis = 0.00;
        $TotalAusent = 0.00;
        $TotalCargas = 0.00;
        $TotalPorCargar= 0.00;
        
        foreach ($reportecarg_labmont as $v) {
        $TotalProg += $v->programados;
        $TotalAsis += $v->asistencia;
        $TotalAusent += ($v->programados - $v->asistencia);
        $TotalCargas += $v->cargas;
        $TotalPorCargar += ($v->asistencia - $v->cargas);
        }
        
        if($TotalProg == 0){
        $porcencarga = (($TotalCargas) / 1 );
        }else{
        $porcencarga = (($TotalCargas) / ($TotalProg));
        }
        $porcentajecar = number_format(($porcencarga * 100), 2, ',', ' ');
        
        return view('aplicacion.cargasLaboratorio')->with('labcargMonitor',$reportecarg_labmont)->with('totalpro', $TotalProg)->with('totalasis', $TotalAsis)->with('TotalAusent',$TotalAusent)->with('totalcargas', $TotalCargas)->with('porcentajecar', $porcentajecar)->with('TotalPorCargar', $TotalPorCargar);                        
        }
        }
        
        public function sustentante_aplicacion(Request $request, $tp_reporte, $id_periodo, $fch ="", $sesion="") {
        
        if($fch == 0){$fecha = '';}else{$fecha = "AND substring(cast(fecha_programada_inicio as text),1,10)='$fch'";}
        if( $sesion ==""){$sesion = '';}else{$sesion = "and substring(cast(fecha_programada_inicio as text),1,10) || '-' || sesion='$sesion'";}       
        
        $laboratorio= $request->laboratorio;        
        foreach ($laboratorio as $lab){
        $val_lab = "'".$lab."'";
        $lab_aux[] = $val_lab;
        }
        $laboratorios = implode(',', $lab_aux); // separar los valores con coma(,).
        $sql_laboratorios = "(" .$laboratorios. ")";
                 
        /// CARGA DE ARCHIVOS
        if($tp_reporte == "fin_aplicacion"){            
            $sustentantes = \DB::select("select id as codigo_sustentante, cedula, apellidos, nombres, reprogramacion, asistencia, asignatura, sustentante_observacion as observacion, 
            estado as estado_carga, cgi_periodo_id, cgi_laboratorio_id, substring(cast(fecha_programada_inicio as text),1,10) AS fecha_evaluacion, sesion as sesion_evaluacion
            from view_sustensesionlab_otielnet_asig where cgi_laboratorio_id in $sql_laboratorios $fecha $sesion ORDER BY apellidos, nombres, fecha_evaluacion, sesion");
            
        return view('aplicacion.cargasSustentante')->with('sustentantes',$sustentantes);
        }
        }
    
}