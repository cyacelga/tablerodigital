<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\CgiPeriodo;

class ProcesoController extends Controller
{
   
    public function __construct()
	{
		$this->middleware('auth');
	}

    public function index()
    {
    	$periodoIniciar = \DB::select("select id, periodo from cgi_periodos where estado='A' order by 1");
        return view("formularios.form_iniciar_proceso")->with("periodoIniciar", $periodoIniciar);
    }
}
