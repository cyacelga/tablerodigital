<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Maatwebsite\Excel\Facades\Excel;

class ExportarborrarController extends Controller
{
        
     public function exportar_info_aplicacion_borrar($id)
    {
        
         $export_info_apli = \DB::select("select codigoamei, zona, institucion, fecha_programada_inicio, sesion, id_sede, distrito_id, provincia, monitor, name_monitor, coordinador, programados, 
cargas, asistencia, reprogramacion, estado_sesion, sesion_aplicador as asistencia_responsable, observacion_sesion,recepcion_aplicativo, descarga_aplicativo, 
observacion_descarga, instalo_aplicativo, comp_inst as maquinas_instaladas, (programados - comp_inst) as maquinas_faltantes, observacion_instala, internet, presentes,
impresora, recepcion_clave, asistencia_observador, poblacion, listado_asistencia, observacion_listado_asistencia, listado_validacion, estadollamada_apli, novedadllamada_apli from umonitoreo where cgi_periodo_id = $id and estado=1");    
                
    Excel::create('DACT_APLICACION', function($excel) use ($export_info_apli) {
        
         $excel->sheet('DD', function($sheet){
       
        // Header
        //$sheet->mergeCells('A1:B1');        
        $sheet->row(1, ['CAMPO','DESCRIPCIÓN']);
        $sheet->row(2, ['CÓDIGO AMIE','CÓDIGO PERTENECIENTE A LA INSTITUCIÓN EDUCATIVA']);
        $sheet->row(3, ['INSTITUCIÓN','NOMBRE DE LA INSTITUCIÓN EDUCATIVA']);
        $sheet->row(4, ['COORDINADOR','COORDINADOR ZONAL ENCARGADO DE TENER COMUNICACIÓN CON LA INSTITUCIÓN']);
        $sheet->row(5, ['CÓDIGO MONITOR','CÓDIGO MONITOR']);
        $sheet->row(6, ['NOMBRE MONITOR','NOMBRE DEL MONITOR ENCARGADO DE TENER COMUNICACIÓN CON LA INSTITUCIÓN']);
        $sheet->row(7, ['ZONA','ZONA A LA QUE PERTENECE LA INSTITUCIÓN EDUCATIVA']);
        $sheet->row(8, ['PROVINCIA','PROVINCIA A LA QUE PERTENECE LA INSTITUCIÓN']);
        $sheet->row(9, ['DISTRITO','DISTRITO AL QUE PERTENECE LA INSTITUCIÓN']);
        $sheet->row(10, ['FECHA PROGRAMADA','FECHA PROGRAMADA DE LA EVALUACIÓN']);
        $sheet->row(11, ['SESIÓN','SESIÓN DE LA EVALUACIÓN']);
        $sheet->row(12, ['PROGRAMADOS','TOTAL DE SUSTENTANTES A EVALUAR']);
        $sheet->row(13, ['CARGAS','TOTAL DE ARCHIVOS CARGADOS EN LA PLATAFORMA CRM']);
        $sheet->row(14, ['ASISTENCIA','TOTAL DE SUSTENTANTES QUE ASISTIERON A LA EVALUACIÓN']);
        $sheet->row(15, ['REPROGRAMACIÓN','TOTAL DE SUSTENTANTES REPROGRAMADOS']);
        $sheet->row(16, ['RECIBIÓ APLICATIVO','CONFIRMACIÓN SI EL RESPONSABLE DE SEDE RECIBIÓ EL APLICATIVO']);
        $sheet->row(17, ['DESCARGÓ APLICATIVO','CONFIRMACIÓN SI EL RESPONSABLE DE SEDE DESCARGÓ EL APLICATIVO']);
        $sheet->row(18, ['OBSERVACIÓN DESCARGA','OBSERVACIÓN SI OCURRIO UN PROBLEMA CON LA DESCARGA']);
        $sheet->row(19, ['INSTALÓ APLICATIVO','CONFIRMACIÓN SI EL APLICATIVO FUE INSTALADO']);
        $sheet->row(20, ['OBSERVACIÓN INSTALACIÓN','OBSERVACIÓN PORQUE NO SE PUDO INSTALAR EL APLICATIVO']);
        $sheet->row(21, ['MAQUINAS INSTALADAS','CANTIDAD DE MAQUINAS EN LA QUE FUERON INSTALADAS EL APLICATIVO']);
        $sheet->row(22, ['MAQUINAS FALTANTES','CANTIDAD DE MAQUINAS FALTANTES POR INSTALAR EL APLICATIVO']);
        $sheet->row(23, ['INICIÓ SESIÓN','INICIO DE LA EVALUACIÓN EN LA SESIÓN CORRESPONDIENTE']);
        $sheet->row(24, ['OBSERVACIÓN SESIÓN','OBSERVACIÓN DE PORQUE NO SE INICIÓ LA EVALUACIÓN']);
        $sheet->row(25, ['PRESENTES','SUSTENTANTES PRESENTES EN LA APLICACIÓN']);
        $sheet->row(26, ['INTERNET','SI LA INSTITUCIÓN CUENTA CON INTERNET']);
        $sheet->row(27, ['IMPRESORA','SI LA INSTITUCIÓN CUENTA CON IMPRESORA']);
        $sheet->row(28, ['RECEPCIÓN DE CLAVE','CONFIRMACIÓN DE RECEPCIÓN DE CLAVES']);
        $sheet->row(29, ['ASISTENCIA RESPONSABLE SEDE','ASISTENCIA RESPONSABLE DE SEDE']);
        $sheet->row(30, ['ASISTENCIA OBSERVADOR','ASISTENCIA DEL OBSERVADOR']);
        $sheet->row(31, ['POBLACIÓN','POBLACIÓN ESCOLAR / NO ESCOLAR']);
        $sheet->row(32, ['LISTADO ASISTENCIA','SÍ EL RESPONSABLE SEDE RECIBIÓ EL LISTADO']);
        $sheet->row(33, ['OBSERVACIÓN LISTADO ASISTENCIA','SÍ EL RESPONSABLE SEDE REPORTA OBSERVACIÓN DEL LISTADO']);
        $sheet->row(34, ['LISTADO VALIDACIÓN','VERIFICACIÓN DE LISTADOS']);
        $sheet->row(35, ['ESTADO DE LLAMADA','ESTADO DE LLAMADA']);
        $sheet->row(36, ['PROBLEMA DE LLAMADA','PROBLEMA DE LLAMADA']);


            
        $sheet->cells('A1:B1', function($cells){
            $cells->setBackground('#0489B1');
            $cells->setFontColor('#FFFFFF');
            $cells->setAlignment('center');
            $cells->setValignment('center');
            $cells->setFontweight('bold');
        });
                 
        });
        
        $excel->sheet('Datos', function($sheet) use ($export_info_apli) {
       
          $sheet->row(1, ['CÓDIGO AMIE', 'INSTITUCIÓN',	'COORDINADOR', 'CÓDIGO MONITOR', 'NOMBRE MONITOR', 'ZONA', 'PROVINCIA', 'DISTRITO', 'FECHA PROGRAMADA',	'SESIÓN', 'PROGRAMADOS',
          'CARGAS', 'ASISTENCIA', 'REPROGRAMACIÓN', 'RECIBIÓ APLICATIVO', 'DESCARGÓ APLICATIVO', 'OBSERVACIÓN DESCARGA', 'INSTALÓ APLICATIVO', 'OBSERVACIÓN INSTALACIÓN', 'MAQUINAS INSTALADAS',
          'MAQUINAS FALTANTES',	'INICIÓ SESIÓN', 'OBSERVACIÓN SESIÓN', 'PRESENTES', 'INTERNET', 'IMPRESORA', 'RECEPCIÓN DE CLAVE', 'ASISTENCIA RESPONSABLE SEDE', 'ASISTENCIA OBSERVADOR', 
          'POBLACIÓN','LISTADO ASISTENCIA', 'OBSERVACIÓN LISTADO ASISTENCIA', 'LISTADO VALIDACIÓN','ESTADO DE LLAMADA', 'PROBLEMA DE LLAMADA'
            ]);  
            
        $sheet->cells('A1:AI1', function($cells){
            $cells->setBackground('#0489B1');
            $cells->setFontColor('#FFFFFF');
            $cells->setAlignment('center');
            $cells->setValignment('center');
            $cells->setFontweight('bold');
        });
        
        // Data       
        $export_info_apli;
        
        foreach ($export_info_apli as $llamadas){
        
            $row = [];
            $row[0] = $llamadas->codigoamei;
            $row[1] = $llamadas->institucion;
            $row[2] = $llamadas->coordinador;
            $row[3] = $llamadas->monitor;
            $row[4] = $llamadas->name_monitor;
            $row[5] = $llamadas->zona;
            $row[6] = $llamadas->provincia;
            $row[7] = $llamadas->distrito_id;
            $row[8] = $llamadas->fecha_programada_inicio;
            $row[9] = $llamadas->sesion;
            $row[10] = $llamadas->programados;
            $row[11] = $llamadas->cargas;
            $row[12] = $llamadas->asistencia;
            $row[13] = $llamadas->reprogramacion;
            $row[14] = $llamadas->recepcion_aplicativo;
            $row[15] = $llamadas->descarga_aplicativo;
            $row[16] = $llamadas->observacion_descarga;
            $row[17] = $llamadas->instalo_aplicativo;
            $row[18] = $llamadas->observacion_instala;
            $row[19] = $llamadas->maquinas_instaladas;
            $row[20] = $llamadas->maquinas_faltantes;
            $row[21] = $llamadas->estado_sesion;
            $row[22] = $llamadas->observacion_sesion;
            $row[23] = $llamadas->presentes;
            $row[24] = $llamadas->internet;
            $row[25] = $llamadas->impresora;
            $row[26] = $llamadas->recepcion_clave;
            $row[27] = $llamadas->asistencia_responsable;
            $row[28] = $llamadas->asistencia_observador;
            $row[29] = $llamadas->poblacion;
            $row[30] = $llamadas->listado_asistencia;
            $row[31] = $llamadas->observacion_listado_asistencia;
            $row[32] = $llamadas->listado_validacion;
            $row[33] = $llamadas->estadollamada_apli;
            $row[34] = $llamadas->novedadllamada_apli;

           
            
            $sheet->appendRow($row);
            }                
        });       
        
    })->export('xlsx');
    
    }
    
}