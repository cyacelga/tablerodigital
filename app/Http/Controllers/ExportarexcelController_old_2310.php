<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Maatwebsite\Excel\Facades\Excel;

class ExportarexcelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function exportar_exceluno($id)
    {      
        Excel::create('DACT_VALIDACION_LABORATORIOS', function($excel) use ($id) {

        $excel->sheet('SEDES_CON_LABORATORIOS', function($sheet) use ($id) {
        
        // Header
       // $sheet->mergeCells('A1:F1'); 
        //$sheet->row(1, ['Reporte de Prueba Uno']);
        $sheet->row(1, ['AMIE', 'PERIODO', 'MONITOR', 'INSTITUCION', 'ZONA', 'PROVINCIA', 'CANTON', 'PARROQUIA', 'DISTRITO', 'MAQUINAS VALIDADAS',
        'IMPRESORA', 'INTERNET', 'SISTEMA', 'OBSERVACIÓN', 'MAQUINAS REQUERIDAS', 'LABORATORIO','ESTADO DE LLAMADA','PROBLEMA DE LA LLAMADA','DICTAMEN','LABORATORIO NUEVO',
        'RESPONSABLE DE SEDE 1', 'CELULAR 1', 'WHATSAPP 1', 'CELULAR 2', 'WHATSAPP 2', 'TELÉFONO 1', 'TELÉFONO 2', 'TELÉFONO 3', 'CORREO ELECTRÓNICO 1', 'CORREO ELECTRÓNICO 2', 
        'MOTIVO INASISTENCIA', 'COMUNICACIÓN RÁPIDA', 'RESPONSABLE DE SEDE 2', 'CELULAR 1 SPTE', 'WHATSAPP 1 SPTE', 'CELULAR 2 SPTE', 'WHATSAPP 2 SPTE', 'TELÉFONO 1 SPTE', 'TELÉFONO 2 SPTE', 
        /* 'TELÉFONO 3 SPTE',*/ 'CORREO ELECTRÓNICO 1 SPTE', 'CORREO ELECTRÓNICO 2 SPTE', 'MOTIVO INASISTENCIA SPTE', 'COMUNICACIÓN RÁPIDA SPTE', 'OBSERVADOR', 'CELULAR OBSERVADOR', 
        'TELÉFONO OBSERVADOR', 'CORREO OBSERVADOR', 'RECEPCIÓN CORREO', 'CAPACITACIÓN', 'DESCARGA', 'INSTALACIÓN', 'IMPRESIÓN MATERIAL', 'TIPO APLICACIÓN', 'AUDITIVA', 'VISUAL', 'OBSERVACIÓN INSTALACIÓN'
            ]);
        
        $sheet->cells('A1:BD1', function($cells){
            $cells->setBackground('#0489B1');
            $cells->setFontColor('#FFFFFF');
            $cells->setAlignment('center');
            $cells->setValignment('center');
            $cells->setFontweight('bold');
        });
        
        // Data            
        $reporteexcel = \DB::select("select (case when l.maxasignada<=l.computadorc and l.impresorac='SI'  then 'OPTIMO'
                   when l.maxasignada<=l.computadorc then 'RECOMENDABLE' 
                   when l.computadorc=0 and l.impresorac ='' and l.internetc ='' then 'POR CONTACTAR'
                   when l.computadorc is not null or l.impresorac is not null or l.internetc is not null then 'POR GESTIONAR'                   
                   when l.computadorc is null and l.impresorac is null and l.internetc is null then 'POR CONTACTAR'
                    ELSE 'POR CONTACTAR' end) as dictamen, l.amie, l.proceso, s.monitor, s.institucion, s.zona, s.provincia, s.canton, s.parroquia, s.distrito, l.computadorc, l.impresorac, l.internetc, l.sistemac, l.observacion, 
                    l.maxasignada as requeridas, l.id_sede as laboratorio, l.visita, s.estadollamada as estadollamadarector, s.novedadllamada as novedadllamadarector,
                    (case when l.created_at is not null then '1' else '0' end) as lab_nuevo,
                    s.rector, s.celular, s.celular1_wa, s.celular_whatsapp, s.celular2_wa, s.telefono1, s.telefono2, s.contactoinstitucion as telefono3, s.correo, s.correoinstitucion, s.motivo1, s.comunicacion,
                    s.rector_suplente, s.celular_suplente, s.celular1_swa, s.celular_whatsapp_suplente, s.celular2_swa, s.telefono1_suplente, s.telefono2_suplente, s.contactoinstitucion_suplente as telefono3_suplente, 
                    s.correo_suplente, s.correoinstitucion_suplente, s.motivo2, s.comunicacion_suplente, s.observador, s.celular_observador, s.telefono_observador, s.correo_observador, s.recibiocomunicado, s.capacitacion, 
                    s.descarga, s.instalacion, s.impresionaplicacion as impresion_material,  s.tipo_aplicacion, s.auditiva, s.visual, s.observacion_instala
                    from laboratories l, sig_institucionv s where l.proceso = $id AND s.estado=1
                    and s.amie=l.amie and s.proceso=l.proceso");
        
        foreach ($reporteexcel as $exportuno){
        
            $row = [];
            $row[0] = $exportuno->amie;
            $row[1] = $exportuno->proceso;
            $row[2] = $exportuno->monitor;
            $row[3] = $exportuno->institucion;
            $row[4] = $exportuno->zona;
            $row[5] = $exportuno->provincia;
            $row[6] = $exportuno->canton;
            $row[7] = $exportuno->parroquia;
            $row[8] = $exportuno->distrito;
            $row[9] = $exportuno->computadorc;
            $row[10] = $exportuno->impresorac;
            $row[11] = $exportuno->internetc;
            $row[12] = $exportuno->sistemac;
            $row[13] = $exportuno->observacion;
            $row[14] = $exportuno->requeridas;
            $row[15] = $exportuno->laboratorio;
            $row[16] = $exportuno->estadollamadarector;
            $row[17] = $exportuno->novedadllamadarector;
            $row[18] = $exportuno->dictamen;
            $row[19] = $exportuno->lab_nuevo; 
            $row[20] = $exportuno->rector; 
            $row[21] = $exportuno->celular; 
            $row[22] = $exportuno->celular1_wa; 
            $row[23] = $exportuno->celular_whatsapp; 
            $row[24] = $exportuno->celular2_wa; 
            $row[25] = $exportuno->telefono1; 
            $row[26] = $exportuno->telefono2; 
            $row[27] = $exportuno->telefono3; 
            $row[28] = $exportuno->correo; 
            $row[29] = $exportuno->correoinstitucion; 
            $row[30] = $exportuno->motivo1;
            $row[31] = $exportuno->comunicacion;            
            $row[32] = $exportuno->rector_suplente;
            $row[33] = $exportuno->celular_suplente;
            $row[34] = $exportuno->celular1_swa;
            $row[35] = $exportuno->celular_whatsapp_suplente;
            $row[36] = $exportuno->celular2_swa;
            $row[37] = $exportuno->telefono1_suplente;
            //$row[38] = $exportuno->telefono2_suplente;
            $row[38] = $exportuno->telefono3_suplente;            
            $row[39] = $exportuno->correo_suplente;
            $row[40] = $exportuno->correoinstitucion_suplente;
            $row[41] = $exportuno->motivo2;
            $row[42] = $exportuno->comunicacion_suplente;
            $row[43] = $exportuno->observador;
            $row[44] = $exportuno->celular_observador;
            $row[45] = $exportuno->telefono_observador;
            $row[46] = $exportuno->correo_observador;
            $row[47] = $exportuno->recibiocomunicado;
            $row[48] = $exportuno->capacitacion;
            $row[49] = $exportuno->descarga;
            $row[50] = $exportuno->instalacion;
            $row[51] = $exportuno->impresion_material;
            $row[52] = $exportuno->tipo_aplicacion;
            $row[53] = $exportuno->auditiva;
            $row[54] = $exportuno->visual;
            $row[55] = $exportuno->observacion_instala;
           
            $sheet->appendRow($row);
            }                
        });

    })->export('xlsx');
    
    }
    
    public function exportar_llamadas($id)
    {      
        Excel::create('DACT_LLAMADAS_RECTORES', function($excel) use ($id) {
            
        $excel->sheet('DD', function($sheet){
       
        // Header
        //$sheet->mergeCells('A1:B1');        
            $sheet->row(1, ['CAMPO','DESCRIPCIÓN']);
            $sheet->row(2, ['AMIE','CÓDIGO PERTENECIENTE A LA INSTITUCIÓN EDUCATIVA']);
            $sheet->row(3, ['MONITOR','CÓDIGO MONITOR']);
            $sheet->row(4, ['INSTITUCIÓN','NOMBRE DE LA INSTITUCIÓN EDUCATIVA']);
            $sheet->row(5, ['ZONA','ZONA A LA QUE PERTENECE LA INSTITUCIÓN EDUCATIVA']);
            $sheet->row(6, ['PROVINCIA','PROVINCIA A LA QUE PERTENECE LA INSTITUCIÓN']);
            $sheet->row(7, ['CANTÓN','CANTÓN A LA QUE PERTENECE LA INSTITUCIÓN']);
            $sheet->row(8, ['PARROQUIA','PARROQUIA A LA QUE PERTENECE LA INSTITUCIÓN']);
            $sheet->row(9, ['DISTRITO','DISTRITO A LA QUE PERTENECE LA INSTITUCIÓN']);
            $sheet->row(10, ['RESPONSABLE DE SEDE 1','NOMBRE DEL RESPONSABLE DE SEDE 1']);
            $sheet->row(11, ['CELULAR 1','NÚMERO CELULAR 1 RESPONSABLE DE SEDE 1']);
            //$sheet->row(12, ['WHATSAPP 1','SÍ EL CELULAR 1 CUENTA CON WHATSAPP']);
            $sheet->row(12, ['CELULAR 2','NÚMERO CELULAR 2 DEL RESPONSABLE DE SEDE 1']);
            //$sheet->row(14, ['WHATSAPP 2','SÍ EL CELULAR 2 CUENTA CON WHATSAPP']);
            $sheet->row(13, ['TELÉFONO 1','NÚMERO TELÉFONO FIJO PRINCIPAL']);
            $sheet->row(14, ['TELÉFONO 2','NÚMERO TELÉFONO FIJO  SECUNDARIO']);
            $sheet->row(15, ['TELÉFONO 3','NÚMERO TELÉFONO OPCIONAL']);
            $sheet->row(16, ['CORREO ELECTRÓNICO 1','CORREO ELECTRÓNICO PRINCIPAL']);
            $sheet->row(17, ['CORREO ELECTRÓNICO 2','CORREO ELECTRÓNICO SECUNDARIO']);
            /* $sheet->row(20, ['MOTIVO INASISTENCIA','MOTIVO POR EL CUÁL EL RESPONSABLE DE SEDE 1 NO ASISTIRÁ']);
            $sheet->row(21, ['COMUNICACIÓN RÁPIDA','MEDIO DE COMUNICACIÓN DEL RESPONSABLE DE SEDE 1']);
            $sheet->row(22, ['RESPONSABLE DE SEDE 2','NOMBRE DEL RESPONSABLE DE SEDE 2']);
            $sheet->row(23, ['CELULAR 1 SPTE','NÚMERO CELULAR 1 RESPONSABLE DE SEDE 2 (SUPLENTE)']);
            $sheet->row(24, ['WHATSAPP 1 SPTE','SÍ EL CELULAR 1 DEL SUPLENTE CUENTA CON WHATSAPP']);
            $sheet->row(25, ['CELULAR 2 SPTE','NÚMERO CELULAR 2 RESPONSABLE DE SEDE 2 (SUPLENTE)']);
            $sheet->row(26, ['WHATSAPP 2 SPTE','SÍ EL CELULAR 2 DEL SUPLENTE CUENTA CON WHATSAPP']);
            $sheet->row(27, ['TELÉFONO 1 SPTE','TELÉFONO FIJO PRINCIPAL, SUPLENTE']);
            $sheet->row(28, ['TELÉFONO 2 SPTE','TELÉFONO FIJO SECUNDARIO, SUPLENTE']);
            //$sheet->row(29, ['TELÉFONO 3 SPTE','NÚMERO TELÉFONO OPCIONAL, SUPLENTE']);
            $sheet->row(29, ['CORREO ELECTRÓNICO 1 SPTE','CORREO ELECTRÓNICO PRINCIPAL']);
            $sheet->row(30, ['CORREO ELECTRÓNICO 2 SPTE','CORREO ELECTRÓNICO SECUNDARIO']);
            $sheet->row(31, ['MOTIVO INASISTENCIA SPTE','MOTIVO POR EL CUÁL EL RESPONSABLE DE SEDE 2 NO ASISTIRÁ']);
            $sheet->row(32, ['COMUNICACIÓN RÁPIDA SPTE','MEDIO DE COMUNICACIÓN DEL SUPLENTE']);  
            $sheet->row(33, ['OBSERVADOR','NOMBRE DEL OBSERVADOR']);
            $sheet->row(34, ['CELULAR OBSERVADOR','NÚMERO CELULAR DEL OBSERVADOR']);
            $sheet->row(35, ['TELÉFONO OBSERVADOR','TELÉFONO FIJO DEL OBSERVADOR']);
            $sheet->row(36, ['CORREO OBSERVADOR','CORREO ELECTRÓNICO DEL OBSERVADOR']); */
            $sheet->row(18, ['ESTADO DE LLAMADA','ESTADO DE LLAMADA AL RESPONSABLE DE SEDE']);
            $sheet->row(19, ['PROBLEMA DE LLAMADA','PROBLEMA DE LLAMADA AL RESPONSABLE DE SEDE']);
            /* $sheet->row(39, ['RECEPCIÓN CORREO','CONFIRMACIÓN DE RECEPCIÓN DEL CORREO']);
            $sheet->row(40, ['CAPACITACIÓN','SÍ RECIBIÓ LA CAPACITACIÓN']);            
            $sheet->row(41, ['DESCARGA','SÍ REALIZÓ LA DESCARGA DEL APLICATIVO']);
            $sheet->row(42, ['INSTALACIÓN','SÍ REALIZÓ LA INSTALACIÓN DEL APLICATIVO']);
            $sheet->row(43, ['IMPRESIÓN MATERIAL','SÍ REALIZÓ LA IMPRESIÓN DEL MATERIAL DE APLICACIÓN']);
            $sheet->row(44, ['TIPO APLICACIÓN','TIPO DE APLICACIÓN EN LA SEDE']); */
            $sheet->row(20, ['10MO DE EGB','CANTIDAD DE ESTUDIANTES QUE TIENEN PARA DECIMO DE EGB']);
            $sheet->row(21, ['1ERO DE BGU','CANTIDAD DE ESTUDIANTES PARA 1ERO DE BACHILLERATO']);
            $sheet->row(22, ['DIRECCIÓN','DIRECCIÓN DE LA INSTITUCIÓN']);
            //$sheet->row(23, ['JORNADA','TIPO DE JORNADA']);
            $sheet->row(23, ['SOSTENIMIENTO','TIPO DE SOSTENIMIENTO']);
            $sheet->row(24, ['ESTADO INSTITUCIÓN','ESTADO EN EL QUE SE ENCUENTRA LA INSTITUCIÓN']);
            $sheet->row(25, ['AMIE ACTUAL','CÓDIGO PERTENECIENTE A LA INSTITUCIÓN EDUCATIVA ACTUAL']);
            $sheet->row(26, ['INSTITUCIÓN ACTUAL','NOMBRE DE LA INSTITUCIÓN EDUCATIVA ACTUAL']);
            $sheet->row(27, ['OBSERVACIÓN','OBSERVACIÓN DE LA INSTITUCIÓN EDUCATIVA ACTUAL']);
            $sheet->row(28, ['TELÉFONO CONTACTADO','TELÉFONO CONTACTADO']);
            $sheet->row(29, ['RECEPCIÓN DE MATERIAL DE EVALUACIÓN','RECEPCIÓN DE MATERIAL DE EVALUACIÓN']);
            /*$sheet->row(30, ['8VO DE EGB','']);
            $sheet->row(31, ['9NO DE EGB','']);
            $sheet->row(32, ['2DO DE BGU','']);
            $sheet->row(33, ['3RO DE BGU','']);*/
             /*$sheet->row(49, ['DOCENTES 1ERO DE EGB','CANTIDAD DE DOCENTES QUE TIENEN PARA 1ERO DE EDUCACIÓN GENERAL BÁSICA']);
            $sheet->row(50, ['INICIAL 2','CONFIRMAR SÍ TIENEN INICIAL 2']);
            $sheet->row(51, ['ESTUDIANTES INICIAL 2','CANTIDAD DE ESTUDIANTES QUE TIENEN PARA INICIAL 2']);
            $sheet->row(52, ['DOCENTES INICIAL 2','CANTIDAD DE DOCENTES QUE TIENEN PARA INICIAL 2']);
            $sheet->row(53, ['REGISTRO MOTIVOS','REGISTRO DE MOTIVOS']); */
            //$sheet->row(41, ['CARGA DE ARCHIVOS','SÍ CONOCE EL PROCEDIMIENTO PARA CARGA DE ARCHIVOS']);
            //$sheet->row(42, ['REGISTRO DIGITAL','SÍ CONOCE EL REGISTRO DE ASISTENCIA DIGITAL']);            
            //$sheet->row(47, ['OBSERVACIÓN INSTALACIÓN','OBSERVACIÓN PORQUE NO SE PUDO INSTALAR EL APLICATIVO']);
            
            
        $sheet->cells('A1:E1', function($cells){
            $cells->setBackground('#0489B1');
            $cells->setFontColor('#FFFFFF');
            $cells->setAlignment('center');
            $cells->setValignment('center');
            $cells->setFontweight('bold');
        });
                 
        });    

        $excel->sheet('Datos', function($sheet) use ($id) {
        
        // Header
       // $sheet->mergeCells('A1:F1'); 
        //$sheet->row(1, ['Reporte de Prueba Uno']);
        $sheet->row(1, ['AMIE',	'MONITOR', 'INSTITUCIÓN', 'ZONA', 'PROVINCIA', 'CANTÓN', 'PARROQUIA', 'DISTRITO', 'RESPONSABLE DE SEDE 1', 'CELULAR 1', /* 'WHATSAPP 1',*/ 'CELULAR 2', //'WHATSAPP 2',
        'TELÉFONO 1', 'TELÉFONO 2', 'TELÉFONO 3', 'CORREO ELECTRÓNICO 1', 'CORREO ELECTRÓNICO 2', /* 'MOTIVO INASISTENCIA', 'COMUNICACIÓN RÁPIDA', 'RESPONSABLE DE SEDE 2', 'CELULAR 1 SPTE', 'WHATSAPP 1 SPTE', 'CELULAR 2 SPTE',
        'WHATSAPP 2 SPTE', 'TELÉFONO 1 SPTE', 'TELÉFONO 2 SPTE', 'TELÉFONO 3 SPTE',  'CORREO ELECTRÓNICO 1 SPTE', 'CORREO ELECTRÓNICO 2 SPTE', 'MOTIVO INASISTENCIA SPTE', 'COMUNICACIÓN RÁPIDA SPTE', 
        'OBSERVADOR', 'CELULAR OBSERVADOR', 'TELÉFONO OBSERVADOR', 'CORREO OBSERVADOR',*/ 'ESTADO DE LLAMADA', 'PROBLEMA DE LLAMADA', /* 'RECEPCIÓN CORREO', 'CAPACITACIÓN', 'DESCARGA', 'INSTALACIÓN', 'IMPRESIÓN MATERIAL', 
        'TIPO APLICACIÓN', 'AUDITIVA', 'VISUAL', 'OBSERVACIÓN INSTALACIÓN', '1ERO DE EGB', 'ESTUDIANTES 1ERO DE EGB', 'DOCENTES 1ERO DE EGB', 'INICIAL 2', 'ESTUDIANTES INICIAL 2', 'DOCENTES INICIAL 2', 'REGISTRO MOTIVOS', 'CARGA DE ARCHIVOS', 'REGISTRO DIGITAL'*/ 
        /*'DECIMO DE EGB','1ERO DE BACHILLERATO',*/'DIRECCIÓN',/*'JORNADA',*/'SOSTENIMIENTO', 'ESTADO INSTITUCIÓN'
        , 'AMIE ACTUAL', 'INSTITUCIÓN ACTUAL', 'OBSERVACIÓN', 'TELÉFONO CONTACTADO', 'RECEPCIÓN DE MATERIAL DE EVALUACIÓN'
        ]);
        
        $sheet->cells('A1:Z1', function($cells){
            $cells->setBackground('#0489B1');
            $cells->setFontColor('#FFFFFF');
            $cells->setAlignment('center');
            $cells->setValignment('center');
            $cells->setFontweight('bold');
        });
        
        // Data            
        $exportLlamadas = \DB::select("select amie, coordinador, monitor, institucion, zona, provincia, canton, parroquia, distrito, correo as correo_institucion,
    telefono1 as telefono1_institucion, telefono2 as telefono2_institucion, rector, celular as celular_rector, celular_whatsapp as celular_rector_whatsapp, correoinstitucion as correo_rector,
    contactoinstitucion as telefono3, recibiocomunicado, estadollamada, novedadllamada, participacion as encuesta_monitor_participacion_sede, comunicacion, rector_suplente, celular_suplente, celular_whatsapp_suplente,
    correoinstitucion_suplente, contactoinstitucion_suplente as telefono3_splt, telefono1_suplente, telefono2_suplente, correo_suplente, comunicacion_suplente, celular1_wa, celular2_wa, celular1_swa, celular2_swa,
    motivo1, motivo2, capacitacion, descarga, instalacion, cargaarchivo, registrodigital, impresionaplicacion, observador, correo_observador, celular_observador, telefono_observador, tipo_aplicacion, auditiva, visual,
    primero_egb, primero_egb_n, primero_egb_d, iniciall_2, inicial_2_n, inicial_2_d, registro_motivos, observacion_instala, decimo_egb_n, primero_bach_n, jornada, sostenimiento, estado_institucion, direccion, amie_actual, institucion_actual, observacion, telefono_contactado, recibiocomunicado, octavo_egb_n, noveno_egb_n, segundo_bach_n, tercero_bach_n
    from sig_institucionv where proceso=$id and estado=1");
        
        foreach ($exportLlamadas as $llamadas){
        
            $row = [];
            $row[0] = $llamadas->amie;
            $row[1] = $llamadas->monitor;
            $row[2] = $llamadas->institucion;
            $row[3] = $llamadas->zona;
            $row[4] = $llamadas->provincia;
            $row[5] = $llamadas->canton;
            $row[6] = $llamadas->parroquia;
            $row[7] = $llamadas->distrito;
            $row[8] = $llamadas->rector;
            $row[9] = $llamadas->celular_rector;
           // $row[10] = $llamadas->celular1_wa;
            $row[10] = $llamadas->celular_rector_whatsapp;
           // $row[12] = $llamadas->celular2_wa;
            $row[11] = $llamadas->telefono1_institucion;
            $row[12] = $llamadas->telefono2_institucion;  
            $row[13] = $llamadas->telefono3;
            $row[14] = $llamadas->correo_rector;
            $row[15] = $llamadas->correo_institucion;           
            /* $row[18] = $llamadas->motivo1;         
            $row[19] = $llamadas->comunicacion;          
            $row[20] = $llamadas->rector_suplente;
            $row[21] = $llamadas->celular_suplente;
            $row[22] = $llamadas->celular1_swa;
            $row[23] = $llamadas->celular_whatsapp_suplente;
            $row[24] = $llamadas->celular2_swa;
            $row[25] = $llamadas->telefono1_suplente;
            //$row[26] = $llamadas->telefono2_suplente;
            $row[26] = $llamadas->telefono3_splt;
            $row[27] = $llamadas->correoinstitucion_suplente;
            $row[28] = $llamadas->correo_suplente;
            $row[29] = $llamadas->motivo2;
            $row[30] = $llamadas->comunicacion_suplente;     
            $row[31] = $llamadas->observador;     
            $row[32] = $llamadas->celular_observador;     
            $row[33] = $llamadas->telefono_observador;     
            $row[34] = $llamadas->correo_observador; */    
            $row[16] = $llamadas->estadollamada;
            $row[17] = $llamadas->novedadllamada;    
           /* $row[37] = $llamadas->recibiocomunicado;     
            $row[38] = $llamadas->capacitacion;
            $row[39] = $llamadas->descarga;
            $row[40] = $llamadas->instalacion;
            $row[41] = $llamadas->impresionaplicacion;
            $row[42] = $llamadas->tipo_aplicacion; */
            /*$row[18] = $llamadas->decimo_egb_n;
            $row[19] = $llamadas->primero_bach_n;*/
            $row[18] = $llamadas->direccion;
            // $row[21] = $llamadas->jornada;
            $row[19] = $llamadas->sostenimiento;
            $row[20] = $llamadas->estado_institucion;
            $row[21] = $llamadas->amie_actual;
            $row[22] = $llamadas->institucion_actual;
            $row[23] = $llamadas->observacion;
            $row[24] = $llamadas->telefono_contactado;
            $row[25] = $llamadas->recibiocomunicado;
            /*$row[28] = $llamadas->octavo_egb_n;
            $row[29] = $llamadas->noveno_egb_n;
            $row[30] = $llamadas->segundo_bach_n;
            $row[31] = $llamadas->tercero_bach_n;*/
           /* $row[43] = $llamadas->auditiva;
            $row[44] = $llamadas->visual;
            $row[45] = $llamadas->observacion_instala;
            $row[45] = $llamadas->primero_egb;
            $row[46] = $llamadas->primero_egb_n;
            $row[47] = $llamadas->primero_egb_d;
            $row[48] = $llamadas->iniciall_2;
            $row[49] = $llamadas->inicial_2_n;
            $row[50] = $llamadas->inicial_2_d;
            $row[51] = $llamadas->registro_motivos; */
            //$row[39] = $llamadas->cargaarchivo;
            //$row[40] = $llamadas->registrodigital;
            
            $sheet->appendRow($row);
            }                
        });

    })->export('xlsx');
    
    }
    
    public function exportar_llamadas_apliValidacion($id)
    {      
        Excel::create('DACT_LLAMADAS_APLICADORES_VALIDACION', function($excel) use ($id) {
                        
        $excel->sheet('Datos', function($sheet) use ($id) {
        
        // Header
       // $sheet->mergeCells('A1:F1'); 
        //$sheet->row(1, ['Reporte de Prueba Uno']);
        $sheet->row(1, ['LABORATORIO', 'JORNADA', 'SESION', 'HORA CONVOCATORIA', 'HORA SALIDA', 'FECHA DE APLICACION', 'IDENTIFICACION APLICADOR', 'NOMBRES DEL APLICADOR',
            'TELEFONO APLICADOR', 'CELULAR APLICADOR', 'CORREO INSTITUCIONAL', 'CORREO PERSONAL', 'ASISTENCIA', 'ESTADO DE LLAMADA', 'NOVEDAD DE LLAMADA',
            'AMIE ORIGEN', 'INSTITUCION ORIGEN' ]);
        
        $sheet->cells('A1:Q1', function($cells){
            $cells->setBackground('#0489B1');
            $cells->setFontColor('#FFFFFF');
            $cells->setAlignment('center');
            $cells->setValignment('center');
            $cells->setFontweight('bold');
        });
                
        // Data            
        $exportLlamadas = \DB::select("select id_sede as laboratorio, jornada, sesion, hora_convocatoria, hora_salida,fecha_aplicacion,
        identificacion as identificacion_aplicador, nombres as nombres_aplicador,
        telefono as telefono_aplicador, celular as celular_aplicador, correoinstitucional, correopersonal,
        asistencia, estadollamada, novedadllamada, amie_origen, institucion_origen  
        from sig_aplicadores where cgi_periodo_id = $id ");
        
        foreach ($exportLlamadas as $llamadas){
        
            $row = [];
            $row[0] = $llamadas->laboratorio;                  
            $row[1] = $llamadas->jornada;
            $row[2] = $llamadas->sesion;
            $row[3] = $llamadas->hora_convocatoria;
            $row[4] = $llamadas->hora_salida;
            $row[5] = $llamadas->fecha_aplicacion;
            $row[6] = $llamadas->identificacion_aplicador;
            $row[7] = $llamadas->nombres_aplicador;
            $row[8] = $llamadas->telefono_aplicador;
            $row[9] = $llamadas->celular_aplicador;
            $row[10] = $llamadas->correoinstitucional;
            $row[11] = $llamadas->correopersonal;
            $row[12] = $llamadas->asistencia;
            $row[13] = $llamadas->estadollamada;
            $row[14] = $llamadas->novedadllamada;
            $row[15] = $llamadas->amie_origen;
            $row[16] = $llamadas->institucion_origen;
            
            $sheet->appendRow($row);
            }                
        });

    })->export('csv');
    
    }
    
    public function exportar_llamadas_apli($id)
    {      
        Excel::create('DACT_LLAMADAS_APLICADORES', function($excel) use ($id) {

        $excel->sheet('Datos', function($sheet) use ($id) {
        
        // Header
       // $sheet->mergeCells('A1:F1'); 
        //$sheet->row(1, ['Reporte de Prueba Uno']);
        $sheet->row(1, ['AMIE', 'LABORATORIO', 'IDENTIFICACION','NOMBRES', 'TELEFONO', 'CELULAR','CORREO INSTITUCIONAL', 'CORREO PERSONAL', 
            'ASISTENCIA', 'ESTADO LLAMADA', 'NOVEDAD LLAMADA', 'OBSERVACIONES', /* 'FECHA PROGRAMADA INICIO', 'SESION',*/ 'COORDINADOR', 'ZONA', 'PERIODO'
            ]);
        
        $sheet->cells('A1:O1', function($cells){
            $cells->setBackground('#0489B1');
            $cells->setFontColor('#FFFFFF');
            $cells->setAlignment('center');
            $cells->setValignment('center');
            $cells->setFontweight('bold');
        });
        
        // Data            
        $exportLlamadas_apli = \DB::select("select ap.amie, ap.id_sede, ap.identificacion, ap.nombres, ap.telefono, ap.celular, ap.correoinstitucional, ap.correopersonal, 
ap.asistencia_apli, ap.estadollamada_apli, ap.novedadllamada_apli, ap.observaciones, ap.claveyusuario, u.fecha_programada_inicio, u.sesion, u.coordinador, u.zona, u.periodo_monitoreo
from (select fecha_programada_inicio, sesion, coordinador, zona, periodo_monitoreo, codigoamei from umonitoreo 
where cgi_periodo_id = $id and estado=1
group by fecha_programada_inicio, sesion, coordinador, zona, periodo_monitoreo, codigoamei
) as  u, sig_aplicadores ap 
where ap.amie=u.codigoamei and ap.cgi_periodo_id=104");
        
        foreach ($exportLlamadas_apli as $llamadas){
        
            $row = [];
            $row[0] = $llamadas->amie;
            $row[1] = $llamadas->id_sede;
            $row[2] = $llamadas->identificacion;
            $row[3] = $llamadas->nombres;
            $row[4] = $llamadas->telefono;
            $row[5] = $llamadas->celular;
            $row[6] = $llamadas->correoinstitucional;
            $row[7] = $llamadas->correopersonal;
            $row[8] = $llamadas->asistencia_apli;
            $row[9] = $llamadas->estadollamada_apli;
            $row[10] = $llamadas->novedadllamada_apli;
            $row[11] = $llamadas->observaciones;
           /* $row[12] = $llamadas->fecha_programada_inicio;
            $row[13] = $llamadas->sesion; */
            $row[12] = $llamadas->coordinador;
            $row[13] = $llamadas->zona;
            $row[14] = $llamadas->periodo_monitoreo;
            
            
            $sheet->appendRow($row);
            }                
        });

    })->export('xlsx');
    
    }
    
     public function exportar_info_aplicacion($id)
    {
        
         $export_info_apli = \DB::select("select codigoamei, zona, institucion, fecha_programada_inicio, sesion, id_sede, distrito_id, provincia, monitor, name_monitor, coordinador, programados, 
cargas, asistencia, reprogramacion, estado_sesion, sesion_aplicador as asistencia_responsable, observacion_sesion,recepcion_aplicativo, descarga_aplicativo, 
observacion_descarga, instalo_aplicativo, comp_inst as maquinas_instaladas, (programados - comp_inst) as maquinas_faltantes, observacion_instala, internet, presentes,
impresora, recepcion_clave, asistencia_observador, poblacion, listado_asistencia, observacion_listado_asistencia, listado_validacion, estadollamada_apli, novedadllamada_apli from umonitoreo where cgi_periodo_id = $id  AND estado=1");    
                
    Excel::create('DACT_APLICACION', function($excel) use ($export_info_apli) {
        
         $excel->sheet('DD', function($sheet){
       
        // Header
        //$sheet->mergeCells('A1:B1');        
        $sheet->row(1, ['CAMPO','DESCRIPCIÓN']);
        $sheet->row(2, ['CÓDIGO AMIE','CÓDIGO PERTENECIENTE A LA INSTITUCIÓN EDUCATIVA']);
        $sheet->row(3, ['INSTITUCIÓN','NOMBRE DE LA INSTITUCIÓN EDUCATIVA']);
        $sheet->row(4, ['COORDINADOR','COORDINADOR ZONAL ENCARGADO DE TENER COMUNICACIÓN CON LA INSTITUCIÓN']);
        $sheet->row(5, ['CÓDIGO MONITOR','CÓDIGO MONITOR']);
        $sheet->row(6, ['NOMBRE MONITOR','NOMBRE DEL MONITOR ENCARGADO DE TENER COMUNICACIÓN CON LA INSTITUCIÓN']);
        $sheet->row(7, ['ZONA','ZONA A LA QUE PERTENECE LA INSTITUCIÓN EDUCATIVA']);
        $sheet->row(8, ['PROVINCIA','PROVINCIA A LA QUE PERTENECE LA INSTITUCIÓN']);
        $sheet->row(9, ['DISTRITO','DISTRITO AL QUE PERTENECE LA INSTITUCIÓN']);
        $sheet->row(10, ['FECHA PROGRAMADA','FECHA PROGRAMADA DE LA EVALUACIÓN']);
        $sheet->row(11, ['SESIÓN','SESIÓN DE LA EVALUACIÓN']);
        $sheet->row(12, ['PROGRAMADOS','TOTAL DE SUSTENTANTES A EVALUAR']);
        $sheet->row(13, ['CARGAS','TOTAL DE ARCHIVOS CARGADOS EN LA PLATAFORMA CRM']);
        $sheet->row(14, ['ASISTENCIA','TOTAL DE SUSTENTANTES QUE ASISTIERON A LA EVALUACIÓN']);
        $sheet->row(15, ['REPROGRAMACIÓN','TOTAL DE SUSTENTANTES REPROGRAMADOS']);
        $sheet->row(16, ['RECIBIÓ APLICATIVO','CONFIRMACIÓN SI EL RESPONSABLE DE SEDE RECIBIÓ EL APLICATIVO']);
        $sheet->row(17, ['DESCARGÓ APLICATIVO','CONFIRMACIÓN SI EL RESPONSABLE DE SEDE DESCARGÓ EL APLICATIVO']);
        $sheet->row(18, ['OBSERVACIÓN DESCARGA','OBSERVACIÓN SI OCURRIO UN PROBLEMA CON LA DESCARGA']);
        $sheet->row(19, ['INSTALÓ APLICATIVO','CONFIRMACIÓN SI EL APLICATIVO FUE INSTALADO']);
        $sheet->row(20, ['OBSERVACIÓN INSTALACIÓN','OBSERVACIÓN PORQUE NO SE PUDO INSTALAR EL APLICATIVO']);
        $sheet->row(21, ['MAQUINAS INSTALADAS','CANTIDAD DE MAQUINAS EN LA QUE FUERON INSTALADAS EL APLICATIVO']);
        $sheet->row(22, ['MAQUINAS FALTANTES','CANTIDAD DE MAQUINAS FALTANTES POR INSTALAR EL APLICATIVO']);
        $sheet->row(23, ['INICIÓ SESIÓN','INICIO DE LA EVALUACIÓN EN LA SESIÓN CORRESPONDIENTE']);
        $sheet->row(24, ['OBSERVACIÓN SESIÓN','OBSERVACIÓN DE PORQUE NO SE INICIÓ LA EVALUACIÓN']);
        $sheet->row(25, ['PRESENTES','SUSTENTANTES PRESENTES EN LA APLICACIÓN']);
        $sheet->row(26, ['INTERNET','SI LA INSTITUCIÓN CUENTA CON INTERNET']);
        $sheet->row(27, ['IMPRESORA','SI LA INSTITUCIÓN CUENTA CON IMPRESORA']);
        $sheet->row(28, ['RECEPCIÓN DE CLAVE','CONFIRMACIÓN DE RECEPCIÓN DE CLAVES']);
        $sheet->row(29, ['ASISTENCIA RESPONSABLE SEDE','ASISTENCIA RESPONSABLE DE SEDE']);
        $sheet->row(30, ['ASISTENCIA OBSERVADOR','ASISTENCIA DEL OBSERVADOR']);
        $sheet->row(31, ['POBLACIÓN','POBLACIÓN ESCOLAR / NO ESCOLAR']);
        $sheet->row(32, ['LISTADO ASISTENCIA','SÍ EL RESPONSABLE SEDE RECIBIÓ EL LISTADO']);
        $sheet->row(33, ['OBSERVACIÓN LISTADO ASISTENCIA','SÍ EL RESPONSABLE SEDE REPORTA OBSERVACIÓN DEL LISTADO']);
        $sheet->row(34, ['LISTADO VALIDACIÓN','VERIFICACIÓN DE LISTADOS']);
        $sheet->row(35, ['ESTADO DE LLAMADA','ESTADO DE LLAMADA']);
        $sheet->row(36, ['PROBLEMA DE LLAMADA','PROBLEMA DE LLAMADA']);


            
        $sheet->cells('A1:B1', function($cells){
            $cells->setBackground('#0489B1');
            $cells->setFontColor('#FFFFFF');
            $cells->setAlignment('center');
            $cells->setValignment('center');
            $cells->setFontweight('bold');
        });
                 
        });
        
        $excel->sheet('Datos', function($sheet) use ($export_info_apli) {
       
          $sheet->row(1, ['CÓDIGO AMIE', 'INSTITUCIÓN',	'COORDINADOR', 'CÓDIGO MONITOR', 'NOMBRE MONITOR', 'ZONA', 'PROVINCIA', 'DISTRITO', 'FECHA PROGRAMADA',	'SESIÓN', 'PROGRAMADOS',
          'CARGAS', 'ASISTENCIA', 'REPROGRAMACIÓN', 'RECIBIÓ APLICATIVO', 'DESCARGÓ APLICATIVO', 'OBSERVACIÓN DESCARGA', 'INSTALÓ APLICATIVO', 'OBSERVACIÓN INSTALACIÓN', 'MAQUINAS INSTALADAS',
          'MAQUINAS FALTANTES',	'INICIÓ SESIÓN', 'OBSERVACIÓN SESIÓN', 'PRESENTES', 'INTERNET', 'IMPRESORA', 'RECEPCIÓN DE CLAVE', 'ASISTENCIA RESPONSABLE SEDE', 'ASISTENCIA OBSERVADOR', 
          'POBLACIÓN', 'LISTADO ASISTENCIA', 'OBSERVACIÓN LISTADO ASISTENCIA', 'LISTADO VALIDACIÓN', 'ESTADO DE LLAMADA', 'PROBLEMA DE LLAMADA'
            ]);  
            
        $sheet->cells('A1:AI1', function($cells){
            $cells->setBackground('#0489B1');
            $cells->setFontColor('#FFFFFF');
            $cells->setAlignment('center');
            $cells->setValignment('center');
            $cells->setFontweight('bold');
        });
        
        // Data       
        $export_info_apli;
        
        foreach ($export_info_apli as $llamadas){
        
            $row = [];
            $row[0] = $llamadas->codigoamei;
            $row[1] = $llamadas->institucion;
            $row[2] = $llamadas->coordinador;
            $row[3] = $llamadas->monitor;
            $row[4] = $llamadas->name_monitor;
            $row[5] = $llamadas->zona;
            $row[6] = $llamadas->provincia;
            $row[7] = $llamadas->distrito_id;
            $row[8] = $llamadas->fecha_programada_inicio;
            $row[9] = $llamadas->sesion;
            $row[10] = $llamadas->programados;
            $row[11] = $llamadas->cargas;
            $row[12] = $llamadas->asistencia;
            $row[13] = $llamadas->reprogramacion;
            $row[14] = $llamadas->recepcion_aplicativo;
            $row[15] = $llamadas->descarga_aplicativo;
            $row[16] = $llamadas->observacion_descarga;
            $row[17] = $llamadas->instalo_aplicativo;
            $row[18] = $llamadas->observacion_instala;
            $row[19] = $llamadas->maquinas_instaladas;
            $row[20] = $llamadas->maquinas_faltantes;
            $row[21] = $llamadas->estado_sesion;
            $row[22] = $llamadas->observacion_sesion;
            $row[23] = $llamadas->presentes;
            $row[24] = $llamadas->internet;
            $row[25] = $llamadas->impresora;
            $row[26] = $llamadas->recepcion_clave;
            $row[27] = $llamadas->asistencia_responsable;
            $row[28] = $llamadas->asistencia_observador;
            $row[29] = $llamadas->poblacion;
            $row[30] = $llamadas->listado_asistencia;
            $row[31] = $llamadas->observacion_listado_asistencia;
            $row[32] = $llamadas->listado_validacion;
            $row[33] = $llamadas->estadollamada_apli;
            $row[34] = $llamadas->novedadllamada_apli;

           
            
            $sheet->appendRow($row);
            }                
        });       
        
    })->export('xlsx');
    
    }
    
}