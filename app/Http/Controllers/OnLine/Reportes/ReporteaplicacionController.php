<?php

namespace App\Http\Controllers\OnLine\Reportes;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ReporteaplicacionController extends Controller
{
    public function index(){
        $periodo = \DB::select("SELECT cgi_periodo_id, name_periodo FROM umonitoreo WHERE estado=1 GROUP BY cgi_periodo_id, name_periodo ORDER BY cgi_periodo_id DESC");
        return view('aplicacion.onLine.reporte_zonal')->with('periodos', $periodo);
    }

Public function reporte_aplicacion($tp_reporte, $id_periodo, $coord, $fch="", $ss="") {
    if($coord =='0'){ $coordinador = '';}else{ $coordinador = "AND coordinador='$coord'"; }        
    if($fch == 0){$fecha = '';}else{ $fecha = "AND fecha_programada_inicio='$fch'"; }        
    if( $ss ==""){$sesion = '';}else{$sesion = "AND sesion='$ss'";}
    if($tp_reporte == "inicio_aplicacion"){

        $reportedescarga= \DB::select("SELECT (case when descargo_credencial is null then 'NO' when descargo_credencial is false then 'NO' else 'SI' end) as descarga, COUNT(*) as cantidad
        FROM view_sustensesionlab_otielnet where cgi_periodo_id in (select periodo_monitoreo from umonitoreo where cgi_periodo_id=$id_periodo and estado='1' $fecha $sesion and estado_sesion2='online' group by periodo_monitoreo)
        and cgi_laboratorio_id in (select cgi_laboratorio_id::int from umonitoreo where cgi_periodo_id=$id_periodo and estado='1' $fecha $sesion and estado_sesion2='online' 
        group by cgi_laboratorio_id) GROUP BY descargo_credencial");
                                
        $reporteinisesion = \DB::select("select  (case when inicio_sesion is null then 'NO' when inicio_sesion is false then 'NO' else 'SI' end) as sesion, COUNT(*) as cantidad
        FROM view_sustensesionlab_otielnet where cgi_periodo_id =$id_periodo and cgi_laboratorio_id::text in 
        (select cgi_laboratorio_id from umonitoreo where cgi_periodo_id=$id_periodo and estado='1' $fecha $sesion and estado_sesion2 ='online' group by cgi_laboratorio_id) GROUP BY inicio_sesion");           
        
        $reporteLlamada = \DB::select("SELECT (case when estadollamada_apli is null then 'POR INICIAR' when estadollamada_apli ='' then 'POR INICIAR' else estadollamada_apli end) as estadollamada, COUNT(*) as cantidadllamada
        FROM umonitoreo WHERE cgi_periodo_id = $id_periodo $fecha $sesion AND estado = 1 and estado_sesion2='online' GROUP BY (case when estadollamada_apli is null then 'POR INICIAR' when estadollamada_apli ='' then 'POR INICIAR' else estadollamada_apli end) ORDER BY estadollamada");
    
        $novedadllamada = \DB::select("SELECT (case when novedadllamada_apli='' then 'NO INICIADA' else novedadllamada_apli end) as novedadllamada_apli, count(*) AS totalnovedad_apli FROM umonitoreo where cgi_periodo_id = $id_periodo $fecha $sesion AND estado=1  and estado_sesion2='online'  and estadollamada_apli='NO INICIADA' group by (case when novedadllamada_apli='' then 'NO INICIADA' else novedadllamada_apli end) order by totalnovedad_apli desc");

        $reporteLlamadaAvance = \DB::select("SELECT (case when estadollamada_aplicadores is null then 'POR CONTACTAR' when estadollamada_aplicadores ='' then 'POR CONTACTAR' else estadollamada_aplicadores end) as estadollamada, COUNT(*) as cantidadllamada
        FROM umonitoreo WHERE cgi_periodo_id = $id_periodo $coordinador $fecha $sesion AND estado = 1 and  estado_sesion2='online' GROUP BY (case when estadollamada_aplicadores is null then 'POR CONTACTAR' when estadollamada_aplicadores ='' then 'POR CONTACTAR' else estadollamada_aplicadores end) ORDER BY estadollamada");
        
        $novedadllamadaAvance = \DB::select("SELECT (case when novedadllamada_aplicadores='' then 'NO CONTESTA' else novedadllamada_aplicadores end) as novedadllamada_aplicadores, count(*) AS totalnovedad_apli FROM umonitoreo where cgi_periodo_id = $id_periodo $coordinador $fecha $sesion AND estado=1 and  estadollamada_aplicadores='NO CONTACTADO' group by (case when novedadllamada_aplicadores='' then 'NO CONTESTA' else novedadllamada_aplicadores end) order by novedadllamada_aplicadores desc");

        $reportedescargaExtension= \DB::select("SELECT (case when descarga_aplicativo is null then 'NO' when descarga_aplicativo ='' then 'NO' else descarga_aplicativo end) as descarga, COUNT(*) as cantidad
        FROM umonitoreo WHERE cgi_periodo_id = $id_periodo $coordinador $fecha $sesion and estado=1 and estado_sesion2='online'  GROUP BY (case when descarga_aplicativo is null then 'NO' when descarga_aplicativo ='' then 'NO' else descarga_aplicativo end)");

        $noDescargoExtension= \DB::select("SELECT (case when observacion_descarga='' then 'SIN DESCARGAR APLICATIVO' 
        when observacion_descarga IS NULL then 'SIN DESCARGAR APLICATIVO'
        when descarga_aplicativo='' and observacion_descarga='SIN PROBLEMA' then 'SIN DESCARGAR APLICATIVO'
        when descarga_aplicativo IS NULl and observacion_descarga='SIN PROBLEMA' then 'SIN DESCARGAR APLICATIVO'
        when descarga_aplicativo ='NO' and observacion_descarga='SIN PROBLEMA' then 'SIN PROBLEMA REGISTRADO'
        when descarga_aplicativo IS NULL and observacion_descarga!='SIN PROBLEMA' then 'SIN SELECCIONAR DESCARGA DEL APLICATIVO'
        else observacion_descarga end) as observacion_descarga, count(*) AS totalobservacion, sum(programados) as sustentantes_afect
        FROM umonitoreo 
        where cgi_periodo_id = $id_periodo $coordinador $fecha $sesion AND estado=1 and estado_sesion2='online' and (descarga_aplicativo is null or descarga_aplicativo=''  or descarga_aplicativo ='NO')
        group by 1 order by 2 desc");

        $reporteinstaloapliExtension= \DB::select("SELECT (case when instalo_aplicativo is null then 'NO' when instalo_aplicativo ='' then 'NO' else instalo_aplicativo end) as instalo, COUNT(*) as cantidad
        FROM umonitoreo WHERE cgi_periodo_id = $id_periodo $coordinador $fecha $sesion AND estado=1 and estado_sesion2='online' GROUP BY (case when instalo_aplicativo is null then 'NO' when instalo_aplicativo ='' then 'NO' else instalo_aplicativo end)");            

        $noInstaloExtension= \DB::select("SELECT (case when instalo_aplicativo='' and observacion_instala='' then 'SIN INSTALAR EXTENSIÓN'
		when instalo_aplicativo='NO' and (observacion_instala='' or observacion_instala is null)  then 'SIN INSTALAR EXTENSIÓN'
		when instalo_aplicativo is null and observacion_instala is null then 'SIN INSTALAR EXTENSIÓN'
		when instalo_aplicativo='NO' then observacion_instala end) as observacion_instala, count(*) AS totalobservacion, sum(programados) as sustentantes_afect
        FROM umonitoreo where cgi_periodo_id = $id_periodo $coordinador $fecha $sesion AND estado=1 and estado_sesion2='online' and (instalo_aplicativo is null or instalo_aplicativo=''  or instalo_aplicativo ='NO')         
        group by 1 order by 2 desc");

        $reporteinisesionOnline = \DB::select("SELECT (case when estado_sesion is null then 'NO' when estado_sesion ='' then 'NO' else estado_sesion end) as sesion, COUNT(*) as cantidad
        FROM umonitoreo WHERE cgi_periodo_id = $id_periodo $coordinador $fecha $sesion AND estado = 1 and estado_sesion2='online' GROUP BY (case when estado_sesion is null then 'NO' when estado_sesion ='' then 'NO' else estado_sesion end)");            

        $noInicioSsesion= \DB::select("SELECT (case when observacion_sesion='' then 'SIN INICIAR SESION' 
        when observacion_sesion IS NULL then 'SIN INICIAR SESION'
        when estado_sesion='' and observacion_sesion='SIN PROBLEMA' then 'SIN INICIAR SESION'
        when estado_sesion IS NULl and observacion_sesion='SIN PROBLEMA' then 'SIN INICIAR SESION'
        when estado_sesion ='NO' and observacion_sesion='SIN PROBLEMA' then 'SIN PROBLEMA REGISTRADO'
        when estado_sesion IS NULL and observacion_sesion!='SIN PROBLEMA' then 'SIN SELECCIONAR ESTADO DE SESIÓN'
        else observacion_sesion end) as observacion_sesion, count(*) AS totalobservacion, sum(programados) as sustentantes_afect
        FROM umonitoreo 
        where cgi_periodo_id = $id_periodo $coordinador $fecha $sesion AND estado=1 and estado_sesion2='online' and (estado_sesion is null or estado_sesion=''  or estado_sesion ='NO')
        group by 1 order by 2 desc");

        $n_Sustentantes = \DB::select("SELECT SUM(a.programados) AS programados 
        FROM (SELECT (CASE WHEN estado_sesion is null THEN 'NO' WHEN estado_sesion ='' THEN 'NO' ELSE estado_sesion END) AS estado_sesion, programados, cgi_periodo_id, fecha_programada_inicio, sesion, estado, estado_sesion2 FROM umonitoreo) as a
        WHERE a.cgi_periodo_id=$id_periodo $coordinador $fecha $sesion  and a.estado=1 and a.estado_sesion2='online' AND a.estado_sesion='NO'");
        
        $Tt_Sustentantes = \DB::select("SELECT SUM(programados) AS t_programados FROM umonitoreo WHERE cgi_periodo_id=$id_periodo $coordinador $fecha $sesion and estado=1");
        
        $Total_Sustentantes= 0.00;
            
            foreach($Tt_Sustentantes as $Tt_Sustentantes){
                $Total_Sustentantes += $Tt_Sustentantes->t_programados;
            }

        $ttNoDescargoExtension = 0.00;
        $ttDSusAfectadosExtension = 0.00;
        foreach ($noDescargoExtension as $v) {
            $ttNoDescargoExtension += $v->totalobservacion;
            $ttDSusAfectadosExtension += $v->sustentantes_afect;
        }
        $ttNoInstaloExtension = 0.00;
        $ttNoInstaloExtension = 0.00;
        $ttISusAfectadosExtension = 0.00;
        foreach ($noInstaloExtension as $v) {
            $ttNoInstaloExtension += $v->totalobservacion;
            $ttISusAfectadosExtension += $v->sustentantes_afect;
        }
        $TotalInstExtension = 0.00;
        foreach ($reporteinstaloapliExtension as $generalInstal){
        $TotalInstExtension += $generalInstal->cantidad;
        }        
            
        $TotalNoContactadoa = 0.00;
        foreach ($novedadllamada as $v) {
            $TotalNoContactadoa += $v->totalnovedad_apli;
        }

        $TotalSesionOnline = 0.00;
        foreach ($reporteinisesionOnline as $generalSesion){
        $TotalSesionOnline += $generalSesion->cantidad;
        }
        
        $ttNoInicio = 0.00;
        $ttSusAfectados= 0.00;
        
            foreach ($noInicioSsesion as $v) {
                $ttNoInicio += $v->totalobservacion;
                $ttSusAfectados += $v->sustentantes_afect;
            }          

    
        $monitores= \DB::select("SELECT zona, count(cgi_laboratorio_id) as laboratorio, name_periodo,
        count(case when descarga_aplicativo ='' then 'NO' when descarga_aplicativo is null then 'NO' when descarga_aplicativo ='NO' then 'NO' end) as descargano,
        count(case when descarga_aplicativo ='SI' then 'SI' end) as descargasi,
        count(case when instalo_aplicativo ='' then 'NO' when instalo_aplicativo is null then 'NO' when instalo_aplicativo ='NO' then 'NO' end) as instalono, 
        count(case when instalo_aplicativo ='SI' then 'SI' end) as instalosi,
        count(case when estado_sesion ='' then 'NO' when estado_sesion is null then 'NO' when estado_sesion ='NO' then 'NO' end) as sesionno,
        count(case when estado_sesion ='SI' then 'SI' end) as sesionsi,
        count(case when estadollamada_apli ='INICIADA' then 'INICIADA' end) as contactado,
        count(case when estadollamada_apli ='NO INICIADA'  then 'NO INICIADA' when estadollamada_apli ='' then 'NO INICIADA' when estadollamada_apli is null then 'NO INICIADA' end) as nocontactado,             
        count(case when estadollamada_apli ='' then 'EN PROCESO' when estadollamada_apli is null then 'EN PROCESO' end) as enproceso,
        count(case when novedadllamada_apli !='' then novedadllamada_apli end) as novedadllamada
        FROM umonitoreo WHERE cgi_periodo_id =$id_periodo AND estado=1 $fecha $sesion and estado_sesion2='online'
        GROUP BY zona, name_periodo order by zona, laboratorio");  
                    
        $TotalDesc = 0.00;
        foreach ($reportedescarga as $generaldesc){
        $TotalDesc += $generaldesc->cantidad;
        }           
            
        $TotalSesion = 0.00;
        foreach ($reporteinisesion as $generalSesion){
        $TotalSesion += $generalSesion->cantidad;
        }
            
        $TotalLlamada = 0.00;
        foreach ($reporteLlamada as $generalLlamada){
        $TotalLlamada += $generalLlamada->cantidadllamada;
        }

           
        $TotalLlamadaAvance = 0.00;
        foreach ($reporteLlamadaAvance as $generalLlamada){
        $TotalLlamadaAvance += $generalLlamada->cantidadllamada;
        }
        
        $TotalNoContactadoAvance = 0.00;

        foreach ($novedadllamadaAvance as $v) {
            $TotalNoContactadoAvance += $v->totalnovedad_apli;
        }

        $TotalDescExtension = 0.00;
        foreach ($reportedescargaExtension as $generaldesc){
        $TotalDescExtension += $generaldesc->cantidad;
        }

        $periodo = 0.00;
        $TotalLab = 0.00;        
        $TotalSesionsi = 0.00;
        $TotalSesionno = 0.00;
        $TotalContactado = 0.00;
        $TotalNoContactado = 0.00;
            
        foreach ($monitores as $v) {
        $periodo = $v->name_periodo;
        $TotalLab += $v->laboratorio;
        $TotalSesionsi += $v->sesionsi;
        $TotalSesionno += $v->sesionno;
        $TotalContactado += $v->contactado;
        $TotalNoContactado += $v->nocontactado;
        }
        return view('aplicacion.onLine.aplicacionZona')
        ->with('fecha', $fecha)->with('ss', $ss)->with('novedad', $novedadllamada)
        ->with('ttnovedad', $TotalNoContactadoa)->with('idperiodo',$id_periodo)
        ->with('fecha',$fch)->with('sesion',$sesion)->with('reportdescgeneral',$reportedescarga)
        ->with('totaldesc',$TotalDesc)->with('reporteLlamada',$reporteLlamada)
        ->with('TotalLlamada',$TotalLlamada)->with('reporteinisesion',$reporteinisesion)
        ->with('TotalSesion',$TotalSesion)->with('periodo',$periodo)
        ->with('coordinador',$coordinador)->with('monitores',$monitores)->with('TotalLab',$TotalLab)
        ->with('TotalSesionsi',$TotalSesionsi)->with('TotalSesionno',$TotalSesionno)
        ->with('TotalContactado',$TotalContactado)->with('TotalNoContactado',$TotalNoContactado)
        ->with('reporteLlamadaAvance',$reporteLlamadaAvance)->with('TotalLlamadaAvance',$TotalLlamadaAvance)
        ->with('novedadllamadaAvance', $novedadllamadaAvance)->with('ttnovedad', $TotalNoContactadoAvance)
        ->with('reportdescgeneralExtension',$reportedescargaExtension)
        ->with('TotalDescExtension',$TotalDescExtension)
        ->with('noDescargoExtension',$noDescargoExtension)
        ->with('ttNoDescargoExtension',$ttNoDescargoExtension)
        ->with('ttDSusAfectadosExtension',$ttDSusAfectadosExtension)
        ->with('reporteinstaloapliExtension',$reporteinstaloapliExtension)
        ->with('TotalInstExtension',$TotalInstExtension)
        ->with('noInstaloExtension',$noInstaloExtension)
        ->with('ttNoInstaloExtension',$ttNoInstaloExtension)
        ->with('ttISusAfectadosExtension',$ttISusAfectadosExtension)
        ->with('reporteinisesionOnline',$reporteinisesionOnline)
        ->with('TotalSesionOnline',$TotalSesionOnline)
        ->with('noInicioSsesion',$noInicioSsesion)->with('ttNoInicio',$ttNoInicio)
        ->with('ttSusAfectados',$ttSusAfectados)
        ->with('n_Sustentantes', $n_Sustentantes)
        ->with('Total_Sustentantes', $Total_Sustentantes);              
        
         

        //// ASISTENCIA        
    }elseif($tp_reporte == "fin_aplicacion"){
        
        $reporteCargas = \DB::select("select a.zona, count(a.laboratorio) as laboratorio, SUM(a.programados) as programados, sum(a.asistencia) as asistencia, sum(a.cargas) as cargas, a.name_periodo
        from (SELECT zona, count(cgi_laboratorio_id) as laboratorio, SUM(programados) as programados, sum(asistencia) as asistencia, sum(cargas) as cargas, name_periodo 
        FROM umonitoreo WHERE cgi_periodo_id =$id_periodo $fecha $sesion AND estado=1 and estado_sesion2='online' AND created_at is null group by zona, cgi_laboratorio_id, name_periodo order by zona) as a 
        GROUP BY a.zona, a.name_periodo order by zona");        
        
        $periodo = 0.00;            
        $TotalProg = 0.00;
        $TotalAsis = 0.00;
        $TotalAusent = 0.00;
        
        foreach ($reporteCargas as $v) {
        $periodo = $v->name_periodo;
        $TotalProg += $v->programados;
        $TotalAsis += $v->asistencia;
        $TotalAusent += ($v->programados - $v->asistencia);
        }       

        $reporteinisesion = \DB::select("select  (case when inicio_sesion is null then 'NO' when inicio_sesion is false then 'NO' else 'SI' end) as sesion, COUNT(*) as cantidad
        FROM view_sustensesionlab_otielnet where cgi_periodo_id =$id_periodo and cgi_laboratorio_id::text in 
        (select cgi_laboratorio_id from umonitoreo where cgi_periodo_id=$id_periodo and estado='1' and asistencia > 0 $fecha $sesion and estado_sesion2 ='online' group by cgi_laboratorio_id) GROUP BY inicio_sesion");
        
        $TotalSesion = 0.00;
        foreach ($reporteinisesion as $generalSesion){
        $TotalSesion += $generalSesion->cantidad;
        }
        if( $ss ==""){$sesion = '';}else{$sesion = "AND sesion_p='$ss'";}
        $reporteestadoevaluacion=\DB::select("select sum(finalizado_c) as finalizado_c, sum(finalizado_pi) as finalizado_pi, sum(suspendido_c) as suspendido_c , sum(suspendido_pi) as suspendido_pi, sum(en_curso_c) as en_curso_c, (sum(en_curso_pi)/sum(en_curso_c)) as en_curso_pi, sum(reprogra_c) as reprogra_c , sum(reprogra_pi) as reprogra_pi, sum(inicio_sesion_c) as inicio_sesion_c 
            from f_tab_totalavance($id_periodo) where online_c =1 $fecha $sesion");
        
        return view('aplicacion.onLine.cargasZona')->with('coordinador',$coordinador)
        ->with('periodo', $periodo)->with('reportecarga', $reporteCargas)
        ->with('totalpro', $TotalProg)->with('totalasis', $TotalAsis)
        ->with('TotalAusent',$TotalAusent)->with('reporteinisesion',$reporteinisesion)
        ->with('TotalSesion',$TotalSesion)
        ->with('reporteestadoevaluacion',$reporteestadoevaluacion[0]);

    }elseif($tp_reporte=='linea_tiempo'){
        
        return view('aplicacion.onLine.lineaTiempo')->with('periodo',$id_periodo);
    }
}
}