<?php

namespace App\Http\Controllers\OnLine;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Storage;
use Illuminate\Support\Facades\Validator;
use App\TableroMonitoreo;
use App\Informe;
use App\SubCategoriaEtiqueta;
use Session;
use Illuminate\Support\Facades\Auth;
use App\User;

class InformeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
      @return \Illuminate\Http\Response
     */

        public function __construct()
    {
        $this->middleware('auth');
    }

    public function form_monitoreo_informe($id){

       $monitoreo=TableroMonitoreo::find($id);
       if($monitoreo){
         $subCategoriaetiqueta=\DB::select('select * from subcategoria_etiqueta where categoria_etiqueta_id = 16 AND estado=TRUE order by 3');
         $informe1 = \DB::select("select * from sig_informe where id_monitoreo='$id'");
         $rutaarchivos= "../storage/archivos/";
         $id2=$id;

         $subCategoriaetiquetaListado=\DB::select('select * from subcategoria_etiqueta where categoria_etiqueta_id = 14');

         return view("formularios.onLine.form_monitoreo_informe")
         ->with("monitoreo",$monitoreo)
         ->with("subCategoriaetiqueta", $subCategoriaetiqueta)
         ->with("informe1", $informe1) 
         ->with("rutaarchivos", $rutaarchivos)
         ->with("id2", $id2)
         ->with("subCategoriaetiquetaListado", $subCategoriaetiquetaListado);
       }else{
         return view("mensajes.msj_rechazado")->with("msj","No existe Laboratorio");
       }
    }

    public function agregar_publicacion(Request $request ){
        $archivo = $request->file('file');
        $input  = array('file' => $archivo) ;
        $reglas = array('file' => 'required|mimes:jpg,jpeg,gif,png,xls,xlsx,doc,docx,pdf|max:9000');  //recordar que para activar mimes se debe descomentar la linea de codigo  'extension=php_fileinfo.dll' del php.ini
        $validacion = Validator::make($input,  $reglas);
        if ($validacion->fails()){
            return response()->json(['status' => 'warning', 'message' => '<b>El archivo es demasiado Grande para subirlo!</b>']);  
        }else{
             $informes= new Informe;
             $informes->id_monitoreo= $request->input("id_monitoreo");
             $informes->id_subcategoria_etiqueta= $request->input("id_subcategoria_etiqueta");
             $informes->observaciones= $request->input("observacion_publicacion");
             $carpeta2=$request->input("id_subcategoria_etiqueta");
             $carpeta = "aplicacion";
             $carpetamonitor = Auth::user()->username;
             $carpetaperiodo = Session::get('Periodo', 0);
             $ruta="online/".$carpetaperiodo."/".$carpetamonitor."/".$carpeta."/".$carpeta2."/".$archivo->getClientOriginalName();
             $r1=Storage::disk('archivos')->put($ruta,  \File::get($archivo) );
             $informes->ruta=$ruta;
             if($informes->save()){            
                return response()->json(['status' => 'success', 'message' => '<b>Archivo agregado correctamente!</b>']);   
              }else{            
                return response()->json(['status' => 'warning', 'message' => '<b>Error al conectarse al servidor, revise su conexión de internet!</b>']);      
              }
         }
    }

    public function borrar_publicacion($id){

       $informes=Informe::find($id);
        if($informes->delete()){            
            return view("mensajes.msj_correcto")->with("msj","Borrado correctamente");   
        }else{            
            return view("mensajes.msj_rechazado")->with("msj","hubo un error vuelva a intentarlo");  
        }
    }

    public static function tiene_listado_cargado($id) {
        $informe1 = \DB::select("select * from sig_informe where id_monitoreo='$id' 
                                  and id_subcategoria_etiqueta in (select id from subcategoria_etiqueta 
                                  where categoria_etiqueta_id = 16 AND estado=TRUE 
                                  AND descripcion_cat ilike 'LISTADO DE ASISTENCIA%')");

        if(count($informe1))
            return 'SI';
        return 'NO';
    }
}