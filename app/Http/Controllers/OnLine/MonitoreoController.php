<?php

namespace App\Http\Controllers\OnLine;

use App\Monitoreo;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\ListaSustentantes;
use App\ListaSustentantes_off;
use App\TableroMonitoreo;
use App\CgiDetalleSustentante;
use App\SigAplicadores;
use App\SigInstiitucion;
use App\Provincia;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Session;

class MonitoreoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $usuario_actual = \Auth::user();
        if ($usuario_actual->user_group_id == 1) {
            $monitoreo = Monitoreo::where('cgi_periodo_id', Session::get('Periodo', 0))->where('estado_sesion2','online')->where('estado', 1)->orderBy('id_sede', 'asc')->orderBy('fecha_programada_inicio', 'asc')->paginate(10);
            $subCategoriaetiqueta = \DB::select('select * from subcategoria_etiqueta where categoria_etiqueta_id = 21 and estado=TRUE order by 3');
            $periodo = Session::get('Periodo', 0);
            $fechaProramadaInicio = \DB::select("select distinct(sesion) as sesion from umonitoreo where estado=1 and cgi_periodo_id = '$periodo' and estado_sesion2 = 'online' order by 1");
            $fecha = 0;
            $avanceasistencia = \DB::table('umonitoreo')->select(\DB::raw('SUM(programados) as programadosa'), \DB::raw('SUM(asistencia) as asistencia'), \DB::raw('round(sum(asistencia)/sum(programados)*100) as porcentajeasistencia'), \DB::raw('round(sum(cargas)/sum(programados)*100) as porcentajecargas'), \DB::raw('round(sum(cargas)) as cargas'))->where('cgi_periodo_id', Session::get('Periodo', 0))->where('estado_sesion2','online')->where('estado', 1)->first();
            $name_periodo = \DB::table('umonitoreo')->where('estado', 1)->where('cgi_periodo_id', Session::get('Periodo', 0))->where('estado_sesion2','online')->first();
            $avanceiniciar = \DB::select("select (case when estado_sesion ='' then 'NO'
             when estado_sesion = 'NO' then estado_sesion
             when estado_sesion is null then 'NO' end) as sesion, count(*) as cantidad from umonitoreo where estado=1 and cgi_periodo_id = '$periodo'  and estado_sesion2 = 'online' and
             (case when estado_sesion ='' then 'NO'
             when estado_sesion = 'NO' then estado_sesion
             when estado_sesion is null then 'NO' end)='NO'
             group by
             (case when estado_sesion ='' then 'NO'
             when estado_sesion = 'NO' then estado_sesion
             when estado_sesion is null then 'NO' end)");
            $avancesesion = \DB::select("select 
a.cantidad, b.sesion, round((a.cantidad::numeric / b.sesion::numeric)*100, 2) as porcentaje from
( 
SELECT (case when estado_sesion ='SI' then estado_sesion end) as sesion, COUNT(*) as cantidad
FROM umonitoreo WHERE estado=1 and cgi_periodo_id = '$periodo' and estado_sesion2 = 'online' and (case when estado_sesion ='SI' then estado_sesion end) = 'SI'   
AND estado = 1 
GROUP BY (case when estado_sesion ='SI' then estado_sesion end)) as a,
(select count(*) as sesion from umonitoreo where estado=1 and cgi_periodo_id = '$periodo' and estado_sesion2 = 'online' ) as b");
            
            return view('listados.onLine.listado_monitoreo')->with("monitoreo", $monitoreo)->with("subCategoriaetiqueta", $subCategoriaetiqueta)->with("fechaProramadaInicio", $fechaProramadaInicio)->with("fecha", $fecha)->with("avanceasistencia", $avanceasistencia)->with("avancesesion", $avancesesion)->with("avanceiniciar", $avanceiniciar)->with("name_periodo", $name_periodo);
        } else {
            $monitoreo = Monitoreo::where('cgi_periodo_id', Session::get('Periodo', 0))->where('estado_sesion2','online')->where('estado', 1)->where('monitor', Auth::user()->username)->orderBy('sesion', 'asc')->orderBy('id_sede', 'asc')->paginate(10);
            $subCategoriaetiqueta = \DB::select('select * from subcategoria_etiqueta where categoria_etiqueta_id = 21  and estado = TRUE order by 1');
            $periodo = Session::get('Periodo', 0);
            $monitor = Auth::user()->username;
            $fechaProramadaInicio = \DB::select("select distinct(sesion) as sesion from umonitoreo where estado=1 and cgi_periodo_id = '$periodo' and estado_sesion2 = 'online' and monitor='$monitor' order by 1");
            $fecha = 0;
            $avanceasistencia = \DB::table('umonitoreo')->select(\DB::raw('SUM(programados) as programadosa'), \DB::raw('SUM(asistencia) as asistencia'), \DB::raw('round(sum(asistencia)/sum(programados)*100) as porcentajeasistencia'), \DB::raw('round(sum(cargas)/sum(programados)*100) as porcentajecargas'), \DB::raw('round(sum(cargas)) as cargas'))->where('cgi_periodo_id', Session::get('Periodo', 0))->where('estado_sesion2','online')->where('monitor', Auth::user()->username)->where('estado', 1)->first();
            $name_periodo = \DB::table('umonitoreo')->where('estado', 1)->where('cgi_periodo_id', Session::get('Periodo', 0))->where('estado_sesion2','online')->first();
            $avanceiniciar = \DB::select("select (case when estado_sesion ='' then 'NO'
             when estado_sesion = 'NO' then estado_sesion
             when estado_sesion is null then 'NO' end) as sesion, count(*) as cantidad from umonitoreo where estado=1 and cgi_periodo_id = '$periodo' and estado_sesion2 = 'online' and
             (case when estado_sesion ='' then 'NO'
             when estado_sesion = 'NO' then estado_sesion
             when estado_sesion is null then 'NO' end)='NO' and monitor='$monitor'
             group by
             (case when estado_sesion ='' then 'NO'
             when estado_sesion = 'NO' then estado_sesion
             when estado_sesion is null then 'NO' end)");
            $avancesesion = \DB::select("select 
a.cantidad, b.sesion, round((a.cantidad::numeric / b.sesion::numeric)*100, 2) as porcentaje from
( 
SELECT (case when estado_sesion ='SI' then estado_sesion end) as sesion, COUNT(*) as cantidad
FROM umonitoreo WHERE estado=1 and cgi_periodo_id = '$periodo' and estado_sesion2 = 'online' and monitor='$monitor' and (case when estado_sesion ='SI' then estado_sesion end) = 'SI'   
AND estado = 1 
GROUP BY (case when estado_sesion ='SI' then estado_sesion end)) as a,
(select count(*) as sesion from umonitoreo where estado=1 and cgi_periodo_id = '$periodo' and estado_sesion2 = 'online' and monitor='$monitor' ) as b");

            //Actualizar inicio de sesión por laboratorio, si almenos un sustentante a ingresado a la aplicación.
            $monitoreoInicioSesion = Monitoreo::select('cgi_laboratorio_id')->where('cgi_periodo_id', Session::get('Periodo', 0))->where('estado_sesion2','online')->where('estado', 1)->where('monitor', Auth::user()->username)->where(function($query) {
                    $query->whereNull('estado_sesion')
                          ->orWhere('estado_sesion','!=', 'SI');
                })->get();

            if(count($monitoreoInicioSesion)>0){
                $monitoreoSesionLab=[];
                foreach ($monitoreoInicioSesion as $monitoreoLab) {
                    $monitoreoSesionLab[]= $monitoreoLab->cgi_laboratorio_id;
                }
                $inicioSustentantesLab=ListaSustentantes::select('cgi_laboratorio_id')->where('inicio_sesion','true')->whereIn('cgi_laboratorio_id', $monitoreoSesionLab)->groupBy('cgi_laboratorio_id')->get();
                if(count($inicioSustentantesLab)>0){
                    $inicioLab=[];
                    foreach ($inicioSustentantesLab as $inicioSustentantes) {
                        $inicioLab[]= $inicioSustentantes->cgi_laboratorio_id;
                    }
                    $sesionLab = Monitoreo::select('estado_sesion')->where('cgi_periodo_id', Session::get('Periodo', 0))->whereIn('cgi_laboratorio_id',$inicioLab)->update(['estado_sesion' => 'SI']);
                }
            }

            return view('listados.onLine.listado_monitoreo')->with("monitoreo", $monitoreo)->with("subCategoriaetiqueta", $subCategoriaetiqueta)->with("fechaProramadaInicio", $fechaProramadaInicio)->with("fecha", $fecha)->with("avanceasistencia", $avanceasistencia)->with("avancesesion", $avancesesion)->with("avanceiniciar", $avanceiniciar)->with("name_periodo", $name_periodo);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function buscar_monitoreo($fecha, $dato = "")
    {
        if ($fecha == 0) {
            $fech = '';
        } else {
            $fech = "AND sesion='$fecha'";
        }
        $usuario_actual = \Auth::user();
        if ($usuario_actual->user_group_id == 1) {
            $avanceasistencia = \DB::table('umonitoreo')->select(\DB::raw('SUM(programados) as programadosa'), \DB::raw('SUM(asistencia) as asistencia'), \DB::raw('round(sum(asistencia)/sum(programados)*100) as porcentajeasistencia'), \DB::raw('round(sum(cargas)/sum(programados)*100) as porcentajecargas'), \DB::raw('round(sum(cargas)) as cargas'))->where('cgi_periodo_id', Session::get('Periodo', 0))->where('estado_sesion2','online')->where('estado', 1)->first();
            $name_periodo = \DB::table('umonitoreo')->where('estado', 1)->where('cgi_periodo_id', Session::get('Periodo', 0))->where('estado_sesion2','online')->first();
            $monitoreo = Monitoreo::Busqueda($fecha, $dato)->where('estado_sesion2','online')->orderBy('codigoamei', 'asc')->orderBy('fecha_programada_inicio', 'asc')->paginate(10);
            $subCategoriaetiqueta = \DB::select('select * from subcategoria_etiqueta where categoria_etiqueta_id = 21 and estado = true order by 1');
            $periodo = Session::get('Periodo', 0);
            $monitor = Auth::user()->username;
            $fechaProramadaInicio = \DB::select("select distinct(sesion) as sesion from umonitoreo where estado=1 and cgi_periodo_id = '$periodo' and estado_sesion2 = 'online' order by 1");
            ///$paises=Pais::all();
            ///$paissel=$paises->find($fecha);
            $avanceiniciar = \DB::select("select (case when estado_sesion ='' then 'NO'
             when estado_sesion = 'NO' then estado_sesion
             when estado_sesion is null then 'NO' end) as sesion, count(*) as cantidad from umonitoreo where estado=1 and cgi_periodo_id = '$periodo' and estado_sesion2 = 'online' and
             (case when estado_sesion ='' then 'NO'
             when estado_sesion = 'NO' then estado_sesion
             when estado_sesion is null then 'NO' end)='NO' $fech
             group by
             (case when estado_sesion ='' then 'NO'
             when estado_sesion = 'NO' then estado_sesion
             when estado_sesion is null then 'NO' end)");
            $avancesesion = \DB::select("select 
a.cantidad, b.sesion, round((a.cantidad::numeric / b.sesion::numeric)*100, 2) as porcentaje from
( 
SELECT (case when estado_sesion ='SI' then estado_sesion end) as sesion, COUNT(*) as cantidad
FROM umonitoreo WHERE estado=1 and cgi_periodo_id = '$periodo' and estado_sesion2 = 'online' $fech and (case when estado_sesion ='SI' then estado_sesion end) = 'SI'   
AND estado = 1 
GROUP BY (case when estado_sesion ='SI' then estado_sesion end)) as a,
(select count(*) as sesion from umonitoreo where estado=1 $fech and cgi_periodo_id = '$periodo'  and estado_sesion2 = 'online') as b");
            return view('listados.onLine.listado_monitoreo')
                ->with("subCategoriaetiqueta", $subCategoriaetiqueta)
                ->with("fechaProramadaInicio", $fechaProramadaInicio)
                ->with("monitoreo", $monitoreo)
                ->with("fecha", $fecha)
                ->with("avanceasistencia", $avanceasistencia)
                ->with("avancesesion", $avancesesion)
                ->with("avanceiniciar", $avanceiniciar)
                ->with("name_periodo", $name_periodo);
        } else {
            $avanceasistencia = \DB::table('umonitoreo')->select(\DB::raw('SUM(programados) as programadosa'), \DB::raw('SUM(asistencia) as asistencia'), \DB::raw('round(sum(asistencia)/sum(programados)*100) as porcentajeasistencia'), \DB::raw('round(sum(cargas)/sum(programados)*100) as porcentajecargas'), \DB::raw('round(sum(cargas)) as cargas'))->where('cgi_periodo_id', Session::get('Periodo', 0))->where('estado_sesion2','online')->where('monitor', Auth::user()->username)->where('estado', 1)->first();
            $name_periodo = \DB::table('umonitoreo')->where('estado', 1)->where('cgi_periodo_id', Session::get('Periodo', 0))->where('estado_sesion2','online')->first();
            $monitoreo = Monitoreo::Busqueda($fecha, $dato)->where('estado_sesion2','online')->where('monitor', Auth::user()->username)->orderBy('codigoamei', 'asc')->orderBy('fecha_programada_inicio', 'asc')->paginate(10);
            $subCategoriaetiqueta = \DB::select('select * from subcategoria_etiqueta where categoria_etiqueta_id = 21 and estado = true order by 1');
            $periodo = Session::get('Periodo', 0);
            $monitor = Auth::user()->username;
            $fechaProramadaInicio = \DB::select("select distinct(sesion) as sesion from umonitoreo where estado=1 and cgi_periodo_id = '$periodo' and estado_sesion2 = 'online' and monitor='$monitor' order by 1");
            ///$paises=Pais::all();
            ///$paissel=$paises->find($fecha);
            $avanceiniciar = \DB::select("select (case when estado_sesion ='' then 'NO'
             when estado_sesion = 'NO' then estado_sesion
             when estado_sesion is null then 'NO' end) as sesion, count(*) as cantidad from umonitoreo where estado=1 and cgi_periodo_id = '$periodo' and estado_sesion2 = 'online' and
             (case when estado_sesion ='' then 'NO'
             when estado_sesion = 'NO' then estado_sesion
             when estado_sesion is null then 'NO' end)='NO' and monitor='$monitor' $fech
             group by
             (case when estado_sesion ='' then 'NO'
             when estado_sesion = 'NO' then estado_sesion
             when estado_sesion is null then 'NO' end)");
            $avancesesion = \DB::select("select 
a.cantidad, b.sesion, round((a.cantidad::numeric / b.sesion::numeric)*100, 2) as porcentaje from
( 
SELECT (case when estado_sesion ='SI' then estado_sesion end) as sesion, COUNT(*) as cantidad
FROM umonitoreo WHERE estado=1 and cgi_periodo_id = '$periodo' and estado_sesion2 = 'online' $fech and monitor='$monitor' and (case when estado_sesion ='SI' then estado_sesion end) = 'SI'   
AND estado = 1 
GROUP BY (case when estado_sesion ='SI' then estado_sesion end)) as a,
(select count(*) as sesion from umonitoreo where estado=1 $fech and cgi_periodo_id = '$periodo' and monitor='$monitor' and estado_sesion2 = 'online' ) as b");
            return view('listados.onLine.listado_monitoreo')
                ->with("subCategoriaetiqueta", $subCategoriaetiqueta)
                ->with("fechaProramadaInicio", $fechaProramadaInicio)
                ->with("monitoreo", $monitoreo)
                ->with("fecha", $fecha)
                ->with("avanceasistencia", $avanceasistencia)
                ->with("avancesesion", $avancesesion)
                ->with("avanceiniciar", $avanceiniciar)
                ->with("name_periodo", $name_periodo);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $monitoreo = TableroMonitoreo::find($id);
        if ($monitoreo) {           
            $categoria = "";            
            //////categoria
            if ($request->input("id_subcategoria_etiqueta") == "") {
                $categoria = $monitoreo->observacion_sesion;
            } elseif ($request->input("id_subcategoria_etiqueta") != "") {
                $categoria = $request->input("id_subcategoria_etiqueta");
            }

            //// update
            $monitoreo->observacion_sesion = $categoria;
            if ($monitoreo->save())
                return response()->json(['status' => 'success', 'message' => '<b>¡Registro guardado!</b>']);
            else
                return response()->json(['status' => 'warning', 'message' => '<b>¡Ha ocurrido un error!</b>']);

        } else {
            return response()->json(['status' => 'warning', 'message' => '<b>¡Registro no encontrado!</b>']);
        }
    }
    public function form_monitoreo_institucion($id, $id_sede, $idmo)
    {
        $institucion = \DB::table('sig_institucionv')->where('amie', $id)->first();
        $umonitoreo = \DB::table('umonitoreo')->where('id2', $idmo)->where('cgi_periodo_id', Session::get('Periodo', 0))->where('estado_sesion2','online')->first();
        return view("formularios.onLine.form_monitoreo_institucion")->with("institucion", $institucion)->with("id", $id)->with("umonitoreo", $umonitoreo)->with('idmo', $idmo);

    }

    public function editar_rector_monitoreo(Request $request)
    {
        $editarAplicador = false;
        $editarResponsable = false;
        $editarSProvincial = false;
        $editarSTecnico = false;
        $respuesta = '';

        //TableroMonitoreo
        $celular1_wa = 0;
        $celular2_wa = 0;
        $celular1_swa = 0;
        $celular2_swa = 0;
        $apli_telef1_wa = 0;
        $tecni_telef1_wa = 0;
        if ($request->input("celular1_wa") == "on" or $request->input("celular1_wa") == "1") {
            $celular1_wa = 1;
        } else {
            $celular1_wa = null;
        }
        /////celular2_wa
        if ($request->input("celular2_wa") == "on" or $request->input("celular2_wa") == "1") {
            $celular2_wa = 1;
        } else {
            $celular2_wa = null;
        }
        /////celular1_swa
        if ($request->input("celular1_swa") == "on" or $request->input("celular1_swa") == "1") {
            $celular1_swa = 1;
        } else {
            $celular1_swa = null;
        }
        /////celular2_swa
        if ($request->input("celular2_swa") == "on" or $request->input("celular2_swa") == "1") {
            $celular2_swa = 1;
        } else {
            $celular2_swa = null;
        }
        /////apli_telef1_wa
        if ($request->input("apli_telef1_wa") == "on" or $request->input("apli_telef1_wa") == "1") {
            $apli_telef1_wa = 1;
        } else {
            $apli_telef1_wa = null;
        }
        /////tecni_telef1_wa
        if ($request->input("tecni_telef1_wa") == "on" or $request->input("tecni_telef1_wa") == "1") {
            $tecni_telef1_wa = 1;
        } else {
            $tecni_telef1_wa = null;
        }
        /////corresponsable_celular_wa
        if ($request->input("corresponsable_celular_wa") == "on" or $request->input("corresponsable_celular_wa") == "1") {
            $corresponsable_celular_wa = 1;
        } else {
            $corresponsable_celular_wa = null;
        }
        $data = $request->all();
        $id2 = $data["id11"];
        $amie = $data["id111"];
        
        $umonitoreos = TableroMonitoreo::find($amie);
        $validaciones = SigInstiitucion::find($id2);
        if ($validaciones) {            
            if (($data["estadollamada"] == "CONTACTADO" || $data["estadollamada"] == "INICIADA") and $data["novedadllamada"] == ""){
               
                $validaciones->celular = $data["celular"];
                $validaciones->celular1_wa = $celular1_wa;
                $validaciones->celular_whatsapp = $data["celularw"];
                $validaciones->celular2_wa = $celular2_wa;
                $validaciones->telefono1 = $data["telefono1"];
                $validaciones->telefono2 = $data["telefono2"];
                $validaciones->contactoinstitucion = $data["tele1"];
                $validaciones->telefono_contactado = $data["telefono_contactado"];                
                $validaciones->comunicacion = $data["comunicacion"];
                $umonitoreos->estadollamada_apli = $data["estadollamada"];
                $umonitoreos->novedadllamada_apli = $data["novedadllamada"];
                $validaciones->recibiocomunicado = $data["recibiocomunicado"];
                $validaciones->observacion = $data["observacion"];

                ///guia evaluador (aplicador)
                if ($umonitoreos->apli_nombre1 !== $data["apli_nombre1"])
                    $editarAplicador = true;
                $umonitoreos->apli_nombre1 = $data["apli_nombre1"];
                if ($umonitoreos->apli_cedula !== $data["apli_cedula"])
                    $editarAplicador = true;
                $umonitoreos->apli_cedula = $data["apli_cedula"];
                if ($umonitoreos->apli_telef1 !== $data["apli_telef1"])
                    $editarAplicador = true;
                $umonitoreos->apli_telef1 = $data["apli_telef1"];
                $umonitoreos->apli_telef1_wa = $apli_telef1_wa;
                if ($umonitoreos->apli_telef2 !== $data["apli_telef2"])
                    $editarAplicador = true;
                $umonitoreos->apli_telef2 = $data["apli_telef2"];
                if ($umonitoreos->apli_telef3 !== $data["apli_telef3"])
                    $editarAplicador = true;
                $umonitoreos->apli_telef3 = $data["apli_telef3"];
                $umonitoreos->apli_telef_contactado = $data["apli_telef_contactado"];
                if ($umonitoreos->apli_email !== $data["apli_email"])
                    $editarAplicador = true;
                $umonitoreos->apli_email = $data["apli_email"];
                if ($umonitoreos->apli_email2 !== $data["apli_email2"])
                    $editarAplicador = true;
                $umonitoreos->apli_email2 = $data["apli_email2"];
                $umonitoreos->apli_comunicacion = $data["apli_comunicacion"];
                $umonitoreos->estadollamada_aplicadores = $data["estadollamada_aplicadores"];
                $umonitoreos->novedadllamada_aplicadores = $data["novedadllamada_aplicadores"];
                $umonitoreos->asistencia_ineval = $data["asistencia_ineval"];
                $umonitoreos->apli_recepcion = $data["apli_recepcion"];
                $umonitoreos->apli_motivo = $data["apli_motivo"];

                ///tecnico de control
                /*if ($umonitoreos->tecni_nombre1 !== $data["tecni_nombre1"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_nombre1 = $data["tecni_nombre1"];
                if ($umonitoreos->tecni_cedula !== $data["tecni_cedula"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_cedula = $data["tecni_cedula"];
                if ($umonitoreos->tecni_telef1 !== $data["tecni_telef1"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_telef1 = $data["tecni_telef1"];
                $umonitoreos->tecni_telef1_wa = $tecni_telef1_wa;
                if ($umonitoreos->tecni_telef2 !== $data["tecni_telef2"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_telef2 = $data["tecni_telef2"];
                if ($umonitoreos->tecni_telef3 !== $data["tecni_telef3"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_telef3 = $data["tecni_telef3"];
                $umonitoreos->tecni_telef_contactado = $data["tecni_telef_contactado"];
                if ($umonitoreos->tecni_email !== $data["tecni_email"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_email = $data["tecni_email"];
                if ($umonitoreos->tecni_email2 !== $data["tecni_email2"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_email2 = $data["tecni_email2"];
                $umonitoreos->tecni_comunicacion = $data["tecni_comunicacion"];
                $umonitoreos->estadollamada_tecnico = $data["estadollamada_tecnico"];
                $umonitoreos->novedadllamada_tecnico = $data["novedadllamada_tecnico"];
                $umonitoreos->tecni_asistencia = $data["tecni_asistencia"];
                $umonitoreos->tecni_recibiocomunicado = $data["tecni_recibiocomunicado"];
                $umonitoreos->tecni_motivo = $data["tecni_motivo"];
                */
                ///corresponsables
                if ($umonitoreos->corresponsable_cedula !== $data["corresponsable_cedula"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable_cedula = $data["corresponsable_cedula"];
                if ($umonitoreos->corresponsable !== $data["corresponsable"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable = $data["corresponsable"];
                if ($umonitoreos->corresponsable_celular !== $data["corresponsable_celular"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable_celular = $data["corresponsable_celular"];
                $umonitoreos->corresponsable_celular_wa = $corresponsable_celular_wa;
                if ($umonitoreos->corresponsable_telefono !== $data["corresponsable_telefono"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable_telefono = $data["corresponsable_telefono"];
                if ($umonitoreos->corresponsable_correo !== $data["corresponsable_correo"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable_correo = $data["corresponsable_correo"];
                if ($umonitoreos->corresponsable_correo2 !== $data["corresponsable_correo2"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable_correo2 = $data["corresponsable_correo2"];
                $umonitoreos->corresponsable_comunicacion = $data["corresponsable_comunicacion"];
                $umonitoreos->estadollamada_corresponsable = $data["estadollamada_corresponsable"];
                $umonitoreos->novedadllamada_corresponsable = $data["novedadllamada_corresponsable"];
                $umonitoreos->asistencia_corresponsable = $data["asistencia_corresponsable"];
                $umonitoreos->recepcion_clave_corresponsable = $data["recepcion_clave_corresponsable"];
                $umonitoreos->corresponsable_motivo = $data["corresponsable_motivo"];                

                if ($validaciones->save()) {
                    $umonitoreos->save();
                    return response()->json(['status' => 'success', 'message' => '<b>¡Registro guardado!</b>']);
                }
            } elseif ($data["estadollamada"] == "CONTACTADO" and $data["novedadllamada"] == "VOLVER A LLAMAR") {
                $validaciones->celular = $data["celular"];
                $validaciones->celular1_wa = $celular1_wa;
                $validaciones->celular_whatsapp = $data["celularw"];
                $validaciones->celular2_wa = $celular2_wa;
                $validaciones->telefono1 = $data["telefono1"];
                $validaciones->telefono2 = $data["telefono2"];
                $validaciones->contactoinstitucion = $data["tele1"];
                $validaciones->telefono_contactado = $data["telefono_contactado"];
                $validaciones->comunicacion = $data["comunicacion"];
                $umonitoreos->estadollamada_apli = $data["estadollamada"];
                $umonitoreos->novedadllamada_apli = $data["novedadllamada"];
                $validaciones->recibiocomunicado = $data["recibiocomunicado"];
                $validaciones->observacion = $data["observacion"];

                ///guia evaluador (aplicador)
                if ($umonitoreos->apli_nombre1 !== $data["apli_nombre1"])
                    $editarAplicador = true;
                $umonitoreos->apli_nombre1 = $data["apli_nombre1"];
                if ($umonitoreos->apli_cedula !== $data["apli_cedula"])
                    $editarAplicador = true;
                $umonitoreos->apli_cedula = $data["apli_cedula"];
                if ($umonitoreos->apli_telef1 !== $data["apli_telef1"])
                    $editarAplicador = true;
                $umonitoreos->apli_telef1 = $data["apli_telef1"];
                $umonitoreos->apli_telef1_wa = $apli_telef1_wa;
                if ($umonitoreos->apli_telef2 !== $data["apli_telef2"])
                    $editarAplicador = true;
                $umonitoreos->apli_telef2 = $data["apli_telef2"];
                if ($umonitoreos->apli_telef3 !== $data["apli_telef3"])
                    $editarAplicador = true;
                $umonitoreos->apli_telef3 = $data["apli_telef3"];
                $umonitoreos->apli_telef_contactado = $data["apli_telef_contactado"];
                if ($umonitoreos->apli_email !== $data["apli_email"])
                    $editarAplicador = true;
                $umonitoreos->apli_email = $data["apli_email"];
                if ($umonitoreos->apli_email2 !== $data["apli_email2"])
                    $editarAplicador = true;
                $umonitoreos->apli_email2 = $data["apli_email2"];
                $umonitoreos->apli_comunicacion = $data["apli_comunicacion"];
                $umonitoreos->estadollamada_aplicadores = $data["estadollamada_aplicadores"];
                $umonitoreos->novedadllamada_aplicadores = $data["novedadllamada_aplicadores"];
                $umonitoreos->asistencia_ineval = $data["asistencia_ineval"];
                $umonitoreos->apli_recepcion = $data["apli_recepcion"];
                $umonitoreos->apli_motivo = $data["apli_motivo"];

                ///tecnico de control
                /*if ($umonitoreos->tecni_nombre1 !== $data["tecni_nombre1"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_nombre1 = $data["tecni_nombre1"];
                if ($umonitoreos->tecni_cedula !== $data["tecni_cedula"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_cedula = $data["tecni_cedula"];
                if ($umonitoreos->tecni_telef1 !== $data["tecni_telef1"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_telef1 = $data["tecni_telef1"];
                $umonitoreos->tecni_telef1_wa = $tecni_telef1_wa;
                if ($umonitoreos->tecni_telef2 !== $data["tecni_telef2"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_telef2 = $data["tecni_telef2"];
                if ($umonitoreos->tecni_telef3 !== $data["tecni_telef3"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_telef3 = $data["tecni_telef3"];
                $umonitoreos->tecni_telef_contactado = $data["tecni_telef_contactado"];
                if ($umonitoreos->tecni_email !== $data["tecni_email"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_email = $data["tecni_email"];
                if ($umonitoreos->tecni_email2 !== $data["tecni_email2"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_email2 = $data["tecni_email2"];
                $umonitoreos->tecni_comunicacion = $data["tecni_comunicacion"];
                $umonitoreos->estadollamada_tecnico = $data["estadollamada_tecnico"];
                $umonitoreos->novedadllamada_tecnico = $data["novedadllamada_tecnico"];
                $umonitoreos->tecni_asistencia = $data["tecni_asistencia"];
                $umonitoreos->tecni_recibiocomunicado = $data["tecni_recibiocomunicado"];
                $umonitoreos->tecni_motivo = $data["tecni_motivo"];
                */
                ///corresponsables
                if ($umonitoreos->corresponsable_cedula !== $data["corresponsable_cedula"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable_cedula = $data["corresponsable_cedula"];
                if ($umonitoreos->corresponsable !== $data["corresponsable"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable = $data["corresponsable"];
                if ($umonitoreos->corresponsable_celular !== $data["corresponsable_celular"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable_celular = $data["corresponsable_celular"];
                $umonitoreos->corresponsable_celular_wa = $corresponsable_celular_wa;
                if ($umonitoreos->corresponsable_telefono !== $data["corresponsable_telefono"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable_telefono = $data["corresponsable_telefono"];
                if ($umonitoreos->corresponsable_correo !== $data["corresponsable_correo"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable_correo = $data["corresponsable_correo"];
                if ($umonitoreos->corresponsable_correo2 !== $data["corresponsable_correo2"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable_correo2 = $data["corresponsable_correo2"];
                $umonitoreos->corresponsable_comunicacion = $data["corresponsable_comunicacion"];
                $umonitoreos->estadollamada_corresponsable = $data["estadollamada_corresponsable"];
                $umonitoreos->novedadllamada_corresponsable = $data["novedadllamada_corresponsable"];
                $umonitoreos->asistencia_corresponsable = $data["asistencia_corresponsable"];
                $umonitoreos->recepcion_clave_corresponsable = $data["recepcion_clave_corresponsable"];
                $umonitoreos->corresponsable_motivo = $data["corresponsable_motivo"];

                if ($validaciones->save()) {
                    $umonitoreos->save();
                    return response()->json(['status' => 'success', 'message' => '<b>¡Registro guardadoooo!</b>']);
                }
            } elseif (($data["estadollamada"] == "NO CONTACTADO" || $data["estadollamada"] == "NO INICIADA") and $data["novedadllamada"] != "") {
                ////Responsable de sede
                $umonitoreos->estadollamada_apli = $data["estadollamada"];
                $umonitoreos->novedadllamada_apli = $data["novedadllamada"];
                $validaciones->observacion = $data["observacion"];

                ///guia evaluador (aplicador)
                if ($umonitoreos->apli_nombre1 !== $data["apli_nombre1"])
                    $editarAplicador = true;
                $umonitoreos->apli_nombre1 = $data["apli_nombre1"];
                if ($umonitoreos->apli_cedula !== $data["apli_cedula"])
                    $editarAplicador = true;
                $umonitoreos->apli_cedula = $data["apli_cedula"];
                if ($umonitoreos->apli_telef1 !== $data["apli_telef1"])
                    $editarAplicador = true;
                $umonitoreos->apli_telef1 = $data["apli_telef1"];
                $umonitoreos->apli_telef1_wa = $apli_telef1_wa;
                if ($umonitoreos->apli_telef2 !== $data["apli_telef2"])
                    $editarAplicador = true;
                $umonitoreos->apli_telef2 = $data["apli_telef2"];
                if ($umonitoreos->apli_telef3 !== $data["apli_telef3"])
                    $editarAplicador = true;
                $umonitoreos->apli_telef3 = $data["apli_telef3"];
                $umonitoreos->apli_telef_contactado = $data["apli_telef_contactado"];
                if ($umonitoreos->apli_email !== $data["apli_email"])
                    $editarAplicador = true;
                $umonitoreos->apli_email = $data["apli_email"];
                if ($umonitoreos->apli_email2 !== $data["apli_email2"])
                    $editarAplicador = true;
                $umonitoreos->apli_email2 = $data["apli_email2"];
                $umonitoreos->apli_comunicacion = $data["apli_comunicacion"];
                $umonitoreos->estadollamada_aplicadores = $data["estadollamada_aplicadores"];
                $umonitoreos->novedadllamada_aplicadores = $data["novedadllamada_aplicadores"];
                $umonitoreos->asistencia_ineval = $data["asistencia_ineval"];
                $umonitoreos->apli_recepcion = $data["apli_recepcion"];
                $umonitoreos->apli_motivo = $data["apli_motivo"];

                ///tecnico de control
                /*if ($umonitoreos->tecni_nombre1 !== $data["tecni_nombre1"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_nombre1 = $data["tecni_nombre1"];
                if ($umonitoreos->tecni_cedula !== $data["tecni_cedula"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_cedula = $data["tecni_cedula"];
                if ($umonitoreos->tecni_telef1 !== $data["tecni_telef1"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_telef1 = $data["tecni_telef1"];
                $umonitoreos->tecni_telef1_wa = $tecni_telef1_wa;
                if ($umonitoreos->tecni_telef2 !== $data["tecni_telef2"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_telef2 = $data["tecni_telef2"];
                if ($umonitoreos->tecni_telef3 !== $data["tecni_telef3"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_telef3 = $data["tecni_telef3"];
                $umonitoreos->tecni_telef_contactado = $data["tecni_telef_contactado"];
                if ($umonitoreos->tecni_email !== $data["tecni_email"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_email = $data["tecni_email"];
                if ($umonitoreos->tecni_email2 !== $data["tecni_email2"])
                    $editarSTecnico = true;
                $umonitoreos->tecni_comunicacion = $data["tecni_comunicacion"];
                $umonitoreos->estadollamada_tecnico = $data["estadollamada_tecnico"];
                $umonitoreos->novedadllamada_tecnico = $data["novedadllamada_tecnico"];
                $umonitoreos->tecni_asistencia = $data["tecni_asistencia"];
                $umonitoreos->tecni_recibiocomunicado = $data["tecni_recibiocomunicado"];
                $umonitoreos->tecni_motivo = $data["tecni_motivo"];
                */
                ///corresponsables
                if ($umonitoreos->corresponsable_cedula !== $data["corresponsable_cedula"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable_cedula = $data["corresponsable_cedula"];
                if ($umonitoreos->corresponsable !== $data["corresponsable"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable = $data["corresponsable"];
                if ($umonitoreos->corresponsable_celular !== $data["corresponsable_celular"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable_celular = $data["corresponsable_celular"];
                $umonitoreos->corresponsable_celular_wa = $corresponsable_celular_wa;
                if ($umonitoreos->corresponsable_telefono !== $data["corresponsable_telefono"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable_telefono = $data["corresponsable_telefono"];
                if ($umonitoreos->corresponsable_correo !== $data["corresponsable_correo"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable_correo = $data["corresponsable_correo"];
                if ($umonitoreos->corresponsable_correo2 !== $data["corresponsable_correo2"])
                    $editarSProvincial = true;
                $umonitoreos->corresponsable_correo2 = $data["corresponsable_correo2"];
                $umonitoreos->corresponsable_comunicacion = $data["corresponsable_comunicacion"];
                $umonitoreos->estadollamada_corresponsable = $data["estadollamada_corresponsable"];
                $umonitoreos->novedadllamada_corresponsable = $data["novedadllamada_corresponsable"];
                $umonitoreos->asistencia_corresponsable = $data["asistencia_corresponsable"];
                $umonitoreos->recepcion_clave_corresponsable = $data["recepcion_clave_corresponsable"];
                $umonitoreos->corresponsable_motivo = $data["corresponsable_motivo"];

                if ($umonitoreos->save()) {
                    $validaciones->save();
                    return response()->json(['status' => 'success', 'message' => '<b>¡Registro guardado!</b>']);
                } else {
                    return response()->json(['status' => 'warning', 'message' => '<b>¡Error al actualizar.!</b>']);
                }
            } else {
                return response()->json(['status' => 'warning', 'message' => '<b>¡Selección Incorrecta!</b>']);
            }
        } else {
            return response()->json(['status' => 'warning', 'message' => '<b>¡Error al actualizar!</b>']);
        }
    }

    public function form_monitoreo_sustentantes($id, $amie, $fecha, $sesion, $id_usuario)
    {
        $listadoSustentantes = ListaSustentantes::where('cgi_laboratorio_id', $id)->orderBy('apellidos', 'asc')->paginate(5000);
        $subCategoriaetiqueta = \DB::select('select * from subcategoria_etiqueta where categoria_etiqueta_id in (3,17,18,19,20,22,23) and estado = true');
        return view("formularios.onLine.form_monitoreo_sustentantes")->with("listadoSustentantes", $listadoSustentantes)
                ->with("subCategoriaetiqueta", $subCategoriaetiqueta);
    }

    public function buscar_sustentantes($id_lab, $dato_sustentantes = "")
    {
        $listadoSustentantes = ListaSustentantes::Busqueda($id_lab, $dato_sustentantes)->orderBy('apellidos', 'asc')->paginate(5000);
        $subCategoriaetiqueta = \DB::select('select * from subcategoria_etiqueta where categoria_etiqueta_id in (3,17,18,19,20,22,23) and estado = true');
        return view('formularios.onLine.form_monitoreo_sustentantes')
                ->with("listadoSustentantes", $listadoSustentantes)->with("subCategoriaetiqueta", $subCategoriaetiqueta);
    }

    public function update_sustentante(Request $request, $id)
    {        
        $asistencia = 0;
        $reprogramacion = 0;
        $subcategoria = null;
        ////asistencia
        if ($request->input("asistencia") == "on" or $request->input("asistencia") == "1") {
            $asistencia = 1;
        } else {
            $asistencia = null;
        }
        ////reprogramacion
        if ($request->input("reprogramacion") == "on" or $request->input("reprogramacion") == "1") {
            $reprogramacion = 1;
        } else {
            $reprogramacion = null;
        }

        if ($request->input("id_subcategoria_etiqueta") == "") {
            $subcategoria = null;
        } else {
            $subcategoria = $request->input("id_subcategoria_etiqueta");
        }

        $variable = (int)$id;
        $sustentantes = CgiDetalleSustentante::find($id);
        if ($sustentantes) {
            $sustentantes->asistencia = $asistencia;
            $sustentantes->reprogramacion = $reprogramacion;
            $sustentantes->monitor = $subcategoria;
            if ($sustentantes->save()) {
                return response()->json(['status' => 'success', 'message' => '<b>¡Registro guardado!</b>']);
            }
        }else{
            return response()->json(['status' => 'warning', 'message' => '<b>¡Registro no encontrado!</b>']);
        }

        return response()->json(['categoria' => $subcategoria, 'asistencia' => $asistencia, 'id' => $variable, 'reprogramacion' => $reprogramacion]);
    }

    public function editar_listados(Request $request){

        $data = $request->all();
        $idUsuario = $data["id5"];
        $usuario = TableroMonitoreo::find($idUsuario);
        $usuario->listado_asistencia = $data["listado_asistencia"];
        $usuario->listado_validacion = $data["listado_validacion"];
        $usuario->observacion_listado_asistencia = $data["observacion_listado_asistencia"];
        $resul = $usuario->save();
        if ($resul) {
            return response()->json(['status' => 'success', 'message' => '<b>¡Registro guardado!</b>']);
        } else {
            return response()->json(['status' => 'warning', 'message' => '<b>hubo un error vuelva a intentarlo!</b>']);
        }
    } 

    public function form_monitoreo_instalacion($id){   
        $monitoreoInstalacion = TableroMonitoreo::find($id);
        $subCategoriaetiqueta = \DB::select('select * from subcategoria_etiqueta where categoria_etiqueta_id = 24 and estado=true');
        $subCategoriaetiquetaInsta = \DB::select('select * from subcategoria_etiqueta where categoria_etiqueta_id = 25 and estado=true');
        $periodo = Session::get('Periodo', 0);
        $codigoveri = \DB::select("select * from sig_codigo_verificacion where cgi_periodo_id = '$periodo'");
        foreach ($codigoveri as $codigo) {
            $codigoVerificacionpc = $codigo->hashpc;
            $codigoVerificacionmovil = $codigo->hashmovil;
        }
        return view("formularios.onLine.form_monitoreo_instalacion")->with("monitoreoInstalacion", $monitoreoInstalacion)->with("subCategoriaetiqueta", $subCategoriaetiqueta)->with("codigoVerificacionpc", $codigoVerificacionpc)->with("codigoVerificacionmovil", $codigoVerificacionmovil)->with("subCategoriaetiquetaInsta", $subCategoriaetiquetaInsta)->with('id', $id);
    }

    public function editar_descarga(Request $request){

        $data = $request->all();
        $idUsuario = $data["id3"];
        $usuario = TableroMonitoreo::find($idUsuario);
        $usuario->recepcion_aplicativo = $data["raplicativo"];
        $usuario->descarga_aplicativo = $data["rdaplicativo"];
        $usuario->observacion_descarga = $data["ndaplicativo"];
        if ($usuario->save()) {
            return response()->json(['status' => 'success', 'message' => '<b>¡Registro guardado!</b>']);
        } else {
            return response()->json(['status' => 'warning', 'message' => '<b>hubo un error vuelva a intentarlo!</b>']);
        }
    }

    public function editar_instalacion(Request $request){

        $data = $request->all();
        $idUsuario = $data["id4"];
        $usuario = TableroMonitoreo::find($idUsuario);
        $usuario->instalo_aplicativo = $data["iaplicativo"];
        $usuario->comp_inst = $data["einstalados"];
        $usuario->observacion_instala = $data["ninstlacion"];
        $usuario->internet = $data["internet"];
        $usuario->impresora = $data["impresora"];
        if ($usuario->save()) {
            return response()->json(['status' => 'success', 'message' => '<b>¡Registro guardado!</b>']);
        } else {
            return response()->json(['status' => 'warning', 'message' => '<b>hubo un error vuelva a intentarlo!</b>']);
        }
    }   

}
