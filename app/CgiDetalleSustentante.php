<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CgiDetalleSustentante extends Model
{
    protected $table = 'cgi_detalle_sustentantes';
    public $timestamps = false; 
}
